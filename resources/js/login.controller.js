(function() {
    'use strict';

    angular
        .module('enlaces.login', ['ng', 'environment', 'angular-jwt', 'angular-loading-bar'])
        .config(function(envServiceProvider) {
            // set the domains and variables for each environment
            envServiceProvider.config({
                domains: {
                    development: ['localhost', 'enlaces.test'],
                    production: ['litisdata.legal', 'enlaces.legal'],
                    test: ['enlaces.assis.com.co'],
                    enlaces: ['litisdata.assis.com.co'],
                    litisdata: ['server2021.litisdata.com', 'litisdata.com']
                },
                vars: {
                    development: {
                        apiUrl: 'http://enlaces.test/api/'
                    },
                    production: {
                        apiUrl: 'https://enlaces.legal/api/'
                    },
                    enlaces: {
                        apiUrl: 'https://litisdata.assis.com.co/api/'
                    },
                    litisdata: {
                        apiUrl: 'https://server2021.litisdata.com/api/'
                    },
                    test: {
                        apiUrl: 'https://enlaces.assis.com.co/api/'
                    },
                    defaults: {
                        apiUrl: 'https://app.enlaces.com/api/'
                    }
                }
            });

            envServiceProvider.check();
        })
        .constant('Constantes', {
            Response: {
                HTTP_OK: 200,
                HTTP_INTERNAL_SERVER_ERROR: 500
            }
        })
        .controller('loginController', loginController);

    loginController.$inject = [
        '$http',
        'jwtHelper',
        'envService',
        'Constantes'
    ];

    function loginController($http, jwtHelper, envService, Constantes){
        var vm = this,
            query = window.location.search.substring(1),
            parametros = parse_query_string(query);
        // vm.attr1 = parametros.attr1 ? +parametros.attr1 : null;

        vm.login = login;
        vm.recuperarPassword = recuperarPassword;
        vm.cambiarPassword = cambiarPassword;

        validateSession();

        function validateSession() {
            var token = localStorage.getItem('token'),
                tokenType = localStorage.getItem('tokenType');
            var isTokenExpired = token ? jwtHelper.isTokenExpired(token) : true;
            if(!isTokenExpired){
                window.location = '/app';
            }
        }

        function login() {
            vm.submitted = true;
            if(vm.form.$valid){
                $http.post(envService.read('apiUrl') + "users/token", {
                    username: vm.username,
                    password: vm.password
                }).then(function (response) {
                    if(response.status === Constantes.Response.HTTP_OK){
                        getSession(response.data.access_token, response.data.token_type);
                    }else{
                        mostrarMensaje(response.data.messages[0]);
                    }
                }).catch(function (error) {
                    mostrarMensaje(error.data.messages[0]);
                });
            }
        }

        function getSession(token, tokenType) {
            $http({
                method: 'GET',
                url: envService.read('apiUrl') + 'users/current/session',
                headers: {
                    'Authorization': tokenType + " " + token
                }
            }).then(function (response) {
                if(response.status === Constantes.Response.HTTP_OK){
                    localStorage.setItem('token', token);
                    localStorage.setItem('tokenType', tokenType);
                    localStorage.setItem('session', JSON.stringify(response.data));
                    window.location = '/app';
                }
            });
        }

        function recuperarPassword() {
            vm.submitted = true;
            if(vm.form.$valid){
                $http.post('/enviar-enlace-recuperacion-password',{
                    username: vm.username
                }).then(function (response) {
                    if(response.status === Constantes.Response.HTTP_OK){
                        vm.enlaceRecuperacionPasswordEnviado = true;
                    } else {
                        mostrarMensaje(response.data.messages[0]);
                    }
                }).catch(function (error) {
                    mostrarMensaje(error.data.messages[0]);
                });
            }
        }

        function cambiarPassword() {
            vm.submitted = true;
            if(vm.form.$valid){
                if(vm.password != vm.confirmacionPassword){
                    mostrarMensaje("Los dos campos de contraseña no coinciden.");
                }else{
                    $http.put('/usuarios/password', {
                        token: $('#token').val(),
                        password: vm.password
                    }).then(function (response) {
                        if(response.status === Constantes.Response.HTTP_OK){
                            vm.restablecimientoCompletado = true;
                        } else {
                            mostrarMensaje(response.data.messages[0]);
                        }
                    }).catch(function (error) {
                        mostrarMensaje(error.data.messages[0]);
                    });
                }
            }
        }

        function mostrarMensaje(message) {
            MDSnackbars.hide();
            MDSnackbars.show({
                type: 'danger',
                text: '<div style="position: relative;"><span>' + message + '</span><a type="button" id="snackbar-action" style="cursor: pointer; position: absolute; right: 0; color: black;"><i class="material-icons">close</i></a></div>',
                html: true,
                fullWidth: true,
                clickToClose: true,
                timeout: 10000,
                animation: 'slideup'
            });
            setTimeout(function () {
                $("#snackbar-action").off("click").on("click", function () {
                    MDSnackbars.hide();
                })
            }, 1000);
        }

        // funciones utiles

        function parse_query_string(query) {
            var vars = query.split("&");
            var query_string = {};
            for (var i = 0; i < vars.length; i++) {
                var pair = vars[i].split("=");
                var key = decodeURIComponent(pair[0]);
                var value = decodeURIComponent(pair[1]);
                // If first entry with this name
                if (typeof query_string[key] === "undefined") {
                    query_string[key] = decodeURIComponent(value);
                    // If second entry with this name
                } else if (typeof query_string[key] === "string") {
                    var arr = [query_string[key], decodeURIComponent(value)];
                    query_string[key] = arr;
                    // If third or later entry with this name
                } else {
                    query_string[key].push(decodeURIComponent(value));
                }
            }
            return query_string;
        }

    }
})();
