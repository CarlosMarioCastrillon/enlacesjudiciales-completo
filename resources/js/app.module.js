(function() {
    'use strict';

    angular
        .module('enlaces.core', [
            'environment',
            'ng', //From the selectize XD
            'ngRoute',
            'angular-jwt',
            'ngAnimate',
            'perfect_scrollbar',
            'angular-loading-bar',
            'cgBusy',
            'ui.bootstrap',
            'smart-table',
            'ngFileUpload',
            'pascalprecht.translate',
            'ngSanitize'
        ]);

    angular
        .module('enlaces.app', [
            /* Shared modules */
            'enlaces.core'
        ]);

})();
