(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('menuController', menuController);

    menuController.$inject = [
        'envService',
        'genericService',
        'sessionUtil',
        'Constantes'
    ];

    function menuController(envService, genericService, sessionUtil, Constantes) {
        var menu = this;
        menu.companyLogo = "https://www.logogenie.net/download/preview/medium/4168236";
        menu.companyName = "Enlaces judiciales";

        cargarMenu();

        function cargarMenu() {
            genericService.obtenerColeccionLigera("opciones-del-sistema",{
                arbol_de_opciones: true
            })
            .then(function (response){
                if(response.status === Constantes.Response.HTTP_OK){
                    var modulos = [],
                        permisosHijos,
                        submodulos,
                        opcionesDelSistemaAplicadas = [],
                        opcionesDelSistema = [].concat(response.data);

                    angular.forEach(opcionesDelSistema, function (opcion) {
                        submodulos = [];
                        if(opcion.hijos && opcion.hijos.length > 0){
                            // Validar si los submodulos están permitidos para el usuario
                            permisosHijos = [];
                            angular.forEach(opcion.hijos, function (hijo) {
                                if(hijo.clave_permiso && sessionUtil.can([hijo.clave_permiso])){
                                    // Marcar el titulo de la página en la URL
                                    if(hijo.url && _.includes(hijo.url, "?")){
                                        var partesUrl = hijo.url.split("?");
                                        hijo.url = partesUrl[0] + "?o=" + hijo.id + "&" + partesUrl[1];
                                    }else{
                                        hijo.url += "?o=" + hijo.id;
                                    }
                                    submodulos.push(hijo);
                                    permisosHijos.push(hijo.clave_permiso);
                                    opcionesDelSistemaAplicadas.push(hijo);
                                }
                            });

                            // Validar que se tenga al menos un permiso a los submodulos
                            if(permisosHijos && permisosHijos.length > 0){
                                opcion.submodulos = submodulos;
                                modulos.push(opcion);
                            }
                        }else if(opcion.clave_permiso && sessionUtil.can([opcion.clave_permiso])){
                            // Marcar el titulo de la página en la URL
                            if(opcion.url && _.includes(opcion.url, "?")){
                                var partesUrl = opcion.url.split("?");
                                opcion.url = partesUrl[0] + "?o=" + opcion.id + "&" + partesUrl[1];
                            }else{
                                opcion.url += "?o=" + opcion.id;
                            }
                            // Si el modulo no tiene hijos se ingresa como un modulo independiente
                            opcion.submodulos = submodulos;
                            modulos.push(opcion);
                            opcionesDelSistemaAplicadas.push(opcion);
                        }
                    });

                    // Guardar opciones aplicadas
                    localStorage.setItem('opcionesDelSistema', JSON.stringify(opcionesDelSistemaAplicadas));

                    // Establecer modulos
                    menu.modulos = modulos;
                }
            });
        }

    }
})();
