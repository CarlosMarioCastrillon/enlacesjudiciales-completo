(function() {
    'use strict';

    angular
        .module('enlaces.start', ['angular-jwt'])
        .controller('startController', startController);

    startController.$inject = [
        'jwtHelper'
    ];

    function startController(jwtHelper){
        var token = localStorage.getItem('token');
        var isTokenExpired = token ? jwtHelper.isTokenExpired(token) : true;
        if(!isTokenExpired){
            window.location = '/app';
        } else {
            window.location = '/login';
        }
    }

})();