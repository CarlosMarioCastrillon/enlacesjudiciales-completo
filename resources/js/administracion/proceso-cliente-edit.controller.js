(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('procesoClienteEditController', procesoClienteEditController);

    procesoClienteEditController.$inject = [
        '$uibModalInstance',
        '$timeout',
        'genericService',
        'messageUtil',
        'Constantes',
        'parametros'
    ];

    function procesoClienteEditController(
        $uibModalInstance, $timeout, genericService, messageUtil, Constantes, parametros
    ){
        var vm = this,
            recurso = "procesos-clientes";

        vm.procesoId = null;
        vm.empresa = parametros.empresa;
        vm.cliente = parametros.cliente;
        // Acciones
        vm.guardar = guardar;

        vm.cancelar = cancelar;
        // inicial autocomples
        initAutocompleteProceso();

        if(parametros.procesoCliente && parametros.procesoCliente.id){
            cargar();
        }else{
            vm.numero_proceso = null;
            vm.estado = true;
        }

        vm.cliente = parametros.cliente;

        function cargar() {
            genericService.cargar(recurso, parametros.procesoCliente.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK) {
                        var procesoCliente = response.data,
                            procesoJudicial = procesoCliente.procesoJudicial;
                        vm.estado = procesoCliente.estado;
                        vm.fechaCreacion = procesoCliente.fecha_creacion;
                        vm.fechaModificacion = procesoCliente.fecha_modificacion;
                        vm.usuarioCreacionNombre = procesoCliente.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = procesoCliente.usuario_modificacion_nombre;
                        vm.conceptoId = procesoCliente.concepto_id;
                        vm.numero_proceso = procesoCliente.numero_proceso;

                        if (procesoJudicial) {
                            vm.procesoId = procesoJudicial.id;
                            vm.opcionesProcesos.push({id: procesoJudicial.id, nombre: procesoJudicial.nombre});
                        }

                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;
                var datos = {
                    id: parametros.procesoCliente ? +parametros.procesoCliente.id : null,
                    usuario_id: vm.cliente.id,
                    proceso_id: vm.procesoId,
                    estado: vm.estado
                };

                var promesa = null;
                if(!(parametros.procesoCliente && parametros.procesoCliente.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        $uibModalInstance.close(response.data.datos);
                        messageUtil.success(response.data.mensajes[0]);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }
                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }

        function initAutocompleteProceso() {
            vm.configProceso = {
                valueField: 'id',
                labelField: 'numero_proceso',
                searchField: ['numero_proceso'],
                sortField: 'numero_proceso',
                allowEmptyOption: false,
                create: false,
                persist: true,
                load: function(query, callback) {
                    if (!query.length) return callback();
                    genericService.obtenerColeccionLigera("procesos-judiciales", {
                        q: query,
                        ligera: true,
                        limite: 100
                    }).then(function(response){
                        callback(response.data);
                        // Validar so hay datos
                        if(!response.data || response.data.length === 0){
                            vm.message = "No se encuentra en la lista";
                        }
                    }).catch(function(){
                        callback();
                    });
                },
                onItemAdd: function (value, $item) {
                    if(vm.procesoId !== value){
                        $timeout(function (){
                            genericService.cargar("procesos-judiciales", vm.procesoId)
                                .then(function (response) {
                                    vm.proceso = response.data;
                                    vm.numero_proceso = vm.proceso.numero_proceso;
                                    vm.message = null;
                                });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.opcionesProcesos = [];
                    vm.procesoId = null;
                    $timeout(function () {
                        vm.limpiarOpcionesProcesos = true;
                    }, 100);
                }
            };
            genericService.obtenerColeccionLigera("procesos-judiciales",{
                ligera: true,
                limite: 100
            })
            .then(function (response){
                $timeout(function () {
                    vm.opcionesProcesos = [].concat(response.data);
                }, 100);
            });
        }
    }
})();
