(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('diligenciaClienteEditController', diligenciaClienteEditController);

    diligenciaClienteEditController.$inject = [
        '$translate',
        '$timeout',
        '$routeParams',
        'genericService',
        'sessionUtil',
        'messageUtil',
        'Upload',
        'langUtil',
        'Constantes'
    ];

    function diligenciaClienteEditController(
        $translate, $timeout, $routeParams, genericService, sessionUtil, messageUtil, Upload, langUtil, Constantes
    ){
        var vm = this,
            i18n = {},
            recurso = "diligencias";

        vm.entorno = sessionUtil.getParametros();
        vm.empresa = sessionUtil.getEmpresa();
        vm.EstadoDiligenciaEnum = Constantes.EstadoDiligencia;

        // Acciones
        vm.guardar = guardar;
        vm.volver = volver;

        // Acciones archivos
        vm.cargarArchivo = cargarArchivo;
        vm.eliminarArchivo = eliminarArchivo;
        vm.descargarArchivo = descargarArchivo;

        // Cargar traducciones
        cargarTraducciones();

        // Instanciar componentes jquery
        instanciarEventosFormatoMoneda();

        // Instanciar autocomples
        initAutocompleteDepartamento();
        initAutocompleteCiudad();
        initAutocompleteEmpresa();
        initAutocompleteTipoDiligencia();
        initAutocompleteProceso();
        initAutocompleteEstadoDiligencia();
        initAutocompleteUsuario();

        vm.deshabilitarFecha = true;
        vm.fecha = moment().format('YYYY-MM-DD');
        vm.indicativoCobro = true;
        vm.indicativoAutorizacion = false;
        vm.deshabilitarEstadoDiligencia = true;


        // Acciones

        function guardar() {
            // Validaciones
            var conErrores = false,
                valorDiligencia = vm.valorDiligencia && vm.valorDiligencia.trim() !== "" ?
                    Number(vm.valorDiligencia.replace(/[^0-9.-]+/g,"")) : null,
                gastoEnvio = vm.gastoEnvio && vm.gastoEnvio.trim() !== "" ?
                    Number(vm.gastoEnvio.replace(/[^0-9.-]+/g,"")) : null,
                otrosGastos = vm.otrosGastos && vm.otrosGastos.trim() !== "" ?
                    Number(vm.otrosGastos.replace(/[^0-9.-]+/g,"")) : null,
                costoDiligencia = vm.costoDiligencia && vm.costoDiligencia.trim() !== "" ?
                    Number(vm.costoDiligencia.replace(/[^0-9.-]+/g,"")) : null,
                costoEnvio = vm.costoEnvio && vm.costoEnvio.trim() !== "" ?
                    Number(vm.costoEnvio.replace(/[^0-9.-]+/g,"")) : null,
                otrosCostos = vm.otrosCostos && vm.otrosCostos.trim() !== "" ?
                    Number(vm.otrosCostos.replace(/[^0-9.-]+/g,"")) : null;

            vm.guardando = true;
            if(!conErrores && vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;
                var datos = {
                    empresa_id: vm.empresaId,
                    proceso_id: vm.procesoId,
                    fecha_diligencia: vm.fecha,
                    tipo_diligencia_id: vm.tipoDiligenciaId,
                    departamento_id: vm.departamentoId,
                    ciudad_id: vm.ciudadId,
                    dependiente_id: vm.usuarioId,
                    valor_diligencia: valorDiligencia,
                    gastos_envio: gastoEnvio,
                    otros_gastos: otrosGastos,
                    costo_diligencia: costoDiligencia,
                    costo_envio: costoEnvio,
                    otros_costos: otrosCostos,
                    estado_diligencia: vm.estadoDiligencia,
                    indicativo_cobro: vm.indicativoCobro,
                    solicitud_autorizacion: vm.indicativoAutorizacion,
                    observaciones_cliente: vm.observacionesCliente,
                    base_64_anexo_diligencia: vm.archivoBase64,
                    nombre_anexo_diligencia: vm.archivoNombre,
                };

                var promesa;
                    promesa = genericService.crear(recurso, datos);


                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        messageUtil.success(response.data.mensajes[0]);
                        volver();
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }
                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function volver() {
            window.location = "#/diligencias/cliente";
        }

        // Acciones archivos

        function cargarArchivo($archivo) {
            Upload.dataUrl($archivo, true).then(function(base64) {
                if(base64){
                    vm.archivoEliminado = false;
                    vm.archivo = $archivo;
                    vm.archivoNombre = $archivo.name;
                    vm.archivoBase64 = base64;
                }
            });
        }

        function descargarArchivo() {
                var a = document.createElement("a");
                document.body.appendChild(a);
                a.style = "display: none";
                var blob = Upload.dataUrltoBlob(vm.archivoBase64, vm.archivoNombre),
                    url = window.URL.createObjectURL(blob);
                a.href = url;
                a.download = vm.archivoNombre;
                a.click();
                window.URL.revokeObjectURL(url);
        }

        function eliminarArchivo() {
            vm.archivo = null;
            vm.archivoNombre = null;
            vm.archivoBase64 = null;
        }

        // Instanciar autocompletes
        function initAutocompleteEmpresa() {
            vm.configEmpresa = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                onItemAdd: function (value, $item) {
                    if(!vm.cargandoProcesos){
                        vm.cargandoProcesos = true;
                        vm.procesoId = null;
                        vm.opcionesProcesos = [];
                        $timeout(function () {
                            vm.limpiarOpcionesProcesos = true;
                            genericService.obtenerColeccionLigera("procesos-judiciales", {
                                ligera: true,
                                empresa_cliente: value,
                                limite: 100
                            })
                                .then(function (response){
                                    var procesos = response.data;
                                    if(vm.diligenciaOriginal && vm.diligenciaOriginal.proceso_judicial) {
                                        vm.procesoId = vm.diligenciaOriginal.proceso_judicial.id;
                                        procesos.push({
                                            id: vm.diligenciaOriginal.proceso_judicial.id,
                                            numero_proceso: vm.diligenciaOriginal.proceso_judicial.numero_proceso
                                        });
                                    }
                                    vm.opcionesProcesos = [].concat(procesos);
                                }).finally(function () {
                                vm.cargandoProcesos = false;
                            });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.opcionesProcesos = [];
                    vm.procesoId = null;
                    $timeout(function () {
                        vm.limpiarOpcionesProcesos = true;
                    }, 100);
                }
            };

            vm.opcionesEmpresas = [];
            genericService.obtenerColeccionLigera("empresas",{
                ligera: true
            })
                .then(function (response){
                    vm.opcionesEmpresas = [].concat(response.data);
                });
            vm.empresaId = vm.empresa.id;
            vm.deshabilitarEmpresaId = true;
        }

        function initAutocompleteProceso() {
            vm.configProceso = {
                valueField: 'id',
                labelField: 'numero_proceso',
                searchField: ['numero_proceso'],
                sortField: 'numero_proceso',
                allowEmptyOption: false,
                create: false,
                persist: true,
                lock: false,
                load: function(query, callback) {
                    if (!query.length) return callback();
                    genericService.obtenerColeccionLigera("procesos-judiciales", {
                        q: query,
                        ligera: true,
                        limite: 100
                    }).then(function(response){
                        callback(response.data);
                        // Validar so hay datos
                        if(!response.data || response.data.length === 0){
                            vm.message = "No se encuentra en la lista";
                        }
                    }).catch(function(){
                        callback();
                    });
                },
                onItemAdd: function (value, $item) {
                    if(vm.procesoId !== value && !vm.cargandoInfo) {
                        vm.cargandoInfo = true;
                        var opcionesDepartamento = vm.opcionesDepartamentos;
                        vm.departamentoId = null;
                        vm.opcionesDepartamentos = [];

                        $timeout(function (){
                            vm.limpiarOpcionesDepartamento = true;
                            genericService.cargar("procesos-judiciales", vm.procesoId)
                                .then(function (response) {
                                    vm.proceso = response.data;
                                    var departamento = response.data.departamento,
                                        ciudad = response.data.ciudad;
                                    vm.opcionesDepartamentos = opcionesDepartamento || [];
                                    vm.opcionesDepartamentos.push({id: departamento.id, nombre: departamento.nombre});

                                    vm.ciudadIdActual = ciudad.id;
                                    vm.departamentoId = departamento.id;

                                }).finally(function () {
                                    vm.cargandoInfo = false;
                                });
                        }, 100);
                    }
                },
                onItemRemove: function () {}
            };
            vm.opcionesProcesos = [];
        }

        function initAutocompleteTipoDiligencia() {
            vm.configTipoDiligencia = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                onItemAdd: function (value, $item) {
                    if(vm.departamentoId && vm.ciudadId && !vm.cargandoTarifas){
                        vm.cargandoTarifas = true;
                        genericService.obtener('tarifas-diligencias',{
                            tipo_diligencia_id: value,
                            departamento_id: vm.departamentoId,
                            ciudad_id: vm.ciudadId,
                            obtener: true
                        })
                        .then(function(response){
                            if(response.status === Constantes.Response.HTTP_OK) {
                                var datos = response.data;
                                if(datos && datos.length > 0 && datos[0].valor_diligencia){
                                    vm.valorDiligencia = langUtil.formatCurrency(datos[0].valor_diligencia);
                                }
                                if(datos && datos.length > 0 && datos[0].gasto_envio){
                                    vm.gastoEnvio = langUtil.formatCurrency(datos[0].gasto_envio);
                                }
                                if(datos && datos.length > 0 && datos[0].otros_gastos){
                                    vm.otrosGastos = langUtil.formatCurrency(datos[0].otros_gastos);
                                }
                            }
                        }).finally(function () {
                            vm.cargandoTarifas = false;
                        });
                    }

                    if(!vm.cargandoTipoDiligencia){
                        vm.cargandoTipoDiligencia = true;
                        genericService.cargar('tipos-diligencias', value)
                            .then(function(response){
                                if(response.status === Constantes.Response.HTTP_OK) {
                                    vm.indicativoAutorizacion = response.data.solicitud_autorizacion;
                                }
                            }).finally(function () {
                                vm.cargandoTipoDiligencia = false;
                            });
                    }
                }
            };

            vm.opcionesTipoDiligencia = [];
            genericService.obtenerColeccionLigera("tipos-diligencias",{
                ligera: true,
            })
            .then(function (response){
                vm.opcionesTipoDiligencia = [].concat(response.data);
            });
        }

        function initAutocompleteDepartamento() {
            vm.configDepartamento = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                onItemAdd: function (value, $item) {
                    if(!vm.cargandoCiudades){
                        vm.cargandoCiudades = true;
                        vm.ciudadId = null;
                        vm.opcionesCiudad = [];
                        $timeout(function () {
                            vm.limpiarOpcionesCiudad = true;
                            genericService.obtenerColeccionLigera("ciudades", {
                                ligera: true,
                                departamento_id: value
                            })
                            .then(function (response){
                                vm.opcionesCiudad = [].concat(response.data);
                                if(vm.ciudadIdActual){
                                    vm.ciudadId = vm.ciudadIdActual;
                                    vm.ciudadIdActual = null;
                                }
                            }).finally(function () {
                                vm.cargandoCiudades = false;
                            });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.opcionesCiudad = [];
                    vm.ciudadId = null;
                    $timeout(function () {
                        vm.limpiarOpcionesCiudad = true;
                    }, 100);
                }
            };
            vm.opcionesDepartamentos = [];
            genericService.obtenerColeccionLigera("departamentos",{
                ligera: true,
            })
            .then(function (response){
                vm.opcionesDepartamentos = [].concat(response.data);
            });
        }

        function initAutocompleteCiudad() {
            vm.configCiudad = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                lock: false,
                onItemAdd: function (value, $item) {
                    if(!vm.cargandoUsuarios){
                        vm.cargandoUsuarios = true;
                        vm.usuarioId = null;
                        vm.opcionesUsuario = [];
                        $timeout(function () {
                            vm.limpiarOpcionesUsuario = true;
                            genericService.obtenerColeccionLigera("usuarios",{
                                ligera: true,
                                es_dependiente: true,
                                ciudad_id: value
                            })
                            .then(function (response){
                                var dependientes = response.data;
                                if(vm.diligenciaOriginal && vm.diligenciaOriginal.dependiente){
                                    vm.usuarioId = vm.diligenciaOriginal.dependiente.id;
                                    dependientes.push({
                                        id: vm.diligenciaOriginal.dependiente.id,
                                        nombre: vm.diligenciaOriginal.dependiente.nombre
                                    });
                                }
                                vm.opcionesUsuario = [].concat(dependientes);
                            }).finally(function () {
                                vm.cargandoUsuarios = false;
                            });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.opcionesUsuario = [];
                    vm.usuarioId = null;
                    $timeout(function () {
                        vm.limpiarOpcionesUsuario = true;
                    }, 100);
                }
            };
            vm.opcionesCiudad = [];
        }

        function initAutocompleteEstadoDiligencia() {
            vm.configEstadoDiligencia = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'id',
                allowEmptyOption: false,
                create: false,
                persist: true,
                lock: false
            };
                vm.opcionesEstadoDiligencia = [
                    {id: Constantes.EstadoDiligencia.SOLICITADO, nombre: "Solicitado"},
                ];
            vm.estadoDiligencia = Constantes.EstadoDiligencia.SOLICITADO;
        }

        function initAutocompleteUsuario() {
            vm.configUsuario = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
            };
            vm.opcionesUsuario = [];
            genericService.obtenerColeccionLigera("usuarios",{
                ligera: true,
                es_dependiente: true,
                ciudad_id: vm.ciudadId ? vm.ciudadId : null
            })
            .then(function (response){
                vm.opcionesUsuario = [].concat(response.data);
            }).finally(function () {
                vm.cargandoUsuarios = false;
            });
        }

        // Instanciar componentes jquery

        function instanciarEventosFormatoMoneda() {
            $("input[data-type='currency']").on({
                keyup: function() {
                    langUtil.jqueryFormatCurrency($(this));
                },
                blur: function() {
                    langUtil.jqueryFormatCurrency($(this), "blur");
                }
            });
        }

        // Traducciones

        function cargarTraducciones() {
            $translate([
                "SOLICITADO", "COTIZADO", "APROBADO", "TRAMITE", "TERMINADO", "CONTABILIZADO", "FACTURADO"
            ]).then(function (translations) {
                i18n.SOLICITADO = translations.SOLICITADO;
                i18n.COTIZADO = translations.COTIZADO;
                i18n.APROBADO = translations.APROBADO;
                i18n.TRAMITE = translations.TRAMITE;
                i18n.TERMINADO = translations.TERMINADO;
                i18n.CONTABILIZADO = translations.CONTABILIZADO;
                i18n.FACTURADO = translations.FACTURADO;
            }, function (translationIds) {});
        }

    }
})();
