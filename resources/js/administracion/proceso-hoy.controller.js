(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('procesoHoyController', procesoHoyController);

    procesoHoyController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function procesoHoyController(
        $scope, $routeParams, $timeout, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            today = new Date(),
            date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();

        vm.filtroFechaInicial = date;
        vm.filtroFechaFinal = date;

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.filtroEstado = null;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.obtenerExcel = obtenerExcel;
        vm.obtenerArchivoAnexo = obtenerArchivoAnexo;
        vm.obtenerArchivoEstadoCartelera = obtenerArchivoEstadoCartelera;

        // Limpiar filtros
        limpiarFiltros();

        // Grid
        function obtenerResumen(tableState, ctrl){
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }
            if(pipePromise) $timeout.cancel(pipePromise);
            pipePromise = $timeout(function() {
                if(!vm.cargando){
                    vm.cargando = true;
                    genericService.obtenerColeccion("actuaciones/reporte-por-empresa", tableState)
                        .then(function (response){
                            if(response.status === Constantes.Response.HTTP_OK){
                                var coleccion = response.data.datos;
                                if(coleccion && coleccion.length > 0 ){
                                    vm.coleccion = [].concat(coleccion);
                                    vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                                    vm.paginacion.paginaActual = response.data.pagina_actual;
                                    vm.paginacion.desde = response.data.desde;
                                    vm.paginacion.hasta = response.data.hasta;
                                    vm.paginacion.total = response.data.total;
                                    tableState.pagination.numberOfPages = response.data.ultima_pagina;
                                }else{
                                    vm.coleccion = [];
                                }
                            }
                        }).finally(function () {
                            vm.cargando = false;
                        });
                }
            }, 500);
        }

        function limpiarFiltros(){
            // vm.filtroFechaInicial = null;
            // vm.filtroFechaFinal = null;
            $scope.isReset = true;
        }

        /*function cargarFiltros() {
            var filtros = gridUtil.getFormattedSearchPredicate("procesoHoy");
            vm.nombre = filtros && filtros.nombre ? filtros.nombre : null;
        }*/

        // Acciones

        function obtenerExcel() {
            if(vm.coleccion.length > 0) {
                genericService.obtenerArchivo("actuaciones/procesos-hoy/excel", {
                    fecha_inicial: vm.filtroFechaInicial,
                    fecha_final: vm.filtroFechaFinal
                });
            }
        }

        function obtenerArchivoAnexo(row) {
            genericService.obtenerArchivo("actuaciones/" + row.id + "/archivo-anexo")
                .then(function (response) {
                    if(response.status === Constantes.Response.HTTP_NOT_FOUND){
                        messageUtil.error("No se encontro un archivo anexo.");
                    }else if(response.status === Constantes.Response.HTTP_FORBIDDEN){
                        messageUtil.error("No tiene permitido descargar este archivo.");
                    }
                });
        }

        function obtenerArchivoEstadoCartelera(row) {
            genericService.obtenerArchivo("actuaciones/" + row.id + "/archivo-estado-cartelera")
                .then(function (response) {
                    if(response.status === Constantes.Response.HTTP_NOT_FOUND){
                        messageUtil.error("No se encontro un archivo de estado cartelera.");
                    }else if(response.status === Constantes.Response.HTTP_FORBIDDEN){
                        messageUtil.error("No tiene permitido descargar este archivo.");
                    }
                });
        }

    }
})();
