(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('diligenciaEditController', diligenciaEditController);

    diligenciaEditController.$inject = [
        '$translate',
        '$timeout',
        '$routeParams',
        'genericService',
        'sessionUtil',
        'messageUtil',
        'Upload',
        'langUtil',
        'Constantes'
    ];

    function diligenciaEditController(
        $translate, $timeout, $routeParams, genericService, sessionUtil, messageUtil, Upload, langUtil, Constantes
    ){
        var vm = this,
            i18n = {},
            recurso = "diligencias",
            diligenciaId = $routeParams.id && $routeParams.id !== "null" ? $routeParams.id : null;

        vm.entorno = sessionUtil.getParametros();
        vm.id = diligenciaId;
        vm.EstadoDiligenciaEnum = Constantes.EstadoDiligencia;
        vm.modoAcceso = $routeParams.modo;
        vm.diligenciaTramite = ($routeParams.dependiente && +$routeParams.dependiente === 1);
        vm.ModoAccesoEnum = {
            CREACION: 1,
            MODIFICACION: 2,
            CAMBIO_ESTADO: 3
        };
        if(!(vm.entorno && vm.entorno.INDICATIVO_COBRO_HABILITADO)){
            vm.deshabilitarIndicativoCobro = true;
        }

        // Acciones
        vm.guardar = guardar;
        vm.volver = volver;

        // Acciones archivos
        vm.cargarArchivo = cargarArchivo;
        vm.cargarAnexoCostos = cargarAnexoCostos;
        vm.cargarAnexoRecibido = cargarAnexoRecibido;
        vm.eliminarArchivo = eliminarArchivo;
        vm.eliminarAnexoCostos = eliminarAnexoCostos;
        vm.eliminarAnexoRecibido = eliminarAnexoRecibido;
        vm.restaurarArchivo = restaurarArchivo;
        vm.restaurarAnexoCostos = restaurarAnexoCostos;
        vm.restaurarAnexoRecibido = restaurarAnexoRecibido;
        vm.descargarArchivo = descargarArchivo;
        vm.descargarAnexoCostos = descargarAnexoCostos;
        vm.descargarAnexoRecibido = descargarAnexoRecibido;

        // Cargar traducciones
        cargarTraducciones();

        // Instanciar componentes jquery
        instanciarEventosFormatoMoneda();

        // Instanciar autocomples
        initAutocompleteDepartamento();
        initAutocompleteCiudad();
        initAutocompleteTipoDiligencia();
        initAutocompleteEmpresa();
        initAutocompleteProceso();
        initAutocompleteEstadoDiligencia();
        if(!vm.diligenciaTramite) {
            initAutocompleteUsuario();
        }

        if(diligenciaId){
            cargar();
        }else{
            vm.indicativoCobro = true;
            vm.indicativoAutorizacion = false;
            vm.deshabilitarFecha = false;
            vm.fecha = moment().format('YYYY-MM-DD');
        }

        // Acciones

        function cargar() {
            genericService.cargar(recurso, diligenciaId)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var diligencia = response.data,
                            ciudad = diligencia.ciudad,
                            departamento = diligencia.departamento,
                            tipoDiligencia = diligencia.tipo_diligencia,
                            empresa = diligencia.empresa,
                            proceso = diligencia.proceso_judicial,
                            dependiente = diligencia.dependiente;

                        vm.diligenciaOriginal = diligencia;
                        vm.numeroDiligencia = diligenciaId;
                        vm.fecha = diligencia.fecha_diligencia;
                        vm.observacionesCliente = diligencia.observaciones_cliente;
                        vm.observacionesInternas = diligencia.observaciones_internas;
                        vm.estadoDiligencia = diligencia.estado_diligencia;
                        vm.fechaCreacion = diligencia.fecha_creacion;
                        vm.archivoNombre = diligencia.archivo_anexo_diligencia;
                        vm.anexoCostosNombre = diligencia.archivo_anexo_costos;
                        vm.anexoRecibidoNombre = diligencia.archivo_anexo_recibido;
                        vm.fechaModificacion = diligencia.fecha_modificacion;
                        vm.usuarioCreacionNombre = diligencia.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = diligencia.usuario_modificacion_nombre;
                        vm.indicativoCobro = diligencia.indicativo_cobro;
                        vm.indicativoAutorizacion = diligencia.solicitud_autorizacion;
                        vm.detalleValores = diligencia.detalle_valores;

                        // Formatear valores
                        if(diligencia.valor_diligencia){
                            vm.valorDiligencia = langUtil.formatCurrency(diligencia.valor_diligencia);
                        }
                        if(diligencia.gastos_envio){
                            vm.gastoEnvio = langUtil.formatCurrency(diligencia.gastos_envio);
                        }
                        if(diligencia.otros_gastos){
                            vm.otrosGastos = langUtil.formatCurrency(diligencia.otros_gastos);
                        }
                        if(diligencia.costo_diligencia){
                            vm.costoDiligencia = langUtil.formatCurrency(diligencia.costo_diligencia);
                        }
                        if(diligencia.costo_envio){
                            vm.costoEnvio = langUtil.formatCurrency(diligencia.costo_envio);
                        }
                        if(diligencia.otros_costos){
                            vm.otrosCostos = langUtil.formatCurrency(diligencia.otros_costos);
                        }

                        // Cargar achivo
                        vm.archivoNombre = diligencia.archivo_anexo_diligencia;

                        // Autocompletes
                        if(empresa){
                            vm.empresaId = empresa.id;
                            vm.opcionesEmpresas.push({id: empresa.id, nombre: empresa.nombre});
                        }
                        if(proceso) {
                            vm.procesoId = proceso.id;
                            vm.opcionesProcesos.push({id: proceso.id, numero_proceso: proceso.numero_proceso});
                        }
                        if(departamento){
                            vm.departamentoId = departamento.id;
                            vm.opcionesDepartamentos.push({id: departamento.id, nombre: departamento.nombre});
                        }
                        if(ciudad){
                            vm.ciudadId = ciudad.id;
                            vm.opcionesCiudad.push({id: ciudad.id, nombre: ciudad.nombre});
                        }
                        if(tipoDiligencia) {
                            vm.tipoDiligenciaId = tipoDiligencia.id;
                            vm.opcionesTipoDiligencia.push({id: tipoDiligencia.id, nombre: tipoDiligencia.nombre});
                        }
                        if(dependiente){
                            vm.usuarioId = dependiente.id;
                            vm.dependienteId = dependiente.id;
                            if (!vm.diligenciaTramite) {
                                vm.opcionesUsuario.push({id: dependiente.id, nombre: dependiente.nombre});
                            }
                        }
                        if (vm.diligenciaTramite){
                            vm.deshabilitarEmpresaId = true;
                            vm.deshabilitarProcesoId = true;
                            vm.deshabilitarDepartamentoId = true;
                            vm.deshabilitarCiudadId = true;
                            vm.deshabilitarTipoDiligenciaId = true;
                            vm.deshabilitarNumeroDiligencia = true;
                            vm.deshabilitarAnexoDiligencia = false;
                            vm.deshabilitarDetalleValores = false;
                            vm.deshabilitarObservacionesCliente = true;
                        }else if(vm.modoAcceso == vm.ModoAccesoEnum.CAMBIO_ESTADO) {
                            switch (diligencia.estado_diligencia){
                                case Constantes.EstadoDiligencia.SOLICITADO:
                                    vm.deshabilitarEmpresaId = true;
                                    vm.deshabilitarProcesoId = true;
                                    vm.deshabilitarDepartamentoId = true;
                                    vm.deshabilitarCiudadId = true;
                                    vm.deshabilitarEstadoDiligencia = true;
                                    vm.deshabilitarIndicativoCobro = true;
                                    vm.deshabilitarIndicativoAutorizacion = true;
                                    vm.deshabilitarFecha = true;
                                    vm.deshabilitarTipoDiligenciaId = true;
                                    vm.deshabilitarDetalleValores = true;
                                    vm.deshabilitarUsuarioId = true;

                                    vm.estadoDiligencia = Constantes.EstadoDiligencia.COTIZADO;
                                    break;
                                case Constantes.EstadoDiligencia.COTIZADO:
                                    vm.deshabilitarEmpresaId = true;
                                    vm.deshabilitarProcesoId = true;
                                    vm.deshabilitarDepartamentoId = true;
                                    vm.deshabilitarCiudadId = true;
                                    vm.deshabilitarEstadoDiligencia = true;
                                    vm.deshabilitarIndicativoCobro = true;
                                    vm.deshabilitarIndicativoAutorizacion = true;
                                    vm.deshabilitarFecha = true;
                                    vm.deshabilitarTipoDiligenciaId = true;
                                    vm.deshabilitarValorDiligencia = true;
                                    vm.deshabilitarGastoEnvio = true;
                                    vm.deshabilitarOtrosGastos = true;
                                    vm.deshabilitarDetalleValores = true;
                                    vm.deshabilitarUsuarioId = true;

                                    vm.estadoDiligencia = Constantes.EstadoDiligencia.APROBADO;
                                    break;
                                case Constantes.EstadoDiligencia.APROBADO:
                                    vm.deshabilitarEmpresaId = true;
                                    vm.deshabilitarProcesoId = true;
                                    vm.deshabilitarDepartamentoId = true;
                                    vm.deshabilitarCiudadId = true;
                                    vm.deshabilitarEstadoDiligencia = true;
                                    vm.deshabilitarIndicativoCobro = true;
                                    vm.deshabilitarIndicativoAutorizacion = true;
                                    vm.deshabilitarFecha = true;
                                    vm.deshabilitarTipoDiligenciaId = true;
                                    vm.deshabilitarDetalleValores = true;
                                    vm.deshabilitarAnexoDiligencia = true;

                                    vm.estadoDiligencia = Constantes.EstadoDiligencia.TRAMITE;
                                    break;
                                case Constantes.EstadoDiligencia.TRAMITE:
                                    vm.deshabilitarEmpresaId = true;
                                    vm.deshabilitarProcesoId = true;
                                    vm.deshabilitarDepartamentoId = true;
                                    vm.deshabilitarCiudadId = true;
                                    vm.deshabilitarEstadoDiligencia = true;
                                    vm.deshabilitarIndicativoCobro = true;
                                    vm.deshabilitarIndicativoAutorizacion = true;
                                    vm.deshabilitarFecha = true;
                                    vm.deshabilitarTipoDiligenciaId = true;
                                    vm.deshabilitarAnexoDiligencia = true;
                                    vm.deshabilitarUsuarioId = true;
                                    vm.mostrarAnexos = true;

                                    vm.estadoDiligencia = Constantes.EstadoDiligencia.TERMINADO;
                                    break;
                                case Constantes.EstadoDiligencia.TERMINADO:
                                    vm.deshabilitarEmpresaId = true;
                                    vm.deshabilitarProcesoId = true;
                                    vm.deshabilitarDepartamentoId = true;
                                    vm.deshabilitarCiudadId = true;
                                    vm.deshabilitarEstadoDiligencia = true;
                                    vm.deshabilitarIndicativoCobro = true;
                                    vm.deshabilitarIndicativoAutorizacion = true;
                                    vm.deshabilitarFecha = true;
                                    vm.deshabilitarTipoDiligenciaId = true;
                                    vm.deshabilitarAnexoDiligencia = true;
                                    vm.deshabilitarUsuarioId = true;
                                    vm.mostrarAnexos = true;

                                    vm.estadoDiligencia = Constantes.EstadoDiligencia.CONTABILIZADO;
                                    break;
                                case Constantes.EstadoDiligencia.CONTABILIZADO:
                                    vm.deshabilitarEmpresaId = true;
                                    vm.deshabilitarProcesoId = true;
                                    vm.deshabilitarDepartamentoId = true;
                                    vm.deshabilitarCiudadId = true;
                                    vm.deshabilitarEstadoDiligencia = true;
                                    vm.deshabilitarIndicativoCobro = true;
                                    vm.deshabilitarIndicativoAutorizacion = true;
                                    vm.deshabilitarFecha = true;
                                    vm.deshabilitarTipoDiligenciaId = true;
                                    vm.deshabilitarAnexoDiligencia = true;
                                    vm.deshabilitarUsuarioId = true;
                                    vm.mostrarAnexos = true;

                                    vm.estadoDiligencia = Constantes.EstadoDiligencia.FACTURADO;
                                    break;
                                default:
                                    vm.deshabilitarEmpresaId = true;
                                    vm.deshabilitarProcesoId = true;
                                    vm.deshabilitarDepartamentoId = true;
                                    vm.deshabilitarCiudadId = true;
                                    vm.deshabilitarEstadoDiligencia = true;
                                    vm.deshabilitarIndicativoCobro = true;
                                    vm.deshabilitarIndicativoAutorizacion = true;
                                    vm.deshabilitarFecha = true;
                                    vm.deshabilitarTipoDiligenciaId = true;
                                    vm.deshabilitarValorDiligencia = true;
                                    vm.deshabilitarGastoEnvio = true;
                                    vm.deshabilitarOtrosGastos = true;
                                    vm.deshabilitarDetalleValores = true;
                                    vm.deshabilitarObservacionesCliente = true;
                                    vm.deshabilitarObservacionesInternas = true;
                                    vm.deshabilitarAnexoDiligencia = true;
                                    break;
                            }
                        }
                    }
                });
        }

        function guardar() {
            // Validaciones
            var conErrores = false,
                valorDiligencia = vm.valorDiligencia && vm.valorDiligencia.trim() !== "" ?
                    Number(vm.valorDiligencia.replace(/[^0-9.-]+/g,"")) : null,
                gastoEnvio = vm.gastoEnvio && vm.gastoEnvio.trim() !== "" ?
                    Number(vm.gastoEnvio.replace(/[^0-9.-]+/g,"")) : null,
                otrosGastos = vm.otrosGastos && vm.otrosGastos.trim() !== "" ?
                    Number(vm.otrosGastos.replace(/[^0-9.-]+/g,"")) : null,
                costoDiligencia = vm.costoDiligencia && vm.costoDiligencia.trim() !== "" ?
                    Number(vm.costoDiligencia.replace(/[^0-9.-]+/g,"")) : null,
                costoEnvio = vm.costoEnvio && vm.costoEnvio.trim() !== "" ?
                    Number(vm.costoEnvio.replace(/[^0-9.-]+/g,"")) : null,
                otrosCostos = vm.otrosCostos && vm.otrosCostos.trim() !== "" ?
                    Number(vm.otrosCostos.replace(/[^0-9.-]+/g,"")) : null;
            if(
                (!valorDiligencia || valorDiligencia === 0) && vm.estadoDiligencia &&
                vm.estadoDiligencia != Constantes.EstadoDiligencia.SOLICITADO
            ){
                conErrores = true;
                vm.valorDiligenicaInvalid = true;
            }

            vm.guardando = true;
            if(!conErrores && vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;
                console.log(vm.dependienteId);
                var datos = {
                    id: diligenciaId,
                    empresa_id: vm.empresaId,
                    proceso_id: vm.procesoId,
                    fecha_diligencia: vm.fecha,
                    tipo_diligencia_id: vm.tipoDiligenciaId,
                    departamento_id: vm.departamentoId,
                    ciudad_id: vm.ciudadId,
                    dependiente_id: vm.usuarioId ? vm.usuarioId : vm.dependienteId ? vm.dependienteId : null,
                    valor_diligencia: valorDiligencia,
                    gastos_envio: gastoEnvio,
                    otros_gastos: otrosGastos,
                    costo_diligencia: costoDiligencia,
                    costo_envio: costoEnvio,
                    otros_costos: otrosCostos,
                    estado_diligencia: vm.estadoDiligencia,
                    indicativo_cobro: vm.indicativoCobro,
                    solicitud_autorizacion: vm.indicativoAutorizacion,
                    observaciones_cliente: vm.observacionesCliente,
                    observaciones_internas: vm.observacionesInternas,
                    detalle_valores: vm.detalleValores,
                    base_64_anexo_diligencia: vm.archivoBase64,
                    base_64_anexo_costos: vm.anexoCostosBase64,
                    base_64_anexo_recibido: vm.anexoRecibidoBase64,
                    nombre_anexo_diligencia: vm.archivoNombre,
                    nombre_anexo_costos: vm.anexoCostosNombre,
                    nombre_anexo_recibido: vm.anexoRecibidoNombre,
                    eliminar_anexo_diligenica: !!vm.archivoEliminado,
                    eliminar_anexo_costos: !!vm.anexoCostosEliminado,
                    eliminar_anexo_recibido: !!vm.anexoRecibidoEliminado,
                    diligencia_tramite: vm.diligenciaTramite
                };

                var promesa;
                if(!(diligenciaId)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        messageUtil.success(response.data.mensajes[0]);
                        volver();
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }
                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function volver() {
            if(vm.diligenciaTramite){
                window.location = "#/diligencias-tramite?dependiente=1";
            }else {
                window.location = "#/diligencias";
            }
        }

        // Acciones archivos

        function cargarArchivo($archivo) {
            Upload.dataUrl($archivo, true).then(function(base64) {
                if(base64){
                    vm.archivoEliminado = false;
                    vm.archivo = $archivo;
                    vm.archivoNombre = $archivo.name;
                    vm.archivoBase64 = base64;
                }
            });
        }

        function cargarAnexoCostos($anexoCostos) {
            Upload.dataUrl($anexoCostos, true).then(function(base64) {
                if(base64){
                    vm.anexoCostosEliminado = false;
                    vm.anexoCostos = $anexoCostos;
                    vm.anexoCostosNombre = $anexoCostos.name;
                    vm.anexoCostosBase64 = base64;
                }
            });
        }

        function cargarAnexoRecibido($anexoRecibido) {
            Upload.dataUrl($anexoRecibido, true).then(function(base64) {
                if(base64){
                    vm.anexoRecibidoEliminado = false;
                    vm.anexoRecibido = $anexoRecibido;
                    vm.anexoRecibidoNombre = $anexoRecibido.name;
                    vm.anexoRecibidoBase64 = base64;
                }
            });
        }

        function descargarArchivo() {
            if(diligenciaId && !vm.archivoBase64){
                genericService.obtenerArchivo(recurso + "/" + diligenciaId + "/anexo-diligencia")
                    .then(function (response) {
                        if(response.status === Constantes.Response.HTTP_NOT_FOUND){
                            messageUtil.error("No se encontro un archivo anexo.");
                        }else if(response.status === Constantes.Response.HTTP_FORBIDDEN){
                            messageUtil.error("No tiene permitido descargar este archivo.");
                        }
                    });
            }else{
                var a = document.createElement("a");
                document.body.appendChild(a);
                a.style = "display: none";
                var blob = Upload.dataUrltoBlob(vm.archivoBase64, vm.archivoNombre),
                    url = window.URL.createObjectURL(blob);
                a.href = url;
                a.download = vm.archivoNombre;
                a.click();
                window.URL.revokeObjectURL(url);
            }
        }

        function descargarAnexoCostos() {
            genericService.obtenerArchivo(recurso + "/" + diligenciaId + "/anexo-costos")
                .then(function (response) {
                    if(response.status === Constantes.Response.HTTP_NOT_FOUND){
                        messageUtil.error("No se encontro un archivo anexo.");
                    }else if(response.status === Constantes.Response.HTTP_FORBIDDEN){
                        messageUtil.error("No tiene permitido descargar este archivo.");
                    }
                });
        }

        function descargarAnexoRecibido() {
            genericService.obtenerArchivo(recurso + "/" + diligenciaId + "/anexo-recibido")
                .then(function (response) {
                    if(response.status === Constantes.Response.HTTP_NOT_FOUND){
                        messageUtil.error("No se encontro un archivo anexo.");
                    }else if(response.status === Constantes.Response.HTTP_FORBIDDEN){
                        messageUtil.error("No tiene permitido descargar este archivo.");
                    }
                });
        }

        function eliminarArchivo() {
            if(diligenciaId && !vm.archivoBase64){
                vm.archivoEliminado = true;
            }else{
                vm.archivo = null;
                vm.archivoNombre = null;
                vm.archivoBase64 = null;
            }
        }

        function eliminarAnexoCostos() {
            if(diligenciaId && !vm.anexoCostosBase64){
                vm.anexoCostosEliminado = true;
            }else{
                vm.anexoCostos = null;
                vm.anexoCostosNombre = null;
                vm.anexoCostosBase64 = null;
            }
        }

        function eliminarAnexoRecibido() {
            if(diligenciaId && !vm.anexoRecibidoBase64){
                vm.anexoRecibidoEliminado = true;
            }else{
                vm.anexoRecibido = null;
                vm.anexoRecibidoNombre = null;
                vm.anexoRecibidoBase64 = null;
            }
        }

        function restaurarArchivo(attachment) {
            vm.archivoEliminado = false;
        }

        function restaurarAnexoCostos(attachment) {
            vm.anexoCostosEliminado = false;
        }

        function restaurarAnexoRecibido(attachment) {
            vm.anexoRecibidoEliminado = false;
        }

        // Instanciar autocompletes

        function initAutocompleteEmpresa() {
            vm.configEmpresa = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                onItemAdd: function (value, $item) {
                    if(!vm.cargandoProcesos){
                        vm.cargandoProcesos = true;
                        vm.procesoId = null;
                        vm.opcionesProcesos = [];
                        $timeout(function () {
                            vm.limpiarOpcionesProcesos = true;
                            genericService.obtenerColeccionLigera("procesos-judiciales", {
                                ligera: true,
                                empresa_cliente: value,
                                limite: 100
                            })
                            .then(function (response){
                                var procesos = response.data;
                                if(vm.diligenciaOriginal && vm.diligenciaOriginal.proceso_judicial) {
                                    vm.procesoId = vm.diligenciaOriginal.proceso_judicial.id;
                                    procesos.push({
                                        id: vm.diligenciaOriginal.proceso_judicial.id,
                                        numero_proceso: vm.diligenciaOriginal.proceso_judicial.numero_proceso
                                    });
                                }
                                vm.opcionesProcesos = [].concat(procesos);
                            }).finally(function () {
                                vm.cargandoProcesos = false;
                            });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.opcionesProcesos = [];
                    vm.procesoId = null;
                    $timeout(function () {
                        vm.limpiarOpcionesProcesos = true;
                    }, 100);
                }
            };

            vm.opcionesEmpresas = [];
            genericService.obtenerColeccionLigera("empresas",{
                ligera: true
            })
            .then(function (response){
                vm.opcionesEmpresas = [].concat(response.data);
            });
        }

        function initAutocompleteProceso() {
            vm.configProceso = {
                valueField: 'id',
                labelField: 'numero_proceso',
                searchField: ['numero_proceso'],
                sortField: 'numero_proceso',
                allowEmptyOption: false,
                create: false,
                persist: true,
                lock: false,
                load: function(query, callback) {
                    if (!query.length) return callback();
                    genericService.obtenerColeccionLigera("procesos-judiciales", {
                        q: query,
                        ligera: true,
                        limite: 100
                    }).then(function(response){
                        callback(response.data);
                        // Validar so hay datos
                        if(!response.data || response.data.length === 0){
                            vm.message = "No se encuentra en la lista";
                        }
                    }).catch(function(){
                        callback();
                    });
                },
                onItemAdd: function (value, $item) {
                    if(!vm.cargandoInfo) {
                        vm.cargandoInfo = true;
                        if (!diligenciaId) {
                            var opcionesDepartamento = vm.opcionesDepartamentos;
                            vm.departamentoId = null;
                            vm.opcionesDepartamentos = [];
                        }
                        $timeout(function (){
                            vm.limpiarOpcionesDepartamento = true;
                            genericService.cargar("procesos-judiciales", value)
                                .then(function (response) {
                                    vm.proceso = response.data;
                                    if (!diligenciaId) {
                                        var departamento = response.data.departamento,
                                            ciudad = response.data.ciudad;
                                        vm.opcionesDepartamentos = opcionesDepartamento || [];
                                        vm.opcionesDepartamentos.push({id: departamento.id, nombre: departamento.nombre});

                                        vm.ciudadIdActual = ciudad.id;
                                        vm.departamentoId = departamento.id;
                                    }
                                }).finally(function () {
                                    vm.cargandoInfo = false;
                                });
                        }, 100);
                    }
                },
                onItemRemove: function () {}
            };
            vm.opcionesProcesos = [];
        }

        function initAutocompleteTipoDiligencia() {
            vm.configTipoDiligencia = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                onItemAdd: function (value, $item) {
                    if(!diligenciaId && vm.departamentoId && vm.ciudadId && !vm.cargandoTarifas){
                        vm.cargandoTarifas = true;
                        genericService.obtener('tarifas-diligencias',{
                            tipo_diligencia_id: value,
                            departamento_id: vm.departamentoId,
                            ciudad_id: vm.ciudadId,
                            obtener: true
                        })
                        .then(function(response){
                            if(response.status === Constantes.Response.HTTP_OK) {
                                var datos = response.data;
                                if(datos && datos.length > 0 && datos[0].valor_diligencia){
                                    vm.valorDiligencia = langUtil.formatCurrency(datos[0].valor_diligencia);
                                }
                                if(datos && datos.length > 0 && datos[0].gasto_envio){
                                    vm.gastoEnvio = langUtil.formatCurrency(datos[0].gasto_envio);
                                }
                                if(datos && datos.length > 0 && datos[0].otros_gastos){
                                    vm.otrosGastos = langUtil.formatCurrency(datos[0].otros_gastos);
                                }
                            }
                        }).finally(function () {
                            vm.cargandoTarifas = false;
                        });
                    }

                    if(!diligenciaId && !vm.cargandoTipoDiligencia){
                        vm.cargandoTipoDiligencia = true;
                        genericService.cargar('tipos-diligencias', value)
                            .then(function(response){
                                if(response.status === Constantes.Response.HTTP_OK) {
                                    vm.indicativoAutorizacion = response.data.solicitud_autorizacion;
                                }
                            }).finally(function () {
                                vm.cargandoTipoDiligencia = false;
                            });
                    }
                }
            };

            vm.opcionesTipoDiligencia = [];
            genericService.obtenerColeccionLigera("tipos-diligencias",{
                ligera: true,
            })
            .then(function (response){
                vm.opcionesTipoDiligencia = [].concat(response.data);
            });
        }

        function initAutocompleteEstadoDiligencia() {
            vm.configEstadoDiligencia = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'id',
                allowEmptyOption: false,
                create: false,
                persist: true,
                lock: false
            };

            if(diligenciaId){
                vm.opcionesEstadoDiligencia = [
                    {id: Constantes.EstadoDiligencia.SOLICITADO, nombre: "Solicitado"},
                    {id: Constantes.EstadoDiligencia.COTIZADO, nombre: "Cotizado"},
                    {id: Constantes.EstadoDiligencia.APROBADO, nombre: "Aprobado"},
                    {id: Constantes.EstadoDiligencia.TRAMITE, nombre: "Trámite"},
                    {id: Constantes.EstadoDiligencia.TERMINADO, nombre: "Terminado"},
                    {id: Constantes.EstadoDiligencia.CONTABILIZADO, nombre: "Contabilizado"},
                    {id: Constantes.EstadoDiligencia.FACTURADO, nombre: "Facturado"}
                ];
            }else{
                vm.opcionesEstadoDiligencia = [
                    {id: Constantes.EstadoDiligencia.SOLICITADO, nombre: "Solicitado"},
                    {id: Constantes.EstadoDiligencia.COTIZADO, nombre: "Cotizado"},
                    {id: Constantes.EstadoDiligencia.APROBADO, nombre: "Aprobado"}
                ];
            }
        }

        function initAutocompleteDepartamento() {
            vm.configDepartamento = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                onItemAdd: function (value, $item) {
                    if(!vm.cargandoCiudades){
                        vm.cargandoCiudades = true;
                        vm.ciudadId = null;
                        vm.opcionesCiudad = [];
                        $timeout(function () {
                            vm.limpiarOpcionesCiudad = true;
                            genericService.obtenerColeccionLigera("ciudades", {
                                ligera: true,
                                departamento_id: value
                            })
                            .then(function (response){
                                vm.opcionesCiudad = [].concat(response.data);
                                if(vm.ciudadIdActual){
                                    vm.ciudadId = vm.ciudadIdActual;
                                    vm.ciudadIdActual = null;
                                }
                            }).finally(function () {
                                vm.cargandoCiudades = false;
                            });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.opcionesCiudad = [];
                    vm.ciudadId = null;
                    $timeout(function () {
                        vm.limpiarOpcionesCiudad = true;
                    }, 100);
                }
            };
            vm.opcionesDepartamentos = [];
            genericService.obtenerColeccionLigera("departamentos",{
                ligera: true,
            })
            .then(function (response){
                vm.opcionesDepartamentos = [].concat(response.data);
            });
        }

        function initAutocompleteCiudad() {
            vm.configCiudad = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                lock: false,
                onItemAdd: function (value, $item) {
                    if(
                        !(vm.id && vm.usuarioId) &&
                        (vm.estadoDiligencia == Constantes.EstadoDiligencia.TRAMITE && !vm.diligenciaTramite) &&
                        !vm.cargandoUsuarios
                    ){
                        vm.cargandoUsuarios = true;
                        vm.usuarioId = null;
                        vm.opcionesUsuario = [];
                        $timeout(function () {
                            vm.limpiarOpcionesUsuario = true;
                            genericService.obtenerColeccionLigera("usuarios",{
                                ligera: true,
                                es_dependiente: true,
                                ciudad_id: value
                            })
                            .then(function (response){
                                vm.opcionesUsuario = [].concat(response.data);
                            });
                            genericService.obtener("usuarios/dependiente-principal")
                            .then(function (response) {
                                let dependientePrincipal = response.data[0];
                                vm.usuarioId = dependientePrincipal.id;
                                vm.opcionesUsuario.push({
                                    id: dependientePrincipal.id,
                                    nombre: dependientePrincipal.nombre
                                });
                            }).finally(function () {
                                vm.cargandoUsuarios = false;
                            });
                        }, 100);
                    }
               },
                onItemRemove: function () {
                    vm.opcionesUsuario = [];
                    vm.usuarioId = null;
                    $timeout(function () {
                        vm.limpiarOpcionesUsuario = true;
                    }, 100);
                }
            };
            vm.opcionesCiudad = [];
        }

        function initAutocompleteUsuario() {
            vm.configUsuario = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
            };
            vm.opcionesUsuario = [];
        }

        // Instanciar componentes jquery

        function instanciarEventosFormatoMoneda() {
            $("input[data-type='currency']").on({
                keyup: function() {
                    langUtil.jqueryFormatCurrency($(this));
                },
                blur: function() {
                    langUtil.jqueryFormatCurrency($(this), "blur");
                }
            });
        }

        // Traducciones

        function cargarTraducciones() {
            $translate([
                "SOLICITADO", "COTIZADO", "APROBADO", "TRAMITE", "TERMINADO", "CONTABILIZADO", "FACTURADO"
            ]).then(function (translations) {
                i18n.SOLICITADO = translations.SOLICITADO;
                i18n.COTIZADO = translations.COTIZADO;
                i18n.APROBADO = translations.APROBADO;
                i18n.TRAMITE = translations.TRAMITE;
                i18n.TERMINADO = translations.TERMINADO;
                i18n.CONTABILIZADO = translations.CONTABILIZADO;
                i18n.FACTURADO = translations.FACTURADO;
            }, function (translationIds) {});
        }

    }
})();
