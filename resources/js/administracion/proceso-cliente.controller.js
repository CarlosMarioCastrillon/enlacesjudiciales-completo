(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('procesoClienteController', procesoClienteController);

    procesoClienteController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        '$uibModal',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function procesoClienteController(
        $scope, $routeParams, $timeout, $uibModal, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "procesos-clientes",
            clienteId = $routeParams.id ? $routeParams.id : null;

        vm.usuarioCliente = ($routeParams.cliente && +$routeParams.cliente === 1);
        vm.empresa = sessionUtil.getEmpresa();

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Cargar los datos del cliente

        genericService.cargar("usuarios", clienteId)
            .then(function (response) {
                if (response.status === Constantes.Response.HTTP_OK) {
                    if (response.data.empresa.id === vm.empresa.id) {
                        vm.cliente = response.data;
                    }else{
                        vm.cliente = null;
                    }
                }
            });
        function obtenerResumen(tableState, ctrl){
            tableState.search = tableState.search || {};
            tableState.search.predicateObject = tableState.search.predicateObject || {};
            tableState.search.predicateObject.cliente_id = clienteId;
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }
            if(pipePromise) $timeout.cancel(pipePromise);
            pipePromise = $timeout(function() {
                if(!vm.cargando){
                    vm.cargando = true;
                    genericService.obtenerColeccion(recurso, tableState)
                        .then(function (response){
                            if(response.status === Constantes.Response.HTTP_OK){
                                var coleccion = response.data.datos;
                                if(coleccion && coleccion.length > 0 ){
                                    vm.coleccion = [].concat(coleccion);
                                    vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                                    vm.paginacion.paginaActual = response.data.pagina_actual;
                                    vm.paginacion.desde = response.data.desde;
                                    vm.paginacion.hasta = response.data.hasta;
                                    vm.paginacion.total = response.data.total;
                                    tableState.pagination.numberOfPages = response.data.ultima_pagina;
                                }else{
                                    vm.coleccion = [];
                                }
                            }
                        }).finally(function () {
                        vm.cargando = false;
                    });
                }
            }, 500);
        }

        // Acciones
        vm.crear = crear;

        // Modulos externos
        vm.confirmarEliminar = confirmarEliminar;

        // Limpiar filtros
        limpiarFiltros();

        function limpiarFiltros(){
            $scope.isReset = true;
        }

        /*function cargarFiltros() {
            var filtros = gridUtil.getFormattedSearchPredicate("procesoCliente");
            vm.nombre = filtros && filtros.nombre ? filtros.nombre : null;
        }*/

        // Acciones

        function eliminar(row){
            genericService.eliminar(recurso, row.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        $scope.$broadcast('refreshGrid');
                        messageUtil.success(response.data.mensajes[0]);
                    }else if(
                        response.status === Constantes.Response.HTTP_CONFLICT ||
                        response.status === Constantes.Response.HTTP_BAD_REQUEST
                    ){
                        messageUtil.error(response.data.mensajes[0]);
                    }
                });
        }

        // Modulos externos

        function crear(row) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + recurso + '/create',
                controller: 'procesoClienteEditController as vm',
                size: 'lg',
                resolve: {
                    parametros: function () {
                        if (row){
                            vm.procesoCliente = row;
                        }else{
                            vm.procesoCliente = [];
                        }
                        return {
                            procesoCliente: vm.procesoCliente,
                            cliente: vm.cliente,
                            empresa: vm.empresa
                        };
                    }
                }
            });

            modalInstance.result.then(function (response) {
                if(row && row.id && response && response.id){
                    procesoCliente.proceso_id = response.proceso_id;
                    procesoCliente.numero_proceso = response.numero_proceso;
                    procesoCliente.cliente_id = clienteId;
                    procesoCliente.estado = response.estado;
                    procesoCliente.usuario_modificacion_id = response.usuario_modificacion_id;
                    procesoCliente.usuario_modificacion_nombre = response.usuario_modificacion_nombre;
                    procesoCliente.fecha_creacion = response.fecha_creacion;
                    procesoCliente.fecha_modificacion = response.fecha_modificacion;
                }else if(response && response.id){
                    $scope.$broadcast('refreshGrid');
                }
            });
        }

        function confirmarEliminar(row) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + 'confirmar-template',
                controller: 'confirmarController as vm',
                size: 'md',
                resolve: {
                    parametros: function () {
                        return {
                            mensaje: "¿Estás seguro de que quieres eliminar el proceso por cliente?"
                        };
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(response.ok){
                    eliminar(row);
                }
            });
        }

    }
})();
