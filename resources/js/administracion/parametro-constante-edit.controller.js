(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('parametroConstanteEditController', parametroConstanteEditController);

    parametroConstanteEditController.$inject = [
        '$uibModalInstance',
        '$timeout',
        'genericService',
        'messageUtil',
        'Constantes',
        'parametro_constante'
    ];

    function parametroConstanteEditController(
        $uibModalInstance, $timeout, genericService, messageUtil, Constantes, parametro_constante
    ){
        var vm = this,
            recurso = "parametros-constantes";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        if(parametro_constante && parametro_constante.id){
            cargar();
        }else{
            vm.estado = true;
        }

        function cargar() {
            genericService.cargar(recurso, parametro_constante.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var parametro_constante = response.data;

                        vm.codigo = parametro_constante.codigo_parametro;
                        vm.descripcion = parametro_constante.descripcion_parametro;
                        vm.valor = parametro_constante.valor_parametro;
                        vm.estado = parametro_constante.estado_registro;
                        vm.fechaCreacion = parametro_constante.fecha_creacion;
                        vm.fechaModificacion = parametro_constante.fecha_modificacion;
                        vm.usuarioCreacionNombre = parametro_constante.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = parametro_constante.usuario_modificacion_nombre;
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;

                var datos = {
                    id: parametro_constante ? +parametro_constante.id : null,
                    codigo_parametro: vm.codigo,
                    descripcion_parametro: vm.descripcion,
                    valor_parametro: vm.valor,
                    estado_registro: vm.estado

            };

                var promesa = null;
                if(!(parametro_constante && parametro_constante.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        messageUtil.success(response.data.mensajes[0]);
                        $uibModalInstance.close(response.data.datos);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }


                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }
    }
})();
