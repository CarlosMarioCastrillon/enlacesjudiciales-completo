(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('actuacionEditController', actuacionEditController);

    actuacionEditController.$inject = [
        '$uibModalInstance',
        '$timeout',
        'genericService',
        'langUtil',
        'messageUtil',
        'Constantes',
        'actuacion',
        'parametros'
    ];

    function actuacionEditController(
        $uibModalInstance, $timeout, genericService, langUtil, messageUtil, Constantes, actuacion, parametros
    ){
        var vm = this,
            recurso = "actuaciones";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;
        vm.guardarMasivo = guardarMasivo;
        vm.validarDemandante = validarDemandante;
        vm.validarDemandado = validarDemandado;

        init();

        function init() {
            // Inicial autocomples
            initAutocompleteDepartamento();
            initAutocompleteCiudad();
            initAutocompleteJuzgado();
            initAutocompleteDespacho();
            initAutocompleteTipoNotificacion();

            // Cargar actuación
            if(parametros && parametros.masivo){
                vm.modoEdicion = true;
                vm.modoEdicionMasiva = true;
                vm.departamentoNombre = parametros.departamento_nombre;
                vm.ciudadNombre = parametros.ciudad_nombre;
                vm.juzgadoNombre = parametros.juzgado_nombre;
                vm.despachoNumero = parametros.despacho_numero;
                vm.tipoNotificacionNombre = parametros.tipo_notificacion_nombre;
                if(parametros.ciudad_id && parametros.departamento_id){
                    vm.departamentoId = parametros.departamento_id;
                    vm.opcionesDepartamento.push({
                        id: parametros.departamento_id,
                        nombre: parametros.departamento_nombre,
                        codigo_dane: parametros.departamento_codigo_dane
                    });
                    vm.departamentoIdOriginal = parametros.departamento_id;
                    vm.opcionesCiudad.push({
                        id: parametros.ciudad_id,
                        nombre: parametros.ciudad_nombre,
                        codigo_dane: parametros.ciudad_codigo_dane
                    });
                    vm.ciudadIdOriginal = parametros.ciudad_id;
                }
            }
            if(actuacion && actuacion.id){
                vm.modoEdicion = true;
                cargar();
            }
        }

        // Acciones

        function cargar() {
            genericService.cargar(recurso, actuacion.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var actuacion = response.data;
                        vm.numeroRadicadoActual = actuacion.numero_radicado_proceso_actual;
                        vm.fechaRegistro = actuacion.fecha_registro;
                        vm.fechaActuacion = actuacion.fecha_actuacion;
                        vm.fechaInicioTermino = actuacion.fecha_inicio_termino;
                        vm.fechaVencimientoTermino = actuacion.fecha_vencimiento_termino;
                        vm.demandante = actuacion.nombres_demandantes;
                        vm.demandado = actuacion.nombres_demandados;
                        vm.anotacion = actuacion.anotacion;
                        vm.descripcionClaseProceso = actuacion.descripcion_clase_proceso;
                        vm.fechaCreacion = actuacion.created_at;
                        vm.fechaModificacion = actuacion.updated_at;
                        vm.usuarioCreacionNombre = actuacion.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = actuacion.usuario_modificacion_nombre;
                        vm.estado = actuacion.estado;
                        if(actuacion.ciudad && actuacion.ciudad.departamento){
                            vm.departamentoId = actuacion.ciudad.departamento.id;
                            vm.opcionesDepartamento.push({
                                id: actuacion.ciudad.departamento.id,
                                nombre: actuacion.ciudad.departamento.nombre,
                                codigo_dane: actuacion.ciudad.departamento.codigo_dane
                            });
                            vm.departamentoIdOriginal = actuacion.ciudad.departamento.id;

                            vm.opcionesCiudad.push({
                                id: actuacion.ciudad.id,
                                nombre: actuacion.ciudad.nombre,
                                codigo_dane: actuacion.ciudad.codigo_dane
                            });
                            vm.ciudadIdOriginal = actuacion.ciudad.id;
                        }
                        if(actuacion.juzgado){
                            vm.opcionesJuzgado.push({
                                id: actuacion.juzgado.id,
                                nombre: actuacion.juzgado.nombre,
                                juzgado: actuacion.juzgado.juzgado,
                                sala: actuacion.juzgado.sala
                            });
                            vm.juzgadoIdOriginal = actuacion.juzgado.id;
                        }
                        if(actuacion.despacho){
                            vm.opcionesDespacho.push({
                                id: actuacion.despacho.id,
                                nombre: actuacion.despacho.nombre,
                                despacho: actuacion.despacho.despacho
                            });
                            vm.despachoIdOriginal = actuacion.despacho.id;
                        }
                        if(actuacion.tipo_notificacion){
                            vm.tipoNotificacionId = actuacion.tipo_notificacion.id;
                            vm.opcionesTipoActuacion.push({
                                id: actuacion.tipo_notificacion.id, nombre: actuacion.tipo_notificacion.nombre
                            });
                        }
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.guardadoDeshabilitado = true;
                var datos = {
                    id: actuacion ? +actuacion.id : null,
                    ciudad_id: vm.ciudadId,
                    juzgado_id: vm.juzgadoId,
                    despacho_id: vm.despachoId,
                    anotacion: vm.anotacion,
                    fecha_registro: vm.fechaRegistro,
                    fecha_actuacion: vm.fechaActuacion,
                    fecha_inicio_termino: vm.fechaInicioTermino,
                    fecha_vencimiento_termino: vm.fechaVencimientoTermino,
                    nombres_demandantes: vm.demandante.trim(),
                    nombres_demandados: vm.demandado.trim(),
                    descripcion_clase_proceso: vm.descripcionClaseProceso ? vm.descripcionClaseProceso.trim() : null,
                    tipo_notificacion_id: langUtil.isNotBlank(vm.tipoNotificacionId) ? vm.tipoNotificacionId : null
                };

                var promesa = null;
                if(!(actuacion && actuacion.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        messageUtil.success(response.data.mensajes[0]);
                        $uibModalInstance.close(response.data.datos);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }
                }).finally(function () {
                    vm.guardadoDeshabilitado = false;
                });
            }
        }

        function guardarMasivo() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.guardadoDeshabilitado = true;
                var datos = {
                    // Campos a modificar
                    ciudad_id: vm.ciudadId,
                    juzgado_id: vm.juzgadoId,
                    despacho_id: vm.despachoId,
                    tipo_notificacion_id: langUtil.isNotBlank(vm.tipoNotificacionId) ? vm.tipoNotificacionId : null,
                    // Filtros
                    filtro_todo: !!parametros.todo,
                    filtro_actuacion_ids:
                        parametros.actuacion_ids && parametros.actuacion_ids.length > 0 ? parametros.actuacion_ids : null,
                    filtro_departamento_id:
                        langUtil.isNotBlank(parametros.filtro_departamento_id) ? parametros.filtro_departamento_id : null,
                    filtro_ciudad_id: langUtil.isNotBlank(parametros.filtro_ciudad_id) ? parametros.filtro_ciudad_id : null,
                    filtro_juzgado_id: langUtil.isNotBlank(parametros.filtro_juzgado_id) ? parametros.filtro_juzgado_id : null,
                    filtro_despacho_id: langUtil.isNotBlank(parametros.filtro_despacho_id) ? parametros.filtro_despacho_id : null,
                    filtro_numero_radicado_actual:
                        langUtil.isNotBlank(parametros.filtro_numero_radicado_actual) ? parametros.filtro_numero_radicado_actual : null,
                    filtro_fecha_desde: langUtil.isNotBlank(parametros.filtro_fecha_desde) ? parametros.filtro_fecha_desde : null,
                    filtro_fecha_hasta: langUtil.isNotBlank(parametros.filtro_fecha_hasta) ? parametros.filtro_fecha_hasta : null,
                    filtro_tipo_notificacion_id:
                        langUtil.isNotBlank(parametros.filtro_tipo_notificacion_id) ? parametros.filtro_tipo_notificacion_id : null
                };

                genericService.modificar(recurso, datos)
                    .then(function(response){
                        if(
                            response.status === Constantes.Response.HTTP_CREATED ||
                            response.status === Constantes.Response.HTTP_OK
                        ){
                            messageUtil.success(response.data.mensajes[0]);
                            $uibModalInstance.close(response.data.datos);
                        }else{
                            messageUtil.error(response.data.mensajes[0]);
                        }
                    }).finally(function () {
                        vm.guardadoDeshabilitado = false;
                    });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }

        function validarDemandante() {
            vm.demandante = vm.demandante ? vm.demandante.toUpperCase() : vm.demandante;
        }

        function validarDemandado() {
            vm.demandado = vm.demandado ? vm.demandado.toUpperCase() : vm.demandado;
        }

        // Autocompletes

        function initAutocompleteDepartamento() {
            vm.configDepartamento = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                render: {
                    item: function(item, escape) {
                        return '<div>' +
                            '<span>' + escape(item.nombre) + '</span>' +
                            '<span> - ' + escape(item.codigo_dane) + '</span>' +
                            '</div>';
                    },
                    option: function(item, escape) {
                        return '<div>' +
                            '<span>' + escape(item.nombre) + '</span>' +
                            '<span> - ' + escape(item.codigo_dane) + '</span>' +
                            '</div>';
                    }
                },
                onItemAdd: function (value, $item) {
                    if(!vm.cargandoCiudades){
                        vm.cargandoCiudades = true;
                        vm.despachoId = null;
                        vm.juzgadoId = null;
                        vm.ciudadId = null;
                        vm.opcionesDespacho = [];
                        vm.opcionesJuzgado = [];
                        vm.opcionesCiudad = [];
                        $timeout(function () {
                            vm.limpiarOpcionesDespacho = true;
                            vm.limpiarOpcionesJuzgado = true;
                            vm.limpiarOpcionesCiudad = true;
                            genericService.obtenerColeccionLigera("ciudades", {
                                ligera: true,
                                departamento_id: value
                            }).then(function (response){
                                vm.opcionesCiudad = [].concat(response.data);
                                if(vm.ciudadIdOriginal){
                                    vm.ciudadId = vm.ciudadIdOriginal;
                                    vm.ciudadIdOriginal = null;
                                }
                            }).finally(function () {
                                vm.cargandoCiudades = false;
                            });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.despachoId = null;
                    vm.juzgadoId = null;
                    vm.ciudadId = null;
                    vm.opcionesDespacho = [];
                    vm.opcionesJuzgado = [];
                    vm.opcionesCiudad = [];
                    $timeout(function () {
                        vm.limpiarOpcionesDespacho = true;
                        vm.limpiarOpcionesJuzgado = true;
                        vm.limpiarOpcionesCiudad = true;
                    }, 300);
                }
            };
            vm.opcionesDepartamento = [];
            genericService.obtenerColeccionLigera("departamentos",{
                ligera: true,
            }).then(function (response){
                vm.opcionesDepartamento = [].concat(response.data);
            });
        }

        function initAutocompleteCiudad() {
            vm.configCiudad = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                render: {
                    item: function(item, escape) {
                        return '<div>' +
                            '<span>' + escape(item.nombre) + '</span>' +
                            '<span> - ' + escape(item.codigo_dane) + '</span>' +
                            '</div>';
                    },
                    option: function(item, escape) {
                        return '<div>' +
                            '<span>' + escape(item.nombre) + '</span>' +
                            '<span> - ' + escape(item.codigo_dane) + '</span>' +
                            '</div>';
                    }
                },
                onItemAdd: function (value, $item) {
                    if(!vm.cargandoJuzgados){
                        vm.cargandoJuzgados = true;
                        vm.despachoId = null;
                        vm.juzgadoId = null;
                        vm.opcionesDespacho = [];
                        vm.opcionesJuzgado = [];
                        $timeout(function () {
                            vm.limpiarOpcionesDespacho = true;
                            vm.limpiarOpcionesJuzgado = true;
                            genericService.obtenerColeccionLigera("juzgados", {
                                ligera: true,
                                ciudad_id: value
                            }).then(function (response){
                                vm.opcionesJuzgado = [].concat(response.data);
                                if(vm.juzgadoIdOriginal){
                                    vm.juzgadoId = vm.juzgadoIdOriginal;
                                    vm.juzgadoIdOriginal = null;
                                }
                            }).finally(function () {
                                vm.cargandoJuzgados = false;
                            });
                        }, 300);
                    }
                },
                onItemRemove: function () {
                    vm.despachoId = null;
                    vm.juzgadoId = null;
                    vm.opcionesDespacho = [];
                    vm.opcionesJuzgado = [];
                    $timeout(function () {
                        vm.limpiarOpcionesDespacho = true;
                        vm.limpiarOpcionesJuzgado = true;
                    }, 300);
                }
            };
            vm.opcionesCiudad = [];
        }

        function initAutocompleteJuzgado() {
            vm.configJuzgado = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                render: {
                    item: function(item, escape) {
                        return '<div>' +
                            '<span>' + escape(item.nombre) + '</span>' +
                            '<span> - ' + escape(item.juzgado) + escape(item.sala) + '</span>' +
                            '</div>';
                    },
                    option: function(item, escape) {
                        return '<div>' +
                            '<span>' + escape(item.nombre) + '</span>' +
                            '<span> - ' + escape(item.juzgado) + escape(item.sala) + '</span>' +
                            '</div>';
                    }
                },
                onItemAdd: function (value, $item) {
                    if(!vm.cargandoDespachos){
                        vm.cargandoDespachos = true;
                        vm.despachoId = null;
                        vm.opcionesDespacho = [];
                        $timeout(function () {
                            vm.limpiarOpcionesDespacho = true;
                            genericService.obtenerColeccionLigera("despachos", {
                                ligera: true,
                                ciudad_id: vm.ciudadId ? vm.ciudadId : null,
                                juzgado_id: value
                            }).then(function (response){
                                vm.opcionesDespacho = [].concat(response.data);
                                if(vm.despachoIdOriginal){
                                    vm.despachoId = vm.despachoIdOriginal;
                                    vm.despachoIdOriginal = null;
                                }else{
                                    var juzgadoNombre = $item[0].innerText;
                                    var despacho000 = _.find(response.data, function (despacho) {
                                        return despacho.despacho == '000';
                                    });
                                    if(despacho000 && juzgadoNombre && !juzgadoNombre.includes('JUZGADO')){
                                        vm.despachoId = despacho000.id;
                                    }
                                }
                            }).finally(function () {
                                vm.cargandoDespachos = false;
                            });
                        }, 300);
                    }
                },
                onItemRemove: function () {
                    vm.despachoId = null;
                    vm.opcionesDespacho = [];
                    $timeout(function () {
                        vm.limpiarOpcionesDespacho = true;
                    }, 300);
                }
            };
            vm.opcionesJuzgado = [];
        }

        function initAutocompleteDespacho() {
            vm.configDespacho = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                render: {
                    item: function(item, escape) {
                        return '<div>' +
                            '<span>' + escape(item.despacho) + '</span>' +
                            '</div>';
                    },
                    option: function(item, escape) {
                        return '<div>' +
                            '<span>' + escape(item.despacho) + '</span>' +
                            '</div>';
                    }
                }
            };
            vm.opcionesDespacho = [];
        }

        function initAutocompleteTipoNotificacion() {
            vm.configTipoNotificacion = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true
            };
            vm.opcionesTipoNotificacion = [];
            genericService.obtenerColeccionLigera("tipos-notificaciones",{
                ligera: true,
            }).then(function (response){
                vm.opcionesTipoNotificacion = [].concat(response.data);
            });
        }

    }
})();
