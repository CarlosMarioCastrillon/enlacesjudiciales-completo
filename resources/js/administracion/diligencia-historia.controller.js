(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('diligenciaHistoriaController', diligenciaHistoriaController);

    diligenciaHistoriaController.$inject = [
        '$translate',
        '$routeParams',
        '$scope',
        '$timeout',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function diligenciaHistoriaController(
        $translate, $routeParams, $scope, $timeout, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            i18n = {},
            pipePromise,
            recurso = "diligencias-historia",
            diligenciaId = $routeParams.id;

        vm.consulta = $routeParams.consulta && $routeParams.consulta !== "null" ? $routeParams.consulta : null;

        vm.diligencia = [];
        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.EstadoDiligenciaEnum = Constantes.EstadoDiligencia;
        vm.activarBuscar = true;

        // Control de grid
        vm.obtenerResumen = obtenerResumen;

        // Modulos externos
        vm.confirmarEliminar = confirmarEliminar;

        // Cargar traducciones
        cargarTraducciones();

        // Cargar autocompletes
        initAutocompleteEstadoDiligencia();

        // Grid

        function obtenerResumen(){
            genericService.cargar('diligencias', diligenciaId)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        vm.diligencia = response.data;
                        vm.coleccion = vm.diligencia.diligencia_historia;
                    }
                }).finally(function () {
                    vm.cargando = false;
                });

        }

        // Acciones

        function eliminar(row){
            genericService.eliminar(recurso, row.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        $scope.$broadcast('refreshGrid');
                        location.reload();
                        messageUtil.success(response.data.mensajes[0]);
                    }else if(
                        response.status === Constantes.Response.HTTP_CONFLICT ||
                        response.status === Constantes.Response.HTTP_BAD_REQUEST
                    ){
                        messageUtil.error(response.data.mensajes[0]);
                    }
                });
        }

        function confirmarEliminar(row) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + 'confirmar-template',
                controller: 'confirmarController as vm',
                size: 'md',
                resolve: {
                    parametros: function () {
                        return {
                            mensaje: "¿Estás seguro de que quieres eliminar la tarifa?"
                        };
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(response.ok){
                    eliminar(row);
                }
            });
        }

        function initAutocompleteEstadoDiligencia() {
            vm.configEstadoDiligencia = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true
            };
            vm.opcionesEstadoDiligencia = [];
        }

        // Traducciones

        function cargarTraducciones() {
            $translate([
                "SOLICITADO", "COTIZADO", "APROBADO"
            ]).then(function (translations) {
                i18n.SOLICITADO = translations.SOLICITADO;
                i18n.COTIZADO = translations.COTIZADO;
                i18n.APROBADO = translations.APROBADO;

                vm.opcionesEstadoDiligencia = [
                    {id: Constantes.EstadoDiligencia.SOLICITADO, nombre: i18n.SOLICITADO},
                    {id: Constantes.EstadoDiligencia.COTIZADO, nombre: i18n.COTIZADO},
                    {id: Constantes.EstadoDiligencia.APROBADO, nombre: i18n.APROBADO}
                ];
            }, function (translationIds) {});
        }
    }
})();
