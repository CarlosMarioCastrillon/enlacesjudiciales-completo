(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('procesoValorController', procesoValorController);

    procesoValorController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        '$uibModal',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function procesoValorController(
        $scope, $routeParams, $timeout, $uibModal, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "procesos-valores",
            procesoId = $routeParams.id ? $routeParams.id : null;

        vm.coleccion = [];
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Cargar todos los valores del proceso
        cargar();

        // Cargar los datos de la proceso
        genericService.cargar("procesos-judiciales", procesoId)
            .then(function (response) {
                if (response.status === Constantes.Response.HTTP_OK) {
                    vm.proceso = response.data;
                }
            });

        function cargar(){
            vm.cargando = true;
            genericService.obtener(recurso, {proceso_id: procesoId})
            .then(function (response){
                if(response.status === Constantes.Response.HTTP_OK){
                    vm.coleccion = response.data;
                }
            }).finally(function () {
                vm.cargando = false;
            });
        }

        // Acciones
        vm.crear = crear;

        // Modulos externos
        vm.confirmarEliminar = confirmarEliminar;

        /*function cargarFiltros() {
            var filtros = gridUtil.getFormattedSearchPredicate("procesoValor");
            vm.nombre = filtros && filtros.nombre ? filtros.nombre : null;
        }*/

        // Acciones

        function eliminar(procesoValor){
            genericService.eliminar(recurso, procesoValor.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        $scope.$broadcast('refreshGrid');
                        messageUtil.success(response.data.mensajes[0]);
                        location.reload();
                    }else if(
                        response.status === Constantes.Response.HTTP_CONFLICT ||
                        response.status === Constantes.Response.HTTP_BAD_REQUEST
                    ){
                        messageUtil.error(response.data.mensajes[0]);
                    }
                });
        }

        // Modulos externos

        function crear(procesoValor) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + recurso + '/create',
                controller: 'procesoValorEditController as vm',
                size: 'md',
                resolve: {
                    parametros: function () {
                        if (procesoValor){
                            vm.procesoValor = procesoValor;
                        }else{
                            vm.procesoValor = [];
                        }
                        return {
                            procesoValor: vm.procesoValor,
                            proceso: vm.proceso
                        };
                    }
                }
            });

            modalInstance.result.then(function (response) {
                if(procesoValor && procesoValor.id && response && response.id){
                    procesoValor.concepto_id = response.concepto_id;
                    procesoValor.fecha = response.fecha;
                    procesoValor.valor = response.valor;
                    procesoValor.proceso_id = procesoId;
                    procesoValor.estado = response.estado;
                    procesoValor.observacion = response.observacion;
                    procesoValor.usuario_modificacion_id = response.usuario_modificacion_id;
                    procesoValor.usuario_modificacion_nombre = response.usuario_modificacion_nombre;
                    procesoValor.fecha_creacion = response.fecha_creacion;
                    procesoValor.fecha_modificacion = response.fecha_modificacion;
                }else if(response && response.id){
                    $scope.$broadcast('refreshGrid');
                }
            });
        }

        function confirmarEliminar(procesoValor) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + 'confirmar-template',
                controller: 'confirmarController as vm',
                size: 'md',
                resolve: {
                    parametros: function () {
                        return {
                            mensaje: "¿Estás seguro de que quieres eliminar el valor?"
                        };
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(response.ok){
                    eliminar(procesoValor);
                }
            });
        }

    }
})();
