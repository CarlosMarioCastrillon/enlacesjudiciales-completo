(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('terminoActuacionController', terminoActuacionController);

    terminoActuacionController.$inject = [
        '$scope',
        '$timeout',
        '$uibModal',
        'sessionUtil',
        'envService',
        '$routeParams',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function terminoActuacionController(
        $scope, $timeout, $uibModal, sessionUtil, envService, $routeParams, genericService, messageUtil, Constantes
    ){
        var vm = this,
            recurso = "actuaciones";
        vm.pasa =  !!($routeParams.pasa && +$routeParams.pasa === 1);

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 5;
        $scope.isReset = true;
        vm.activarBuscar = true;
        vm.filtroVenceHoy = false;
        vm.filtroPorVencer = false;
        vm.filtroPendientes = false;
        vm.filtroMasDeDiezDias = false;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.buscar = buscar;
        vm.obtenerExcel = obtenerExcel;
        vm.obtenerArchivoAnexo = obtenerArchivoAnexo;
        vm.pasarActuaciones = pasarActuaciones;

        // Limpiar filtros
        limpiarFiltros();

        // Grid

        function buscar() {
            vm.buscando = true;
            if(vm.form.$valid) {
                vm.activarBuscar = true;
                $scope.$broadcast('refreshGrid');
            }
        }

        function obtenerResumen(tableState, ctrl){
            tableState.search = tableState.search || {};
            tableState.search.predicateObject = tableState.search.predicateObject || {};
            tableState.search.predicateObject.cumplimiento = vm.pasa;
            tableState.search.predicateObject.vence_hoy = vm.filtroVenceHoy;
            tableState.search.predicateObject.por_vencer = vm.filtroPorVencer;
            tableState.search.predicateObject.pendientes = vm.filtroPendientes;
            tableState.search.predicateObject.mas_de_diz_dias = vm.filtroMasDeDiezDias;

            if(vm.paginacion && vm.paginacion.paginaActual && tableState.pagination){
                var start = tableState.pagination.start || 0,
                    number = tableState.pagination.number || 10,
                    nextPage = (start + number) / number;
                if(vm.paginacion.paginaActual != nextPage){
                    vm.activarBuscar = true;
                }else if(vm.activarBuscar){
                    tableState.pagination.start = 0;
                    tableState.pagination.number = vm.itemsPorPagina;
                }
            }

            if(vm.activarBuscar && !vm.cargando){
                vm.cargando = true;
                genericService.obtenerColeccion(recurso + "/actuaciones-pendientes", tableState)
                .then(function (response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var coleccion = response.data.datos;
                        if(coleccion && coleccion.length > 0 ){
                            var fechaActual = moment(),
                                diferencia = null,
                                fechaVencimientoTermino = null;
                            angular.forEach(coleccion, function (row) {
                                fechaVencimientoTermino = moment(row.fecha_vencimiento_termino);
                                diferencia = fechaVencimientoTermino.diff(fechaActual, 'days');
                                if(diferencia === 0){
                                    row.style = {'background-color': '#EEF2E6'};
                                }else if(diferencia >= 1 && diferencia <= 3){
                                    row.style = {'background-color': '#FFFF9D'};
                                }else if(diferencia >= 4 && diferencia <= 10){
                                    row.style = {'background-color': '#9BFF93'};
                                }else if(diferencia > 10){
                                    row.style = {'background-color': '#92D0FE'};
                                }
                            });
                            vm.coleccion = [].concat(coleccion);
                            vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                            vm.paginacion.paginaActual = response.data.pagina_actual;
                            vm.paginacion.desde = response.data.desde;
                            vm.paginacion.hasta = response.data.hasta;
                            vm.paginacion.total = response.data.total;
                            tableState.pagination.numberOfPages = response.data.ultima_pagina;
                        }else{
                            vm.coleccion = [];
                        }
                    }
                }).finally(function () {
                    vm.buscando = false;
                    vm.cargando = false;
                    vm.activarBuscar = false;
                });
            }
        }

        function pasarActuaciones() {
            var actuacionIds = [];
            angular.forEach(vm.coleccion, function (row) {
                if(row.pasa){
                    actuacionIds.push(row.id);
                }
            });

            if(actuacionIds.length > 0){
                genericService.modificar(recurso + "/actuaciones-pendientes", actuacionIds)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        buscar();
                        messageUtil.success(response.data.mensajes[0]);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }
                });
            }else{
                messageUtil.error("Debe seleccionar al menos una actuación");
            }
        }

        function limpiarFiltros(){
            vm.nombre = null;
            vm.ciudad = null;
            $scope.isReset = true;
        }

        // Acciones

        function obtenerExcel() {
            if(vm.coleccion.length > 0) {
                genericService.obtenerArchivo("actuaciones/actuaciones-pendientes/excel", {
                    proceso: vm.filtroProceso,
                    actuacion: vm.filtroActuacion,
                    demandante: vm.filtroDemandante,
                    demandado: vm.filtroDemandado,
                    vence_hoy: vm.filtroVenceHoy,
                    por_vencer: vm.filtroPorVencer,
                    pendientes: vm.filtroPendientes,
                    mas_de_diz_dias: vm.filtroMasDeDiezDias,
                    cumplimiento: vm.pasa
                });
            }
        }

        function obtenerArchivoAnexo(row) {
            genericService.obtenerArchivo("actuaciones/" + row.id + "/archivo-anexo")
                .then(function (response) {
                    if(response.status === Constantes.Response.HTTP_NOT_FOUND){
                        messageUtil.error("No se encontro un archivo anexo.");
                    }else if(response.status === Constantes.Response.HTTP_FORBIDDEN){
                        messageUtil.error("No tiene permitido descargar este archivo.");
                    }
                });
        }

    }
})();
