(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('actuacionImportarController', actuacionImportarController);

    actuacionImportarController.$inject = [
        '$scope',
        '$http',
        '$routeParams',
        '$timeout',
        '$uibModal',
        'sessionUtil',
        'envService',
        'genericService',
        'Upload',
        'messageUtil',
        'Constantes'
    ];

    function actuacionImportarController(
        $scope, $http, $routeParams, $timeout, $uibModal, sessionUtil, envService, genericService, Upload, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "actuaciones";

        // Variables de inicio
        vm.excelActivo = true;
        vm.textoActivo = false;
        vm.pasoImportarDatos = true;
        vm.pasoVerificarDatos = false;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Instancias
        initWizard();

        // Acciones
        vm.finalizarImportarExcel = finalizarImportarExcel;
        vm.importar = importar;
        vm.obtenerExcel = obtenerExcel;

        // Acciones archivos
        vm.cargarArchivo = cargarArchivo;
        vm.descargarArchivo = descargarArchivo;
        vm.eliminarArchivo = eliminarArchivo;

        // Paso 1, para ir al Paso 2

        function importar() {
            if(vm.excelActivo){
                importarExcel();
            }else{
                importarTexto();
            }
        }

        function importarExcel(){
            if(!vm.cargandoArchivo){
                vm.cargandoArchivo = true;
                genericService.cargarArchivo(recurso + "/importar-excel", vm.archivo)
                    .then(function (response) {
                        if(response.status === Constantes.Response.HTTP_CREATED){
                            // Datos de respuesta
                            var datos = response.data.datos;
                            vm.errores = datos.errores;
                            vm.registrosFallidos = datos.registros_fallidos;
                            vm.registrosCargados = datos.registros_cargados;
                            vm.registrosProcesados = datos.registros_procesados;

                            // Mostrar mensaje de respuesta
                            messageUtil.success(response.data.mensajes[0]);
                        }
                    }).catch(function (error) {
                        vm.errores = [];
                        vm.registrosFallidos = 0;
                        vm.registrosCargados = 0;
                        vm.registrosProcesados = 0;

                        // Mostrar mensaje de respuesta
                        messageUtil.error(error.data.mensajes[0]);
                    }).finally(function () {
                        vm.pasoImportarDatos = false;
                        vm.pasoVerificarDatos = true;
                        vm.cargandoArchivo = false;
                    });
            }
        }

        function importarTexto() {
            if(!vm.cargandoArchivo){
                vm.cargandoArchivo = true;
                genericService.crear(recurso + "/importar-texto", {
                    texto: vm.procesosEnTexto
                })
                .then(function (response) {
                    if(response.status === Constantes.Response.HTTP_CREATED){
                        // Datos de respuesta
                        var datos = response.data.datos;
                        vm.errores = datos.errores;
                        vm.registrosFallidos = datos.registros_fallidos;
                        vm.registrosCargados = datos.registros_cargados;
                        vm.registrosProcesados = datos.registros_procesados;

                        // Mostrar mensaje de respuesta
                        messageUtil.success(response.data.mensajes[0]);
                    }
                }).catch(function (error) {
                    vm.errores = [];
                    vm.registrosFallidos = 0;
                    vm.registrosCargados = 0;
                    vm.registrosProcesados = 0;

                    // Mostrar mensaje de respuesta
                    messageUtil.error(error.data.mensajes[0]);
                }).finally(function () {
                    vm.pasoImportarDatos = false;
                    vm.pasoVerificarDatos = true;
                    vm.cargandoArchivo = false;
                });
            }
        }

        function finalizarImportarExcel(){
            vm.pasoVerificarDatos = false;
            vm.procesosEnTexto = null;
            vm.archivo = null;
            vm.archivoNombre = null;
            vm.archivoBase64 = null;
            vm.pasoImportarDatos = true;
        }

        // Acciones archivos

        function cargarArchivo($archivo) {
            Upload.dataUrl($archivo, true).then(function(base64) {
                if(base64){
                    vm.archivo = $archivo;
                    vm.archivoNombre = $archivo.name;
                    vm.archivoBase64 = base64;
                }
            });
        }

        function descargarArchivo() {
            var a = document.createElement("a");
            document.body.appendChild(a);
            a.style = "display: none";

            var blob = Upload.dataUrltoBlob(vm.archivoBase64, vm.archivoNombre),
                url = window.URL.createObjectURL(blob);

            a.href = url;
            a.download = vm.archivoNombre;
            a.click();
            window.URL.revokeObjectURL(url);
        }

        // Acciones

        function obtenerExcel() {
            genericService.obtenerArchivo(recurso + "/importar-excel",{});
        }

        function eliminarArchivo() {
            vm.archivo = null;
            vm.archivoNombre = null;
            vm.archivoBase64 = null;
        }

        // Instanciar modulos

        function initWizard(){
            //Initialize tooltips
            $('.nav-tabs > li a[title]').tooltip();
            //Wizard
            $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
                var $target = $(e.target);
                if ($target.parent().hasClass('disabled')) {
                    return false;
                }
            });
        }

    }
})();
