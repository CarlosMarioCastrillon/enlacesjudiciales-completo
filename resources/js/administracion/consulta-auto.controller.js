(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('consultaAutoController', consultaAutoController);

    consultaAutoController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function consultaAutoController(
        $scope, $routeParams, $timeout, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            today = new Date(),
            fechaInicial = (today.getFullYear()-1) + '-' + (today.getMonth() + 1) + '-' + today.getDate(),
            fechaFinal = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();

        vm.filtroFechaInicial = fechaInicial;
        vm.filtroFechaFinal = fechaFinal;

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.activarBuscar = false;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.buscar = buscar;
        vm.obtenerExcel = obtenerExcel;
        vm.obtenerArchivoAnexo = obtenerArchivoAnexo;

        // Limpiar filtros
        limpiarFiltros();

        // Grid

        function buscar() {
            vm.buscando = true;
            if(vm.form.$valid) {
                vm.activarBuscar = true;
                $scope.$broadcast('refreshGrid');
            }
        }

        function obtenerResumen(tableState, ctrl){
            tableState.search = tableState.search || {};
            tableState.search.predicateObject = tableState.search.predicateObject || {};
            tableState.search.predicateObject.url = true;
            tableState.search.predicateObject.tipo_movimiento = true;
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }

            if(vm.paginacion && vm.paginacion.paginaActual && tableState.pagination){
                var start = tableState.pagination.start || 0,
                    number = tableState.pagination.number || 10,
                    nextPage = (start + number) / number;
                if(vm.paginacion.paginaActual != nextPage){
                    vm.activarBuscar = true;
                }else if(vm.activarBuscar){
                    tableState.pagination.start = 0;
                    tableState.pagination.number = vm.itemsPorPagina;
                }
            }

            if(vm.activarBuscar && !vm.cargando){
                vm.cargando = true;
                genericService.obtenerColeccion("actuaciones/reporte-por-empresa", tableState)
                    .then(function (response){
                        if(response.status === Constantes.Response.HTTP_OK){
                            var coleccion = response.data.datos;
                            if(coleccion && coleccion.length > 0 ){
                                vm.coleccion = [].concat(coleccion);
                                vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                                vm.paginacion.paginaActual = response.data.pagina_actual;
                                vm.paginacion.desde = response.data.desde;
                                vm.paginacion.hasta = response.data.hasta;
                                vm.paginacion.total = response.data.total;
                                tableState.pagination.numberOfPages = response.data.ultima_pagina;
                            }else{
                                vm.coleccion = [];
                            }
                        }
                    }).finally(function () {
                        vm.buscando = false;
                        vm.cargando = false;
                        vm.activarBuscar = false;
                    });
            }
        }

        function limpiarFiltros(){
            vm.filtroFechaInicial = fechaInicial;
            vm.filtroFechaFinal = fechaFinal;
            vm.coleccion = [];
            $scope.isReset = true;
        }

        /*function cargarFiltros() {
            var filtros = gridUtil.getFormattedSearchPredicate("procesoHoy");
            vm.nombre = filtros && filtros.nombre ? filtros.nombre : null;
        }*/

        // Acciones

        function obtenerExcel() {
            if(vm.coleccion.length > 0) {
                genericService.obtenerArchivo("actuaciones/autos/excel", {
                    fecha_inicial: vm.filtroFechaInicial,
                    fecha_final: vm.filtroFechaFinal,
                    con_archivo_anexo: true,
                    tipo_movimiento: true
                });
            }
        }

        function obtenerArchivoAnexo(row) {
            genericService.obtenerArchivo("actuaciones/" + row.id + "/archivo-anexo")
                .then(function (response) {
                    if(response.status === Constantes.Response.HTTP_NOT_FOUND){
                        messageUtil.error("No se encontro un archivo anexo.");
                    }else if(response.status === Constantes.Response.HTTP_FORBIDDEN){
                        messageUtil.error("No tiene permitido descargar este archivo.");
                    }
                });
        }
    }
})();
