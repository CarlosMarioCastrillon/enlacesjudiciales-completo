(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('procesoValorEditController', procesoValorEditController);

    procesoValorEditController.$inject = [
        '$uibModalInstance',
        '$timeout',
        'genericService',
        'messageUtil',
        'Constantes',
        'parametros'
    ];

    function procesoValorEditController(
        $uibModalInstance, $timeout, genericService, messageUtil, Constantes, parametros
    ){
        var vm = this,
            recurso = "procesos-valores";


        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;
        vm.cargarConceptos = cargarConceptos;

        // inicial autocomples
        initAutocompleteTipoConcepto();

        if(parametros.proceso && parametros.procesoValor.id){
            cargar();
        }else{
            vm.estado = true;
        }

        vm.proceso = parametros.proceso;

        function cargar() {
            genericService.cargar(recurso, parametros.procesoValor.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK) {
                        var procesoValor = response.data,
                            concepto = procesoValor.concepto;
                        vm.valor = procesoValor.valor;
                        vm.fecha = procesoValor.fecha;
                        vm.observacion = procesoValor.observacion;
                        vm.estado = procesoValor.estado;
                        vm.fechaCreacion = procesoValor.fecha_creacion;
                        vm.fechaModificacion = procesoValor.fecha_modificacion;
                        vm.usuarioCreacionNombre = procesoValor.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = procesoValor.usuario_modificacion_nombre;
                        vm.conceptoId = procesoValor.concepto_id;

                        if (concepto) {
                            vm.tipoConcepto = concepto.tipo_concepto;
                            vm.conceptoId = concepto.id;
                            vm.opcionesConceptos.push({id: concepto.id, nombre: concepto.nombre});
                        }

                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;
                var datos = {
                    id: parametros.procesoValor ? +parametros.procesoValor.id : null,
                    concepto_id: vm.conceptoId,
                    valor: vm.valor,
                    fecha: vm.fecha,
                    observacion: vm.observacion,
                    proceso_id: vm.proceso.id,
                    estado: vm.estado
                };

                var promesa = null;
                if(!(parametros.procesoValor && parametros.procesoValor.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        $uibModalInstance.close(response.data.datos);
                        messageUtil.success(response.data.mensajes[0]);
                        location.reload();
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }
                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }

        function cargarConceptos() {
            vm.opcionesConceptos = [];
            $timeout(function () {
                vm.limpiarOpcionesConceptos = true;
                genericService.obtenerColeccionLigera("conceptos-economicos",{
                    ligera: true,
                    tipo_concepto_id: vm.tipoConcepto
                })
                .then(function (response){
                    vm.opcionesConceptos = [].concat(response.data);
                });
            }, 100);
        }

        function initAutocompleteTipoConcepto() {
            vm.configConcepto = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
            };
            vm.opcionesConceptos = [];
        }
    }
})();
