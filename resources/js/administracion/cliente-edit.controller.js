(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('clienteEditController', clienteEditController);

    clienteEditController.$inject = [
        '$uibModalInstance',
        '$timeout',
        'genericService',
        'messageUtil',
        'Constantes',
        'cliente'
    ];

    function clienteEditController(
        $uibModalInstance, $timeout, genericService, messageUtil, Constantes, cliente
    ){
        var vm = this,
            recurso = "clientes";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        // inicial autocomples
        initAutocompleteDepartamento();
        initAutocompleteCiudad();
        initAutocompleteTipoDocumento();

        if(cliente && cliente.id){
            cargar();
        }else{
            vm.estado = true;
        }

        function cargar() {
            genericService.cargar(recurso, cliente.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var cliente = response.data,
                            ciudad = cliente.ciudad,
                            departamento = cliente.departamento,
                            tipo_documento = cliente.tipo_documento;

                        vm.nombre = cliente.nombre;
                        vm.numeroDocumento = cliente.numero_documento;
                        vm.direccion = cliente.direccion;
                        vm.telefonoFijo = cliente.telefono_uno;
                        vm.telefonoCelular = cliente.telefono_dos;
                        vm.correo = cliente.email;
                        vm.estado = cliente.estado;
                        vm.fechaCreacion = cliente.fecha_creacion;
                        vm.fechaModificacion = cliente.fecha_modificacion;
                        vm.usuarioCreacionNombre = cliente.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = cliente.usuario_modificacion_nombre;

                        if(ciudad){
                            vm.departamentoId = departamento.id;
                            vm.opcionesDepartamento.push({id: departamento.id, nombre: departamento.nombre});
                            vm.ciudadId = ciudad.id;
                            vm.opcionesCiudad.push({id: ciudad.id, nombre: ciudad.nombre});
                            vm.limpiarOpcionesCiudad = false;
                        }
                        if(tipo_documento){
                            vm.tipoDocumentoId = tipo_documento.id;
                            vm.opcionesTipoDocumento.push({id: tipo_documento.id, codigo: tipo_documento.codigo});
                        }
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;

                var datos = {
                    id: cliente ? +cliente.id : null,
                    nombre: vm.nombre.toUpperCase(),
                    tipo_documento_id: vm.tipoDocumentoId,
                    numero_documento: vm.numeroDocumento,
                    departamento_id: vm.departamentoId,
                    ciudad_id: vm.ciudadId,
                    direccion: vm.direccion,
                    telefono_uno: vm.telefonoFijo,
                    telefono_dos: vm.telefonoCelular,
                    email: vm.correo,
                    estado: vm.estado

            };

                var promesa = null;
                if(!(cliente && cliente.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        messageUtil.success(response.data.mensajes[0]);
                        $uibModalInstance.close(response.data.datos);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }


                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }

        function initAutocompleteDepartamento() {
            vm.configDepartamento = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                onItemAdd: function (value, $item) {
                    if(vm.departamentoId !== value){
                        vm.opcionesCiudad = [];
                        $timeout(function () {
                            vm.limpiarOpcionesCiudad = true;
                            genericService.obtenerColeccionLigera("ciudades", {
                                ligera: true,
                                departamento_id: value
                            })
                            .then(function (response){
                                vm.opcionesCiudad = [].concat(response.data);
                            });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.opcionesCiudad = [];
                    vm.ciudadId = null;
                    $timeout(function () {
                        vm.limpiarOpcionesCiudad = true;
                    }, 100);
                }
            };

            vm.opcionesDepartamento = [];
            genericService.obtenerColeccionLigera("departamentos",{
                ligera: true,
            })
            .then(function (response){
                vm.opcionesDepartamentos = [].concat(response.data);
            });
        }

        function initAutocompleteCiudad() {
            vm.configCiudad = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                lock: false,
            };
            vm.opcionesCiudad = [];
        }

        function initAutocompleteTipoDocumento() {
            vm.configTipoDocumento = {
                valueField: 'id',
                labelField: 'codigo',
                searchField: ['codigo'],
                sortField: 'codigo',
                allowEmptyOption: false,
                create: false,
                persist: true,
            };

            vm.opcionesTipoDocumento = [];
            genericService.obtenerColeccionLigera("tipos-documentos",{
                ligera: true,
            })
            .then(function (response){
                vm.opcionesTipoDocumento = [].concat(response.data);
            });
        }
    }
})();
