(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('procesoAreaController', procesoAreaController);

    procesoAreaController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        '$uibModal',
        'envService',
        'genericService',
        'sessionUtil',
        'messageUtil',
        'Constantes'
    ];

    function procesoAreaController(
        $scope, $routeParams, $timeout, $uibModal, envService, genericService, sessionUtil, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "procesos-areas",
            empresaId = sessionUtil.getEmpresa().id;

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        vm.activarBuscar = false;
        $scope.isReset = true;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Cargar los datos de la empresa
        genericService.cargar("empresas", empresaId)
            .then(function (response) {
                if (response.status === Constantes.Response.HTTP_OK) {
                    vm.empresa = response.data;
                }
            });

        // Control de grid
        vm.obtenerResumen = obtenerResumen;

        // Acciones
        vm.buscar = buscar;
        vm.obtenerExcel = obtenerExcel;
        vm.limpiarFiltros = limpiarFiltros;

        // inicial autocomples
        initAutocompleteArea();

        // Limpiar filtros
        limpiarFiltros();


        function buscar() {
            vm.buscando = true;
            if(vm.form.$valid && !vm.deshabilitarBuscado) {
                vm.activarBuscar = true;
                $scope.$broadcast('refreshGrid');
            }
        }

        // Grid
        function obtenerResumen(tableState, ctrl){
            tableState.search = tableState.search || {};
            tableState.search.predicateObject = tableState.search.predicateObject || {};
            tableState.search.predicateObject.empresa_id = empresaId;
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }

            if(vm.paginacion && vm.paginacion.paginaActual && tableState.pagination){
                var start = tableState.pagination.start || 0,
                    number = tableState.pagination.number || 10,
                    nextPage = (start + number) / number;
                if(vm.paginacion.paginaActual != nextPage){
                    vm.activarBuscar = true;
                }
            }

            if(vm.activarBuscar && !vm.cargando){
                vm.cargando = true;
                genericService.obtenerColeccion('procesos-judiciales', tableState).then(function (response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var coleccion = response.data.datos;
                        if(coleccion && coleccion.length > 0 ){
                            vm.coleccion = [].concat(coleccion);
                            vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                            vm.paginacion.paginaActual = response.data.pagina_actual;
                            vm.paginacion.desde = response.data.desde;
                            vm.paginacion.hasta = response.data.hasta;
                            vm.paginacion.total = response.data.total;
                            tableState.pagination.numberOfPages = response.data.ultima_pagina;
                        }else{
                            vm.coleccion = [];
                        }
                    }
                }).finally(function () {
                    vm.buscando = false;
                    vm.cargando = false;
                    vm.activarBuscar = false;
                });
            }
        }

        function limpiarFiltros(){
            vm.filtroArea = null;
            vm.coleccion = [];
        }

        function obtenerExcel() {
            if(vm.coleccion.length > 0) {
                genericService.obtenerArchivo(recurso + "/excel", {
                    area: vm.filtroArea,
                });
            }
        }

        function initAutocompleteArea() {
            vm.configArea = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
            };

            vm.opcionesArea = [];
            genericService.obtenerColeccionLigera("areas",{
                ligera: true,
                empresa_id: empresaId
            })
                .then(function (response){
                    vm.opcionesArea = [].concat(response.data);
                });
        }
    }
})();
