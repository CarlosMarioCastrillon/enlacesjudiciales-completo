(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('clienteController', clienteController);

    clienteController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        '$uibModal',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function clienteController(
        $scope, $routeParams, $timeout, $uibModal, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "clientes";

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.crear = crear;
        vm.obtenerExcel = obtenerExcel;

        // Modulos externos
        vm.confirmarEliminar = confirmarEliminar;

        // Limpiar filtros
        limpiarFiltros();

        // Grid
        function obtenerResumen(tableState, ctrl){
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }
            if(pipePromise) $timeout.cancel(pipePromise);
            pipePromise = $timeout(function() {
                if(!vm.cargando){
                    vm.cargando = true;
                    genericService.obtenerColeccion(recurso, tableState)
                        .then(function (response){
                            if(response.status === Constantes.Response.HTTP_OK){
                                var coleccion = response.data.datos;
                                if(coleccion && coleccion.length > 0 ){
                                    vm.coleccion = [].concat(coleccion);
                                    vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                                    vm.paginacion.paginaActual = response.data.pagina_actual;
                                    vm.paginacion.desde = response.data.desde;
                                    vm.paginacion.hasta = response.data.hasta;
                                    vm.paginacion.total = response.data.total;
                                    tableState.pagination.numberOfPages = response.data.ultima_pagina;
                                }else{
                                    vm.coleccion = [];
                                }
                            }
                        }).finally(function () {
                            vm.cargando = false;
                        });
                }
            }, 500);
        }

        function limpiarFiltros(){
                vm.filtroNombre = null;
                vm.filtroNumeroDocumento = null;
                vm.filtroCiudadNombre = null;
                $scope.isReset = true;
        }

        /*function cargarFiltros() {
            var filtros = gridUtil.getFormattedSearchPredicate("cliente");
            vm.nombre = filtros && filtros.nombre ? filtros.nombre : null;
        }*/

        function obtenerExcel() {
            genericService.obtenerArchivo(recurso + "/excel", {
                nombre: vm.filtroNombre,
                numero_documento: vm.filtroNumeroDocumento,
                ciudad_nombre: vm.filtroCiudadNombre
            });
        }

        // Acciones

        function eliminar(row){
            genericService.eliminar(recurso, row.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        $scope.$broadcast('refreshGrid');
                        messageUtil.success(response.data.mensajes[0]);
                    }else if(
                        response.status === Constantes.Response.HTTP_CONFLICT ||
                        response.status === Constantes.Response.HTTP_BAD_REQUEST
                    ){
                        messageUtil.error(response.data.mensajes[0]);
                    }
                });
        }

        // Modulos externos

        function crear(cliente) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + recurso + '/create',
                controller: 'clienteEditController as vm',
                size: 'md',
                resolve: {
                    cliente: function () {
                        return cliente;
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(cliente && cliente.id && response && response.id){
                    cliente.nombre = response.nombre;
                    cliente.tipo_documento_codigo = response.tipo_documento.codigo;
                    cliente.numero_documento = response.numero_documento;
                    cliente.departamento_nombre = response.departamento.nombre;
                    cliente.ciudad_nombre = response.ciudad.nombre;
                    cliente.telefono_uno = response.telefono_uno;
                    cliente.telefono_dos = response.telefono_dos;
                    cliente.email = response.email;
                    cliente.estado = response.estado;
                    cliente.usuario_modificacion_id = response.usuario_modificacion_id;
                    cliente.usuario_modificacion_nombre = response.usuario_modificacion_nombre;
                    cliente.fecha_creacion = response.fecha_creacion;
                    cliente.fecha_modificacion = response.fecha_modificacion;

                }else if(response && response.id){
                    $scope.$broadcast('refreshGrid');
                }
            });
        }

        function confirmarEliminar(row) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + 'confirmar-template',
                controller: 'confirmarController as vm',
                size: 'lg',
                resolve: {
                    parametros: function () {
                        return {
                            mensaje: "¿Estás seguro de que quieres eliminar el cliente?"
                        };
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(response.ok){
                    eliminar(row);
                }
            });
        }

    }
})();
