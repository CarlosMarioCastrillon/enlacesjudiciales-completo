(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('procesoJudicialEditController', procesoJudicialEditController)
        .value('cgBusyDefaults',{
            message: 'Consultando proceso en la rama...'
        });

    procesoJudicialEditController.$inject = [
        '$uibModalInstance',
        '$timeout',
        'genericService',
        'sessionUtil',
        'langUtil',
        'messageUtil',
        'Constantes',
        'procesoJudicial'
    ];

    function procesoJudicialEditController(
        $uibModalInstance, $timeout, genericService, sessionUtil, langUtil, messageUtil, Constantes, procesoJudicial
    ){
        var vm = this,
            recurso = "procesos-judiciales";

        // Acciones
        vm.validarYGuardar = validarYGuardar;
        vm.cancelar = cancelar;
        vm.confirmarValidar = confirmarValidar;
        vm.validar = validar;
        vm.validarAnio = validarAnio;
        vm.validarRadicado = validarRadicado;
        vm.validarConsecutivo = validarConsecutivo;
        vm.modificarConsecutivo = modificarConsecutivo;
        vm.validarDemandante = validarDemandante;
        vm.validarDemandado = validarDemandado;
        vm.validarObservaciones = validarObservaciones;
        vm.establecerCambioInstancia = establecerCambioInstancia;

        // inicial autocomples
        initAutocompleteDepartamento();
        initAutocompleteCiudad();
        initAutocompleteJuzgado();
        initAutocompleteDespacho();
        initAutocompleteResponsable();
        initAutocompleteClaseProceso();
        initAutocompleteEtapaProceso();
        initAutocompleteArea();
        initAutocompleteCliente();
        initAutocompleteTipoActuacion();

        // Cargar pantalla
        init();

        function init() {
            // Parametros constantes
            vm.entorno = sessionUtil.getParametros();

            // Enumeradores
            vm.CambioInstanciaEnum = Constantes.CambioInstancia;

            vm.promesasActivas = [];
            var minDate = moment().subtract(40, 'years');
            vm.anioOptions = {
                useCurrent: false, minDate: minDate, maxDate: 'now', format: 'YYYY'
            };
            if(procesoJudicial && procesoJudicial.id){
                vm.modoEdicion = true;
                cargar();
            }else{
                vm.procesoValidado = false;
                vm.camposSinValidarOcultos = true;
            }

            // Configuración busy
            vm.cbusy = {
                promise: vm.promesasActivas,
                message:'Validando proceso...'
            }

            // Mostrar campos
            if(vm.entorno && +vm.entorno.VER_RESPONSABLE_EN_PROCESO){
                vm.verResponsable = true;
            }
            if(vm.entorno && +vm.entorno.VER_CLASE_PROCESO_EN_PROCESO){
                vm.verClaseProceso = true;
            }
            if(vm.entorno && +vm.entorno.VER_AREA_EN_PROCESO){
                vm.verArea = true;
            }

            // Evitar letras radicado
            $timeout(function () {
                $('.only-numbers').on("keypress", function (event) {
                    var charCode = (event.keyCode ? event.keyCode : event.which);
                    if (charCode !== 46 && charCode > 31 && (charCode < 48 || charCode > 57)){
                        event.preventDefault();
                    }
                });
            }, 100);
        }

        function cargar() {
            genericService.cargar(recurso, procesoJudicial.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var procesoJudicial = response.data;

                        vm.anio = procesoJudicial.anio_proceso;
                        vm.radicado = procesoJudicial.numero_radicado;
                        vm.radicadoProcesoActual = procesoJudicial.numero_radicado_proceso_actual;
                        vm.anioNombre = procesoJudicial.anio_proceso;
                        vm.radicadoNombre = procesoJudicial.numero_radicado;
                        vm.consecutivoNombre = procesoJudicial.numero_radicado_proceso_actual.substring(21, 23);
                        vm.radicadoProcesoActualNombre = procesoJudicial.numero_radicado_proceso_actual;
                        vm.observaciones = procesoJudicial.observaciones;
                        vm.demandante = procesoJudicial.nombres_demandantes;
                        vm.demandado = procesoJudicial.nombres_demandados;
                        vm.fechaCreacion = procesoJudicial.fecha_creacion;
                        vm.fechaModificacion = procesoJudicial.fecha_modificacion;
                        vm.usuarioCreacionNombre = procesoJudicial.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = procesoJudicial.usuario_modificacion_nombre;
                        vm.estado = procesoJudicial.indicativo_estado;
                        if(procesoJudicial.departamento){
                            vm.departamentoId = procesoJudicial.departamento.id;
                            vm.departamentoNombre = procesoJudicial.departamento.nombre;
                            vm.opcionesDepartamento.push({
                                id: procesoJudicial.departamento.id,
                                nombre: procesoJudicial.departamento.nombre,
                                codigo_dane: procesoJudicial.departamento.codigo_dane
                            });
                            vm.departamentoIdOriginal = procesoJudicial.departamento.id;
                        }
                        if(procesoJudicial.ciudad){
                            vm.ciudadNombre = procesoJudicial.ciudad.nombre;
                            vm.opcionesCiudad.push({
                                id: procesoJudicial.ciudad.id,
                                nombre: procesoJudicial. ciudad.nombre,
                                codigo_dane: procesoJudicial.ciudad.codigo_dane
                            });
                            vm.ciudadIdOriginal = procesoJudicial.ciudad.id;
                        }
                        if(procesoJudicial.juzgado){
                            vm.juzgadoNombre = procesoJudicial.juzgado.nombre;
                            vm.opcionesJuzgado.push({
                                id: procesoJudicial.juzgado.id,
                                nombre: procesoJudicial.juzgado.nombre,
                                juzgado: procesoJudicial.juzgado.juzgado,
                                sala: procesoJudicial.juzgado.sala
                            });
                            vm.juzgadoIdOriginal = procesoJudicial.juzgado.id;
                        }
                        if(procesoJudicial.despacho){
                            vm.despachoNombre = procesoJudicial.despacho.nombre;
                            vm.opcionesDespacho.push({
                                id: procesoJudicial.despacho.id,
                                nombre: procesoJudicial.despacho.nombre,
                                despacho: procesoJudicial.despacho.despacho
                            });
                            vm.despachoIdOriginal = procesoJudicial.despacho.id;
                        }
                        if(procesoJudicial.abogado){
                            vm.responsableId = procesoJudicial.abogado.id;
                            vm.opcionesResponsable.push({
                                id: procesoJudicial.abogado.id, nombre: procesoJudicial.abogado.nombre
                            });
                        }
                        if(procesoJudicial.clase_proceso){
                            vm.claseProcesoId = procesoJudicial.clase_proceso.id;
                            vm.opcionesClaseProceso.push({
                                id: procesoJudicial.clase_proceso.id, nombre: procesoJudicial.clase_proceso.nombre
                            });
                        }
                        if(procesoJudicial.etapa_proceso){
                            vm.etapaProcesoId = procesoJudicial.etapa_proceso.id;
                            vm.opcionesEtapaProceso.push({
                                id: procesoJudicial.etapa_proceso.id, nombre: procesoJudicial.etapa_proceso.nombre
                            });
                        }
                        if(procesoJudicial.area){
                            vm.areaId = procesoJudicial.area.id;
                            vm.opcionesArea.push({
                                id: procesoJudicial.area.id, nombre: procesoJudicial.area.nombre
                            });
                        }
                        if(procesoJudicial.cliente){
                            vm.clienteId = procesoJudicial.cliente.id;
                            vm.opcionesCliente.push({
                                id: procesoJudicial.cliente.id, nombre: procesoJudicial.cliente.nombre
                            });
                        }
                        if(procesoJudicial.tipo_actuacion){
                            vm.tipoActuacionId = procesoJudicial.tipo_actuacion.id;
                            vm.opcionesTipoActuacion.push({
                                id: procesoJudicial.tipo_actuacion.id, nombre: procesoJudicial.tipo_actuacion.nombre
                            });
                        }
                    }
                });
        }

        function validarYGuardar() {
            if(!vm.procesoValidado){
                validar();
            }else{
                guardar();
            }
        }

        function confirmarValidar() {
            vm.procesoValidado = false;
            if(
                !vm.procesoValidado &&
                vm.radicadoProcesoActual && vm.radicadoProcesoActual.trim().length === 23
            ){
                validar();
            }
        }

        function validar() {
            if(
                !vm.procesoValidado &&
                vm.radicadoProcesoActual && vm.radicadoProcesoActual.trim().length === 23 && !vm.guardadoDeshabilitado
            ){
                vm.guardadoDeshabilitado = true;
                var datos = {
                    id: procesoJudicial ? +procesoJudicial.id : null,
                    departamento_id: vm.departamentoId ? vm.departamentoId : null,
                    ciudad_id: vm.ciudadId ? vm.ciudadId : null,
                    juzgado_id: vm.juzgadoId ? vm.juzgadoId : null,
                    despacho_id: vm.despachoId ? vm.despachoId : null,
                    anio_proceso: vm.anio ? vm.anio : null,
                    numero_radicado: vm.radicado ? vm.radicado.trim() : null,
                    numero_radicado_proceso_actual: vm.radicadoProcesoActual ? vm.radicadoProcesoActual.trim() : null
                };
                if(vm.modoEdicion){
                    datos.con_cambio_instancia = vm.conCambioInstancia;
                }
                var promesa = genericService.crear(recurso + "/validacion", datos);
                vm.promesasActivas.push(promesa);
                promesa.then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var reporte = response.data.datos;
                        if(reporte.demandante || reporte.demandado){
                            vm.demandante = reporte.demandante ? reporte.demandante.toUpperCase() : vm.demandante;
                            vm.demandado = reporte.demandado ? reporte.demandado.toUpperCase() : vm.demandado;
                        }else{
                            messageUtil.warning(reporte.mensaje);
                        }
                        vm.procesoValidado = true;
                        vm.camposSinValidarOcultos = false;
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }
                }).finally(function () {
                    vm.guardadoDeshabilitado = false;
                });
            }else if(!(procesoJudicial && procesoJudicial.id) && !vm.procesoValidado && !vm.guardadoDeshabilitado){
                messageUtil.error("Tamaño del radicado proceso actual errado. Debe tener 23 dígitos.");
            }
        }

        function validarAnio() {
            if(vm.anio && vm.anio.length < 4){
                messageUtil.error("Tamaño de número de año errado. Debe tener 4 digitos.");
            }
        }

        function validarRadicado() {
            if(vm.radicado && vm.radicado.length < 5){
                vm.radicado = langUtil.zeroFill(vm.radicado, 5);
            }
        }

        function validarConsecutivo() {
            if(vm.consecutivo && vm.consecutivo.length < 2){
                vm.consecutivo = langUtil.zeroFill(vm.consecutivo, 2);
            }
        }

        function modificarConsecutivo() {
            var consecutivo = vm.consecutivo;
            if(consecutivo && consecutivo.length < 2){
                consecutivo = langUtil.zeroFill(consecutivo, 2);
            }
            if(vm.radicadoProcesoActual && vm.radicadoProcesoActual.length >= 21){
                vm.radicadoProcesoActual = vm.radicadoProcesoActual.substring(0, 21) +
                    (consecutivo ? consecutivo : '');
            }
            if(+vm.consecutivo > 5){
                messageUtil.error("El dato consecutivo debe ser menor o igual a 5.");
            }
        }

        function validarDemandante() {
            vm.demandante = vm.demandante ? vm.demandante.toUpperCase() : vm.demandante;
        }

        function validarDemandado() {
            vm.demandado = vm.demandado ? vm.demandado.toUpperCase() : vm.demandado;
        }

        function validarObservaciones() {
            vm.observaciones = vm.observaciones ? vm.observaciones.toUpperCase() : vm.observaciones;
        }

        function establecerCambioInstancia() {
            if(vm.conCambioInstancia && vm.conCambioInstancia === vm.conCambioInstanciaActual){
                vm.conCambioInstancia = null;
            }else{
                vm.conCambioInstanciaActual = vm.conCambioInstancia;
            }

            var numeroConsecutivoActual = vm.radicadoProcesoActualNombre.substr(21, 2);
            if(vm.conCambioInstancia === Constantes.CambioInstancia.INFERIOR && parseInt(numeroConsecutivoActual) === 0){
                vm.conCambioInstancia = vm.conCambioInstancia === Constantes.CambioInstancia.SUPERIOR;
                messageUtil.error("No puede existir instancia anterior para el proceso.");
            }
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.guardadoDeshabilitado = true;
                var datos = {
                    id: procesoJudicial ? +procesoJudicial.id : null,
                    departamento_id: vm.departamentoId,
                    ciudad_id: vm.ciudadId,
                    juzgado_id: vm.juzgadoId,
                    despacho_id: vm.despachoId,
                    anio_proceso: vm.anio ? vm.anio : null,
                    numero_radicado: vm.radicado.trim(),
                    numero_radicado_proceso_actual: vm.radicadoProcesoActual.trim(),
                    nombres_demandantes: vm.demandante.trim(),
                    nombres_demandados: vm.demandado.trim(),
                    observaciones: vm.observaciones ? vm.observaciones.trim() : null,
                    abogado_responsable_id: langUtil.isNotBlank(vm.responsableId) ? vm.responsableId : null,
                    ultima_clase_proceso_id: langUtil.isNotBlank(vm.claseProcesoId) ? vm.claseProcesoId : null,
                    area_id: langUtil.isNotBlank(vm.areaId) ? vm.areaId : null
                };
                if(vm.modoEdicion){
                    datos.con_cambio_instancia = vm.conCambioInstancia;
                    datos.ultima_etapa_proceso_id = langUtil.isNotBlank(vm.etapaProcesoId) ? vm.etapaProcesoId : null;
                    datos.cliente_id = langUtil.isNotBlank(vm.clienteId) ? vm.clienteId : null;
                    datos.ultimo_tipo_actuacion_id = langUtil.isNotBlank(vm.tipoActuacionId) ? vm.tipoActuacionId : null;
                    datos.indicativo_estado = vm.estado;
                }

                var promesa = null;
                if(!(procesoJudicial && procesoJudicial.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        messageUtil.success(response.data.mensajes[0]);
                        $uibModalInstance.close(response.data.datos);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }
                }).finally(function () {
                    vm.guardadoDeshabilitado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }

        function initAutocompleteDepartamento() {
            vm.configDepartamento = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                render: {
                    item: function(item, escape) {
                        return '<div>' +
                            '<span>' + escape(item.nombre) + '</span>' +
                            '<span> - ' + escape(item.codigo_dane) + '</span>' +
                            '</div>';
                    },
                    option: function(item, escape) {
                        return '<div>' +
                            '<span>' + escape(item.nombre) + '</span>' +
                            '<span> - ' + escape(item.codigo_dane) + '</span>' +
                            '</div>';
                    }
                },
                onItemAdd: function (value, $item) {
                    if(!vm.cargandoCiudades){
                        vm.cargandoCiudades = true;
                        vm.despachoId = null;
                        vm.juzgadoId = null;
                        vm.ciudadId = null;
                        vm.opcionesDespacho = [];
                        vm.opcionesJuzgado = [];
                        vm.opcionesCiudad = [];
                        $timeout(function () {
                            vm.limpiarOpcionesDespacho = true;
                            vm.limpiarOpcionesJuzgado = true;
                            vm.limpiarOpcionesCiudad = true;
                            genericService.obtenerColeccionLigera("ciudades", {
                                ligera: true,
                                departamento_id: value
                            }).then(function (response){
                                vm.opcionesCiudad = [].concat(response.data);
                                if(vm.ciudadIdOriginal){
                                    vm.ciudadId = vm.ciudadIdOriginal;
                                    vm.ciudadIdOriginal = null;
                                }
                            }).finally(function () {
                                vm.cargandoCiudades = false;
                            });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.despachoId = null;
                    vm.juzgadoId = null;
                    vm.ciudadId = null;
                    vm.opcionesDespacho = [];
                    vm.opcionesJuzgado = [];
                    vm.opcionesCiudad = [];
                    $timeout(function () {
                        vm.limpiarOpcionesDespacho = true;
                        vm.limpiarOpcionesJuzgado = true;
                        vm.limpiarOpcionesCiudad = true;
                    }, 300);
                }
            };
            vm.opcionesDepartamento = [];
            genericService.obtenerColeccionLigera("departamentos",{
                ligera: true,
            })
            .then(function (response){
                vm.opcionesDepartamento = [].concat(response.data);
            });
        }

        function initAutocompleteCiudad() {
            vm.configCiudad = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                render: {
                    item: function(item, escape) {
                        return '<div>' +
                            '<span>' + escape(item.nombre) + '</span>' +
                            '<span> - ' + escape(item.codigo_dane) + '</span>' +
                            '</div>';
                    },
                    option: function(item, escape) {
                        return '<div>' +
                            '<span>' + escape(item.nombre) + '</span>' +
                            '<span> - ' + escape(item.codigo_dane) + '</span>' +
                            '</div>';
                    }
                },
                onItemAdd: function (value, $item) {
                    if(!vm.cargandoJuzgados){
                        vm.cargandoJuzgados = true;
                        vm.despachoId = null;
                        vm.juzgadoId = null;
                        vm.opcionesDespacho = [];
                        vm.opcionesJuzgado = [];
                        $timeout(function () {
                            vm.limpiarOpcionesDespacho = true;
                            vm.limpiarOpcionesJuzgado = true;
                            genericService.obtenerColeccionLigera("juzgados", {
                                ligera: true,
                                ciudad_id: value,
                                juzgado_actual: 1
                            }).then(function (response){
                                vm.opcionesJuzgado = [].concat(response.data);
                                if(vm.juzgadoIdOriginal){
                                    vm.juzgadoId = vm.juzgadoIdOriginal;
                                    vm.juzgadoIdOriginal = null;
                                }
                            }).finally(function () {
                                vm.cargandoJuzgados = false;
                            });
                        }, 300);
                    }
                },
                onItemRemove: function () {
                    vm.despachoId = null;
                    vm.juzgadoId = null;
                    vm.opcionesDespacho = [];
                    vm.opcionesJuzgado = [];
                    $timeout(function () {
                        vm.limpiarOpcionesDespacho = true;
                        vm.limpiarOpcionesJuzgado = true;
                    }, 300);
                }
            };
            vm.opcionesCiudad = [];
        }

        function initAutocompleteJuzgado() {
            vm.configJuzgado = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                render: {
                    item: function(item, escape) {
                        return '<div>' +
                            '<span>' + escape(item.nombre) + '</span>' +
                            '<span> - ' + escape(item.juzgado) + escape(item.sala) + '</span>' +
                            '</div>';
                    },
                    option: function(item, escape) {
                        return '<div>' +
                            '<span>' + escape(item.nombre) + '</span>' +
                            '<span> - ' + escape(item.juzgado) + escape(item.sala) + '</span>' +
                            '</div>';
                    }
                },
                onItemAdd: function (value, $item) {
                    if(!vm.cargandoDespachos){
                        vm.cargandoDespachos = true;
                        vm.despachoId = null;
                        vm.opcionesDespacho = [];
                        $timeout(function () {
                            vm.limpiarOpcionesDespacho = true;
                            genericService.obtenerColeccionLigera("despachos", {
                                ligera: true,
                                ciudad_id: vm.ciudadId ? vm.ciudadId : null,
                                juzgado_id: value
                            })
                            .then(function (response){
                                vm.opcionesDespacho = [].concat(response.data);
                                if(vm.despachoIdOriginal){
                                    vm.despachoId = vm.despachoIdOriginal;
                                    vm.despachoIdOriginal = null;
                                }else{
                                    var juzgadoNombre = $item[0].innerText;
                                    var despacho000 = _.find(response.data, function (despacho) {
                                        return despacho.despacho == '000';
                                    });
                                    if(despacho000 && juzgadoNombre && !juzgadoNombre.includes('JUZGADO')){
                                        vm.despachoId = despacho000.id;
                                    }
                                }
                            }).finally(function () {
                                vm.cargandoDespachos = false;
                            });
                        }, 300);
                    }
                },
                onItemRemove: function () {
                    vm.despachoId = null;
                    vm.opcionesDespacho = [];
                    $timeout(function () {
                        vm.limpiarOpcionesDespacho = true;
                    }, 300);
                }
            };
            vm.opcionesJuzgado = [];
        }

        function initAutocompleteDespacho() {
            vm.configDespacho = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                render: {
                    item: function(item, escape) {
                        return '<div>' +
                            '<span>' + escape(item.despacho) + '</span>' +
                            '</div>';
                    },
                    option: function(item, escape) {
                        return '<div>' +
                            '<span>' + escape(item.despacho) + '</span>' +
                            '</div>';
                    }
                }
            };
            vm.opcionesDespacho = [];
        }

        function initAutocompleteResponsable() {
            vm.configResponsable = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
            };
            vm.opcionesResponsable = [];
            genericService.obtenerColeccionLigera("usuarios",{
                ligera: true,
            })
            .then(function (response){
                vm.opcionesResponsable = [].concat(response.data);
            });
        }

        function initAutocompleteClaseProceso() {
            vm.configClaseProceso = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
            };
            vm.opcionesClaseProceso = [];
            genericService.obtenerColeccionLigera("clases-de-procesos",{
                ligera: true,
            })
            .then(function (response){
                vm.opcionesClaseProceso = [].concat(response.data);
            });
        }

        function initAutocompleteEtapaProceso() {
            vm.configEtapaProceso = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
            };
            vm.opcionesEtapaProceso = [];
            genericService.obtenerColeccionLigera("etapas-de-procesos",{
                ligera: true,
            })
            .then(function (response){
                vm.opcionesEtapaProceso = [].concat(response.data);
            });
        }

        function initAutocompleteArea() {
            vm.configArea = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true
            };
            vm.opcionesArea = [];
            genericService.obtenerColeccionLigera("areas",{
                ligera: true
            })
            .then(function (response){
                vm.opcionesArea = [].concat(response.data);
            });
        }

        function initAutocompleteCliente() {
            vm.configCliente = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true
            };
            vm.opcionesCliente = [];
            genericService.obtenerColeccionLigera("clientes",{
                ligera: true
            })
            .then(function (response){
                vm.opcionesCliente = [].concat(response.data);
            });
        }

        function initAutocompleteTipoActuacion() {
            vm.configTipoActuacion = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true
            };
            vm.opcionesTipoActuacion = [];
            genericService.obtenerColeccionLigera("tipos-actuaciones",{
                ligera: true
            })
            .then(function (response){
                vm.opcionesTipoActuacion = [].concat(response.data);
            });
        }

    }
})();
