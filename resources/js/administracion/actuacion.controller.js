(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('actuacionController', actuacionController);

    actuacionController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        '$uibModal',
        'sessionUtil',
        'envService',
        'genericService',
        'langUtil',
        'messageUtil',
        'Constantes'
    ];

    function actuacionController(
        $scope, $routeParams, $timeout, $uibModal, sessionUtil, envService, genericService,
        langUtil, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "actuaciones";

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.crear = crear;
        vm.seleccionar = seleccionar;
        vm.seleccionarTodo = seleccionarTodo;

        // Modulos externos
        vm.confirmarEliminar = confirmarEliminar;
        vm.confirmarEliminarMasivo = confirmarEliminarMasivo;

        init();

        function init() {
            vm.coleccion = [];
            vm.paginacion = {};
            vm.itemsPorPagina = 25;
            $scope.isReset = true;
            vm.titulo = sessionUtil.getTituloVista($routeParams.o);

            // inicial autocomples
            initAutocompleteDepartamento();
            initAutocompleteCiudad();
            initAutocompleteJuzgado();
            initAutocompleteDespacho();
            initAutocompleteTipoNotificacion();
        }

        // Grid

        function obtenerResumen(tableState, ctrl){
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }
            if(pipePromise) $timeout.cancel(pipePromise);
            pipePromise = $timeout(function() {
                if(!vm.cargando){
                    vm.cargando = true;
                    genericService.obtenerColeccion(recurso, tableState)
                        .then(function (response){
                            if(response.status === Constantes.Response.HTTP_OK){
                                var coleccion = response.data.datos;
                                if(coleccion && coleccion.length > 0 ){
                                    angular.forEach(coleccion, function (row) {
                                        row.seleccionado = vm.todoSeleccionado;
                                    });
                                    vm.coleccion = [].concat(coleccion);
                                    vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                                    vm.paginacion.paginaActual = response.data.pagina_actual;
                                    vm.paginacion.desde = response.data.desde;
                                    vm.paginacion.hasta = response.data.hasta;
                                    vm.paginacion.total = response.data.total;
                                    tableState.pagination.numberOfPages = response.data.ultima_pagina;
                                }else{
                                    vm.coleccion = [];
                                }
                            }
                        }).finally(function () {
                            vm.cargando = false;
                        });
                }
            }, 500);
        }

        function limpiarFiltros(){
            vm.filtroDepartamentoId = null;
            vm.filtroCiudadId = null;
            vm.filtroJuzgadoId = null;
            vm.filtroDespachoId = null;
            vm.filtroNumeroRadicadoActual = null;
            vm.filtroFechaDesde = null;
            vm.filtroFechaHasta = null;
            vm.filtroTipoNotificacionId = null;
            $scope.isReset = true;
        }

        // Acciones

        function eliminar(row){
            genericService.eliminar(recurso, row.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        $scope.$broadcast('refreshGrid');
                        messageUtil.success(response.data.mensajes[0]);
                    }else if(
                        response.status === Constantes.Response.HTTP_CONFLICT ||
                        response.status === Constantes.Response.HTTP_BAD_REQUEST
                    ){
                        messageUtil.error(response.data.mensajes[0]);
                    }
                });
        }

        function eliminarMasivo(){
            var actuacionIds = [];
            angular.forEach(vm.coleccion, function (row) {
                if(row.seleccionado){
                    actuacionIds.push(row.id);
                }
            });
            genericService.modificar(recurso + "/todo/eliminar", {
                filtro_todo: !!vm.todoSeleccionado,
                filtro_actuacion_ids: !vm.todoSeleccionado ? actuacionIds : null,
                filtro_departamento_id: langUtil.isNotBlank(vm.filtroDepartamentoId) ? vm.filtroDepartamentoId : null,
                filtro_ciudad_id: langUtil.isNotBlank(vm.filtroCiudadId) ? vm.filtroCiudadId : null,
                filtro_juzgado_id: langUtil.isNotBlank(vm.filtroJuzgadoId) ? vm.filtroJuzgadoId : null,
                filtro_despacho_id: langUtil.isNotBlank(vm.filtroDespachoId) ? vm.filtroDespachoId : null,
                filtro_numero_radicado_actual:
                    langUtil.isNotBlank(vm.filtroNumeroRadicadoActual) ? vm.filtroNumeroRadicadoActual : null,
                filtro_fecha_desde: langUtil.isNotBlank(vm.filtroFechaDesde) ? vm.filtroFechaDesde : null,
                filtro_fecha_hasta: langUtil.isNotBlank(vm.filtroFechaHasta) ? vm.filtroFechaHasta : null,
                filtro_tipo_notificacion_id:
                    langUtil.isNotBlank(vm.filtroTipoNotificacionId) ? vm.filtroTipoNotificacionId : null
            })
            .then(function(response){
                if(response.status === Constantes.Response.HTTP_OK){
                    vm.todoSeleccionado = false;
                    $scope.$broadcast('refreshGrid');
                    messageUtil.success(response.data.mensajes[0]);
                }else if(
                    response.status === Constantes.Response.HTTP_CONFLICT ||
                    response.status === Constantes.Response.HTTP_BAD_REQUEST
                ){
                    messageUtil.error(response.data.mensajes[0]);
                }
            });
        }

        function seleccionar(row) {
            if(!row.seleccionado && vm.todoSeleccionado){
                vm.todoSeleccionado = false;
            }
        }

        function seleccionarTodo() {
            angular.forEach(vm.coleccion, function (actuacion) {
                actuacion.seleccionado = vm.todoSeleccionado;
            });
        }

        // Modulos externos

        function crear(actuacion) {
            var conError = false;
            if(!actuacion){
                var key, mensaje,
                    departamentoId = null, departamentoNombre = null, departamentoCodigoDane = null,
                    ciudadId = null, ciudadNombre = null, ciudadCodigoDane = null,
                    juzgadoId = null, juzgadoNombre = null, juzgadoNumero = null,
                    despachoId = null, despachoNumero = null, tipoNotificacionNombre = null,
                    coleccion = _.filter(vm.coleccion, function (row) {
                        return row.seleccionado;
                    });
                if(!(coleccion && coleccion.length > 0)){
                    conError = true;
                    mensaje = "Debe seleccionar al menos una actuación.";
                }
                var departamentos = _.groupBy(coleccion, 'departamento_id');
                if(departamentos && langUtil.getSize(departamentos) === 1){
                    for (key in departamentos) {
                        departamentoId = departamentos[key][0].departamento_id;
                        departamentoNombre = departamentos[key][0].departamento_nombre;
                        departamentoCodigoDane = departamentos[key][0].departamento_codigo_dane;
                    }
                    var ciudades = _.groupBy(coleccion, 'ciudad_id');
                    if(ciudades && langUtil.getSize(ciudades) === 1){
                        for (key in ciudades) {
                            ciudadId = ciudades[key][0].ciudad_id;
                            ciudadNombre = ciudades[key][0].ciudad_nombre;
                            ciudadCodigoDane = ciudades[key][0].ciudad_codigo_dane;
                        }
                        var juzgados = _.groupBy(coleccion, 'juzgado_id');
                        if(juzgados && langUtil.getSize(juzgados) === 1){
                            for (key in juzgados) {
                                juzgadoId = juzgados[key][0].juzgado_id;
                                juzgadoNombre = juzgados[key][0].juzgado_nombre;
                                juzgadoNumero = juzgados[key][0].juzgado_numero;
                            }
                            var despachos = _.groupBy(coleccion, 'despacho_id');
                            if(despachos && langUtil.getSize(despachos) === 1){
                                for (key in despachos) {
                                    despachoId = despachos[key][0].despacho_id;
                                    despachoNumero = despachos[key][0].despacho_numero;
                                }
                            }else{
                                despachoNumero = "VARIOS";
                                conError = true;
                                mensaje = "Para modificación masiva de información se debe seleccionar solo un despacho.";
                            }
                        }else{
                            juzgadoNombre = "VARIOS";
                            despachoNumero = "VARIOS";
                            conError = true;
                            mensaje = "Para modificación masiva de información se debe seleccionar solo un juzgado.";
                        }
                    }else{
                        ciudadNombre = "VARIOS";
                        juzgadoNombre = "VARIOS";
                        despachoNumero = "VARIOS";
                    }
                }else{
                    departamentoNombre = "VARIOS";
                    ciudadNombre = "VARIOS";
                    juzgadoNombre = "VARIOS";
                    despachoNumero = "VARIOS";
                }
                var tiposNotificaciones = _.groupBy(coleccion, 'departamento_id');
                if(tiposNotificaciones && langUtil.getSize(tiposNotificaciones) === 1){
                    for (key in tiposNotificaciones) {
                        tipoNotificacionNombre = tiposNotificaciones[key][0].tipo_notificacion_nombre;
                    }
                }else{
                    tipoNotificacionNombre = "VARIOS";
                }
            }

            if(!conError){
                var actuacionIds = [];
                angular.forEach(vm.coleccion, function (row) {
                    if(row.seleccionado){
                        actuacionIds.push(row.id);
                    }
                });

                var modalInstance = $uibModal.open({
                    animation: true,
                    backdrop: 'static',
                    templateUrl: envService.read('apiUrl') + recurso + '/create',
                    controller: 'actuacionEditController as vm',
                    size: 'lg',
                    resolve: {
                        actuacion: function () {
                            return actuacion;
                        },
                        parametros: function () {
                            return !actuacion ? {
                                masivo: true,
                                todo: !!vm.todoSeleccionado,
                                actuacion_ids: !vm.todoSeleccionado ? actuacionIds : null,
                                filtro_departamento_id: vm.filtroDepartamentoId,
                                filtro_ciudad_id: vm.filtroCiudadId,
                                filtro_juzgado_id: vm.filtroJuzgadoId,
                                filtro_despacho_id: vm.filtroDespachoId,
                                filtro_numero_radicado_actual: vm.filtroNumeroRadicadoActual,
                                filtro_fecha_desde: vm.filtroFechaDesde,
                                filtro_fecha_hasta: vm.filtroFechaHasta,
                                filtro_tipo_notificacion_id: vm.filtroTipoNotificacionId,
                                departamento_id: departamentoId,
                                departamento_nombre: departamentoNombre,
                                departamento_codigo_dane: departamentoCodigoDane,
                                ciudad_id: ciudadId,
                                ciudad_nombre: ciudadNombre,
                                ciudad_codigo_dane: ciudadCodigoDane,
                                juzgado_id: juzgadoId,
                                juzgado_nombre: juzgadoNombre,
                                juzgado_numero: juzgadoNumero,
                                despacho_id: despachoId,
                                despacho_numero: despachoNumero,
                                tipo_notificacion_nombre: tipoNotificacionNombre
                            } : null;
                        }
                    }
                });
                modalInstance.result.then(function (response) {
                    vm.todoSeleccionado = false;
                    $scope.$broadcast('refreshGrid');
                });
            }

            if(conError){
                messageUtil.error(mensaje);
            }
        }

        function confirmarEliminar(row) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + 'confirmar-template',
                controller: 'confirmarController as vm',
                size: 'md',
                resolve: {
                    parametros: function () {
                        return {
                            mensaje: "¿Estás seguro de que quieres eliminar la actuación?"
                        };
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(response.ok){
                    eliminar(row);
                }
            });
        }

        function confirmarEliminarMasivo() {
            var conError = false,
                coleccion = _.filter(vm.coleccion, function (row) {
                    return row.seleccionado;
                });
            if(!(coleccion && coleccion.length > 0)){
                conError = true;
            }

            if(!conError){
                var modalInstance = $uibModal.open({
                    animation: true,
                    backdrop: 'static',
                    templateUrl: envService.read('apiUrl') + 'confirmar-template',
                    controller: 'confirmarController as vm',
                    size: 'md',
                    resolve: {
                        parametros: function () {
                            return {
                                mensaje: "¿Está seguro que desea eliminar la actuaciones seleccionadas?"
                            };
                        }
                    }
                });
                modalInstance.result.then(function (response) {
                    if(response.ok){
                        eliminarMasivo();
                    }
                });
            }else{
                messageUtil.error("Debe seleccionar al menos una actuación.");
            }
        }

        // Autocompletes

        function initAutocompleteDepartamento() {
            vm.configDepartamento = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                render: {
                    item: function(item, escape) {
                        return '<div>' +
                            '<span>' + escape(item.nombre) + '</span>' +
                            '<span> - ' + escape(item.codigo_dane) + '</span>' +
                            '</div>';
                    },
                    option: function(item, escape) {
                        return '<div>' +
                            '<span>' + escape(item.nombre) + '</span>' +
                            '<span> - ' + escape(item.codigo_dane) + '</span>' +
                            '</div>';
                    }
                },
                onItemAdd: function (value, $item) {
                    if(!vm.cargandoCiudades){
                        vm.cargandoCiudades = true;
                        vm.filtroDespachoId = null;
                        vm.filtroJuzgadoId = null;
                        vm.filtroCiudadId = null;
                        vm.opcionesDespacho = [];
                        vm.opcionesJuzgado = [];
                        vm.opcionesCiudad = [];
                        $timeout(function () {
                            vm.limpiarOpcionesDespacho = true;
                            vm.limpiarOpcionesJuzgado = true;
                            vm.limpiarOpcionesCiudad = true;
                            genericService.obtenerColeccionLigera("ciudades", {
                                ligera: true,
                                departamento_id: value
                            }).then(function (response){
                                vm.opcionesCiudad = [].concat(response.data);
                            }).finally(function () {
                                vm.cargandoCiudades = false;
                            });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.filtroDespachoId = null;
                    vm.filtroJuzgadoId = null;
                    vm.filtroCiudadId = null;
                    vm.opcionesDespacho = [];
                    vm.opcionesJuzgado = [];
                    vm.opcionesCiudad = [];
                    $timeout(function () {
                        vm.limpiarOpcionesDespacho = true;
                        vm.limpiarOpcionesJuzgado = true;
                        vm.limpiarOpcionesCiudad = true;
                    }, 300);
                }
            };
            vm.opcionesDepartamento = [];
            genericService.obtenerColeccionLigera("departamentos",{
                ligera: true,
            })
            .then(function (response){
                vm.opcionesDepartamento = [].concat(response.data);
            });
        }

        function initAutocompleteCiudad() {
            vm.configCiudad = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                render: {
                    item: function(item, escape) {
                        return '<div>' +
                            '<span>' + escape(item.nombre) + '</span>' +
                            '<span> - ' + escape(item.codigo_dane) + '</span>' +
                            '</div>';
                    },
                    option: function(item, escape) {
                        return '<div>' +
                            '<span>' + escape(item.nombre) + '</span>' +
                            '<span> - ' + escape(item.codigo_dane) + '</span>' +
                            '</div>';
                    }
                },
                onItemAdd: function (value, $item) {
                    if(!vm.cargandoJuzgados){
                        vm.cargandoJuzgados = true;
                        vm.filtroDespachoId = null;
                        vm.filtroJuzgadoId = null;
                        vm.opcionesDespacho = [];
                        vm.opcionesJuzgado = [];
                        $timeout(function () {
                            vm.limpiarOpcionesDespacho = true;
                            vm.limpiarOpcionesJuzgado = true;
                            genericService.obtenerColeccionLigera("juzgados", {
                                ligera: true,
                                ciudad_id: value
                            }).then(function (response){
                                vm.opcionesJuzgado = [].concat(response.data);
                            }).finally(function () {
                                vm.cargandoJuzgados = false;
                            });
                        }, 300);
                    }
                },
                onItemRemove: function () {
                    vm.filtroDespachoId = null;
                    vm.filtroJuzgadoId = null;
                    vm.opcionesDespacho = [];
                    vm.opcionesJuzgado = [];
                    $timeout(function () {
                        vm.limpiarOpcionesDespacho = true;
                        vm.limpiarOpcionesJuzgado = true;
                    }, 300);
                }
            };
            vm.opcionesCiudad = [];
        }

        function initAutocompleteJuzgado() {
            vm.configJuzgado = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                render: {
                    item: function(item, escape) {
                        return '<div>' +
                            '<span>' + escape(item.nombre) + '</span>' +
                            '<span> - ' + escape(item.juzgado) + escape(item.sala) + '</span>' +
                            '</div>';
                    },
                    option: function(item, escape) {
                        return '<div>' +
                            '<span>' + escape(item.nombre) + '</span>' +
                            '<span> - ' + escape(item.juzgado) + escape(item.sala) + '</span>' +
                            '</div>';
                    }
                },
                onItemAdd: function (value, $item) {
                    if(!vm.cargandoDespachos){
                        vm.cargandoDespachos = true;
                        vm.filtroDespachoId = null;
                        vm.opcionesDespacho = [];
                        $timeout(function () {
                            vm.limpiarOpcionesDespacho = true;
                            genericService.obtenerColeccionLigera("despachos", {
                                ligera: true,
                                ciudad_id: vm.filtroCiudadId ? vm.filtroCiudadId : null,
                                juzgado_id: value
                            }).then(function (response){
                                vm.opcionesDespacho = [].concat(response.data);
                            }).finally(function () {
                                vm.cargandoDespachos = false;
                            });
                        }, 300);
                    }
                },
                onItemRemove: function () {
                    vm.filtroDespachoId = null;
                    vm.opcionesDespacho = [];
                    $timeout(function () {
                        vm.limpiarOpcionesDespacho = true;
                    }, 300);
                }
            };
            vm.opcionesJuzgado = [];
        }

        function initAutocompleteDespacho() {
            vm.configDespacho = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                render: {
                    item: function(item, escape) {
                        return '<div>' +
                            '<span>' + escape(item.despacho) + '</span>' +
                            '</div>';
                    },
                    option: function(item, escape) {
                        return '<div>' +
                            '<span>' + escape(item.despacho) + '</span>' +
                            '</div>';
                    }
                }
            };
            vm.opcionesDespacho = [];
        }

        function initAutocompleteTipoNotificacion() {
            vm.configTipoNotificacion = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true
            };
            vm.opcionesTipoNotificacion = [];
            genericService.obtenerColeccionLigera("tipos-notificaciones",{
                ligera: true,
            }).then(function (response){
                vm.opcionesTipoNotificacion = [].concat(response.data);
            });
        }

    }
})();
