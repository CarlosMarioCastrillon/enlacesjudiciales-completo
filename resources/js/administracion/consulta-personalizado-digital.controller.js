(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('consultaPersonalizadoDigitalController', consultaPersonalizadoDigitalController);

    consultaPersonalizadoDigitalController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function consultaPersonalizadoDigitalController(
        $scope, $routeParams, $timeout, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            today = new Date(),
            fechaInicial = (today.getFullYear()-1) + '-' + (today.getMonth() + 1) + '-' + today.getDate(),
            fechaFinal = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();

        vm.filtroFechaInicial = fechaInicial;
        vm.filtroFechaFinal = fechaFinal;

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 5;
        $scope.isReset = true;
        vm.activarBuscar = false;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid

        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones

        vm.buscar = buscar;
        vm.obtenerExcel = obtenerExcel;
        vm.obtenerArchivoAnexo = obtenerArchivoAnexo;

        // Instanciar autocomples

        initAutocompleteDepartamento();
        initAutocompleteCiudad();
        initAutocompleteJuzgado();
        initAutocompleteDespacho();
        initAutocompleteTipoNotificacion();

        // Limpiar filtros
        limpiarFiltros();

        function buscar() {
            vm.buscando = true;
            if(vm.form.$valid) {
                vm.activarBuscar = true;
                $scope.$broadcast('refreshGrid');
            }
        }

        function obtenerResumen(tableState, ctrl){
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }

            if(vm.paginacion && vm.paginacion.paginaActual && tableState.pagination){
                var start = tableState.pagination.start || 0,
                    number = tableState.pagination.number || 10,
                    nextPage = (start + number) / number;
                if(vm.paginacion.paginaActual != nextPage){
                    vm.activarBuscar = true;
                }else if(vm.activarBuscar){
                    tableState.pagination.start = 0;
                    tableState.pagination.number = vm.itemsPorPagina;
                }
            }

            if(vm.activarBuscar && !vm.cargando){
                vm.cargando = true;
                genericService.obtenerColeccion("remanentes", tableState).then(function (response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var coleccion = response.data.datos;
                        if(coleccion && coleccion.length > 0 ){
                            vm.coleccion = [].concat(coleccion);
                            vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                            vm.paginacion.paginaActual = response.data.pagina_actual;
                            vm.paginacion.desde = response.data.desde;
                            vm.paginacion.hasta = response.data.hasta;
                            vm.paginacion.total = response.data.total;
                            tableState.pagination.numberOfPages = response.data.ultima_pagina;
                        }else{
                            vm.coleccion = [];
                        }
                    }
                }).finally(function () {
                    vm.buscando = false;
                    vm.cargando = false;
                    vm.activarBuscar = false;
                });
            }
        }

        function limpiarFiltros(){
            vm.filtroDepartamento = null;
            vm.filtroCiudad = null;
            vm.filtroJuzgado = null;
            vm.filtroDespacho = null;
            vm.filtroTipoNotificacion = null;
            vm.filtroRadicado = null;
            vm.filtroTipoProceso = null;
            vm.filtroDetalle = null;
            vm.filtroFechaInicial = fechaInicial;
            vm.filtroFechaFinal = fechaFinal;
            vm.coleccion = [];
        }

        function obtenerExcel() {
            if(vm.coleccion.length > 0) {
                genericService.obtenerArchivo("actuaciones/personalizados-digitales/excel", {
                    ciudad: vm.filtroCiudad,
                    juzgado: vm.filtroJuzgado,
                    despacho: vm.filtroDespacho,
                    tipo_notificacion: vm.filtroTipoNotificacion,
                    radicado: vm.filtroRadicado,
                    descripcion_clase_proceso: vm.filtroTipoProceso,
                    detalle: vm.filtroDetalle,
                    fecha_inicial: vm.filtroFechaInicial,
                    fecha_final: vm.filtroFechaFinal
                });
            }
        }

        function obtenerArchivoAnexo(row) {
            genericService.obtenerArchivo("actuaciones/" + row.id + "/archivo-anexo")
                .then(function (response) {
                    if(response.status === Constantes.Response.HTTP_NOT_FOUND){
                        messageUtil.error("No se encontro un archivo anexo.");
                    }else if(response.status === Constantes.Response.HTTP_FORBIDDEN){
                        messageUtil.error("No tiene permitido descargar este archivo.");
                    }
                });
        }

        // Instanciar autocompletes

        function initAutocompleteDepartamento() {
            vm.configDepartamento = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                onItemAdd: function (value, $item) {
                    if(vm.filtroDepartamento !== value){
                        vm.filtroCiudad = null;
                        vm.opcionesCiudad = [];
                        $timeout(function () {
                            vm.limpiarOpcionesCiudad = true;
                            genericService.obtenerColeccionLigera("ciudades", {
                                ligera: true,
                                departamento_id: value
                            })
                                .then(function (response){
                                    vm.opcionesCiudad = [].concat(response.data);
                                });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.opcionesCiudad = [];
                    vm.filtroCiudad = null;
                    $timeout(function () {
                        vm.limpiarOpcionesCiudad = true;
                    }, 100);
                }
            };

            vm.opcionesDepartamento = [];
            genericService.obtenerColeccionLigera("departamentos",{
                ligera: true,
            })
                .then(function (response){
                    vm.opcionesDepartamentos = [].concat(response.data);
                });
        }

        function initAutocompleteCiudad() {
            vm.configCiudad = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                lock: false,
                onItemAdd: function (value, $item) {
                    if(vm.filtroCiudad !== value){
                        vm.opcionesJuzgado = [];
                        vm.opcionesDespacho = [];
                        vm.opcionesProceso = [];
                        vm.filtroJuzgado = null;
                        vm.filtroDespacho = null;
                        vm.filtroProceso = null;
                        $timeout(function () {
                            vm.limpiarOpcionesJuzgado = true;
                            vm.limpiarOpcionesDespacho = true;
                            vm.limpiarOpcionesProceso = true;
                            vm.filtroJuzgado = null;
                            vm.filtroDespacho = null;
                            vm.filtroProceso = null;
                            genericService.obtenerColeccionLigera("juzgados", {
                                ligera: true,
                                ciudad_id: value
                            })
                                .then(function (response){
                                    vm.opcionesJuzgado = [].concat(response.data);
                                });
                            genericService.obtenerColeccionLigera("procesos-judiciales", {
                                ligera: true,
                                ciudad_id: value,
                                limite: 100
                            })
                                .then(function (response){
                                    vm.opcionesProceso = [].concat(response.data);
                                });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.opcionesJuzgado = [];
                    vm.opcionesDespacho = [];
                    vm.opcionesProceso = [];
                    vm.filtroJuzgado = null;
                    vm.filtroDespacho = null;
                    vm.filtroProceso = null;
                    $timeout(function () {
                        vm.limpiarOpcionesJuzgado = true;
                        vm.limpiarOpcionesDespacho = true;
                        vm.limpiarOpcionesProceso = true;
                    }, 100);
                }
            };
            vm.opcionesCiudad = [];
        }

        function initAutocompleteJuzgado() {
            vm.configJuzgado = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                onItemAdd: function (value, $item) {
                    if(vm.filtroJuzgado !== value){
                        vm.opcionesDespacho = [];
                        vm.filtroDespacho = null;
                        $timeout(function () {
                            vm.limpiarOpcionesDespacho = true;
                            genericService.obtenerColeccionLigera("despachos", {
                                ligera: true,
                                juzgado_id: value
                            })
                                .then(function (response){
                                    vm.opcionesDespacho = [].concat(response.data);
                                });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.opcionesDespacho = [];
                    vm.filtroDespacho = null;
                    $timeout(function () {
                        vm.limpiarOpcionesDespacho = true;
                    }, 100);
                }
            };

            vm.opcionesJuzgado = [];
        }

        function initAutocompleteDespacho() {
            vm.configDespacho = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,

            };

            vm.opcionesDespacho = [];
        }

        function initAutocompleteTipoNotificacion() {
            vm.configTipoNotificacion = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true
            };
            vm.opcionesTipoNotificacion = [];
            genericService.obtenerColeccionLigera("tipos-notificaciones",{
                ligera: true,
                // empresa_id: empresaId
            })
                .then(function (response){
                    vm.opcionesTipoNotificacion = [].concat(response.data);
                });
        }
    }
})();
