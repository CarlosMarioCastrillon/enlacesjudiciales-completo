(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('diligenciaHistoriaEditController', diligenciaHistoriaEditController);

    diligenciaHistoriaEditController.$inject = [
        '$translate',
        '$timeout',
        '$routeParams',
        'genericService',
        'messageUtil',
        'Upload',
        'langUtil',
        'Constantes'
    ];

    function diligenciaHistoriaEditController(
        $translate, $timeout, $routeParams, genericService, messageUtil, Upload, langUtil, Constantes
    ){
        var vm = this,
            diligenciaId = $routeParams.diligencia_id,
            diligenciaHistoriaId = $routeParams.id;

        vm.EstadoDiligenciaEnum = Constantes.EstadoDiligencia;

        // Acciones
        vm.volver = volver;

        // Acciones archivos
        vm.descargarArchivoDiligencia = descargarArchivoDiligencia;
        vm.descargarArchivoCostos = descargarArchivoCostos;
        vm.descargarArchivoRecibido = descargarArchivoRecibido;

        // Carga inicial
        cargar();

        // Acciones

        function cargar() {
            genericService.cargar('diligencias/' + diligenciaId + '/historia', diligenciaHistoriaId)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var diligenciaHistoria = response.data,
                            ciudad = diligenciaHistoria.ciudad,
                            departamento = diligenciaHistoria.departamento,
                            tipoDiligencia = diligenciaHistoria.tipo_diligencia,
                            empresa = diligenciaHistoria.empresa,
                            proceso = diligenciaHistoria.proceso_judicial;

                        vm.numeroDiligencia = diligenciaId;
                        vm.secuencia = diligenciaHistoria.secuencia;
                        vm.empresa = empresa.nombre;
                        vm.numeroProceso = proceso.numero_proceso;
                        vm.claseProceso = proceso.clase_proceso;
                        vm.demandante = proceso.demandante;
                        vm.demandado = proceso.demandado;
                        vm.juzgado = proceso.juzgado;
                        vm.departamento = departamento.nombre;
                        vm.ciudad = ciudad.nombre;
                        vm.fechaDiligencia = diligenciaHistoria.fecha_diligencia;
                        vm.tipoDiligencia = tipoDiligencia.nombre;
                        vm.estadoDiligencia = diligenciaHistoria.estado_diligencia;
                        vm.fechaModificacion = diligenciaHistoria.fecha_modificacion;
                        vm.solicitudAutorizacion = diligenciaHistoria.solicitud_autorizacion;
                        vm.indicativoCobro = diligenciaHistoria.indicativo_cobro;
                        vm.observacionesCliente = diligenciaHistoria.observaciones_cliente;
                        vm.observacionesInternas = diligenciaHistoria.observaciones_internas;
                        vm.fechaCreacion = diligenciaHistoria.fecha_creacion;
                        vm.usuarioCreacionNombre = diligenciaHistoria.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = diligenciaHistoria.usuario_modificacion_nombre;

                        vm.valorDiligencia = diligenciaHistoria.valor_diligencia ? diligenciaHistoria.valor_diligencia : 0;
                        vm.gastoEnvio = diligenciaHistoria.gastos_envio ? diligenciaHistoria.gastos_envio : 0;
                        vm.otrosGastos = diligenciaHistoria.otros_gastos ? diligenciaHistoria.otros_gastos : 0;
                        vm.costoDiligencia = diligenciaHistoria.costo_diligencia ? diligenciaHistoria.costo_diligencia : 0;
                        vm.costoEnvio = diligenciaHistoria.costo_envio ? diligenciaHistoria.costo_envio : 0;
                        vm.otrosCostos = diligenciaHistoria.otros_costos ? diligenciaHistoria.otros_costos : 0;

                        // Cargar achivo
                        vm.archivoDiligencia = diligenciaHistoria.archivo_anexo_diligencia;
                        vm.archivoCostos = diligenciaHistoria.archivo_anexo_costos;
                        vm.archivoRecibido = diligenciaHistoria.archivo_anexo_recibido;
                    }
                });
        }

        function volver() {
            window.history.back();
        }

        // Acciones archivos

        function descargarArchivoDiligencia() {
            genericService.obtenerArchivo('diligencias/' + diligenciaId + '/historia/' + diligenciaHistoriaId + "/anexo-diligencia")
            .then(function (response) {
                if(response.status === Constantes.Response.HTTP_NOT_FOUND){
                    messageUtil.error("No se encontro un archivo anexo.");
                }else if(response.status === Constantes.Response.HTTP_FORBIDDEN){
                    messageUtil.error("No tiene permitido descargar este archivo.");
                }
            });
        }

        function descargarArchivoCostos() {
            genericService.obtenerArchivo('diligencias/' + diligenciaId + '/historia/' + diligenciaHistoriaId + "/anexo-costos")
            .then(function (response) {
                if(response.status === Constantes.Response.HTTP_NOT_FOUND){
                    messageUtil.error("No se encontro un archivo anexo.");
                }else if(response.status === Constantes.Response.HTTP_FORBIDDEN){
                    messageUtil.error("No tiene permitido descargar este archivo.");
                }
            });
        }

        function descargarArchivoRecibido() {
            genericService.obtenerArchivo('diligencias/' + diligenciaId + '/historia/' + diligenciaHistoriaId + "/anexo-recibido")
            .then(function (response) {
                if(response.status === Constantes.Response.HTTP_NOT_FOUND){
                    messageUtil.error("No se encontro un archivo anexo.");
                }else if(response.status === Constantes.Response.HTTP_FORBIDDEN){
                    messageUtil.error("No tiene permitido descargar este archivo.");
                }
            });
        }

    }
})();
