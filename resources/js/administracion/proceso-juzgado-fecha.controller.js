(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('procesoJuzgadoFechaController', procesoJuzgadoFechaController);

    procesoJuzgadoFechaController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function procesoJuzgadoFechaController(
        $scope, $routeParams, $timeout, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise;

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 5;
        $scope.isReset = true;
        vm.activarBuscar = false;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.buscar = buscar;
        vm.obtenerExcel = obtenerExcel;
        vm.obtenerArchivoAnexo = obtenerArchivoAnexo;

        // inicial autocomples
        initAutocompleteCiudad();
        initAutocompleteJuzgado();
        initAutocompleteDespacho();
        initAutocompleteProceso();

        // Limpiar filtros
        limpiarFiltros();

        // Grid

        function buscar() {
            vm.buscando = true;
            if(vm.form.$valid) {
                vm.activarBuscar = true;
                $scope.$broadcast('refreshGrid');
            }
        }

        function obtenerResumen(tableState, ctrl){
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }

            if(vm.paginacion && vm.paginacion.paginaActual && tableState.pagination){
                var start = tableState.pagination.start || 0,
                    number = tableState.pagination.number || 10,
                    nextPage = (start + number) / number;
                if(vm.paginacion.paginaActual != nextPage){
                    vm.activarBuscar = true;
                }else if(vm.activarBuscar){
                    tableState.pagination.start = 0;
                    tableState.pagination.number = vm.itemsPorPagina;
                }
            }

            if(vm.activarBuscar && !vm.cargando){
                vm.cargando = true;
                genericService.obtenerColeccion("actuaciones/reporte", tableState).then(function (response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var coleccion = response.data.datos;
                        if(coleccion && coleccion.length > 0 ){
                            vm.coleccion = [].concat(coleccion);
                            vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                            vm.paginacion.paginaActual = response.data.pagina_actual;
                            vm.paginacion.desde = response.data.desde;
                            vm.paginacion.hasta = response.data.hasta;
                            vm.paginacion.total = response.data.total;
                            tableState.pagination.numberOfPages = response.data.ultima_pagina;
                        }else{
                            vm.coleccion = [];
                        }
                    }
                }).finally(function () {
                    vm.buscando = false;
                    vm.cargando = false;
                    vm.activarBuscar = false;
                });
            }
        }


        function limpiarFiltros(){
            vm.filtroFecha = null;
            vm.filtroCiudad = null;
            vm.filtroJuzgado = null;
            vm.filtroDespacho = null;
            vm.filtroProceso = null;
            vm.coleccion = [];
        }

        // Acciones

        function obtenerExcel() {
            if(vm.coleccion.length > 0) {
                genericService.obtenerArchivo("actuaciones/procesos-juzgado-fecha/excel", {
                    fecha: vm.filtroFecha,
                    ciudad: vm.filtroCiudad,
                    juzgado: vm.filtroJuzgado,
                    despacho: vm.filtroDespacho,
                    proceso: vm.filtroProceso
                });
            }
        }

        function obtenerArchivoAnexo(row) {
            genericService.obtenerArchivo("actuaciones/" + row.id + "/archivo-anexo")
                .then(function (response) {
                    if(response.status === Constantes.Response.HTTP_NOT_FOUND){
                        messageUtil.error("No se encontro un archivo anexo.");
                    }else if(response.status === Constantes.Response.HTTP_FORBIDDEN){
                        messageUtil.error("No tiene permitido descargar este archivo.");
                    }
                });
        }

        function initAutocompleteCiudad() {
            vm.configCiudad = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                lock: false,
                onItemAdd: function (value, $item) {
                    if(vm.filtroCiudad !== value){
                        vm.opcionesJuzgado = [];
                        vm.opcionesDespacho = [];
                        vm.opcionesProceso = [];
                        vm.filtroJuzgado = null;
                        vm.filtroDespacho = null;
                        vm.filtroProceso = null;
                        $timeout(function () {
                            vm.limpiarOpcionesJuzgado = true;
                            vm.limpiarOpcionesDespacho = true;
                            vm.limpiarOpcionesProceso = true;
                            vm.filtroJuzgado = null;
                            vm.filtroDespacho = null;
                            vm.filtroProceso = null;
                            genericService.obtenerColeccionLigera("juzgados", {
                                ligera: true,
                                ciudad_id: value
                            })
                                .then(function (response){
                                    vm.opcionesJuzgado = [].concat(response.data);
                                });
                            genericService.obtenerColeccionLigera("procesos-judiciales", {
                                ligera: true,
                                ciudad_id: value,
                                limite: 100
                            })
                            .then(function (response){
                                vm.opcionesProceso = [].concat(response.data);
                            });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.opcionesJuzgado = [];
                    vm.opcionesDespacho = [];
                    vm.opcionesProceso = [];
                    vm.filtroJuzgado = null;
                    vm.filtroDespacho = null;
                    vm.filtroProceso = null;
                    $timeout(function () {
                        vm.limpiarOpcionesJuzgado = true;
                        vm.limpiarOpcionesDespacho = true;
                        vm.limpiarOpcionesProceso = true;
                    }, 100);
                }
            };
            vm.opcionesCiudad = [];
            genericService.obtenerColeccionLigera("ciudades", {
                ligera: true,
                con_cobertura: true
            })
            .then(function (response){
                vm.opcionesCiudad = [].concat(response.data);
            });
        }

        function initAutocompleteJuzgado() {
            vm.configJuzgado = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                onItemAdd: function (value, $item) {
                    if(vm.filtroJuzgado !== value){
                        vm.opcionesDespacho = [];
                        vm.filtroDespacho = null;
                        $timeout(function () {
                            vm.limpiarOpcionesDespacho = true;
                            genericService.obtenerColeccionLigera("despachos", {
                                ligera: true,
                                juzgado_id: value
                            })
                                .then(function (response){
                                    vm.opcionesDespacho = [].concat(response.data);
                                });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.opcionesDespacho = [];
                    vm.filtroDespacho = null;
                    $timeout(function () {
                        vm.limpiarOpcionesDespacho = true;
                    }, 100);
                }
            };

            vm.opcionesJuzgado = [];
        }

        function initAutocompleteDespacho() {
            vm.configDespacho = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
            };

            vm.opcionesDespacho = [];
        }

        function initAutocompleteProceso() {
            vm.configProceso = {
                valueField: 'id',
                labelField: 'numero_proceso',
                searchField: ['numero_proceso'],
                sortField: 'numero_proceso',
                allowEmptyOption: false,
                create: false,
                persist: true,
            };

            vm.opcionesProceso = [];
        }
    }
})();
