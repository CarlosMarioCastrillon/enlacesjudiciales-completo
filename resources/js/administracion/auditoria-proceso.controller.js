(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('auditoriaProcesoController', auditoriaProcesoController);

    auditoriaProcesoController.$inject = [
        '$scope',
        '$http',
        '$routeParams',
        '$timeout',
        'sessionUtil',
        'envService',
        'genericService',
        'Upload',
        'messageUtil',
        'Constantes'
    ];

    function auditoriaProcesoController(
        $scope, $http, $routeParams, $timeout, sessionUtil, envService, genericService, Upload, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "auditoria-procesos";

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.abrirFormulario = abrirFormulario;
        vm.programar = programar;
        vm.cancelar = cancelar;
        vm.descargarAuditoria = descargarAuditoria;
        vm.descargarNovedades = descargarNovedades;

        // Acciones archivos
        vm.cargarArchivo = cargarArchivo;
        vm.descargarArchivo = descargarArchivo;
        vm.eliminarArchivo = eliminarArchivo;

        // Limpiar filtros
        limpiarFiltros();

        // Grid

        function obtenerResumen(tableState, ctrl){
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }
            if(pipePromise) $timeout.cancel(pipePromise);
            pipePromise = $timeout(function() {
                if(!vm.cargando){
                    vm.cargando = true;
                    genericService.obtenerColeccion(recurso, tableState)
                        .then(function (response){
                            if(response.status === Constantes.Response.HTTP_OK){
                                var coleccion = response.data.datos;
                                if(coleccion && coleccion.length > 0 ){
                                    vm.coleccion = [].concat(coleccion);
                                    vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                                    vm.paginacion.paginaActual = response.data.pagina_actual;
                                    vm.paginacion.desde = response.data.desde;
                                    vm.paginacion.hasta = response.data.hasta;
                                    vm.paginacion.total = response.data.total;
                                    tableState.pagination.numberOfPages = response.data.ultima_pagina;
                                }else{
                                    vm.coleccion = [];
                                }
                            }
                        }).finally(function () {
                            vm.cargando = false;
                        });
                }
            }, 500);
        }

        function limpiarFiltros(){
            $scope.isReset = true;
        }

        // Acciones

        function abrirFormulario() {
            limpiarFormulario();
            vm.formularioVisible = !vm.formularioVisible;
        }

        function programar(){
            var conErrores = false, mensaje;
            if(!(vm.fechaProgramacion || vm.archivo)){
                conErrores = true;
                mensaje = "Debe indicar la fecha de programación o cargar un archivo.";
            }

            if(!conErrores && !vm.cargandoArchivo){
                vm.cargandoArchivo = true;
                var fd = new FormData();
                if(vm.fechaProgramacion){
                    fd.append('recientes', vm.recientes);
                    fd.append('fecha_programacion', vm.fechaProgramacion);
                }
                if(vm.fechaProgramacionPasada){
                    fd.append('fecha_programacion_pasada', vm.fechaProgramacionPasada);
                }
                if(vm.archivo){
                    fd.append('file', vm.archivo);
                }
                return $http.post(envService.read('apiUrl') + recurso, fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined}
                }).then(function (response) {
                    if(response.status === Constantes.Response.HTTP_CREATED){
                        // Recargar grid
                        limpiarFiltros();

                        // Limpiar formulario
                        limpiarFormulario();

                        // Carrar formulario
                        vm.formularioVisible = false;

                        // Mostrar mensaje de respuesta
                        messageUtil.success(response.data.mensajes[0]);
                    }
                }).finally(function () {
                    vm.cargandoArchivo = false;
                });
            }

            if(conErrores){
                messageUtil.error(mensaje);
            }
        }

        function cancelar() {
            limpiarFormulario();
            vm.formularioVisible = false;
        }

        function descargarAuditoria(row){
            genericService.obtenerArchivo('auditoria-procesos/excel', {
                programacion_id: row.id
            });
        }

        function descargarNovedades(row){
            genericService.obtenerArchivo('auditoria-procesos/novedades/excel', {
                programacion_id: row.id
            });
        }

        function limpiarFormulario() {
            eliminarArchivo();
            vm.recientes = true;
            vm.fechaProgramacion = null;
            vm.fechaProgramacionPasada = null;
        }

        // Acciones archivos

        function cargarArchivo($archivo) {
            Upload.dataUrl($archivo, true).then(function(base64) {
                if(base64){
                    vm.archivo = $archivo;
                    vm.archivoNombre = $archivo.name;
                    vm.archivoBase64 = base64;
                }
            });
        }

        function descargarArchivo() {
            var a = document.createElement("a");
            document.body.appendChild(a);
            a.style = "display: none";

            var blob = Upload.dataUrltoBlob(vm.archivoBase64, vm.archivoNombre),
                url = window.URL.createObjectURL(blob);

            a.href = url;
            a.download = vm.archivoNombre;
            a.click();
            window.URL.revokeObjectURL(url);
        }

        function eliminarArchivo() {
            vm.archivo = null;
            vm.archivoNombre = null;
            vm.archivoBase64 = null;
        }

    }
})();
