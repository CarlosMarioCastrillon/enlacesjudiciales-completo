(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('diligenciaClienteController', diligenciaClienteController);

    diligenciaClienteController.$inject = [
        '$translate',
        '$scope',
        '$timeout',
        '$routeParams',
        '$uibModal',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function diligenciaClienteController(
        $translate, $scope, $timeout, $routeParams, $uibModal, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            i18n = {},
            pipePromise,
            recurso = "diligencias";

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.EstadoDiligenciaEnum = Constantes.EstadoDiligencia;
        vm.activarBuscar = true;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Modulos externos
        vm.confirmarEliminar = confirmarEliminar;

        //Acciones
        vm.buscar = buscar;
        vm.obtenerExcel = obtenerConsultaExcel;


        // Acciones archivos
        vm.descargarArchivo = descargarArchivo;
        vm.descargarAnexoCostos = descargarAnexoCostos;
        vm.descargarAnexoRecibido = descargarAnexoRecibido;

        // Limpiar filtros
        limpiarFiltros();

        // Cargar traducciones
        cargarTraducciones();

        // Cargar autocompletes
        initAutocompleteEstadoDiligencia();

        // Grid

        function buscar() {
            vm.buscando = true;
            if(vm.form.$valid) {
                vm.activarBuscar = true;
                $scope.$broadcast('refreshGrid');
            }
        }

        function obtenerResumen(tableState, ctrl) {
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }

            if(vm.paginacion && vm.paginacion.paginaActual && tableState.pagination){
                var start = tableState.pagination.start || 0,
                    number = tableState.pagination.number || 10,
                    nextPage = (start + number) / number;
                if(vm.paginacion.paginaActual != nextPage){
                    vm.activarBuscar = true;
                }else if(vm.activarBuscar){
                    tableState.pagination.start = 0;
                    tableState.pagination.number = vm.itemsPorPagina;
                }
            }

            if(vm.activarBuscar && !vm.cargando){
                vm.cargando = true;
                genericService.obtenerColeccion(recurso + '/consulta-cliente', tableState).then(function (response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var coleccion = response.data.datos;
                        if(coleccion && coleccion.length > 0 ){
                            vm.coleccion = [].concat(coleccion);
                            vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                            vm.paginacion.paginaActual = response.data.pagina_actual;
                            vm.paginacion.desde = response.data.desde;
                            vm.paginacion.hasta = response.data.hasta;
                            vm.paginacion.total = response.data.total;
                            tableState.pagination.numberOfPages = response.data.ultima_pagina;
                        }else{
                            vm.coleccion = [];
                        }
                    }
                }).finally(function () {
                    vm.buscando = false;
                    vm.cargando = false;
                    vm.activarBuscar = false;
                });
            }
        }

        function obtenerConsultaExcel() {
            genericService.obtenerArchivo(recurso + "/consulta-cliente/excel", {
                numero_diligencia: vm.numeroDiligencia,
                numero_proceso: vm.proceso,
                estado_diligencia: vm.filterEstado,
                fecha_inicial: vm.filterFechaInicial,
                fecha_final: vm.filterFechaFinal,
            });
        }

        function limpiarFiltros(){
            vm.numeroDiligencia = null;
            vm.proceso = null;
            vm.filtroEstado = null;
            vm.filtroFechaInicial = null;
            vm.filtroFechaFinal = null;
            vm.activarBuscar = true;
            $scope.isReset = true;

        }

        /*function cargarFiltros() {
            var filtros = gridUtil.getFormattedSearchPredicate("diligencia");
            vm.nombre = filtros && filtros.nombre ? filtros.nombre : null;
        }*/

        // Acciones

        function eliminar(row){
            genericService.eliminar(recurso, row.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        $scope.$broadcast('refreshGrid');
                        location.reload();
                        messageUtil.success(response.data.mensajes[0]);
                    }else if(
                        response.status === Constantes.Response.HTTP_CONFLICT ||
                        response.status === Constantes.Response.HTTP_BAD_REQUEST
                    ){
                        messageUtil.error(response.data.mensajes[0]);
                    }
                });
        }

        function confirmarEliminar(row) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + 'confirmar-template',
                controller: 'confirmarController as vm',
                size: 'md',
                resolve: {
                    parametros: function () {
                        return {
                            mensaje: "¿Estás seguro de que quieres eliminar la tarifa?"
                        };
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(response.ok){
                    eliminar(row);
                }
            });
        }

        function descargarArchivo(row) {
            genericService.obtenerArchivo(recurso + "/" + row.id + "/anexo-diligencia")
                .then(function (response) {
                    if(response.status === Constantes.Response.HTTP_NOT_FOUND){
                        messageUtil.error("No se encontro un archivo anexo.");
                    }else if(response.status === Constantes.Response.HTTP_FORBIDDEN){
                        messageUtil.error("No tiene permitido descargar este archivo.");
                    }
                });
        }

        function descargarAnexoCostos(row) {
            genericService.obtenerArchivo(recurso + "/" + row.id + "/anexo-costos")
                .then(function (response) {
                    if(response.status === Constantes.Response.HTTP_NOT_FOUND){
                        messageUtil.error("No se encontro un archivo anexo.");
                    }else if(response.status === Constantes.Response.HTTP_FORBIDDEN){
                        messageUtil.error("No tiene permitido descargar este archivo.");
                    }
                });
        }

        function descargarAnexoRecibido(row) {
            genericService.obtenerArchivo(recurso + "/" + row.id + "/anexo-recibido")
                .then(function (response) {
                    if(response.status === Constantes.Response.HTTP_NOT_FOUND){
                        messageUtil.error("No se encontro un archivo anexo.");
                    }else if(response.status === Constantes.Response.HTTP_FORBIDDEN){
                        messageUtil.error("No tiene permitido descargar este archivo.");
                    }
                });
        }

        function initAutocompleteEstadoDiligencia() {
            vm.configEstadoDiligencia = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'id',
                allowEmptyOption: false,
                create: false,
                persist: true
            };
            vm.opcionesEstadoDiligencia = [];
        }

        // Traducciones

        function cargarTraducciones() {
            $translate([
                "SOLICITADO", "COTIZADO", "APROBADO", "TRAMITE", "TERMINADO", "CONTABILIZADO", "FACTURADO"
            ]).then(function (translations) {
                i18n.SOLICITADO = translations.SOLICITADO;
                i18n.COTIZADO = translations.COTIZADO;
                i18n.APROBADO = translations.APROBADO;
                i18n.TRAMITE = translations.TRAMITE;
                i18n.TERMINADO = translations.TERMINADO;
                i18n.CONTABILIZADO = translations.CONTABILIZADO;
                i18n.FACTURADO = translations.FACTURADO;

                vm.opcionesEstadoDiligencia = [
                    {id: Constantes.EstadoDiligencia.SOLICITADO, nombre: i18n.SOLICITADO},
                    {id: Constantes.EstadoDiligencia.COTIZADO, nombre: i18n.COTIZADO},
                    {id: Constantes.EstadoDiligencia.APROBADO, nombre: i18n.APROBADO},
                    {id: Constantes.EstadoDiligencia.TRAMITE, nombre: i18n.TRAMITE},
                    {id: Constantes.EstadoDiligencia.TERMINADO, nombre: i18n.TERMINADO},
                    {id: Constantes.EstadoDiligencia.CONTABILIZADO, nombre: i18n.CONTABILIZADO},
                    {id: Constantes.EstadoDiligencia.FACTURADO, nombre: i18n.FACTURADO}
                ];
            }, function (translationIds) {});
        }
    }
})();
