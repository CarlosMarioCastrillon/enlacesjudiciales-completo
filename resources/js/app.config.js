(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .factory('httpResponseInterceptor', ['$q', '$rootScope', '$location', 'Constantes', function($q, $rootScope, $location, Constantes) {
            return {
                responseError: function(rejection) {
                    if (rejection.status === Constantes.Response.HTTP_UNAUTHORIZED) {
                        localStorage.clear();
                        window.location = '/login';
                    }
                    return $q.reject(rejection);
                }
            };
        }]);

    angular
        .module('enlaces.app')
        .config(function(envServiceProvider) {
            // set the domains and variables for each environment
            envServiceProvider.config({
                domains: {
                    development: ['localhost', 'enlaces.test'],
                    production: ['pcl.legal', 'mle.legal'],
                    test: ['enlaces.assis.com.co'],
                    enlaces: ['litisdata.assis.com.co'],
                    litisdata: ['server2021.litisdata.com', 'litisdata.com']
                },
                vars: {
                    development: {
                        apiUrl: 'http://enlaces.test/api/'
                    },
                    production: {
                        apiUrl: 'https://enlaces.legal/api/'
                    },
                    enlaces: {
                        apiUrl: 'https://litisdata.assis.com.co/api/'
                    },
                    litisdata: {
                        apiUrl: 'https://server2021.litisdata.com/api/'
                    },
                    test: {
                        apiUrl: 'https://enlaces.assis.com.co/api/'
                    },
                    defaults: {
                        apiUrl: 'https://app.enlaces.com/api/'
                    }
                }
            });

            envServiceProvider.check();
        })
        .config(function($sceDelegateProvider) {
            $sceDelegateProvider.resourceUrlWhitelist([
                // Allow same origin resource loads.
                'self',
                // Allow loading from our assets domain. **.
                'https://enlaces.test/**',
                'https://pcl.legal/**',
                'https://test.pcl.legal/**'
            ])
        })
        .config(function($httpProvider, jwtOptionsProvider) {
            jwtOptionsProvider.config({
                authPrefix: localStorage.getItem('tokenType') + " ",
                tokenGetter: function() {
                    return localStorage.getItem('token');
                },
                whiteListedDomains: [
                    'localhost',
                    'enlaces.test',
                    'local.enlaces',
                    'pcl.legal',
                    'mle.legal',
                    'test.pcl.legal',
                    'test.mle.legal'
                ]
            });

            $httpProvider.interceptors.push('jwtInterceptor');
            $httpProvider.interceptors.push('httpResponseInterceptor');
        })
        .config(function(datetimepickerProvider) {
            datetimepickerProvider.setOptions({
                locale: 'en'
            });
        })
        .config(function(cfpLoadingBarProvider) {
            cfpLoadingBarProvider.includeSpinner  = false;
        })
        .config( function($translateProvider) {
            // TODO @Javier tomar de la configuración del usuario
            var locale = "es_a";

            $translateProvider
                .useStaticFilesLoader({
                    prefix: '/i18n/',
                    suffix: '.json'
                })
                .preferredLanguage(locale ? locale : "es_a")
                .useMissingTranslationHandlerLog();
        });

    angular
        .module('enlaces.app')
        .run(runBlock);

    runBlock.$inject = [
        '$http',
        '$rootScope',
        '$location',
        '$anchorScroll',
        'envService',
        'jwtHelper'
    ];

    function runBlock($http, $rootScope, $location, $anchorScroll, envService, jwtHelper) {
        /*$http.get(envService.read('apiUrl') + "users/current/session")
        .then(function (response){
            localStorage.setItem('session', JSON.stringify(response.data));
        })
        .catch(function (error){
            // TODO Darle manejo de errores de sessión
        });*/

        $rootScope.$on('$locationChangeStart', function() {
            var token = localStorage.getItem('token');
            var isTokenExpired = token ? jwtHelper.isTokenExpired(token) : true;
            if(isTokenExpired){
                localStorage.clear();
                window.location = '/login';
            }
        });

        $rootScope.$on('$routeChangeSuccess', function(newRoute, oldRoute) {
            if($location.hash()) $anchorScroll();
        });
    }

})();
