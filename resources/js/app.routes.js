(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .config(function($routeProvider, envServiceProvider) {
            $routeProvider
                .when('/', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'roles/index-template',
                    controller: 'rolController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                // Seguridad
                .when('/roles', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'roles/index-template',
                    controller: 'rolController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })
                .when('/usuarios', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'usuarios/index-template',
                    controller: 'usuarioController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })
                // Opciones del sistema
                .when('/opciones-del-sistema', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'opciones-del-sistema/index-template',
                    controller: 'opcionSistemaController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })
                // Permisos
                .when('/roles/:id/permisos', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'roles/null/permisos/create',
                    controller: 'permisoEditController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })
                .when('/auditoria-maestros', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'auditoria-maestros/index-template',
                    controller: 'auditoriaMaestroController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                // Configuración
                .when('/servicios', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'servicios/index-template',
                    controller: 'servicioController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/tipos-de-empresa', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'tipos-de-empresa/index-template',
                    controller: 'tipoDeEmpresaController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/clases-de-procesos', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'clases-de-procesos/index-template',
                    controller: 'claseProcesoController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/etapas-de-procesos', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'etapas-de-procesos/index-template',
                    controller: 'etapaProcesoController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/departamentos', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'departamentos/index-template',
                    controller: 'departamentoController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/etapas-de-la-clase-de-proceso', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'etapa-clase-procesos/index-template',
                    controller: 'etapaClaseProcesoController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/zonas', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'zonas/index-template',
                    controller: 'zonaController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/ciudades', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'ciudades/index-template',
                    controller: 'ciudadController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/juzgados', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'juzgados/index-template',
                    controller: 'juzgadoController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/tipos-de-documento', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'tipos-documentos/index-template',
                    controller: 'tipoDocumentoController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/despachos', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'despachos/index-template',
                    controller: 'despachoController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/empresas', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'empresas/index-template',
                    controller: 'empresaController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/empresas/:id/areas', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'areas/index-template',
                    controller: 'areaController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/empresas/areas', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'areas/index-template',
                    controller: 'areaController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/empresas/:id/planes', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'planes/index-template',
                    controller: 'planController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/conceptos-economicos', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'conceptos-economicos/index-template',
                    controller: 'conceptoEconomicoController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/copia-conceptos-economicos', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'copia-conceptos-economicos/index-template',
                    controller: 'copiaConceptoEconomicoController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/empresas/:empresaId/usuarios', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'usuarios/index-template',
                    controller: 'usuarioController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/tipos-de-diligencias', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'tipos-diligencias/index-template',
                    controller: 'tipoDiligenciaController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/empresas/:id/coberturas', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'coberturas/index-template',
                    controller: 'coberturaController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/tarifas-diligencias', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'tarifas-diligencias/index-template',
                    controller: 'tarifaDiligenciaController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/clientes', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'clientes/index-template',
                    controller: 'clienteController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/tipos-de-actuaciones', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'tipos-actuaciones/index-template',
                    controller: 'tipoActuacionController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })
                .when('/mis-procesos-judiciales', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'procesos-judiciales/index-template',
                    controller: 'procesoJudicialController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })
                .when('/mis-procesos-judiciales/:id/valores', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'procesos-valores/index-template',
                    controller: 'procesoValorController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })
                .when('/mis-procesos-judiciales/importacion', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'procesos-judiciales/importar-template',
                    controller: 'procesoJudicialImportarController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })
                .when('/actuaciones', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'actuaciones/index-template',
                    controller: 'actuacionController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })
                .when('/actuaciones/importacion', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'actuaciones/importar-template',
                    controller: 'actuacionImportarController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/usuarios-clientes/:id/procesos-cliente', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'procesos-clientes/index-template',
                    controller: 'procesoClienteController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/diligencias', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'diligencias/index-template',
                    controller: 'diligenciaController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/diligencias-tramite', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'diligencias/tramite/template',
                    controller: 'diligenciaController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/diligencias/:id/formulario', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'diligencias/create',
                    controller: 'diligenciaEditController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/diligencias-tramite/:id/formulario', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'diligencias/create',
                    controller: 'diligenciaEditController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/diligencias/:id/historia', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'diligencias/historia/template',
                    controller: 'diligenciaHistoriaController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })
                .when('/diligencias/:diligencia_id/historia/:id/formulario', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'diligencias/historia/create',
                    controller: 'diligenciaHistoriaEditController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/diligencias/cliente', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'diligencias/consulta-cliente/template',
                    controller: 'diligenciaClienteController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/diligencias/formulario-cliente', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'diligencias/form-cliente/template',
                    controller: 'diligenciaClienteEditController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/tipos-de-notificacion', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'tipos-notificaciones/index-template',
                    controller: 'tipoNotificacionController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/mis-procesos-en-vigilancia', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'vigilancias-procesos/index-template',
                    controller: 'vigilanciaProcesoController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/salario-minimo', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'salarios-minimos/index-template',
                    controller: 'salarioMinimoController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/TES', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'TES/index-template',
                    controller: 'tesController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/IPC', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'IPC/index-template',
                    controller: 'ipcController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/auxiliares-de-la-justicia', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'auxiliares-justicia/index-template',
                    controller: 'auxiliarJusticiaController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/auxiliares-de-la-justicia/importacion', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'auxiliares-justicia/importar-template',
                    controller: 'auxiliarJusticiaImportarController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/mis-procesos-hoy', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'actuaciones/procesos-hoy/template',
                    controller: 'procesoHoyController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/procesos-por-juzgado-fecha', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'actuaciones/procesos-juzgado-fecha/template',
                    controller: 'procesoJuzgadoFechaController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/buscar-demandante-demandado', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'actuaciones/demandantes-demandados/template',
                    controller: 'busquedaDemandanteDemandadoController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/procesos-por-area', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'procesos-areas/index-template',
                    controller: 'procesoAreaController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/informacion-general-procesos', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'procesos-judiciales/procesos-generales/template',
                    controller: 'procesoGeneralController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/consulta-autos', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'actuaciones/autos/template',
                    controller: 'consultaAutoController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/consulta-remates', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'remates/template',
                    controller: 'consultaRemateController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/consulta-remanentes', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'remanentes/template',
                    controller: 'consultaRemanenteController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/consulta-reporte-judicial', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'remanentes/template',
                    controller: 'consultaRemanenteController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/consulta-personalizado-digital', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'actuaciones/personalizados-digitales/template',
                    controller: 'consultaPersonalizadoDigitalController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/audiencia-pendiente', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'actuaciones/audiencias-pendientes/template',
                    controller: 'audienciaPendienteController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/audiencia-cumplida', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'actuaciones/audiencias-cumplidas/template',
                    controller: 'audienciaCumplidaController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/termino-actuacion', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'actuaciones/termino-actuacion/template',
                    controller: 'terminoActuacionController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/actuacion-cumplida', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'actuaciones/termino-actuacion/template',
                    controller: 'terminoActuacionController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/parametros-constantes', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'parametros-constantes/index-template',
                    controller: 'parametroConstanteController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/usuarios-clientes', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'usuarios/cliente/template',
                    controller: 'usuarioController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/auditoria-procesos', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'auditoria-procesos/index-template',
                    controller: 'auditoriaProcesoController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })


                .otherwise({redirectTo: '/'});
        });
})();
