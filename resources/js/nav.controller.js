(function(angular) {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('navController', navController);

    navController.$inject = [
        '$http',
        'envService',
        '$uibModal',
        'sessionUtil',
        'messageUtil',
        'Constantes'
    ];

    function navController($http, envService, $uibModal,  sessionUtil, messageUtil, Constants){
        var nav = this;

        nav.session = sessionUtil.getAll();

        // Acciones
        nav.logout = logout;
        nav.infoEmpresa = infoEmpresa;

        function logout() {
            localStorage.removeItem('token');
            localStorage.removeItem('tokenType');
            window.location = '/login';
        }

        function infoEmpresa() {
            $uibModal.open({
                animation: true,

                templateUrl: envService.read('apiUrl') + 'infoEmpresa/template',
                controller: 'infoEmpresaController as vm',
                size: 'lg',
                resolve: {
                    empresa: nav.session.empresa.id
                }
            });
        }

    }
})(window.angular);
