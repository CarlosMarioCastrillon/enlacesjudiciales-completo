(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('rolEditController', rolEditController);

    rolEditController.$inject = [
        '$uibModalInstance',
        'genericService',
        'messageUtil',
        'Constantes',
        'rol'
    ];

    function rolEditController(
        $uibModalInstance, genericService, messageUtil, Constantes, rol
    ){
        var vm = this,
            recurso = "roles";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        // Init autocompletes
        initAutoCompleteTipoRol();

        if(rol && rol.id){
            cargar();
        }else{
            vm.estado = true;
        }

        function cargar() {
            genericService.cargar(recurso, rol.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var rol = response.data;
                        vm.nombre = rol.nombre;
                        vm.tipo = rol.tipo;
                        vm.estado = rol.estado;
                        vm.fechaCreacion = rol.fecha_creacion;
                        vm.fechaModificacion = rol.fecha_modificacion;
                        vm.usuarioCreacionNombre = rol.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = rol.usuario_modificacion_nombre;
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardando){
                vm.deshabilitarGuardando = true;
                var datos = {
                    id: rol ? +rol.id: null,
                    nombre: vm.nombre,
                    tipo: vm.tipo,
                    estado: vm.estado
                };

                var promesa = null;
                if(!(rol && rol.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        $uibModalInstance.close(response.data.datos);
                        messageUtil.success(response.data.mensajes[0]);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }
                }).finally(function () {
                    vm.deshabilitarGuardando = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }

        // Autocompletes

        function initAutoCompleteTipoRol() {
            vm.configTipoRol = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'id',
                allowEmptyOption: false,
                create: false,
                persist: true
            };
            vm.opcionesTipoRol = [
                {id: Constantes.TipoRol.CLIENTE, nombre: "Del cliente"},
                {id: Constantes.TipoRol.SISTEMA, nombre: "Del sistema"},
                {id: Constantes.TipoRol.CLIENTE_DE_CLIENTE, nombre: "Del cliente de mi cliente"}
            ];
        }

    }
})();
