
(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('auditoriaMaestroController', auditoriaMaestroController);

    auditoriaMaestroController.$inject = [
        '$scope',
        '$timeout',
        '$routeParams',
        '$uibModal',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function auditoriaMaestroController(
        $scope, $timeout, $routeParams, $uibModal, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "auditoria-maestros";

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Limpiar filtros
        limpiarFiltros();

        // Grid
        function obtenerResumen(tableState, ctrl){
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }
            if(pipePromise) $timeout.cancel(pipePromise);
            pipePromise = $timeout(function() {
                if(!vm.cargando){
                    vm.cargando = true;
                    genericService.obtenerColeccion(recurso, tableState)
                        .then(function (response){
                            if(response.status === Constantes.Response.HTTP_OK){
                                var coleccion = response.data.datos;
                                if(coleccion && coleccion.length > 0 ){
                                    var nombreRecursoSplit;
                                    coleccion.forEach(function (row) {
                                        nombreRecursoSplit = row.nombre_recurso.split("\\");
                                        row.nombre_recurso = nombreRecursoSplit[nombreRecursoSplit.length - 1];
                                        row.recurso = JSON.parse(row.recurso_original);
                                        if(row.recurso_resultante){
                                            row.recurso_modificado = JSON.parse(row.recurso_resultante);
                                        }
                                    });
                                    vm.coleccion = [].concat(coleccion);
                                    vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                                    vm.paginacion.paginaActual = response.data.pagina_actual;
                                    vm.paginacion.desde = response.data.desde;
                                    vm.paginacion.hasta = response.data.hasta;
                                    vm.paginacion.total = response.data.total;
                                    tableState.pagination.numberOfPages = response.data.ultima_pagina;
                                }else{
                                    vm.coleccion = [];
                                }
                            }
                        }).finally(function () {
                            vm.cargando = false;
                        });
                }
            }, 500);
        }

        function limpiarFiltros(){
            vm.filtroNombre = null;
            vm.fitroDescripcion = null;
            vm.filtroFechaDesde = null;
            vm.filtroFechaHasta = null;
            $scope.isReset = true;
        }

        /*function cargarFiltros() {
            var filtros = gridUtil.getFormattedSearchPredicate("servicio");
            vm.nombre = filtros && filtros.nombre ? filtros.nombre : null;
        }*/

    }
})();
