(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('usuarioCambioClaveController', usuarioCambioClaveController);

    usuarioCambioClaveController.$inject = [
        '$uibModalInstance',
        'genericService',
        'messageUtil',
        'Constantes',
        'parametros'
    ];

    function usuarioCambioClaveController($uibModalInstance, genericService, messageUtil, Constantes, parametros){
        var vm = this;
        vm.usuario = parametros;

        // Acciones
        vm.cambiarClave = cambiarClave;
        vm.cancelar = cancelar;

        function cambiarClave() {
            var conErrores = false;
            if(vm.usuario.nuevaClave !== vm.usuario.confirmacionClave){
                conErrores = true;
                messageUtil.error("La contraseña de confirmación debe ser igual a la nueva contraseña.");
            }
            vm.guardando = true;
            if(vm.form.$valid && !conErrores){
                var datos = {
                    id: parametros.id,
                    nueva_clave: vm.usuario.nuevaClave,
                    cambio_clave: true
                };
                genericService.modificar("usuarios", datos)
                    .then(function (response) {
                        if(response.status === Constantes.Response.HTTP_OK){
                            messageUtil.success(response.data.mensajes[0]);
                            $uibModalInstance.close();
                        }else{
                            messageUtil.error(response.data.mensajes[0]);
                        }
                    });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }
    }
})();
