(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('opcionSistemaEditController', opcionSistemaEditController);

    opcionSistemaEditController.$inject = [
        '$uibModalInstance',
        'genericService',
        'messageUtil',
        'Constantes',
        'opcionSistema'
    ];

    function opcionSistemaEditController(
        $uibModalInstance, genericService, messageUtil, Constantes, opcionSistema
    ){
        var vm = this,
            recurso = "opciones-del-sistema";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        // inicial autocomples
        initAutocompleteModulo();

        if(opcionSistema && opcionSistema.id){
            cargar();
        }else{
            vm.estado = true;
        }

        function cargar() {
            genericService.cargar(recurso, opcionSistema.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var opcionSistema = response.data;
                        vm.nombre = opcionSistema.nombre;
                        vm.iconoMenu = opcionSistema.icono_menu;
                        vm.clavePermiso = opcionSistema.clave_permiso;
                        vm.posicion = opcionSistema.posicion;
                        vm.url = opcionSistema.url;
                        vm.urlAyuda = opcionSistema.url_ayuda;
                        vm.estado = opcionSistema.estado;
                        vm.moduloId = opcionSistema.opcion_del_sistema_id;
                        vm.fechaCreacion = opcionSistema.fecha_creacion;
                        vm.fechaModificacion = opcionSistema.fecha_modificacion;
                        vm.usuarioCreacionNombre = opcionSistema.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = opcionSistema.usuario_modificacion_nombre;
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;
                var datos = {
                    id: opcionSistema ? +opcionSistema.id : null,
                    nombre: vm.nombre,
                    icono_menu: vm.iconoMenu,
                    clave_permiso: vm.clavePermiso,
                    posicion: vm.posicion,
                    url: vm.url,
                    url_ayuda: vm.urlAyuda,
                    estado: vm.estado,
                    opcion_del_sistema_id: vm.moduloId
                };

                var promesa = null;
                if(!(opcionSistema && opcionSistema.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        $uibModalInstance.close(response.data.datos);
                        messageUtil.success(response.data.mensajes[0]);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }
                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }

        function initAutocompleteModulo() {
            vm.configModulo = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
            };

            vm.opcionesModulo = [];
            genericService.obtenerColeccionLigera("opciones-del-sistema",{
                ligera: true,
                sin_padre: true
            })
            .then(function (response){
                vm.opcionesModulo = [].concat(response.data);
            });
        }
    }
})();
