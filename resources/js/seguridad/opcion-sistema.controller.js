(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('opcionSistemaController', opcionSistemaController);

    opcionSistemaController.$inject = [
        '$scope',
        '$timeout',
        '$routeParams',
        '$uibModal',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function opcionSistemaController(
        $scope, $timeout, $routeParams, $uibModal, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "opciones-del-sistema";

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.crear = crear;

        // Limpiar filtros
        limpiarFiltros();

        // Grid
        function obtenerResumen(tableState, ctrl){
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }
            if(pipePromise) $timeout.cancel(pipePromise);
            pipePromise = $timeout(function() {
                if(!vm.cargando){
                    vm.cargando = true;
                    genericService.obtenerColeccion(recurso, tableState)
                        .then(function (response){
                            if(response.status === Constantes.Response.HTTP_OK){
                                var coleccion = response.data.datos;
                                if(coleccion && coleccion.length > 0 ){
                                    vm.coleccion = [].concat(coleccion);
                                    vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                                    vm.paginacion.paginaActual = response.data.pagina_actual;
                                    vm.paginacion.desde = response.data.desde;
                                    vm.paginacion.hasta = response.data.hasta;
                                    vm.paginacion.total = response.data.total;
                                    tableState.pagination.numberOfPages = response.data.ultima_pagina;
                                }else{
                                    vm.coleccion = [];
                                }
                            }
                        }).finally(function () {
                            vm.cargando = false;
                        });
                }
            }, 500);
        }

        function limpiarFiltros(){
            vm.nombre = null;
            vm.marca = null;
            $scope.isReset = true;
        }

        /*function cargarFiltros() {
            var filtros = gridUtil.getFormattedSearchPredicate("servicio");
            vm.nombre = filtros && filtros.nombre ? filtros.nombre : null;
        }*/

        // Modulos externos

        function crear(opcionSistema) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + recurso + '/create',
                controller: 'opcionSistemaEditController as vm',
                size: 'md',
                resolve: {
                    opcionSistema: function () {
                        return opcionSistema;
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(opcionSistema && opcionSistema.id && response && response.id){
                    opcionSistema.nombre = response.nombre;
                    opcionSistema.icono_menu = response.icono_menu;
                    opcionSistema.clave_permiso = response.clave_permiso;
                    opcionSistema.posicion = response.posicion;
                    opcionSistema.estado = response.estado;
                    opcionSistema.opcion_del_sistema_id = response.opcion_del_sistema_id;
                    opcionSistema.usuario_modificacion_id = response.usuario_modificacion_id;
                    opcionSistema.usuario_modificacion_nombre = response.usuario_modificacion_nombre;
                    opcionSistema.fecha_creacion = response.fecha_creacion;
                    opcionSistema.fecha_modificacion = response.fecha_modificacion;
                }else if(response && response.id){
                    $scope.$broadcast('refreshGrid');
                }
            });
        }

    }
})();
