(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('usuarioEditController', usuarioEditController);

    usuarioEditController.$inject = [
        '$timeout',
        '$uibModalInstance',
        'genericService',
        'messageUtil',
        'Constantes',
        'sessionUtil',
        'parametros'
    ];

    function usuarioEditController(
        $timeout, $uibModalInstance, genericService, messageUtil, Constantes, sessionUtil, parametros
    ){
        var vm = this,
            usuario = parametros.usuario,
            empresa = sessionUtil.getEmpresa(),
            recurso = "usuarios";

        vm.usuarioCliente = parametros.cliente ? parametros.cliente : null;

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        // Init autocompletes
        initAutocompleteEmpresa();
        initAutocompleteRol();
        initAutocompleteTipoDocumento();
        initAutocompleteDepartamento();
        initAutocompleteCiudad();
        initAutocompleteArea();

        if(usuario && usuario.id){
            vm.formatoFechaVencimiento = {
                useCurrent: false,
                format: 'YYYY-MM-DD'
            };
            cargar();
        }else{
            vm.estado = true;
            vm.dependientePrincipal = false;
            vm.conMicroportal = false;
            vm.formatoFechaVencimiento = {
                useCurrent: false,
                format: 'YYYY-MM-DD',
                minDate: 'now'
            };
        }

        if(vm.usuarioCliente){
            vm.empresaId = empresa.id;
            vm.deshabilitarEmpresaId = true;
            vm.deshabilitarRolId = true;
        }else{
            vm.deshabilitarEmpresaId = false;
            vm.deshabilitarRolId = false;
        }

        function cargar() {
            genericService.cargar(recurso, usuario.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var usuario = response.data,
                            rol = usuario.rol,
                            tipoDocumento = usuario.tipo_documento,
                            empresa = usuario.empresa,
                            departamento = usuario.departamento,
                            ciudad = usuario.ciudad,
                            area = usuario.area;

                        // Guardar el usuario origial
                        vm.usuarioOriginal = usuario;

                        // Cargar datos basicos de usuario
                        vm.id = usuario.id;
                        vm.documento = usuario.documento;
                        vm.nombre = usuario.nombre;
                        vm.emailUno = usuario.email_uno;
                        vm.emailDos = usuario.email_dos;
                        vm.fechaVencimiento = usuario.fecha_vencimiento;
                        vm.dependientePrincipal = !!usuario.dependiente_principal;
                        vm.conMicroportal = !!usuario.con_microportal;
                        vm.conMovimientoProceso = !!usuario.con_movimiento_proceso;
                        vm.conMontajeActuacion = !!usuario.con_montaje_actuacion;
                        vm.conMovimientoProcesoDos = !!usuario.con_movimiento_proceso_dos;
                        vm.conMontajeActuacionDos = !!usuario.con_montaje_actuacion_dos;
                        vm.conTarea = !!usuario.con_tarea;
                        vm.conRespuestaTarea = !!usuario.con_respuesta_tarea;
                        vm.conVencimientoTerminoTarea = !!usuario.con_ven_termino_tarea;
                        vm.conVencimientoTerminoActuacionRama = !!usuario.con_ven_termino_actuacion_rama;
                        vm.conVencimientoTerminoActuacionArchivo = !!usuario.con_ven_termino_actuacion_archivo;
                        vm.estado = usuario.estado;
                        vm.fechaCreacion = usuario.fecha_creacion;
                        vm.fechaModificacion = usuario.fecha_modificacion;
                        vm.usuarioCreacionNombre = usuario.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = usuario.usuario_modificacion_nombre;

                        // Cargar rol
                        vm.rolId = rol.id;
                        vm.opcionesRol.push({id: rol.id, nombre: rol.nombre});

                        // Cargar tipo de documento
                        vm.tipoDocumentoId = tipoDocumento.id;
                        vm.opcionesTipoDocumento.push({id: tipoDocumento.id, codigo: tipoDocumento.codigo});

                        // Cargar empresa
                        vm.empresaId = empresa.id;
                        vm.opcionesEmpresa.push({id: empresa.id, nombre: empresa.nombre});

                        // Cargar departamento
                        if(departamento){
                            vm.departamentoId = departamento.id;
                            vm.opcionesDepartamento.push({id: departamento.id, nombre: departamento.nombre});
                        }

                        // Cargar ciudad
                        if(ciudad){
                            vm.ciudadId = ciudad.id;
                            vm.opcionesCiudad.push({id: ciudad.id, nombre: ciudad.nombre});
                        }

                        // Cargar area
                        if(area){
                            vm.areaId = area.id;
                            vm.opcionesArea.push({id: area.id, nombre: area.nombre});
                        }
                    }
                });
        }

        function guardar() {
            var conError = false, message;
            if(vm.emailDos && vm.emailUno == vm.emailDos){
                conError = true;
                message = "El E-mail dos debe ser diferente al E-mail uno."
            }
            vm.guardando = true;
            if(!conError && vm.form.$valid && !vm.deshabilitarGuardando){
                vm.deshabilitarGuardando = true;
                var datos = {
                    id: usuario ? +usuario.id: null,
                    documento: vm.documento,
                    nombre: vm.nombre,
                    clave: vm.clave,
                    email_uno: vm.emailUno,
                    email_dos: vm.emailDos,
                    fecha_vencimiento: vm.fechaVencimiento,
                    dependiente_principal: vm.dependientePrincipal,
                    con_microportal: vm.conMicroportal,
                    con_movimiento_proceso: vm.conMovimientoProceso,
                    con_montaje_actuacion: vm.conMontajeActuacion,
                    con_movimiento_proceso_dos: vm.conMovimientoProcesoDos,
                    con_montaje_actuacion_dos: vm.conMontajeActuacionDos,
                    con_tarea: vm.conTarea,
                    con_respuesta_tarea: vm.conRespuestaTarea,
                    con_ven_termino_tarea: vm.conVencimientoTerminoTarea,
                    con_ven_termino_actuacion_rama: vm.conVencimientoTerminoActuacionRama,
                    con_ven_termino_actuacion_archivo: vm.conVencimientoTerminoActuacionArchivo,
                    estado: vm.estado,
                    tipo_documento_id: vm.tipoDocumentoId,
                    empresa_id: vm.empresaId,
                    rol_id: vm.rolId,
                    departamento_id: vm.departamentoId,
                    ciudad_id: vm.ciudadId,
                    area_id: vm.areaId
                };

                var promesa = null;
                if(!(usuario && usuario.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        $uibModalInstance.close(response.data.datos);
                        messageUtil.success(response.data.mensajes[0]);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }
                }).finally(function () {
                    vm.deshabilitarGuardando = false;
                });
            }

            if(conError){
                messageUtil.error(message);
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }

        // Autocompletes

        function initAutocompleteEmpresa() {
            vm.configEmpresa = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                onItemAdd: function (value, $item) {
                    if(vm.empresaId !== value){
                        vm.opcionesArea = [];
                        $timeout(function () {
                            vm.limpiarOpcionesArea = true;
                            genericService.obtenerColeccionLigera("areas", {
                                ligera: true,
                                empresa_id: value
                            })
                                .then(function (response){
                                    var areas = response.data;
                                    if(vm.usuarioOriginal && vm.usuarioOriginal.area){
                                        vm.areaId = vm.usuarioOriginal.area.id;
                                        vm.opcionesArea.push({
                                            id: vm.usuarioOriginal.area.id,
                                            nombre: vm.usuarioOriginal.area.nombre
                                        });
                                    }
                                    vm.opcionesArea = [].concat(areas);
                                });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.opcionesArea = [];
                    vm.areaId = null;
                    $timeout(function () {
                        vm.limpiarOpcionesArea = true;
                    }, 100);
                }
            };

            vm.opcionesEmpresa = [];
            genericService.obtenerColeccionLigera("empresas", { ligera: true })
                .then(function (response){
                    vm.opcionesEmpresa = [].concat(response.data);
                });
        }

        function initAutocompleteRol() {
            vm.configRol = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true
            };

            vm.opcionesRol = [];
            genericService.obtenerColeccionLigera("roles",
                {
                    ligera: true,
                    cliente: vm.usuarioCliente
                })
                .then(function (response){
                    vm.opcionesRol = [].concat(response.data);
                    if (!!vm.usuarioCliente) {
                        vm.rolId = vm.opcionesRol[0].id;
                    }
                });
        }

        function initAutocompleteArea() {
            vm.configArea = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true
            };
            vm.opcionesArea = [];
        }

        function initAutocompleteDepartamento() {
            vm.configDepartamento = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                onItemAdd: function (value, $item) {
                    if(vm.departamentoId !== value){
                        vm.opcionesCiudad = [];
                        $timeout(function () {
                            vm.limpiarOpcionesCiudad = true;
                            genericService.obtenerColeccionLigera("ciudades", {
                                ligera: true,
                                departamento_id: value
                            })
                            .then(function (response){
                                var ciudades = response.data;
                                if(vm.usuarioOriginal && vm.usuarioOriginal.ciudad){
                                    vm.ciudadId = vm.usuarioOriginal.ciudad.id;
                                    vm.opcionesCiudad.push({
                                        id: vm.usuarioOriginal.ciudad.id,
                                        nombre: vm.usuarioOriginal.ciudad.nombre
                                    });
                                }
                                vm.opcionesCiudad = [].concat(ciudades);
                            });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.opcionesCiudad = [];
                    vm.ciudadId = null;
                    $timeout(function () {
                        vm.limpiarOpcionesCiudad = true;
                    }, 100);
                }
            };

            vm.opcionesDepartamento = [];
            genericService.obtenerColeccionLigera("departamentos",{
                ligera: true
            })
            .then(function (response){
                vm.opcionesDepartamento = [].concat(response.data);
            });
        }

        function initAutocompleteCiudad() {
            vm.configCiudad = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                lock: false
            };
            vm.opcionesCiudad = [];
        }

        function initAutocompleteTipoDocumento() {
            vm.configTipoDocumento = {
                valueField: 'id',
                labelField: 'codigo',
                searchField: ['codigo'],
                sortField: 'codigo',
                allowEmptyOption: false,
                create: false,
                persist: true
            };

            vm.opcionesTipoDocumento = [];
            genericService.obtenerColeccionLigera("tipos-documentos",{
                ligera: true
            })
            .then(function (response){
                vm.opcionesTipoDocumento = [].concat(response.data);
            });
        }

    }
})();
