(function () {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('permisoEditController', permisoEditController);

    permisoEditController.$inject = [
        '$scope',
        '$http',
        '$routeParams',
        '$timeout',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function permisoEditController(
        $scope, $http, $routeParams, $timeout, envService, genericService, messageUtil, Constantes
    ) {
        var vm = this,
            promise,
            pipeSincronizarPermisos,
            rolId = $routeParams.id;

        vm.coleccion = [];
        vm.TipoRolEnum = Constantes.TipoRol;

        // Acciones
        vm.filtrar = filtrar;
        vm.cambiarPermiso = cambiarPermiso;

        // inicial autocomples
        initAutocompleteModulo();
        initAutocompleteSubmodulo();

        // Carga inicial
        cargarRol();
        obtenerPermisos();

        function cargarRol() {
            genericService.cargar("roles", rolId)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        vm.rol = response.data;
                    }
                });
        }

        function filtrar() {
            if (promise !== null) {
                $timeout.cancel(promise);
            }
            promise = $timeout(function () {
                obtenerPermisos();
                promise = null;
            }, 400);
        }

        function obtenerPermisos() {
            genericService.obtener("roles/" + rolId + "/permisos", {
                modulo_id: vm.moduloId,
                submodulo_id: vm.submoduloId
            })
            .then(function (response){
                if(response.status === Constantes.Response.HTTP_OK){
                    var permisos = response.data;
                    vm.coleccion = permisos;
                }
            });
        }

        function cambiarPermiso(permiso) {
            vm.permisosAOtorgar = vm.permisosAOtorgar || [];
            vm.permisosARevocar = vm.permisosARevocar || [];
            if(permiso.permitido){
                vm.permisosAOtorgar.push(permiso.clave);
            }else{
                vm.permisosARevocar.push(permiso.clave);
            }

            clearTimeout(pipeSincronizarPermisos);
            pipeSincronizarPermisos = setTimeout(sincronizarPermisos, 1000);
        }

        function sincronizarPermisos() {
            if(vm.permisosAOtorgar && vm.permisosAOtorgar.length > 0){
                $http.post(envService.read('apiUrl') + "roles/" + rolId + "/permisos", vm.permisosAOtorgar);
                vm.permisosAOtorgar = [];
            }

            if(vm.permisosARevocar && vm.permisosARevocar.length > 0){
                $http.put(envService.read('apiUrl') + "roles/" + rolId + "/permisos", vm.permisosARevocar);
                vm.permisosARevocar = [];
            }
        }

        // Autocompletes

        function initAutocompleteModulo() {
            vm.configModulo = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                onItemAdd: function (value, $item) {
                    if(!vm.cargandoSubmodulos){
                        vm.cargandoSubmodulos = true;
                        vm.opcionesSubmodulo = [];
                        vm.submoduloId = null;
                        $timeout(function () {
                            vm.limpiarOpcionesSubmodulo = true;
                            genericService.obtenerColeccionLigera("opciones-del-sistema", {
                                ligera: true,
                                padre_id: value
                            })
                            .then(function (response){
                                vm.opcionesSubmodulo = [].concat(response.data);
                            }).finally(function () {
                                vm.cargandoSubmodulos = false;
                            });
                        }, 100);
                    }
                },
                onItemRemove: function(value){
                    vm.opcionesSubmodulo = [];
                    vm.submoduloId = null;
                    $timeout(function () {
                        vm.limpiarOpcionesSubmodulo = true;
                    }, 100);
                }
            };

            vm.opcionesModulo = [{id: null, nombre: null}];
            genericService.obtenerColeccionLigera("opciones-del-sistema",{
                ligera: true,
                sin_padre: true
            })
            .then(function (response){
                vm.opcionesModulo = [].concat(response.data);
            });
        }

        function initAutocompleteSubmodulo() {
            vm.configSubmodulo = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                onItemAdd: function (value, $item) {
                    //
                },
                onItemRemove: function(value){
                    //
                }
            };
            vm.opcionesSubmodulo = [{id: null, nombre: null}];
        }

    }
})();
