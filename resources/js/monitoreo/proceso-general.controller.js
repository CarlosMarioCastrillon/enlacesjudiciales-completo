(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('procesoGeneralController', procesoGeneralController);

    procesoGeneralController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function procesoGeneralController(
        $scope, $routeParams, $timeout, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "procesos-generales";

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 5;
        $scope.isReset = true;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.buscar = buscar;
        vm.obtenerExcel = obtenerExcel;

        // inicial autocomples
        initAutocompleteEmpresa();

        // Limpiar filtros
        limpiarFiltros();

        // Grid

        function buscar() {
            vm.buscando = true;
            if(vm.form.$valid && !vm.deshabilitarBuscado) {
                vm.activarBuscar = true;
                $scope.$broadcast('refreshGrid');
            }
        }

        function obtenerResumen(tableState, ctrl){
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }

            if(vm.paginacion && vm.paginacion.paginaActual && tableState.pagination){
                var start = tableState.pagination.start || 0,
                    number = tableState.pagination.number || 10,
                    nextPage = (start + number) / number;
                if(vm.paginacion.paginaActual != nextPage){
                    vm.activarBuscar = true;
                }else if(vm.activarBuscar){
                    tableState.pagination.start = 0;
                    tableState.pagination.number = vm.itemsPorPagina;
                }
            }

            if(vm.activarBuscar && !vm.cargando){
                vm.cargando = true;
                genericService.obtenerColeccion(recurso, tableState).then(function (response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var coleccion = response.data.datos;
                        if(coleccion && coleccion.length > 0 ){
                            vm.coleccion = [].concat(coleccion);
                            vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                            vm.paginacion.paginaActual = response.data.pagina_actual;
                            vm.paginacion.desde = response.data.desde;
                            vm.paginacion.hasta = response.data.hasta;
                            vm.paginacion.total = response.data.total;
                            tableState.pagination.numberOfPages = response.data.ultima_pagina;
                        }else{
                            vm.coleccion = [];
                        }
                    }
                }).finally(function () {
                    vm.buscando = false;
                    vm.cargando = false;
                    vm.activarBuscar = false;
                });
            }
        }

        function limpiarFiltros(){
            vm.filtroEmpresa = null;
            vm.filtroProceso = null;
            vm.filtroDemandante = null;
            vm.filtroDemandado = null;
            vm.activarBuscar = true;
            $scope.isReset = true;
        }

        // Acciones

        function obtenerExcel() {
            genericService.obtenerArchivo(recurso + "/excel", {
                empresa: vm.filtroEmpresa,
                numero_proceso: vm.filtroProceso,
                demandante: vm.filtroDemandante,
                demandado: vm.filtroDemandado
            });
        }

        function initAutocompleteEmpresa() {
            vm.configEmpresa = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
            };
            vm.opcionesEmpresa = [];
            genericService.obtenerColeccionLigera("empresas",{
                ligera: true,
            })
                .then(function (response){
                    vm.opcionesEmpresa = [].concat(response.data);
                });
        }
    }
})();
