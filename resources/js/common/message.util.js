(function($) {
    'use strict';

    angular.module('enlaces.app')
        .factory('messageUtil', messageUtil);

    function messageUtil(){

        return {
            error: error,
            warning: warning,
            success: success,
            showFormError: showFormError,
            showSnackbar: showSnackbar
        };
        
        function error(message) {
            MDSnackbars.hide();
            MDSnackbars.show({
                type: 'danger',
                text: '<div style="position: relative;"><span>' + message + '</span><a type="button" id="snackbar-action" style="cursor: pointer; position: absolute; right: 0; color: black;"><i class="material-icons">close</i></a></div>',
                html: true,
                fullWidth: true,
                clickToClose: true,
                timeout: 10000,
                animation: 'slideup'
            });
            setTimeout(function () {
                $("#snackbar-action").off("click").on("click", function () {
                    MDSnackbars.hide();
                })
            }, 1000);
        }

        function warning(message) {
            MDSnackbars.hide();
            MDSnackbars.show({
                type: 'warning',
                text: '<div style="position: relative;"><span>' + message + '</span><a type="button" id="snackbar-action" style="cursor: pointer; position: absolute; right: 0; color: black;"><i class="material-icons">close</i></a></div>',
                html: true,
                fullWidth: true,
                clickToClose: true,
                timeout: 10000,
                animation: 'slideup'
            });
            setTimeout(function () {
                $("#snackbar-action").off("click").on("click", function () {
                    MDSnackbars.hide();
                })
            }, 1000);
        }
        
        function success(message) {
            MDSnackbars.hide();
            MDSnackbars.show({
                type: 'success',
                text: '<div style="position: relative;"><span>' + message + '</span><a type="button" id="snackbar-action" style="cursor: pointer; position: absolute; right: 0; color: black;"><i class="material-icons">close</i></a></div>',
                html: true,
                fullWidth: true,
                clickToClose: true,
                timeout: 10000,
                animation: 'slideup'
            });
            setTimeout(function () {
                $("#snackbar-action").off("click").on("click", function () {
                    MDSnackbars.hide();
                })
            }, 1000);
        }

        function showSnackbar(options, actionOptions) {
            MDSnackbars.hide();
            MDSnackbars.show(options);
            if(actionOptions){
                //Se pone el timeout por que se demora unos segundos en pintarse el snackbar luego del show
                setTimeout(function () {
                    $("#snackbar-action").off("click").on("click", function () {
                        angular.element(document.getElementById(actionOptions.scopeElement)).scope()[actionOptions.controllerName][actionOptions.callFunction].apply(this, actionOptions.parameters);
                        angular.element(document.getElementById(actionOptions.scopeElement)).scope().$apply();
                    })
                }, 1000);
            }
        }

        function showFormError(message) {
            $("#error-message").html(message);
        }
    }
})(jQuery);