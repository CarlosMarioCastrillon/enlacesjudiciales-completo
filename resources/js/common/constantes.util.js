(function() {
    'use strict';

    angular.module('enlaces.app')
        .constant('Constantes', {
            Response: {
                HTTP_OK: 200,
                HTTP_CREATED: 201,
                HTTP_ACCEPTED: 202,
                HTTP_NO_CONTENT: 204,
                HTTP_BAD_REQUEST: 400,
                HTTP_UNAUTHORIZED: 401,
                HTTP_FORBIDDEN: 403,
                HTTP_NOT_FOUND: 404,
                HTTP_CONFLICT: 409
            },
            SiNo: {
                SI: 'S',
                NO: 'N'
            },
            TipoRol: {
                CLIENTE: 1,
                SISTEMA: 2,
                CLIENTE_DE_CLIENTE: 3
            },
            ClaseConcepto: {
                DEBITO: 'DB',
                CREDITO: 'CR'
            },
            TipoConcepto: {
                COSTOS_INTERESES_DEMANDA: 'D',
                HONORARIOS_EXTRAS: 'G',
                COSTOS_DE_PROCESOS: 'C'
            },
            EstadoDiligencia: {
                SOLICITADO: 1,
                COTIZADO: 2,
                APROBADO: 3,
                TRAMITE: 4,
                TERMINADO: 5,
                CONTABILIZADO: 6,
                FACTURADO: 7
            },
            CambioInstancia: {
                SUPERIOR: 1,
                INFERIOR: 2
            }
        });
})();
