(function(angular) {
    'use strict';

    angular.module('enlaces.app')
        .factory('genericService', genericService);

    genericService.$inject = ['$http', 'fileUtil', 'envService'];

    function genericService($http, fileUtil, envService){

        return {
            obtener: obtener,
            cargar: cargar,
            crear: crear,
            modificar: modificar,
            eliminar: eliminar,
            cargarArchivo: cargarArchivo,
            obtenerArchivo: obtenerArchivo,
            obtenerColeccion: obtenerColeccion,
            obtenerColeccionLigera: obtenerColeccionLigera

        };

        function obtener(recurso, params){
            return $http.get(envService.read('apiUrl') + recurso, params ? {
                params: params
            } : null)
            .then(function (response){
                return response;
            })
            .catch(function (error){
                return error;
            });
        }

        function cargar(recurso, id){
            return $http.get(envService.read('apiUrl') + recurso + "/" + id)
                .then(function (response){
                    return response;
                })
                .catch(function (error){
                    return error;
                });
        }

        function crear(recurso, object) {
            return $http.post(envService.read('apiUrl') + recurso, object)
                .then(function (response){
                    return response;
                })
                .catch(function (error){
                    return error;
                });
        }

        function modificar(recurso, object) {
            return $http.put(envService.read('apiUrl') + recurso + (object.id ? "/" + object.id : ""), object)
                .then(function (response){
                    return response;
                })
                .catch(function (error){
                    return error;
                });
        }

        function eliminar(recurso, id, params) {
            return $http.delete(envService.read('apiUrl') + recurso + (id ? "/" + id : ""), params ? {
                params: params
            } : null)
                .then(function (response){
                    return response;
                })
                .catch(function (error){
                    return error;
                });
        }

        function obtenerArchivo(recurso, params){
            return fileUtil.get(envService.read('apiUrl') + recurso, params, true);
        }

        function cargarArchivo(recurso, file){
            var fd = new FormData();
            fd.append('file', file);
            return $http.post(envService.read('apiUrl') + recurso, fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            });
        }

        function obtenerColeccion(recurso, tableState) {
            var pagination = tableState.pagination,
                search = tableState.search && tableState.search.predicateObject ? tableState.search.predicateObject : {},
                sort = tableState.sort && tableState.sort.predicate ? tableState.sort.predicate : [],
                start = pagination.start || 0,
                number = pagination.number || 10,
                actualPage = (start + number) / number,
                searchStr = "?page=" + actualPage + "&limite=" + number,
                sortStr = "&ordenar_por=";

            angular.forEach(search, function (value, index) {
                searchStr += "&" + index + "=" + value;
            });

            angular.forEach(sort, function (value, index) {
                sortStr += value.predicate + ":" + (value.reverse ? "desc" : "asc");
                if((index + 1) < sort.length){
                    sortStr += ",";
                }
            });

            return $http.get(envService.read('apiUrl') + recurso + searchStr + (sort.length > 0 ? sortStr : ""))
            .then(function (response){
                return response;
            })
            .catch(function (error){
                return error;
            });
        }

        function obtenerColeccionLigera(recurso, params) {
            return $http.get(envService.read('apiUrl') + recurso, params ? {
                params: params
            } : null)
            .then(function (response){
                return response;
            })
            .catch(function (error){
                return error;
            });
        }

    }
})(window.angular);
