(function($) {
    'use strict';

    angular.module('enlaces.app')
        .factory('selectizeUtil', selectizeUtil);

    function selectizeUtil(){

        return {
            disable: disable,
            enable: enable
        };

        function disable(id){
            return $("#" + id + " > .selectize-control > .selectize-input > input").prop('disabled', true);
        }

        function enable(id){
            return $("#" + id + " > .selectize-control > .selectize-input > input").prop('disabled', false);
        }

    }
})(jQuery);