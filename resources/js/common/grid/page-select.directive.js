(function() {
    'use strict';

    angular.module('enlaces.app')
    .directive('pageSelect', pageSelect);

    function pageSelect(){
        return {
            restrict: 'E',
            template: '<div><div style="float: left; margin-right: 4px; margin-top: 7px;">Página: </div><div style="float: left; width: 35px; margin-top: 6px;"><input class="input-grid" style="text-align: center; width: 40px;" type="number" ng-model="inputPage" ng-change="selectPage(inputPage)"></div></div>',
            link: function(scope, element, attrs) {
                scope.$watch('currentPage', function(c) {
                    scope.inputPage = c;
                });
            }
        }
    }
})();