(function() {
    'use strict';

    angular.module('enlaces.app')
        .directive('stSearchToggle', stSearchToggle);

    function stSearchToggle(){
        return {
            restrict: 'AE',
            require: '^stTable',
            scope: {
                searchModel: '=',
                searchPredicate: '@'
            },
            link: function(scope, element, attr, table) {
                scope.$watch('searchModel', function(){
                    table.search(scope.searchModel, scope.searchPredicate);
                }, true);
            }
        }
    }
})();