(function() {
    'use strict';

    angular.module('enlaces.app')
    .directive('stReset', reset);

    function reset(){
        return {
            require: '^stTable',
            link: function (scope, element, attr, ctrl) {
                scope.$watch(function () {
                    if(scope.isReset) {
                        // remove local storage
                        if (attr.stPersist) {
                            localStorage.removeItem(attr.stPersist);
                        }
                        // reset table state
                        var tableState = ctrl.tableState();
                        tableState.search = {};
                        tableState.sort = {};
                        tableState.pagination.start = 0;
                        ctrl.pipe();
                        // reset scope value
                        scope.isReset = false;
                    }
                });
            }
        };
    }
})();