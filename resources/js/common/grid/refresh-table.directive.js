(function() {
    'use strict';

    angular.module('enlaces.app')
    .directive('refreshTable', refreshTable);

    function refreshTable(){
        return {
            require:'stTable',
            restrict: "A",
            link:function(scope, elem, attr, table){
                scope.$on("refreshGrid", function() {
                    table.pipe(table.tableState());
                });
            }
        };
    }
})();