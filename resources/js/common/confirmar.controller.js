(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('confirmarController', confirmarController);

    confirmarController.$inject = [
        '$uibModalInstance',
        'parametros'
    ];

    function confirmarController($uibModalInstance, parametros){
        var vm = this;
        vm.mensaje = parametros.mensaje;

        // Acciones
        vm.ok = ok;
        vm.cancelar = cancelar;

        function ok() {
            $uibModalInstance.close({
                ok: true
            });
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }
    }
})();
