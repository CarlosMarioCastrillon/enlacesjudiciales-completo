(function() {
    'use strict';

    angular.module('enlaces.app')
        .factory('fileUtil', fileUtil);

    fileUtil.$inject = [
        '$http',
        'Constantes'
    ];

    function fileUtil($http, Constantes){

        return {
            get: get
        };

        function get(httpPath, params, toDownload, method) {
            var promise = null;
            if(method && method === "POST"){
                promise = $http.post(httpPath, params, {
                    responseType: 'arraybuffer'
                });
            }else{
                promise = $http.get(httpPath, {
                    params: params,
                    responseType: 'arraybuffer'
                });
            }

            return promise
            .then(openComplete)
            .catch(openFailed);

            function openComplete(response) {
                if(response.status === Constantes.Response.HTTP_OK){
                    var octetStreamMime = 'application/octet-stream';
                    var success = false;

                    // Get the headers
                    var headers = response.headers();

                    // Get the filename from the [x-filename|content-disposition] header or default to "file"
                    var filename = "file";
                    if(headers['x-filename']){
                        filename = headers['x-filename'];
                    }else if(headers['content-disposition']) {
                        //Content-Disposition: inline; filename="file"
                        filename = headers['content-disposition'].split(";")[1].split("=")[1].replace(/"/gi, "");
                    }

                    // Determine the content type from the header or default to "application/octet-stream"
                    var contentType = headers['content-type'] || octetStreamMime;

                    // Prepare a blob URL
                    var blob = new Blob([response.data], { type: contentType });

                    try {
                        // Try using saveBlob if supported -- IE11 & Edge --
                        if(toDownload){
                            if(navigator.msSaveBlob)
                                navigator.msSaveBlob(blob, filename);
                            else {
                                // Try using other saveBlob implementations, if available
                                var saveBlob = navigator.webkitSaveBlob || navigator.mozSaveBlob || navigator.saveBlob;
                                if(saveBlob === undefined) throw "Not supported";
                                saveBlob(blob, filename);
                            }
                        }else {
                            navigator.msSaveOrOpenBlob(blob, filename);
                        }
                        success = true;
                    } catch(ex) {
                        console.log(ex);
                    }

                    // Get the blob url creator
                    if(!success){
                        var urlCreator = window.URL || window.webkitURL || window.mozURL || window.msURL;
                        var url = urlCreator.createObjectURL(blob);
                        if(urlCreator) {
                            // Try to use a download link
                            var link = document.createElement('a');
                            try {
                                if(toDownload){
                                    link.href = url;
                                    link.download = filename;
                                    link.click();
                                } else {
                                    var isChrome = false,
                                        isChromium = window.chrome,
                                        winNav = window.navigator,
                                        vendorName = winNav.vendor,
                                        isOpera = winNav.userAgent.indexOf("OPR") > -1,
                                        isIEedge = winNav.userAgent.indexOf("Edge") > -1,
                                        isIOSChrome = winNav.userAgent.match("CriOS");

                                    if (isIOSChrome) {
                                        isChrome = true;
                                    } else {
                                        isChrome = !!(isChromium !== null && typeof isChromium !== "undefined" &&
                                        vendorName === "Google Inc." && isOpera === false && isIEedge === false);
                                    }

                                    if(isChrome){
                                        link.href = url;
                                        link.download = filename;
                                        link.click();
                                    } else {
                                        var reader = new FileReader();
                                        reader.onload = function(event){
                                            window.open(event.target.result, '_blank');
                                        };
                                        reader.readAsDataURL(blob);
                                    }
                                }

                                success = true;
                            } catch(ex) {
                                console.log(ex);
                            }

                            if(!success) {
                                // Fallback to window.location method
                                try {
                                    // Prepare a blob URL
                                    // Use application/octet-stream when using window.location to force download
                                    window.location = url;
                                    success = true;
                                } catch(ex) {
                                    console.log(ex);
                                }
                            }
                        }
                    }

                    if(!success) {
                        // Fallback to window.open method
                        window.open(httpPath, '_blank', '');
                    }
                }

                return response;
            }

            function openFailed(error){
                console.log(error);
                return error;
            }
        }

    }

})();