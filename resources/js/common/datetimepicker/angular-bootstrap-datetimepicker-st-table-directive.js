(function () {
    'use strict';

    angular
        .module('enlaces.app')
        .provider('datetimepickerStTable', function () {
            var default_options = {};

            this.setOptions = function (options) {
                default_options = options;
            };

            this.$get = function () {
                return {
                    getOptions: function () {
                        return default_options;
                    }
                };
            };
        })
        .directive('datetimepickerStTable', [
            '$timeout',
            'datetimepickerStTable',
            function ($timeout,
                      datetimepickerStTable) {

                var default_options = datetimepickerStTable.getOptions();

                return {
                    require: ['?ngModel','^?stTable'],
                    restrict: 'AE',
                    scope: {
                        datetimepickerOptions: '@',
                        searchModel: '=',
                        searchPredicate: '@'
                    },
                    link: function ($scope, $element, $attrs, directives) {
                        var passed_in_options = $scope.$eval($attrs.datetimepickerOptions);
                        var options = jQuery.extend({}, default_options, passed_in_options);

                        var ngModelCtrl = directives[0],
                            table = directives[1];

                        $element
                            .on('dp.change', function (e) {
                                if (ngModelCtrl) {
                                    $timeout(function () {
                                        ngModelCtrl.$setViewValue(e.target.value);
                                    });
                                }
                            })
                            .datetimepicker(options);

                        function setPickerValue() {
                            var date = null;

                            if (ngModelCtrl && ngModelCtrl.$viewValue) {
                                date = ngModelCtrl.$viewValue;
                            }

                            $element
                                .data('DateTimePicker')
                                .date(date);
                        }

                        if (ngModelCtrl) {
                            ngModelCtrl.$render = function () {
                                setPickerValue();
                            };
                        }

                        $scope.$watch('searchModel', function(){
                            if(table){
                                table.search($scope.searchModel, $scope.searchPredicate);
                            }
                        }, true);

                        setPickerValue();
                    }
                };
            }
        ]);
})();