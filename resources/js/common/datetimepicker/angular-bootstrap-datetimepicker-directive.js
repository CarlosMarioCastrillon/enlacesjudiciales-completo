(function () {
    'use strict';

    angular
        .module('enlaces.app')
        .provider('datetimepicker', function () {
            var default_options = {};

            this.setOptions = function (options) {
                default_options = options;
            };

            this.$get = function () {
                return {
                    getOptions: function () {
                        return default_options;
                    }
                };
            };
        })
        .directive('datetimepicker', [
            '$timeout',
            'datetimepicker',
            function ($timeout,
                      datetimepicker) {

                var default_options = datetimepicker.getOptions();

                return {
                    require: ['?ngModel'],
                    restrict: 'AE',
                    scope: {
                        datetimepickerOptions: '@',
                        searchModel: '=',
                        searchPredicate: '@'
                    },
                    link: function ($scope, $element, $attrs, directives) {
                        var passed_in_options = $scope.$eval($attrs.datetimepickerOptions);
                        var options = jQuery.extend({}, default_options, passed_in_options);

                        var ngModelCtrl = directives[0];

                        $element
                            .on('dp.change', function (e) {
                                if (ngModelCtrl) {
                                    $timeout(function () {
                                        ngModelCtrl.$setViewValue(e.target.value);
                                    });
                                }
                            })
                            .datetimepicker(options);

                        function setPickerValue() {
                            var date = null;

                            if (ngModelCtrl && ngModelCtrl.$viewValue) {
                                date = ngModelCtrl.$viewValue;
                            }

                            $element
                                .data('DateTimePicker')
                                .date(date);
                        }

                        if (ngModelCtrl) {
                            ngModelCtrl.$render = function () {
                                setPickerValue();
                            };
                        }

                        setPickerValue();
                    }
                };
            }
        ]);
})();