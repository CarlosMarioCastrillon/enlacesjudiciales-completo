(function() {
    'use strict';

    angular.module('enlaces.app')
        .factory('sessionUtil', sessionUtil);

    function sessionUtil(){

        return {
            getAll: getAll,
            getRol: getRol,
            getUsuario: getUsuario,
            getEmpresa: getEmpresa,
            getTipoDocumento: getTipoDocumento,
            getPermisos: getPermisos,
            getParametros: getParametros,
            getTituloVista: getTituloVista,
            can: can
        };

        function getAll() {
            return JSON.parse(localStorage.getItem('session'));
        }

        function getRol() {
            var session = JSON.parse(localStorage.getItem('session'));
            return session.rol;
        }

        function getUsuario() {
            var session = JSON.parse(localStorage.getItem('session'));
            return session.usuario;
        }

        function getEmpresa() {
            var session = JSON.parse(localStorage.getItem('session'));
            return session.empresa;
        }

        function getTipoDocumento() {
            var session = JSON.parse(localStorage.getItem('session'));
            return session.tipo_documento;
        }

        function getPermisos() {
            var session = JSON.parse(localStorage.getItem('session'));
            return session.permisos;
        }

        function getParametros() {
            var session = JSON.parse(localStorage.getItem('session'));
            return session.parametros;
        }

        function getTituloVista(opcionId) {
            var opcionesDelSistema = JSON.parse(localStorage.getItem('opcionesDelSistema')),
                opcion = _.find(opcionesDelSistema, function (opcionAux) {
                    return opcionAux.id == opcionId;
                });

            return opcion ? opcion.nombre : '';
        }

        function can(permisos) {
            var can = false,
                permisoAux = null,
                session = JSON.parse(localStorage.getItem('session'));

            angular.forEach(permisos, function (permisoAValidar) {
                permisoAux = _.find(session.permisos, function (permiso) {
                    return permiso === permisoAValidar;
                });

                if(permisoAux){
                    can = true;
                }
            });

            return can;
        }

    }

})();
