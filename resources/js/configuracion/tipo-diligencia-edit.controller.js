(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('tipoDiligenciaEditController', tipoDiligenciaEditController);

    tipoDiligenciaEditController.$inject = [
        '$uibModalInstance',
        'genericService',
        'messageUtil',
        'Constantes',
        'tipoDiligencia'
    ];

    function tipoDiligenciaEditController(
        $uibModalInstance, genericService, messageUtil, Constantes, tipoDiligencia
    ){
        var vm = this,
            recurso = "tipos-diligencias";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        if(tipoDiligencia && tipoDiligencia.id){
            cargar();
        }else{
            vm.estado = true;
        }

        function cargar() {
            genericService.cargar(recurso, tipoDiligencia.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var tipoDiligencia = response.data;
                        vm.nombre = tipoDiligencia.nombre;
                        vm.solicitudAutorizacion = tipoDiligencia.solicitud_autorizacion;
                        vm.estado = tipoDiligencia.estado;
                        vm.fechaCreacion = tipoDiligencia.fecha_creacion;
                        vm.fechaModificacion = tipoDiligencia.fecha_modificacion;
                        vm.usuarioCreacionNombre = tipoDiligencia.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = tipoDiligencia.usuario_modificacion_nombre;
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;
                var datos = {
                    id: tipoDiligencia ? +tipoDiligencia.id : null,
                    nombre: vm.nombre.toUpperCase(),
                    solicitud_autorizacion: vm.solicitudAutorizacion,
                    estado: vm.estado
                };

                var promesa = null;
                if(!(tipoDiligencia && tipoDiligencia.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        $uibModalInstance.close(response.data.datos);
                        messageUtil.success(response.data.mensajes[0]);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }
                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }
    }
})();
