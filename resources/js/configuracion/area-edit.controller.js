(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('areaEditController', areaEditController);

    areaEditController.$inject = [
        '$uibModalInstance',
        'genericService',
        'messageUtil',
        'Constantes',
        'parametros'
    ];

    function areaEditController(
        $uibModalInstance, genericService, messageUtil, Constantes, parametros
    ){
        var vm = this,
            recurso = "areas";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        // inicial autocomples
        initAutocompleteUsuario();

        if(parametros.area && parametros.area.id){
            cargar();
        }else{
            vm.estado = true;
        }

        function cargar() {
            genericService.cargar(recurso, parametros.area.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var area = response.data,
                            usuario = area.usuario;
                        vm.nombre = area.nombre;
                        vm.estado = area.estado;
                        vm.fechaCreacion = area.fecha_creacion;
                        vm.fechaModificacion = area.fecha_modificacion;
                        vm.usuarioCreacionNombre = area.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = area.usuario_modificacion_nombre;
                        if(usuario) {
                            vm.usuarioId = area.usuario_id;
                            vm.opcionesUsuario.push({id: usuario.id, nombre: usuario.nombre});
                        }
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;
                var datos = {
                    id: parametros.area ? +parametros.area.id : null,
                    nombre: vm.nombre.toUpperCase(),
                    empresa_id: parametros.empresa.id,
                    usuario_id: vm.usuarioId,
                    estado: vm.estado
                };

                var promesa = null;
                if(!(parametros.area && parametros.area.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        $uibModalInstance.close(response.data.datos);
                    }

                    messageUtil.success(response.data.mensajes[0]);
                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }

        function initAutocompleteUsuario() {
            vm.configUsuario = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
            };

            vm.opcionesUsuario = [];
            genericService.obtenerColeccionLigera("usuarios",{
                ligera: true,
                empresa_id: parametros.empresa.id
            })
                .then(function (response){
                    vm.opcionesUsuario = [].concat(response.data);
                });
        }
    }
})();
