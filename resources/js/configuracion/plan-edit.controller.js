(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('planEditController', planEditController);

    planEditController.$inject = [
        '$uibModalInstance',
        '$timeout',
        'genericService',
        'messageUtil',
        'Constantes',
        'parametros'
    ];

    function planEditController(
        $uibModalInstance, $timeout, genericService, messageUtil, Constantes, parametros
    ){
        var vm = this,
            recurso = "planes";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        // inicial autocomples
        initAutocompleteDepartamento();
        initAutocompleteCiudad();
        initAutocompleteServicio();

        if(parametros.plan && parametros.plan.id){
            cargar();
        }else{
            vm.estado = true;
        }

        function cargar() {
            genericService.cargar(recurso, parametros.plan.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK) {
                        var plan = response.data,
                            servicio = plan.servicio,
                            ciudad = plan.ciudad,
                            departamento = plan.departamento;
                        vm.cantidad = plan.cantidad;
                        vm.valor = plan.valor;
                        vm.observacion = plan.observacion;
                        vm.estado = plan.estado;
                        vm.fechaCreacion = plan.fecha_creacion;
                        vm.fechaModificacion = plan.fecha_modificacion;
                        vm.usuarioCreacionNombre = plan.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = plan.usuario_modificacion_nombre;
                        vm.departamentoId = plan.departamento_id;
                        vm.ciudadId = plan.ciudad_id;
                        vm.servicioId = plan.servicio_id;

                        if (ciudad) {
                            vm.departamentoId = departamento.id;
                            vm.opcionesDepartamentos.push({id: departamento.id, nombre: departamento.nombre});
                            vm.ciudadId = ciudad.id;
                            vm.opcionesCiudad.push({id: ciudad.id, nombre: ciudad.nombre});
                            vm.limpiarOpcionesCiudad = false;
                        }

                        if (servicio) {
                            vm.servicioId = servicio.id;
                            vm.opcionesServicios.push({id: servicio.id, nombre: servicio.nombre});
                        }

                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;
                var datos = {
                    id: parametros.plan ? +parametros.plan.id : null,
                    servicio_id: vm.servicioId,
                    departamento_id: vm.departamentoId,
                    ciudad_id: vm.ciudadId,
                    empresa_id: parametros.empresa.id,
                    cantidad: vm.cantidad,
                    valor: vm.valor,
                    observacion: vm.observacion,
                    estado: vm.estado
                };

                var promesa = null;
                if(!(parametros.plan && parametros.plan.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        $uibModalInstance.close(response.data.datos);
                        messageUtil.success(response.data.mensajes[0]);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }
                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }

        function initAutocompleteDepartamento() {
            vm.configDepartamento = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                onItemAdd: function (value, $item) {
                    if(vm.departamentoId !== value){
                        vm.opcionesCiudad = [];
                        $timeout(function () {
                            vm.limpiarOpcionesCiudad = true;
                            genericService.obtenerColeccionLigera("ciudades", {
                                ligera: true,
                                departamento_id: value
                            })
                            .then(function (response){
                                vm.opcionesCiudad = [].concat(response.data);
                            });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.opcionesCiudad = [];
                    vm.ciudadId = null;
                    $timeout(function () {
                        vm.limpiarOpcionesCiudad = true;
                    }, 100);
                }
            };

            vm.opcionesDepartamentos = [];
            genericService.obtenerColeccionLigera("departamentos",{
                ligera: true,
            })
            .then(function (response){
                vm.opcionesDepartamentos = [].concat(response.data);
            });
        }

        function initAutocompleteCiudad() {
            vm.configCiudad = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                lock: false,
            };
            vm.opcionesCiudad = [];
        }

        function initAutocompleteServicio() {
            vm.configServicio = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
            };

            vm.opcionesServicios = [];
            genericService.obtenerColeccionLigera("servicios",{
                ligera: true,
            })
                .then(function (response){
                    vm.opcionesServicios = [].concat(response.data);
                });
        }

    }
})();
