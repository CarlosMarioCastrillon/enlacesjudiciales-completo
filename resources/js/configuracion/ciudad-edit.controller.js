(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('ciudadEditController', ciudadEditController);

    ciudadEditController.$inject = [
        '$uibModalInstance',
        'genericService',
        'messageUtil',
        'Constantes',
        'ciudad'
    ];

    function ciudadEditController(
        $uibModalInstance, genericService, messageUtil, Constantes, ciudad
    ){
        var vm = this,
            recurso = "ciudades";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        // inicial autocomples
        initAutocompleteDepartamento();

        if(ciudad && ciudad.id){
            cargar();
        }else{
            vm.estado = true;
            vm.ramaJudicial = false;
        }

        function cargar() {
            genericService.cargar(recurso, ciudad.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var ciudad = response.data;
                        vm.nombre = ciudad.nombre;
                        vm.codigoDane = ciudad.codigo_dane;
                        vm.departamentoId = ciudad.departamento_id;
                        vm.estado = ciudad.estado;
                        vm.ramaJudicial = ciudad.rama_judicial;
                        vm.fechaCreacion = ciudad.fecha_creacion;
                        vm.fechaModificacion = ciudad.fecha_modificacion;
                        vm.usuarioCreacionNombre = ciudad.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = ciudad.usuario_modificacion_nombre;
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;
                var datos = {
                    id: ciudad ? +ciudad.id : null,
                    nombre: vm.nombre.toUpperCase(),
                    codigo_dane: vm.codigoDane,
                    departamento_id: vm.departamentoId,
                    estado: vm.estado,
                    rama_judicial: vm.ramaJudicial
                };

                var promesa = null;
                if(!(ciudad && ciudad.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        messageUtil.success(response.data.mensajes[0]);
                        $uibModalInstance.close(response.data.datos);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }


                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }

        function initAutocompleteDepartamento() {
            vm.configDepartamento = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
            };

            vm.opcionesDepartamento = [];
            genericService.obtenerColeccionLigera("departamentos",{
                ligera: true,
            })
            .then(function (response){
                vm.opcionesDepartamentos = [].concat(response.data);
            });
        }
    }
})();
