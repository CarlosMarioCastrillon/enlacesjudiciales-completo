(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('despachoController', despachoController);

    despachoController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        '$uibModal',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function despachoController(
        $scope, $routeParams, $timeout, $uibModal, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "despachos";

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.crear = crear;

        // Modulos externos
        vm.confirmarEliminar = confirmarEliminar;

        // Limpiar filtros
        limpiarFiltros();

        // Grid
        function obtenerResumen(tableState, ctrl){
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }
            if(pipePromise) $timeout.cancel(pipePromise);
            pipePromise = $timeout(function() {
                if(!vm.cargando){
                    vm.cargando = true;
                    genericService.obtenerColeccion(recurso, tableState)
                        .then(function (response){
                            if(response.status === Constantes.Response.HTTP_OK){
                                var coleccion = response.data.datos;
                                if(coleccion && coleccion.length > 0 ){
                                    vm.coleccion = [].concat(coleccion);
                                    vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                                    vm.paginacion.paginaActual = response.data.pagina_actual;
                                    vm.paginacion.desde = response.data.desde;
                                    vm.paginacion.hasta = response.data.hasta;
                                    vm.paginacion.total = response.data.total;
                                    tableState.pagination.numberOfPages = response.data.ultima_pagina;
                                }else{
                                    vm.coleccion = [];
                                }
                            }
                        }).finally(function () {
                            vm.cargando = false;
                        });
                }
            }, 500);
        }

        function limpiarFiltros(){
            vm.nombre = null;
            vm.ciudad = null;
            $scope.isReset = true;
        }

        /*function cargarFiltros() {
            var filtros = gridUtil.getFormattedSearchPredicate("despacho");
            vm.nombre = filtros && filtros.nombre ? filtros.nombre : null;
        }*/

        // Acciones

        function eliminar(row){
            genericService.eliminar(recurso, row.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        $scope.$broadcast('refreshGrid');
                        messageUtil.success(response.data.mensajes[0]);
                    }else if(
                        response.status === Constantes.Response.HTTP_CONFLICT ||
                        response.status === Constantes.Response.HTTP_BAD_REQUEST
                    ){
                        messageUtil.error(response.data.mensajes[0]);
                    }
                });
        }

        // Modulos externos

        function crear(despacho) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + recurso + '/create',
                controller: 'despachoEditController as vm',
                size: 'md',
                resolve: {
                    despacho: function () {
                        return despacho;
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(despacho && despacho.id && response && response.id){
                    despacho.nombre = response.nombre;
                    despacho.departamento_nombre = response.departamento.nombre;
                    despacho.ciudad_nombre = response.ciudad.nombre;
                    despacho.juzgado = response.juzgado;
                    despacho.sala = response.sala;
                    despacho.despacho = response.despacho;
                    despacho.estado = response.estado;
                    despacho.usuario_modificacion_id = response.usuario_modificacion_id;
                    despacho.usuario_modificacion_nombre = response.usuario_modificacion_nombre;
                    despacho.fecha_creacion = response.fecha_creacion;
                    despacho.fecha_modificacion = response.fecha_modificacion;
                }else if(response && response.id){
                    $scope.$broadcast('refreshGrid');
                }
            });
        }

        function confirmarEliminar(row) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + 'confirmar-template',
                controller: 'confirmarController as vm',
                size: 'md',
                resolve: {
                    parametros: function () {
                        return {
                            mensaje: "¿Estás seguro de que quieres eliminar el despacho?"
                        };
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(response.ok){
                    eliminar(row);
                }
            });
        }

    }
})();
