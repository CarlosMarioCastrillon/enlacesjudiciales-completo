(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('tipoDocumentoEditController', tipoDocumentoEditController);

    tipoDocumentoEditController.$inject = [
        '$uibModalInstance',
        'genericService',
        'messageUtil',
        'Constantes',
        'tipoDocumento'
    ];

    function tipoDocumentoEditController(
        $uibModalInstance, genericService, messageUtil, Constantes, tipoDocumento
    ){
        var vm = this,
            recurso = "tipos-documentos";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        if(tipoDocumento && tipoDocumento.id){
            cargar();
        }else{
            vm.estado = true;
        }

        function cargar() {
            genericService.cargar(recurso, tipoDocumento.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var tipoDocumento = response.data;
                        vm.codigo = tipoDocumento.codigo;
                        vm.nombre = tipoDocumento.nombre;
                        vm.estado = tipoDocumento.estado;
                        vm.fechaCreacion = tipoDocumento.fecha_creacion;
                        vm.fechaModificacion = tipoDocumento.fecha_modificacion;
                        vm.usuarioCreacionNombre = tipoDocumento.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = tipoDocumento.usuario_modificacion_nombre;
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;
                var datos = {
                    id: tipoDocumento ? +tipoDocumento.id : null,
                    codigo: vm.codigo.toUpperCase(),
                    nombre: vm.nombre.toUpperCase(),
                    estado: vm.estado
                };

                var promesa = null;
                if(!(tipoDocumento && tipoDocumento.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        $uibModalInstance.close(response.data.datos);
                        messageUtil.success(response.data.mensajes[0]);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }
                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }
    }
})();
