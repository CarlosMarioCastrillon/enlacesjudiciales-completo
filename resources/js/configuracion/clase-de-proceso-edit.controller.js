(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('claseProcesoEditController', claseProcesoEditController);

    claseProcesoEditController.$inject = [
        '$uibModalInstance',
        'genericService',
        'messageUtil',
        'Constantes',
        'claseProceso'
    ];

    function claseProcesoEditController(
        $uibModalInstance, genericService, messageUtil, Constantes, claseProceso
    ){
        var vm = this,
            recurso = "clases-de-procesos";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        if(claseProceso && claseProceso.id){
            cargar();
        }else{
            vm.estado = true;
        }

        function cargar() {
            genericService.cargar(recurso, claseProceso.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var claseProceso = response.data;
                        vm.nombre = claseProceso.nombre;
                        vm.estado = claseProceso.estado;
                        vm.fechaCreacion = claseProceso.fecha_creacion;
                        vm.fechaModificacion = claseProceso.fecha_modificacion;
                        vm.usuarioCreacionNombre = claseProceso.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = claseProceso.usuario_modificacion_nombre;
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;
                var datos = {
                    id: claseProceso ? +claseProceso.id : null,
                    nombre: vm.nombre.toUpperCase(),
                    estado: vm.estado
                };

                var promesa = null;
                if(!(claseProceso && claseProceso.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        $uibModalInstance.close(response.data.datos);
                    }

                    messageUtil.success(response.data.mensajes[0]);
                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }
    }
})();
