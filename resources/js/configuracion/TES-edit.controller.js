(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('tesEditController', tesEditController);

    tesEditController.$inject = [
        '$uibModalInstance',
        'genericService',
        'messageUtil',
        'Constantes',
        'tes'
    ];

    function tesEditController(
        $uibModalInstance, genericService, messageUtil, Constantes, tes
    ){
        var vm = this,
            recurso = "TES";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        if(tes && tes.id){
            cargar();
            // var today = moment().millisecond(0).second(0).minute(0).hour(0),
            //     fechaMaxima = moment(tes.fecha).millisecond(0).second(0).minute(0).hour(0);
            //
            // vm.datetimepickerOptions = {
            //     useCurrent: false,
            //     maxDate: fechaMaxima.diff(today, 'days') >= 0 ? today : fechaMaxima,
            //     format: 'YYYY-MM-DD'
            // };
        }else{
            vm.estado = true;
        }

        function cargar() {
            genericService.cargar(recurso, tes.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var tes = response.data;
                        vm.fecha = tes.fecha;
                        vm.porcentaje1Anio = tes.porcentaje_1anio;
                        vm.porcentaje5Anio = tes.porcentaje_5anio;
                        vm.porcentaje10Anio = tes.porcentaje_10anio;
                        vm.estado = tes.estado;
                        vm.fechaCreacion = tes.fecha_creacion;
                        vm.fechaModificacion = tes.fecha_modificacion;
                        vm.usuarioCreacionNombre = tes.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = tes.usuario_modificacion_nombre;
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;
                var datos = {
                    id: tes ? +tes.id : null,
                    fecha: vm.fecha,
                    porcentaje_1anio: vm.porcentaje1Anio,
                    porcentaje_5anio: vm.porcentaje5Anio,
                    porcentaje_10anio: vm.porcentaje10Anio,
                    estado: vm.estado
                };

                var promesa = null;
                if(!(tes && tes.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        $uibModalInstance.close(response.data.datos);
                        messageUtil.success(response.data.mensajes[0]);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }
                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }
    }
})();
