(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('despachoEditController', despachoEditController);

    despachoEditController.$inject = [
        '$uibModalInstance',
        '$timeout',
        'genericService',
        'messageUtil',
        'Constantes',
        'despacho'
    ];

    function despachoEditController(
        $uibModalInstance, $timeout, genericService, messageUtil, Constantes, despacho
    ){
        var vm = this,
            recurso = "despachos";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        // inicial autocomples
        initAutocompleteDepartamento();
        initAutocompleteCiudad();
        initAutocompleteDespacho();

        if(despacho && despacho.id){
            cargar();
        }else{
            vm.estado = true;
            vm.despachoActual = true;
        }

        function cargar() {
            genericService.cargar(recurso, despacho.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var despacho = response.data,
                            ciudad = despacho.ciudad,
                            departamento = despacho.departamento;

                        vm.nombre = despacho.nombre;
                        vm.juzgado = despacho.juzgado;
                        vm.sala = despacho.sala;
                        vm.despacho = despacho.despacho;
                        vm.estado = despacho.estado;
                        vm.fechaCreacion = despacho.fecha_creacion;
                        vm.fechaModificacion = despacho.fecha_modificacion;
                        vm.usuarioCreacionNombre = despacho.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = despacho.usuario_modificacion_nombre;

                        if(ciudad){
                            vm.departamentoId = departamento.id;
                            vm.opcionesDepartamento.push({id: departamento.id, nombre: departamento.nombre});
                            vm.ciudadId = ciudad.id;
                            vm.opcionesCiudad.push({id: ciudad.id, nombre: ciudad.nombre});
                            vm.limpiarOpcionesCiudad = false;
                        }
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;

                var datos = {
                    id: despacho ? +despacho.id : null,
                    nombre: vm.nombre.toUpperCase(),
                    departamento_id: vm.departamentoId,
                    ciudad_id: vm.ciudadId,
                    juzgado: vm.juzgado,
                    sala: vm.sala,
                    despacho: vm.despacho,
                    estado: vm.estado

            };

                var promesa = null;
                if(!(despacho && despacho.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        messageUtil.success(response.data.mensajes[0]);
                        $uibModalInstance.close(response.data.datos);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }


                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }

        function initAutocompleteDespacho() {
            vm.configDespacho = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                render: {
                    item: function(item, escape) {
                        return '<div data-nombre="' + escape(item.nombre) +
                            '" data-sala="' + escape(item.sala) +
                            '" data-juzgado="' + escape(item.juzgado) +
                            '" data-despacho="' + escape(item.despacho) +
                            '">' + escape(item.nombre) + '</div>';
                    }
                },
                onItemAdd: function (value, $item) {
                    if(vm.despachoReferencia !== value){
                        vm.nombre = $item[0].dataset.nombre;
                        vm.sala = $item[0].dataset.sala;
                        vm.juzgado = $item[0].dataset.juzgado;
                        vm.despacho =  $item[0].dataset.despacho;
                    }
                },
                onItemRemove: function () {
                    vm.nombre = null;
                    vm.sala = null;
                    vm.juzgado = null;
                    vm.despacho = null;
                }
            };

            vm.opcionesDespachos = [];
            genericService.obtenerColeccionLigera("despachos",{
                de_referencia: true,
            })
                .then(function (response){
                    vm.opcionesDespachos = [].concat(response.data);
                });
        }

        function initAutocompleteDepartamento() {
            vm.configDepartamento = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                onItemAdd: function (value, $item) {
                    if(vm.departamentoId !== value){
                        vm.opcionesCiudad = [];
                        $timeout(function () {
                            vm.limpiarOpcionesCiudad = true;
                            genericService.obtenerColeccionLigera("ciudades", {
                                ligera: true,
                                departamento_id: value
                            })
                            .then(function (response){
                                vm.opcionesCiudad = [].concat(response.data);
                            });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.opcionesCiudad = [];
                    vm.ciudadId = null;
                    $timeout(function () {
                        vm.limpiarOpcionesCiudad = true;
                    }, 100);
                }
            };

            vm.opcionesDepartamento = [];
            genericService.obtenerColeccionLigera("departamentos",{
                ligera: true,
            })
            .then(function (response){
                vm.opcionesDepartamentos = [].concat(response.data);
            });
        }

        function initAutocompleteCiudad() {
            vm.configCiudad = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                lock: false,
            };
            vm.opcionesCiudad = [];
        }
    }
})();
