(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('tipoActuacionEditController', tipoActuacionEditController);

    tipoActuacionEditController.$inject = [
        '$uibModalInstance',
        'genericService',
        'messageUtil',
        'Constantes',
        'tipoActuacion'
    ];

    function tipoActuacionEditController(
        $uibModalInstance, genericService, messageUtil, Constantes, tipoActuacion
    ){
        var vm = this,
            recurso = "tipos-actuaciones";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        if(tipoActuacion && tipoActuacion.id){
            cargar();
        }else{
            vm.estado = true;
        }

        function cargar() {
            genericService.cargar(recurso, tipoActuacion.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var tipoActuacion = response.data;
                        vm.nombre = tipoActuacion.nombre;
                        vm.estado = tipoActuacion.estado;
                        vm.fechaCreacion = tipoActuacion.fecha_creacion;
                        vm.fechaModificacion = tipoActuacion.fecha_modificacion;
                        vm.usuarioCreacionNombre = tipoActuacion.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = tipoActuacion.usuario_modificacion_nombre;
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;
                var datos = {
                    id: tipoActuacion ? +tipoActuacion.id : null,
                    nombre: vm.nombre.toUpperCase(),
                    estado: vm.estado
                };

                var promesa = null;
                if(!(tipoActuacion && tipoActuacion.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        $uibModalInstance.close(response.data.datos);
                        messageUtil.success(response.data.mensajes[0]);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }
                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }
    }
})();
