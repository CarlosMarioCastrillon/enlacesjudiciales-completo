(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('salarioMinimoEditController', salarioMinimoEditController);

    salarioMinimoEditController.$inject = [
        '$uibModalInstance',
        'genericService',
        'messageUtil',
        'Constantes',
        'salarioMinimo'
    ];

    function salarioMinimoEditController(
        $uibModalInstance, genericService, messageUtil, Constantes, salarioMinimo
    ){
        var vm = this,
            recurso = "salarios-minimos";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        if(salarioMinimo && salarioMinimo.id){
            cargar();
        }else{
            vm.estado = true;
        }

        function cargar() {
            genericService.cargar(recurso, salarioMinimo.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var salarioMinimo = response.data;
                        vm.fecha = salarioMinimo.fecha;
                        vm.valor = salarioMinimo.valor_salario_min;
                        vm.estado = salarioMinimo.estado;
                        vm.fechaCreacion = salarioMinimo.fecha_creacion;
                        vm.fechaModificacion = salarioMinimo.fecha_modificacion;
                        vm.usuarioCreacionNombre = salarioMinimo.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = salarioMinimo.usuario_modificacion_nombre;
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;
                var datos = {
                    id: salarioMinimo ? +salarioMinimo.id : null,
                    fecha: vm.fecha,
                    valor_salario_min: vm.valor,
                    estado: vm.estado
                };

                var promesa = null;
                if(!(salarioMinimo && salarioMinimo.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        $uibModalInstance.close(response.data.datos);
                        messageUtil.success(response.data.mensajes[0]);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }
                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                    //location.reload();
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }
    }
})();
