(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('empresaEditController', empresaEditController);

    empresaEditController.$inject = [
        '$uibModalInstance',
        '$timeout',
        'genericService',
        'messageUtil',
        'Constantes',
        'empresa'
    ];

    function empresaEditController(
        $uibModalInstance, $timeout, genericService, messageUtil, Constantes, empresa
    ){
        var vm = this,
            recurso = "empresas";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        // inicial autocomples
        initAutocompleteDepartamento();
        initAutocompleteCiudad();
        initAutocompleteTipoDocumento();
        initAutocompleteTipoEmpresa();
        initAutocompleteZona();

        if(empresa && empresa.id){
            vm.id = empresa.id;
            cargar();
            var today = moment().millisecond(0).second(0).minute(0).hour(0),
                fechaVencimiento = moment(empresa.fecha_vencimiento).millisecond(0).second(0).minute(0).hour(0);

            vm.datetimepickerOptions = {
                useCurrent: false,
                minDate: fechaVencimiento.diff(today, 'days') >= 0 ? today : fechaVencimiento,
                format: 'YYYY-MM-DD'
            };
        }else{
            vm.estado = true;
            vm.actuacionDigital = false;
            vm.vigilanciaProceso = false;
            vm.dataproceso = false;
            vm.datetimepickerOptions = {
                useCurrent: false,
                minDate: 'now',
                format: 'YYYY-MM-DD'
            };
        }

        function cargar() {
            genericService.cargar(recurso, empresa.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var empresa = response.data,
                            ciudad = empresa.ciudad,
                            departamento = empresa.departamento,
                            tipo_documento = empresa.tipo_documento,
                            tipo_empresa = empresa.tipo_empresa,
                            zona = empresa.zona;

                        vm.nombre = empresa.nombre;
                        vm.numeroDocumento = empresa.numero_documento;
                        vm.direccion = empresa.direccion;
                        vm.telefonoFijo = empresa.telefono_uno;
                        vm.telefonoCelular = empresa.telefono_dos;
                        vm.telefonoOtro = empresa.telefono_tres;

                        vm.correo = empresa.email;
                        vm.vigilanciaProceso = empresa.vigilancia;
                        vm.actuacionDigital = empresa.actuaciones_digitales;
                        vm.dataproceso = empresa.dataprocesos;
                        vm.fechaVencimiento = empresa.fecha_vencimiento;
                        vm.observacion = empresa.observacion;
                        vm.estado = empresa.estado;
                        vm.fechaCreacion = empresa.fecha_creacion;
                        vm.fechaModificacion = empresa.fecha_modificacion;
                        vm.usuarioCreacionNombre = empresa.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = empresa.usuario_modificacion_nombre;

                        if(ciudad){
                            vm.departamentoId = departamento.id;
                            vm.opcionesDepartamentos.push({id: departamento.id, nombre: departamento.nombre});
                            vm.ciudadId = ciudad.id;
                            vm.opcionesCiudad.push({id: ciudad.id, nombre: ciudad.nombre});
                            vm.limpiarOpcionesCiudad = false;
                        }
                        if(tipo_documento){
                            vm.tipoDocumentoId = tipo_documento.id;
                            vm.opcionesTipoDocumento.push({id: tipo_documento.id, codigo: tipo_documento.codigo});
                        }
                        if(tipo_empresa){
                            vm.tipoEmpresaId = tipo_empresa.id;
                            vm.opcionesTipoEmpresa.push({id: tipo_empresa.id, nombre: tipo_empresa.nombre});
                        }
                        if(zona){
                            vm.zonaId = zona.id;
                            vm.opcionesZona.push({id: zona.id, nombre: zona.nombre});
                        }
                    }
                });
        }

        function guardar() {
            // Formatear datos
            vm.numeroDocumento = !!vm.numeroDocumento ? vm.numeroDocumento.trim().replace(/\s/g, '') : vm.numeroDocumento;

            // Validaciones
            var conError = false;
            if(!(!!vm.telefonoFijo && vm.telefonoFijo !== "" || !!vm.telefonoCelular && vm.telefonoCelular !== "")){
                conError = true;
                messageUtil.error("Debe ingresar el Teléfono fijo o el Teléfono celular.");
            }

            vm.guardando = true;
            if(!conError && vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;

                var datos = {
                    id: empresa ? +empresa.id : null,
                    nombre: vm.nombre.toUpperCase(),
                    tipo_documento_id: vm.tipoDocumentoId,
                    numero_documento: vm.numeroDocumento,
                    departamento_id: vm.departamentoId,
                    ciudad_id: vm.ciudadId,
                    direccion: vm.direccion,
                    tipo_empresa_id: vm.tipoEmpresaId,
                    zona_id: vm.zonaId,
                    telefono_uno: vm.telefonoFijo,
                    telefono_dos: vm.telefonoCelular,
                    telefono_tres: vm.telefonoOtro,
                    email: vm.correo,
                    fecha_vencimiento: vm.fechaVencimiento,
                    con_actuaciones_digitales: vm.actuacionDigital,
                    con_vigilancia: vm.vigilanciaProceso,
                    con_data_procesos: vm.dataproceso,
                    observacion: vm.observacion,
                    clave: vm.clave,
                    estado: vm.estado
                };

                var promesa = null;
                if(!(empresa && empresa.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        messageUtil.success(response.data.mensajes[0]);
                        $uibModalInstance.close(response.data.datos);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }


                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }

            if(!vm.form.$valid){
                messageUtil.error("Se presentaron errores en el formulario.");
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }

        function initAutocompleteDepartamento() {
            vm.configDepartamento = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                onItemAdd: function (value, $item) {
                    if(vm.departamentoId !== value){
                        vm.opcionesCiudad = [];
                        $timeout(function () {
                            vm.limpiarOpcionesCiudad = true;
                            genericService.obtenerColeccionLigera("ciudades", {
                                ligera: true,
                                departamento_id: value
                            })
                            .then(function (response){
                                vm.opcionesCiudad = [].concat(response.data);
                            });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.opcionesCiudad = [];
                    vm.ciudadId = null;
                    $timeout(function () {
                        vm.limpiarOpcionesCiudad = true;
                    }, 100);
                }
            };

            vm.opcionesDepartamentos = [];
            genericService.obtenerColeccionLigera("departamentos",{
                ligera: true,
            })
            .then(function (response){
                vm.opcionesDepartamentos = [].concat(response.data);
            });
        }

        function initAutocompleteCiudad() {
            vm.configCiudad = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                lock: false,
            };
            vm.opcionesCiudad = [];
        }

        function initAutocompleteTipoDocumento() {
            vm.configTipoDocumento = {
                valueField: 'id',
                labelField: 'codigo',
                searchField: ['codigo'],
                sortField: 'codigo',
                allowEmptyOption: false,
                create: false,
                persist: true,
            };

            vm.opcionesTipoDocumento = [];
            genericService.obtenerColeccionLigera("tipos-documentos",{
                ligera: true,
            })
            .then(function (response){
                vm.opcionesTipoDocumento = [].concat(response.data);
            });
        }

        function initAutocompleteTipoEmpresa() {
            vm.configTipoEmpresa = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
            };

            vm.opcionesTipoEmpresa = [];
            genericService.obtenerColeccionLigera("tipos-de-empresa",{
                ligera: true,
            })
            .then(function (response){
                vm.opcionesTipoEmpresa = [].concat(response.data);
            });
        }

        function initAutocompleteZona() {
            vm.configZona = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
            };

            vm.opcionesZona = [];
            genericService.obtenerColeccionLigera("zonas",{
                ligera: true,
            })
            .then(function (response){
                vm.opcionesZona = [].concat(response.data);
            });
        }

    }
})();
