(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('servicioEditController', servicioEditController);

    servicioEditController.$inject = [
        '$uibModalInstance',
        'genericService',
        'messageUtil',
        'Constantes',
        'servicio'
    ];

    function servicioEditController(
        $uibModalInstance, genericService, messageUtil, Constantes, servicio
    ){
        var vm = this,
            recurso = "servicios";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        if(servicio && servicio.id){
            cargar();
        }else{
            vm.estado = true;
        }

        function cargar() {
            genericService.cargar(recurso, servicio.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var servicio = response.data;
                        vm.nombre = servicio.nombre;
                        vm.estado = servicio.estado;
                        vm.fechaCreacion = servicio.fecha_creacion;
                        vm.fechaModificacion = servicio.fecha_modificacion;
                        vm.usuarioCreacionNombre = servicio.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = servicio.usuario_modificacion_nombre;
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;
                var datos = {
                    id: servicio ? +servicio.id : null,
                    nombre: vm.nombre.toUpperCase(),
                    estado: vm.estado
                };

                var promesa = null;
                if(!(servicio && servicio.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        $uibModalInstance.close(response.data.datos);
                        messageUtil.success(response.data.mensajes[0]);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }
                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }
    }
})();
