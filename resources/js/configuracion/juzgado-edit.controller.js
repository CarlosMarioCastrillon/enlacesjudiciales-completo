(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('juzgadoEditController', juzgadoEditController);

    juzgadoEditController.$inject = [
        '$uibModalInstance',
        '$timeout',
        'genericService',
        'messageUtil',
        'Constantes',
        'juzgado'
    ];

    function juzgadoEditController(
        $uibModalInstance, $timeout, genericService, messageUtil, Constantes, juzgado
    ){
        var vm = this,
            recurso = "juzgados";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        // inicial autocomples
        initAutocompleteDepartamento();
        initAutocompleteCiudad();
        initAutocompleteJuzgado();

        if(juzgado && juzgado.id){
            cargar();
        }else{
            vm.estado = true;
            vm.juzgadoActual = true;
        }

        function cargar() {
            genericService.cargar(recurso, juzgado.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var juzgado = response.data,
                            ciudad = juzgado.ciudad,
                            departamento = juzgado.departamento;

                        vm.nombre = juzgado.nombre;
                        vm.juzgado = juzgado.juzgado;
                        vm.sala = juzgado.sala;
                        vm.juzgadoRama = juzgado.juzgado_rama;
                        vm.juzgadoActual = juzgado.juzgado_actual;
                        vm.estado = juzgado.estado;
                        vm.fechaCreacion = juzgado.fecha_creacion;
                        vm.fechaModificacion = juzgado.fecha_modificacion;
                        vm.usuarioCreacionNombre = juzgado.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = juzgado.usuario_modificacion_nombre;

                        if(ciudad){
                            vm.departamentoId = departamento.id;
                            vm.opcionesCiudad.push({id: ciudad.id, nombre: ciudad.nombre});
                            vm.ciudadId = ciudad.id;
                            vm.opcionesDepartamento.push({id: departamento.id, nombre: departamento.nombre});
                            vm.limpiarOpcionesCiudad = false;
                        }
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;

                var datos = {
                    id: juzgado ? +juzgado.id : null,
                    nombre: vm.nombre.toUpperCase(),
                    departamento_id: vm.departamentoId,
                    ciudad_id: vm.ciudadId,
                    juzgado: vm.juzgado,
                    sala: vm.sala,
                    juzgado_actual: vm.juzgadoActual,
                    estado: vm.estado

            };

                var promesa = null;
                if(!(juzgado && juzgado.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        messageUtil.success(response.data.mensajes[0]);
                        $uibModalInstance.close(response.data.datos);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }


                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }

        function initAutocompleteJuzgado() {
            vm.configJuzgado = {
                valueField: 'juzgado_rama',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                render: {
                    item: function(item, escape) {
                        return '<div data-juzgado-rama="' + escape(item.juzgado_rama) +
                            '" data-nombre="' + escape(item.nombre) +
                            '">' + escape(item.nombre) + '</div>';
                    }
                },
                onItemAdd: function (value, $item) {
                    if(vm.juzgadoReferencia !== value){
                        var juzgadoRama = $item[0].dataset.juzgadoRama,
                            juzgadoNombre = $item[0].dataset.nombre;
                        vm.nombre = juzgadoNombre;
                        vm.sala = juzgadoRama.substr(7,2);
                        vm.juzgado = juzgadoRama.substr(5,2);
                    }
                },
                onItemRemove: function () {
                    vm.nombre = null;
                    vm.sala = null;
                    vm.juzgado = null;
                }
            };

            vm.opcionesJuzgado = [];
            genericService.obtenerColeccionLigera("juzgados",{
                de_referencia: true
            })
            .then(function (response){
                vm.opcionesJuzgado = [].concat(response.data);
            });
        }

        function initAutocompleteDepartamento() {
            vm.configDepartamento = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                onItemAdd: function (value, $item) {
                    if(vm.departamentoId !== value){
                        vm.opcionesCiudad = [];
                        $timeout(function () {
                            vm.limpiarOpcionesCiudad = true;
                            genericService.obtenerColeccionLigera("ciudades", {
                                ligera: true,
                                departamento_id: value
                            })
                            .then(function (response){
                                vm.opcionesCiudad = [].concat(response.data);
                            });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.opcionesCiudad = [];
                    vm.ciudadId = null;
                    $timeout(function () {
                        vm.limpiarOpcionesCiudad = true;
                    }, 100);
                }
            };

            vm.opcionesDepartamentos = [];
            genericService.obtenerColeccionLigera("departamentos",{
                ligera: true,
            })
            .then(function (response){
                vm.opcionesDepartamentos = [].concat(response.data);
            });
        }

        function initAutocompleteCiudad() {
            vm.configCiudad = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                lock: false,
            };
            vm.opcionesCiudad = [];
        }
    }
})();
