(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('tarifaDiligenciaEditController', tarifaDiligenciaEditController);

    tarifaDiligenciaEditController.$inject = [
        '$uibModalInstance',
        '$timeout',
        'genericService',
        'messageUtil',
        'Constantes',
        'tarifaDiligencia'
    ];

    function tarifaDiligenciaEditController(
        $uibModalInstance, $timeout, genericService, messageUtil, Constantes, tarifaDiligencia
    ){
        var vm = this,
            recurso = "tarifas-diligencias";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        // inicial autocomples
        initAutocompleteDepartamento();
        initAutocompleteCiudad();
        initAutocompleteTipoDiligencia();

        if(tarifaDiligencia && tarifaDiligencia.id){
            cargar();
        }else{
            vm.estado = true;
            vm.tarifaDiligenciaActual = true;
        }

        function cargar() {
            genericService.cargar(recurso, tarifaDiligencia.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var tarifaDiligencia = response.data,
                            ciudad = tarifaDiligencia.ciudad,
                            departamento = tarifaDiligencia.departamento,
                            tipoDiligencia = tarifaDiligencia.tipo_diligencia;

                        vm.valorDiligencia = tarifaDiligencia.valor_diligencia;
                        vm.gastoEnvio = tarifaDiligencia.gasto_envio;
                        vm.otrosGastos = tarifaDiligencia.otros_gastos;
                        vm.estado = tarifaDiligencia.estado;
                        vm.fechaCreacion = tarifaDiligencia.fecha_creacion;
                        vm.fechaModificacion = tarifaDiligencia.fecha_modificacion;
                        vm.usuarioCreacionNombre = tarifaDiligencia.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = tarifaDiligencia.usuario_modificacion_nombre;

                        if(tipoDiligencia) {
                            vm.tipoDiligenciaId = tipoDiligencia.id;
                            vm.opcionesTipoDiligencia.push({id: tipoDiligencia.id, nombre: tipoDiligencia.nombre});
                        }
                        if(ciudad){
                            vm.departamentoId = departamento.id;
                            vm.opcionesDepartamento.push({id: departamento.id, nombre: departamento.nombre});
                            vm.ciudadId = ciudad.id;
                            vm.opcionesCiudad.push({id: ciudad.id, nombre: ciudad.nombre});
                            vm.limpiarOpcionesCiudad = false;
                        }
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;

                var datos = {
                    id: tarifaDiligencia ? +tarifaDiligencia.id : null,
                    tipo_diligencia_id: vm.tipoDiligenciaId,
                    departamento_id: vm.departamentoId,
                    ciudad_id: vm.ciudadId,
                    valor_diligencia: vm.valorDiligencia,
                    gasto_envio: vm.gastoEnvio,
                    otros_gastos: vm.otrosGastos,
                    estado: vm.estado

            };

                var promesa = null;
                if(!(tarifaDiligencia && tarifaDiligencia.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        messageUtil.success(response.data.mensajes[0]);
                        $uibModalInstance.close(response.data.datos);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }


                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }

        function initAutocompleteTipoDiligencia() {
            vm.configTipoDiligencia = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
            };

            vm.opcionesTipoDiligencia = [];
            genericService.obtenerColeccionLigera("tipos-diligencias",{
                ligera: true,
            })
                .then(function (response){
                    vm.opcionesTipoDiligencia = [].concat(response.data);
                });
        }

        function initAutocompleteDepartamento() {
            vm.configDepartamento = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                onItemAdd: function (value, $item) {
                    if(vm.departamentoId !== value){
                        vm.opcionesCiudad = [];
                        $timeout(function () {
                            vm.limpiarOpcionesCiudad = true;
                            genericService.obtenerColeccionLigera("ciudades", {
                                ligera: true,
                                departamento_id: value
                            })
                            .then(function (response){
                                vm.opcionesCiudad = [].concat(response.data);
                            });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.opcionesCiudad = [];
                    vm.ciudadId = null;
                    $timeout(function () {
                        vm.limpiarOpcionesCiudad = true;
                    }, 100);
                }
            };

            vm.opcionesDepartamento = [];
            genericService.obtenerColeccionLigera("departamentos",{
                ligera: true,
            })
            .then(function (response){
                vm.opcionesDepartamentos = [].concat(response.data);
            });
        }

        function initAutocompleteCiudad() {
            vm.configCiudad = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                lock: false,
            };
            vm.opcionesCiudad = [];
        }
    }
})();
