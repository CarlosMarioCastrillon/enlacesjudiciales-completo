(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('coberturaEditController', coberturaEditController);

    coberturaEditController.$inject = [
        '$uibModalInstance',
        '$timeout',
        'genericService',
        'messageUtil',
        'Constantes',
        'parametros'
    ];

    function coberturaEditController(
        $uibModalInstance, $timeout, genericService, messageUtil, Constantes, parametros
    ){
        var vm = this;

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;
        vm.seleccionar = seleccionar;
        vm.seleccionarTodo = seleccionarTodo;
        vm.deseleccionar = deseleccionar;
        vm.deseleccionarTodo = deseleccionarTodo;

        // inicial autocomples
        initAutocompleteDepartamento();

        // Cargar coberturas
        cargar();
        if(parametros.cobertura && parametros.cobertura.departamento_id){
            vm.departamentoId = parametros.cobertura.departamento_id;
        }

        function cargar() {
            genericService.cargar("empresas", parametros.empresa.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK) {
                        // Coberturas actuales
                        vm.ciudadesEnCobertura = response.data.ciudades_en_cobertura;
                        var departamento = response.data.departamentos_en_cobertura;
                        if(vm.ciudadesEnCobertura) {
                            vm.departamentoId = departamento.id;
                            vm.opcionesDepartamento.push({id: departamento.id, nombre: departamento.nombre});
                        }
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;

                var ciudadesIds = [];
                if(vm.ciudadesCobertura && vm.ciudadesCobertura.length > 0){
                    vm.ciudadesCobertura.forEach(function (ciudadCobertura) {
                        ciudadesIds.push(ciudadCobertura.id);
                    });
                }

                var datos = {
                    departamento_id: vm.departamentoId,
                    ciudad_ids: ciudadesIds
                };

                var promesa = genericService.modificar("empresas/" + parametros.empresa.id + "/coberturas", datos);
                promesa.then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        messageUtil.success(response.data.mensajes[0]);
                        $uibModalInstance.close();
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }

                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }

        function seleccionar(){
            vm.ciudades = vm.ciudades || [];
            vm.ciudadesCobertura = vm.ciudadesCobertura || [];
            vm.ciudades.forEach(function (ciudad) {
                if(_.includes(vm.ciudadesSinSeleccionar, ciudad.id + "")){
                    vm.ciudadesCobertura.push(ciudad);
                }
            });
            _.remove(vm.ciudades, function (ciudad) {
                return _.includes(vm.ciudadesSinSeleccionar, ciudad.id + "");
            });

            vm.ciudadesSinSeleccionar = [];
        }

        function deseleccionar(){
            vm.ciudades = vm.ciudades || [];
            vm.ciudadesCobertura = vm.ciudadesCobertura || [];
            vm.ciudadesCobertura.forEach(function (ciudadCobertura) {
                if(_.includes(vm.ciudadesSeleccionadas, ciudadCobertura.id + "")){
                    vm.ciudades.push(ciudadCobertura);
                }
            });
            _.remove(vm.ciudadesCobertura, function (ciudadCobertura) {
                return _.includes(vm.ciudadesSeleccionadas, ciudadCobertura.id + "");
            });
            vm.ciudadesSeleccionadas = [];
        }

        function seleccionarTodo(){
            vm.ciudadesCobertura =  _.union(vm.ciudades, vm.ciudadesCobertura);
            vm.ciudades = [];
            vm.ciudadesSinSeleccionar = [];
            vm.ciudadesSeleccionadas = [];
        }

        function deseleccionarTodo(){
            vm.ciudades =  _.union(vm.ciudadesCobertura, vm.ciudades);
            vm.ciudadesCobertura = [];
            vm.ciudadesSinSeleccionar = [];
            vm.ciudadesSeleccionadas = [];
        }

        // Autocompletes

        function initAutocompleteDepartamento() {
            vm.configDepartamento = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                onItemAdd: function (value, $item) {
                    if(vm.departamentoId !== value && !vm.limpiarOpcionesCiudad){
                        vm.ciudades = [];
                        vm.ciudadesCobertura = [];
                        $timeout(function () {
                            vm.cargandoCiudades = true;
                            genericService.obtenerColeccionLigera("ciudades", {
                                ligera: true,
                                departamento_id: value
                            })
                            .then(function (response){
                                var ciudades = response.data;
                                if(ciudades && ciudades.length > 0){
                                    ciudades.forEach(function (ciudad) {
                                        vm.ciudades = vm.ciudades || [];
                                        vm.ciudadesCobertura = vm.ciudadesCobertura || [];
                                        if(_.includes(vm.ciudadesEnCobertura, ciudad.id)){
                                            vm.ciudadesCobertura.push(ciudad);
                                        }else{
                                            vm.ciudades.push(ciudad);
                                        }
                                    });
                                }
                            }).finally(function () {
                                vm.cargandoCiudades = false;
                            });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.ciudades = [];
                }
            };

            vm.opcionesDepartamentos = [];
            genericService.obtenerColeccionLigera("departamentos",{
                ligera: true,
            })
            .then(function (response){
                vm.opcionesDepartamentos = [].concat(response.data);
            });
        }

    }
})();
