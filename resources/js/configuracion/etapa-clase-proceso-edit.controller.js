(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('etapaClaseProcesoEditController', etapaClaseProcesoEditController);

    etapaClaseProcesoEditController.$inject = [
        '$uibModalInstance',
        'genericService',
        'messageUtil',
        'Constantes',
        'etapaClaseProceso'
    ];

    function etapaClaseProcesoEditController(
        $uibModalInstance, genericService, messageUtil, Constantes, etapaClaseProceso
    ){
        var vm = this,
            recurso = "etapa-clase-procesos";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        // inicial autocomples
        initAutocompleteClaseProceso();
        initAutocompleteEtapaProceso();

        if(etapaClaseProceso && etapaClaseProceso.id){
            cargar();
        }else{
            vm.estado = true;
        }

        function cargar() {
            genericService.cargar(recurso, etapaClaseProceso.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var etapaClaseProceso = response.data,
                            claseProceso = etapaClaseProceso.clase_proceso,
                            etapaProceso = etapaClaseProceso.etapa_proceso;

                        vm.estado = etapaClaseProceso.estado;
                        vm.fechaCreacion = etapaClaseProceso.fecha_creacion;
                        vm.fechaModificacion = etapaClaseProceso.fecha_modificacion;
                        vm.usuarioCreacionNombre = etapaClaseProceso.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = etapaClaseProceso.usuario_modificacion_nombre;

                        if(claseProceso){
                            vm.claseProcesoId = claseProceso.id;
                            vm.opcionesClaseProceso.push({id: claseProceso.id, nombre: claseProceso.nombre});
                        }

                        if(etapaProceso){
                            vm.etapaProcesoId = etapaProceso.id;
                            vm.opcionesEtapaProceso.push({id: etapaProceso.id, nombre: etapaProceso.nombre});
                        }
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;
                var datos = {
                    id: etapaClaseProceso ? +etapaClaseProceso.id : null,
                    etapa_proceso_id: vm.etapaProcesoId,
                    clase_proceso_id: vm.claseProcesoId,
                    estado: vm.estado
                };

                var promesa = null;
                if(!(etapaClaseProceso && etapaClaseProceso.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        $uibModalInstance.close(response.data.datos);
                    }

                    messageUtil.success(response.data.mensajes[0]);
                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }

        function initAutocompleteClaseProceso() {
            vm.configClaseProceso = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
            };

            vm.opcionesClaseProceso = [];
            genericService.obtenerColeccionLigera("clases-de-procesos",{
                ligera: true,
            })
            .then(function (response){
                vm.opcionesClaseProceso = [].concat(response.data);
            });
        }

        function initAutocompleteEtapaProceso() {
            vm.configEtapaProceso = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
            };

            vm.opcionesEtapaProceso = [];
            genericService.obtenerColeccionLigera("etapas-de-procesos",{
                ligera: true,
            })
            .then(function (response){
                vm.opcionesEtapaProceso = [].concat(response.data);
            });
        }
    }
})();
