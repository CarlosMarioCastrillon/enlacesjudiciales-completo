(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('empresaController', empresaController);

    empresaController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        '$uibModal',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function empresaController(
        $scope, $routeParams, $timeout, $uibModal, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "empresas";

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.filtroEstado = null;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.crear = crear;
        vm.obtenerExcel = obtenerExcel;

        // Modulos externos
        vm.confirmarEliminar = confirmarEliminar;

        // Limpiar filtros
        limpiarFiltros();

        // Grid
        function obtenerResumen(tableState, ctrl){
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }

            if(pipePromise) $timeout.cancel(pipePromise);
            pipePromise = $timeout(function() {
                if(!vm.cargando){
                    vm.cargando = true;
                    genericService.obtenerColeccion(recurso, tableState)
                        .then(function (response){
                            if(response.status === Constantes.Response.HTTP_OK){
                                var coleccion = response.data.datos;
                                if(coleccion && coleccion.length > 0 ){
                                    vm.coleccion = [].concat(coleccion);
                                    vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                                    vm.paginacion.paginaActual = response.data.pagina_actual;
                                    vm.paginacion.desde = response.data.desde;
                                    vm.paginacion.hasta = response.data.hasta;
                                    vm.paginacion.total = response.data.total;
                                    tableState.pagination.numberOfPages = response.data.ultima_pagina;
                                }else{
                                    vm.coleccion = [];
                                }
                            }
                        }).finally(function () {
                            vm.cargando = false;
                        });
                }
            }, 500);
        }

        function limpiarFiltros(){
            vm.filtroNombre = null;
            vm.filtroCiudadNombre = null;
            vm.filtroZonaNombre = null;
            vm.filtroNumeroDocumento = null;
            vm.filtroEstado = null;
            $scope.isReset = true;
        }

        /*function cargarFiltros() {
            var filtros = gridUtil.getFormattedSearchPredicate("empresa");
            vm.nombre = filtros && filtros.nombre ? filtros.nombre : null;
        }*/

        // Acciones

        function eliminar(row){
            genericService.eliminar(recurso, row.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        $scope.$broadcast('refreshGrid');
                        messageUtil.success(response.data.mensajes[0]);
                    }else if(
                        response.status === Constantes.Response.HTTP_CONFLICT ||
                        response.status === Constantes.Response.HTTP_BAD_REQUEST
                    ){
                        messageUtil.error(response.data.mensajes[0]);
                    }
                });
        }

        // Modulos externos

        function crear(empresa) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + recurso + '/create',
                controller: 'empresaEditController as vm',
                size: 'lg',
                resolve: {
                    empresa: function () {
                        return empresa;
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(empresa && empresa.id && response && response.id){
                    empresa.nombre = response.nombre;
                    empresa.tipo_documento_codigo = response.tipo_documento.codigo;
                    empresa.tipo_empresa_nombre = response.tipo_empresa.nombre;
                    empresa.numero_documento = response.numero_documento;
                    empresa.departamento_nombre = response.departamento.nombre;
                    empresa.ciudad_nombre = response.ciudad.nombre;
                    empresa.zona_nombre = response.zona.nombre;
                    empresa.telefono_uno = response.telefono_uno;
                    empresa.telefono_dos = response.telefono_dos;
                    empresa.telefono_tres = response.telefono_tres;
                    empresa.email = response.email;
                    empresa.fecha_vencimiento = response.fecha_vencimiento;
                    empresa.actuaciones_digitales = response.actuaciones_digitales;
                    empresa.vigilancia = response.vigilancia;
                    empresa.dataprocesos = response.dataprocesos;
                    empresa.observacion = response.observacion;
                    empresa.estado = response.estado;
                    empresa.usuario_modificacion_id = response.usuario_modificacion_id;
                    empresa.usuario_modificacion_nombre = response.usuario_modificacion_nombre;
                    empresa.fecha_creacion = response.fecha_creacion;
                    empresa.fecha_modificacion = response.fecha_modificacion;

                }else if(response && response.id){
                    $scope.$broadcast('refreshGrid');
                }
            });
        }

        function obtenerExcel() {
            genericService.obtenerArchivo(recurso + "/excel", {
                nombre: vm.filtroNombre,
                numero_documento: vm.filtroNumeroDocumento,
                ciudad_nombre: vm.filtroCiudadNombre,
                zona_nombre: vm.filtroZonaNombre,
                estado: vm.filtroEstado,
            });
        }

        function confirmarEliminar(row) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + 'confirmar-template',
                controller: 'confirmarController as vm',
                size: 'lg',
                resolve: {
                    parametros: function () {
                        return {
                            mensaje: "¿Estás seguro de que quieres eliminar el empresa?"
                        };
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(response.ok){
                    eliminar(row);
                }
            });
        }

    }
})();
