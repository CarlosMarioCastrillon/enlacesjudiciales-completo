(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('zonaEditController', zonaEditController);

    zonaEditController.$inject = [
        '$uibModalInstance',
        'genericService',
        'messageUtil',
        'Constantes',
        'zona'
    ];

    function zonaEditController(
        $uibModalInstance, genericService, messageUtil, Constantes, zona
    ){
        var vm = this,
            recurso = "zonas";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        if(zona && zona.id){
            cargar();
        }else{
            vm.estado = true;
        }

        function cargar() {
            genericService.cargar(recurso, zona.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var zona = response.data;
                        vm.nombre = zona.nombre;
                        vm.coordinadorVigilancia = zona.coordinador_vigilancia;
                        vm.estado = zona.estado;
                        vm.fechaCreacion = zona.fecha_creacion;
                        vm.fechaModificacion = zona.fecha_modificacion;
                        vm.usuarioCreacionNombre = zona.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = zona.usuario_modificacion_nombre;
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;
                var datos = {
                    id: zona ? +zona.id : null,
                    nombre: vm.nombre.toUpperCase(),
                    coordinador_vigilancia: vm.coordinadorVigilancia,
                    estado: vm.estado
                };

                var promesa = null;
                if(!(zona && zona.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        $uibModalInstance.close(response.data.datos);
                        messageUtil.success(response.data.mensajes[0]);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }
                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }
    }
})();
