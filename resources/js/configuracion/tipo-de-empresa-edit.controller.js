(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('tipoDeEmpresaEditController', tipoDeEmpresaEditController);

    tipoDeEmpresaEditController.$inject = [
        '$uibModalInstance',
        'genericService',
        'messageUtil',
        'Constantes',
        'tipoDeEmpresa'
    ];

    function tipoDeEmpresaEditController(
        $uibModalInstance, genericService, messageUtil, Constantes, tipoDeEmpresa
    ){
        var vm = this,
            recurso = "tipos-de-empresa";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        if(tipoDeEmpresa && tipoDeEmpresa.id){
            cargar();
        }else{
            vm.estado = true;
        }

        function cargar() {
            genericService.cargar(recurso, tipoDeEmpresa.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var tipoDeEmpresa = response.data;
                        vm.nombre = tipoDeEmpresa.nombre;
                        vm.estado = tipoDeEmpresa.estado;
                        vm.fechaCreacion = tipoDeEmpresa.fecha_creacion;
                        vm.fechaModificacion = tipoDeEmpresa.fecha_modificacion;
                        vm.usuarioCreacionNombre = tipoDeEmpresa.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = tipoDeEmpresa.usuario_modificacion_nombre;
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;
                var datos = {
                    id: tipoDeEmpresa ? +tipoDeEmpresa.id : null,
                    nombre: vm.nombre.toUpperCase(),
                    estado: vm.estado
                };

                var promesa = null;
                if(!(tipoDeEmpresa && tipoDeEmpresa.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        $uibModalInstance.close(response.data.datos);
                        messageUtil.success(response.data.mensajes[0]);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }
                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }
    }
})();
