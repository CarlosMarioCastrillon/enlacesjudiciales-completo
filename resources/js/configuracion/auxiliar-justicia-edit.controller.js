(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('auxiliarJusticiaEditController', auxiliarJusticiaEditController);

    auxiliarJusticiaEditController.$inject = [
        '$uibModalInstance',
        'genericService',
        'messageUtil',
        'Constantes',
        'auxiliarJusticia'
    ];

    function auxiliarJusticiaEditController(
        $uibModalInstance, genericService, messageUtil, Constantes, auxiliarJusticia
    ){
        var vm = this,
            recurso = "auxiliares-justicia";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        if(auxiliarJusticia && auxiliarJusticia.id){
            cargar();
        }else{
            vm.estado = true;
        }

        function cargar() {
            genericService.cargar(recurso, auxiliarJusticia.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var auxiliarJusticia = response.data;
                        vm.numeroDocumento = auxiliarJusticia.numero_documento,
                        vm.nombresAuxiliares = auxiliarJusticia.nombres_auxiliar,
                        vm.apellidosAuxiliares = auxiliarJusticia.apellidos_auxiliar,
                        vm.ciudad = auxiliarJusticia.nombre_ciudad,
                        vm.direccionOficina = auxiliarJusticia.direccion_oficina,
                        vm.telefonoOficina = auxiliarJusticia.telefono_oficina,
                        vm.cargo = auxiliarJusticia.cargo,
                        vm.telefonoCelular = auxiliarJusticia.telefono_celular,
                        vm.direccionResidencia = auxiliarJusticia.direccion_residencia,
                        vm.telefonoResidencia = auxiliarJusticia.telefono_residencia,
                        vm.correo = auxiliarJusticia.correo_electronico,
                        vm.estado = auxiliarJusticia.estado;
                        vm.fechaCreacion = auxiliarJusticia.fecha_creacion;
                        vm.fechaModificacion = auxiliarJusticia.fecha_modificacion;
                        vm.usuarioCreacionNombre = auxiliarJusticia.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = auxiliarJusticia.usuario_modificacion_nombre;
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;
                var datos = {
                    id: auxiliarJusticia ? +auxiliarJusticia.id : null,
                    numero_documento: vm.numeroDocumento,
                    nombres_auxiliar: vm.nombresAuxiliares.toUpperCase(),
                    apellidos_auxiliar: vm.apellidosAuxiliares.toUpperCase(),
                    nombre_ciudad: vm.ciudad.toUpperCase(),
                    direccion_oficina: vm.direccionOficina.toUpperCase(),
                    telefono_oficina: vm.telefonoOficina,
                    cargo: vm.cargo,
                    telefono_celular: vm.telefonoCelular,
                    direccion_residencia: vm.direccionResidencia,
                    telefono_residencia: vm.telefonoResidencia,
                    correo_electronico: vm.correo,
                    estado: vm.estado
                };

                var promesa = null;
                if(!(auxiliarJusticia && auxiliarJusticia.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        $uibModalInstance.close(response.data.datos);
                        messageUtil.success(response.data.mensajes[0]);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }
                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }
    }
})();
