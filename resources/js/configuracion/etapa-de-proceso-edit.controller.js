(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('etapaProcesoEditController', etapaProcesoEditController);

    etapaProcesoEditController.$inject = [
        '$uibModalInstance',
        'genericService',
        'messageUtil',
        'Constantes',
        'etapaProceso'
    ];

    function etapaProcesoEditController(
        $uibModalInstance, genericService, messageUtil, Constantes, etapaProceso
    ){
        var vm = this,
            recurso = "etapas-de-procesos";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        if(etapaProceso && etapaProceso.id){
            cargar();
        }else{
            vm.estado = true;
        }

        function cargar() {
            genericService.cargar(recurso, etapaProceso.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var etapaProceso = response.data;
                        vm.nombre = etapaProceso.nombre;
                        vm.estado = etapaProceso.estado;
                        vm.fechaCreacion = etapaProceso.fecha_creacion;
                        vm.fechaModificacion = etapaProceso.fecha_modificacion;
                        vm.usuarioCreacionNombre = etapaProceso.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = etapaProceso.usuario_modificacion_nombre;
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;
                var datos = {
                    id: etapaProceso ? +etapaProceso.id : null,
                    nombre: vm.nombre.toUpperCase(),
                    estado: vm.estado
                };

                var promesa = null;
                if(!(etapaProceso && etapaProceso.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        $uibModalInstance.close(response.data.datos);
                        messageUtil.success(response.data.mensajes[0]);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }
                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }
    }
})();
