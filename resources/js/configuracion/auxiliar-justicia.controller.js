(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('auxiliarJusticiaController', auxiliarJusticiaController);

    auxiliarJusticiaController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        '$uibModal',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function auxiliarJusticiaController(
        $scope, $routeParams, $timeout, $uibModal, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "auxiliares-justicia";

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.crear = crear;

        // Modulos externos
        vm.confirmarEliminar = confirmarEliminar;

        // Limpiar filtros
        limpiarFiltros();

        // Grid
        function obtenerResumen(tableState, ctrl){
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }
            if(pipePromise) $timeout.cancel(pipePromise);
            pipePromise = $timeout(function() {
                if(!vm.cargando){
                    vm.cargando = true;
                    genericService.obtenerColeccion(recurso, tableState)
                        .then(function (response){
                            if(response.status === Constantes.Response.HTTP_OK){
                                var coleccion = response.data.datos;
                                if(coleccion && coleccion.length > 0 ){
                                    vm.coleccion = [].concat(coleccion);
                                    vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                                    vm.paginacion.paginaActual = response.data.pagina_actual;
                                    vm.paginacion.desde = response.data.desde;
                                    vm.paginacion.hasta = response.data.hasta;
                                    vm.paginacion.total = response.data.total;
                                    tableState.pagination.numberOfPages = response.data.ultima_pagina;
                                }else{
                                    vm.coleccion = [];
                                }
                            }
                        }).finally(function () {
                            vm.cargando = false;
                        });
                }
            }, 500);
        }

        function limpiarFiltros(){
            vm.filtroNumeroDocumento = null;
            vm.filtroNombresAuxiliares = null;
            vm.filtroApellidosAuxiliares = null;
            vm.filtroCiudadNombre = null;
            vm.filtroEstado = null;
            $scope.isReset = true;
        }

        /*function cargarFiltros() {
            var filtros = gridUtil.getFormattedSearchPredicate("auxiliarJusticia");
            vm.nombre = filtros && filtros.nombre ? filtros.nombre : null;
        }*/

        // Acciones

        function eliminar(row){
            genericService.eliminar(recurso, row.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        $scope.$broadcast('refreshGrid');
                        messageUtil.success(response.data.mensajes[0]);
                    }else if(
                        response.status === Constantes.Response.HTTP_CONFLICT ||
                        response.status === Constantes.Response.HTTP_BAD_REQUEST
                    ){
                        messageUtil.error(response.data.mensajes[0]);
                    }
                });
        }

        // Modulos externos

        function crear(auxiliarJusticia) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + recurso + '/create',
                controller: 'auxiliarJusticiaEditController as vm',
                size: 'lg',
                resolve: {
                    auxiliarJusticia: function () {
                        return auxiliarJusticia;
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(auxiliarJusticia && auxiliarJusticia.id && response && response.id){
                    auxiliarJusticia.numero_documento = response.numero_documento,
                    auxiliarJusticia.nombres_auxiliar = response.nombres_auxiliar,
                    auxiliarJusticia.apellidos_auxiliar = response.apellidos_auxiliar,
                    auxiliarJusticia.ciudad = response.nombre_ciudad,
                    auxiliarJusticia.direccion_oficina = response.direccion_oficina,
                    auxiliarJusticia.telefono_oficina = response.telefono_oficina,
                    auxiliarJusticia.cargo = response.cargo,
                    auxiliarJusticia.telefono_celular = response.telefono_celular,
                    auxiliarJusticia.direccion_residencia = response.direccion_residencia,
                    auxiliarJusticia.telefono_residencia = response.telefono_residencia,
                    auxiliarJusticia.correo = response.correo_electronico,
                    auxiliarJusticia.estado = response.estado;
                    auxiliarJusticia.usuario_modificacion_id = response.usuario_modificacion_id;
                    auxiliarJusticia.usuario_modificacion_nombre = response.usuario_modificacion_nombre;
                    auxiliarJusticia.fecha_creacion = response.fecha_creacion;
                    auxiliarJusticia.fecha_modificacion = response.fecha_modificacion;
                }else if(response && response.id){
                    $scope.$broadcast('refreshGrid');
                }
            });
        }
        function importar() {

        }

        function confirmarEliminar(row) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + 'confirmar-template',
                controller: 'confirmarController as vm',
                size: 'md',
                resolve: {
                    parametros: function () {
                        return {
                            mensaje: "¿Estás seguro de que quieres eliminar el auxiliar de la justicia?"
                        };
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(response.ok){
                    eliminar(row);
                }
            });
        }

    }
})();
