(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('departamentoEditController', departamentoEditController);

    departamentoEditController.$inject = [
        '$uibModalInstance',
        'genericService',
        'messageUtil',
        'Constantes',
        'departamento'
    ];

    function departamentoEditController(
        $uibModalInstance, genericService, messageUtil, Constantes, departamento
    ){
        var vm = this,
            recurso = "departamentos";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        if(departamento && departamento.id){
            cargar();
        }else{
            vm.estado = true;
        }

        function cargar() {
            genericService.cargar(recurso, departamento.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var departamento = response.data;
                        vm.nombre = departamento.nombre;
                        vm.codigoDane = departamento.codigo_dane;
                        vm.estado = departamento.estado;
                        vm.fechaCreacion = departamento.fecha_creacion;
                        vm.fechaModificacion = departamento.fecha_modificacion;
                        vm.usuarioCreacionNombre = departamento.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = departamento.usuario_modificacion_nombre;
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;
                var datos = {
                    id: departamento ? +departamento.id : null,
                    nombre: vm.nombre.toUpperCase(),
                    codigo_dane: vm.codigoDane,
                    estado: vm.estado
                };

                var promesa = null;
                if(!(departamento && departamento.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        $uibModalInstance.close(response.data.datos);
                        messageUtil.success(response.data.mensajes[0]);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }


                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }
    }
})();
