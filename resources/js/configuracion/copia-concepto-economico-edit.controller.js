(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('copiaConceptoEconomicoEditController', copiaConceptoEconomicoEditController);

    copiaConceptoEconomicoEditController.$inject = [
        '$translate',
        '$uibModalInstance',
        'genericService',
        'messageUtil',
        'Constantes',
        'conceptoEconomico'
    ];

    function copiaConceptoEconomicoEditController(
        $translate, $uibModalInstance, genericService, messageUtil, Constantes, conceptoEconomico
    ){
        var vm = this,
            i18n = {},
            recurso = "copia-conceptos-economicos";

        vm.ClaseConceptoEnum = Constantes.ClaseConcepto;

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        // Cargar traducciones
        cargarTraducciones();

        // Cargar autocompletes
        initAutocompleteTipoConcepto();

        if(conceptoEconomico && conceptoEconomico.id){
            cargar();
        }else{
            vm.estado = true;
        }

        function cargar() {
            genericService.cargar(recurso, conceptoEconomico.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var conceptoEconomico = response.data;
                        vm.nombre = conceptoEconomico.nombre;
                        vm.tipoConcepto = conceptoEconomico.tipo_concepto;
                        vm.claseConcepto = conceptoEconomico.clase_concepto;
                        vm.estado = conceptoEconomico.estado;
                        vm.fechaCreacion = conceptoEconomico.fecha_creacion;
                        vm.fechaModificacion = conceptoEconomico.fecha_modificacion;
                        vm.usuarioCreacionNombre = conceptoEconomico.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = conceptoEconomico.usuario_modificacion_nombre;
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;
                var datos = {
                    id: conceptoEconomico ? +conceptoEconomico.id : null,
                    nombre: vm.nombre.toUpperCase(),
                    tipo_concepto: vm.tipoConcepto,
                    clase_concepto: vm.claseConcepto,
                    estado: vm.estado
                };

                var promesa = null;
                if(!(conceptoEconomico && conceptoEconomico.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        $uibModalInstance.close(response.data.datos);
                    }

                    messageUtil.success(response.data.mensajes[0]);
                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }

        // Autocompletes

        function initAutocompleteTipoConcepto() {
            vm.configTipoConcepto = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true
            };
        }

        // Traducciones

        function cargarTraducciones() {
            $translate([
                "COSTOS_INTERESES_DEMANDA", "HONORARIOS_EXTRAS", "COSTOS_DE_PROCESOS"
            ]).then(function (translations) {
                i18n.COSTOS_INTERESES_DEMANDA = translations.COSTOS_INTERESES_DEMANDA;
                i18n.HONORARIOS_EXTRAS = translations.HONORARIOS_EXTRAS;
                i18n.COSTOS_DE_PROCESOS = translations.COSTOS_DE_PROCESOS;

                vm.opcionesTipoConcepto = [
                    {id: Constantes.TipoConcepto.COSTOS_INTERESES_DEMANDA, nombre: i18n.COSTOS_INTERESES_DEMANDA},
                    {id: Constantes.TipoConcepto.HONORARIOS_EXTRAS, nombre: i18n.HONORARIOS_EXTRAS},
                    {id: Constantes.TipoConcepto.COSTOS_DE_PROCESOS, nombre: i18n.COSTOS_DE_PROCESOS}
                ];
            }, function (translationIds) {});
        }
    }
})();
