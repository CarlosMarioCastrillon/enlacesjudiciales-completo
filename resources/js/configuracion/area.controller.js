(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('areaController', areaController);

    areaController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        '$uibModal',
        'envService',
        'genericService',
        'sessionUtil',
        'messageUtil',
        'Constantes'
    ];

    function areaController(
        $scope, $routeParams, $timeout, $uibModal, envService, genericService, sessionUtil, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "areas",
            empresaId = $routeParams.id && $routeParams.id !== "null" ? $routeParams.id : sessionUtil.getEmpresa().id;

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.titulo = $routeParams.o ? sessionUtil.getTituloVista($routeParams.o) : "Areas de la empresa";

        // Cargar los datos de la empresa
        genericService.cargar("empresas", empresaId)
            .then(function (response) {
                if (response.status === Constantes.Response.HTTP_OK) {
                    vm.empresa = response.data;
                }
            });

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.crear = crear;

        // Modulos externos
        vm.confirmarEliminar = confirmarEliminar;

        // Limpiar filtros
        limpiarFiltros();

        // Grid
        function obtenerResumen(tableState, ctrl){
            tableState.search = tableState.search || {};
            tableState.search.predicateObject = tableState.search.predicateObject || {};
            tableState.search.predicateObject.empresa_id = empresaId;
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }
            if(pipePromise) $timeout.cancel(pipePromise);
            pipePromise = $timeout(function() {
                if(!vm.cargando){
                    vm.cargando = true;
                    genericService.obtenerColeccion(recurso, tableState)
                        .then(function (response){
                            if(response.status === Constantes.Response.HTTP_OK){
                                var coleccion = response.data.datos;
                                if(coleccion && coleccion.length > 0 ){
                                    vm.coleccion = [].concat(coleccion);
                                    vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                                    vm.paginacion.paginaActual = response.data.pagina_actual;
                                    vm.paginacion.desde = response.data.desde;
                                    vm.paginacion.hasta = response.data.hasta;
                                    vm.paginacion.total = response.data.total;
                                    tableState.pagination.numberOfPages = response.data.ultima_pagina;
                                }else{
                                    vm.coleccion = [];
                                }
                            }
                        }).finally(function () {
                            vm.cargando = false;
                        });
                }
            }, 500);
        }

        function limpiarFiltros(){
            vm.nombre = null;
            vm.marca = null;
            $scope.isReset = true;
        }

        /*function cargarFiltros() {
            var filtros = gridUtil.getFormattedSearchPredicate("area");
            vm.nombre = filtros && filtros.nombre ? filtros.nombre : null;
        }*/

        // Acciones

        function eliminar(row){
            genericService.eliminar(recurso, row.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        $scope.$broadcast('refreshGrid');
                        messageUtil.success(response.data.mensajes[0]);
                    }else if(
                        response.status === Constantes.Response.HTTP_CONFLICT ||
                        response.status === Constantes.Response.HTTP_BAD_REQUEST
                    ){
                        messageUtil.error(response.data.mensajes[0]);
                    }
                });
        }

        // Modulos externos

        function crear(area) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + recurso + '/create',
                controller: 'areaEditController as vm',
                size: 'md',
                resolve: {
                    parametros: function () {
                        return {
                            area: area,
                            empresa: vm.empresa
                        };
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(area && area.id && response && response.id){
                    area.nombre = response.nombre;
                    area.usuario_id = response.usuario_id;
                    area.estado = response.estado;
                    area.usuario_modificacion_id = response.usuario_modificacion_id;
                    area.usuario_modificacion_nombre = response.usuario_modificacion_nombre;
                    area.fecha_creacion = response.fecha_creacion;
                    area.fecha_modificacion = response.fecha_modificacion;
                }else if(response && response.id){
                    $scope.$broadcast('refreshGrid');
                }
            });
        }

        function confirmarEliminar(row) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + 'confirmar-template',
                controller: 'confirmarController as vm',
                size: 'md',
                resolve: {
                    parametros: function () {
                        return {
                            mensaje: "¿Estás seguro de que quieres eliminar la area?"
                        };
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(response.ok){
                    eliminar(row);
                }
            });
        }

    }
})();
