(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('conceptoEconomicoController', conceptoEconomicoController);

    conceptoEconomicoController.$inject = [
        '$translate',
        '$scope',
        '$routeParams',
        '$timeout',
        '$uibModal',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function conceptoEconomicoController(
     $translate, $scope, $routeParams, $timeout, $uibModal, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            i18n = {},
            recurso = "conceptos-economicos";

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.TipoConceptoEnum = Constantes.TipoConcepto;
        vm.ClaseConceptoEnum = Constantes.ClaseConcepto;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.crear = crear;

        // Modulos externos
        vm.confirmarEliminar = confirmarEliminar;

        // Limpiar filtros
        limpiarFiltros();

        // Cargar traducciones
        cargarTraducciones();

        // Cargar autocompletes
        initAutocompleteTipoConcepto();

        // Grid
        function obtenerResumen(tableState, ctrl){
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }
            if(pipePromise) $timeout.cancel(pipePromise);
            pipePromise = $timeout(function() {
                if(!vm.cargando){
                    vm.cargando = true;
                    genericService.obtenerColeccion(recurso, tableState)
                        .then(function (response){
                            if(response.status === Constantes.Response.HTTP_OK){
                                var coleccion = response.data.datos;
                                if(coleccion && coleccion.length > 0 ){
                                    vm.coleccion = [].concat(coleccion);
                                    vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                                    vm.paginacion.paginaActual = response.data.pagina_actual;
                                    vm.paginacion.desde = response.data.desde;
                                    vm.paginacion.hasta = response.data.hasta;
                                    vm.paginacion.total = response.data.total;
                                    tableState.pagination.numberOfPages = response.data.ultima_pagina;
                                }else{
                                    vm.coleccion = [];
                                }
                            }
                        }).finally(function () {
                            vm.cargando = false;
                        });
                }
            }, 500);
        }

        function limpiarFiltros(){
            vm.nombre = null;
            vm.filterTipoConcepto = null;
            $scope.isReset = true;
        }

        /*function cargarFiltros() {
            var filtros = gridUtil.getFormattedSearchPredicate("conceptoEconomico");
            vm.nombre = filtros && filtros.nombre ? filtros.nombre : null;
        }*/

        // Acciones

        function eliminar(row){
            genericService.eliminar(recurso, row.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        $scope.$broadcast('refreshGrid');
                        messageUtil.success(response.data.mensajes[0]);
                    }else if(
                        response.status === Constantes.Response.HTTP_CONFLICT ||
                        response.status === Constantes.Response.HTTP_BAD_REQUEST
                    ){
                        messageUtil.error(response.data.mensajes[0]);
                    }
                });
        }

        // Modulos externos

        function crear(conceptoEconomico) {
            // genericService.obtenerArchivo(recurso + "/excel", {
            //     nombre: vm.nombre
            // });

            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + recurso + '/create',
                controller: 'conceptoEconomicoEditController as vm',
                size: 'md',
                resolve: {
                    conceptoEconomico: function () {
                        return conceptoEconomico;
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(conceptoEconomico && conceptoEconomico.id && response && response.id){
                    conceptoEconomico.nombre = response.nombre;
                    conceptoEconomico.tipo_concepto = response.tipo_concepto;
                    conceptoEconomico.clase_concepto = response.clase_concepto;
                    conceptoEconomico.estado = response.estado;
                    conceptoEconomico.usuario_modificacion_id = response.usuario_modificacion_id;
                    conceptoEconomico.usuario_modificacion_nombre = response.usuario_modificacion_nombre;
                    conceptoEconomico.fecha_creacion = response.fecha_creacion;
                    conceptoEconomico.fecha_modificacion = response.fecha_modificacion;
                }else if(response && response.id){
                    $scope.$broadcast('refreshGrid');
                }
            });
        }

        function confirmarEliminar(row) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + 'confirmar-template',
                controller: 'confirmarController as vm',
                size: 'md',
                resolve: {
                    parametros: function () {
                        return {
                            mensaje: "¿Estás seguro de que quieres eliminar el concepto económico?"
                        };
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(response.ok){
                    eliminar(row);
                }
            });
        }

        function initAutocompleteTipoConcepto() {
            vm.configTipoConcepto = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
            };
            vm.opcionesTipoConcepto = [];
        }

        // Traducciones

        function cargarTraducciones() {
            $translate([
                "COSTOS_INTERESES_DEMANDA", "HONORARIOS_EXTRAS", "COSTOS_DE_PROCESOS"
            ]).then(function (translations) {
                i18n.COSTOS_INTERESES_DEMANDA = translations.COSTOS_INTERESES_DEMANDA;
                i18n.HONORARIOS_EXTRAS = translations.HONORARIOS_EXTRAS;
                i18n.COSTOS_DE_PROCESOS = translations.COSTOS_DE_PROCESOS;

                vm.opcionesTipoConcepto = [
                    {id: Constantes.TipoConcepto.COSTOS_INTERESES_DEMANDA, nombre: i18n.COSTOS_INTERESES_DEMANDA},
                    {id: Constantes.TipoConcepto.HONORARIOS_EXTRAS, nombre: i18n.HONORARIOS_EXTRAS},
                    {id: Constantes.TipoConcepto.COSTOS_DE_PROCESOS, nombre: i18n.COSTOS_DE_PROCESOS}
                ];
            }, function (translationIds) {});
        }

    }
})();
