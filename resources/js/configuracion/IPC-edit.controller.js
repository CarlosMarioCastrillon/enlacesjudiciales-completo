(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('ipcEditController', ipcEditController);

    ipcEditController.$inject = [
        '$uibModalInstance',
        'genericService',
        'messageUtil',
        'Constantes',
        'ipc'
    ];

    function ipcEditController(
        $uibModalInstance, genericService, messageUtil, Constantes, ipc
    ){
        var vm = this,
            recurso = "IPC";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        if(ipc && ipc.id){
            cargar();
            // var today = moment().millisecond(0).second(0).minute(0).hour(0),
            //     fechaMaxima = moment(ipc.fecha).millisecond(0).second(0).minute(0).hour(0);
            //
            // vm.datetimepickerOptions = {
            //     useCurrent: false,
            //     maxDate: fechaMaxima.diff(today, 'days') >= 0 ? today : fechaMaxima,
            //     format: 'YYYY-MM-DD'
            // };
        }else{
            vm.estado = true;
        }

        function cargar() {
            genericService.cargar(recurso, ipc.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var ipc = response.data;
                        vm.fecha = ipc.fecha;
                        vm.indiceIPC = ipc.indice_IPC;
                        vm.inflacionAnual = ipc.inflacion_anual;
                        vm.inflacionMensual = ipc.inflacion_mensual;
                        vm.inflacionAnio = ipc.inflacion_anio;
                        vm.estado = ipc.estado;
                        vm.fechaCreacion = ipc.fecha_creacion;
                        vm.fechaModificacion = ipc.fecha_modificacion;
                        vm.usuarioCreacionNombre = ipc.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = ipc.usuario_modificacion_nombre;
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;
                var datos = {
                    id: ipc ? +ipc.id : null,
                    fecha: vm.fecha,
                    indice_IPC: vm.indiceIPC,
                    inflacion_anual: vm.inflacionAnual,
                    inflacion_mensual: vm.inflacionMensual,
                    inflacion_anio: vm.inflacionAnio,
                    estado: vm.estado
                };

                var promesa = null;
                if(!(ipc && ipc.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        $uibModalInstance.close(response.data.datos);
                        messageUtil.success(response.data.mensajes[0]);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }
                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }
    }
})();
