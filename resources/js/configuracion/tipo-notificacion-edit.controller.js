(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('tipoNotificacionEditController', tipoNotificacionEditController);

    tipoNotificacionEditController.$inject = [
        '$uibModalInstance',
        'genericService',
        'messageUtil',
        'Constantes',
        'tipoNotificacion'
    ];

    function tipoNotificacionEditController(
        $uibModalInstance, genericService, messageUtil, Constantes, tipoNotificacion
    ){
        var vm = this,
            recurso = "tipos-notificaciones";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        if(tipoNotificacion && tipoNotificacion.id){
            cargar();
        }else{
            vm.estado = true;
        }

        function cargar() {
            genericService.cargar(recurso, tipoNotificacion.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var tipoNotificacion = response.data;
                        vm.nombre = tipoNotificacion.nombre;
                        vm.estado = tipoNotificacion.estado;
                        vm.fechaCreacion = tipoNotificacion.fecha_creacion;
                        vm.fechaModificacion = tipoNotificacion.fecha_modificacion;
                        vm.usuarioCreacionNombre = tipoNotificacion.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = tipoNotificacion.usuario_modificacion_nombre;
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;
                var datos = {
                    id: tipoNotificacion ? +tipoNotificacion.id : null,
                    nombre: vm.nombre.toUpperCase(),
                    estado: vm.estado
                };

                var promesa = null;
                if(!(tipoNotificacion && tipoNotificacion.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        $uibModalInstance.close(response.data.datos);
                        messageUtil.success(response.data.mensajes[0]);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }
                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }
    }
})();
