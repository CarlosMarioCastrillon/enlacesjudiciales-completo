(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('infoEmpresaController', infoEmpresaController);

    infoEmpresaController.$inject = [
        '$uibModalInstance',
        'genericService',
        'messageUtil',
        'Constantes',
        'empresa'
    ];

    function infoEmpresaController(
        $uibModalInstance, genericService, messageUtil, Constantes, empresa
    ){
        var vm = this;

        //Variables para mostrar y ocultar
        vm.mostrarPlanes = false;
        vm.mostrarUsuarios = false;
        vm.mostrarAreas = false;

        //control grid
       // vm.cargarEmpresa = cargarEmpresa;
       // vm.obtenerPlanes = obtenerPlanes;
       // vm.obtenerUsuarios = obtenerUsuarios;
        //vm.obtenerAreas = obtenerAreas;

       // function cargarEmpresa() {
            genericService.cargar("empresas", empresa)
                .then(function (response) {
                    if (response.status === Constantes.Response.HTTP_OK) {
                        vm.empresa = response.data;
                    }
                });
        //}

      //  function obtenerPlanes() {
      //      if(!vm.cargandoPlanes) {
            //    vm.cargandoPlanes = true;
                genericService.obtener("planes", {
                    empresa_id: empresa,
                    simple: true
                }).then(function (response) {
                        if (response.status === Constantes.Response.HTTP_OK) {
                            var planes = response.data;
                            if (planes && planes.length > 0) {
                                vm.planes = [].concat(planes);
                            } else {
                                vm.planes = [];
                            }
                        }
                    }).finally(function () {
                    vm.cargandoPlanes = false;
                });
        //    }
       // }

/*        function obtenerUsuarios() {
            if(!vm.cargandoUsuarios) {
                vm.cargandoUsuarios = true;*/
                genericService.obtener("usuarios", {
                    empresa_id: empresa,
                    simple: true
                }).then(function (response) {
                        if (response.status === Constantes.Response.HTTP_OK) {
                            var usuarios = response.data;
                            if (usuarios && usuarios.length > 0) {
                                vm.usuarios = [].concat(usuarios);
                            } else {
                                vm.usuarios = [];
                            }
                        }
                    }).finally(function () {
                    vm.cargandoUsuarios = false;
                });
  /*          }
        }*/

/*        function obtenerAreas() {
            if(!vm.cargandoAreas) {
                vm.cargandoAreas = true;*/
                genericService.obtener("areas", {
                    empresa_id: empresa,
                    simple: true
                }).then(function (response) {
                        if (response.status === Constantes.Response.HTTP_OK) {
                            var areas = response.data;
                            if (areas && areas.length > 0) {
                                vm.areas = [].concat(areas);
                            } else {
                                vm.areas = [];
                            }
                        }
                    }).finally(function () {
                    vm.cargandoAreas = false;
                });
/*            }
        }*/

    }
})();
