<div class="modal-header">
    <h3 class="modal-title">Confirmar</h3>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <p>@{{ vm.mensaje }}</p>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button class="btn btn-primary" type="button" ng-click="vm.ok()">Si</button>
    <button class="btn btn-default" type="button" ng-click="vm.cancelar()">No</button>
</div>
<script>$(document).ready(function(){$.material.init();});</script>
