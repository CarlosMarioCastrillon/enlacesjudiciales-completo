<div id="consulta_reporte_judicial" class="row" st-table="vm.coleccion" st-pipe="vm.obtenerResumen" st-persist="resumenConsultaReporteJudicial" st-reset="isReset" refresh-table>
    <div class="card main-card" style="margin-bottom: 0;">
        <div class="card-content">
            <h4 class="card-title">@{{ vm.titulo }}</h4>
            <button class="btn btn-success btn-fab nex-btn-add" title="Exportar a excel" ng-click="vm.obtenerExcel()">
                <i class="mdi mdi-file-excel"></i>
            </button>
            <button  class="btn btn-default btn-fab nex-btn-add" style="top: 2.3em;" type="button" rel="tooltip" title="{{ __('common.limpiar_filtros') }}" ng-click="vm.limpiarFiltros()">
                <i class="material-icons">clear_all</i>
            </button>
            <button class="btn btn-primary btn-fab nex-btn-add" title="{{ __('common.buscar') }}" ng-click="vm.buscar()" ng-disabled="vm.deshabilitarBuscado" style="top: 4.5em;">
                <i class="material-icons">search</i>
            </button>
            <form name="vm.form" autocomplete="off" novalidate>
                <style>
                    .item{
                        font-size: 12px;
                    }
                    .form-control{
                        height: 28px;
                    }
                    .form-group.label-floating label.control-label{
                        top: -15px;
                    }
                    .form-group.label-floating.is-not-empty label.control-label{
                        top: -28px;
                        line-height: 2;
                    }
                    .form-group{
                        margin-top: 10px;
                    }
                    hr{
                        margin-top: 10px;
                        margin-bottom: 1px;
                    }
                </style>
                <div class="row" style="font-size: 12px;">
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <div ng-class="['form-group label-floating', {'is-empty': !vm.filtroDepartamento, 'is-not-empty': vm.filtroDepartamento,
                        'with-error': vm.buscando && vm.form.departamento.$invalid}]">
                            <label class="control-label">{{ __('common.departamento') }}</label>
                            <select selectize="vm.configDepartamento" options="vm.opcionesDepartamentos" disabled-typing="true" ng-model="vm.filtroDepartamento"
                                    search-model="vm.filtroDepartamento" search-predicate="departamento" name="departamento" class="form-control"></select>
                            {{--<span class="help-block show" ng-show="vm.buscando && vm.form.departamento.$invalid">
                                <span ng-show="vm.form.departamento.$error.required">Requerido</span>
                            </span>--}}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <div ng-class="['form-group label-floating', {'is-empty': !vm.filtroCiudad, 'is-not-empty': vm.filtroCiudad, 'with-error': vm.buscando && vm.form.ciudad.$invalid}]">
                            <label class="control-label">{{ __('common.ciudad') }}</label>
                            <select selectize="vm.configCiudad" options="vm.opcionesCiudad" disabled-typing="true" ng-model="vm.filtroCiudad" search-model="vm.filtroCiudad"
                                    search-predicate="ciudad" name="ciudad" class="form-control" clear-options="vm.limpiarOpcionesCiudad"></select>
                           {{-- <span class="help-block show" ng-show="vm.buscando && vm.form.ciudad.$invalid">
                                <span ng-show="vm.form.ciudad.$error.required">Requerido</span>
                            </span>--}}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <div ng-class="['form-group label-floating', {'is-empty': !vm.filtroJuzgado, 'is-not-empty': vm.filtroJuzgado}]">
                            <label class="control-label">{{ __('common.juzgado') }}</label>
                            <select selectize="vm.configJuzgado" options="vm.opcionesJuzgado" disabled-typing="true" ng-model="vm.filtroJuzgado" search-model="vm.filtroJuzgado"
                                    search-predicate="juzgado" name="juzgado" class="form-control" clear-options="vm.limpiarOpcionesJuzgado"></select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                        <div class="input-group">
                            <div ng-class="['form-group label-floating', {'is-empty': !vm.filtroFechaInicial, 'is-not-empty': vm.filtroFechaInicial,
                            'with-error': vm.buscando && vm.form.fechaInicial.$invalid}]">
                                <label class="control-label">{{ __('common.fecha_desde') }}<span class="text-danger"> *</span></label>
                                <input st-search-datetimepicker search-model="vm.filtroFechaInicial" search-predicate="fecha_inicial" datetimepicker-st-table
                                       datetimepicker-options="{useCurrent: false, format: 'YYYY-MM-DD'}" ng-model="vm.filtroFechaInicial" name="fechaInicial" class="form-control" type="text"/>
                                <i class="input-group-addon fa fa-calendar"></i>
                                <span class="help-block show" ng-show="vm.buscando && vm.form.fechaInicial.$invalid">
                                    <span ng-show="vm.form.fechaInicial.$error.required">Requerido</span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <div ng-class="['form-group label-floating', {'is-empty': !vm.filtroDespacho, 'is-not-empty': vm.filtroDespacho}]">
                            <label class="control-label">{{ __('common.despacho') }}</label>
                            <select selectize="vm.configDespacho" options="vm.opcionesDespacho" disabled-typing="true" ng-model="vm.filtroDespacho" search-model="vm.filtroDespacho"
                            search-predicate="despacho" name="despacho" class="form-control" clear-options="vm.limpiarOpcionesDespacho"></select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <div ng-class="['form-group label-floating', {'is-empty': !vm.filtroTipoNotificacion, 'is-not-empty': vm.filtroTipoNotificacion}]">
                            <label class="control-label">{{ __('common.tipo_notificacion') }}</label>
                            <select selectize="vm.configTipoNotificacion" options="vm.opcionesTipoNotificacion" disabled-typing="true" ng-model="vm.filtroTipoNotificacion"
                                    search-model="vm.filtroTipoNotificacion" search-predicate="tipo_notificacion" name="tipoNotificacion" class="form-control"></select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                        <div class="input-group">
                            <div ng-class="['form-group label-floating', {'is-empty': !vm.filtroFechaFinal, 'is-not-empty': vm.filtroFechaFinal,
                        'with-error': vm.buscando && vm.form.fechaFinal.$invalid}]">
                                <label class="control-label">{{ __('common.fecha_hasta') }}<span class="text-danger"> *</span></label>
                                <input st-search-datetimepicker search-model="vm.filtroFechaFinal" search-predicate="fecha_final" datetimepicker-st-table
                                datetimepicker-options="{useCurrent: false, format: 'YYYY-MM-DD'}" ng-model="vm.filtroFechaFinal" name="fechaFinal" class="form-control" type="text"/>
                                <i class="input-group-addon fa fa-calendar"></i>
                                <span class="help-block show" ng-show="vm.buscando && vm.form.fechaFinal.$invalid">
                                    <span ng-show="vm.form.fechaFinal.$error.required">Requerido</span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <hr>
            <div class="row" style="margin: 0;" ng-if="!vm.cargando && vm.coleccion.length > 0">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 hidden-xs text-left" style="padding: 0;">
                    <nav>
                        <ul class="pagination pagination-primary">
                            <li class="text-left">
                                <a style="padding: 0 5px;" href="" type="button" rel="tooltip" title="{{ __('common.limpiar_filtros') }}" ng-click="vm.limpiarFiltros()">
                                    <i class="material-icons">clear_all</i>
                                </a>
                            </li>
                            <li class="text-left">
                                <span style="text-transform: none; font-size: 14px;">
                                    @{{ 'Mostrando ' + vm.paginacion.desde + ' a ' + vm.paginacion.hasta + ' de ' + vm.paginacion.total + ' resultados - Página ' + vm.paginacion.paginaActual + ' de ' + vm.paginacion.ultimaPagina }}
                                </span>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding: 0;">
                    <div class="text-right" st-pagination="" st-items-by-page="vm.itemsPorPagina" st-displayed-pages="5" st-template="/paginacion-template" style="float: right;"></div>
                    <div class="text-right" style="float: right; margin: 26px 25px 26px 0;">
                        <label style="color: rgba(0,0,0,0.68);">
                            Filas:
                            <select ng-options="option for option in [5, 10, 25, 50, 100, 500] track by option" ng-model="vm.itemsPorPagina" class="select-grid"></select>
                        </label>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-hover table-sm table-ellipsis" style="font-size: 10px;">
                    <thead>
                        <tr>
                            <th st-multi-sort="radicado" style="width: 134px">{{ __('common.radicado') }}</th>
                            <th st-multi-sort="descripcion_clase_proceso">{{ __('common.tipo_proceso') }}</th>
                            <th st-multi-sort="tipo_notificacion" style="width: 134px">{{ __('common.tipo_notificacion') }}</th>
                            <th st-multi-sort="demandante" style="width: 120px;">{{ __('common.demandante') }}</th>
                            <th st-multi-sort="demandado" style="width: 120px;">{{ __('common.demandado') }}</th>
                            <th st-multi-sort="anotacion" style="min-width: 200px;">{{ __('common.detalle') }}</th>
                            <th st-multi-sort="juzgado_actual" style="width: 36px;">{{ __('common.juzgado') }}<br>{{ __('common.actual') }}</th>
                            <th st-multi-sort="despacho" style="width: 42px">{{ __('common.desp.') }}</th>
                            <th class="text-right" st-multi-sort="fecha" style="width: 55px;">{{ __('common.fecha') }}</th>
                        </tr>
                    </thead>
                    <thead ng-show="vm.cargando">
                        <tr>
                            <td colspan="11" class="text-center" style="padding: 0px;">
                                <div class="progress progress-striped active" style="margin: 0px;">
                                    <div class="progress-bar" style="width: 100%"></div>
                                </div>
                            </td>
                        </tr>
                    </thead>
                    <tbody ng-show="!vm.cargando">
                        <tr ng-repeat="row in vm.coleccion">
                            <td>@{{row.numero_proceso}}</td>
                            <td>@{{row.descripcion_clase_proceso}}</td>
                            <td>@{{row.tipo_notificacion}}</td>
                            <td style="white-space: normal;">@{{row.nombres_demandantes}}</td>
                            <td style="white-space: normal;">@{{row.nombres_demandados}}</td>
                            <td class="text-left" style="white-space: normal;">@{{row.anotacion}}</td>
                            <td>@{{row.juzgado_nombre}}</td>
                            <td class="text-right">@{{row.despacho_numero}}</td>
                            <td>@{{row.fecha_actuacion}}</td>
                        </tr>

                    </tbody>
                    <tfoot ng-if="!vm.cargando && vm.coleccion.length > 0">
                        <script>$(document).ready(function(){$.material.ripples('table .btn');});</script>
                    </tfoot>
                </table>
            </div>
            <div class="row" style="margin: 0;" ng-show="!vm.cargando && vm.coleccion.length == 0">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 0;">
                    <nav>
                        <ul class="pagination pagination-primary">
                            <li class="text-left">
                                <a style="padding: 0 5px;" href="" type="button" rel="tooltip" title="{{ __('common.limpiar_filtros') }}" ng-click="vm.limpiarFiltros()">
                                    <i class="material-icons">clear_all</i>
                                </a>
                                <span style="text-transform: none; font-size: 14px;">{{ __('common.sin_resultados') }}</span>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="row" style="margin: 0;" ng-if="!vm.cargando && vm.coleccion.length > 0">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 hidden-xs text-left" style="padding: 0;">
                    <nav>
                        <ul class="pagination pagination-primary">
                            <li class="text-left">
                                <a style="padding: 0 5px;" type="button" rel="tooltip" title="{{ __('common.limpiar_filtros') }}" ng-click="vm.limpiarFiltros()">
                                    <i class="material-icons">clear_all</i>
                                </a>
                            </li>
                            <li class="text-left">
                                <span style="text-transform: none; font-size: 14px;">
                                    @{{ 'Mostrando ' + vm.paginacion.desde + ' a ' + vm.paginacion.hasta + ' de ' + vm.paginacion.total + ' resultados - Página ' + vm.paginacion.paginaActual + ' de ' + vm.paginacion.ultimaPagina }}
                                </span>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding: 0;">
                    <div class="text-right" st-pagination="" st-items-by-page="vm.itemsPorPagina" st-displayed-pages="5" st-template="/paginacion-template" style="float: right;"></div>
                    <div class="text-right" style="float: right; margin: 26px 25px 26px 0;">
                        <label style="color: rgba(0,0,0,0.68);">
                            Filas:
                            <select ng-options="option for option in [5, 10, 25, 50, 100, 500] track by option" ng-model="vm.itemsPorPagina" class="select-grid"></select>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>$(document).ready(function(){$.material.init();});</script>
</div>
