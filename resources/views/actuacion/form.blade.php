<div class="modal-header">
    <h4 class="card-title">{{ __('common.datos_actuacion') }}</h4>
</div>
<div class="modal-body" style="padding-top: 0; padding-bottom: 0;" cg-busy="vm.promesasActivas">
    <form name="vm.form" autocomplete="off" novalidate>
        <div class="row" ng-show="vm.modoEdicion && !!vm.modoEdicionMasiva" style="margin-top: 10px; margin-bottom: 10px;">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <strong>Datos actuales:</strong>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <strong>Modificar por:</strong>
            </div>
        </div>
        <div class="row" ng-show="!vm.modoEdicionMasiva">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.numeroRadicadoActual, 'is-not-empty': vm.numeroRadicadoActual}]">
                    <label class="control-label">{{ __('common.radicado_proceso_actual') }}</label>
                    <input ng-model="vm.numeroRadicadoActual" class="form-control" type="text" disabled>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" ng-show="vm.modoEdicion && vm.modoEdicionMasiva" style="background-color: #f5f4f4;">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.departamentoNombre, 'is-not-empty': vm.departamentoNombre}]">
                    <label class="control-label">{{ __('common.departamento') }}</label>
                    <input ng-model="vm.departamentoNombre" class="form-control" type="text" disabled>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.departamentoId, 'is-not-empty': vm.departamentoId, 'with-error': vm.guardando && vm.form.departamento.$invalid}]">
                    <label class="control-label">{{ __('common.departamento') }}</label>
                    <select selectize="vm.configDepartamento" options="vm.opcionesDepartamento" ng-model="vm.departamentoId" name="departamento" required class="form-control"></select>
                    <span class="help-block show" ng-show="vm.guardando && vm.form.departamento.$invalid">
                        <span ng-show="vm.form.departamento.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" ng-show="vm.modoEdicion && !!vm.modoEdicionMasiva" style="background-color: #f5f4f4;">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.ciudadNombre, 'is-not-empty': vm.ciudadNombre}]">
                    <label class="control-label">{{ __('common.ciudad') }}</label>
                    <input ng-model="vm.ciudadNombre" class="form-control" type="text" disabled>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.ciudadId, 'is-not-empty': vm.ciudadId, 'with-error': vm.guardando && vm.form.ciudad.$invalid}]">
                    <label class="control-label">{{ __('common.ciudad') }}</label>
                    <select selectize="vm.configCiudad" options="vm.opcionesCiudad" ng-model="vm.ciudadId" name="ciudad" required class="form-control" clear-options="vm.limpiarOpcionesCiudad"></select>
                    <span class="help-block show" ng-show="vm.guardando && vm.form.ciudad.$invalid">
                        <span ng-show="vm.form.ciudad.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" ng-show="vm.modoEdicion && !!vm.modoEdicionMasiva" style="background-color: #f5f4f4;">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.juzgadoNombre, 'is-not-empty': vm.juzgadoNombre}]">
                    <label class="control-label">{{ __('common.juzgado_actual') }}</label>
                    <input ng-model="vm.juzgadoNombre" class="form-control" type="text" disabled>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.juzgadoId, 'is-not-empty': vm.juzgadoId, 'with-error': vm.guardando && vm.form.juzgado.$invalid}]">
                    <label class="control-label">{{ __('common.juzgado_actual') }}</label>
                    <select selectize="vm.configJuzgado" options="vm.opcionesJuzgado" ng-model="vm.juzgadoId" name="juzgado" required class="form-control" clear-options="vm.limpiarOpcionesJuzgado"></select>
                    <span class="help-block show" ng-show="vm.guardando && vm.form.juzgado.$invalid">
                        <span ng-show="vm.form.juzgado.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" ng-show="vm.modoEdicion && !!vm.modoEdicionMasiva" style="background-color: #f5f4f4;">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.despachoNumero, 'is-not-empty': vm.despachoNumero}]">
                    <label class="control-label">{{ __('common.despacho') }}</label>
                    <input ng-model="vm.despachoNumero" class="form-control" type="text" disabled>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.despachoId, 'is-not-empty': vm.despachoId, 'with-error': vm.guardando && vm.form.despacho.$invalid}]">
                    <label class="control-label">{{ __('common.despacho') }}</label>
                    <select selectize="vm.configDespacho" options="vm.opcionesDespacho" ng-model="vm.despachoId" name="despacho" required class="form-control" clear-options="vm.limpiarOpcionesDespacho"></select>
                    <span class="help-block show" ng-show="vm.guardando && vm.form.despacho.$invalid">
                        <span ng-show="vm.form.despacho.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
        </div>
        <div class="row" ng-show="!vm.modoEdicionMasiva">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="input-group">
                    <div ng-class="['form-group label-floating', {'is-empty': !vm.fechaRegistro, 'is-not-empty': vm.fechaRegistro, 'with-error': vm.guardando && vm.form.fechaRegistro.$invalid}]">
                        <label class="control-label">{{ __('common.fecha_registro') }}</label>
                        <input datetimepicker-st-table datetimepicker-options="{useCurrent: true, format: 'YYYY-MM-DD'}" ng-model="vm.fechaRegistro" name="fechaRegistro" ng-required="!vm.modoEdicionMasiva" class="form-control" type="text"/>
                        <i class="input-group-addon fa fa-calendar" style="top: 0;"></i>
                        <span class="help-block show" ng-show="vm.guardando && vm.form.fechaRegistro.$invalid">
                            <span ng-show="vm.form.fechaRegistro.$error.required">Requerido</span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="input-group">
                    <div ng-class="['form-group label-floating', {'is-empty': !vm.fechaActuacion, 'is-not-empty': vm.fechaActuacion, 'with-error': vm.guardando && vm.form.fechaActuacion.$invalid}]">
                        <label class="control-label">{{ __('common.fecha_actuacion') }}</label>
                        <input datetimepicker-st-table datetimepicker-options="{useCurrent: true, format: 'YYYY-MM-DD'}" ng-model="vm.fechaActuacion" name="fechaActuacion" ng-required="!vm.modoEdicionMasiva" class="form-control" type="text"/>
                        <i class="input-group-addon fa fa-calendar" style="top: 0;"></i>
                        <span class="help-block show" ng-show="vm.guardando && vm.form.fechaActuacion.$invalid">
                            <span ng-show="vm.form.fechaActuacion.$error.required">Requerido</span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="input-group">
                    <div ng-class="['form-group label-floating', {'is-empty': !vm.fechaInicioTermino, 'is-not-empty': vm.fechaInicioTermino}]">
                        <label class="control-label">{{ __('common.fecha_inicio_termino') }}</label>
                        <input datetimepicker-st-table datetimepicker-options="{useCurrent: true, format: 'YYYY-MM-DD'}" ng-model="vm.fechaInicioTermino" class="form-control" type="text"/>
                        <i class="input-group-addon fa fa-calendar" style="top: 0;"></i>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="input-group">
                    <div ng-class="['form-group label-floating', {'is-empty': !vm.fechaVencimientoTermino, 'is-not-empty': vm.fechaVencimientoTermino}]">
                        <label class="control-label">{{ __('common.fecha_vencimiento_termino') }}</label>
                        <input datetimepicker-st-table datetimepicker-options="{useCurrent: true, format: 'YYYY-MM-DD'}" ng-model="vm.fechaVencimientoTermino" class="form-control" type="text"/>
                        <i class="input-group-addon fa fa-calendar" style="top: 0;"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" ng-show="!vm.modoEdicionMasiva">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.demandante, 'is-not-empty': vm.demandante, 'with-error': vm.guardando && vm.form.demandante.$invalid}]">
                    <label class="control-label">{{ __('common.demandante') }}</label>
                    <input ng-model="vm.demandante" name="demandante" ng-required="!vm.modoEdicionMasiva" class="form-control" type="text" ng-change="vm.validarDemandante()">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.demandante.$invalid">
                        <span ng-show="vm.form.demandante.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.demandado, 'is-not-empty': vm.demandado, 'with-error': vm.guardando && vm.form.demandado.$invalid}]">
                    <label class="control-label">{{ __('common.demandado') }}</label>
                    <input ng-model="vm.demandado" name="demandado" ng-required="!vm.modoEdicionMasiva" class="form-control" type="text" ng-change="vm.validarDemandado()">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.demandado.$invalid">
                        <span ng-show="vm.form.demandado.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.anotacion, 'is-not-empty': vm.anotacion}]">
                    <label class="control-label">{{ __('common.actuacion_') }}</label>
                    <input ng-model="vm.anotacion" class="form-control" type="text">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" ng-show="!vm.modoEdicionMasiva">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.descripcionClaseProceso, 'is-not-empty': vm.descripcionClaseProceso}]">
                    <label class="control-label">{{ __('common.clase_de_proceso') }}</label>
                    <input ng-model="vm.descripcionClaseProceso" class="form-control" type="text">
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" ng-show="vm.modoEdicion && vm.modoEdicionMasiva" style="background-color: #f5f4f4;">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.tipoNotificacionNombre, 'is-not-empty': vm.tipoNotificacionNombre}]">
                    <label class="control-label">{{ __('common.tipo_notificacion') }}</label>
                    <input ng-model="vm.tipoNotificacionNombre" class="form-control" type="text" disabled>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.tipoNotificacionId, 'is-not-empty': vm.tipoNotificacionId}]">
                    <label class="control-label">{{ __('common.tipo_notificacion') }}</label>
                    <select selectize="vm.configTipoNotificacion" options="vm.opcionesTipoNotificacion" ng-model="vm.tipoNotificacionId" class="form-control"></select>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="modal-footer" style="padding: 10px 24px 14px 0;">
    <button class="btn btn-primary" type="button" ng-click="vm.guardar()" ng-disabled="vm.guardadoDeshabilitado" ng-show="!vm.modoEdicionMasiva">{{ __('common.guardar') }}</button>
    <button class="btn btn-primary" type="button" ng-click="vm.guardarMasivo()" ng-disabled="vm.guardadoDeshabilitado" ng-show="vm.modoEdicionMasiva">{{ __('common.guardar') }}</button>
    <button class="btn btn-default" type="button" ng-click="vm.cancelar()" ng-disabled="vm.guardadoDeshabilitado">{{ __('common.cancelar') }}</button>
</div>
<script>$(document).ready(function(){$.material.init();});</script>


