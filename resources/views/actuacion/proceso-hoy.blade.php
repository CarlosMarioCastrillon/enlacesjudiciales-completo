<div id="proceso_hoy" class="row" st-table="vm.coleccion" st-pipe="vm.obtenerResumen" st-persist="resumenProcesoHoy" st-reset="isReset" refresh-table>
    <div class="card main-card" style="margin-bottom: 0;">
        <div class="card-content">
            <h4 class="card-title">@{{ vm.titulo }}</h4>
            @can('ListarProcesoHoy')
                <button class="btn btn-success btn-fab nex-btn-add" title="Exportar a excel" ng-click="vm.obtenerExcel()">
                    <i class="mdi mdi-file-excel"></i>
                </button>
            @endcan
            <div class="row">
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <div class="input-group">
                            <div ng-class="['form-group label-floating', {'is-empty': !vm.filtroFechaInicial, 'is-not-empty': vm.filtroFechaInicial}]">
                                <label class="control-label">{{ __('common.fecha_inicial') }}</label>
                                <input st-search-datetimepicker search-model="vm.filtroFechaInicial" search-predicate="fecha_inicial" datetimepicker-st-table datetimepicker-options="{useCurrent: false, format: 'YYYY-MM-DD'}" ng-model="vm.filtroFechaInicial" class="form-control" type="text"/>
                                <i class="input-group-addon fa fa-calendar"></i>
                            </div>
                        </div>
                    </div>
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 col-sm-offset-2 col-md-offset-2 col-lg-offset-2">
                    <div class="input-group">
                        <div ng-class="['form-group label-floating', {'is-empty': !vm.filtroFechaFinal, 'is-not-empty': vm.filtroFechaFinal}]">
                            <label class="control-label">{{ __('common.fecha_final') }}</label>
                            <input st-search-datetimepicker search-model="vm.filtroFechaFinal" search-predicate="fecha_final" datetimepicker-st-table datetimepicker-options="{useCurrent: false, format: 'YYYY-MM-DD'}" ng-model="vm.filtroFechaFinal" class="form-control" type="text"/>
                            <i class="input-group-addon fa fa-calendar"></i>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row" style="margin: 0;" ng-if="!vm.cargando && vm.coleccion.length > 0">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 hidden-xs text-left" style="padding: 0;">
                    <nav>
                        <ul class="pagination pagination-primary">
                            <li class="text-left">
                                <a style="padding: 0 5px;" href="" type="button" rel="tooltip" title="{{ __('common.limpiar_filtros') }}" ng-click="vm.limpiarFiltros()">
                                    <i class="material-icons">clear_all</i>
                                </a>
                            </li>
                            <li class="text-left">
                                <span style="text-transform: none; font-size: 14px;">
                                    @{{ 'Mostrando ' + vm.paginacion.desde + ' a ' + vm.paginacion.hasta + ' de ' + vm.paginacion.total + ' resultados - Página ' + vm.paginacion.paginaActual + ' de ' + vm.paginacion.ultimaPagina }}
                                </span>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding: 0;">
                    <div class="text-right" st-pagination="" st-items-by-page="vm.itemsPorPagina" st-displayed-pages="5" st-template="/paginacion-template" style="float: right;"></div>
                    <div class="text-right" style="float: right; margin: 26px 25px 26px 0;">
                        <label style="color: rgba(0,0,0,0.68);">
                            Filas:
                            <select ng-options="option for option in [5, 10, 25, 50, 100, 500] track by option" ng-model="vm.itemsPorPagina" class="select-grid"></select>
                        </label>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-hover table-sm table-ellipsis" style="font-size: 14px;">
                    <thead>
                        <tr>
                            <th st-multi-sort="fecha" class="text-right">{{ __('common.fecha') }}</th>
                            <th st-multi-sort="nombre_ciudad">{{ __('common.nombre') }}<br>{{ __('common.ciudad') }}</th>
                            <th st-multi-sort="juzgado_actual">{{ __('common.juzgado') }}<br>{{ __('common.actual') }}</th>
                            <th st-multi-sort="nombre_despacho" class="text-right">{{ __('common.nro.') }}<br>{{ __('common.desp.') }}</th>
                            <th st-multi-sort="proceso">{{ __('common.proceso_') }}</th>
                            <th st-multi-sort="demandante">{{ __('common.demandante') }}</th>
                            <th st-multi-sort="demandado">{{ __('common.demandado') }}</th>
                            <th st-multi-sort="actuacion">{{ __('common.actuacion_') }}</th>
                            <th class="text-center">{{ __('common.historico') }}</th>
                            <th st-multi-sort="tipo_actuacion" class="text-center">{{ __('common.tipo_actuacion') }}</th>
                            <th class="text-center">{{ __('common.estado_imagen') }}</th>
                        </tr>
                    </thead>
                    <thead ng-show="vm.cargando">
                        <tr>
                            <td colspan="11" class="text-center" style="padding: 0px;">
                                <div class="progress progress-striped active" style="margin: 0px;">
                                    <div class="progress-bar" style="width: 100%"></div>
                                </div>
                            </td>
                        </tr>
                    </thead>
                    <tbody ng-show="!vm.cargando">
                        <tr ng-repeat="row in vm.coleccion">
                            <td class="text-right">@{{row.fecha_actuacion}}</td>
                            <td>@{{row.ciudad_nombre}}</td>
                            <td>@{{row.juzgado_nombre}}</td>
                            <td class="text-right">@{{row.despacho_numero}}</td>
                            <td>@{{row.numero_proceso}}</td>
                            <td>@{{row.nombres_demandantes}}</td>
                            <td>@{{row.nombres_demandados}}</td>
                            <td>@{{row.actuacion}}</td>
                            <td class="td-actions text-center">
                                <a type="button" rel="tooltip" class="btn btn-primary btn-simple" title="{{ __('common.historico') }}" ng-click="vm.crear(row)">
                                    <i class="fa fa-calendar"></i>
                                </a>
                            </td>
                            <td class="text-center">
                                <span ng-if="row.tipo_movimiento == 'AR'">Consulta rama</span>
                                <span ng-if="row.tipo_movimiento == 'AC'">Estado cartelera</span>
                            </td>
                            <td class="td-actions text-center">
                                <a type="button" rel="tooltip" class="btn btn-primary btn-simple" title="Consultar en la rama judicial"
                                   ng-show="row.tipo_movimiento == 'AR'" ng-click="">
                                    <i class="material-icons">visibility</i>
                                </a>
                                <a type="button" rel="tooltip" class="btn btn-primary btn-simple" title="Archivo estado cartelera"
                                   ng-show="row.tipo_movimiento == 'AC'" ng-click="vm.obtenerArchivoEstadoCartelera(row)">
                                    <i class="mat-icon mdi mdi-file-table mdi-24px"></i>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">2020-01-01</td>
                            <td>Cartagena</td>
                            <td>Juzgado civil municipal cuarta rama 09</td>
                            <td class="text-right">002</td>
                            <td>12345678901234567890123</td>
                            <td>Javier Esteban Barraza Mendez</td>
                            <td>Javier Esteban Barraza Mendez</td>
                            <td>Colabolrar con los juzgados municipales para su detención inmediata, no se abstenga</td>
                            <td class="td-actions text-center">
                                <a type="button" rel="tooltip" class="btn btn-primary btn-simple" title="{{ __('common.historico') }}" ng-click="vm.crear(row)">
                                    <i class="fa fa-calendar"></i>
                                </a>
                            </td>
                            <td class="text-center">
                                <span>Consulta<br>rama</span>
                            </td>
                            <td class="td-actions text-center">
                                <a type="button" rel="tooltip" class="btn btn-primary btn-simple" title="Consultar en la rama judicial" ng-click="">
                                    <i class="material-icons">visibility</i>
                                </a>
                            </td>
                        </tr>
                    </tbody>
                    <tfoot ng-if="!vm.cargando && vm.coleccion.length > 0">
                        <script>$(document).ready(function(){$.material.ripples('table .btn');});</script>
                    </tfoot>
                </table>
            </div>
            <div class="row" style="margin: 0;" ng-show="!vm.cargando && vm.coleccion.length == 0">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 0;">
                    <nav>
                        <ul class="pagination pagination-primary">
                            <li class="text-left">
                                <a style="padding: 0 5px;" href="" type="button" rel="tooltip" title="{{ __('common.limpiar_filtro') }}" ng-click="vm.limpiarFiltros()">
                                    <i class="material-icons">clear_all</i>
                                </a>
                                <span style="text-transform: none; font-size: 14px;">{{ __('common.sin_resultados') }}</span>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="row" style="margin: 0;" ng-if="!vm.cargando && vm.coleccion.length > 0">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 hidden-xs text-left" style="padding: 0;">
                    <nav>
                        <ul class="pagination pagination-primary">
                            <li class="text-left">
                                <a style="padding: 0 5px;" type="button" rel="tooltip" title="{{ __('common.limpiar_filtros') }}" ng-click="vm.limpiarFiltros()">
                                    <i class="material-icons">clear_all</i>
                                </a>
                            </li>
                            <li class="text-left">
                                <span style="text-transform: none; font-size: 14px;">
                                    @{{ 'Mostrando ' + vm.paginacion.desde + ' a ' + vm.paginacion.hasta + ' de ' + vm.paginacion.total + ' resultados - Página ' + vm.paginacion.paginaActual + ' de ' + vm.paginacion.ultimaPagina }}
                                </span>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding: 0;">
                    <div class="text-right" st-pagination="" st-items-by-page="vm.itemsPorPagina" st-displayed-pages="5" st-template="/paginacion-template" style="float: right;"></div>
                    <div class="text-right" style="float: right; margin: 26px 25px 26px 0;">
                        <label style="color: rgba(0,0,0,0.68);">
                            Filas:
                            <select ng-options="option for option in [5, 10, 25, 50, 100, 500] track by option" ng-model="vm.itemsPorPagina" class="select-grid"></select>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>$(document).ready(function(){$.material.init();});</script>
</div>
