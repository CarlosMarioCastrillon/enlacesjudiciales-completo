<div id="consulta_remate" class="row" st-table="vm.coleccion" st-pipe="vm.obtenerResumen" st-persist="resumenConsultaRemate" st-reset="isReset" refresh-table>
    <div class="card main-card" style="margin-bottom: 0;">
        <div class="card-content">
            <h4 class="card-title">@{{ vm.titulo }}</h4>
            <button class="btn btn-success btn-fab nex-btn-add" title="Exportar a excel" ng-click="vm.obtenerExcel()">
                <i class="mdi mdi-file-excel"></i>
            </button>
            <button  class="btn btn-default btn-fab nex-btn-add" style="top: 65px;" type="button" rel="tooltip" title="{{ __('common.limpiar_filtros') }}" ng-click="vm.limpiarFiltros()">
                <i class="material-icons">clear_all</i>
            </button>
            <button class="btn btn-primary btn-fab nex-btn-add" title="{{ __('common.buscar') }}" ng-click="vm.buscar()" ng-disabled="vm.deshabilitarBuscado" style="top: 130px;">
                <i class="material-icons">search</i>
            </button>
            <form name="vm.form" autocomplete="off" novalidate>
                <style>
                    .item{
                        font-size: 12px;
                    }
                    .form-control{
                        height: 28px;
                    }
                    .form-group.label-floating label.control-label{
                        top: -15px;
                    }
                    .form-group.label-floating.is-not-empty label.control-label{
                        top: -28px;
                    }
                    hr{
                        margin-top: 10px;
                        margin-bottom: 10px;
                    }
                </style>
                <div class="row" style="font-size: 12px;">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div ng-class="['form-group label-floating', {'is-empty': !vm.filtroDepartamento, 'is-not-empty': vm.filtroDepartamento, 'with-error': vm.buscando && vm.form.departamento.$invalid}]">
                            <label class="control-label">{{ __('common.departamento') }}<span class="text-danger"> *</span></label>
                            <select selectize="vm.configDepartamento" options="vm.opcionesDepartamentos" disabled-typing="true" ng-model="vm.filtroDepartamento" search-model="vm.filtroDepartamento" search-predicate="departamento" name="departamento" required class="form-control"></select>
                            <span class="help-block show" ng-show="vm.buscando && vm.form.departamento.$invalid">
                                <span ng-show="vm.form.departamento.$error.required">Requerido</span>
                            </span>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div ng-class="['form-group label-floating', {'is-empty': !vm.filtroCiudad, 'is-not-empty': vm.filtroCiudad, 'with-error': vm.buscando && vm.form.ciudad.$invalid}]">
                            <label class="control-label">{{ __('common.ciudad') }}<span class="text-danger"> *</span></label>
                            <select selectize="vm.configCiudad" options="vm.opcionesCiudad" disabled-typing="true" ng-model="vm.filtroCiudad" search-model="vm.filtroCiudad" search-predicate="ciudad" name="ciudad" required class="form-control" clear-options="vm.limpiarOpcionesCiudad"></select>
                            <span class="help-block show" ng-show="vm.buscando && vm.form.ciudad.$invalid">
                                <span ng-show="vm.form.ciudad.$error.required">Requerido</span>
                            </span>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <div class="input-group">
                            <div ng-class="['form-group label-floating', {'is-empty': !vm.filtroFechaInicial, 'is-not-empty': vm.filtroFechaInicial}]">
                                <label class="control-label">{{ __('common.fecha_desde') }}</label>
                                <input st-search-datetimepicker search-model="vm.filtroFechaInicial" search-predicate="fecha_inicial" datetimepicker-st-table datetimepicker-options="{useCurrent: false, format: 'YYYY-MM-DD'}" ng-model="vm.filtroFechaInicial" class="form-control" type="text"/>
                                <i class="input-group-addon fa fa-calendar"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div ng-class="['form-group label-floating', {'is-empty': !vm.filtroJuzgado, 'is-not-empty': vm.filtroJuzgado, 'with-error': vm.buscando && vm.form.juzgado.$invalid}]">
                            <label class="control-label">{{ __('common.juzgado') }}<span class="text-danger"> *</span></label>
                            <select selectize="vm.configJuzgado" options="vm.opcionesJuzgado" disabled-typing="true" ng-model="vm.filtroJuzgado" search-model="vm.filtroJuzgado" search-predicate="juzgado" name="juzgado" required class="form-control" clear-options="vm.limpiarOpcionesJuzgado"></select>
                            <span class="help-block show" ng-show="vm.buscando && vm.form.juzgado.$invalid">
                                <span ng-show="vm.form.juzgado.$error.required">Requerido</span>
                            </span>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div ng-class="['form-group label-floating', {'is-empty': !vm.filtroDespacho, 'is-not-empty': vm.filtroDespacho}]">
                            <label class="control-label">{{ __('common.despacho') }}</label>
                            <select selectize="vm.configDespacho" options="vm.opcionesDespacho" disabled-typing="true" ng-model="vm.filtroDespacho" search-model="vm.filtroDespacho" search-predicate="despacho" name="despacho" class="form-control" clear-options="vm.limpiarOpcionesDespacho"></select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <div class="input-group">
                            <div ng-class="['form-group label-floating', {'is-empty': !vm.filtroFechaFinal, 'is-not-empty': vm.filtroFechaFinal}]">
                                <label class="control-label">{{ __('common.fecha_hasta') }}</label>
                                <input st-search-datetimepicker search-model="vm.filtroFechaFinal" search-predicate="fecha_final" datetimepicker-st-table datetimepicker-options="{useCurrent: false, format: 'YYYY-MM-DD'}" ng-model="vm.filtroFechaFinal" class="form-control" type="text"/>
                                <i class="input-group-addon fa fa-calendar"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <hr>
            <div class="row" style="margin: 0;" ng-if="!vm.cargando && vm.coleccion.length > 0">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 hidden-xs text-left" style="padding: 0;">
                    <nav>
                        <ul class="pagination pagination-primary">
                            <li class="text-left">
                                <a style="padding: 0 5px;" href="" type="button" rel="tooltip" title="{{ __('common.limpiar_filtros') }}" ng-click="vm.limpiarFiltros()">
                                    <i class="material-icons">clear_all</i>
                                </a>
                            </li>
                            <li class="text-left">
                                <span style="text-transform: none; font-size: 14px;">
                                    @{{ 'Mostrando ' + vm.paginacion.desde + ' a ' + vm.paginacion.hasta + ' de ' + vm.paginacion.total + ' resultados - Página ' + vm.paginacion.paginaActual + ' de ' + vm.paginacion.ultimaPagina }}
                                </span>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding: 0;">
                    <div class="text-right" st-pagination="" st-items-by-page="vm.itemsPorPagina" st-displayed-pages="5" st-template="/paginacion-template" style="float: right;"></div>
                    <div class="text-right" style="float: right; margin: 26px 25px 26px 0;">
                        <label style="color: rgba(0,0,0,0.68);">
                            Filas:
                            <select ng-options="option for option in [5, 10, 25, 50, 100, 500] track by option" ng-model="vm.itemsPorPagina" class="select-grid"></select>
                        </label>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-hover table-sm table-ellipsis" style="font-size: 10px;">
                    <thead>
                        <tr>
                            <th class="text-right" st-multi-sort="fecha" style="width: 55px;">{{ __('common.fecha') }}<br>{{ __('common.publicacion') }}</th>
                            <th st-multi-sort="juzgado_actual" style="width: 36px;">{{ __('common.juzgado') }}<br>{{ __('common.actual') }}</th>
                            <th st-multi-sort="despacho" style="width: 42px">{{ __('common.desp.') }}</th>
                            <th st-multi-sort="anotacion">{{ __('common.detalle') }}</th>
                            <th st-multi-sort="demandante" style="width: 140px;">{{ __('common.demandante') }}</th>
                            <th st-multi-sort="demandado" style="width: 140px;">{{ __('common.demandado') }}</th>
                            <th st-multi-sort="proceso" style="width: 134px">{{ __('common.radicado') }}</th>
                            <th class="text-center" style="width: 40px;">{{ __('common.foto') }}<br>{{ __('common.aviso') }}</th>
                        </tr>
                    </thead>
                    <thead ng-show="vm.cargando">
                        <tr>
                            <td colspan="10" class="text-center" style="padding: 0px;">
                                <div class="progress progress-striped active" style="margin: 0px;">
                                    <div class="progress-bar" style="width: 100%"></div>
                                </div>
                            </td>
                        </tr>
                    </thead>
                    <tbody ng-show="!vm.cargando">
                        <tr ng-repeat="row in vm.coleccion">
                            <td>@{{row.fecha_actuacion}}</td>
                            <td>@{{row.juzgado_nombre}}</td>
                            <td class="text-right">@{{row.despacho_numero}}</td>
                            <td class="text-left" style="white-space: normal;">@{{row.anotacion}}</td>
                            <td style="white-space: normal;">@{{row.nombres_demandantes}}</td>
                            <td style="white-space: normal;">@{{row.nombres_demandados}}</td>
                            <td>@{{row.numero_proceso}}</td>
                            <td class="td-actions text-center">
                                <a type="button" rel="tooltip" class="btn btn-primary btn-simple" title="{{ __('common.imagen') }}" ng-click="vm.obtenerArchivoAnexo(row)">
                                    <i class="fa fa-eye"></i>
                                </a>
                            </td>
                        </tr>

                    </tbody>
                    <tfoot ng-if="!vm.cargando && vm.coleccion.length > 0">
                        <script>$(document).ready(function(){$.material.ripples('table .btn');});</script>
                    </tfoot>
                </table>
            </div>
            <div class="row" style="margin: 0;" ng-show="!vm.cargando && vm.coleccion.length == 0">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 0;">
                    <nav>
                        <ul class="pagination pagination-primary">
                            <li class="text-left">
                                <a style="padding: 0 5px;" href="" type="button" rel="tooltip" title="{{ __('common.limpiar_filtros') }}" ng-click="vm.limpiarFiltros()">
                                    <i class="material-icons">clear_all</i>
                                </a>
                                <span style="text-transform: none; font-size: 14px;">{{ __('common.sin_resultados') }}</span>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="row" style="margin: 0;" ng-if="!vm.cargando && vm.coleccion.length > 0">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 hidden-xs text-left" style="padding: 0;">
                    <nav>
                        <ul class="pagination pagination-primary">
                            <li class="text-left">
                                <a style="padding: 0 5px;" type="button" rel="tooltip" title="{{ __('common.limpiar_filtros') }}" ng-click="vm.limpiarFiltros()">
                                    <i class="material-icons">clear_all</i>
                                </a>
                            </li>
                            <li class="text-left">
                                <span style="text-transform: none; font-size: 14px;">
                                    @{{ 'Mostrando ' + vm.paginacion.desde + ' a ' + vm.paginacion.hasta + ' de ' + vm.paginacion.total + ' resultados - Página ' + vm.paginacion.paginaActual + ' de ' + vm.paginacion.ultimaPagina }}
                                </span>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding: 0;">
                    <div class="text-right" st-pagination="" st-items-by-page="vm.itemsPorPagina" st-displayed-pages="5" st-template="/paginacion-template" style="float: right;"></div>
                    <div class="text-right" style="float: right; margin: 26px 25px 26px 0;">
                        <label style="color: rgba(0,0,0,0.68);">
                            Filas:
                            <select ng-options="option for option in [5, 10, 25, 50, 100, 500] track by option" ng-model="vm.itemsPorPagina" class="select-grid"></select>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>$(document).ready(function(){$.material.init();});</script>
</div>
