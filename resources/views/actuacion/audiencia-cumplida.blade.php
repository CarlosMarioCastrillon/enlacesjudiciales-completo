<div id="proceso_juzgado_fecha" class="row" st-table="vm.coleccion" st-pipe="vm.obtenerResumen" st-persist="resumenAudienciaPendiente" st-reset="isReset" refresh-table>
    <div class="card main-card" style="margin-bottom: 0;">
        <div class="card-content">
            <h4 class="card-title">@{{ vm.titulo }}</h4>
            <button class="btn btn-success btn-fab nex-btn-add" title="Exportar a excel" ng-click="vm.obtenerExcel()">
                <i class="mdi mdi-file-excel"></i>
            </button>
            <button  class="btn btn-default btn-fab nex-btn-add" style="top: 75px;" type="button" rel="tooltip" title="{{ __('common.limpiar_filtros') }}" ng-click="vm.limpiarFiltros()">
                <i class="material-icons">clear_all</i>
            </button>
            <button class="btn btn-primary btn-fab nex-btn-add" title="{{ __('common.buscar') }}" ng-click="vm.buscar()" ng-disabled="vm.deshabilitarBuscado" style="top: 140px;">
                <i class="material-icons">search</i>
            </button>
            <form name="vm.form" autocomplete="off" novalidate>
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <div ng-class="['form-group label-floating', {'is-empty': !vm.filtroProceso, 'is-not-empty': vm.filtroProceso}]">
                            <label class="control-label">{{ __('common.proceso_') }}</label>
                            <input st-search="proceso" ng-model="vm.filtroProceso" class="form-control">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div ng-class="['form-group label-floating', {'is-empty': !vm.filtroActuacion, 'is-not-empty': vm.filtroActuacion}]">
                            <label class="control-label">{{ __('common.actuacion_') }}</label>
                            <input st-search="actuacion" ng-model="vm.filtroActuacion" class="form-control">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <div ng-class="['form-group label-floating', {'is-empty': !vm.filtroDemandante, 'is-not-empty': vm.filtroDemandante}]">
                            <label class="control-label">{{ __('common.demandante') }}</label>
                            <input st-search="demandante" ng-model="vm.filtroDemandante" class="form-control" name="demandante">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div ng-class="['form-group label-floating', {'is-empty': !vm.filtroDemandado, 'is-not-empty': vm.filtroDemandado}]">
                            <label class="control-label">{{ __('common.demandado') }}</label>
                            <input st-search="demandado" ng-model="vm.filtroDemandado" class="form-control" name="demandado">
                        </div>
                    </div>
                </div>
            </form>
            <hr>
            <div class="row" style="margin: 0;" ng-if="!vm.cargando && vm.coleccion.length > 0">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 hidden-xs text-left" style="padding: 0;">
                    <nav>
                        <ul class="pagination pagination-primary">
                            <li class="text-left">
                                <a style="padding: 0 5px;" href="" type="button" rel="tooltip" title="{{ __('common.limpiar_filtros') }}" ng-click="vm.limpiarFiltros()">
                                    <i class="material-icons">clear_all</i>
                                </a>
                            </li>
                            <li class="text-left">
                                <span style="text-transform: none; font-size: 14px;">
                                    @{{ 'Mostrando ' + vm.paginacion.desde + ' a ' + vm.paginacion.hasta + ' de ' + vm.paginacion.total + ' resultados - Página ' + vm.paginacion.paginaActual + ' de ' + vm.paginacion.ultimaPagina }}
                                </span>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding: 0;">
                    <div class="text-right" st-pagination="" st-items-by-page="vm.itemsPorPagina" st-displayed-pages="5" st-template="/paginacion-template" style="float: right;"></div>
                    <div class="text-right" style="float: right; margin: 26px 25px 26px 0;">
                        <label style="color: rgba(0,0,0,0.68);">
                            Filas:
                            <select ng-options="option for option in [5, 10, 25, 50, 100, 500] track by option" ng-model="vm.itemsPorPagina" class="select-grid"></select>
                        </label>
                    </div>
                </div>
            </div>
            <div class="table-responsive" style="font-size: 10px;">
                <table class="table table-hover table-sm table-ellipsis">
                    <thead>
                        <tr>
                            <th class="text-center" style="width: 47px;">{{ __('common.anexo') }}</th>
                            <th st-multi-sort="proceso" style="width: 159px">{{ __('common.proceso_') }}</th>
                            <th st-multi-sort="demandante" style="max-width: 150px;">{{ __('common.demandante') }}</th>
                            <th st-multi-sort="demandado" style="max-width: 150px;">{{ __('common.demandado') }}</th>
                            <th st-multi-sort="actuacion" style="min-width: 150px;">{{ __('common.actuacion_') }}</th>
                            <th class="text-right" st-multi-sort="fecha_vencimiento" style="width: 66px;">{{ __('common.fecha') }}<br>{{ __('common.vencimiento') }}</th>
                            <th st-multi-sort="ultima_actualizacion" class="text-right" style="width: 93px;">{{ __('common.ultima') }}<br>{{ __('common.actualizacion') }}</th>
                            <th st-multi-sort="actualizado_por" style="width: 80px;">{{ __('common.actualizado_por' )}}</th>
                            <th class="text-right" st-multi-sort="falta" style="width: 37px;">{{ __('common.falta' )}}</th>
                        </tr>
                    </thead>
                    <thead ng-show="vm.cargando">
                        <tr>
                            <td colspan="10" class="text-center" style="padding: 0px;">
                                <div class="progress progress-striped active" style="margin: 0px;">
                                    <div class="progress-bar" style="width: 100%"></div>
                                </div>
                            </td>
                        </tr>
                    </thead>
                    <tbody ng-show="!vm.cargando">
                        <tr ng-repeat="row in vm.coleccion">
                            <td class="td-actions text-center">
                                <a type="button" rel="tooltip" class="btn btn-primary btn-simple" title="{{ __('common.anexo') }}" ng-click="vm.obtenerArchivoAnexo(row)">
                                    <i class="fa fa-eye"></i>
                                </a>
                            </td>
                            <td>@{{row.numero_proceso}}</td>
                            <td>@{{row.nombres_demandantes}}</td>
                            <td>@{{row.nombres_demandados}}</td>
                            <td>@{{row.observaciones}}</td>
                            <td class="text-right">@{{row.fecha_vencimiento_termino}}</td>
                            <td class="text-right">@{{row.fecha_modificacion.substr(0,10)}}</td>
                            <td>@{{row.usuario_modificacion_nombre.substr(0,25)}}</td>
                            <td class="text-right">@{{row.falta}}</td>
                        </tr>

                    </tbody>
                    <tfoot ng-if="!vm.cargando && vm.coleccion.length > 0">
                        <script>$(document).ready(function(){$.material.ripples('table .btn');});</script>
                    </tfoot>
                </table>
            </div>
            <div class="row" style="margin: 0;" ng-show="!vm.cargando && vm.coleccion.length == 0">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 0;">
                    <nav>
                        <ul class="pagination pagination-primary">
                            <li class="text-left">
                                <a style="padding: 0 5px;" href="" type="button" rel="tooltip" title="{{ __('common.limpiar_filtro') }}" ng-click="vm.limpiarFiltros()">
                                    <i class="material-icons">clear_all</i>
                                </a>
                                <span style="text-transform: none; font-size: 14px;">{{ __('common.sin_resultados') }}</span>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="row" style="margin: 0;" ng-if="!vm.cargando && vm.coleccion.length > 0">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 hidden-xs text-left" style="padding: 0;">
                    <nav>
                        <ul class="pagination pagination-primary">
                            <li class="text-left">
                                <a style="padding: 0 5px;" type="button" rel="tooltip" title="{{ __('common.limpiar_filtros') }}" ng-click="vm.limpiarFiltros()">
                                    <i class="material-icons">clear_all</i>
                                </a>
                            </li>
                            <li class="text-left">
                                <span style="text-transform: none; font-size: 14px;">
                                    @{{ 'Mostrando ' + vm.paginacion.desde + ' a ' + vm.paginacion.hasta + ' de ' + vm.paginacion.total + ' resultados - Página ' + vm.paginacion.paginaActual + ' de ' + vm.paginacion.ultimaPagina }}
                                </span>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding: 0;">
                    <div class="text-right" st-pagination="" st-items-by-page="vm.itemsPorPagina" st-displayed-pages="5" st-template="/paginacion-template" style="float: right;"></div>
                    <div class="text-right" style="float: right; margin: 26px 25px 26px 0;">
                        <label style="color: rgba(0,0,0,0.68);">
                            Filas:
                            <select ng-options="option for option in [5, 10, 25, 50, 100, 500] track by option" ng-model="vm.itemsPorPagina" class="select-grid"></select>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>$(document).ready(function(){$.material.init();});</script>
</div>
