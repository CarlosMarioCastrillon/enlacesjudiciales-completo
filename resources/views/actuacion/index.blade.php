<div id="actuacion" class="row" st-table="vm.coleccion" st-pipe="vm.obtenerResumen" st-persist="resumenActuacion" st-reset="isReset" refresh-table>
    <div class="card main-card" style="margin-bottom: 0;">
        <div class="card-content">
            <h4 class="card-title">@{{ vm.titulo }}</h4>
            <div class="row">
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div ng-class="['form-group label-floating', {'is-empty': !vm.filtroDepartamentoId, 'is-not-empty': vm.filtroDepartamentoId,
                        'with-error': vm.buscando && vm.form.departamento.$invalid}]">
                        <label class="control-label">{{ __('common.departamento') }}</label>
                        <select selectize="vm.configDepartamento" options="vm.opcionesDepartamento" ng-model="vm.filtroDepartamentoId"
                                search-model="vm.filtroDepartamentoId" search-predicate="departamento_id" class="form-control"></select>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div ng-class="['form-group label-floating', {'is-empty': !vm.filtroCiudadId, 'is-not-empty': vm.filtroCiudadId}]">
                        <label class="control-label">{{ __('common.ciudad') }}</label>
                        <select selectize="vm.configCiudad" options="vm.opcionesCiudad" ng-model="vm.filtroCiudadId" search-model="vm.filtroCiudadId"
                                search-predicate="ciudad_id" class="form-control" clear-options="vm.limpiarOpcionesCiudad"></select>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div ng-class="['form-group label-floating', {'is-empty': !vm.filtroJuzgadoId, 'is-not-empty': vm.filtroJuzgadoId}]">
                        <label class="control-label">{{ __('common.juzgado') }}</label>
                        <select selectize="vm.configJuzgado" options="vm.opcionesJuzgado" disabled-typing="true" ng-model="vm.filtroJuzgadoId" search-model="vm.filtroJuzgadoId"
                                search-predicate="juzgado_id" class="form-control" clear-options="vm.limpiarOpcionesJuzgado"></select>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div ng-class="['form-group label-floating', {'is-empty': !vm.filtroDespachoId, 'is-not-empty': vm.filtroDespachoId}]">
                        <label class="control-label">{{ __('common.despacho') }}</label>
                        <select selectize="vm.configDespacho" options="vm.opcionesDespacho" disabled-typing="true" ng-model="vm.filtroDespachoId" search-model="vm.filtroDespachoId"
                                search-predicate="despacho_id" class="form-control" clear-options="vm.limpiarOpcionesDespacho"></select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div ng-class="['form-group label-floating', {'is-empty': !vm.filtroTipoNotificacionId, 'is-not-empty': vm.filtroTipoNotificacionId}]">
                        <label class="control-label">{{ __('common.tipo_notificacion') }}</label>
                        <select selectize="vm.configTipoNotificacion" options="vm.opcionesTipoNotificacion" disabled-typing="true" ng-model="vm.filtroTipoNotificacionId" search-model="vm.filtroTipoNotificacionId"
                                search-predicate="tipo_notificacion_id" class="form-control"></select>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div ng-class="['form-group label-floating', {'is-empty': !vm.filtroNumeroRadicadoActual, 'is-not-empty': vm.filtroNumeroRadicadoActual}]">
                        <label class="control-label">{{ __('common.proceso_') }}</label>
                        <input st-search="numero_radicado_actual" ng-model="vm.filtroNumeroRadicadoActual" class="form-control">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div class="input-group">
                        <div ng-class="['form-group label-floating', {'is-empty': !vm.filtroFechaDesde, 'is-not-empty': vm.filtroFechaDesde}]">
                            <label class="control-label">{{ __('common.fecha_desde') }}</label>
                            <input st-search-datetimepicker search-model="vm.filtroFechaDesde" search-predicate="fecha_desde" datetimepicker-st-table
                                   datetimepicker-options="{useCurrent: false, format: 'YYYY-MM-DD'}" ng-model="vm.filtroFechaDesde" class="form-control" type="text"/>
                            <i class="input-group-addon fa fa-calendar"></i>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div class="input-group">
                        <div ng-class="['form-group label-floating', {'is-empty': !vm.filtroFechaHasta, 'is-not-empty': vm.filtroFechaHasta}]">
                            <label class="control-label">{{ __('common.fecha_hasta') }}</label>
                            <input st-search-datetimepicker search-model="vm.filtroFechaHasta" search-predicate="fecha_hasta" datetimepicker-st-table
                                   datetimepicker-options="{useCurrent: false, format: 'YYYY-MM-DD'}" ng-model="vm.filtroFechaHasta" class="form-control" type="text"/>
                            <i class="input-group-addon fa fa-calendar"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    @can('ModificarActuacion')
                        <button class="btn btn-primary btn-round btn-fab nex-btn-add" title="Modificar actuaciones" ng-click="vm.crear()" style="position: relative !important; right: 5px;">
                            <i class="material-icons">edit</i>
                        </button>
                    @endcan
                    @can('EliminarActuacion')
                        <button class="btn btn-danger btn-round btn-fab nex-btn-add" title="Eliminar actuaciones" ng-click="vm.confirmarEliminarMasivo()" style="position: relative !important; right: 0;">
                            <i class="material-icons">delete</i>
                        </button>
                    @endcan
                </div>
            </div>
            <hr>
            <div class="row" style="margin: 0;" ng-if="!vm.cargando && vm.coleccion.length > 0">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 hidden-xs text-left" style="padding: 0;">
                    <nav>
                        <ul class="pagination pagination-primary">
                            <li class="text-left">
                                <a style="padding: 0 5px;" href="" type="button" rel="tooltip" title="{{ __('common.limpiar_filtros') }}" ng-click="vm.limpiarFiltros()">
                                    <i class="material-icons">clear_all</i>
                                </a>
                            </li>
                            <li class="text-left">
                                <span style="text-transform: none; font-size: 14px;">
                                    @{{ 'Mostrando ' + vm.paginacion.desde + ' a ' + vm.paginacion.hasta + ' de ' + vm.paginacion.total + ' resultados - Página ' + vm.paginacion.paginaActual + ' de ' + vm.paginacion.ultimaPagina }}
                                </span>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding: 0;">
                    <div class="text-right" st-pagination="" st-items-by-page="vm.itemsPorPagina" st-displayed-pages="5" st-template="/paginacion-template" style="float: right;"></div>
                    <div class="text-right" style="float: right; margin: 26px 25px 26px 0;">
                        <label style="color: rgba(0,0,0,0.68);">
                            Filas:
                            <select ng-options="option for option in [5, 10, 25, 50, 100, 500] track by option" ng-model="vm.itemsPorPagina" class="select-grid"></select>
                        </label>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-hover table-sm table-ellipsis" style="font-size: 12px;">
                    <thead>
                        <tr>
                            <th width="2%">
                                <input type="checkbox" ng-model="vm.todoSeleccionado" ng-change="vm.seleccionarTodo()">
                            </th>
                            <th st-multi-sort="ciudad_nombre" width="8%">{{ __('common.ciudad') }}</th>
                            <th st-multi-sort="juzgado_nombre" width="8%">{{ __('common.juzgado_actual') }}</th>
                            <th st-multi-sort="despacho_numero" class="text-center" width="8%">{{ __('common.numero_despacho') }}</th>
                            <th st-multi-sort="numero_radicado_actual" width="8%">{{ __('common.proceso_') }}</th>
                            <th st-multi-sort="demandante" width="8%">{{ __('common.demandante') }}</th>
                            <th st-multi-sort="demandado" width="8%">{{ __('common.demandado') }}</th>
                            <th st-multi-sort="anotacion" width="15%">{{ __('common.actuacion_') }}</th>
                            <th st-multi-sort="fecha_registro" class="text-right" width="6%">{{ __('common.fecha') }}</th>
                            <th st-multi-sort="tipo_notificacion_nombre" width="8%">{{ __('common.tipo_notificacion') }}</th>
                            <th st-multi-sort="descripcion_clase_proceso" width="8%">{{ __('common.clase_de_proceso') }}</th>
                            <th class="text-right" width="3%">{{ __('common.acciones') }}</th>
                        </tr>
                    </thead>
                    <thead ng-show="vm.cargando">
                        <tr>
                            <td colspan="12" class="text-center" style="padding: 0px;">
                                <div class="progress progress-striped active" style="margin: 0px;">
                                    <div class="progress-bar" style="width: 100%"></div>
                                </div>
                            </td>
                        </tr>
                    </thead>
                    <tbody ng-show="!vm.cargando">
                        <tr ng-repeat="row in vm.coleccion">
                            <td width="2%">
                                <input type="checkbox" ng-model="row.seleccionado" ng-change="vm.seleccionar(row)">
                            </td>
                            <td width="8%">@{{row.ciudad_nombre}}</td>
                            <td width="8%">@{{row.juzgado_nombre}}</td>
                            <td class="text-center" width="8%">@{{row.despacho_numero}}</td>
                            <td width="8%">@{{row.numero_radicado_proceso_actual}}</td>
                            <td width="8%">@{{row.demandantes}}</td>
                            <td width="8%">@{{row.demandados}}</td>
                            <td width="15%">@{{row.anotacion}}</td>
                            <td class="text-right" width="6%">@{{row.fecha_registro}}</td>
                            <td width="8%">@{{row.tipo_notificacion_nombre}}</td>
                            <td width="8%">@{{row.descripcion_clase_proceso}}</td>
                            <td class="td-actions text-right" width="3%">
                                @can('ModificarActuacion')
                                <a type="button" rel="tooltip" class="btn btn-primary btn-simple" title="{{ __('common.modificar') }}" ng-click="vm.crear(row)">
                                    <i class="material-icons">edit</i>
                                </a>
                                @endcan
                                @can('EliminarActuacion')
                                <a type="button" rel="tooltip" class="btn btn-danger btn-simple" title="{{ __('common.eliminar') }}" ng-click="vm.confirmarEliminar(row)">
                                    <i class="material-icons">delete</i>
                                </a>
                                @endcan
                            </td>
                        </tr>
                    </tbody>
                    <tfoot ng-if="!vm.cargando && vm.coleccion.length > 0">
                        <script>$(document).ready(function(){$.material.ripples('table .btn');});</script>
                    </tfoot>
                </table>
            </div>
            <div class="row" style="margin: 0;" ng-show="!vm.cargando && vm.coleccion.length == 0">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 0;">
                    <nav>
                        <ul class="pagination pagination-primary">
                            <li class="text-left">
                                <a style="padding: 0 5px;" href="" type="button" rel="tooltip" title="{{ __('common.limpiar_filtro') }}" ng-click="vm.limpiarFiltros()">
                                    <i class="material-icons">clear_all</i>
                                </a>
                                <span style="text-transform: none; font-size: 14px;">{{ __('common.sin_resultados') }}</span>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="row" style="margin: 0;" ng-if="!vm.cargando && vm.coleccion.length > 0">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 hidden-xs text-left" style="padding: 0;">
                    <nav>
                        <ul class="pagination pagination-primary">
                            <li class="text-left">
                                <a style="padding: 0 5px;" href="" type="button" rel="tooltip" title="{{ __('common.limpiar_filtros') }}" ng-click="vm.limpiarFiltros()">
                                    <i class="material-icons">clear_all</i>
                                </a>
                            </li>
                            <li class="text-left">
                                <span style="text-transform: none; font-size: 14px;">
                                    @{{ 'Mostrando ' + vm.paginacion.desde + ' a ' + vm.paginacion.hasta + ' de ' + vm.paginacion.total + ' resultados - Página ' + vm.paginacion.paginaActual + ' de ' + vm.paginacion.ultimaPagina }}
                                </span>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding: 0;">
                    <div class="text-right" st-pagination="" st-items-by-page="vm.itemsPorPagina" st-displayed-pages="5" st-template="/paginacion-template" style="float: right;"></div>
                    <div class="text-right" style="float: right; margin: 26px 25px 26px 0;">
                        <label style="color: rgba(0,0,0,0.68);">
                            Filas:
                            <select ng-options="option for option in [5, 10, 25, 50, 100, 500] track by option" ng-model="vm.itemsPorPagina" class="select-grid"></select>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>$(document).ready(function(){$.material.init();});</script>
</div>
