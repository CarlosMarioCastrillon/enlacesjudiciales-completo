<div id="consulta_auto" class="row" st-table="vm.coleccion" st-pipe="vm.obtenerResumen" st-persist="resumenConsultaAuto" st-reset="isReset" refresh-table>
    <div class="card main-card" style="margin-bottom: 0;">
        <div class="card-content">
            <h4 class="card-title">@{{ vm.titulo }}</h4>
            @can('ListarConsultaAuto')
                <button class="btn btn-success btn-fab nex-btn-add" title="Exportar a excel" ng-click="vm.obtenerExcel()">
                    <i class="mdi mdi-file-excel"></i>
                </button>
            @endcan
            <form name="vm.form" autocomplete="off" novalidate>
                <div class="row">
                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                            <div class="input-group">
                                <div ng-class="['form-group label-floating', {'is-empty': !vm.filtroFechaInicial, 'is-not-empty': vm.filtroFechaInicial}]">
                                    <label class="control-label">{{ __('common.fecha_inicial') }}</label>
                                    <input st-search-datetimepicker search-model="vm.filtroFechaInicial" search-predicate="fecha_inicial" datetimepicker-st-table

                                           datetimepicker-options="{useCurrent: false, format: 'YYYY-MM-DD'}" ng-model="vm.filtroFechaInicial" class="form-control" type="text"/>
                                    <i class="input-group-addon fa fa-calendar"></i>
                                </div>
                            </div>
                        </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 col-sm-offset-2 col-md-offset-2 col-lg-offset-2">
                        <div class="input-group">
                            <div ng-class="['form-group label-floating', {'is-empty': !vm.filtroFechaFinal, 'is-not-empty': vm.filtroFechaFinal}]">
                                <label class="control-label">{{ __('common.fecha_final') }}</label>
                                <input st-search-datetimepicker search-model="vm.filtroFechaFinal" search-predicate="fecha_final" datetimepicker-st-table
                                       datetimepicker-options="{useCurrent: false, format: 'YYYY-MM-DD'}" ng-model="vm.filtroFechaFinal" class="form-control" type="text"/>
                                <i class="input-group-addon fa fa-calendar"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <button class="btn btn-primary btn-fab nex-btn-add" title="{{ __('common.buscar') }}" ng-click="vm.buscar()" ng-disabled="vm.deshabilitarBuscado" style="top: 2.5em; right: 29.5%">
                <i class="material-icons">search</i>
            </button>
            <button  class="btn btn-default btn-fab nex-btn-add" style="top: 2.5em; left: 47%;" type="button" rel="tooltip" title="{{ __('common.limpiar_filtros') }}" ng-click="vm.limpiarFiltros()">
                <i class="material-icons">clear_all</i>
            </button>
            <hr>
            <div class="row" style="margin: 0;" ng-if="!vm.cargando && vm.coleccion.length > 0">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 hidden-xs text-left" style="padding: 0;">
                    <nav>
                        <ul class="pagination pagination-primary">
                            <li class="text-left">
                                <a style="padding: 0 5px;" href="" type="button" rel="tooltip" title="{{ __('common.limpiar_filtros') }}" ng-click="vm.limpiarFiltros()">
                                    <i class="material-icons">clear_all</i>
                                </a>
                            </li>
                            <li class="text-left">
                                <span style="text-transform: none; font-size: 14px;">
                                    @{{ 'Mostrando ' + vm.paginacion.desde + ' a ' + vm.paginacion.hasta + ' de ' + vm.paginacion.total + ' resultados - Página ' + vm.paginacion.paginaActual + ' de ' + vm.paginacion.ultimaPagina }}
                                </span>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding: 0;">
                    <div class="text-right" st-pagination="" st-items-by-page="vm.itemsPorPagina" st-displayed-pages="5" st-template="/paginacion-template" style="float: right;"></div>
                    <div class="text-right" style="float: right; margin: 26px 25px 26px 0;">
                        <label style="color: rgba(0,0,0,0.68);">
                            Filas:
                            <select ng-options="option for option in [5, 10, 25, 50, 100, 500] track by option" ng-model="vm.itemsPorPagina" class="select-grid"></select>
                        </label>
                    </div>
                </div>
            </div>
            <div class="table-responsive" style="font-size: 12px;">
                <table class="table table-hover table-sm table-ellipsis">
                    <thead>
                        <tr>
                            <th st-multi-sort="fecha" class="text-right" style="width: 66px;">{{ __('common.fecha') }}</th>
                            <th st-multi-sort="nombre_ciudad">{{ __('common.nombre') }}<br>{{ __('common.ciudad') }}</th>
                            <th st-multi-sort="proceso" style="min-width: 100px">{{ __('common.proceso_') }}</th>
                            <th st-multi-sort="tipo_movimiento" style="width: 66px;">{{ __('common.tipo') }}</th>
                            <th st-multi-sort="demandante" style="width: 150px;">{{ __('common.demandante') }}</th>
                            <th st-multi-sort="demandado" style="width: 150px;">{{ __('common.demandado') }}</th>
                            <th st-multi-sort="actuacion" style="width: 200px;">{{ __('common.actuacion_') }}</th>
                            <th class="text-right" style="width: 47px;">{{ __('common.anexo') }}</th>
                        </tr>
                    </thead>
                    <thead ng-show="vm.cargando">
                        <tr>
                            <td colspan="10" class="text-center" style="padding: 0px;">
                                <div class="progress progress-striped active" style="margin: 0px;">
                                    <div class="progress-bar" style="width: 100%"></div>
                                </div>
                            </td>
                        </tr>
                    </thead>
                    <tbody ng-show="!vm.cargando">
                        <tr ng-repeat="row in vm.coleccion">
                            <td class="text-right">@{{row.fecha_actuacion}}</td>
                            <td>@{{row.ciudad_nombre}}</td>
                            <td>@{{row.numero_proceso}}</td>
                            <td>
                                <span ng-if="row.tipo_movimiento == 'EX'">{{ __('common.expediente') }}</span>
                                <span ng-if="row.tipo_movimiento == 'AC'">{{ __('common.actuacion_') }}</span>
                                <span ng-if="row.tipo_movimiento == 'VG'">{{ __('common.vigilancia_') }}</span>
                            </td>
                            <td>@{{row.nombres_demandantes}}</td>
                            <td>@{{row.nombres_demandados}}</td>
                            <td>@{{row.actuacion}}</td>
                            <td class="td-actions text-right">
                                <a type="button" rel="tooltip" class="btn btn-primary btn-simple" title="{{ __('common.descargar') }}"  ng-click="vm.obtenerArchivoAnexo(row)">
                                    <i class="fa fa-download"></i>
                                </a>
                            </td>
                        </tr>

                    </tbody>
                    <tfoot ng-if="!vm.cargando && vm.coleccion.length > 0">
                        <script>$(document).ready(function(){$.material.ripples('table .btn');});</script>
                    </tfoot>
                </table>
            </div>
            <div class="row" style="margin: 0;" ng-show="!vm.cargando && vm.coleccion.length == 0">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 0;">
                    <nav>
                        <ul class="pagination pagination-primary">
                            <li class="text-left">
                                <a style="padding: 0 5px;" href="" type="button" rel="tooltip" title="{{ __('common.limpiar_filtro') }}" ng-click="vm.limpiarFiltros()">
                                    <i class="material-icons">clear_all</i>
                                </a>
                                <span style="text-transform: none; font-size: 14px;">{{ __('common.sin_resultados') }}</span>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="row" style="margin: 0;" ng-if="!vm.cargando && vm.coleccion.length > 0">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 hidden-xs text-left" style="padding: 0;">
                    <nav>
                        <ul class="pagination pagination-primary">
                            <li class="text-left">
                                <a style="padding: 0 5px;" type="button" rel="tooltip" title="{{ __('common.limpiar_filtros') }}" ng-click="vm.limpiarFiltros()">
                                    <i class="material-icons">clear_all</i>
                                </a>
                            </li>
                            <li class="text-left">
                                <span style="text-transform: none; font-size: 14px;">
                                    @{{ 'Mostrando ' + vm.paginacion.desde + ' a ' + vm.paginacion.hasta + ' de ' + vm.paginacion.total + ' resultados - Página ' + vm.paginacion.paginaActual + ' de ' + vm.paginacion.ultimaPagina }}
                                </span>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding: 0;">
                    <div class="text-right" st-pagination="" st-items-by-page="vm.itemsPorPagina" st-displayed-pages="5" st-template="/paginacion-template" style="float: right;"></div>
                    <div class="text-right" style="float: right; margin: 26px 25px 26px 0;">
                        <label style="color: rgba(0,0,0,0.68);">
                            Filas:
                            <select ng-options="option for option in [5, 10, 25, 50, 100, 500] track by option" ng-model="vm.itemsPorPagina" class="select-grid"></select>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>$(document).ready(function(){$.material.init();});</script>
</div>
