<div id="proceso_juzgado_fecha" class="row" st-table="vm.coleccion" st-pipe="vm.obtenerResumen" st-persist="resumenProcesoJuzgadoFecha" st-reset="isReset" refresh-table>
    <div class="card main-card" style="margin-bottom: 0;">
        <div class="card-content">
            <h4 class="card-title">@{{ vm.titulo }}</h4>
            @can('CrearProceso')
            @endcan
            <button class="btn btn-primary btn-round btn-fab nex-btn-add" title="Nuevo proceso" ng-click="vm.crear()">
                    <i class="material-icons">add</i>
            </button>
            <button class="btn btn-success btn-fab nex-btn-add" title="Exportar a excel" ng-click="vm.obtenerExcel()" style="right: 65px;">
                <i class="mdi mdi-file-excel"></i>
            </button>
            <button  class="btn btn-default btn-fab nex-btn-add" style="top: 75px;" type="button" rel="tooltip" title="{{ __('common.limpiar_filtros') }}" ng-click="vm.limpiarFiltros()">
                <i class="material-icons">clear_all</i>
            </button>
            <button class="btn btn-primary btn-fab nex-btn-add" title="{{ __('common.buscar') }}" ng-click="vm.buscar()" ng-disabled="vm.deshabilitarBuscado" style="top: 140px;">
                <i class="material-icons">search</i>
            </button>
            <form name="vm.form" autocomplete="off" novalidate>
                <div class="row">
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <div class="input-group">
                            <div ng-class="['form-group label-floating', {'is-empty': !vm.filtroFecha, 'is-not-empty': vm.filtroFecha, 'with-error': vm.buscando && vm.form.fecha.$invalid}]">
                                <label class="control-label">{{ __('common.fecha') }}<span class="text-danger"> *</span></label>
                                <input st-search-datetimepicker search-model="vm.filtroFecha" search-predicate="fecha" datetimepicker-st-table
                                       datetimepicker-options="{useCurrent: false, format: 'YYYY-MM-DD'}" ng-model="vm.filtroFecha" name="fecha" required class="form-control" type="text"/>
                                <i class="input-group-addon fa fa-calendar"></i>
                                <span class="help-block show" ng-show="vm.buscando && vm.form.fecha.$invalid">
                                    <span ng-show="vm.form.fecha.$error.required">Requerido</span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7  col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
                        <div ng-class="['form-group label-floating', {'is-empty': !vm.filtroCiudad, 'is-not-empty': vm.filtroCiudad, 'with-error': vm.buscando && vm.form.ciudad.$invalid}]">
                            <label class="control-label">{{ __('common.ciudad') }}<span class="text-danger"> *</span></label>
                            <select selectize="vm.configCiudad" options="vm.opcionesCiudad" disabled-typing="true" ng-model="vm.filtroCiudad" search-model="vm.filtroCiudad"
                                    search-predicate="ciudad" name="ciudad" required class="form-control" clear-options="vm.limpiarOpcionesCiudad"></select>
                            <span class="help-block show" ng-show="vm.buscando && vm.form.ciudad.$invalid">
                                <span ng-show="vm.form.ciudad.$error.required">Requerido</span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <div ng-class="['form-group label-floating', {'is-empty': !vm.filtroJuzgado, 'is-not-empty': vm.filtroJuzgado}]">
                            <label class="control-label">{{ __('common.juzgado') }}</label>
                            <select selectize="vm.configJuzgado" options="vm.opcionesJuzgado" disabled-typing="true" ng-model="vm.filtroJuzgado" search-model="vm.filtroJuzgado"
                                    search-predicate="juzgado" name="juzgado" class="form-control" clear-options="vm.limpiarOpcionesJuzgado"></select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
                        <div ng-class="['form-group label-floating', {'is-empty': !vm.filtroDespacho, 'is-not-empty': vm.filtroDespacho}]">
                            <label class="control-label">{{ __('common.despacho') }}</label>
                            <select selectize="vm.configDespacho" options="vm.opcionesDespacho" disabled-typing="true" ng-model="vm.filtroDespacho" search-model="vm.filtroDespacho"
                                    search-predicate="despacho" name="despacho" class="form-control" clear-options="vm.limpiarOpcionesDespacho"></select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
                        <div ng-class="['form-group label-floating', {'is-empty': !vm.filtroProceso, 'is-not-empty': vm.filtroProceso}]">
                            <label class="control-label">{{ __('common.proceso_') }}</label>
                            <input st-search="proceso" ng-model="vm.filtroProceso" class="form-control">
                        </div>
                    </div>
                </div>
            </form>
            <hr>
            <div class="row" style="margin: 0;" ng-if="!vm.cargando && vm.coleccion.length > 0">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 hidden-xs text-left" style="padding: 0;">
                    <nav>
                        <ul class="pagination pagination-primary">
                            <li class="text-left">
                                <a style="padding: 0 5px;" href="" type="button" rel="tooltip" title="{{ __('common.limpiar_filtros') }}" ng-click="vm.limpiarFiltros()">
                                    <i class="material-icons">clear_all</i>
                                </a>
                            </li>
                            <li class="text-left">
                                <span style="text-transform: none; font-size: 14px;">
                                    @{{ 'Mostrando ' + vm.paginacion.desde + ' a ' + vm.paginacion.hasta + ' de ' + vm.paginacion.total + ' resultados - Página ' + vm.paginacion.paginaActual + ' de ' + vm.paginacion.ultimaPagina }}
                                </span>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding: 0;">
                    <div class="text-right" st-pagination="" st-items-by-page="vm.itemsPorPagina" st-displayed-pages="5" st-template="/paginacion-template" style="float: right;"></div>
                    <div class="text-right" style="float: right; margin: 26px 25px 26px 0;">
                        <label style="color: rgba(0,0,0,0.68);">
                            Filas:
                            <select ng-options="option for option in [5, 10, 25, 50, 100, 500] track by option" ng-model="vm.itemsPorPagina" class="select-grid"></select>
                        </label>
                    </div>
                </div>
            </div>
            <div class="table-responsive" style="font-size: 12px;">
                <table class="table table-hover table-sm table-ellipsis">
                    <thead>
                        <tr>
                            <th st-multi-sort="fecha" class="text-right" style="width: 66px;">{{ __('common.fecha') }}</th>
                            <th st-multi-sort="nombre_ciudad">{{ __('common.nombre') }}<br>{{ __('common.ciudad') }}</th>
                            <th st-multi-sort="juzgado_actual" style="width: 66px;">{{ __('common.juzgado') }}<br>{{ __('common.actual') }}</th>
                            <th st-multi-sort="nombre_despacho" class="text-right">{{ __('common.nro.') }}<br>{{ __('common.desp.') }}</th>
                            <th st-multi-sort="proceso" style="width: 159px">{{ __('common.proceso_') }}</th>
                            <th st-multi-sort="demandante" style="width: 150px;">{{ __('common.demandante') }}</th>
                            <th st-multi-sort="demandado" style="width: 150px;">{{ __('common.demandado') }}</th>
                            <th st-multi-sort="actuacion" style="width: 170px;">{{ __('common.actuacion_') }}</th>
                            <th st-multi-sort="resultado" style="max-width: 71px;">{{ __('common.resultado' )}}</th>
                            <th class="text-center">{{ __('common.imagen') }}</th>
                        </tr>
                    </thead>
                    <thead ng-show="vm.cargando">
                        <tr>
                            <td colspan="10" class="text-center" style="padding: 0px;">
                                <div class="progress progress-striped active" style="margin: 0px;">
                                    <div class="progress-bar" style="width: 100%"></div>
                                </div>
                            </td>
                        </tr>
                    </thead>
                    <tbody ng-show="!vm.cargando">
                        <tr ng-repeat="row in vm.coleccion">
                            <td class="text-right">@{{row.fecha_actuacion}}</td>
                            <td>@{{row.ciudad_nombre}}</td>
                            <td>@{{row.juzgado_nombre}}</td>
                            <td class="text-right">@{{row.despacho_numero}}</td>
                            <td>@{{row.numero_proceso}}</td>
                            <td>@{{row.nombres_demandantes}}</td>
                            <td>@{{row.nombres_demandados}}</td>
                            <td>@{{row.actuacion}}</td>
                            <td>
                                <span ng-if="row.proceso_id != null">
                                     <span ng-if="row.proceso_id">Procesado</span>
                                     <span ng-if="!row.proceso_id">No hay proceso afectado con esta actuación</span>
                                </span>
                                <span ng-if="row.proceso_id == null">No hay proceso afectado con esta actuación</span>
                            </td>
                            <td class="td-actions text-center">
                                <a type="button" rel="tooltip" class="btn btn-primary btn-simple" title="{{ __('common.imagen') }}" ng-click="vm.obtenerArchivoAnexo(row)">
                                    <i class="fa fa-eye"></i>
                                </a>
                            </td>
                        </tr>
                    </tbody>
                    <tfoot ng-if="!vm.cargando && vm.coleccion.length > 0">
                        <script>$(document).ready(function(){$.material.ripples('table .btn');});</script>
                    </tfoot>
                </table>
            </div>
            <div class="row" style="margin: 0;" ng-show="!vm.cargando && vm.coleccion.length == 0">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 0;">
                    <nav>
                        <ul class="pagination pagination-primary">
                            <li class="text-left">
                                <a style="padding: 0 5px;" href="" type="button" rel="tooltip" title="{{ __('common.limpiar_filtro') }}" ng-click="vm.limpiarFiltros()">
                                    <i class="material-icons">clear_all</i>
                                </a>
                                <span style="text-transform: none; font-size: 14px;">{{ __('common.sin_resultados') }}</span>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="row" style="margin: 0;" ng-if="!vm.cargando && vm.coleccion.length > 0">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 hidden-xs text-left" style="padding: 0;">
                    <nav>
                        <ul class="pagination pagination-primary">
                            <li class="text-left">
                                <a style="padding: 0 5px;" type="button" rel="tooltip" title="{{ __('common.limpiar_filtros') }}" ng-click="vm.limpiarFiltros()">
                                    <i class="material-icons">clear_all</i>
                                </a>
                            </li>
                            <li class="text-left">
                                <span style="text-transform: none; font-size: 14px;">
                                    @{{ 'Mostrando ' + vm.paginacion.desde + ' a ' + vm.paginacion.hasta + ' de ' + vm.paginacion.total + ' resultados - Página ' + vm.paginacion.paginaActual + ' de ' + vm.paginacion.ultimaPagina }}
                                </span>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding: 0;">
                    <div class="text-right" st-pagination="" st-items-by-page="vm.itemsPorPagina" st-displayed-pages="5" st-template="/paginacion-template" style="float: right;"></div>
                    <div class="text-right" style="float: right; margin: 26px 25px 26px 0;">
                        <label style="color: rgba(0,0,0,0.68);">
                            Filas:
                            <select ng-options="option for option in [5, 10, 25, 50, 100, 500] track by option" ng-model="vm.itemsPorPagina" class="select-grid"></select>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>$(document).ready(function(){$.material.init();});</script>
</div>
