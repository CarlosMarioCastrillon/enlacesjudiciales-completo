<div class="modal-header">
    <h4 class="card-title">{{ __('common.datos_despacho') }}</h4>
</div>
<div class="modal-body" style="padding-top: 0;">
    <form name="vm.form" autocomplete="off" novalidate>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.departamentoId, 'is-not-empty': vm.departamentoId, 'with-error': vm.guardando && vm.form.departamento.$invalid}]">
                    <label class="control-label">{{ __('common.departamento') }}</label>
                    <select selectize="vm.configDepartamento" options="vm.opcionesDepartamentos" disabled-typing="false" ng-model="vm.departamentoId" name="departamento" required class="form-control"></select>
                    <span class="help-block show" ng-show="vm.guardando && vm.form.departamento.$invalid">
                        <span ng-show="vm.form.departamento.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.ciudadId, 'is-not-empty': vm.ciudadId, 'with-error': vm.guardando && vm.form.ciudad.$invalid}]">
                    <label class="control-label">{{ __('common.ciudad') }}</label>
                    <select selectize="vm.configCiudad" options="vm.opcionesCiudad" disabled-typing="false" ng-model="vm.ciudadId" name="ciudad" required class="form-control" clear-options="vm.limpiarOpcionesCiudad"></select>
                    <span class="help-block show">Debe seleccionar el departamento</span>
                    <br>
                    <span class="help-block show" ng-show="vm.guardando && vm.form.ciudad.$invalid">
                        <span ng-show="vm.form.ciudad.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.despachoReferencia, 'is-not-empty': vm.despachoReferencia}]">
                    <label class="control-label">{{ __('common.despacho_referencia') }}</label>
                    <select selectize="vm.configDespacho" options="vm.opcionesDespachos" disabled-typing="false" ng-model="vm.despachoReferencia" class="form-control"></select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.nombre, 'is-not-empty': vm.nombre, 'with-error': vm.guardando && vm.form.nombre.$invalid}]">
                    <label class="control-label">{{ __('common.nombre') }}</label>
                    <input ng-model="vm.nombre" name="nombre" required maxlength="191" class="form-control" type="text">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.nombre.$invalid">
                        <span ng-show="vm.form.nombre.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.juzgado, 'is-not-empty': vm.juzgado, 'with-error': vm.guardando && vm.form.juzgado.$invalid}]" style="padding-bottom: 30px; margin-top: 40px;">
                    <label class="control-label" style="top: -30px;">{{ __('common.codigo') }}<br>{{ __('common.juzgado_corporacion') }}</label>
                    <input ng-model="vm.juzgado"  ng-model-options="{allowInvalid: true }" name="juzgado" ng-pattern="/^[0-9]{2}$/" required maxlength="2" class="form-control" type="text">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.juzgado.$invalid">
                        <span ng-show="vm.form.juzgado.$error.required">Requerido</span>
                        <span ng-show="vm.form.juzgado.$error.pattern">El Código del juzgado o corporación debe ser un número de 2 dígitos</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.sala, 'is-not-empty': vm.sala, 'with-error': vm.guardando && vm.form.sala.$invalid}]" style="padding-bottom: 30px; margin-top: 40px;">
                    <label class="control-label" style="top: -30px;">{{ __('common.codigo') }}<br>{{ __('common.sala_especialidad') }}</label>
                    <input ng-model="vm.sala"  ng-model-options="{allowInvalid: true }" name="sala" ng-pattern="/^[0-9]{2}$/" required maxlength="2" class="form-control" type="text">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.sala.$invalid">
                        <span ng-show="vm.form.sala.$error.required">Requerido</span>
                        <span ng-show="vm.form.sala.$error.pattern">El Código de la sala o especialidad debe ser un número de 2 dígitos</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.despacho, 'is-not-empty': vm.despacho, 'with-error': vm.guardando && vm.form.despacho.$invalid}]" style="padding-bottom: 30px; margin-top: 40px; ">
                    <label class="control-label">{{ __('common.numero_despacho') }}</label>
                    <input ng-model="vm.despacho"  ng-model-options="{allowInvalid: true }" name="despacho" ng-pattern="/^[0-9]{3}$/" required maxlength="3" class="form-control" type="text">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.despacho.$invalid">
                        <span ng-show="vm.form.despacho.$error.required">Requerido</span>
                        <span ng-show="vm.form.despacho.$error.pattern">El número del despacho debe ser de 3 dígitos</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <p style="margin-bottom: 0;">{{ __('common.estado') }}</p>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="vm.estado" ng-value="true">
                                {{ __('common.activo') }}
                            </label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="vm.estado" ng-value="false">
                                {{ __('common.inactivo') }}
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button class="btn btn-primary" type="button" ng-click="vm.guardar()" ng-disabled="vm.deshabilitarGuardado">{{ __('common.guardar') }}</button>
    <button class="btn btn-default" type="button" ng-click="vm.cancelar()">{{ __('common.cancelar') }}</button>
</div>
<script>$(document).ready(function(){$.material.init();});</script>
