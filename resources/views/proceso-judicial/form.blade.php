<div class="modal-header">
    <h4 class="card-title">{{ __('common.datos_proceso_judicial') }}</h4>
</div>
<div class="modal-body" style="padding-top: 0; padding-bottom: 0;" cg-busy="vm.promesasActivas">
    <form name="vm.form" autocomplete="off" novalidate>
        <div class="row" ng-show="!!vm.modoEdicion">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <p style="margin-bottom: 0;">{{ __('common.cambio_de_instancia') }}</p>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="radio">
                            <label ng-click="vm.establecerCambioInstancia();">
                                <input type="radio" ng-model="vm.conCambioInstancia" ng-value="vm.CambioInstanciaEnum.SUPERIOR">
                                {{ __('common.superior') }}
                            </label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="radio">
                            <label ng-click="vm.establecerCambioInstancia()";>
                                <input type="radio" ng-model="vm.conCambioInstancia" ng-value="vm.CambioInstanciaEnum.INFERIOR">
                                {{ __('common.anterior') }}
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" ng-show="vm.modoEdicion && !!vm.conCambioInstancia" style="margin-top: 10px; margin-bottom: 10px;">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <strong>Informacion actual del proceso:</strong>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <strong>Nuevos datos por cambio de instancia:</strong>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" ng-show="vm.modoEdicion && !!vm.conCambioInstancia" style="background-color: #f5f4f4;">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.departamentoNombre, 'is-not-empty': vm.departamentoNombre}]">
                    <label class="control-label">{{ __('common.departamento') }}</label>
                    <input ng-model="vm.departamentoNombre" class="form-control" type="text" disabled>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.departamentoId, 'is-not-empty': vm.departamentoId, 'with-error': vm.guardando && vm.form.departamento.$invalid}]">
                    <label class="control-label">{{ __('common.departamento') }}</label>
                    <select selectize="vm.configDepartamento" options="vm.opcionesDepartamento" ng-model="vm.departamentoId" name="departamento" required class="form-control"></select>
                    <span class="help-block show" ng-show="vm.guardando && vm.form.departamento.$invalid">
                        <span ng-show="vm.form.departamento.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" ng-show="vm.modoEdicion && !!vm.conCambioInstancia" style="background-color: #f5f4f4;">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.ciudadNombre, 'is-not-empty': vm.ciudadNombre}]">
                    <label class="control-label">{{ __('common.ciudad') }}</label>
                    <input ng-model="vm.ciudadNombre" class="form-control" type="text" disabled>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.ciudadId, 'is-not-empty': vm.ciudadId, 'with-error': vm.guardando && vm.form.ciudad.$invalid}]">
                    <label class="control-label">{{ __('common.ciudad') }}</label>
                    <select selectize="vm.configCiudad" options="vm.opcionesCiudad" ng-model="vm.ciudadId" name="ciudad" required class="form-control" clear-options="vm.limpiarOpcionesCiudad"></select>
                    <span class="help-block show" ng-show="vm.guardando && vm.form.ciudad.$invalid">
                        <span ng-show="vm.form.ciudad.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" ng-show="vm.modoEdicion && !!vm.conCambioInstancia" style="background-color: #f5f4f4;">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.juzgadoNombre, 'is-not-empty': vm.juzgadoNombre}]">
                    <label class="control-label">{{ __('common.juzgado_actual') }}</label>
                    <input ng-model="vm.juzgadoNombre" class="form-control" type="text" disabled>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.juzgadoId, 'is-not-empty': vm.juzgadoId, 'with-error': vm.guardando && vm.form.juzgado.$invalid}]">
                    <label class="control-label">{{ __('common.juzgado_actual') }}</label>
                    <select selectize="vm.configJuzgado" options="vm.opcionesJuzgado" ng-model="vm.juzgadoId" name="juzgado" required class="form-control" clear-options="vm.limpiarOpcionesJuzgado"></select>
                    <span class="help-block show" ng-show="vm.guardando && vm.form.juzgado.$invalid">
                        <span ng-show="vm.form.juzgado.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" ng-show="vm.modoEdicion && !!vm.conCambioInstancia" style="background-color: #f5f4f4;">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.despachoNombre, 'is-not-empty': vm.despachoNombre}]">
                    <label class="control-label">{{ __('common.numero_juzgado_actual') }}</label>
                    <input ng-model="vm.despachoNombre" class="form-control" type="text" disabled>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.despachoId, 'is-not-empty': vm.despachoId, 'with-error': vm.guardando && vm.form.despacho.$invalid}]">
                    <label class="control-label">{{ __('common.numero_juzgado_actual') }}</label>
                    <select selectize="vm.configDespacho" options="vm.opcionesDespacho" ng-model="vm.despachoId" name="despacho" required class="form-control" clear-options="vm.limpiarOpcionesDespacho"></select>
                    <span class="help-block show" ng-show="vm.guardando && vm.form.despacho.$invalid">
                        <span ng-show="vm.form.despacho.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" ng-show="vm.modoEdicion && !!vm.conCambioInstancia" style="background-color: #f5f4f4;">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.anioNombre, 'is-not-empty': vm.anioNombre}]">
                    <label class="control-label">{{ __('common.anio') }}</label>
                    <input ng-model="vm.anioNombre" class="form-control" type="text" disabled>
                </div>
            </div>
            <div ng-class="['col-xs-12', {'col-sm-3 col-md-3 col-lg-3': !(vm.modoEdicion && !!vm.conCambioInstancia), 'col-sm-6 col-md-6 col-lg-6': vm.modoEdicion && !!vm.conCambioInstancia}]">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.anio, 'is-not-empty': vm.anio, 'with-error': vm.guardando && vm.form.anio.$invalid}]">
                    <label class="control-label">{{ __('common.anio') }}</label>
                    <input datetimepicker datetimepicker-options="@{{ vm.anioOptions }}" ng-model="vm.anio" name="anio" required maxlength="4" class="form-control" type="text" ng-blur="vm.validarAnio()"/>
                    <span class="help-block show" ng-show="vm.guardando && vm.form.anio.$invalid">
                        <span ng-show="vm.form.anio.$error.required">Requerido</span>
                        <span ng-show="vm.form.anio.$error.maxlength">Máximo 4 dígitos</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" ng-show="vm.modoEdicion && !!vm.conCambioInstancia" style="background-color: #f5f4f4;">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.radicadoNombre, 'is-not-empty': vm.radicadoNombre}]">
                    <label class="control-label">{{ __('common.radicado') }}</label>
                    <input ng-model="vm.radicadoNombre" class="form-control" type="text" disabled>
                </div>
            </div>
            <div ng-class="['col-xs-12', {'col-sm-3 col-md-3 col-lg-3': !(vm.modoEdicion && !!vm.conCambioInstancia), 'col-sm-6 col-md-6 col-lg-6': vm.modoEdicion && !!vm.conCambioInstancia}]">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.radicado, 'is-not-empty': vm.radicado, 'with-error': vm.guardando && vm.form.radicado.$invalid}]">
                    <label class="control-label">{{ __('common.radicado') }}</label>
                    <input ng-model="vm.radicado" name="radicado" required maxlength="5" class="form-control only-numbers" type="text" ng-blur="vm.validarRadicado()">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.radicado.$invalid">
                        <span ng-show="vm.form.radicado.$error.required">Requerido</span>
                        <span ng-show="vm.form.radicado.$error.maxlength">Máximo 5 dígitos</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" ng-show="vm.modoEdicion && !!vm.conCambioInstancia" style="background-color: #f5f4f4;">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.consecutivoNombre, 'is-not-empty': vm.consecutivoNombre}]">
                    <label class="control-label">Consecutivo</label>
                    <input ng-model="vm.consecutivoNombre" class="form-control" type="text" disabled>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" ng-show="vm.modoEdicion && !!vm.conCambioInstancia">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.consecutivo, 'is-not-empty': vm.consecutivo, 'with-error': vm.guardando && vm.form.consecutivo.$invalid}]">
                    <label class="control-label">Consecutivo</label>
                    <input ng-model="vm.consecutivo" name="consecutivo" ng-required="vm.modoEdicion && !!vm.conCambioInstancia" maxlength="2" class="form-control only-numbers" type="text" ng-change="vm.modificarConsecutivo()" ng-blur="vm.validarConsecutivo()">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.consecutivo.$invalid">
                        <span ng-show="vm.form.consecutivo.$error.required">Requerido</span>
                        <span ng-show="vm.form.consecutivo.$error.maxlength">Máximo 2 dígitos</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" ng-show="vm.modoEdicion && !!vm.conCambioInstancia" style="background-color: #f5f4f4;">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.radicadoProcesoActualNombre, 'is-not-empty': vm.radicadoProcesoActualNombre}]">
                    <label class="control-label">{{ __('common.radicado_proceso_actual') }}</label>
                    <input ng-model="vm.radicadoProcesoActualNombre" class="form-control" type="text" disabled>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.radicadoProcesoActual, 'is-not-empty': vm.radicadoProcesoActual, 'with-error': (vm.form.radicadoProcesoActual.$dirty && vm.radicadoProcesoActual.length < 23) || (vm.guardando && vm.form.radicadoProcesoActual.$invalid)}]">
                    <label class="control-label">{{ __('common.radicado_proceso_actual') }}</label>
                    <input ng-model="vm.radicadoProcesoActual" name="radicadoProcesoActual" required maxlength="23" class="form-control only-numbers" type="text" ng-change="vm.confirmarValidar()" ng-blur="vm.validar()">
                    <span class="help-block show" ng-show="vm.form.radicadoProcesoActual.$dirty && vm.radicadoProcesoActual.length < 23">
                        <span>Mínimo 23 dígitos</span>
                    </span>
                    <span class="help-block show" ng-show="vm.guardando && vm.form.radicadoProcesoActual.$invalid">
                        <span ng-show="vm.form.radicadoProcesoActual.$error.required">Requerido</span>
                        <span ng-show="vm.form.radicadoProcesoActual.$error.maxlength">Máximo 23 dígitos</span>
                    </span>
                </div>
            </div>
        </div>
        <div class="row" ng-hide="vm.camposSinValidarOcultos">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.demandante, 'is-not-empty': vm.demandante, 'with-error': vm.guardando && vm.form.demandante.$invalid}]">
                    <label class="control-label">{{ __('common.demandante') }}</label>
                    <input ng-model="vm.demandante" name="demandante" required class="form-control" type="text" ng-change="vm.validarDemandante()">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.demandante.$invalid">
                        <span ng-show="vm.form.demandante.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.demandado, 'is-not-empty': vm.demandado, 'with-error': vm.guardando && vm.form.demandado.$invalid}]">
                    <label class="control-label">{{ __('common.demandado') }}</label>
                    <input ng-model="vm.demandado" name="demandado" required class="form-control" type="text" ng-change="vm.validarDemandado()">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.demandado.$invalid">
                        <span ng-show="vm.form.demandado.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.observaciones, 'is-not-empty': vm.observaciones}]">
                    <label class="control-label">{{ __('common.observaciones') }}</label>
                    <input ng-model="vm.observaciones" class="form-control" type="text" ng-change="vm.validarObservaciones()">
                </div>
            </div>
        </div>
        <div class="row" ng-hide="vm.camposSinValidarOcultos && !vm.modoEdicion">
            <div ng-class="['col-xs-12', {
                'col-sm-3 col-md-3 col-lg-3': !!vm.modoEdicion,
                'col-sm-6 col-md-6 col-lg-6': !vm.modoEdicion && vm.verResponsable && (vm.verClaseProceso || vm.verArea),
                'col-sm-12 col-md-12 col-lg-12': vm.verResponsable && !(vm.verClaseProceso && vm.verArea)
            }]" ng-show="vm.verResponsable">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.responsableId, 'is-not-empty': vm.responsableId}]">
                    <label class="control-label" >{{ __('common.responsable') }}</label>
                    <select selectize="vm.configResponsable" options="vm.opcionesResponsable" ng-model="vm.responsableId" class="form-control"></select>
                </div>
            </div>
            <div ng-class="['col-xs-12', {
                'col-sm-3 col-md-3 col-lg-3': !!vm.modoEdicion || (vm.verClaseProceso && vm.verResponsable && vm.verArea),
                'col-sm-6 col-md-6 col-lg-6': !vm.modoEdicion && vm.verClaseProceso && ((vm.verResponsable && !vm.verArea) || (!vm.verResponsable && vm.verArea)),
                'col-sm-12 col-md-12 col-lg-12': !vm.modoEdicion && vm.verClaseProceso && !(vm.verResponsable && vm.verArea)
            }]" ng-show="vm.verClaseProceso">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.claseProcesoId, 'is-not-empty': vm.claseProcesoId}]">
                    <label class="control-label" >{{ __('common.clase_de_proceso') }}</label>
                    <select selectize="vm.configClaseProceso" options="vm.opcionesClaseProceso" ng-model="vm.claseProcesoId" class="form-control"></select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" ng-show="!!vm.modoEdicion">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.etapaProcesoId, 'is-not-empty': vm.etapaProcesoId}]">
                    <label class="control-label" >{{ __('common.etapa_de_proceso') }}</label>
                    <select selectize="vm.configEtapaProceso" options="vm.opcionesEtapaProceso" ng-model="vm.etapaProcesoId" class="form-control"></select>
                </div>
            </div>
            <div ng-class="['col-xs-12', {
                'col-sm-3 col-md-3 col-lg-3': !!vm.modoEdicion || (vm.verArea && vm.verResponsable && vm.verClaseProceso),
                'col-sm-6 col-md-6 col-lg-6': !vm.modoEdicion && vm.verArea && ((vm.verResponsable && !vm.verClaseProceso) || (!vm.verResponsable && vm.verClaseProceso)),
                'col-sm-12 col-md-12 col-lg-12': !vm.modoEdicion && vm.verArea && !(vm.verResponsable && vm.verClaseProceso)
            }]" ng-show="vm.verArea">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.areaId, 'is-not-empty': vm.areaId}]">
                    <label class="control-label" >{{ __('common.area') }}</label>
                    <select selectize="vm.configArea" options="vm.opcionesArea" ng-model="vm.areaId" class="form-control"></select>
                </div>
            </div>
        </div>
        <div class="row" ng-show="!!vm.modoEdicion">
            <div class="col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.clienteId, 'is-not-empty': vm.clienteId}]">
                    <label class="control-label">{{ __('common.cliente') }}</label>
                    <select selectize="vm.configCliente" options="vm.opcionesCliente" ng-model="vm.clienteId" class="form-control"></select>
                </div>
            </div>
            <div class="col-sm-3 col-md-3 col-lg-3">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.tipoActuacionId, 'is-not-empty': vm.tipoActuacionId}]">
                    <label class="control-label">{{ __('common.actuacion_') }}</label>
                    <select selectize="vm.configTipoActuacion" options="vm.opcionesTipoActuacion" ng-model="vm.tipoActuacionId" class="form-control"></select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <p style="margin-bottom: 0;">{{ __('common.estado') }}</p>
                    </div>
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="vm.estado" ng-value="true">
                                {{ __('common.activo') }}
                            </label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="vm.estado" ng-value="false">
                                {{ __('common.inactivo') }}
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="modal-footer" style="padding: 10px 24px 14px 0;">
    <button class="btn btn-primary" type="button" ng-click="vm.validarYGuardar()" ng-disabled="vm.guardadoDeshabilitado">
        <span ng-hide="vm.camposSinValidarOcultos">{{ __('common.guardar') }}</span>
        <span ng-hide="!vm.camposSinValidarOcultos">{{ __('common.validar') }}</span>
    </button>
    <button class="btn btn-default" type="button" ng-click="vm.cancelar()" ng-disabled="vm.guardadoDeshabilitado">{{ __('common.cancelar') }}</button>
</div>
<script>$(document).ready(function(){$.material.init();});</script>


