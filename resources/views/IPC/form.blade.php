<div class="modal-header">
    <h4 class="card-title">{{ __('common.datos_IPC') }}</h4>
</div>
<div class="modal-body">
    <form name="vm.form" autocomplete="off" novalidate>
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="    padding-bottom: 21px;">
              <div class="input-group">
                    <div ng-class="['form-group label-floating', {'is-empty': !vm.fecha, 'is-not-empty': vm.fecha, 'with-error': vm.guardando && vm.form.fecha.$invalid}]">
                        <label class="control-label">{{ __('common.fecha') }}</label>
                        <input datetimepicker datetimepicker-options="{useCurrent: false, format: 'YYYY-MM-DD', maxDate: 'now' }" ng-model="vm.fecha" class="form-control" name="fecha" required type="text"/>
                        <i class="input-group-addon fa fa-calendar en-modal"></i>
                        <span class="help-block show" ng-show="vm.guardando && vm.form.fecha.$invalid">
                            <span ng-show="vm.form.fecha.$error.required">Requerido</span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.indiceIPC, 'is-not-empty': vm.indiceIPC || vm.indiceIPC==0, 'with-error': vm.guardando && vm.form.indiceIPC.$invalid||vm.indiceIPC==0}]">
                    <label class="control-label">{{ __('common.indice_IPC') }}</label>
                    <input ng-model="vm.indiceIPC" ng-model-options="{allowInvalid: true }" name="indiceIPC" min="0.01" required pattern="[0-9]+([\.][0-9]{0,2})?"  class="form-control" type="text">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.indiceIPC.$invalid||vm.indiceIPC==0">
                        <span ng-show="vm.form.indiceIPC.$error.required && vm.indiceIPC != 0">Requerido</span>
                        <span ng-show="vm.indiceIPC == 0">El campo debe ser mayor que cero</span>
                        <span ng-show="vm.form.indiceIPC.$error.pattern">El campo debe contener valores numéricos mayores a cero</span>
                    </span>
                    <br>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.inflacionAnual, 'is-not-empty': vm.inflacionAnual, 'with-error': vm.guardando && vm.form.inflacionAnual.$invalid}]">
                    <label class="control-label">{{ __('common.inflacion_anual') }}</label>
                    <input ng-model="vm.inflacionAnual" ng-model-options="{allowInvalid: true }" name="inflacionAnual" required maxlength="6" max="100" pattern="^-?[0-9]+([\.][0-9]{0,2})?" class="form-control" type="text">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.inflacionAnual.$invalid">
                        <span ng-show="vm.form.inflacionAnual.$error.required">Requerido</span>
                        <span ng-show="vm.form.inflacionAnual.$error.pattern">El campo debe contener valores entre +/- 0 y 100</span>
                    </span>
                    <br>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.inflacionMensual, 'is-not-empty': vm.inflacionMensual, 'with-error': vm.guardando && vm.form.inflacionMensual.$invalid}]">
                    <label class="control-label">{{ __('common.inflacion_mensual') }}</label>
                    <input ng-model="vm.inflacionMensual" ng-model-options="{allowInvalid: true }" name="inflacionMensual" required maxlength="6" max="100" pattern="^-?[0-9]+([\.][0-9]{0,2})?" class="form-control" type="text">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.inflacionMensual.$invalid">
                        <span ng-show="vm.form.inflacionMensual.$error.required">Requerido</span>
                        <span ng-show="vm.form.inflacionMensual.$error.pattern">El campo debe contener valores entre +/- 0 y 100</span>
                    </span>
                    <br>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.inflacionAnio, 'is-not-empty': vm.inflacionAnio, 'with-error': vm.guardando && vm.form.inflacionAnio.$invalid}]">
                    <label class="control-label">{{ __('common.inflacion_anio') }}</label>
                    <input ng-model="vm.inflacionAnio" ng-model-options="{allowInvalid: true }" name="inflacionAnio" required maxlength="6" max="100" pattern="^-?[0-9]+([\.][0-9]{0,2})?" class="form-control" type="text">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.inflacionAnio.$invalid">
                        <span ng-show="vm.form.inflacionAnio.$error.required">Requerido</span>
                        <span ng-show="vm.form.inflacionAnio.$error.pattern">El campo debe contener valores entre +/- 0 y 100</span>
                    </span>
                    <br>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <p style="margin-bottom: 0;">{{ __('common.estado') }}</p>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="vm.estado" ng-value="true">
                                {{ __('common.activo') }}
                            </label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="vm.estado" ng-value="false">
                                {{ __('common.inactivo') }}
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button class="btn btn-primary" type="button" ng-click="vm.guardar()" ng-disabled="vm.deshabilitarGuardado">{{ __('common.guardar') }}</button>
    <button class="btn btn-default" type="button" ng-click="vm.cancelar()">{{ __('common.cancelar') }}</button>
</div>
<script>$(document).ready(function(){$.material.init();});</script>
