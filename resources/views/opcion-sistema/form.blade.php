<div class="modal-header">
    <h4 class="card-title">{{ __('common.datos_opcion_del_sistema') }}</h4>
</div>
<div class="modal-body">
    <form name="vm.form" autocomplete="off" novalidate>
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.nombre, 'is-not-empty': vm.nombre, 'with-error': vm.guardando && vm.form.nombre.$invalid}]">
                    <label class="control-label">{{ __('common.nombre') }}</label>
                    <input ng-model="vm.nombre" name="nombre" required maxlength="191" class="form-control" type="text">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.nombre.$invalid">
                        <span ng-show="vm.form.nombre.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.moduloId, 'is-not-empty': vm.moduloId}]">
                    <label class="control-label">{{ __('common.modulo') }}</label>
                    <select selectize="vm.configModulo" options="vm.opcionesModulo" ng-model="vm.moduloId" class="form-control"></select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.url, 'is-not-empty': vm.url, 'with-error': vm.guardando && vm.form.url.$invalid}]">
                    <label class="control-label">{{ __('common.url_redireccion') }}</label>
                    <input ng-model="vm.url" name="url" required maxlength="191" class="form-control" type="text">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.url.$invalid">
                        <span ng-show="vm.form.url.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.urlAyuda, 'is-not-empty': vm.urlAyuda}]">
                    <label class="control-label">{{ __('common.url_ayuda') }}</label>
                    <input ng-model="vm.urlAyuda" maxlength="191" class="form-control" type="text">
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.iconoMenu, 'is-not-empty': vm.iconoMenu}]">
                    <label class="control-label">{{ __('common.icono') }}</label>
                    <input ng-model="vm.iconoMenu" maxlength="191" class="form-control" type="text">
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.clavePermiso, 'is-not-empty': vm.clavePermiso}]">
                    <label class="control-label">{{ __('common.clave_permiso') }}</label>
                    <input ng-model="vm.clavePermiso" maxlength="191" class="form-control" type="text">
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.posicion, 'is-not-empty': vm.posicion, 'with-error': vm.guardando && vm.form.posicion.$invalid}]">
                    <label class="control-label">{{ __('common.posicion') }}</label>
                    <input type="number" ng-model="vm.posicion" name="posicion" required maxlength="191" class="form-control" type="text">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.posicion.$invalid">
                        <span ng-show="vm.form.posicion.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <p style="margin-bottom: 0;">{{ __('common.estado') }}</p>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="vm.estado" ng-value="true">
                                {{ __('common.activo') }}
                            </label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="vm.estado" ng-value="false">
                                {{ __('common.inactivo') }}
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button class="btn btn-primary" type="button" ng-click="vm.guardar()" ng-disabled="vm.deshabilitarGuardado">{{ __('common.guardar') }}</button>
    <button class="btn btn-default" type="button" ng-click="vm.cancelar()">{{ __('common.cancelar') }}</button>
</div>
<script>$(document).ready(function(){$.material.init();});</script>
