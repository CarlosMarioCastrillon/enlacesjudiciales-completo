<div class="modal-header">
    <h4 class="card-title">{{ __('common.datos_cliente') }}</h4>
</div>
<div class="modal-body" style="padding-top: 0; padding-bottom: 0px;">
    <form name="vm.form" autocomplete="off" novalidate>
        <div class="row" style="margin-bottom: -4px;">
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.tipoDocumentoId, 'is-not-empty': vm.tipoDocumentoId, 'with-error': vm.guardando && vm.form.tipoDocumento.$invalid}]">
                    <label class="control-label">{{ __('common.tipo_documento') }}</label>
                    <select selectize="vm.configTipoDocumento" options="vm.opcionesTipoDocumento" disabled-typing="true" ng-model="vm.tipoDocumentoId" name="tipoDocumento" required class="form-control"></select>
                    <span class="help-block show" ng-show="vm.guardando && vm.form.tipoDocumento.$invalid">
                        <span ng-show="vm.form.tipoDocumento.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.numeroDocumento, 'is-not-empty': vm.numeroDocumento, 'with-error': vm.guardando && vm.form.numeroDocumento.$invalid}]">
                    <label class="control-label" style="margin-top: 12px;">{{ __('common.numero_documento') }}</label>
                    <input ng-model="vm.numeroDocumento" name="numeroDocumento" required maxlength="191" class="form-control" type="text">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.numeroDocumento.$invalid">
                            <span ng-show="vm.form.numeroDocumento.$error.required">Requerido</span>
                        </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.nombre, 'is-not-empty': vm.nombre, 'with-error': vm.guardando && vm.form.nombre.$invalid}]">
                    <label class="control-label">{{ __('common.nombre') }}</label>
                    <input ng-model="vm.nombre" name="nombre" required maxlength="191" class="form-control" type="text">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.nombre.$invalid">
                    <span ng-show="vm.form.nombre.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.departamentoId, 'is-not-empty': vm.departamentoId, 'with-error': vm.guardando && vm.form.departamento.$invalid}]">
                    <label class="control-label">{{ __('common.departamento') }}</label>
                    <select selectize="vm.configDepartamento" options="vm.opcionesDepartamentos" disabled-typing="true" ng-model="vm.departamentoId" name="departamento" class="form-control"></select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.ciudadId, 'is-not-empty': vm.ciudadId, 'with-error': vm.guardando && vm.form.ciudad.$invalid}]">
                    <label class="control-label">{{ __('common.ciudad') }}</label>
                    <select selectize="vm.configCiudad" options="vm.opcionesCiudad" disabled-typing="true" ng-model="vm.ciudadId" name="ciudad" class="form-control" clear-options="vm.limpiarOpcionesCiudad"></select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.direccion, 'is-not-empty': vm.direccion, 'with-error': vm.guardando && vm.form.direccion.$invalid}]">
                    <label class="control-label">{{ __('common.direccion') }}</label>
                    <input ng-model="vm.direccion" name="direccion" maxlength="191" class="form-control" type="text">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.direccion.$invalid">
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.telefonoFijo, 'is-not-empty': vm.telefonoFijo, 'with-error': vm.guardando && vm.form.telefonoFijo.$invalid}]">
                    <label class="control-label">{{ __('common.telefono_fijo') }}</label>
                    <input ng-model="vm.telefonoFijo"  ng-model-options="{allowInvalid: true }" name="telefonoFijo" ng-pattern="/^[0-9]{7,15}$/" maxlength="15" class="form-control">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.telefonoFijo.$invalid">
                            <span ng-show="vm.form.telefonoFijo.$error.pattern">Ingrese un número de teléfono válido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.telefonoCelular, 'is-not-empty': vm.telefonoCelular, 'with-error': !!vm.telefonoCelular && vm.form.telefonoCelular.$invalid}]">
                    <label class="control-label">{{ __('common.telefono_celular') }}</label>
                    <input ng-model="vm.telefonoCelular"  ng-model-options="{allowInvalid: true }" name="telefonoCelular" ng-pattern="/^[0-9]{10,15}$/" maxlength="15" class="form-control">
                    <span class="help-block show" ng-show="!!vm.telefonoCelular && vm.form.telefonoCelular.$invalid">
                        <span ng-show="vm.form.telefonoCelular.$error.pattern">Ingrese un número de celular válido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.correo, 'is-not-empty': vm.correo, 'with-error': vm.guardando && vm.form.correo.$invalid}]" style="margin-bottom: 20px;">
                    <label class="control-label">{{ __('common.correo_electronico') }}</label>
                    <input ng-model="vm.correo"  ng-model-options="{allowInvalid: true }" name="correo" email class="form-control" type="email">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.correo.$invalid">
                            <span ng-show="vm.form.email.$error.email">Debe ingresar un correo válido<</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <p style="margin-bottom: 0;">{{ __('common.estado') }}</p>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="vm.estado" ng-value="true">
                                {{ __('common.activo') }}
                            </label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="vm.estado" ng-value="false">
                                {{ __('common.inactivo') }}
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button class="btn btn-primary" type="button" ng-click="vm.guardar()" ng-disabled="vm.deshabilitarGuardado">{{ __('common.guardar') }}</button>
    <button class="btn btn-default" type="button" ng-click="vm.cancelar()">{{ __('common.cancelar') }}</button>
</div>
<script>$(document).ready(function(){$.material.init();});</script>


