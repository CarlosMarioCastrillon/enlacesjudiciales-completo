<div id="auxiliar-justicia-importar" class="row">
    <div class="card main-card" style="margin-bottom: 0;">
        <div class="card-content">
            <h4 class="card-title">@{{ vm.titulo }}</h4>
            <style>
                .wizard {
                    margin: 20px auto;
                    background: #fff;
                }

                .wizard .nav-tabs {
                    position: relative;
                    margin: 40px auto;
                    margin-bottom: 0;
                    border-bottom-color: #e0e0e0;
                    background: #49de6021;
                }

                .wizard > div.wizard-inner {
                    position: relative;
                }

                .connecting-line {
                    height: 2px;
                    background: #e0e0e0;
                    position: absolute;
                    width: 70%;
                    margin: 0 auto;
                    left: 0;
                    right: 0;
                    top: 50%;
                    z-index: 1;
                }

                .wizard .nav-tabs > li.active > a, .wizard .nav-tabs > li.active > a:hover, .wizard .nav-tabs > li.active > a:focus {
                    color: #555555;
                    cursor: default;
                    border: 0;
                    border-bottom-color: transparent;
                }

                span.round-tab {
                    width: 70px;
                    height: 70px;
                    line-height: 70px;
                    display: inline-block;
                    border-radius: 100px;
                    background: #fff;
                    border: 2px solid #e0e0e0;
                    z-index: 2;
                    position: absolute;
                    left: 0;
                    text-align: center;
                    font-size: 25px;
                }
                span.round-tab i{
                    color:#555555;
                }
                .wizard li.active span.round-tab {
                    background: #fff;
                    border: 2px solid #5bc0de;

                }
                .wizard li.active span.round-tab i{
                    color: #5bc0de;
                }

                span.round-tab:hover {
                    color: #333;
                    border: 2px solid #333;
                }

                .wizard .nav-tabs > li {
                    width: 25%;
                }

                .wizard li:after {
                    content: " ";
                    position: absolute;
                    left: 46%;
                    opacity: 0;
                    margin: 0 auto;
                    bottom: 0px;
                    border: 5px solid transparent;
                    border-bottom-color: #5bc0de;
                    transition: 0.1s ease-in-out;
                }

                .wizard li.active:after {
                    content: " ";
                    position: absolute;
                    left: 46%;
                    opacity: 1;
                    margin: 0 auto;
                    bottom: 0px;
                    border: 10px solid transparent;
                    border-bottom-color: #5bc0de;
                }

                .wizard .nav-tabs > li a {
                    width: 70px;
                    height: 70px;
                    margin: 20px auto;
                    border-radius: 100%;
                    padding: 0;
                }

                .wizard .nav-tabs > li a:hover {
                    background: transparent;
                }

                .wizard .tab-pane {
                    position: relative;
                    padding-top: 50px;
                }

                .wizard h3 {
                    margin-top: 0;
                }

                @media( max-width : 585px ) {

                    .wizard {
                        width: 90%;
                        height: auto !important;
                    }

                    span.round-tab {
                        font-size: 16px;
                        width: 50px;
                        height: 50px;
                        line-height: 50px;
                    }

                    .wizard .nav-tabs > li a {
                        width: 50px;
                        height: 50px;
                        line-height: 50px;
                    }

                    .wizard li.active:after {
                        content: " ";
                        position: absolute;
                        left: 35%;
                    }
                }
            </style>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <section>
                        <div class="wizard">
                            <div class="wizard-inner">
                                <div class="connecting-line"></div>
                                <ul class="nav nav-tabs" role="tablist" style="margin-top: -10px; margin-bottom: 21px;">
                                    <li role="presentation" ng-class="{'active': vm.pasoImportarDatos, 'disabled': !vm.pasoImportarDatos}">
                                        <a data-toggle="tab" aria-controls="step1" role="tab" title="1. Cargar archivo">
                                            <span class="round-tab">
                                                <i class="glyphicon glyphicon-folder-open"></i>
                                            </span>
                                        </a>
                                        <div align="center" style="margin-top: -7%; padding-bottom: 8px;"><small>Cargar archivo</small></div>
                                    </li>
                                    <li role="presentation" ng-class="{'active': vm.pasoVerificarDatos, 'disabled': !vm.pasoVerificarDatos}" style="float: right;">
                                        <a data-toggle="tab" aria-controls="complete" role="tab" title="2. Verificación de datos">
                                        <span class="round-tab">
                                            <i class="glyphicon glyphicon-ok"></i>
                                        </span>
                                        </a>
                                        <div align="center" style="margin-top: -7%; padding-bottom: 8px;"><small>Verificación de datos</small></div>
                                    </li>
                                </ul>
                            </div>

                            <div class="tab-content" style="margin-top: 0;">
                                <div ng-class="['tab-pane', {'active': vm.pasoImportarDatos}]" role="tabpanel">
                                    <form role="form">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <h4 class="card-title" style="margin-bottom: 10px;">1. Cargar archivo</h4>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <div class="attachment-img-drop-box"
                                                     ngf-drop="vm.cargarArchivo($file)"
                                                     ngf-multiple="false"
                                                     ngf-allow-dir="true"
                                                     ngf-drag-over-class="'dragover'"
                                                     ngf-pattern="'.xls,.xlsx'"
                                                     accept="'.xls,.xlsx'"
                                                     ngf-resize="{width: 250, height: 100, quality: 1}"
                                                     style="padding: 20px 0 0 0; min-height: 206px;">
                                                    <div style="text-align: center;">
                                                        <p>Arrastre el archivo aquí o pulse para subir: <a style="cursor: pointer;" type="button" ngf-multiple="false" ngf-select="vm.cargarArchivo($file)">Seleccione archivo para carga</a></p>
                                                    </div>
                                                    <div class="row" ng-show="vm.archivo">
                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                                                             <span class="attachment-item-container">
                                                                <a ng-click="vm.descargarArchivo(vm.archivo)">
                                                                    @{{ vm.archivoNombre }}
                                                                </a>
                                                                <a type="button" rel="tooltip" class="btn btn-xs btn-danger btn-simple attachment-item-btn-delete"
                                                                   title="Eliminar" ng-click="vm.eliminarArchivo(vm.archivoNombre)">
                                                                    <i class="material-icons">delete</i>
                                                                </a>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div ngf-no-file-drop>Arrastre de archivo no permitido en este navegador</div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right">
                                                <button type="button" class="btn btn-md btn-primary" ng-click="vm.importarExcel()" ng-disabled="!vm.archivo || vm.cargandoArchivo">
                                                    Cargar y continuar
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div ng-class="['tab-pane', {'active': vm.pasoVerificarDatos}]" role="tabpanel" style="top: -52px;">
                                    <form role="form">
                                        <div class="row">
                                            <div id="auxiliarJusticiaCargaErros" class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                    <div class="card-content">
                                                        <h4 class="card-title" style="margin-bottom: 10px;">2. Verificación de datos</h4>
                                                        <button class="btn btn-success btn-fab nex-btn-add" title="Exportar a excel" ng-click="vm.obtenerExcel()" style="right: 16px;">
                                                            <i class="mdi mdi-file-excel"></i>
                                                        </button>
                                                        <br>
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                <strong class="control-label text-left">{{ __('common.registros_procesados') }}:</strong>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                <span class="control-label text-left">@{{vm.registrosProcesados}}</span>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                <strong class="control-label text-left">{{ __('common.registros_cargados') }}:</strong>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                <span class="control-label text-left">@{{vm.registrosCargados}}</span>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                                <strong class="control-label text-left">{{ __('common.registros_fallidos') }}:</strong>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                <span class="control-label text-left">@{{vm.registrosFallidos}}</span>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <div class="table-responsive">
                                                            <table class="table table-hover table-sm table-ellipsis">
                                                                <thead>
                                                                    <tr>
                                                                        <th style="width: 5%;">{{ __('common.numero_documento') }}</th>
                                                                        <th>{{ __('common.nombres') }}</th>
                                                                        <th>{{ __('common.apellidos') }}</th>
                                                                        <th style="width: 110px;">{{ __('common.cargo') }}</th>
                                                                        <th style="min-width: 40%;">{{ __('common.observacion') }}</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr ng-repeat="row in vm.errores">
                                                                        <td class="text-left">@{{row.numero_documento}}</td>
                                                                        <td class="text-left">@{{row.nombres_auxiliar}}</td>
                                                                        <td class="text-left">@{{row.apellidos_auxiliar}}</td>
                                                                        <td class="text-left">@{{row.cargo}}</td>
                                                                        <td class="text-left" ng-bind-html="row.observacion"></td>
                                                                    </tr>
                                                                </tbody>
                                                                <tfoot ng-if="!vm.cargando && vm.coleccion.length > 0">
                                                                    <script>$(document).ready(function(){$.material.ripples('table .btn');});</script>
                                                                </tfoot>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right">
                                                        <button type="button" class="btn btn-md btn-default" ng-click="vm.finalizarImportarExcel()">
                                                            Finalizar
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div ng-class="['tab-pane', {'active': vm.datosCorrectos}]" role="tabpanel" style="top: -52px;">
                                    <a type="button" rel="tooltip" class="btn btn-xs btn-danger btn-simple attachment-item-btn-delete" title="Terminado" ng-click="vm.eliminarArchivo(vm.archivoNombre)">
                                        <i class="material-icons">delete</i>
                                    </a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
    <script>$(document).ready(function(){$.material.init();});</script>
</div>
