<div class="modal-header">
    <h4 class="card-title">{{ __('common.datos_auxiliar_justicia') }}</h4>
</div>
<div class="modal-body" style="padding-top: 0; padding-bottom: 0px;">
    <form name="vm.form" autocomplete="off" novalidate>
        <div class="row" style="margin-bottom: -4px;">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.numeroDocumento, 'is-not-empty': vm.numeroDocumento, 'with-error': vm.guardando && vm.form.numeroDocumento.$invalid}]">
                    <label class="control-label" style="margin-top: 12px;">{{ __('common.numero_documento') }}</label>
                    <input ng-model="vm.numeroDocumento" name="numeroDocumento" required maxlength="191" class="form-control" type="text">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.numeroDocumento.$invalid">
                            <span ng-show="vm.form.numeroDocumento.$error.required">Requerido</span>
                        </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.nombresAuxiliares, 'is-not-empty': vm.nombresAuxiliares, 'with-error': vm.guardando && vm.form.nombresAuxiliares.$invalid}]" style="top: -10px;">
                    <label class="control-label">{{ __('common.nombres') }}</label>
                    <input ng-model="vm.nombresAuxiliares" name="nombresAuxiliares" required maxlength="191" class="form-control" type="text">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.nombresAuxiliares.$invalid">
                    <span ng-show="vm.form.nombresAuxiliares.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.apellidosAuxiliares, 'is-not-empty': vm.apellidosAuxiliares, 'with-error': vm.guardando && vm.form.apellidosAuxiliares.$invalid}]" style="top: -10px;">
                    <label class="control-label">{{ __('common.apellidos') }}</label>
                    <input ng-model="vm.apellidosAuxiliares" name="apellidosAuxiliares" required maxlength="191" class="form-control" type="text">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.apellidosAuxiliares.$invalid">
                    <span ng-show="vm.form.apellidosAuxiliares.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.ciudad, 'is-not-empty': vm.ciudad, 'with-error': vm.guardando && vm.form.ciudad.$invalid}]" style="top: -10px;">
                    <label class="control-label">{{ __('common.ciudad') }}</label>
                    <input ng-model="vm.ciudad" name="ciudad" required class="form-control" maxlength="191" type="text">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.ciudad.$invalid">
                    <span ng-show="vm.form.ciudad.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.direccionOficina, 'is-not-empty': vm.direccionOficina, 'with-error': vm.guardando && vm.form.direccionOficina.$invalid}]" style="top: -10px;">
                    <label class="control-label">{{ __('common.direccion_oficina') }}</label>
                    <input ng-model="vm.direccionOficina" name="direccionOficina" required maxlength="191" class="form-control" type="text">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.direccionOficina.$invalid">
                    <span ng-show="vm.form.direccionOficina.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.telefonoOficina, 'is-not-empty': vm.telefonoOficina, 'with-error': vm.guardando && vm.form.telefonoOficina.$invalid}]" style="top: -10px;">
                    <label class="control-label">{{ __('common.telefono_oficina') }}</label>
                    <input ng-model="vm.telefonoOficina"  ng-model-options="{allowInvalid: true }" name="telefonoOficina" ng-pattern="/^[0-9]{7,15}$/" required maxlength="15" class="form-control">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.telefonoOficina.$invalid">
                            <span ng-show="vm.form.telefonoOficina.$error.required">Requerido</span>
                            <span ng-show="vm.form.telefonoOficina.$error.pattern">Ingrese un número de teléfono válido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.cargo, 'is-not-empty': vm.cargo, 'with-error': vm.guardando && vm.form.cargo.$invalid}]" style="top: -10px;">
                    <label class="control-label">{{ __('common.cargo') }}</label>
                    <input ng-model="vm.cargo" name="cargo" required maxlength="191" class="form-control" type="text">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.cargo.$invalid">
                    <span ng-show="vm.form.cargo.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.telefonoCelular, 'is-not-empty': vm.telefonoCelular, 'with-error': !!vm.telefonoCelular && vm.form.telefonoCelular.$invalid}]" style="top: -10px;">
                    <label class="control-label">{{ __('common.telefono_celular') }}</label>
                    <input ng-model="vm.telefonoCelular"  ng-model-options="{allowInvalid: true }" name="telefonoCelular" ng-pattern="/^[0-9]{10,15}$/" maxlength="15" class="form-control">
                    <span class="help-block show" ng-show="!!vm.telefonoCelular && vm.form.telefonoCelular.$invalid">
                        <span ng-show="vm.form.telefonoCelular.$error.pattern">Ingrese un número de celular válido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.direccionResidencia, 'is-not-empty': vm.direccionResidencia, 'with-error': vm.guardando && vm.form.direccionResidencia.$invalid}]" style="top: -10px;">
                    <label class="control-label">{{ __('common.direccion_residencia') }}</label>
                    <input ng-model="vm.direccionResidencia" name="direccionResidencia" maxlength="191" class="form-control" type="text">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.direccionResidencia.$invalid">
                    <span ng-show="vm.form.direccionResidencia.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.telefonoResidencia, 'is-not-empty': vm.telefonoResidencia, 'with-error': !!vm.telefonoResidencia && vm.form.telefonoResidencia.$invalid}]" style="top: -10px;">
                    <label class="control-label">{{ __('common.telefono_residencia') }}</label>
                    <input ng-model="vm.telefonoResidencia"  ng-model-options="{allowInvalid: true }" name="telefonoResidencia" ng-pattern="/^[0-9]{7,15}$/" maxlength="15" class="form-control">
                    <span class="help-block show" ng-show="!!vm.telefonoResidencia && vm.form.telefonoResidencia.$invalid">
                        <span ng-show="vm.form.telefonoResidencia.$error.pattern">Ingrese un teléfono válido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.correo, 'is-not-empty': vm.correo, 'with-error': vm.guardando && vm.form.correo.$invalid}]" style="top: -10px;">
                    <label class="control-label">{{ __('common.correo_electronico') }}</label>
                    <input ng-model="vm.correo" name="correo" required email class="form-control" type="email">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.correo.$invalid">
                            <span ng-show="vm.form.email.$error.required">Requerido</span>
                            <span ng-show="vm.form.email.$error.email">Debe ingresar un correo válido<</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <p style="margin-bottom: 0;">{{ __('common.estado') }}</p>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="vm.estado" ng-value="true">
                                {{ __('common.activo') }}
                            </label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="vm.estado" ng-value="false">
                                {{ __('common.inactivo') }}
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button class="btn btn-primary" type="button" ng-click="vm.guardar()" ng-disabled="vm.deshabilitarGuardado">{{ __('common.guardar') }}</button>
    <button class="btn btn-default" type="button" ng-click="vm.cancelar()">{{ __('common.cancelar') }}</button>
</div>
<script>$(document).ready(function(){$.material.init();});</script>


