<div id="permiso" class="row">
    <div class="card main-card" style="margin-bottom: 5px;">
        <div class="card-content">
            <h4 class="card-title">{{ __('common.permisos') }}</h4>
            <div class="row">
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" ng-show="vm.rol">
                    <label class="card-item-label">{{ trans('common.rol') }}</label>
                    <p>@{{ vm.rol.nombre }}</p>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" ng-show="vm.rol">
                    <label class="card-item-label">{{ trans('common.tipo_rol') }}</label>
                    <p ng-if="vm.rol.tipo == vm.TipoRolEnum.CLIENTE">Del cliente</p>
                    <p ng-if="vm.rol.tipo == vm.TipoRolEnum.SISTEMA">Del sistema</p>
                    <p ng-if="vm.rol.tipo == vm.TipoRolEnum.CLIENTE_DE_CLIENTE">Del cliente de mi cliente</p>
                </div>
            </div>
        </div>
    </div>
    <div class="card main-card">
        <div class="card-content">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div ng-class="['form-group label-floating', {'is-empty': !vm.moduloId, 'is-not-empty': vm.moduloId}]">
                        <label class="control-label">{{ __('common.modulo') }}</label>
                        <select selectize="vm.configModulo" options="vm.opcionesModulo" ng-model="vm.moduloId" class="form-control" ng-change="vm.filtrar()"></select>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div ng-class="['form-group label-floating', {'is-empty': !vm.submoduloId, 'is-not-empty': vm.submoduloId}]">
                        <label class="control-label">{{ __('common.submodulo') }}</label>
                        <select selectize="vm.configSubmodulo" options="vm.opcionesSubmodulo" ng-model="vm.submoduloId" clear-options="vm.limpiarOpcionesSubmodulo" class="form-control" ng-change="vm.filtrar()"></select>
                        <span class="help-block show">
                            Debe seleccionar el módulo
                        </span>
                    </div>
                </div>
            </div>
            <hr>
            <div ng-repeat="(key, modulo) in vm.coleccion">
                <div data-toggle="collapse" aria-expanded="true" data-target="#accordion@{{ key }}" class="clickable main-module-list">
                    <div>
                        <h4 class="panel-title" style="font-weight: 400">@{{modulo.nombre}}</h4>
                        <i class="mdi mdi-chevron-down mdi-24px module-list-icon" aria-hidden="true"></i>
                        <i class="mdi mdi-chevron-up mdi-24px module-list-icon" aria-hidden="true"></i>
                    </div>
                </div>
                <div id="accordion@{{ key }}" class="collapse in">
                    <div ng-class="['in sub-module-list', {'with-border': k != modulo.submodulos.length - 1}]" ng-if="modulo.submodulos"
                         ng-repeat="(k, submodulo) in modulo.submodulos">
                        <div>
                            <div data-toggle="collapse" data-target="#accordion@{{ key }}@{{ k }}" aria-expanded="true" class="sub-module-list-toggle">
                                <i class="mdi mdi-chevron-down mdi-24px"></i><i class="mdi mdi-chevron-right mdi-24px"></i>
                                <span>@{{submodulo.nombre}}</span>
                            </div>
                            <div id="accordion@{{ key }}@{{ k }}" class="collapse in">
                                <div class="under-sub-module-list">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3"
                                             ng-repeat="permiso in submodulo.permisos">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="optionsCheckboxes" ng-model="permiso.permitido" ng-change="vm.cambiarPermiso(permiso)">
                                                    <span class="checkbox-text">@{{ permiso.nombre }}</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="sub-module-list" ng-if="modulo.permisos">
                        <div>
                            <div class="under-sub-module-list">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3"
                                         ng-repeat="permiso in modulo.permisos">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="optionsCheckboxes" ng-model="permiso.permitido">
                                                <span class="checkbox-text">@{{ permiso.nombre }}</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <script>$(document).ready(function () {$.material.init();});</script>
            </div>
        </div>
    </div>
    <script>$(document).ready(function () {$.material.init();});</script>
</div>


