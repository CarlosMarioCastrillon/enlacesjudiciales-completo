<nav ng-if="numPages && pages.length >= 2">
    <ul class="pagination pagination-primary">
        <li>
            <a ng-click="selectPage(1)" href="javascript: void(0);" style="padding: 1px 1px; margin: 0;">
                <i class="material-icons">first_page</i>
            </a>
        </li>
        <li>
            <a ng-click="selectPage(currentPage - 1)" href="javascript: void(0);" style="padding: 1px 1px; margin: 0;">
                <i class="material-icons">navigate_before</i>
            </a>
        </li>
        <li ng-repeat="page in pages" ng-class="{active: page==currentPage}">
            <a href="javascript: void(0);" ng-click="selectPage(page)">
                @{{page}}
            </a>
        </li>
        <li>
            <a ng-click="selectPage(currentPage + 1)" href="javascript: void(0);" style="padding: 1px 1px; margin: 0;">
                <i class="material-icons">navigate_next</i>
            </a>
        </li>
        <li>
            <a ng-click="selectPage(numPages)" href="javascript: void(0);" style="padding: 1px 1px; margin: 0;">
                <i class="material-icons">last_page</i>
            </a>
        </li>
    </ul>
</nav>