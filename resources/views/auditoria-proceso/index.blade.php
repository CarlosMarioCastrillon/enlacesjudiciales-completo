<div id="auditoria-procesos" class="row" st-table="vm.coleccion" st-pipe="vm.obtenerResumen" st-persist="resumenProgramaciones" st-reset="isReset" refresh-table>
    <div class="card main-card" style="margin-bottom: 0;">
        <div class="card-content">
            <h4 class="card-title">@{{ vm.titulo }}</h4>
            @can('ProgramarAuditoriaProcesos')
                <button ng-class="['btn btn-round btn-fab nex-btn-add', {'btn-primary': !vm.formularioVisible, 'btn-danger': vm.formularioVisible}]" title="@{{ !vm.formularioVisible ? 'Nueva programación' : 'Cerrar formulario' }}"
                   ng-click="vm.abrirFormulario()" ng-disabled="vm.cargandoArchivo">
                    <i class="material-icons" ng-if="!vm.formularioVisible">add</i>
                    <i class="material-icons" ng-if="vm.formularioVisible">close</i>
                </button>
            @endcan
            <div class="row" ng-show="vm.formularioVisible">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card" style="margin: 20px 0 0 0;">
                        <div class="card-content">
                            <form name="vm.form" autocomplete="off" novalidate>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                        <div class="input-group">
                                            <div ng-class="['form-group label-floating', {'is-empty': !vm.fechaProgramacion, 'is-not-empty': vm.fechaProgramacion}]">
                                                <label class="control-label">{{ __('common.fecha_programacion') }}</label>
                                                <input datetimepicker datetimepicker-options="{sideBySide: true, format : 'YYYY-MM-DD HH:mm'}" ng-model="vm.fechaProgramacion" class="form-control" type="text"/>
                                                <i class="input-group-addon fa fa-calendar"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                        <div class="input-group">
                                            <div ng-class="['form-group label-floating', {'is-empty': !vm.fechaProgramacionPasada, 'is-not-empty': vm.fechaProgramacionPasada}]">
                                                <label class="control-label">{{ __('common.fecha_programacion_pasada') }}</label>
                                                <input datetimepicker datetimepicker-options="{sideBySide: true, format : 'YYYY-MM-DD HH:mm'}" ng-model="vm.fechaProgramacionPasada" class="form-control" type="text"/>
                                                <i class="input-group-addon fa fa-calendar"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                        <div class="form-group">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" ng-model="vm.recientes"> {{ trans('common.con_actuaciones_recientes') }}
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="attachment-img-drop-box"
                                             ngf-drop="vm.cargarArchivo($file)"
                                             ngf-multiple="false"
                                             ngf-allow-dir="true"
                                             ngf-drag-over-class="'dragover'"
                                             ngf-pattern="'.xls,.xlsx'"
                                             accept="'.xls,.xlsx'"
                                             ngf-resize="{width: 250, height: 100, quality: 1}"
                                             style="padding: 20px 0 0 0; min-height: 100px;">
                                            <div style="text-align: center;">
                                                <p>Arrastre el archivo aquí o pulse para subir: <a style="cursor: pointer;" type="button" ngf-multiple="false" ngf-select="vm.cargarArchivo($file)">Seleccione archivo para carga</a></p>
                                            </div>
                                            <div class="row" ng-show="vm.archivo">
                                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                                                     <span class="attachment-item-container">
                                                        <a ng-click="vm.descargarArchivo(vm.archivo)">
                                                            @{{ vm.archivoNombre }}
                                                        </a>
                                                        <a type="button" rel="tooltip" class="btn btn-xs btn-danger btn-simple attachment-item-btn-delete"
                                                           title="Eliminar" ng-click="vm.eliminarArchivo(vm.archivoNombre)">
                                                            <i class="material-icons">delete</i>
                                                        </a>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div ngf-no-file-drop>Arrastre de archivo no permitido en este navegador</div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="card-footer text-right">
                            <button class="btn btn-primary" type="button" ng-click="vm.programar()" ng-disabled="vm.cargandoArchivo">{{ __('common.guardar') }}</button>
                            <button class="btn btn-default" type="button" ng-click="vm.cancelar()" ng-disabled="vm.cargandoArchivo">{{ __('common.cancelar') }}</button>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row" style="margin: 0;" ng-if="!vm.cargando && vm.coleccion.length > 0">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 hidden-xs text-left" style="padding: 0;">
                    <nav>
                        <ul class="pagination pagination-primary">
                            <li class="text-left">
                                <a style="padding: 0 5px;" href="" type="button" rel="tooltip" title="{{ __('common.limpiar_filtros') }}" ng-click="vm.limpiarFiltros()">
                                    <i class="material-icons">clear_all</i>
                                </a>
                            </li>
                            <li class="text-left">
                                <span style="text-transform: none; font-size: 14px;">
                                    @{{ 'Mostrando ' + vm.paginacion.desde + ' a ' + vm.paginacion.hasta + ' de ' + vm.paginacion.total + ' resultados - Página ' + vm.paginacion.paginaActual + ' de ' + vm.paginacion.ultimaPagina }}
                                </span>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding: 0;">
                    <div class="text-right" st-pagination="" st-items-by-page="vm.itemsPorPagina" st-displayed-pages="5" st-template="/paginacion-template" style="float: right;"></div>
                    <div class="text-right" style="float: right; margin: 26px 25px 26px 0;">
                        <label style="color: rgba(0,0,0,0.68);">
                            Filas:
                            <select ng-options="option for option in [5, 10, 25, 50, 100, 500] track by option" ng-model="vm.itemsPorPagina" class="select-grid"></select>
                        </label>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-hover table-sm">
                    <thead>
                        <tr>
                            <th st-multi-sort="id" class="text-center">#</th>
                            <th st-multi-sort="fecha_programacion" class="text-right">{{ __('common.fecha_programacion') }}</th>
                            <th st-multi-sort="fecha_programacion_pasada" class="text-right">{{ __('common.fecha_programacion_pasada') }}</th>
                            <th st-multi-sort="fecha_ejecucion" class="text-right">{{ __('common.fecha_ejecucion') }}</th>
                            <th st-multi-sort="fecha_proceso" class="text-right">{{ __('common.fecha_proceso') }}</th>
                            <th st-multi-sort="recientes" class="text-center">{{ __('common.recientes') }}</th>
                            <th st-multi-sort="estado" class="text-center">{{ __('common.estado') }}</th>
                            <th st-multi-sort="lotes_ejecutados" class="text-right">{{ __('common.lotes_ejecutados') }}</th>
                            @if(auth()->user()->can('DescargarAuditoriaProcesos') || auth()->user()->can('DescargarNovedadProcesos'))
                                <th class="text-right">{{ __('common.acciones') }}</th>
                            @endif
                        </tr>
                    </thead>
                    <thead ng-show="vm.cargando">
                        <tr>
                            <td colspan="9" class="text-center" style="padding: 0px;">
                                <div class="progress progress-striped active" style="margin: 0px;">
                                    <div class="progress-bar" style="width: 100%"></div>
                                </div>
                            </td>
                        </tr>
                    </thead>
                    <tbody ng-show="!vm.cargando">
                        <tr ng-repeat="row in vm.coleccion">
                            <td class="text-right">@{{row.id}}</td>
                            <td class="text-right">@{{row.fecha_programacion}}</td>
                            <td class="text-right">@{{row.fecha_programacion_pasada ? row.fecha_programacion_pasada : '--'}}</td>
                            <td class="text-right">@{{row.fecha_ejecucion ? row.fecha_ejecucion : '--'}}</td>
                            <td class="text-right">@{{row.fecha_proceso ? row.fecha_proceso : '--'}}</td>
                            <td class="text-center">
                                <span ng-if="row.recientes">{{ __('common.si') }}</span>
                                <span ng-if="!row.recientes">{{ __('common.no') }}</span>
                            </td>
                            <td class="text-center">
                                <span class="label label-default" ng-if="row.estado == 1">{{ __('common.sin_ejecutar') }}</span>
                                <span class="label label-warning" ng-if="row.estado == 2">{{ __('common.en_ejecucion') }}</span>
                                <span class="label label-success" ng-if="row.estado == 3">{{ __('common.ejecutado') }}</span>
                            </td>
                            <td class="text-right">@{{ !!row.lotes ? row.lotes_ejecutados + ' / ' +  row.lotes : '--'}}</td>
                            @if(auth()->user()->can('DescargarAuditoriaProcesos') || auth()->user()->can('DescargarNovedadProcesos'))
                                <td class="td-actions text-right">
                                    @can('DescargarAuditoriaProcesos')
                                        <a type="button" rel="tooltip" class="btn btn-info btn-simple" title="{{ __('common.descargar_auditoria') }}" ng-click="vm.descargarAuditoria(row)">
                                            <i class="mdi mdi-file-excel-outline mdi-18px"></i>
                                        </a>
                                    @endcan
                                    @can('DescargarNovedadProcesos')
                                        <a type="button" rel="tooltip" class="btn btn-success btn-simple" title="{{ __('common.descargar_novedades') }}" ng-click="vm.descargarNovedades(row)">
                                            <i class="mdi mdi-file-excel mdi-18px"></i>
                                        </a>
                                    @endcan
                                </td>
                            @endif
                        </tr>
                    </tbody>
                    <tfoot ng-if="!vm.cargando && vm.coleccion.length > 0">
                        <script>$(document).ready(function(){$.material.ripples('table .btn');});</script>
                    </tfoot>
                </table>
            </div>
            <div class="row" style="margin: 0;" ng-show="!vm.cargando && vm.coleccion.length == 0">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 0;">
                    <nav>
                        <ul class="pagination pagination-primary">
                            <li class="text-left">
                                <a style="padding: 0 5px;" href="" type="button" rel="tooltip" title="{{ __('common.limpiar_filtro') }}" ng-click="vm.limpiarFiltros()">
                                    <i class="material-icons">clear_all</i>
                                </a>
                                <span style="text-transform: none; font-size: 14px;">{{ __('common.sin_resultados') }}</span>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="row" style="margin: 0;" ng-if="!vm.cargando && vm.coleccion.length > 0">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 hidden-xs text-left" style="padding: 0;">
                    <nav>
                        <ul class="pagination pagination-primary">
                            <li class="text-left">
                                <a style="padding: 0 5px;" href="" type="button" rel="tooltip" title="{{ __('common.limpiar_filtros') }}" ng-click="vm.limpiarFiltros()">
                                    <i class="material-icons">clear_all</i>
                                </a>
                            </li>
                            <li class="text-left">
                                <span style="text-transform: none; font-size: 14px;">
                                    @{{ 'Mostrando ' + vm.paginacion.desde + ' a ' + vm.paginacion.hasta + ' de ' + vm.paginacion.total + ' resultados - Página ' + vm.paginacion.paginaActual + ' de ' + vm.paginacion.ultimaPagina }}
                                </span>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding: 0;">
                    <div class="text-right" st-pagination="" st-items-by-page="vm.itemsPorPagina" st-displayed-pages="5" st-template="/paginacion-template" style="float: right;"></div>
                    <div class="text-right" style="float: right; margin: 26px 25px 26px 0;">
                        <label style="color: rgba(0,0,0,0.68);">
                            Filas:
                            <select ng-options="option for option in [5, 10, 25, 50, 100, 500] track by option" ng-model="vm.itemsPorPagina" class="select-grid"></select>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>$(document).ready(function(){$.material.init();});</script>
</div>
