<!DOCTYPE html>
<html lang="en" ng-app="enlaces.login">
    <head>
        <meta charset="utf-8">
        <meta name="theme-color" content="#D0AD4C">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />

        <title>{{ env('APP_NAME') }}</title>
        <link rel="shortcut icon" href="/favicon.ico" />

        <!-- Styles -->
        <link rel="stylesheet" href="{{ mix('/css/login-' . env('APP_THEME', 'a') . '.css') }}" type='text/css' media="all">

        <!--  Fonts and icons  -->
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons"/>

        <style>
            button {
                width: 100%;
            }
            img {
                margin-left: -25px;
            }
            @media (min-width: 768px) {
                .login-container {
                    margin-top: 70px;
                }
            }
            #loading-bar .bar {
                background: red;
                height: 2px;
            }
            #loading-bar-spinner .spinner-icon {
                border: solid 2px transparent;
                border-top-color: red;
                border-left-color: red;
            }
        </style>
    </head>
    <body ng-controller="loginController as vm">
        <div class="container-fluid">
            <div class="row login-container">
                <div class="col-xs-12 col-sm-8 col-md-4 col-lg-4 col-sm-offset-2 col-md-offset-4 col-lg-offset-4" ng-if="!vm.enlaceRecuperacionPasswordEnviado">
                    <div class="card">
                        <div class="card-content">
                            <h4 class="card-title">{{ trans('common.olvido_su_contrasenia') }}</h4>
                            <form name="vm.form" autocomplete="off" novalidate>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <p class="text-justify">
                                            {{ __('common.indicanos_tu_identificacion_para_restablecer_contrasenia') }}
                                        </p>
                                        <div ng-class="['form-group label-floating', {'is-empty': !vm.username, 'with-error': vm.submitted && vm.form.username.$invalid}]">
                                            <label class="control-label">{{ trans('common.identificacion') }}</label>
                                            <input type="text" class="form-control" ng-model="vm.username" name="username" required maxlength="255">
                                            <span class="help-block show" ng-show="vm.submitted && vm.form.username.$invalid">
                                                <span ng-show="vm.form.username.$error.required">Requerido</span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left">
                                        <button type="submit" class="btn btn-primary btn-fill" ng-click="vm.recuperarPassword()">{{ trans('common.enviar_correo') }}</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-8 col-md-6 col-lg-6 col-sm-offset-2 col-md-offset-3 col-lg-offset-3" ng-if="vm.enlaceRecuperacionPasswordEnviado">
                    <div class="card">
                        <div class="card-content">
                            <h4 class="card-title">{{ trans('common.enlace_de_restablecimiento_de_password_enviado_titulo') }}</h4>
                            <form name="vm.form" autocomplete="off" novalidate>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <p class="text-justify">
                                            {{ __('common.enlace_de_restablecimiento_de_password_enviado_mensaje') }}
                                        </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left">
                                        <a class="btn btn-primary btn-fill" href="/login">Regresar</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src={{ mix('/js/login.js') }}></script>
    </body>
</html>
<script>$(document).ready(function(){$.material.init();});</script>
