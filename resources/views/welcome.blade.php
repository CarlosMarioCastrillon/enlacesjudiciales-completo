
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="theme-color" content="#D0AD4C">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />

        <title>{{ env('APP_TITLE') }}</title>
        <link rel="shortcut icon" href="/favicon.ico" />

        <!-- Styles -->
        <link rel="stylesheet" href="{{ mix('/css/app-' . env('APP_THEME', 'a') . '.css') }}" type='text/css' media="all">

        <!--  Fonts and icons  -->
        <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons"/>
        <style>
            .logo {
                display: block;
                margin-left: auto;
                margin-right: auto;
                width: 98px;
                height: 64px;
            }
        </style>
    </head>
    <body ng-app="enlaces.app">
        <div class="layout">
            <div class="menu app-inner mat-sidenav-container mat-sidenav-transition" ng-controller="menuController as menu" ng-cloak>
                {{--TODO por el momento solo para escritorio--}}
                <perfect-scrollbar id="menu-container"
                                   style="width: 240px;"
                                   class="sidebar-panel mat-sidenav mat-sidenav-side mat-sidenav-opened"
                                   wheel-propagation="true"
                                   wheel-speed="2"
                                   min-scrollbar-length="20">
                    <div class="navigation mat-nav-list">
                        <div class="navigation mat-nav-list" ng-if="menu.companyLogo">
                            <div class="text-center">
                                <img ng-src="@{{ menu.companyLogo }}" style="max-width: 230px; max-height: 230px;">
                            </div>
                            <hr style="margin-bottom: 0px; margin-top: 10px;">
                        </div>
                        <div ng-repeat="modulo in menu.modulos" ng-class="['mat-list-item', {'open': modulo.abierto}]">
                            <div class="mat-list-item-content">
                                <a class="relative"
                                   style="display: flex; box-sizing: border-box; flex-direction: row;"
                                   href="@{{ modulo.url }}" ng-if="modulo.submodulos.length == 0">
                                    <div class="mat-icon mdi mdi-@{{ modulo.icono_menu }} mdi-24px"></div>
                                    <span>@{{ modulo.nombre }}</span>
                                    <span style="flex: 1 1 1e-09px; box-sizing: border-box;"></span>
                                </a>
                                <a class="relative"
                                   style="display: flex; box-sizing: border-box; flex-direction: row;"
                                   ng-click="modulo.abierto = !modulo.abierto;" ng-if="modulo.submodulos.length > 0">
                                    <div class="mat-icon mdi mdi-@{{ modulo.icono_menu }} mdi-24px"></div>
                                    <span>@{{ modulo.nombre }}</span>
                                    <span style="flex: 1 1 1e-09px; box-sizing: border-box;"></span>
                                    <div class="menu-caret mat-icon material-icons">@{{ modulo.abierto ? 'arrow_drop_up' : 'arrow_drop_down' }}</div>
                                </a>
                                <div class="sub-menu mat-nav-list" ng-if="modulo.submodulos.length > 0">
                                    <div class="mat-list-item" ng-repeat="submodulo in modulo.submodulos">
                                        <div class="mat-list-item-content">
                                            <a class="relative" href="@{{ submodulo.url }}">@{{ submodulo.nombre }}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div ng-if="menu.modulos.length > 0">
                            <script>
                                if(typeof $ !== 'undefined'){
                                    $(document).ready(function(){$.material.ripples('.mat-list-item-content a');});
                                }
                            </script>
                        </div>
                    </div>
                </perfect-scrollbar>
            </div>
            <div class="contenido">
                <div class="main-header mat-toolbar mat-primary" role="toolbar" ng-controller="navController as nav" style="position: -webkit-sticky; position: sticky; top: 0; z-index: 1000;">
                    <div class="mat-toolbar-layout">
                        <div class="mat-toolbar-row">
                            <div class="row-direction"></div>
                            <div class="row-reverse-direction">
                                <div>
                                    <ul class="nav navbar-nav navbar-right hidden-xs" style="margin-right: 0;">
                                        <li>
                                            <a class="btn btn-primary" style="padding: 5px; margin-right: 10px;" ng-click="nav.infoEmpresa()">
                                                <i class="material-icons" style="position: absolute;left: 0;top: 10px;font-size: 35px;">business_center</i>
                                                <div style="margin-left: 35px; text-align: left;">
                                                    <p style="margin: 0;">@{{ nav.session.empresa.numero_documento }}</p>
                                                    <p style="margin: 0;">@{{ nav.session.empresa.nombre }}</p>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="dropdown" style="margin-right: -7px;">
                                            <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" style="padding: 5px;">
                                                <i class="material-icons" style="position: absolute;left: 0;top: 10px;font-size: 35px;">person</i>
                                                <div style="margin-left: 35px; text-align: left;">
                                                    <p style="margin: 0;">@{{ nav.session.tipo_documento.codigo + '  ' + nav.session.usuario.documento }}</p>
                                                    <p style="margin: 0;">@{{ nav.session.usuario.nombre }}</p>
                                                </div>
                                            </a>
                                            <ul class="dropdown-menu" style="margin-top: -10px;">
                                                <li>
                                                    <div class="navbar-login text-left" style="color: #3C4858; padding: 0 5px 0 5px;">
                                                        <strong>@{{ nav.session.rol.nombre }}</strong><br>
                                                        <span class="small">@{{ nav.session.usuario.email }}</span>
                                                    </div>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <div class="navbar-login navbar-login-session text-right">
                                                        <a class="btn btn-danger btn-sm" ng-click="nav.logout()">Cerrar sesión</a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
{{--                <perfect-scrollbar id="view-container"--}}
{{--                                   class="mat-sidenav-content"--}}
{{--                                   update-on="rebuild:scrollbar"--}}
{{--                                   wheel-propagation="true"--}}
{{--                                   wheel-speed="2"--}}
{{--                                   min-scrollbar-length="20">--}}

{{--                </perfect-scrollbar>--}}
                <div id="view-container" class="mat-sidenav-content">
                    <div class="plantillas content" ng-cloak>
                        <div class="container-fluid">
                            <div ng-view></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src={{ mix('/js/libraries.js') }}></script>
        <script src={{ mix('/js/app.js') }}></script>

    </body>
</html>
