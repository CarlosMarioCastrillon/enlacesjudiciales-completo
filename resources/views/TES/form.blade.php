<div class="modal-header">
    <h4 class="card-title">{{ __('common.datos_TES') }}</h4>
</div>
<div class="modal-body">
    <form name="vm.form" autocomplete="off" novalidate>
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="    padding-bottom: 21px;">
              <div class="input-group">
                    <div ng-class="['form-group label-floating', {'is-empty': !vm.fecha, 'is-not-empty': vm.fecha, 'with-error': vm.guardando && vm.form.fecha.$invalid}]">
                        <label class="control-label">{{ __('common.fecha') }}</label>
                        <input datetimepicker datetimepicker-options="{useCurrent: false, format: 'YYYY-MM-DD', maxDate: 'now' }" ng-model="vm.fecha" class="form-control" name="fecha" required type="text"/>
                        <i class="input-group-addon fa fa-calendar en-modal"></i>
                        <span class="help-block show" ng-show="vm.guardando && vm.form.fecha.$invalid">
                            <span ng-show="vm.form.fecha.$error.required">Requerido</span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.porcentaje1Anio, 'is-not-empty': vm.porcentaje1Anio, 'with-error': vm.guardando && vm.form.porcentaje1Anio.$invalid}]">
                    <label class="control-label">{{ __('common.porcentaje_1anio') }}</label>
                    <input ng-model="vm.porcentaje1Anio" name="porcentaje1Anio" required maxlength="10" min="0" max="100" class="form-control" type="text">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.porcentaje1Anio.$invalid">
                        <span ng-show="vm.form.porcentaje1Anio.$error.required">Requerido</span>
                    </span>
                    <br>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.porcentaje5Anio, 'is-not-empty': vm.porcentaje5Anio, 'with-error': vm.guardando && vm.form.porcentaje5Anio.$invalid}]">
                    <label class="control-label">{{ __('common.porcentaje_5anio') }}</label>
                    <input ng-model="vm.porcentaje5Anio" name="porcentaje5Anio" required maxlength="10" min="0" max="100" class="form-control" type="text">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.porcentaje5Anio.$invalid">
                        <span ng-show="vm.form.porcentaje5Anio.$error.required">Requerido</span>
                    </span>
                    <br>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.porcentaje10Anio, 'is-not-empty': vm.porcentaje10Anio, 'with-error': vm.guardando && vm.form.porcentaje10Anio.$invalid}]">
                    <label class="control-label">{{ __('common.porcentaje_10anio') }}</label>
                    <input ng-model="vm.porcentaje10Anio" name="porcentaje10Anio" required maxlength="10" min="0" max="100" class="form-control" type="text">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.porcentaje1Anio.$invalid">
                        <span ng-show="vm.form.porcentaje1Anio.$error.required">Requerido</span>
                    </span>
                    <br>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <p style="margin-bottom: 0;">{{ __('common.estado') }}</p>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="vm.estado" ng-value="true">
                                {{ __('common.activo') }}
                            </label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="vm.estado" ng-value="false">
                                {{ __('common.inactivo') }}
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button class="btn btn-primary" type="button" ng-click="vm.guardar()" ng-disabled="vm.deshabilitarGuardado">{{ __('common.guardar') }}</button>
    <button class="btn btn-default" type="button" ng-click="vm.cancelar()">{{ __('common.cancelar') }}</button>
</div>
<script>$(document).ready(function(){$.material.init();});</script>
