<div class="modal-header">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h4 class="card-title">{{ __('common.proceso_cliente') }}</h4>
    </div>
</div>
<br>
<div class="modal-body">
    <form name="vm.form" autocomplete="off" novalidate>
        <br>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <style>
                hr {
                    margin-top: 4px;
                    margin-bottom: 8px;
                }
            </style>

            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <strong class="control-label text-left">{{ __('common.identificacion') }}:</strong>
                </div>
                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                    <span class="control-label text-left">@{{vm.empresa.numero_documento}}</span>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <strong class="control-label text-left">{{ __('common.nombre_empresa') }}:</strong>
                </div>
                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                    <span class="control-label text-left">@{{vm.empresa.nombre}}</span>
                </div>
            </div>
            <hr>
            <br>
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <strong class="control-label text-left">{{ __('common.usuario') }}:</strong>
                </div>
                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                    <span class="control-label text-left">@{{vm.cliente.documento}} &nbsp;</span>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <strong class="control-label text-left">{{ __('common.nombre') }}:</strong>
                </div>
                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                    <span class="control-label text-left">@{{vm.cliente.nombre}} &nbsp;</span>
                </div>
            </div>
            <hr style="margin-bottom: 30px;">
            <div ng-class="['form-group label-floating', {'is-empty': !vm.procesoId, 'is-not-empty': vm.procesoId, 'with-error': vm.guardando && vm.form.proceso.$invalid}]" style="top: -7px; margin-bottom: 3px;">
                <label class="control-label">{{ __('common.proceso_') }}</label>
                <select selectize="vm.configProceso" options="vm.opcionesProcesos" total='2' disabled-typing="false" clear-options="vm.limpiarOpcionesProcesos" ng-model="vm.procesoId" name="proceso" class="form-control" required></select>
                <span class="help-block show" ng-show="vm.guardando && vm.form.proceso.$invalid">
                    <span ng-show="vm.form.proceso.$error.required">Requerido</span>
                </span>
                <span class="help-block show text-danger">@{{ vm.message }}</span>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                    <span><strong class="control-label text-left">{{ __('common.departamento') }}:</strong></span>
                </div>
                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                    <span class="control-label text-left">@{{vm.proceso.departamento.nombre}} &nbsp;</span>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                    <span><strong class="control-label text-left">{{ __('common.ciudad') }}:</strong></span>
                </div>
                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                    <span class="control-label text-left">@{{vm.proceso.ciudad.nombre}} &nbsp;</span>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                    <strong class="control-label text-left">{{ __('common.juzgado') }}:</strong>
                </div>
                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                    <span class="control-label text-left">@{{vm.proceso.juzgado.nombre}} &nbsp;</span>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                    <strong class="control-label text-left">{{ __('common.demandante') }}:</strong>
                </div>
                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                    <span class="control-label text-left">@{{vm.proceso.demandante}} &nbsp;</span>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                    <strong class="control-label text-left">{{ __('common.demandado') }}:</strong>
                </div>
                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                    <span class="control-label text-left">@{{vm.proceso.demandado}} &nbsp;</span>
                </div>
            </div>
            <hr>
            <br>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <strong style="margin-bottom: 0;">{{ __('common.estado') }}</strong>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <div class="radio">
                        <label>
                            <input type="radio" ng-model="vm.estado" ng-value="true">
                            {{ __('common.activo') }}
                        </label>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                    <div class="radio">
                        <label>
                            <input type="radio" ng-model="vm.estado" ng-value="false">
                            {{ __('common.inactivo') }}
                        </label>
                    </div>
                </div>
            </div>
            </div>
    </form>
</div>
<div class="modal-footer">
    <button class="btn btn-primary" type="button" ng-click="vm.guardar()" ng-disabled="vm.deshabilitarGuardado">{{ __('common.guardar') }}</button>
    <button class="btn btn-default" type="button" ng-click="vm.cancelar()">{{ __('common.cancelar') }}</button>
</div>
<script>$(document).ready(function(){$.material.init();});</script>
