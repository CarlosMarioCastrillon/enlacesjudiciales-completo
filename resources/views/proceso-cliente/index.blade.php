<div id="ProcesoCliente" class="row" st-table="vm.coleccion" st-pipe="vm.obtenerResumen" st-persist="resumenProcesoCliente" st-reset="isReset" refresh-table>
    <div class="card main-card" style="margin-bottom: 0;">
        <div class="card-content">
            <h4 class="card-title">@{{ vm.titulo }}</h4>
            <br>
            <div class="row">
                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                    <strong class="control-label text-left">{{ __('common.identificacion') }}:</strong>
                </div>
                <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                    <span class="control-label text-left">@{{vm.empresa.numero_documento}}</span>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                    <strong class="control-label text-left">{{ __('common.nombre_empresa') }}:</strong>
                </div>
                <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                    <span class="control-label text-left">@{{vm.empresa.nombre}}</span>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                    <strong class="control-label text-left">{{ __('common.usuario') }}:</strong>
                </div>
                <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                    <span class="control-label text-left">@{{vm.cliente.documento}} &nbsp;</span>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                    <strong class="control-label text-left">{{ __('common.nombre') }}:</strong>
                </div>
                <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                    <span class="control-label text-left">@{{vm.cliente.nombre}} &nbsp;</span>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                    <strong class="control-label text-left">{{ __('common.fecha_vencimiento') }}:</strong>
                </div>
                <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                    <span class="control-label text-left">@{{vm.cliente.fecha_vencimiento}} &nbsp;</span>
                </div>
            </div>
            @can('CrearProcesoCliente')
                <a class="btn btn-primary btn-round btn-fab nex-btn-add" title="Nuevo valor" ng-click="vm.crear()" style="top: 144px; right: 17px;">
                    <i class="material-icons">add</i>
                </a>
            @endcan
        </div>
        <div class="card-content">
            <hr style="margin-top: -15px; margin-bottom: 5px;">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="row" style="margin: 0;" ng-if="!vm.cargando && vm.coleccion.length > 0">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 hidden-xs text-left" style="padding: 0;">
                                <nav>
                                    <ul class="pagination pagination-primary">
                                        <li class="text-left">
                                            <a style="padding: 0 5px;" href="" type="button" rel="tooltip" title="{{ __('common.limpiar_filtros') }}" ng-click="vm.limpiarFiltros()">
                                                <i class="material-icons">clear_all</i>
                                            </a>
                                        </li>
                                        <li class="text-left">
                                <span style="text-transform: none; font-size: 14px;">
                                    @{{ 'Mostrando ' + vm.paginacion.desde + ' a ' + vm.paginacion.hasta + ' de ' + vm.paginacion.total + ' resultados - Página ' + vm.paginacion.paginaActual + ' de ' + vm.paginacion.ultimaPagina }}
                                </span>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding: 0;">
                                <div class="text-right" st-pagination="" st-items-by-page="vm.itemsPorPagina" st-displayed-pages="5" st-template="/paginacion-template" style="float: right;"></div>
                                <div class="text-right" style="float: right; margin: 26px 25px 26px 0;">
                                    <label style="color: rgba(0,0,0,0.68);">
                                        Filas:
                                        <select ng-options="option for option in [5, 10, 25, 50, 100, 500] track by option" ng-model="vm.itemsPorPagina" class="select-grid"></select>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-hover table-sm">
                                <thead>
                                <tr>
                                    <th class="text-right">{{ __('common.proceso_') }}</th>
                                    <th class="text-right">{{ __('common.consecutivo') }}</th>
                                    <th>{{ __('common.nombre') }} <br> {{ __('common.ciudad') }}</th>
                                    <th>{{ __('common.nombre') }} <br> {{ __('common.despacho') }}</th>
                                    <th>{{ __('common.demandante') }}</th>
                                    <th>{{ __('common.demandado') }}</th>
                                    <th class="text-center">{{ __('common.estado') }}</th>
                                    @if(auth()->user()->can('ModificarProcesoCliente') || auth()->user()->can('EliminarProcesoCliente'))
                                        <th class="text-right">{{ __('common.acciones') }}</th>
                                    @endif
                                </tr>
                                </thead>
                                <tbody ng-show="!vm.cargando">
                                <tr ng-repeat="row in vm.coleccion">
                                    <td class="text-right">@{{row.numero_proceso}}</td>
                                    <td class="text-right">@{{row.consecutivo}}</td>
                                    <td>@{{row.ciudad_nombre}}</td>
                                    <td>@{{row.despacho_nombre}}</td>
                                    <td>@{{row.demandantes}}</td>
                                    <td>@{{row.demandados}}</td>
                                    <td class="text-center">
                                        <span class="label label-success" ng-if="!!(+row.estado)">{{ __('common.activo') }}</span>
                                        <span class="label label-danger" ng-if="!(+row.estado)">{{ __('common.inactivo') }}</span>
                                    </td>
                                    @if(auth()->user()->can('ModificarProcesoCliente') || auth()->user()->can('EliminarProcesoCliente'))
                                        <td class="td-actions text-right">
                                            @can('ModificarProcesoCliente')
                                                <a type="button" rel="tooltip" class="btn btn-primary btn-simple" title="{{ __('common.modificar') }}" ng-click="vm.crear(row)">
                                                    <i class="material-icons">mode_edit</i>
                                                </a>
                                            @endcan
                                            @can('EliminarProcesoCliente')
                                                <a type="button" rel="tooltip" class="btn btn-danger btn-simple" title="{{ __('common.eliminar') }}" ng-click="vm.confirmarEliminar(row)">
                                                    <i class="material-icons">delete</i>
                                                </a>
                                            @endcan
                                        </td>
                                    @endif
                                </tr>
                                </tbody>
                                <tfoot ng-if="!vm.cargando && vm.coleccion.length > 0">
                                <script>$(document).ready(function(){$.material.ripples('table .btn');});</script>
                                </tfoot>
                            </table>
                        </div>
                        <div class="row" style="margin: 0;" ng-show="!vm.cargando && vm.coleccion.length == 0">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 0;">
                                <nav>
                                    <ul class="pagination pagination-primary">
                                        <li class="text-left">
                                            <a style="padding: 0 5px;" href="" type="button" rel="tooltip" title="{{ __('common.limpiar_filtro') }}" ng-click="vm.limpiarFiltros()">
                                                <i class="material-icons">clear_all</i>
                                            </a>
                                            <span style="text-transform: none; font-size: 14px;">{{ __('common.sin_resultados') }}</span>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                        <div class="row" style="margin: 0;" ng-if="!vm.cargando && vm.coleccion.length > 0">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 hidden-xs text-left" style="padding: 0;">
                                <nav>
                                    <ul class="pagination pagination-primary">
                                        <li class="text-left">
                                            <a style="padding: 0 5px;" href="" type="button" rel="tooltip" title="{{ __('common.limpiar_filtros') }}" ng-click="vm.limpiarFiltros()">
                                                <i class="material-icons">clear_all</i>
                                            </a>
                                        </li>
                                        <li class="text-left">
                                <span style="text-transform: none; font-size: 14px;">
                                    @{{ 'Mostrando ' + vm.paginacion.desde + ' a ' + vm.paginacion.hasta + ' de ' + vm.paginacion.total + ' resultados - Página ' + vm.paginacion.paginaActual + ' de ' + vm.paginacion.ultimaPagina }}
                                </span>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding: 0;">
                                <div class="text-right" st-pagination="" st-items-by-page="vm.itemsPorPagina" st-displayed-pages="5" st-template="/paginacion-template" style="float: right;"></div>
                                <div class="text-right" style="float: right; margin: 26px 25px 26px 0;">
                                    <label style="color: rgba(0,0,0,0.68);">
                                        Filas:
                                        <select ng-options="option for option in [5, 10, 25, 50, 100, 500] track by option" ng-model="vm.itemsPorPagina" class="select-grid"></select>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
     <script>$(document).ready(function(){$.material.init();});</script>
 </div>
