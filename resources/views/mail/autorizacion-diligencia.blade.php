<!doctype html>
    <html lang="es">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
            <title>Autorización diligencia</title>
        </head>
    <body>
        <p>Respetado (a)</p>
        <p>{{ $diligencia['empresa']['nombre'] ?? '' }}</p>
        <br>
        <p>
            De acuerdo a su solicitud para realizar la diligencia {{ $diligencia['tipo_diligencia']['nombre'] ?? '' }} para el
            proceso {{  $diligencia['proceso_judicial']['numero_proceso'] ?? '' }} correspondiente a:
        </p>
        <table>
            <tr>
                <td>Demandante:</td>
                <td>{{  $diligencia['proceso_judicial']['demandante'] ?? '' }}</td>
            </tr>
            <tr>
                <td>Demandado:</td>
                <td>{{  $diligencia['proceso_judicial']['demandado'] ?? '' }}</td>
            </tr>
            <tr>
                <td>Ciudad:</td>
                <td>{{  $diligencia['ciudad']['nombre'] ?? '' }}</td>
            </tr>
            <tr>
                <td>Juzgado:</td>
                <td>{{  $diligencia['proceso_judicial']['juzgado'] ?? '' }}</td>
            </tr>
        </table>
        <p>
            Le informamos que los valores estimados para esta diligencia en el momento de enviar este correo de  cotización son:
        </p>
        <table>
            <tr>
                <td>Valor diligencia:</td>
                <td>${{ number_format($diligencia['valor_diligencia'] ?? 0, 0, '.', ',') }}</td>
            </tr>
            <tr>
                <td>Gastos envío:</td>
                <td>${{ number_format($diligencia['gastos_envio'] ?? 0, 0, '.', ',') }}</td>
            </tr>
            <tr>
                <td>Otros gastos:</td>
                <td>${{ number_format($diligencia['otros_gastos'] ?? 0, 0, '.', ',') }}</td>
            </tr>
            <tr>
                <td>Valor total:</td>
                <td>${{ number_format(($diligencia['valor_diligencia'] ?? 0) + ($diligencia['gastos_envio'] ?? 0) + ($diligencia['otros_gastos'] ?? 0), 0, '.', ',') }}</td>
            </tr>
        </table>
        <br>
        <p>
            <strong>Nota: Si se llegaran a presentar gastos adicionales se le informará en su momento.</strong>
        </p>
        <p>
            Favor responder este correo indicando la aprobación o rechazo de esta cotización
        </p>
        <br>
        <p>
            Quedamos atentos a su respuesta.
        </p>
        <br>
        <p>Gracias.</p>
    </body>
</html>
