<div style="text-align: justify;">
    {!! __('common.correo_para_establecer_de_password', ['enlace' => '<a href="' . env('APP_URL') . '">' . env('APP_NAME') . '</a>']) !!}<br><br>
    {{ __('common.por_favor_vaya_pagina_nuevo_password') }}: <br><br>
    <a href="{{ env('APP_URL') . '/restablecer-password/' . $dto['token'] }}">{{ env('APP_URL') . '/restablecer-password/' . $dto['token'] }}</a><br><br>
</div>
