<!DOCTYPE html>
<html lang="en" ng-app="enlaces.login" style="height: 100%;">
    <head>
        <meta charset="utf-8">
        <meta name="theme-color" content="#D0AD4C">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />

        <title>{{ env('APP_NAME') }}</title>
        <link rel="shortcut icon" href="/favicon.ico" />

        <!-- Styles -->
        <link rel="stylesheet" href="{{ mix('/css/login-' . env('APP_THEME', 'a') . '.css') }}" type='text/css' media="all">

        <!--  Fonts and icons  -->
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons"/>

        <style>
            button {
                width: 100%;
            }
            img {
                margin-left: -25px;
            }
            @media (min-width: 768px) {
                .login-container {
                    margin-top: 70px;
                }
            }
            #loading-bar .bar {
                background: red;
                height: 2px;
            }
            #loading-bar-spinner .spinner-icon {
                border: solid 2px transparent;
                border-top-color: red;
                border-left-color: red;
            }
            .box {
                display: flex;
                height: 100%;
            }
            .col-md-7 {
                display: inline;
                padding-left: 15px;
                padding-right: 15px;
                width: 100%;
                max-width: 700px;
                margin-right: auto;
                margin-left: auto;
                margin-top: auto;
                margin-bottom: auto;
            }

        </style>
    </head>
    <body ng-controller="loginController as vm" class="box">
        <div class="col-md-7" >
            <div class="container-fluid">
                <div class="row login-container align-middle" style="margin-top: 0;">
                    <div class="col-xs-12 col-sm-8 col-md-7 col-lg-5 col-sm-offset-2 col-md-offset-2 col-lg-offset-3">
                        <div class="card">
                            <div class="card-header text-center" >
                                <img src="/image/{{ env('APP_LOGO') }}" alt="Marca">
                            </div>
                            <div class="card-content">
                                <form name="vm.form" autocomplete="off" novalidate>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div ng-class="['form-group label-floating', {'is-empty': !vm.username, 'with-error': vm.submitted && vm.form.username.$invalid}]">
                                                <label class="control-label">Identificación</label>
                                                <input type="text" class="form-control" ng-model="vm.username" name="username" required maxlength="255">
                                                <span class="help-block show" ng-show="vm.submitted && vm.form.username.$invalid">
                                                    <span ng-show="vm.form.username.$error.required">Requerido</span>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div ng-class="['form-group label-floating', {'is-empty': !vm.password, 'with-error': vm.submitted && vm.form.password.$invalid}]">
                                                <label class="control-label">Contraseña</label>
                                                <input type="password" class="form-control" ng-model="vm.password" name="password" required maxlength="255">
                                                <span class="help-block show" ng-show="vm.submitted && vm.form.password.$invalid">
                                                    <span ng-show="vm.form.password.$error.required">Requerido</span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right">
                                            <button type="submit" class="btn btn-primary btn-fill" ng-click="vm.login()">Iniciar sesión</button>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right">
                                            <a href="/restablecer-password">Olvidé mi contraseña</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="hidden-xs hidden-sm col-md-5 col-lg-5 text-center one" style="background-color: red;">
                <img src="/image/enlaces2.png" alt="Imagen de fondo">
        </div>
        <script src={{ mix('/js/login.js') }}></script>
    </body>
</html>
<script>$(document).ready(function(){$.material.init();});</script>
