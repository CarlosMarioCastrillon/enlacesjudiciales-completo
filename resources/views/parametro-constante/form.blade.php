<div class="modal-header">
    <h4 class="card-title">{{ __('common.datos_parametros_constantes') }}</h4>
</div>
<div class="modal-body" style="padding-top: 0; padding-bottom: 0px;">
    <form name="vm.form" autocomplete="off" novalidate>
        <div class="row" style="margin-bottom: -4px;">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.codigo, 'is-not-empty': vm.codigo, 'with-error': vm.guardando && vm.form.codigo.$invalid}]">
                    <label class="control-label" style="margin-top: 12px;">{{ __('common.codigo_parametro') }}</label>
                    <input ng-model="vm.codigo" name="codigo" required maxlength="191" class="form-control" type="text">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.codigo.$invalid">
                            <span ng-show="vm.form.codigo.$error.required">Requerido</span>
                        </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.descripcion, 'is-not-empty': vm.descripcion, 'with-error': vm.guardando && vm.form.descripcion.$invalid}]">
                    <label class="control-label">{{ __('common.descripcion') }}</label>
                    <input ng-model="vm.descripcion" name="descripcion" required maxlength="191" class="form-control" type="text">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.descripcion.$invalid">
                    <span ng-show="vm.form.descripcion.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.valor, 'is-not-empty': vm.valor, 'with-error': vm.guardando && vm.form.valor.$invalid}]">
                    <label class="control-label">{{ __('common.valor_parametro') }}</label>
                    <input ng-model="vm.valor" name="valor" maxlength="191" class="form-control" type="text">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.valor.$invalid">
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <p style="margin-bottom: 0;">{{ __('common.estado') }}</p>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="vm.estado" ng-value="true">
                                {{ __('common.activo') }}
                            </label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="vm.estado" ng-value="false">
                                {{ __('common.inactivo') }}
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button class="btn btn-primary" type="button" ng-click="vm.guardar()" ng-disabled="vm.deshabilitarGuardado">{{ __('common.guardar') }}</button>
    <button class="btn btn-default" type="button" ng-click="vm.cancelar()">{{ __('common.cancelar') }}</button>
</div>
<script>$(document).ready(function(){$.material.init();});</script>


