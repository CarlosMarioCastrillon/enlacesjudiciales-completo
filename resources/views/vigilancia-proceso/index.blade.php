<div id="vigilanciaProceso" class="row" st-table="vm.coleccion" st-pipe="vm.obtenerResumen" st-persist="resumenVigilanciaProceso" st-reset="isReset" refresh-table>
    <div class="card main-card" style="margin-bottom: 0;">
        <div class="card-content">
            <h4 class="card-title">@{{ vm.titulo }}</h4>
            @can('ListarVigilanciaProceso')
                <button class="btn btn-success btn-fab nex-btn-add" title="Exportar a excel" ng-click="vm.obtenerExcel()">
                    <i class="mdi mdi-file-excel"></i>
                </button>
                <button  class="btn btn-default btn-fab nex-btn-add" style="top: 47px;" type="button" rel="tooltip" title="{{ __('common.limpiar_filtros') }}" ng-click="vm.limpiarFiltros()">
                    <i class="material-icons">clear_all</i>
                </button>
                <button class="btn btn-primary btn-fab nex-btn-add" title="{{ __('common.buscar') }}" ng-click="vm.buscar()" ng-disabled="vm.deshabilitarBuscado" style="top: 89px;">
                    <i class="material-icons">search</i>
                </button>
            @endcan
            <form name="vm.form" autocomplete="off" novalidate>
                <div class="row">
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" style="left: 22px;">
                        <div class="row" style="margin-left: -41px; margin-right: 58px;">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <label class="control-label" style="margin-top: 11px; margin-left: 6px; margin-bottom: 0px;">{{ __('common.estado') }}</label>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="radio">
                                    <label>
                                        <input type="radio" st-search="estado" ng-model="vm.filtroEstado" ng-value="1">
                                        {{ __('common.activo') }}
                                    </label>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="radio">
                                    <label>
                                        <input type="radio" st-search="estado" ng-model="vm.filtroEstado" ng-value="2">
                                        {{ __('common.inactivo') }}
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9" style="padding-left: 0px; left: -34px; padding-right: 38px;">
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="form-group label-floating">
                                <label class="control-label">{{ __('common.proceso_') }}</label>
                                <input st-search="numero_proceso" ng-model="vm.filtroProceso" class="form-control" type="text"/>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="form-group label-floating">
                                <label class="control-label">{{ __('common.demandante') }}</label>
                                <input st-search="demandante" ng-model="vm.filtroDemandante" class="form-control" type="text"/>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="form-group label-floating">
                                <label class="control-label">{{ __('common.demandado') }}</label>
                                <input st-search="demandado" ng-model="vm.filtroDemandado" class="form-control" type="text"/>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <hr>
            <div class="row" style="margin: 0;" ng-if="!vm.cargando && vm.coleccion.length > 0">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 hidden-xs text-left" style="padding: 0;">
                    <nav>
                        <ul class="pagination pagination-primary">
                            <li class="text-left">
                                <a style="padding: 0 5px;" href="" type="button" rel="tooltip" title="{{ __('common.limpiar_filtros') }}" ng-click="vm.limpiarFiltros()">
                                    <i class="material-icons">clear_all</i>
                                </a>
                            </li>
                            <li class="text-left">
                                <span style="text-transform: none; font-size: 14px;">
                                    @{{ 'Mostrando ' + vm.paginacion.desde + ' a ' + vm.paginacion.hasta + ' de ' + vm.paginacion.total + ' resultados - Página ' + vm.paginacion.paginaActual + ' de ' + vm.paginacion.ultimaPagina }}
                                </span>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding: 0;">
                    <div class="text-right" st-pagination="" st-items-by-page="vm.itemsPorPagina" st-displayed-pages="5" st-template="/paginacion-template" style="float: right;"></div>
                    <div class="text-right" style="float: right; margin: 26px 25px 26px 0;">
                        <label style="color: rgba(0,0,0,0.68);">
                            Filas:
                            <select ng-options="option for option in [5, 10, 25, 50, 100, 500] track by option" ng-model="vm.itemsPorPagina" class="select-grid"></select>
                        </label>
                    </div>
                </div>
            </div>
            <div class="table-responsive" style="font-size: 12px;">
                <table class="table table-hover table-sm table-ellipsis">
                    <thead>
                        <tr>
                            <th st-multi-sort="nombre_juzgado">{{ __('common.nombre') }}<br>{{ __('common.juzgado') }}</th>
                            <th st-multi-sort="juzgado_actual" style="width: 80px;">{{ __('common.juzgado') }}<br>{{ __('common.actual') }}</th>
                            <th st-multi-sort="nombre_ciudad">{{ __('common.nombre') }}<br>{{ __('common.ciudad') }}</th>
                            <th st-multi-sort="proceso" style="min-width: 159px">{{ __('common.proceso_') }}</th>
                            <th st-multi-sort="consecutivo">{{ __('common.consecutivo') }}</th>
                            <th st-multi-sort="nombre_despacho">{{ __('common.nombre') }}<br>{{ __('common.despacho') }}</th>
                            <th st-multi-sort="demandante" style="width: 140px;">{{ __('common.demandante') }}</th>
                            <th st-multi-sort="demandado" style="width: 140px;">{{ __('common.demandado') }}</th>
                            <th st-multi-sort="observacion">{{ __('common.observacion') }}</th>
                            <th st-multi-sort="fecha_creacion">{{ __('common.fecha_creacion') }}</th>
                            <th st-multi-sort="ultima_actualizacion">{{ __('common.ultima_actualizacion') }}</th>
                        </tr>
                    </thead>
                    <thead ng-show="vm.cargando">
                        <tr>
                            <td colspan="11" class="text-center" style="padding: 0px;">
                                <div class="progress progress-striped active" style="margin: 0px;">
                                    <div class="progress-bar" style="width: 100%"></div>
                                </div>
                            </td>
                        </tr>
                    </thead>
                    <tbody ng-show="!vm.cargando">
                        <tr ng-repeat="row in vm.coleccion">
                            <td>@{{row.juzgado_nombre}}</td>
                            <td>@{{row.juzgado_actual}}</td>
                            <td>@{{row.ciudad_nombre}}</td>
                            <td>@{{row.numero_proceso}}</td>
                            <td class="text-right">@{{row.numero_consecutivo}}</td>
                            <td>@{{row.despacho_nombre}}</td>
                            <td>@{{row.nombres_demandantes}}</td>
                            <td>@{{row.nombres_demandados}}</td>
                            <td>@{{row.observaciones}}</td>
                            <td>@{{row.fecha_creacion}}</td>
                            <td>@{{row.fecha_modificacion}}</td>
                        </tr>
                    </tbody>
                    <tfoot ng-if="!vm.cargando && vm.coleccion.length > 0">
                        <script>$(document).ready(function(){$.material.ripples('table .btn');});</script>
                    </tfoot>
                </table>
            </div>
            <div class="row" style="margin: 0;" ng-show="!vm.cargando && vm.coleccion.length == 0">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 0;">
                    <nav>
                        <ul class="pagination pagination-primary">
                            <li class="text-left">
                                <a style="padding: 0 5px;" href="" type="button" rel="tooltip" title="{{ __('common.limpiar_filtro') }}" ng-click="vm.limpiarFiltros()">
                                    <i class="material-icons">clear_all</i>
                                </a>
                                <span style="text-transform: none; font-size: 14px;">{{ __('common.sin_resultados') }}</span>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="row" style="margin: 0;" ng-if="!vm.cargando && vm.coleccion.length > 0">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 hidden-xs text-left" style="padding: 0;">
                    <nav>
                        <ul class="pagination pagination-primary">
                            <li class="text-left">
                                <a style="padding: 0 5px;" href="" type="button" rel="tooltip" title="{{ __('common.limpiar_filtros') }}" ng-click="vm.limpiarFiltros()">
                                    <i class="material-icons">clear_all</i>
                                </a>
                            </li>
                            <li class="text-left">
                                <span style="text-transform: none; font-size: 14px;">
                                    @{{ 'Mostrando ' + vm.paginacion.desde + ' a ' + vm.paginacion.hasta + ' de ' + vm.paginacion.total + ' resultados - Página ' + vm.paginacion.paginaActual + ' de ' + vm.paginacion.ultimaPagina }}
                                </span>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding: 0;">
                    <div class="text-right" st-pagination="" st-items-by-page="vm.itemsPorPagina" st-displayed-pages="5" st-template="/paginacion-template" style="float: right;"></div>
                    <div class="text-right" style="float: right; margin: 26px 25px 26px 0;">
                        <label style="color: rgba(0,0,0,0.68);">
                            Filas:
                            <select ng-options="option for option in [5, 10, 25, 50, 100, 500] track by option" ng-model="vm.itemsPorPagina" class="select-grid"></select>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>$(document).ready(function(){$.material.init();});</script>
</div>
