<div class="modal-header">
    <h4 class="card-title">{{ __('common.proceso_valor') }}</h4>
</div>
<div class="modal-body">
    <form name="vm.form" autocomplete="off" novalidate>
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <span><strong class="control-label text-left">{{ __('common.proceso_') }}:</strong></span>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <strong class="control-label text-left">{{ __('common.ciudad') }}:</strong>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <strong class="control-label text-left">{{ __('common.juzgado_actual') }}:</strong>
                </div>
            </div>
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <span class="control-label text-left">@{{vm.proceso.numero_proceso}}</span>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <span class="control-label text-left">@{{vm.proceso.ciudad.nombre}}</span>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <span class="control-label text-left">@{{vm.proceso.juzgado_actual}}</span>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"5>
                <form>
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="vm.tipoConcepto" value="D" ng-change="vm.cargarConceptos()">
                                @{{ 'COSTOS_INTERESES_DEMANDA' | translate }}
                            </label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="vm.tipoConcepto" value="G" ng-change="vm.cargarConceptos()">
                                @{{ 'HONORARIOS_EXTRAS' | translate }}
                            </label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="vm.tipoConcepto" value="C" ng-change="vm.cargarConceptos()">
                                @{{ 'COSTOS_DE_PROCESOS' | translate }}
                            </label>
                        </div>
                    </div>
                </div>
                </form>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.conceptoId, 'is-not-empty': vm.conceptoId, 'with-error': vm.guardando && vm.form.concepto.$invalid}]" style="top: -10px;">
                    <label class="control-label">{{ __('common.concepto') }}</label>
                    <select selectize="vm.configConcepto" options="vm.opcionesConceptos" disabled-typing="true" clear-options="vm.limpiarOpcionesConceptos" ng-model="vm.conceptoId" name="concepto" class="form-control" required></select>
                    <span class="help-block show" ng-show="vm.guardando && vm.form.concepto.$invalid">
                        <span ng-show="vm.form.concepto.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="input-group">
                    <div ng-class="['form-group label-floating', {'is-empty': !vm.fecha, 'is-not-empty': vm.fecha, 'with-error': vm.guardando && vm.form.fecha.$invalid}]">
                        <label class="control-label">Fecha</label>
                        <input datetimepicker datetimepicker-options="{useCurrent: false, format: 'YYYY-MM-DD'}" ng-model="vm.fecha" name="fecha" class="form-control" required type="text"/>
                        <i class="input-group-addon fa fa-calendar en-modal"></i>
                        <span class="help-block show" ng-show="vm.guardando && vm.form.fecha.$invalid">
                            <span ng-show="vm.form.fecha.$error.required">Requerido</span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.valor, 'is-not-empty': vm.valor, 'with-error': vm.guardando && vm.form.valor.$invalid}]"  style="padding-bottom: 0;">
                    <label class="control-label">{{ __('common.valor') }}</label>
                    <input ng-model="vm.valor" name="valor" maxlength="191"  ng-pattern="/^[0-9]{0,100}$/" class="form-control" required type="text">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.valor.$invalid">
                            <span ng-show="vm.form.valor.$error.pattern">Debe ingresar un valor numérica<</span>
                    </span>
                    <br>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div  ng-class="['form-group label-floating', {'is-empty': !vm.observacion, 'is-not-empty': vm.observacion}]" style="top: -10px;">
                    <label class="control-label">{{ __('common.observacion') }}</label>
                    <textarea ng-model="vm.observacion" name="observacion" class="form-control" type="text" maxlength="191" rows="1"></textarea>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <p style="margin-bottom: 0;">{{ __('common.estado') }}</p>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="vm.estado" ng-value="true">
                                {{ __('common.activo') }}
                            </label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="vm.estado" ng-value="false">
                                {{ __('common.inactivo') }}
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button class="btn btn-primary" type="button" ng-click="vm.guardar()" ng-disabled="vm.deshabilitarGuardado">{{ __('common.guardar') }}</button>
    <button class="btn btn-default" type="button" ng-click="vm.cancelar()">{{ __('common.cancelar') }}</button>
</div>
<script>$(document).ready(function(){$.material.init();});</script>
