<div id="ProcesoValor" class="row" st-table="vm.coleccion" st-pipe="vm.obtenerResumen" st-persist="resumenProcesoValor" st-reset="isReset" refresh-table>
    <div class="card main-card" style="margin-bottom: 0;">
        <div class="card-content">
            <h4 class="card-title">@{{ vm.titulo }}</h4>
            <br>
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <span><strong class="control-label text-left">{{ __('common.proceso_') }}:</strong></span>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <strong class="control-label text-left">{{ __('common.ciudad') }}:</strong>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <strong class="control-label text-left">{{ __('common.juzgado_actual') }}:</strong>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <strong class="control-label text-left">{{ __('common.demandante') }}:</strong>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <strong class="control-label text-left">{{ __('common.demandado') }}:</strong>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <span class="control-label text-left">@{{vm.proceso.numero_proceso}} &nbsp;</span>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <span class="control-label text-left">@{{vm.proceso.ciudad.nombre}} &nbsp;</span>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <span class="control-label text-left">@{{vm.proceso.juzgado_actual}} &nbsp;</span>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <span class="control-label text-left">@{{vm.proceso.demandante}} &nbsp;</span>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <span class="control-label text-left">@{{vm.proceso.demandado}} &nbsp;</span>
                    </div>
                </div>
            </div>
            @can('CrearValorProceso')
                <a class="btn btn-primary btn-round btn-fab nex-btn-add" title="Nuevo valor" ng-click="vm.crear()" style="top: 166px; right: 21px;">
                    <i class="material-icons">add</i>
                </a>
            @endcan
        </div>
        <div ng-repeat="procesosPorTipoConcepto in vm.coleccion" ng-if="procesosPorTipoConcepto.tipo_concepto == 'D'">
                <strong><hr></strong>
                <div class="card-header">
                        <h4 class="card-title">
                            <span>@{{ 'COSTOS_INTERESES_DEMANDA' | translate }}</span>
                        </h4>
                    </div>
                <div class="card-content">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="table-responsive">
                                    <table class="table table-hover table-sm">
                                        <thead>
                                        <tr>
                                            <th>{{ __('common.concepto') }}</th>
                                            <th>{{ __('common.fecha') }}</th>
                                            <th>{{ __('common.observacion') }}</th>
                                            <th class="text-center">{{ __('common.estado') }}</th>
                                            <th class="text-right">{{ __('common.credito_') }}</th>
                                            <th class="text-right">{{ __('common.debito_') }}</th>
                                            <th class="text-right">{{ __('common.saldo') }}</th>
                                            @if(auth()->user()->can('ModificarValorProceso') || auth()->user()->can('EliminarValorProceso'))
                                                <th class="text-right">{{ __('common.acciones') }}</th>
                                            @endif
                                        </tr>
                                        </thead>
                                        <tbody ng-show="!vm.cargando">
                                        <tr ng-repeat="procesoValor in procesosPorTipoConcepto.procesos_valor">
                                            <td>@{{procesoValor.concepto.nombre}}</td>
                                            <td>@{{procesoValor.fecha}}</td>
                                            <td>@{{procesoValor.observacion}}</td>
                                            <td class="text-center">
                                                <span class="label label-success" ng-if="!!(+procesoValor.estado)">{{ __('common.activo') }}</span>
                                                <span class="label label-danger" ng-if="!(+procesoValor.estado)">{{ __('common.inactivo') }}</span>
                                            </td>
                                            <td class="currency text-right">
                                                <span ng-if="!!(procesoValor.concepto.clase_concepto == 'CR')">@{{procesoValor.valor|currency}}</span>
                                                <span ng-if="!(procesoValor.concepto.clase_concepto == 'CR')">0</span>
                                            </td>
                                            <td class="currency text-right">
                                                <span ng-if="!!(procesoValor.concepto.clase_concepto == 'DB')">@{{procesoValor.valor|currency}}</span>
                                                <span ng-if="!(procesoValor.concepto.clase_concepto == 'DB')">0</span>
                                            </td>
                                            <td class="currency text-right">@{{procesoValor.valor|currency}}</td>
                                            @if(auth()->user()->can('ModificarValorProceso') || auth()->user()->can('EliminarValorProceso'))
                                                <td class="td-actions text-right">
                                                    @can('ModificarValorProceso')
                                                        <a type="button" rel="tooltip" class="btn btn-primary btn-simple" title="{{ __('common.modificar') }}" ng-click="vm.crear(procesoValor)">
                                                            <i class="material-icons">mode_edit</i>
                                                        </a>
                                                    @endcan
                                                    @can('EliminarValorProceso')
                                                        <a type="button" rel="tooltip" class="btn btn-danger btn-simple" title="{{ __('common.eliminar') }}" ng-click="vm.confirmarEliminar(procesoValor)">
                                                            <i class="material-icons">delete</i>
                                                        </a>
                                                    @endcan
                                                </td>
                                            @endif
                                        </tr>
                                        </tbody>
                                        <tfoot ng-if="!vm.cargando && vm.coleccion.length > 0">
                                        <tr>
                                            <td colspan="4">
                                                <b>Total</b>
                                            </td>
                                            <td class="text-right">
                                                <b>@{{ procesosPorTipoConcepto.total_credito|currency }}</b>
                                            </td>
                                            <td class="text-right">
                                                <b>@{{ procesosPorTipoConcepto.total_debito|currency }}</b>
                                            </td>
                                            <td class="text-right">
                                                <b>@{{ procesosPorTipoConcepto.saldo|currency }}</b>
                                            </td>
                                            <td></td>
                                        </tr>
                                        </tfoot>
                                        <tfoot ng-if="!vm.cargando && vm.coleccion.length > 0">
                                        <script>$(document).ready(function(){$.material.ripples('table .btn');});</script>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                <hr>
            </div>
        <div ng-repeat="procesosPorTipoConcepto in vm.coleccion" ng-if="procesosPorTipoConcepto.tipo_concepto == 'G'">
                <strong><hr></strong>
                <div class="card-header">
                        <h4 class="card-title">
                            <span>@{{ 'HONORARIOS_EXTRAS' | translate }}</span>
                        </h4>
                    </div>
                <div class="card-content">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="table-responsive">
                                    <table class="table table-hover table-sm">
                                        <thead>
                                        <tr>
                                            <th>{{ __('common.concepto') }}</th>
                                            <th>{{ __('common.fecha') }}</th>
                                            <th>{{ __('common.observacion') }}</th>
                                            <th class="text-center">{{ __('common.estado') }}</th>
                                            <th class="text-right">{{ __('common.credito_') }}</th>
                                            <th class="text-right">{{ __('common.debito_') }}</th>
                                            <th class="text-right">{{ __('common.saldo') }}</th>
                                            @if(auth()->user()->can('ModificarValorProceso') || auth()->user()->can('EliminarValorProceso'))
                                                <th class="text-right">{{ __('common.acciones') }}</th>
                                            @endif
                                        </tr>
                                        </thead>
                                        <tbody ng-show="!vm.cargando">
                                        <tr ng-repeat="procesoValor in procesosPorTipoConcepto.procesos_valor">
                                            <td>@{{procesoValor.concepto.nombre}}</td>
                                            <td>@{{procesoValor.fecha}}</td>
                                            <td>@{{procesoValor.observacion}}</td>
                                            <td class="text-center">
                                                <span class="label label-success" ng-if="!!(+procesoValor.estado)">{{ __('common.activo') }}</span>
                                                <span class="label label-danger" ng-if="!(+procesoValor.estado)">{{ __('common.inactivo') }}</span>
                                            </td>
                                            <td class="currency text-right">
                                                <span ng-if="!!(procesoValor.concepto.clase_concepto == 'CR')">@{{procesoValor.valor|currency}}</span>
                                                <span ng-if="!(procesoValor.concepto.clase_concepto == 'CR')">0</span>
                                            </td>
                                            <td class="currency text-right">
                                                <span ng-if="!!(procesoValor.concepto.clase_concepto == 'DB')">@{{procesoValor.valor|currency}}</span>
                                                <span ng-if="!(procesoValor.concepto.clase_concepto == 'DB')">0</span>
                                            </td>
                                            <td class="currency text-right">@{{procesoValor.valor|currency}}</td>
                                            @if(auth()->user()->can('ModificarValorProceso') || auth()->user()->can('EliminarValorProceso'))
                                                <td class="td-actions text-right">
                                                    @can('ModificarValorProceso')
                                                        <a type="button" rel="tooltip" class="btn btn-primary btn-simple" title="{{ __('common.modificar') }}" ng-click="vm.crear(procesoValor)">
                                                            <i class="material-icons">mode_edit</i>
                                                        </a>
                                                    @endcan
                                                    @can('EliminarValorProceso')
                                                        <a type="button" rel="tooltip" class="btn btn-danger btn-simple" title="{{ __('common.eliminar') }}" ng-click="vm.confirmarEliminar(procesoValor)">
                                                            <i class="material-icons">delete</i>
                                                        </a>
                                                    @endcan
                                                </td>
                                            @endif
                                        </tr>
                                        </tbody>
                                        <tfoot ng-if="!vm.cargando && vm.coleccion.length > 0">
                                        <tr>
                                            <td colspan="4">
                                                <b>Total</b>
                                            </td>
                                            <td class="text-right">
                                                <b>@{{ procesosPorTipoConcepto.total_credito|currency }}</b>
                                            </td>
                                            <td class="text-right">
                                                <b>@{{ procesosPorTipoConcepto.total_debito|currency }}</b>
                                            </td>
                                            <td class="text-right">
                                                <b>@{{ procesosPorTipoConcepto.saldo|currency }}</b>
                                            </td>
                                            <td></td>
                                        </tr>
                                        </tfoot>
                                        <tfoot ng-if="!vm.cargando && vm.coleccion.length > 0">
                                        <script>$(document).ready(function(){$.material.ripples('table .btn');});</script>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                <hr>
            </div>
        <div ng-repeat="procesosPorTipoConcepto in vm.coleccion" ng-if="procesosPorTipoConcepto.tipo_concepto == 'C'" >
                <strong><hr></strong>
                <div class="card-header">
                        <h4 class="card-title">
                            <span>@{{ 'COSTOS_DE_PROCESOS' | translate }}</span>
                        </h4>
                    </div>
                <div class="card-content">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="table-responsive">
                                    <table class="table table-hover table-sm">
                                        <thead>
                                        <tr>
                                            <th>{{ __('common.concepto') }}</th>
                                            <th>{{ __('common.fecha') }}</th>
                                            <th>{{ __('common.observacion') }}</th>
                                            <th class="text-center">{{ __('common.estado') }}</th>
                                            <th class="text-right">{{ __('common.credito_') }}</th>
                                            <th class="text-right">{{ __('common.debito_') }}</th>
                                            <th class="text-right">{{ __('common.saldo') }}</th>
                                            @if(auth()->user()->can('ModificarValorProceso') || auth()->user()->can('EliminarValorProceso'))
                                                <th class="text-right">{{ __('common.acciones') }}</th>
                                            @endif
                                        </tr>
                                        </thead>
                                        <tbody ng-show="!vm.cargando">
                                        <tr ng-repeat="procesoValor in procesosPorTipoConcepto.procesos_valor">
                                            <td>@{{procesoValor.concepto.nombre}}</td>
                                            <td>@{{procesoValor.fecha}}</td>
                                            <td>@{{procesoValor.observacion}}</td>
                                            <td class="text-center">
                                                <span class="label label-success" ng-if="!!(+procesoValor.estado)">{{ __('common.activo') }}</span>
                                                <span class="label label-danger" ng-if="!(+procesoValor.estado)">{{ __('common.inactivo') }}</span>
                                            </td>
                                            <td class="currency text-right">
                                                <span ng-if="!!(procesoValor.concepto.clase_concepto == 'CR')">@{{procesoValor.valor|currency}}</span>
                                                <span ng-if="!(procesoValor.concepto.clase_concepto == 'CR')">0</span>
                                            </td>
                                            <td class="currency text-right">
                                                <span ng-if="!!(procesoValor.concepto.clase_concepto == 'DB')">@{{procesoValor.valor|currency}}</span>
                                                <span ng-if="!(procesoValor.concepto.clase_concepto == 'DB')">0</span>
                                            </td>
                                            <td class="currency text-right">@{{procesoValor.valor|currency}}</td>
                                            @if(auth()->user()->can('ModificarValorProceso') || auth()->user()->can('EliminarValorProceso'))
                                                <td class="td-actions text-right">
                                                    @can('ModificarValorProceso')
                                                        <a type="button" rel="tooltip" class="btn btn-primary btn-simple" title="{{ __('common.modificar') }}" ng-click="vm.crear(procesoValor)">
                                                            <i class="material-icons">mode_edit</i>
                                                        </a>
                                                    @endcan
                                                    @can('EliminarValorProceso')
                                                        <a type="button" rel="tooltip" class="btn btn-danger btn-simple" title="{{ __('common.eliminar') }}" ng-click="vm.confirmarEliminar(procesoValor)">
                                                            <i class="material-icons">delete</i>
                                                        </a>
                                                    @endcan
                                                </td>
                                            @endif
                                        </tr>
                                        </tbody>
                                        <tfoot ng-if="!vm.cargando && vm.coleccion.length > 0">
                                        <tr>
                                            <td colspan="4">
                                                <b>Total</b>
                                            </td>
                                            <td class="text-right">
                                                <b>@{{ procesosPorTipoConcepto.total_credito|currency }}</b>
                                            </td>
                                            <td class="text-right">
                                                <b>@{{ procesosPorTipoConcepto.total_debito|currency }}</b>
                                            </td>
                                            <td class="text-right">
                                                <b>@{{ procesosPorTipoConcepto.saldo|currency }}</b>
                                            </td>
                                            <td></td>
                                        </tr>
                                        </tfoot>
                                        <tfoot ng-if="!vm.cargando && vm.coleccion.length > 0">
                                        <script>$(document).ready(function(){$.material.ripples('table .btn');});</script>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                <hr>
            </div>
    </div>
     <script>$(document).ready(function(){$.material.init();});</script>
 </div>
