<div id="proceso-area" class="row" st-table="vm.coleccion" st-pipe="vm.obtenerResumen" st-persist="resumenProcesoArea" st-reset="isReset" refresh-table>
    <div class="card main-card" style="margin-bottom: 0;">
        <div class="card-content">
            <h4 class="card-title">@{{ vm.titulo }}</h4>
            <button class="btn btn-success btn-fab nex-btn-add" title="Exportar a excel" ng-click="vm.obtenerExcel()">
                <i class="mdi mdi-file-excel"></i>
            </button>
            <br>
            <form name="vm.form" autocomplete="off" novalidate>
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <strong class="control-label text-left">{{ __('common.identificacion') }}:</strong>
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <span class="control-label text-left">@{{vm.empresa.numero_documento}}</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <strong class="control-label text-left">{{ __('common.nombre_empresa') }}:</strong>
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <span class="control-label text-left">@{{vm.empresa.nombre}}</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <strong class="control-label text-left">{{ __('common.departamento') }}:</strong>
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <span class="control-label text-left">@{{vm.empresa.departamento.nombre}}</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <strong class="control-label text-left">{{ __('common.ciudad') }}:</strong>
                    </div>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <span class="control-label text-left">@{{vm.empresa.ciudad.nombre}}</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div ng-class="['form-group label-floating', {'is-empty': !vm.filtroArea, 'is-not-empty': vm.filtroArea, 'with-error': vm.buscando && vm.form.area.$invalid}]">
                            <label class="control-label">{{ __('common.area') }}</label>
                            <select selectize="vm.configArea" options="vm.opcionesArea" disabled-typing="true" ng-model="vm.filtroArea" search-model="vm.filtroArea" search-predicate="area" name="area" required class="form-control"></select>
                            <span class="help-block show" ng-show="vm.buscando && vm.form.area.$invalid">
                                <span ng-show="vm.form.area.$error.required">Requerido</span>
                            </span>
                        </div>
                    </div>
                    <button class="btn btn-primary btn-fab nex-btn-add" title="{{ __('common.buscar') }}" ng-click="vm.buscar()" ng-disabled="vm.deshabilitarBuscado" style="top: 7.5em; right: 62.5%">
                        <i class="material-icons">search</i>
                    </button>
                    <button  class="btn btn-default btn-fab nex-btn-add" style="top: 7.5em; left: -19%;" type="button" rel="tooltip" title="{{ __('common.limpiar_filtros') }}" ng-click="vm.limpiarFiltros()">
                        <i class="material-icons">clear_all</i>
                    </button>
                </div>
            </form>
            <hr>
            <div class="row" style="margin: 0;" ng-if="!vm.cargando && vm.coleccion.length > 0">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 hidden-xs text-left" style="padding: 0;">
                    <nav>
                        <ul class="pagination pagination-primary">
                            <li class="text-left">
                                <a style="padding: 0 5px;" href="" type="button" rel="tooltip" title="{{ __('common.limpiar_filtros') }}" ng-click="vm.limpiarFiltros()">
                                    <i class="material-icons">clear_all</i>
                                </a>
                            </li>
                            <li class="text-left">
                                <span style="text-transform: none; font-size: 14px;">
                                    @{{ 'Mostrando ' + vm.paginacion.desde + ' a ' + vm.paginacion.hasta + ' de ' + vm.paginacion.total + ' resultados - Página ' + vm.paginacion.paginaActual + ' de ' + vm.paginacion.ultimaPagina }}
                                </span>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding: 0;">
                    <div class="text-right" st-pagination="" st-items-by-page="vm.itemsPorPagina" st-displayed-pages="5" st-template="/paginacion-template" style="float: right;"></div>
                    <div class="text-right" style="float: right; margin: 26px 25px 26px 0;">
                        <label style="color: rgba(0,0,0,0.68);">
                            Filas:
                            <select ng-options="option for option in [5, 10, 25, 50, 100, 500] track by option" ng-model="vm.itemsPorPagina" class="select-grid"></select>
                        </label>
                    </div>
                </div>
            </div>
            <div class="table-responsive" style="font-size: 12px;">
                <table class="table table-hover table-sm table-ellipsis">
                    <thead>
                        <tr>
                            <th st-multi-sort="nombre_ciudad">{{ __('common.nombre') }}<br>{{ __('common.ciudad') }}</th>
                            <th st-multi-sort="proceso">{{ __('common.proceso_') }}</th>
                            <th st-multi-sort="juzgado_actual">{{ __('common.juzgado') }}<br>{{ __('common.actual') }}</th>
                            <th st-multi-sort="despacho_numero" class="text-right">{{ __('common.nro.') }}<br>{{ __('common.desp.') }}</th>
                            <th st-multi-sort="demandante">{{ __('common.demandante') }}</th>
                            <th st-multi-sort="demandado">{{ __('common.demandado') }}</th>
                        </tr>
                    </thead>
                    <thead ng-show="vm.cargando">
                        <tr>
                            <td colspan="10" class="text-center" style="padding: 0px;">
                                <div class="progress progress-striped active" style="margin: 0px;">
                                    <div class="progress-bar" style="width: 100%"></div>
                                </div>
                            </td>
                        </tr>
                    </thead>
                    <tbody ng-show="!vm.cargando">
                        <tr ng-repeat="row in vm.coleccion">
                            <td>@{{row.ciudad_nombre}}</td>
                            <td>@{{row.numero_proceso}}</td>
                            <td>@{{row.juzgado_nombre}}</td>
                            <td class="text-right">@{{row.despacho_numero}}</td>
                            <td>@{{row.nombres_demandantes}}</td>
                            <td>@{{row.nombres_demandados}}</td>
                        </tr>

                    </tbody>
                    <tfoot ng-if="!vm.cargando && vm.coleccion.length > 0">
                        <script>$(document).ready(function(){$.material.ripples('table .btn');});</script>
                    </tfoot>
                </table>
            </div>
            <div class="row" style="margin: 0;" ng-show="!vm.cargando && vm.coleccion.length == 0">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 0;">
                    <nav>
                        <ul class="pagination pagination-primary">
                            <li class="text-left">
                                <a style="padding: 0 5px;" href="" type="button" rel="tooltip" title="{{ __('common.limpiar_filtro') }}" ng-click="vm.limpiarFiltros()">
                                    <i class="material-icons">clear_all</i>
                                </a>
                                <span style="text-transform: none; font-size: 14px;">{{ __('common.sin_resultados') }}</span>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="row" style="margin: 0;" ng-if="!vm.cargando && vm.coleccion.length > 0">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 hidden-xs text-left" style="padding: 0;">
                    <nav>
                        <ul class="pagination pagination-primary">
                            <li class="text-left">
                                <a style="padding: 0 5px;" type="button" rel="tooltip" title="{{ __('common.limpiar_filtros') }}" ng-click="vm.limpiarFiltros()">
                                    <i class="material-icons">clear_all</i>
                                </a>
                            </li>
                            <li class="text-left">
                                <span style="text-transform: none; font-size: 14px;">
                                    @{{ 'Mostrando ' + vm.paginacion.desde + ' a ' + vm.paginacion.hasta + ' de ' + vm.paginacion.total + ' resultados - Página ' + vm.paginacion.paginaActual + ' de ' + vm.paginacion.ultimaPagina }}
                                </span>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding: 0;">
                    <div class="text-right" st-pagination="" st-items-by-page="vm.itemsPorPagina" st-displayed-pages="5" st-template="/paginacion-template" style="float: right;"></div>
                    <div class="text-right" style="float: right; margin: 26px 25px 26px 0;">
                        <label style="color: rgba(0,0,0,0.68);">
                            Filas:
                            <select ng-options="option for option in [5, 10, 25, 50, 100, 500] track by option" ng-model="vm.itemsPorPagina" class="select-grid"></select>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>$(document).ready(function(){$.material.init();});</script>
</div>
