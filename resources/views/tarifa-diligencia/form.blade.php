<div class="modal-header">
    <h4 class="card-title">{{ __('common.datos_tarifa_diligencia') }}</h4>
</div>
<div class="modal-body" style="padding-top: 0;">
    <form name="vm.form" autocomplete="off" novalidate>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.tipoDiligenciaId, 'is-not-empty': vm.tipoDiligenciaId, 'with-error': vm.guardando && vm.form.tipoDiligencia.$invalid}]">
                    <label class="control-label">{{ __('common.tipo_diligencia') }}</label>
                    <select selectize="vm.configTipoDiligencia" options="vm.opcionesTipoDiligencia" disabled-typing="true" ng-model="vm.tipoDiligenciaId" name="tipoDiligencia" required class="form-control"></select>
                    <span class="help-block show" ng-show="vm.guardando && vm.form.tipoDiligencia.$invalid">
                        <span ng-show="vm.form.tipoDiligencia.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.departamentoId, 'is-not-empty': vm.departamentoId, 'with-error': vm.guardando && vm.form.departamento.$invalid}]">
                    <label class="control-label">{{ __('common.departamento') }}</label>
                    <select selectize="vm.configDepartamento" options="vm.opcionesDepartamentos" disabled-typing="true" ng-model="vm.departamentoId" name="departamento" required class="form-control"></select>
                    <span class="help-block show" ng-show="vm.guardando && vm.form.departamento.$invalid">
                        <span ng-show="vm.form.departamento.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.ciudadId, 'is-not-empty': vm.ciudadId, 'with-error': vm.guardando && vm.form.ciudad.$invalid}]">
                    <label class="control-label">{{ __('common.ciudad') }}</label>
                    <select selectize="vm.configCiudad" options="vm.opcionesCiudad" disabled-typing="true" ng-model="vm.ciudadId" name="ciudad" required class="form-control" clear-options="vm.limpiarOpcionesCiudad"></select>
                    <span class="help-block show">Debe seleccionar el departamento</span>
                    <span class="help-block show" ng-show="vm.guardando && vm.form.ciudad.$invalid">
                        <span ng-show="vm.form.ciudad.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.valorDiligencia, 'is-not-empty': vm.valorDiligencia, 'with-error': vm.guardando && vm.form.valorDiligencia.$invalid}]">
                    <label class="control-label">{{ __('common.valor') }} {{ __('common.diligencia') }}</label>
                    <input ng-model="vm.valorDiligencia" name="valorDiligencia" required class="form-control">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.valorDiligencia.$invalid">
                        <span ng-show="vm.form.valorDiligencia.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.gastoEnvio, 'is-not-empty': vm.gastoEnvio || vm.gastoEnvio == 0, 'with-error': vm.guardando && vm.form.gastoEnvio.$invalid}]">
                    <label class="control-label">{{ __('common.gastos_envio') }}</label>
                    <input ng-model="vm.gastoEnvio" name="gastoEnvio" class="form-control" numeric type="text">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.gastoEnvio.$invalid">
                        <span ng-show="vm.form.gastoEnvio.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.otrosGastos, 'is-not-empty': vm.otrosGastos || vm.otrosGastos == 0, 'with-error': vm.guardando && vm.form.otrosGastos.$invalid}]" style="padding-bottom: 30px;">
                    <label class="control-label">{{ __('common.otros_gastos') }}</label>
                    <input ng-model="vm.otrosGastos" name="otrosGastos" class="form-control" type="text">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.otrosGastos.$invalid">
                        <span ng-show="vm.form.otrosGastos.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <p style="margin-bottom: 0;">{{ __('common.estado') }}</p>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="vm.estado" ng-value="true">
                                {{ __('common.activo') }}
                            </label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="vm.estado" ng-value="false">
                                {{ __('common.inactivo') }}
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button class="btn btn-primary" type="button" ng-click="vm.guardar()" ng-disabled="vm.deshabilitarGuardado">{{ __('common.guardar') }}</button>
    <button class="btn btn-default" type="button" ng-click="vm.cancelar()">{{ __('common.cancelar') }}</button>
</div>
<script>$(document).ready(function(){$.material.init();});</script>
