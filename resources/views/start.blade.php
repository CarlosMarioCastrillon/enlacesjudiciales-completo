<!DOCTYPE html>
<html lang="en" ng-app="enlaces.start">
    <head>
        <meta charset="utf-8">
        <meta name="theme-color" content="#D0AD4C">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />

        <title>Bienvenido</title>
        <link rel="shortcut icon" href="/favicon.ico" />

        <style>
            svg {
                position: absolute;
                top: 40%;
                left: 45%;
            }
        </style>

    </head>
    <body ng-controller="startController as vm">
        <svg width="200px"  height="200px"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-ripple" style="background: none;">
            <circle cx="50" cy="50" r="21.1259" fill="none" ng-attr-stroke="@{{config.c1}}" ng-attr-stroke-width="@{{config.width}}" stroke="#fdfdfd" stroke-width="2">
                <animate attributeName="r" calcMode="spline" values="0;40" keyTimes="0;1" dur="1" keySplines="0 0.2 0.8 1" begin="-0.5s" repeatCount="indefinite"></animate>
                <animate attributeName="opacity" calcMode="spline" values="1;0" keyTimes="0;1" dur="1" keySplines="0.2 0 0.8 1" begin="-0.5s" repeatCount="indefinite"></animate>
            </circle>
            <circle cx="50" cy="50" r="38.1391" fill="none" ng-attr-stroke="@{{config.c2}}" ng-attr-stroke-width="@{{config.width}}" stroke="#85a2b6" stroke-width="2">
                <animate attributeName="r" calcMode="spline" values="0;40" keyTimes="0;1" dur="1" keySplines="0 0.2 0.8 1" begin="0s" repeatCount="indefinite"></animate>
                <animate attributeName="opacity" calcMode="spline" values="1;0" keyTimes="0;1" dur="1" keySplines="0.2 0 0.8 1" begin="0s" repeatCount="indefinite"></animate>
            </circle>
        </svg>
        <script src={{ mix('/js/start.js') }}></script>
    </body>
</html>