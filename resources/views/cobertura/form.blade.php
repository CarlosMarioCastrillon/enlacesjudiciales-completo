<style>
    SELECT, INPUT[type="text"] {
        width: 100%;
        box-sizing: border-box;
        height: auto;
        border: 1px solid #d0d0d0;
    }
    SECTION {
        padding: 8px;
        background-color: white;
        overflow: auto;

    }
    SECTION > DIV {
        float: left;
        padding: 4px;
    }
    SECTION > DIV + DIV {
        width: 40px;
        text-align: center;
    }
</style>
<div class="modal-header">
    <h4 class="card-title">{{ __('common.datos_cobertura') }}</h4>
</div>
<div class="modal-body">
    <form name="vm.form" autocomplete="off" novalidate>
        <div class="row">
             <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.departamentoId, 'is-not-empty': vm.departamentoId, 'with-error': vm.guardando && vm.form.departamento.$invalid}]" style="top: -10px;">
                    <label class="control-label">{{ __('common.departamento') }}</label>
                    <select selectize="vm.configDepartamento" options="vm.opcionesDepartamentos" disabled-typing="true" ng-model="vm.departamentoId" name="departamento" class="form-control"></select>
                    <span class="help-block show" ng-show="vm.guardando && vm.form.departamento.$invalid">
                        <span ng-show="vm.form.departamento.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <label style="padding-bottom: 7px;">{{ __('common.ciudades') }}</label>
            </div>
                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                    <select ng-model="vm.ciudadesSinSeleccionar" multiple size="10">
                        <option ng-repeat="ciudad in vm.ciudades" ng-value="ciudad.id">@{{ ciudad.nombre }}</option>
                    </select>
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="top: 36px;">
                    <a type="button" rel="tooltip" class="btn btn-dark center-block align-middle" title="{{ __('common.modificar') }}" ng-click="vm.seleccionarTodo()" style="padding: 2px 10px;">
                        <i class="material-icons">last_page</i>
                    </a>
                    <a type="button" rel="tooltip" class="btn btn-default center-block" title="{{ __('common.modificar') }}" ng-click="vm.seleccionar()" style="padding: 2px 10px;">
                        <i class="material-icons">chevron_right</i>
                    </a>
                    <a type="button" rel="tooltip" class="btn btn-default center-block" title="{{ __('common.modificar') }}" ng-click="vm.deseleccionar()" style="padding: 2px 10px;">
                        <i class="material-icons">chevron_left</i>
                    </a>
                    <a type="button" rel="tooltip" class="btn btn-default center-block" title="{{ __('common.modificar') }}" ng-click="vm.deseleccionarTodo()" style="padding: 2px 10px;">
                        <i class="material-icons">first_page</i>
                    </a>
                </div>
                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                    <select ng-model="vm.ciudadesSeleccionadas" multiple size="10">
                        <option ng-repeat="ciudad in vm.ciudadesCobertura" ng-value="ciudad.id">@{{ ciudad.nombre }}</option>
                    </select>
                </div>
        </div>
    </form>
</div>
<div class="modal-footer" style="margin-right: 9px;">
    <button class="btn btn-primary" type="button" ng-click="vm.guardar()" ng-disabled="vm.deshabilitarGuardado">{{ __('common.guardar') }}</button>
    <button class="btn btn-default" type="button" ng-click="vm.cancelar()">{{ __('common.cancelar') }}</button>
</div>
<script>$(document).ready(function(){$.material.init();});</script>
