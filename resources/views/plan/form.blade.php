<div class="modal-header">
    <h4 class="card-title">{{ __('common.datos_plan') }}</h4>
</div>
<div class="modal-body">
    <form name="vm.form" autocomplete="off" novalidate>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.servicioId, 'is-not-empty': vm.servicioId, 'with-error': vm.guardando && vm.form.servicio.$invalid}]" style="top: -10px;">
                    <label class="control-label">{{ __('common.servicio') }}</label>
                    <select selectize="vm.configServicio" options="vm.opcionesServicios" disabled-typing="true" ng-model="vm.servicioId" name="servicio" required class="form-control"></select>
                    <span class="help-block show" ng-show="vm.guardando && vm.form.servicio.$invalid">
                        <span ng-show="vm.form.servicio.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
             <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.departamentoId, 'is-not-empty': vm.departamentoId, 'with-error': vm.guardando && vm.form.departamento.$invalid}]" style="top: -10px;">
                    <label class="control-label">{{ __('common.departamento') }}</label>
                    <select selectize="vm.configDepartamento" options="vm.opcionesDepartamentos" disabled-typing="true" ng-model="vm.departamentoId" name="departamento" required class="form-control"></select>
                    <span class="help-block show" ng-show="vm.guardando && vm.form.departamento.$invalid">
                        <span ng-show="vm.form.departamento.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.ciudadId, 'is-not-empty': vm.ciudadId, 'with-error': vm.guardando && vm.form.ciudad.$invalid}]">
                    <label class="control-label">{{ __('common.ciudad') }}</label>
                    <select selectize="vm.configCiudad" options="vm.opcionesCiudad" disabled-typing="true" ng-model="vm.ciudadId" name="ciudad" required class="form-control"></select>
                    <span class="help-block show" ng-show="vm.guardando && vm.form.ciudad.$invalid">
                        <span ng-show="vm.form.ciudad.$error.required">Si elige departamento debe elegir ciudad</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div ng-class="['form-group label-floating', {'is-empty': !(!!vm.cantidad || vm.cantidad == 0), 'is-not-empty': !!vm.cantidad || vm.cantidad == 0, 'with-error': vm.guardando && vm.form.cantidad.$invalid}]"  style="padding-bottom: 0; top: 10px;">
                    <label class="control-label">{{ __('common.cantidad') }}</label>
                    <input ng-model="vm.cantidad" name="cantidad" min="0" step="1" required ng-pattern="/^[0-9]+$/" class="form-control" type="number">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.cantidad.$invalid">
                        <span ng-show="vm.form.cantidad.$error.required">Requerido</span>
                        <span ng-show="vm.form.cantidad.$error.pattern">Debe ingresar una cantidad entero.</span>
                    </span>
                    <br>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div ng-class="['form-group label-floating', {'is-empty': !(!!vm.valor || vm.valor == 0), 'is-not-empty': !!vm.valor || vm.valor == 0, 'with-error': vm.guardando && vm.form.valor.$invalid}]"  style="padding-bottom: 0;">
                    <label class="control-label">{{ __('common.valor') }}</label>
                    <input ng-model="vm.valor" name="valor" min="0" step="1" ng-pattern="/^[0-9]+$/" class="form-control" type="number">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.valor.$invalid">
                        <span ng-show="vm.form.cantidad.$error.pattern">Debe ingresar una cantidad entero.</span>
                    </span>
                    <br>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div  ng-class="['form-group label-floating', {'is-empty': !vm.observacion, 'is-not-empty': vm.observacion}]" style="top: -10px;">
                    <label class="control-label">{{ __('common.observacion') }}</label>
                    <textarea ng-model="vm.observacion" name="observacion" class="form-control" type="text" rows="1"></textarea>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <p style="margin-bottom: 0;">{{ __('common.estado') }}</p>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="vm.estado" ng-value="true">
                                {{ __('common.activo') }}
                            </label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="vm.estado" ng-value="false">
                                {{ __('common.inactivo') }}
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button class="btn btn-primary" type="button" ng-click="vm.guardar()" ng-disabled="vm.deshabilitarGuardado">{{ __('common.guardar') }}</button>
    <button class="btn btn-default" type="button" ng-click="vm.cancelar()">{{ __('common.cancelar') }}</button>
</div>
<script>$(document).ready(function(){$.material.init();});</script>
