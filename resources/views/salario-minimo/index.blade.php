<div id="salarioMinimo" class="row" st-table="vm.coleccion" st-pipe="vm.obtenerResumen" st-persist="resumenSalarioMinimo" st-reset="isReset" refresh-table>
    <div class="card main-card" style="margin-bottom: 0;">
        <div class="card-content">
            <h4 class="card-title">@{{ vm.titulo }}</h4>
            @can('CrearSalarioMinimo')
                <a class="btn btn-primary btn-round btn-fab nex-btn-add" title="Nuevo servicio" ng-click="vm.crear()">
                    <i class="material-icons">add</i>
                </a>
            @endcan
            <div class="row">
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                   <div class="input-group">
                        <div ng-class="['form-group label-floating', {'is-empty': !vm.filtroFecha, 'is-not-empty': vm.filtroFecha}]">
                            <label class="control-label">{{ __('common.fecha') }}</label>
                            <input st-search-datetimepicker search-model="vm.filtroFecha" search-predicate="fecha" datetimepicker-st-table datetimepicker-options="{useCurrent: false, format: 'YYYY-MM-DD'}" ng-model="vm.filtroFecha" class="form-control" type="text"/>
                            <i class="input-group-addon fa fa-calendar"></i>
                        </div>
                   </div>
                </div>
            </div>
            <hr>
            <div class="row" style="margin: 0;" ng-if="!vm.cargando && vm.coleccion.length > 0">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 hidden-xs text-left" style="padding: 0;">
                    <nav>
                        <ul class="pagination pagination-primary">
                            <li class="text-left">
                                <a style="padding: 0 5px;" href="" type="button" rel="tooltip" title="{{ __('common.limpiar_filtros') }}" ng-click="vm.limpiarFiltros()">
                                    <i class="material-icons">clear_all</i>
                                </a>
                            </li>
                            <li class="text-left">
                                <span style="text-transform: none; font-size: 14px;">
                                    @{{ 'Mostrando ' + vm.paginacion.desde + ' a ' + vm.paginacion.hasta + ' de ' + vm.paginacion.total + ' resultados - Página ' + vm.paginacion.paginaActual + ' de ' + vm.paginacion.ultimaPagina }}
                                </span>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding: 0;">
                    <div class="text-right" st-pagination="" st-items-by-page="vm.itemsPorPagina" st-displayed-pages="5" st-template="/paginacion-template" style="float: right;"></div>
                    <div class="text-right" style="float: right; margin: 26px 25px 26px 0;">
                        <label style="color: rgba(0,0,0,0.68);">
                            Filas:
                            <select ng-options="option for option in [5, 10, 25, 50, 100, 500] track by option" ng-model="vm.itemsPorPagina" class="select-grid"></select>
                        </label>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-hover table-sm">
                    <thead>
                        <tr>
                            <th st-multi-sort="fecha">{{ __('common.fecha') }}</th>
                            <th st-multi-sort="valor">{{ __('common.valor') }}</th>
                            <th st-multi-sort="estado" class="text-center">{{ __('common.estado') }}</th>
                            <th st-multi-sort="usuario_creacion_nombre" class="text-left">{{ __('common.creado_por') }}</th>
                            <th st-multi-sort="fecha_creacion" class="text-right">{{ __('common.fecha_creacion') }}</th>
                            <th st-multi-sort="usuario_modificacion_nombre">{{ __('common.modificado_por') }}</th>
                            <th st-multi-sort="fecha_modificacion" class="text-right">{{ __('common.fecha_modificacion') }}</th>
                            @if(auth()->user()->can('ModificarSalarioMinimo') || auth()->user()->can('EliminarSalarioMinimo'))
                                <th class="text-right">{{ __('common.acciones') }}</th>
                            @endif
                        </tr>
                    </thead>
                    <thead ng-show="vm.cargando">
                        <tr>
                            <td colspan="8" class="text-center" style="padding: 0px;">
                                <div class="progress progress-striped active" style="margin: 0px;">
                                    <div class="progress-bar" style="width: 100%"></div>
                                </div>
                            </td>
                        </tr>
                    </thead>
                    <tbody ng-show="!vm.cargando">
                        <tr ng-repeat="row in vm.coleccion">
                            <td>@{{row.fecha}}</td>
                            <td>@{{row.valor_salario_min | currency:$:0 }}</td>
                            <td class="text-left">
                                <span class="label label-success" ng-if="!!(+row.estado)">{{ __('common.activo') }}</span>
                                <span class="label label-danger" ng-if="!(+row.estado)">{{ __('common.inactivo') }}</span>
                            </td>
                            <td>@{{ row.usuario_creacion_nombre }}</td>
                            <td class="text-right">@{{ row.fecha_creacion  }}</td>
                            <td class="text-left">@{{ row.usuario_modificacion_nombre }}</td>
                            <td class="text-right">@{{ row.fecha_modificacion }}</td>
                            @if(auth()->user()->can('ModificarSalarioMinimo') || auth()->user()->can('EliminarSalarioMinimo'))
                                <td class="td-actions text-right">
                                @can('ModificarSalarioMinimo')
                                        <a type="button" rel="tooltip" class="btn btn-primary btn-simple" title="{{ __('common.modificar') }}" ng-click="vm.crear(row)">
                                            <i class="material-icons">mode_edit</i>
                                        </a>
                                @endcan
                                @can('EliminarSalarioMinimo')
                                        <a type="button" rel="tooltip" class="btn btn-danger btn-simple" title="{{ __('common.eliminar') }}" ng-click="vm.confirmarEliminar(row)">
                                            <i class="material-icons">delete</i>
                                        </a>
                                @endcan
                                </td>
                            @endif
                        </tr>
                    </tbody>
                    <tfoot ng-if="!vm.cargando && vm.coleccion.length > 0">
                        <script>$(document).ready(function(){$.material.ripples('table .btn');});</script>
                    </tfoot>
                </table>
            </div>
            <div class="row" style="margin: 0;" ng-show="!vm.cargando && vm.coleccion.length == 0">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 0;">
                    <nav>
                        <ul class="pagination pagination-primary">
                            <li class="text-left">
                                <a style="padding: 0 5px;" href="" type="button" rel="tooltip" title="{{ __('common.limpiar_filtro') }}" ng-click="vm.limpiarFiltros()">
                                    <i class="material-icons">clear_all</i>
                                </a>
                                <span style="text-transform: none; font-size: 14px;">{{ __('common.sin_resultados') }}</span>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="row" style="margin: 0;" ng-if="!vm.cargando && vm.coleccion.length > 0">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 hidden-xs text-left" style="padding: 0;">
                    <nav>
                        <ul class="pagination pagination-primary">
                            <li class="text-left">
                                <a style="padding: 0 5px;" href="" type="button" rel="tooltip" title="{{ __('common.limpiar_filtros') }}" ng-click="vm.limpiarFiltros()">
                                    <i class="material-icons">clear_all</i>
                                </a>
                            </li>
                            <li class="text-left">
                                <span style="text-transform: none; font-size: 14px;">
                                    @{{ 'Mostrando ' + vm.paginacion.desde + ' a ' + vm.paginacion.hasta + ' de ' + vm.paginacion.total + ' resultados - Página ' + vm.paginacion.paginaActual + ' de ' + vm.paginacion.ultimaPagina }}
                                </span>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding: 0;">
                    <div class="text-right" st-pagination="" st-items-by-page="vm.itemsPorPagina" st-displayed-pages="5" st-template="/paginacion-template" style="float: right;"></div>
                    <div class="text-right" style="float: right; margin: 26px 25px 26px 0;">
                        <label style="color: rgba(0,0,0,0.68);">
                            Filas:
                            <select ng-options="option for option in [5, 10, 25, 50, 100, 500] track by option" ng-model="vm.itemsPorPagina" class="select-grid"></select>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>$(document).ready(function(){$.material.init();});</script>
</div>
