<div class="modal-header">
    <h4 class="card-title">{{ __('common.datos_ciudad') }}</h4>
</div>
<div class="modal-body" style="padding-top: 0;">
    <form name="vm.form" autocomplete="off" novalidate>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.nombre, 'is-not-empty': vm.nombre, 'with-error': vm.guardando && vm.form.nombre.$invalid}]">
                    <label class="control-label">{{ __('common.ciudad') }}</label>
                    <input ng-model="vm.nombre" name="nombre" required maxlength="191" class="form-control" type="text">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.nombre.$invalid">
                        <span ng-show="vm.form.nombre.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.departamentoId, 'is-not-empty': vm.departamentoId, 'with-error': vm.guardando && vm.form.departamento.$invalid}]">
                    <label class="control-label">{{ __('common.departamento') }}</label>
                    <select selectize="vm.configDepartamento" options="vm.opcionesDepartamentos" disabled-typing="true" ng-model="vm.departamentoId" name="departamento" required class="form-control"></select>
                    <span class="help-block show" ng-show="vm.guardando && vm.form.departamento.$invalid">
                        <span ng-show="vm.form.departamento.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.codigoDane, 'is-not-empty': vm.codigoDane, 'with-error': vm.guardando && vm.form.codigoDane.$invalid}]" style="padding-bottom: 30px;"><label class="control-label">{{ __('common.codigo_dane') }}</label>
                    <input ng-model="vm.codigoDane"  ng-model-options="{allowInvalid: true }" name="codigoDane" ng-pattern="/^[0-9]{5}$/" required maxlength="5" class="form-control" type="text">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.codigoDane.$invalid">
                        <span ng-show="vm.form.codigoDane.$error.required">Requerido</span>
                        <span ng-show="vm.form.codigoDane.$error.pattern">El Código ciudad DANE debe ser un número de 5 dígitos</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="row" style="padding-bottom: 10px;">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <span>{{ __('common.rama_judicial') }}</span>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="vm.ramaJudicial" ng-value="true">
                                {{ __('common.si') }}
                            </label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="vm.ramaJudicial" ng-value="false">
                                {{ __('common.no') }}
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <p style="margin-bottom: 0;">{{ __('common.estado') }}</p>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="vm.estado" ng-value="true">
                                {{ __('common.activo') }}
                            </label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="vm.estado" ng-value="false">
                                {{ __('common.inactivo') }}
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button class="btn btn-primary" type="button" ng-click="vm.guardar()" ng-disabled="vm.deshabilitarGuardado">{{ __('common.guardar') }}</button>
    <button class="btn btn-default" type="button" ng-click="vm.cancelar()">{{ __('common.cancelar') }}</button>
</div>
<script>$(document).ready(function(){$.material.init();});</script>
