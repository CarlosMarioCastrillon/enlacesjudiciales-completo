<div id="empresa" class="row" st-table="vm.coleccion" st-pipe="vm.obtenerResumen" st-persist="resumenEmpresa" st-reset="isReset" refresh-table>
    <div class="card main-card" style="margin-bottom: 0;">
        <div class="card-content">
            <h4 class="card-title">@{{ vm.titulo }}</h4>
            @can('CrearEmpresa')
                <a class="btn btn-primary btn-round btn-fab nex-btn-add" title="Nuevo empresa" ng-click="vm.crear()">
                    <i class="material-icons">add</i>
                </a>
                <button class="btn btn-success btn-fab nex-btn-add" style="right: 65px;" title="Exportar a excel" ng-click="vm.obtenerExcel()">
                    <i class="mdi mdi-file-excel"></i>
                </button>
            @endcan
            <div class="row">
                <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9" style="padding-left: 0px;">
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <div class="form-group label-floating">
                            <label class="control-label">{{ __('common.empresa') }}</label>
                            <input st-search="nombre" ng-model="vm.filtroNombre" class="form-control" type="text"/>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <div class="form-group label-floating">
                            <label class="control-label">{{ __('common.numero_documento') }}</label>
                            <input st-search="numero_documento" ng-model="vm.filtroNumeroDocumento" class="form-control" type="text"/>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <div class="form-group label-floating">
                            <label class="control-label">{{ __('common.ciudad') }}</label>
                            <input st-search="ciudad_nombre" ng-model="vm.filtroCiudadNombre" class="form-control" type="text"/>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <div class="form-group label-floating">
                            <label class="control-label">{{ __('common.zona') }}</label>
                            <input st-search="zona_nombre" ng-model="vm.filtroZonaNombre" class="form-control" type="text"/>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" style="left: 22px;">
                    <div class="row" style="margin-left: -41px; margin-right: 105px; margin-top: 2px;">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label class="control-label" style="margin-top: 11px; margin-left: 6px; margin-bottom: 0px;">{{ __('common.estado') }}</label>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="radio">
                                <label>
                                    <input type="radio" st-search="estado" ng-model="vm.filtroEstado" ng-value="1">
                                    {{ __('common.activo') }}
                                </label>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="radio">
                                <label>
                                    <input type="radio" st-search="estado" ng-model="vm.filtroEstado" ng-value="0">
                                    {{ __('common.inactivo') }}
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row" style="margin: 0;" ng-if="!vm.cargando && vm.coleccion.length > 0">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 hidden-xs text-left" style="padding: 0;">
                    <nav>
                        <ul class="pagination pagination-primary">
                            <li class="text-left">
                                <a style="padding: 0 5px;" href="" type="button" rel="tooltip" title="{{ __('common.limpiar_filtros') }}" ng-click="vm.limpiarFiltros()">
                                    <i class="material-icons">clear_all</i>
                                </a>
                            </li>
                            <li class="text-left">
                                <span style="text-transform: none; font-size: 14px;">
                                    @{{ 'Mostrando ' + vm.paginacion.desde + ' a ' + vm.paginacion.hasta + ' de ' + vm.paginacion.total + ' resultados - Página ' + vm.paginacion.paginaActual + ' de ' + vm.paginacion.ultimaPagina }}
                                </span>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding: 0;">
                    <div class="text-right" st-pagination="" st-items-by-page="vm.itemsPorPagina" st-displayed-pages="5" st-template="/paginacion-template" style="float: right;"></div>
                    <div class="text-right" style="float: right; margin: 26px 25px 26px 0;">
                        <label style="color: rgba(0,0,0,0.68);">
                            Filas:
                            <select ng-options="option for option in [5, 10, 25, 50, 100, 500] track by option" ng-model="vm.itemsPorPagina" class="select-grid"></select>
                        </label>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-hover table-sm">
                    <thead>
                        <tr>
                            <th st-multi-sort="nombre">{{ __('common.nombre') }} {{ __('common.empresa') }}</th>
                            <th st-multi-sort="numero_documento" class="text-right">{{ __('common.numero') }}<br>{{ __('common.documento') }}</th>
                            <th st-multi-sort="ciudad">{{ __('common.ciudad') }}</th>
                            <th st-multi-sort="actuacion_digital">{{ __('common.actuacion') }}<br>{{ __('common.digital') }}</th>
                            <th st-multi-sort="vigilancia_proceso">{{ __('common.vigilancia') }}<br>{{ __('common.proceso') }}</th>
                            <th st-multi-sort="dataprocesos">{{ __('common.dataprocesos') }}</th>
                            <th class="text-center">{{ __('common.planes') }}</th>
                            <th class="text-center">{{ __('common.cobertura') }}</th>
                            <th class="text-center">{{ __('common.usuarios') }}</th>
                            <th class="text-center">{{ __('common.areas') }}</th>
                            <th class="text-right">{{ __('common.fecha') }}<br>{{ __('common.ingreso') }}</th>
                            <th class="text-right">{{ __('common.fecha') }}<br>{{ __('common.vencimiento') }}</th>
                            <th st-multi-sort="estado" class="text-center">{{ __('common.estado') }}</th>
                            @if(auth()->user()->can('ModificarEmpresa') || auth()->user()->can('EliminarEmpresa'))
                                <th class="text-right">{{ __('common.acciones') }}</th>
                            @endif
                        </tr>
                    </thead>
                    <thead ng-show="vm.cargando">
                        <tr>
                            <td colspan="14" class="text-center" style="padding: 0px;">
                                <div class="progress progress-striped active" style="margin: 0px;">
                                    <div class="progress-bar" style="width: 100%"></div>
                                </div>
                            </td>
                        </tr>
                    </thead>
                    <tbody ng-show="!vm.cargando">
                        <tr ng-repeat="row in vm.coleccion">
                            <td>@{{row.nombre}}</td>
                            <td class="text-right">@{{row.numero_documento}}</td>
                            <td>@{{row.ciudad_nombre}}</td>
                            <td class="text-center">
                                <span ng-if="!!(+row.con_actuaciones_digitales)">{{ __('common.si') }}</span>
                                <span ng-if="!(+row.con_actuaciones_digitales)">{{ __('common.no') }}</span>
                            </td>
                            <td class="text-center">
                                <span ng-if="!!(+row.con_vigilancia)">{{ __('common.si') }}</span>
                                <span ng-if="!(+row.con_vigilancia)">{{ __('common.no') }}</span>
                            </td>
                            <td class="text-center">
                                <span ng-if="!!(+row.con_data_procesos)">{{ __('common.si') }}</span>
                                <span ng-if="!(+row.con_data_procesos)">{{ __('common.no') }}</span>
                            </td>
                            <td class="td-actions text-center">
                                <a type="button" rel="tooltip" class="btn btn-primary btn-simple" title="{{ __('common.planes') }}" href="#/empresas/@{{ row.id }}/planes">
                                    <i class="material-icons">ballot</i>
                                </a>
                            </td>
                            <td class="td-actions text-center">
                                <a type="button" rel="tooltip" class="btn btn-primary btn-simple" title="{{ __('common.cobertura') }}" href="#/empresas/@{{ row.id }}/coberturas">
                                    <i class="material-icons">language</i>
                                </a>
                            </td>
                            <td class="td-actions text-center">
                                <a type="button" rel="tooltip" class="btn btn-primary btn-simple" title="{{ __('common.usuarios') }}" href="#/empresas/@{{ row.id }}/usuarios">
                                    <i class="material-icons">people</i>
                                </a>
                            </td>
                            <td class="td-actions text-center">
                                <a type="button" rel="tooltip" class="btn btn-primary btn-simple" title="{{ __('common.areas') }}" href="#/empresas/@{{ row.id }}/areas">
                                    <i class="material-icons">account_tree</i>
                                </a>
                            </td>
                            <td class="text-right">@{{row.fecha_creacion?row.fecha_creacion.substring(0,10):''}}</td>
                            <td class="text-right">@{{row.fecha_vencimiento}}</td>
                            <td class="text-center">
                                <span class="label label-success" ng-if="!!(+row.estado)">{{ __('common.activo') }}</span>
                                <span class="label label-danger" ng-if="!(+row.estado)">{{ __('common.inactivo') }}</span>
                            </td>
                            @if(auth()->user()->can('ModificarEmpresa') || auth()->user()->can('EliminarEmpresa'))
                                <td class="td-actions text-right">
                                @can('ModificarEmpresa')
                                        <a type="button" rel="tooltip" class="btn btn-primary btn-simple" title="{{ __('common.modificar') }}" ng-click="vm.crear(row)">
                                            <i class="material-icons">mode_edit</i>
                                        </a>
                                @endcan
                                @can('EliminarEmpresa')
                                    <a type="button" rel="tooltip" class="btn btn-danger btn-simple" title="{{ __('common.eliminar') }}" ng-click="vm.confirmarEliminar(row)">
                                        <i class="material-icons">delete</i>
                                    </a>
                                @endcan
                                </td>
                            @endif
                        </tr>
                    </tbody>
                    <tfoot ng-if="!vm.cargando && vm.coleccion.length > 0">
                        <script>$(document).ready(function(){$.material.ripples('table .btn');});</script>
                    </tfoot>
                </table>
            </div>
            <div class="row" style="margin: 0;" ng-show="!vm.cargando && vm.coleccion.length == 0">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 0;">
                    <nav>
                        <ul class="pagination pagination-primary">
                            <li class="text-left">
                                <a style="padding: 0 5px;" href="" type="button" rel="tooltip" title="{{ __('common.limpiar_filtro') }}" ng-click="vm.limpiarFiltros()">
                                    <i class="material-icons">clear_all</i>
                                </a>
                                <span style="text-transform: none; font-size: 14px;">{{ __('common.sin_resultados') }}</span>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="row" style="margin: 0;" ng-if="!vm.cargando && vm.coleccion.length > 0">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 hidden-xs text-left" style="padding: 0;">
                    <nav>
                        <ul class="pagination pagination-primary">
                            <li class="text-left">
                                <a style="padding: 0 5px;" href="" type="button" rel="tooltip" title="{{ __('common.limpiar_filtros') }}" ng-click="vm.limpiarFiltros()">
                                    <i class="material-icons">clear_all</i>
                                </a>
                            </li>
                            <li class="text-left">
                                <span style="text-transform: none; font-size: 14px;">
                                    @{{ 'Mostrando ' + vm.paginacion.desde + ' a ' + vm.paginacion.hasta + ' de ' + vm.paginacion.total + ' resultados - Página ' + vm.paginacion.paginaActual + ' de ' + vm.paginacion.ultimaPagina }}
                                </span>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding: 0;">
                    <div class="text-right" st-pagination="" st-items-by-page="vm.itemsPorPagina" st-displayed-pages="5" st-template="/paginacion-template" style="float: right;"></div>
                    <div class="text-right" style="float: right; margin: 26px 25px 26px 0;">
                        <label style="color: rgba(0,0,0,0.68);">
                            Filas:
                            <select ng-options="option for option in [5, 10, 25, 50, 100, 500] track by option" ng-model="vm.itemsPorPagina" class="select-grid"></select>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>$(document).ready(function(){$.material.init();});</script>

</div>
