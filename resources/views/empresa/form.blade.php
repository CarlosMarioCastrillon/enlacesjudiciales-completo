<div class="modal-header">
    <h4 class="card-title">{{ __('common.datos_empresa') }}</h4>
</div>
<div class="modal-body" style="padding-top: 0; padding-bottom: 0px;">
    <form name="vm.form" autocomplete="off" novalidate>
        <div class="row" style="margin-bottom: -4px;">
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.tipoDocumentoId, 'is-not-empty': vm.tipoDocumentoId, 'with-error': vm.guardando && vm.form.tipoDocumento.$invalid}]" style="top: -10px;">
                    <label class="control-label">{{ __('common.tipo_documento') }}</label>
                    <select selectize="vm.configTipoDocumento" options="vm.opcionesTipoDocumento" disabled-typing="false" ng-model="vm.tipoDocumentoId" name="tipoDocumento" required class="form-control"></select>
                    <span class="help-block show" ng-show="vm.guardando && vm.form.tipoDocumento.$invalid">
                        <span ng-show="vm.form.tipoDocumento.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.numeroDocumento, 'is-not-empty': vm.numeroDocumento, 'with-error': vm.guardando && vm.form.numeroDocumento.$invalid}]" style="top: -10px;">
                    <label class="control-label" style="margin-top: 12px;">{{ __('common.numero_documento') }}</label>
                    <input ng-model="vm.numeroDocumento" name="numeroDocumento" required maxlength="191" class="form-control" type="text">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.numeroDocumento.$invalid">
                            <span ng-show="vm.form.numeroDocumento.$error.required">Requerido</span>
                        </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.nombre, 'is-not-empty': vm.nombre, 'with-error': vm.guardando && vm.form.nombre.$invalid}]" style="top: -10px;">
                    <label class="control-label">{{ __('common.nombre') }}</label>
                    <input ng-model="vm.nombre" name="nombre" required maxlength="191" class="form-control" type="text">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.nombre.$invalid">
                    <span ng-show="vm.form.nombre.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.departamentoId, 'is-not-empty': vm.departamentoId, 'with-error': vm.guardando && vm.form.departamento.$invalid}]" style="top: -10px;">
                    <label class="control-label">{{ __('common.departamento') }}</label>
                    <select selectize="vm.configDepartamento" options="vm.opcionesDepartamentos" disabled-typing="false" ng-model="vm.departamentoId" name="departamento" required class="form-control"></select>
                    <span class="help-block show" ng-show="vm.guardando && vm.form.departamento.$invalid">
                    <span ng-show="vm.form.departamento.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.ciudadId, 'is-not-empty': vm.ciudadId, 'with-error': vm.guardando && vm.form.ciudad.$invalid}]" style="top: -10px;">
                    <label class="control-label">{{ __('common.ciudad') }}</label>
                    <select selectize="vm.configCiudad" options="vm.opcionesCiudad" disabled-typing="false" ng-model="vm.ciudadId" name="ciudad" required class="form-control" clear-options="vm.limpiarOpcionesCiudad"></select>
                    <span class="help-block show" ng-show="vm.guardando && vm.form.ciudad.$invalid">
                    <span ng-show="vm.form.ciudad.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.direccion, 'is-not-empty': vm.direccion, 'with-error': vm.guardando && vm.form.direccion.$invalid}]" style="top: -10px;">
                    <label class="control-label">{{ __('common.direccion') }}</label>
                    <input ng-model="vm.direccion" name="direccion" required maxlength="191" class="form-control" type="text">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.direccion.$invalid">
                    <span ng-show="vm.form.direccion.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.tipoEmpresaId, 'is-not-empty': vm.tipoEmpresaId, 'with-error': vm.guardando && vm.form.tipoEmpresa.$invalid}]" style="top: -10px;">
                    <label class="control-label" >{{ __('common.tipo_de_empresa') }}</label>
                    <select selectize="vm.configTipoEmpresa" options="vm.opcionesTipoEmpresa" disabled-typing="false" ng-model="vm.tipoEmpresaId" name="tipoEmpresa" required class="form-control"></select>
                    <span class="help-block show" ng-show="vm.guardando && vm.form.tipoEmpresa.$invalid">
                        <span ng-show="vm.form.tipoEmpresa.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.zonaId, 'is-not-empty': vm.zonaId, 'with-error': vm.guardando && vm.form.zona.$invalid}]" style="top: -10px;">
                    <label class="control-label">{{ __('common.zona') }}</label>
                    <select selectize="vm.configZona" options="vm.opcionesZona" disabled-typing="false" ng-model="vm.zonaId" name="zona" required class="form-control"></select>
                    <span class="help-block show" ng-show="vm.guardando && vm.form.zona.$invalid">
                        <span ng-show="vm.form.zona.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.telefonoFijo, 'is-not-empty': vm.telefonoFijo, 'with-error': vm.guardando && vm.form.telefonoFijo.$invalid}]" style="top: -10px;">
                    <label class="control-label">{{ __('common.telefono_fijo') }}</label>
                    <input ng-model="vm.telefonoFijo"  ng-model-options="{allowInvalid: true}" name="telefonoFijo" ng-pattern="/^[0-9]{7,15}$/" maxlength="15" class="form-control">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.telefonoFijo.$invalid">
                            <span ng-show="vm.form.telefonoFijo.$error.pattern">Ingrese un número de teléfono válido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.telefonoCelular, 'is-not-empty': vm.telefonoCelular, 'with-error': !!vm.telefonoCelular && vm.form.telefonoCelular.$invalid}]" style="top: -10px;">
                    <label class="control-label">{{ __('common.telefono_celular') }}</label>
                    <input ng-model="vm.telefonoCelular" ng-model-options="{allowInvalid: true}" name="telefonoCelular" ng-pattern="/^[0-9]{10,15}$/" maxlength="15" class="form-control">
                    <span class="help-block show" ng-show="!!vm.telefonoCelular && vm.form.telefonoCelular.$invalid">
                        <span ng-show="vm.form.telefonoCelular.$error.pattern">Ingrese un número de celular válido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.telefonoOtro, 'is-not-empty': vm.telefonoOtro, 'with-error': !!vm.telefonoOtro && vm.form.telefonoOtro.$invalid}]" style="top: -10px;">
                    <label class="control-label">{{ __('common.telefono_otro') }}</label>
                    <input ng-model="vm.telefonoOtro"  ng-model-options="{allowInvalid: true }" name="telefonoOtro" ng-pattern="/^[0-9]{7,15}$/" maxlength="15" class="form-control">
                    <span class="help-block show" ng-show="!!vm.telefonoOtro && vm.form.telefonoOtro.$invalid">
                        <span ng-show="vm.form.telefonoOtro.$error.pattern">Ingrese un teléfono válido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.correo, 'is-not-empty': vm.correo}]" style="top: -10px;">
                    <label class="control-label">{{ __('common.correo_electronico') }}</label>
                    <input ng-model="vm.correo" email class="form-control">
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="row" style="padding-bottom: 10px;">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <span>{{ __('common.con_act_dig') }}</span>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="vm.actuacionDigital" ng-value="true">
                                {{ __('common.si') }}
                            </label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="vm.actuacionDigital" ng-value="false">
                                {{ __('common.no') }}
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="row" style="padding-bottom: 10px;">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <span>{{ __('common.con_vig_proc') }}</span>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="vm.vigilanciaProceso" ng-value="true">
                                {{ __('common.si') }}
                            </label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="vm.vigilanciaProceso" ng-value="false">
                                {{ __('common.no') }}
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="row" style="padding-bottom: 10px;">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <span>{{ __('common.con_dataprocesos') }}</span>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="vm.dataproceso" ng-value="true">
                                {{ __('common.si') }}
                            </label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="vm.dataproceso" ng-value="false">
                                {{ __('common.no') }}
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="top: -23px;">
                <div  ng-class="['form-group label-floating', {'is-empty': !vm.observacion, 'is-not-empty': vm.observacion}]">
                    <label class="control-label">{{ __('common.observacion') }}</label>
                    <textarea ng-model="vm.observacion" name="observacion" class="form-control" type="text" rows="1"></textarea>
                </div>
            </div>
            <div ng-class="['col-xs-12', {'col-sm-3 col-md-3 col-lg-3': !vm.id, 'col-sm-6 col-md-6 col-lg-6': !!vm.id}]" style="top: -22px;">
                <div class="input-group">
                    <div ng-class="['form-group label-floating', {'is-empty': !vm.fechaVencimiento, 'is-not-empty': vm.fechaVencimiento}]">
                        <label class="control-label">Fecha Vencimiento</label>
                        <input datetimepicker datetimepicker-options="@{{ vm.datetimepickerOptions }}" ng-model="vm.fechaVencimiento" class="form-control" type="text"/>
                        <i class="input-group-addon fa fa-calendar en-modal"></i>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" ng-if="!vm.id" style="top: -22px;">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.clave, 'is-not-empty': vm.clave, 'with-error': vm.guardando && vm.form.clave.$invalid}]">
                    <label class="control-label">{{ __('common.contrasenia') }}</label>
                    <input ng-model="vm.clave" name="clave" ng-required="!vm.id" maxlength="191" class="form-control" type="text">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.clave.$invalid">
                        <span ng-show="vm.form.clave.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="top: -21px;">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <p style="margin-bottom: 0;">{{ __('common.estado') }}</p>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="vm.estado" ng-value="true">
                                {{ __('common.activo') }}
                            </label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="vm.estado" ng-value="false">
                                {{ __('common.inactivo') }}
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button class="btn btn-primary" type="button" ng-click="vm.guardar()" ng-disabled="vm.deshabilitarGuardado">{{ __('common.guardar') }}</button>
    <button class="btn btn-default" type="button" ng-click="vm.cancelar()">{{ __('common.cancelar') }}</button>
</div>
<script>$(document).ready(function(){$.material.init();});</script>


