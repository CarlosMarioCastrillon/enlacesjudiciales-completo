<div class="modal-header">
    <h4 class="card-title">{{ __('common.info_empresa') }}</h4>
</div>
<div class="modal-body">
    <div class="card-content">
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <strong class="control-label text-left">{{ __('common.tipo_documento') }}:</strong>
            </div>
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                <span class="control-label text-left">@{{vm.empresa.tipo_documento.codigo}}</span>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <strong class="control-label text-left">{{ __('common.documento') }}:</strong>
            </div>
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                <span class="control-label text-left">@{{vm.empresa.numero_documento}}</span>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <strong class="control-label text-left">{{ __('common.nombre_empresa') }}:</strong>
            </div>
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                <span class="control-label text-left">@{{vm.empresa.nombre}}</span>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <strong class="control-label text-left">{{ __('common.tipo_de_empresa') }}:</strong>
            </div>
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                <span class="control-label text-left">@{{vm.empresa.tipo_empresa.nombre}}</span>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <strong class="control-label text-left">{{ __('common.departamento') }}:</strong>
            </div>
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                <span class="control-label text-left">@{{vm.empresa.departamento.nombre}}</span>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <strong class="control-label text-left">{{ __('common.ciudad') }}:</strong>
            </div>
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                <span class="control-label text-left">@{{vm.empresa.ciudad.nombre}}</span>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <strong class="control-label text-left">{{ __('common.direccion') }}:</strong>
            </div>
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                <span class="control-label text-left">@{{vm.empresa.direccion}}</span>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <strong class="control-label text-left">{{ __('common.telefono_fijo') }}:</strong>
            </div>

            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                <span class="control-label text-left">@{{vm.empresa.telefono_uno}}</span>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <strong class="control-label text-left">{{ __('common.email') }}:</strong>
            </div>
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                <span class="control-label text-left">@{{vm.empresa.email}}</span>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <strong class="control-label text-left">{{ __('common.estado') }}:</strong>
            </div>
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7" ng-show="!!vm.empresa">
                <span ng-if="!!(+vm.empresa.estado)">{{ __('common.activo') }}</span>
                <span ng-if="!(+vm.empresa.estado)">{{ __('common.inactivo') }}</span>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <strong class="control-label text-left">{{ __('common.fecha_creacion') }}:</strong>
            </div>
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                <span class="control-label text-left">@{{vm.empresa.fecha_creacion.substr(0,10)}}</span>
            </div>
        </div>
    </div>
    <style>
        button {
            border-radius: 100%;
            background-color: #f8f8f8;
            padding: 0.4rem;
            border-color: gray;
            height: 24px;
            width: 24px;
            font-size: 12px;
            padding-top: 1px;
        }
        .glyphicon{
            width: 16px;
            height: 16px;
            line-height: 1.6rem;
            padding-left: 5px;
            bottom: 0px;
            margin-bottom: 3px;
            margin-right: 2px;
            margin-top: 1.4px;

        }
        .panel .panel-heading a[aria-expanded="true"]{
            color: black;
        }
        .panel .panel-heading a:hover{
            color: gray;
        }
    </style>
    <div uib-accordion-group is-open="vm.mostrarPlanes" class="panel-default"  aria-expanded="true" data-toggle="collapse" data-target="#ElementToExpandOnClick" class="clickable main-module-list">
        <uib-accordion-heading>
            <button ng-click="vm.mostrarPlanes = !vm.mostrarPlanes">
                <i class="glyphicon" ng-class="{'glyphicon-chevron-down': vm.mostrarPlanes, 'glyphicon-chevron-right': !vm.mostrarPlanes}"></i>
            </button>
            <span style="position: absolute; left: 60px; font-size: 16px;">Planes contratados</span>
        </uib-accordion-heading>
        <div class="card-content">
            <div class="table-responsive">
                <table class="table table-hover table-sm">
                    <thead>
                    <tr>
                        <th>{{ __('common.servicio') }}</th>
                        <th>{{ __('common.ciudad') }}</th>
                        <th>{{ __('common.cantidad') }}</th>
                        <th>{{ __('common.valor') }}</th>
                        <th class="text-center">{{ __('common.estado') }}</th>
                        <th>{{ __('common.creado_por') }}</th>
                        <th class="text-right">{{ __('common.fecha_creacion') }}</th>
                        <th>{{ __('common.modificado_por') }}</th>
                        <th class="text-right">{{ __('common.fecha_modificacion') }}</th>

                    </tr>
                    </thead>
                    <thead ng-show="vm.cargandoPlanes">
                    <tr>
                        <td colspan="11" class="text-center" style="padding: 0px;">
                            <div class="progress progress-striped active" style="margin: 0px;">
                                <div class="progress-bar" style="width: 100%"></div>
                            </div>
                        </td>
                    </tr>
                    </thead>
                    <tbody ng-show="!vm.cargandoPlanes">
                    <tr ng-repeat="row in vm.planes">
                        <td>@{{row.servicio_nombre}}</td>
                        <td>@{{row.ciudad_nombre}}</td>
                        <td>@{{row.cantidad}}</td>
                        <td>@{{row.valor}}</td>
                        <td class="text-center">
                            <span ng-if="!!(+row.estado)">{{ __('common.activo') }}</span>
                            <span ng-if="!(+row.estado)">{{ __('common.inactivo') }}</span>
                        </td>
                        <td>@{{ row.usuario_creacion_nombre }}</td>
                        <td class="text-right">@{{ row.fecha_creacion  }}</td>
                        <td>@{{ row.usuario_modificacion_nombre }}</td>
                        <td class="text-right">@{{ row.fecha_modificacion }}</td>
                    </tr>
                    </tbody>
                    <tfoot ng-if="!vm.cargandoPlanes && vm.coleccion.length > 0">
                    <script>$(document).ready(function(){$.material.ripples('table .btn');});</script>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <div uib-accordion-group is-open="vm.mostrarUsuarios" class="panel-default">
        <h5 class="card-title" ng-click=""></h5>
        <uib-accordion-heading>
            <button ng-click="vm.mostrarUsuarios = !vm.mostrarUsuarios">
                <i class="glyphicon" ng-class="{'glyphicon-chevron-down': vm.mostrarUsuarios, 'glyphicon-chevron-right': !vm.mostrarUsuarios}"></i>
            </button>
            <span style="position: absolute; left: 60px; font-size: 16px;">Usuarios Registrados</span>

        </uib-accordion-heading>
        <div class="card-content" ng-show="!!vm.mostrarUsuarios">
            <div class="table-responsive">
                <table class="table table-hover table-sm">
                    <thead>
                    <tr>
                        <th style="width: 82px;">{{ __('common.usuario') }}</th>
                        <th>{{ __('common.nombre') }}</th>
                        <th>{{ __('common.rol') }}</th>
                        <th class="text-right" style="width: 88px;">{{ __('common.vencimiento') }}</th>
{{--                        <th class="text-right" style="width: 88px;">{{ __('common.ultimo') }} <br> {{ __('common.ingreso') }}</th>--}}
                        <th class="text-center">{{ __('common.estado') }}</th>
                        <th>{{ __('common.creado_por') }}</th>
                        <th class="text-right">{{ __('common.fecha_creacion') }}</th>
                        <th style="width: 98px;">{{ __('common.modificado_por') }}</th>
                        <th class="text-right" style="width: 123px;">{{ __('common.fecha_modificacion') }}</th>
                    </tr>
                    </thead>
                    <thead ng-show="vm.cargandoUsuarios">
                    <tr>
                        <td colspan="11" class="text-center" style="padding: 0px;">
                            <div class="progress progress-striped active" style="margin: 0px;">
                                <div class="progress-bar" style="width: 100%"></div>
                            </div>
                        </td>
                    </tr>
                    </thead>
                    <tbody ng-show="!vm.cargandoUsuarios">
                    <tr ng-repeat="row in vm.usuarios">
                        <td>@{{ row.documento }}</td>
                        <td>@{{ row.nombre }}</td>
                        <td>@{{ row.rol }}</td>
                        <td class="text-right">@{{ row.fecha_vencimiento }}</td>
{{--                        <td class="text-right">@{{ row.ultimo_ingreso }}</td>--}}
                        <td class="text-center">
                            <span ng-if="!!(+row.estado)">{{ __('common.activo') }}</span>
                            <span ng-if="!(+row.estado)">{{ __('common.inactivo') }}</span>
                        </td>
                        <td>@{{ row.usuario_creacion_nombre }}</td>
                        <td class="text-right">@{{ row.fecha_creacion  }}</td>
                        <td>@{{ row.usuario_modificacion_nombre }}</td>
                        <td class="text-right">@{{ row.fecha_modificacion }}</td>
                    </tr>
                    </tbody>
                    <tfoot ng-if="!vm.cargandoUsuarios && vm.usuarios.length > 0">
                    <script>$(document).ready(function(){$.material.ripples('table .btn');});</script>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <div uib-accordion-group is-open="vm.mostrarAreas" class="panel-default">
        <h5 class="card-title" ng-click=""></h5>
        <uib-accordion-heading>
            <button ng-click="vm.mostrarAreas = !vm.mostrarAreas">
                <i class="glyphicon" ng-class="{'glyphicon-chevron-down': vm.mostrarAreas, 'glyphicon-chevron-right': !vm.mostrarAreas}"></i>
            </button>
            <span style="position: absolute; left: 60px; font-size: 16px;">{{ __('common.areas') }}</span>
        </uib-accordion-heading>
        <div class="card-content" ng-show="!!vm.mostrarAreas">
            <div class="table-responsive">
                <table class="table table-hover table-sm">
                    <thead>
                    <tr>
                        <th>{{ __('common.area') }}</th>
                        <th>{{ __('common.responsable') }}</th>
                        <th class="text-center">{{ __('common.estado') }}</th>
                        <th>{{ __('common.creado_por') }}</th>
                        <th class="text-right">{{ __('common.fecha_creacion') }}</th>
                        <th>{{ __('common.modificado_por') }}</th>
                        <th class="text-right">{{ __('common.fecha_modificacion') }}</th>
                    </tr>
                    </thead>
                    <thead ng-show="vm.cargandoAreas">
                    <tr>
                        <td colspan="8" class="text-center" style="padding: 0px;">
                            <div class="progress progress-striped active" style="margin: 0px;">
                                <div class="progress-bar" style="width: 100%"></div>
                            </div>
                        </td>
                    </tr>
                    </thead>
                    <tbody ng-show="!vm.cargandoAreas">
                    <tr ng-repeat="row in vm.areas">
                        <td>@{{row.nombre}}</td>
                        <td>@{{row.usuario_nombre}}</td>
                        <td class="text-center">
                            <span ng-if="!!(+row.estado)">{{ __('common.activo') }}</span>
                            <span ng-if="!(+row.estado)">{{ __('common.inactivo') }}</span>
                        </td>
                        <td>@{{ row.usuario_creacion_nombre }}</td>
                        <td class="text-right">@{{ row.fecha_creacion  }}</td>
                        <td>@{{ row.usuario_modificacion_nombre }}</td>
                        <td class="text-right">@{{ row.fecha_modificacion }}</td>
                    </tr>
                    </tbody>
                    <tfoot ng-if="!vm.cargandoAreas && vm.areas.length > 0">
                    <script>$(document).ready(function(){$.material.ripples('table .btn');});</script>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

</div>
<script>$(document).ready(function(){$.material.init();});</script>
