<div class="modal-header">
    <h3 class="modal-title">Cambio contraseña</h3>
</div>
<div class="modal-body">
    <form name="vm.form" autocomplete="off" novalidate>
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.usuario.empresaNombre, 'is-not-empty': vm.usuario.empresaNombre}]">
                    <label class="control-label">{{ __('common.empresa') }}</label>
                    <input ng-model="vm.usuario.empresaNombre" class="form-control" type="text" disabled>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.usuario.documento, 'is-not-empty': vm.usuario.documento}]">
                    <label class="control-label">{{ __('common.documento') }}</label>
                    <input ng-model="vm.usuario.documento" class="form-control" type="text" disabled>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.usuario.nombre, 'is-not-empty': vm.usuario.nombre}]">
                    <label class="control-label">{{ __('common.nombre') }}</label>
                    <input ng-model="vm.usuario.nombre" class="form-control" type="text" disabled>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.usuario.rolNombre, 'is-not-empty': vm.usuario.rolNombre}]">
                    <label class="control-label">{{ __('common.rol') }}</label>
                    <input ng-model="vm.usuario.rolNombre" class="form-control" type="text" disabled>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.usuario.nuevaClave, 'is-not-empty': vm.usuario.nuevaClave, 'with-error': vm.guardando && vm.form.nuevaClave.$invalid}]">
                    <label class="control-label">Nueva contraseña</label>
                    <input name="nuevaClave" required ng-model="vm.usuario.nuevaClave" class="form-control" type="password" maxlength="50">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.nuevaClave.$invalid">
                   <span ng-show="vm.form.nuevaClave.$error.required">Requerido</span>
                </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.usuario.confirmacionClave, 'is-not-empty': vm.usuario.confirmacionClave, 'with-error': vm.guardando && vm.form.confirmacionClave.$invalid}]">
                    <label class="control-label">Confirmar contraseña</label>
                    <input name="confirmacionClave" required ng-model="vm.usuario.confirmacionClave" class="form-control" type="password" maxlength="50">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.nuevaClave.$invalid">
                   <span ng-show="vm.form.confirmacionClave.$error.required">Requerido</span>
                </span>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button class="btn btn-primary" type="button" ng-click="vm.cambiarClave()">Guardar</button>
    <button class="btn btn-default" type="button" ng-click="vm.cancelar()">No</button>
</div>
<script>$(document).ready(function(){$.material.init();});</script>
