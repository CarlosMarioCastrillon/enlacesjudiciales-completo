<div id="rol" class="row" st-table="vm.coleccion" st-pipe="vm.obtenerResumen" st-persist="resumenRol" st-reset="isReset" refresh-table ng-hide="">
    <div class="card main-card" style="margin-bottom: 0;">
        <div class="card-content">
            <h4 class="card-title" ng-show="vm.usuarioCliente">{{ __('common.usuarios_clientes') }}</h4>
            @can('CrearUsuario'|'CrearUsuarioCliente')
                <a class="btn btn-primary btn-round btn-fab nex-btn-add" title="{{ __('common.nuevo_usuario') }}" ng-click="vm.crear()">
                    <i class="material-icons">add</i>
                </a>
            @endcan
            <div ng-hide="vm.usuarioCliente" class="row" style="border-color: #0f0f0f">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group label-floating">
                        <label class="control-label">{{ __('common.nombre') }}</label>
                        <input st-search="nombre" ng-model="vm.nombre" class="form-control" type="text"/>
                    </div>
                </div>
            </div>
            <div>
                <br>
                <div class="row">
                    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                        <strong class="control-label text-left">{{ __('common.identificacion') }}:</strong>
                    </div>
                    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                        <span class="control-label text-left">@{{vm.empresa.numero_documento}}</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                        <strong class="control-label text-left">{{ __('common.nombre_empresa') }}:</strong>
                    </div>
                    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                        <span class="control-label text-left">@{{vm.empresa.nombre}}</span>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row" style="margin: 0;" ng-if="!vm.cargando && vm.coleccion.length > 0">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 hidden-xs text-left" style="padding: 0;">
                    <nav>
                        <ul class="pagination pagination-primary">
                            <li class="text-left">
                                <a style="padding: 0 5px;" href="" type="button" rel="tooltip" title="{{ __('common.limpiar_filtros') }}" ng-click="vm.limpiarFiltros()">
                                    <i class="material-icons">clear_all</i>
                                </a>
                            </li>
                            <li class="text-left">
                                <span style="text-transform: none; font-size: 14px;">
                                    @{{ 'Mostrando ' + vm.paginacion.desde + ' a ' + vm.paginacion.hasta + ' de ' + vm.paginacion.total + ' resultados - Página ' + vm.paginacion.paginaActual + ' de ' + vm.paginacion.ultimaPagina }}
                                </span>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding: 0;">
                    <div class="text-right" st-pagination="" st-items-by-page="vm.itemsPorPagina" st-displayed-pages="5" st-template="/paginacion-template" style="float: right;"></div>
                    <div class="text-right" style="float: right; margin: 26px 25px 26px 0;">
                        <label style="color: rgba(0,0,0,0.68);">
                            Filas:
                            <select ng-options="option for option in [5, 10, 25, 50, 100, 500] track by option" ng-model="vm.itemsPorPagina" class="select-grid"></select>
                        </label>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-hover table-sm">
                    <thead>
                    <tr>
                        <th ng-if="vm.usuarioCliente">{{ __('common.tipo') }}<br>{{ __('common.docto') }}</th>
                        <th ng-if="vm.usuarioCliente" style="width: 82px;">{{ __('common.numero') }}<br>{{ __('common.Documento') }}</th>
                        <th>{{ __('common.nombre') }}</th>
                        <th ng-if="!vm.usuarioCliente">{{ __('common.identificacion') }}</th>
                        <th ng-if="!vm.usuarioCliente">{{ __('common.empresa') }}</th>
                        <th>
                            <span ng-if="!vm.usuarioCliente">{{ __('common.email') }}</span>
                            <span ng-if="vm.usuarioCliente">{{ __('common.correo_electronico') }}</span>
                        </th>
                        <th class="text-right" style="width: 88px;">{{ __('common.vencimiento') }}</th>
                        <th class="text-center">{{ __('common.estado') }}</th>
                        <th>{{ __('common.creado_por') }}</th>
                        <th class="text-right" ng-if="!vm.usuarioCliente">{{ __('common.fecha_creacion') }}</th>
                        <th style="width: 98px;">{{ __('common.modificado_por') }}</th>
                        <th class="text-right" style="width: 123px;">{{ __('common.fecha_modificacion') }}</th>
                        @if(auth()->user()->can('ModificarUsuario') || auth()->user()->can('EliminarUsuario')
                        || auth()->user()->can('ModificarUsuarioCliente') || auth()->user()->can('EliminarUsuarioCliente'))
                            <th class="text-right">{{ __('common.acciones') }}</th>
                        @endif
                    </tr>
                    </thead>
                    <thead ng-show="vm.cargando">
                    <tr>
                        <td colspan="11" class="text-center" style="padding: 0px;">
                            <div class="progress progress-striped active" style="margin: 0px;">
                                <div class="progress-bar" style="width: 100%"></div>
                            </div>
                        </td>
                    </tr>
                    </thead>
                    <tbody ng-show="!vm.cargando">
                    <tr ng-repeat="row in vm.coleccion">
                        <td ng-if="vm.usuarioCliente">@{{ row.tipo_documento }}</td>
                        <td ng-if="vm.usuarioCliente">@{{ row.documento }}</td>
                        <td>@{{ row.nombre }}</td>
                        <td ng-if="!vm.usuarioCliente">@{{ row.documento }}</td>
                        <td ng-if="!vm.usuarioCliente">@{{ row.empresa_nombre }}</td>
                        <td>@{{ row.email_uno }}</td>
                        <td class="text-right">@{{ row.fecha_vencimiento }}</td>
                        <td class="text-center">
                            <span class="label label-success" ng-if="!!(+row.estado)">{{ __('common.activo') }}</span>
                            <span class="label label-danger" ng-if="!(+row.estado)">{{ __('common.inactivo') }}</span>
                        </td>
                        <td>@{{ row.usuario_creacion_nombre }}</td>
                        <td class="text-right" ng-if="!vm.usuarioCliente">@{{ row.fecha_creacion  }}</td>
                        <td>@{{ row.usuario_modificacion_nombre }}</td>
                        <td class="text-right">@{{ row.fecha_modificacion }}</td>
                        @if(auth()->user()->can('ModificarUsuario') || auth()->user()->can('EliminarUsuario')
                        || auth()->user()->can('ModificarUsuarioCliente') || auth()->user()->can('EliminarUsuarioCliente'))
                            <td class="td-actions text-right">
                                {{--<a href="#/roles/@{{row.id}}/permisos" type="button" rel="tooltip" class="btn btn-primary btn-simple" title="{{ __('common.permisos') }}" ng-click="vm.asignarPermisos(row)">
                                    <i class="material-icons">lock_open</i>
                                </a>--}}
                                @can('ModificarUsuario' | 'ModificarUsuarioCliente')
                                    <a type="button" rel="tooltip" class="btn btn-primary btn-simple" title="{{ __('common.modificar') }}" ng-click="vm.crear(row)">
                                        <i class="material-icons">mode_edit</i>
                                    </a>
                                @endcan
                                @can('ModificarUsuarioCliente')
                                    <a ng-show="vm.usuarioCliente" type="button" rel="tooltip" class="btn btn-primary btn-simple" title="{{ __('common.procesos') }}" href="#/usuarios-clientes/@{{ row.id }}/procesos-cliente">
                                        <i class="material-icons">list</i>
                                    </a>
                                    <a ng-show="vm.usuarioCliente" type="button" rel="tooltip" class="btn btn-primary btn-simple" title="{{ __('common.clave') }}" ng-click="">
                                        <i class="material-icons">lock_open</i>
                                    </a>
                                @endcan
                                @can('EliminarUsuario')
                                    <a type="button" rel="tooltip" class="btn btn-danger btn-simple" title="{{ __('common.eliminar') }}" ng-click="vm.confirmarEliminar(row)">
                                        <i class="material-icons">delete</i>
                                    </a>
                                @endcan
                            </td>
                        @endif
                    </tr>
                    </tbody>
                    <tfoot ng-if="!vm.cargando && vm.coleccion.length > 0">
                    <script>$(document).ready(function(){$.material.ripples('table .btn');});</script>
                    </tfoot>
                </table>
            </div>
            <div class="row" style="margin: 0;" ng-show="!vm.cargando && vm.coleccion.length == 0">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 0;">
                    <nav>
                        <ul class="pagination pagination-primary">
                            <li class="text-left">
                                <a style="padding: 0 5px;" href="" type="button" rel="tooltip" title="{{ __('common.limpiar_filtros') }}" ng-click="vm.limpiarFiltros()">
                                    <i class="material-icons">clear_all</i>
                                </a>
                                <span style="text-transform: none; font-size: 14px;">{{ __('common.sin_resultados') }}</span>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="row" style="margin: 0;" ng-if="!vm.cargando && vm.coleccion.length > 0">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 hidden-xs text-left" style="padding: 0;">
                    <nav>
                        <ul class="pagination pagination-primary">
                            <li class="text-left">
                                <a style="padding: 0 5px;" href="" type="button" rel="tooltip" title="{{ __('common.limpiar_filtros') }}" ng-click="vm.limpiarFiltros()">
                                    <i class="material-icons">clear_all</i>
                                </a>
                            </li>
                            <li class="text-left">
                                <span style="text-transform: none; font-size: 14px;">
                                    @{{ 'Mostrando ' + vm.paginacion.desde + ' a ' + vm.paginacion.hasta + ' de ' + vm.paginacion.total + ' resultados - Página ' + vm.paginacion.paginaActual + ' de ' + vm.paginacion.ultimaPagina }}
                                </span>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding: 0;">
                    <div class="text-right" st-pagination="" st-items-by-page="vm.itemsPorPagina" st-displayed-pages="5" st-template="/paginacion-template" style="float: right;"></div>
                    <div class="text-right" style="float: right; margin: 26px 25px 26px 0;">
                        <label style="color: rgba(0,0,0,0.68);">
                            Filas:
                            <select ng-options="option for option in [5, 10, 25, 50, 100, 500] track by option" ng-model="vm.itemsPorPagina" class="select-grid"></select>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>$(document).ready(function(){$.material.init();});</script>
</div>
