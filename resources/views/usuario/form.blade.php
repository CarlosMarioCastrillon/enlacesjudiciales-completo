<div class="modal-header">
    <h4 class="card-title">{{ __('common.datos_usuario') }}</h4>
</div>
<div class="modal-body">
    <form name="vm.form" autocomplete="off" novalidate>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.empresaId, 'is-not-empty': vm.empresaId,
                'with-error': vm.guardando && vm.form.empresaId.$invalid, 'disabled': vm.deshabilitarEmpresaId}]">
                    <label class="control-label">{{ __('common.empresa') }}</label>
                    <select selectize="vm.configEmpresa" options="vm.opcionesEmpresa" disabled-input="vm.deshabilitarEmpresaId" ng-model="vm.empresaId"
                            name="empresaId" required class="form-control"></select>
                    <span class="help-block show" ng-show="vm.guardando && vm.form.empresaId.$invalid">
                        <span ng-show="vm.form.empresaId.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
            <div ng-class="['col-xs-12', {'col-sm-6 col-md-6 col-lg-6': !vm.id, 'col-sm-12 col-md-12 col-lg-12': !!vm.id}]">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.nombre, 'is-not-empty': vm.nombre, 'with-error': vm.guardando && vm.form.nombre.$invalid}]">
                    <label class="control-label">{{ __('common.nombre') }}</label>
                    <input ng-model="vm.nombre" name="nombre" required maxlength="191" class="form-control" type="text">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.nombre.$invalid">
                        <span ng-show="vm.form.nombre.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" ng-if="!vm.id">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.clave, 'is-not-empty': vm.clave,
                'with-error': vm.guardando && vm.form.clave.$invalid}]">
                    <label class="control-label">{{ __('common.contrasenia') }}</label>
                    <input ng-model="vm.clave" name="clave" ng-required="!vm.id" maxlength="191" class="form-control" type="text">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.clave.$invalid">
                        <span ng-show="vm.form.clave.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.tipoDocumentoId, 'is-not-empty': vm.tipoDocumentoId,
                'with-error': vm.guardando && vm.form.tipoDocumentoId.$invalid}]">
                    <label class="control-label">{{ __('common.tipo_documento') }}</label>
                    <select type="text" selectize="vm.configTipoDocumento" options="vm.opcionesTipoDocumento"
                            ng-model="vm.tipoDocumentoId" name="tipoDocumentoId" required class="form-control"></select>
                    <span class="help-block show" ng-show="vm.guardando && vm.form.tipoDocumentoId.$invalid">
                        <span ng-show="vm.form.tipoDocumentoId.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.documento, 'is-not-empty': vm.documento,
                'with-error': vm.guardando && vm.form.documento.$invalid}]">
                    <label class="control-label">{{ __('common.documento_de_identificacion') }}</label>
                    <input ng-model="vm.documento" name="documento" required maxlength="191" class="form-control" type="text">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.documento.$invalid">
                        <span ng-show="vm.form.documento.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.emailUno, 'is-not-empty': !!vm.emailUno,
                'with-error': vm.guardando && vm.form.emailUno.$invalid}]">
                    <label class="control-label">{{ __('common.email_uno') }}</label>
                    <input ng-model="vm.emailUno" name="emailUno" required maxlength="191" class="form-control" type="email">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.emailUno.$invalid">
                        <span ng-show="vm.form.emailUno.$error.required">Requerido</span>
                        <span ng-show="vm.form.emailUno.$error.email">Email invalido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" ng-hide="!!vm.usuarioCliente">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.emailDos, 'is-not-empty': !!vm.emailDos,
                'with-error': vm.guardando && vm.form.emailDos.$invalid}]">
                    <label class="control-label">{{ __('common.email_dos') }}</label>
                    <input ng-model="vm.emailDos" name="emailDos" maxlength="191" class="form-control" type="email">
                    <span class="help-block show" ng-show="vm.guardando && vm.form.emailDos.$invalid">
                        <span ng-show="vm.form.emailDos.$error.email">Email invalido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.rolId, 'is-not-empty': vm.rolId,
                'with-error': vm.guardando && vm.form.rolId.$invalid, 'disabled': vm.deshabilitarRolId}]">
                    <label class="control-label">{{ __('common.rol') }}</label>
                    <select selectize="vm.configRol" options="vm.opcionesRol" disabled-input="vm.deshabilitarRolId"
                            ng-model="vm.rolId" name="rolId" required class="form-control"></select>
                    <span class="help-block show" ng-show="vm.guardando && vm.form.rolId.$invalid">
                        <span ng-show="vm.form.rolId.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="input-group">
                    <div ng-class="['form-group label-floating', {'is-empty': !vm.fechaVencimiento, 'is-not-empty': vm.fechaVencimiento}]">
                        <label class="control-label">{{ __('common.fecha_vencimiento') }}</label>
                        <input datetimepicker-st-table datetimepicker-options="@{{ vm.formatoFechaVencimiento }}"
                               ng-model="vm.fechaVencimiento" class="form-control" type="text"/>
                        <i class="input-group-addon fa fa-calendar en-modal"></i>
                    </div>
                </div>
            </div>
            @if($rol->id == $SUPERUSUARIO_ROL_ID)
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" ng-hide="!!vm.usuarioCliente">
                    <div ng-class="['form-group label-floating', {'is-empty': !vm.departamentoId, 'is-not-empty': vm.departamentoId}]">
                        <label class="control-label">{{ __('common.departamento') }}</label>
                        <select selectize="vm.configDepartamento" options="vm.opcionesDepartamento"
                                ng-model="vm.departamentoId" name="departamentoId" class="form-control"></select>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" ng-hide="!!vm.usuarioCliente">
                    <div ng-class="['form-group label-floating', {'is-empty': !vm.ciudadId, 'is-not-empty': vm.ciudadId}]">
                        <label class="control-label">{{ __('common.municipio') }}</label>
                        <select selectize="vm.configCiudad" options="vm.opcionesCiudad" ng-model="vm.ciudadId"
                                name="ciudadId" class="form-control" clear-options="vm.limpiarOpcionesCiudad"></select>
                    </div>
                </div>
            @endif
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" ng-hide="!!vm.usuarioCliente">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.areaId, 'is-not-empty': vm.areaId}]">
                    <label class="control-label">{{ __('common.area') }}</label>
                    <select selectize="vm.configArea" options="vm.opcionesArea" ng-model="vm.areaId" name="areaId" class="form-control"
                            clear-options="vm.limpiarOpcionesArea"></select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <p style="margin-bottom: 0;">{{ __('common.estado') }}</p>
                    </div>
                    <div ng-class="['col-xs-12', {'col-sm-4 col-md-4 col-lg-4':!vm.usuarioCliente, 'col-sm-8 col-md-8 col-lg-8':!!vm.usuarioCliente}]">
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="vm.estado" ng-value="true">
                                {{ __('common.activo') }}
                            </label>
                        </div>
                    </div>
                    <div ng-class="['col-xs-12', {'col-sm-8 col-md-8 col-lg-8':!vm.usuarioCliente, 'col-sm-4 col-md-4 col-lg-4':!!vm.usuarioCliente}]">
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="vm.estado" ng-value="false">
                                {{ __('common.inactivo') }}
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4" ng-hide="!!vm.usuarioCliente">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <p style="margin-bottom: 0;">{{ __('common.dependiente_principal') }}</p>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="vm.dependientePrincipal" ng-value="false">
                                {{ __('common.no') }}
                            </label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="vm.dependientePrincipal" ng-value="true">
                                {{ __('common.si') }}
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4" ng-hide="!!vm.usuarioCliente">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <p style="margin-bottom: 0;">{{ __('common.microportal') }}</p>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="vm.conMicroportal" ng-value="false">
                                {{ __('common.no') }}
                            </label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="vm.conMicroportal" ng-value="true">
                                {{ __('common.si') }}
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" ng-hide="!!vm.usuarioCliente">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <p>
                    <strong>{{ __('common.alertas_email_uno') }}</strong>
                </p>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="checkbox" style="margin: 0;">
                            <label style="color: black;">
                                <input type="checkbox" ng-model="vm.conMovimientoProceso"> {{ __('common.movimiento_proceso') }}
                            </label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="checkbox" style="margin: 0;">
                            <label style="color: black;">
                                <input type="checkbox" ng-model="vm.conMontajeActuacion"> {{ __('common.montaje_actuacion') }}
                            </label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="checkbox" style="margin: 0;">
                            <label style="color: black;">
                                <input type="checkbox" ng-model="vm.conTarea"> {{ __('common.tarea') }}
                            </label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="checkbox" style="margin: 0;">
                            <label style="color: black;">
                                <input type="checkbox" ng-model="vm.conRespuestaTarea"> {{ __('common.respuesta_de_tarea') }}
                            </label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="checkbox" style="margin: 0;">
                            <label style="color: black;">
                                <input type="checkbox" ng-model="vm.conVencimientoTerminoTarea"> {{ __('common.vencimiento_termino_tarea') }}
                            </label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="checkbox" style="margin: 0;">
                            <label style="color: black;">
                                <input type="checkbox" ng-model="vm.conVencimientoTerminoActuacionRama"> {{ __('common.vencimiento_termino_actuacion_rm') }}
                            </label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="checkbox" style="margin: 0;">
                            <label style="color: black;">
                                <input type="checkbox" ng-model="vm.conVencimientoTerminoActuacionArchivo"> {{ __('common.vencimiento_termino_actuacion_plano') }}
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <p>
                    <strong>{{ __('common.alertas_email_dos') }}</strong>
                </p>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="checkbox" style="margin: 0;">
                            <label style="color: black;">
                                <input type="checkbox" ng-model="vm.conMovimientoProcesoDos"> {{ __('common.movimiento_proceso') }}
                            </label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="checkbox" style="margin: 0;">
                            <label style="color: black;">
                                <input type="checkbox" ng-model="vm.conMontajeActuacionDos"> {{ __('common.montaje_actuacion') }}
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button class="btn btn-primary" type="button" ng-click="vm.guardar()">{{ __('common.guardar') }}</button>
    <button class="btn btn-default" type="button" ng-click="vm.cancelar()">{{ __('common.cancelar') }}</button>
</div>
<script>$(document).ready(function(){$.material.init();});</script>
