<div id="diligencia-create" class="row">
    <div class="card main-card">
        <div class="card-content">
            <h4 class="card-title" style="margin-bottom: 25px;">{{ __('common.datos_diligencia') }}</h4>
            <div class="card-body">
                <form name="vm.form" autocomplete="off" novalidate>
                    <div class="row">
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" ng-show="vm.diligenciaTramite">
                                    <div ng-class="['form-group label-floating', {'is-empty': !vm.procesoId, 'is-not-empty': vm.procesoId}]">
                                        <label class="control-label">{{ __('common.Diligencia') }}</label>
                                        <input ng-model="vm.numeroDiligencia" name="numeroDiligencia" ng-disabled="vm.deshabilitarNumeroDiligencia" class="form-control" type="text">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div ng-class="['form-group label-floating', {'is-empty': !vm.empresaId, 'is-not-empty': vm.empresaId, 'with-error': vm.guardando && vm.form.empresa.$invalid, 'disabled': vm.deshabilitarEmpresaId}]">
                                        <label class="control-label">{{ __('common.empresa') }}</label>
                                        <select selectize="vm.configEmpresa" options="vm.opcionesEmpresas" disabled-input="vm.deshabilitarEmpresaId" ng-model="vm.empresaId" name="empresa" required class="form-control"></select>
                                        <span class="help-block show" ng-show="vm.guardando && vm.form.empresa.$invalid">
                                            <span ng-show="vm.form.empresa.$error.required">Requerido</span>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div ng-class="['form-group label-floating', {'is-empty': !vm.procesoId, 'is-not-empty': vm.procesoId, 'with-error': vm.guardando && vm.form.proceso.$invalid, 'disabled': vm.deshabilitarProcesoId}]">
                                        <label class="control-label">{{ __('common.proceso_') }}</label>
                                        <select selectize="vm.configProceso" options="vm.opcionesProcesos" disabled-input="vm.deshabilitarProcesoId" clear-options="vm.limpiarOpcionesProcesos" ng-model="vm.procesoId" name="proceso" class="form-control" required></select>
                                        <span class="help-block show" ng-show="vm.guardando && vm.form.proceso.$invalid">
                                            <span ng-show="vm.form.proceso.$error.required">Requerido</span>
                                        </span>
                                        <span class="help-block show text-danger" ng-show="!!vm.message">@{{ vm.message }}</span>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" ng-hide="vm.diligenciaTramite">
                                    <div class="input-group">
                                        <div ng-class="['form-group label-floating', {'is-empty': !vm.fecha, 'is-not-empty': vm.fecha,
                                        'with-error': vm.guardando && vm.form.fecha.$invalid}]">
                                            <label class="control-label">{{ __('common.fecha') }}</label>
                                            <input datetimepicker datetimepicker-options="{useCurrent: false, format: 'YYYY-MM-DD'}" ng-disabled="vm.deshabilitarFecha" ng-model="vm.fecha" name="fecha" class="form-control" required type="text"/>
                                            <i class="input-group-addon fa fa-calendar"></i>
                                            <span class="help-block show" ng-show="vm.guardando && vm.form.fecha.$invalid">
                                                <span ng-show="vm.form.fecha.$error.required">Requerido</span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8" style="margin-bottom: -30px;">
                            <div class="card">
                                <div class="card-content">
                                    <style>
                                        hr {
                                            margin: 9px 0 0 10px;
                                        }
                                    </style>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 text-right">
                                            <strong class="text-left">{{ __('common.clase_proceso') }}:</strong>
                                        </div>
                                        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                            <span class="text-left">@{{vm.proceso.clase_proceso.nombre}} &nbsp;</span>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 text-right">
                                            <strong class="text-left">{{ __('common.demandante') }}:</strong>
                                        </div>
                                        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                                            <span class="text-left">@{{vm.proceso.demandante}} &nbsp;</span>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 text-right">
                                            <strong class="text-left">{{ __('common.demandado') }}:</strong>
                                        </div>
                                        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                                            <span class="text-left">@{{vm.proceso.demandado}} &nbsp</span>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 text-right">
                                            <strong class="control-label text-left">{{ __('common.juzgado') }}:</strong>
                                        </div>
                                        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                                            <span class="control-label text-left">@{{vm.proceso.juzgado.nombre}} &nbsp;</span>
                                        </div>
                                    </div>
                                    <hr>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div ng-class="{row:vm.diligenciaTramite}">
                        <div ng-class="{row:!vm.diligenciaTramite}">
                            <div ng-class="['col-xs-12 ', {'col-sm-6 col-md-6 col-lg-6': vm.diligenciaTramite, 'col-sm-4 col-md-4 col-lg-4': !vm.diligenciaTramite}]">
                                <div ng-class="['form-group label-floating', {'is-empty': !vm.departamentoId, 'is-not-empty': vm.departamentoId, 'with-error': vm.guardando && vm.form.departamento.$invalid, 'disabled': vm.deshabilitarDepartamentoId}]">
                                    <label class="control-label">{{ __('common.departamento') }}</label>
                                    <select selectize="vm.configDepartamento" options="vm.opcionesDepartamentos" disabled-input="vm.deshabilitarDepartamentoId" ng-model="vm.departamentoId" name="departamento" required class="form-control"></select>
                                    <span class="help-block show" ng-show="vm.guardando && vm.form.departamento.$invalid">
                                        <span ng-show="vm.form.departamento.$error.required">Requerido</span>
                                    </span>
                                </div>
                            </div>
                            <div  ng-class="['col-xs-12 ', {'col-sm-6 col-md-6 col-lg-6': vm.diligenciaTramite, 'col-sm-4 col-md-4 col-lg-4': !vm.diligenciaTramite}]">
                                <div ng-class="['form-group label-floating', {'is-empty': !vm.ciudadId, 'is-not-empty': vm.ciudadId, 'with-error': vm.guardando && vm.form.ciudad.$invalid, 'disabled': vm.deshabilitarCiudadId}]">
                                    <label class="control-label">{{ __('common.ciudad') }}</label>
                                    <select selectize="vm.configCiudad" options="vm.opcionesCiudad" disabled-input="vm.deshabilitarCiudadId" ng-model="vm.ciudadId" name="ciudad" required class="form-control" clear-options="vm.limpiarOpcionesCiudad"></select>
                                    <span class="help-block show" ng-show="vm.guardando && vm.form.ciudad.$invalid">
                                        <span ng-show="vm.form.ciudad.$error.required">Requerido</span>
                                    </span>
                                    <span class="help-block show" ng-show="!vm.departamentoId && vm.ciudadId">Debe seleccionar el departamento</span>
                                </div>
                            </div>
                            <div ng-class="['col-xs-12 ', {'col-sm-6 col-md-6 col-lg-6': vm.diligenciaTramite, 'col-sm-4 col-md-4 col-lg-4': !vm.diligenciaTramite}]">
                                <div ng-class="['form-group label-floating', {'is-empty': !vm.tipoDiligenciaId, 'is-not-empty': vm.tipoDiligenciaId, 'with-error': vm.guardando && vm.form.tipoDiligencia.$invalid, 'disabled': vm.deshabilitarTipoDiligenciaId}]">
                                    <label class="control-label">{{ __('common.tipo_diligencia') }}</label>
                                    <select selectize="vm.configTipoDiligencia" options="vm.opcionesTipoDiligencia" disabled-input="vm.deshabilitarTipoDiligenciaId" ng-model="vm.tipoDiligenciaId" name="tipoDiligencia" required class="form-control" clear-options="vm.limpiarOpcionesTipoDiligencia"></select>
                                    <span class="help-block show" ng-show="vm.guardando && vm.form.tipoDiligencia.$invalid">
                                        <span ng-show="vm.form.tipoDiligencia.$error.required">Requerido</span>
                                    </span>
                                </div>
                            </div>
                           {{-- <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" ng-if="vm.diligenciaTramite">
                                <div ng-class="['form-group label-floating', {'is-empty': !vm.otrosGastos || vm.valorDiligencia !== '0', 'is-not-empty': !!vm.otrosGastos || vm.otrosGastos == '0', 'with-error': vm.guardando && vm.form.otrosGastos.$invalid}]" style="padding-bottom: 30px;">
                                    <label class="control-label">{{ __('common.otros_gastos') }}</label>
                                    <input ng-model="vm.otrosGastos" name="otrosGastos" ng-disabled="vm.deshabilitarOtrosGastos" class="form-control" type="text"
                                           pattern="^\$\d{1,3}(,\d{3})*(\.\d+)?$" data-type="currency">
                                </div>
                            </div>--}}
                        </div>
                        <div ng-class="{row:!vm.diligenciaTramite}">
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4" ng-hide="vm.diligenciaTramite">
                                <div ng-class="['form-group label-floating', {'is-empty': !vm.estadoDiligencia, 'is-not-empty': vm.estadoDiligencia,
                                'disabled': vm.deshabilitarEstadoDiligencia}]">
                                    <label class="control-label">{{ __('common.estado') }}</label>
                                    <select selectize="vm.configEstadoDiligencia" options="vm.opcionesEstadoDiligencia" disabled-input="vm.deshabilitarEstadoDiligencia" ng-model="vm.estadoDiligencia" class="form-control"></select>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4" ng-hide="vm.diligenciaTramite">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <p style="margin-bottom: 0;">{{ __('common.indicativo_cobro') }}</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" ng-model="vm.indicativoCobro" ng-value="true" ng-disabled="vm.deshabilitarIndicativoCobro">
                                                {{ __('common.si') }}
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" ng-model="vm.indicativoCobro" ng-value="false" ng-disabled="vm.deshabilitarIndicativoCobro">
                                                {{ __('common.no') }}
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4" ng-hide="vm.diligenciaTramite">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <p style="margin-bottom: 0;">{{ __('common.indicativo_autorizacion') }}</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" ng-model="vm.indicativoAutorizacion" ng-value="true" ng-disabled="vm.deshabilitarIndicativoAutorizacion">
                                                {{ __('common.si') }}
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" ng-model="vm.indicativoAutorizacion" ng-value="false" ng-disabled="vm.deshabilitarIndicativoAutorizacion">
                                                {{ __('common.no') }}
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div ng-class="{row:!vm.diligenciaTramite}">
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4" ng-hide="vm.diligenciaTramite">
                                <div ng-class="['form-group label-floating', {'is-empty': !vm.valorDiligencia || vm.valorDiligencia !== '0', 'is-not-empty': !!vm.valorDiligencia || vm.valorDiligencia === '0', 'with-error': vm.guardando && vm.valorDiligenicaInvalid}]">
                                    <label class="control-label">{{ __('common.valor') }} {{ __('common.diligencia') }}</label>
                                    <input ng-model="vm.valorDiligencia" name="valorDiligencia" ng-disabled="vm.deshabilitarValorDiligencia" class="form-control" type="text" pattern="^\$\d{1,3}(,\d{3})*(\.\d+)?$" data-type="currency">
                                    <span class="help-block show" ng-show="vm.guardando && vm.valorDiligenicaInvalid">
                                        <span>Debe ingresar un valor mayor a cero.</span>
                                    </span>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4" ng-hide="vm.diligenciaTramite">
                                <div ng-class="['form-group label-floating', {'is-empty': !vm.gastoEnvio || vm.gastoEnvio !== '0', 'is-not-empty': !!vm.gastoEnvio || vm.gastoEnvio === '0', 'with-error': vm.guardando && vm.form.gastoEnvio.$invalid}]">
                                    <label class="control-label">{{ __('common.gastos_envio') }}</label>
                                    <input ng-model="vm.gastoEnvio" name="gastoEnvio" ng-disabled="vm.deshabilitarGastoEnvio" class="form-control" type="text" pattern="^\$\d{1,3}(,\d{3})*(\.\d+)?$" data-type="currency">
                                </div>
                            </div>
                            <div ng-class="['col-xs-12',{'col-sm-4 col-md-4 col-lg-4':!vm.diligenciaTramite, 'col-sm-6 col-md-6 col-lg-6':vm.diligenciaTramite}]">
                                <div ng-class="['form-group label-floating', {'is-empty': !vm.otrosGastos || vm.otrosGastos !== '0', 'is-not-empty': !!vm.otrosGastos || vm.otrosGastos == '0', 'with-error': vm.guardando && vm.form.otrosGastos.$invalid}]" style="padding-bottom: 30px;">
                                    <label class="control-label">{{ __('common.otros_gastos') }}</label>
                                    <input ng-model="vm.otrosGastos" name="otrosGastos" ng-disabled="vm.deshabilitarOtrosGastos" class="form-control" type="text" pattern="^\$\d{1,3}(,\d{3})*(\.\d+)?$" data-type="currency">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4" ng-hide="vm.diligenciaTramite">
                                <div ng-class="['form-group label-floating', {'is-empty': !vm.costoDiligencia || vm.costoDiligencia !== '0', 'is-not-empty': !!vm.costoDiligencia || vm.costoDiligencia === '0', 'with-error': vm.guardando && vm.costoDiligenicaInvalid}]">
                                    <label class="control-label">{{ __('common.costo') }} {{ __('common.diligencia') }}</label>
                                    <input ng-model="vm.costoDiligencia" name="costoDiligencia" ng-disabled="vm.deshabilitarValorDiligencia" class="form-control" type="text" pattern="^\$\d{1,3}(,\d{3})*(\.\d+)?$" data-type="currency">
                                    <span class="help-block show" ng-show="vm.guardando && vm.costoDiligenicaInvalid">
                                        <span>Debe ingresar un valor mayor a cero.</span>
                                    </span>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4" ng-hide="vm.diligenciaTramite">
                                <div ng-class="['form-group label-floating', {'is-empty': !vm.costoEnvio || vm.costoEnvio !== '0', 'is-not-empty': !!vm.costoEnvio || vm.costoEnvio === '0', 'with-error': vm.guardando && vm.form.costoEnvio.$invalid}]">
                                    <label class="control-label">{{ __('common.costo_envio') }}</label>
                                    <input ng-model="vm.costoEnvio" name="costoEnvio" ng-disabled="vm.deshabilitarGastoEnvio" class="form-control" type="text" pattern="^\$\d{1,3}(,\d{3})*(\.\d+)?$" data-type="currency">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4" ng-hide="vm.diligenciaTramite">
                                <div ng-class="['form-group label-floating', {'is-empty': !vm.otrosCostos || vm.otrosCostos !== '0', 'is-not-empty': !!vm.otrosCostos || vm.otrosCostos == '0', 'with-error': vm.guardando && vm.form.otrosCostos.$invalid}]" style="padding-bottom: 30px;">
                                    <label class="control-label">{{ __('common.otros_costos') }}</label>
                                    <input ng-model="vm.otrosCostos" name="otrosCostos" ng-disabled="vm.deshabilitarOtrosGastos" class="form-control" type="text" pattern="^\$\d{1,3}(,\d{3})*(\.\d+)?$" data-type="currency">
                                </div>
                            </div>
                        </div>
                    <div class="row" ng-show="!!vm.id && vm.estadoDiligencia >= vm.EstadoDiligenciaEnum.TERMINADO||vm.diligenciaTramite">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div  ng-class="['form-group label-floating', {'is-empty': !vm.detalleValores, 'is-not-empty': !!vm.detalleValores}]" style="top: -10px;">
                                <label class="control-label">{{ __('common.detalle_valores') }}</label>
                                <textarea ng-model="vm.detalleValores" ng-disabled="vm.deshabilitarDetalleValores" class="form-control" type="text" maxlength="191" rows="1"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="top: -1px;">
                            <div  ng-class="['form-group label-floating', {'is-empty': !vm.observacionesCliente, 'is-not-empty': !!vm.observacionesCliente}]" style="top: -10px;">
                                <label class="control-label">{{ __('common.observaciones_cliente') }}</label>
                                <textarea ng-model="vm.observacionesCliente" ng-disabled="vm.deshabilitarObservacionesCliente" class="form-control" type="text" maxlength="191" rows="1"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div  ng-class="['form-group label-floating', {'is-empty': !vm.observacionesInternas, 'is-not-empty': !!vm.observacionesInternas}]" style="top: -10px;">
                                <label class="control-label">{{ __('common.observaciones_internas') }}</label>
                                <textarea ng-model="vm.observacionesInternas" ng-disabled="vm.deshabilitarObservacionesInternas" class="form-control" type="text" maxlength="191" rows="1"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row" ng-show="!!vm.id" ng-if="!vm.diligenciaTramite">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div ng-class="['form-group label-floating', {'is-empty': !vm.usuarioId, 'is-not-empty': vm.usuarioId, 'with-error': vm.guardando && vm.form.usuario.$invalid, 'disabled': vm.deshabilitarUsuarioId}]">
                                <label class="control-label">{{ __('common.dependiente') }}</label>
                                <select selectize="vm.configUsuario" options="vm.opcionesUsuario" disabled-input="vm.deshabilitarUsuarioId" ng-model="vm.usuarioId" name="usuarioId" ng-required="!!vm.id && !vm.deshabilitarUsuarioId" clear-options="vm.limpiarOpcionesUsuario" class="form-control"></select>
                                <span class="help-block show" ng-show="vm.guardando && vm.form.usuario.$invalid">
                                    <span ng-show="vm.form.usuario.$error.required">Requerido</span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="row" ng-if="!vm.diligenciaTramite">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label class="card-item-label">{{ __('common.anexo_diligencia') }}</label>
                            <div class="attachment-img-drop-box"
                                 ngf-drop="vm.cargarArchivo($file)"
                                 ngf-multiple="false"
                                 ngf-allow-dir="true"
                                 ngf-drag-over-class="'dragover'"
                                 ngf-pattern="'.doc,.docx,.xls,.xlsx,.ppt,.pptx,.txt,.odt,.ods,.odp,.pdf,image/*'"
                                 accept="'.doc,.docx,.xls,.xlsx,.ppt,.pptx,.txt,.odt,.ods,.odp,.pdf,image/*'"
                                 ngf-resize="{width: 250, height: 100, quality: 1}"
                                 ngf-drop-disabled="vm.deshabilitarAnexoDiligencia"
                                 style="padding: 20px 0 0 0; min-height: 100px;">
                                <div style="text-align: center;" ng-show="!vm.deshabilitarAnexoDiligencia">
                                    <p>Arrastre el archivo aquí o pulse para subir: <a style="cursor: pointer;" type="button" ngf-multiple="false"
                                                                                       ngf-select="vm.cargarArchivo($file)">Seleccione archivo para carga</a></p>
                                </div>
                                <div class="row" ng-show="vm.archivoNombre && !vm.mostrarAnexos">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                                         <span class="attachment-item-container">
                                            <span ng-show="vm.deshabilitarAnexoDiligencia" style="color: #AAAAAA;">Archivo adjunto: </span>
                                            <a ng-class="['attachment-item', {'deleted': vm.archivoEliminado}]"  ng-click="vm.descargarArchivo()">
                                                @{{ vm.archivoNombre }}
                                            </a>
                                            <a type="button" rel="tooltip" class="btn btn-xs btn-danger btn-simple attachment-item-btn-delete"
                                               title="Eliminar" ng-click="vm.eliminarArchivo()" ng-show="!vm.archivoEliminado && !vm.deshabilitarAnexoDiligencia">
                                                <i class="material-icons">delete</i>
                                            </a>
                                             <a type="button" rel="tooltip" class="btn btn-xs btn-primary btn-simple attachment-item-btn-restore"
                                                title="Restaurar" ng-click="vm.restaurarArchivo()" ng-show="!!vm.archivoEliminado && !vm.deshabilitarAnexoDiligencia">
                                                <i class="material-icons">restore</i>
                                            </a>
                                        </span>
                                    </div>
                                </div>
                                <div class="row" ng-show="!!vm.id && !vm.archivoNombre && !vm.mostrarAnexos">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                                        <span style="margin-top: 20px;">No hay archivos cargados.</span>
                                    </div>
                                </div>
                                <div class="row" ng-show="!!vm.id && vm.mostrarAnexos">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                                        <span style="color: #AAAAAA;">Archivo adjunto: </span>
                                        <a class="attachment-item"  ng-click="vm.descargarArchivo()" ng-show="vm.archivoNombre" style="cursor: pointer;">
                                            @{{ vm.archivoNombre }}
                                        </a>
                                        <span ng-show="!vm.archivoNombre">No hay archivo cargado.</span>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                                        <span style="color: #AAAAAA;">Anexo costos: </span>
                                        <a class="attachment-item"  ng-click="vm.descargarAnexoCostos()" ng-show="vm.anexoCostosNombre" style="cursor: pointer;">
                                            @{{ vm.anexoCostosNombre }}
                                        </a>
                                        <span ng-show="!vm.anexoCostosNombre">No hay archivo cargado.</span>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                                        <span style="color: #AAAAAA;">Anexo recibido: </span>
                                        <a class="attachment-item"  ng-click="vm.descargarAnexoRecibido()" ng-show="vm.anexoRecibidoNombre" style="cursor: pointer;">
                                            @{{ vm.anexoRecibidoNombre }}
                                        </a>
                                        <span ng-show="!vm.anexoRecibidoNombre">No hay archivo cargado.</span>
                                    </div>
                                </div>
                            </div>
                            <div ngf-no-file-drop>Arrastre de archivo no permitido en este navegador</div>
                        </div>
                    </div>
                    <div class="row" ng-if="vm.diligenciaTramite">
                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                <label class="card-item-label">{{ __('common.anexo_diligencia') }}:</label>
                            </div>
                            <div class="row" ng-show="vm.archivoNombre">
                                <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 text-left">
                                         <span>
                                            <a class="attachment-item" ng-click="vm.descargarArchivo()" style="cursor: pointer;">
                                                @{{ vm.archivoNombre }}
                                            </a>
                                            <a type="button" rel="tooltip" class="btn btn-xs btn-primary btn-simple attachment-item-btn-delete"
                                               title="Descargar"  ng-click="vm.descargarArchivo()">
                                                <i class="material-icons">get_app</i>
                                            </a>
                                        </span>
                                </div>
                            </div>
                    </div>
                    <div class="row" ng-if="vm.diligenciaTramite">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label class="card-item-label">{{ __('common.anexo_otros_gastos') }}</label>
                            <div class="attachment-img-drop-box"
                                 ngf-drop="vm.cargarArchivo($file)"
                                 ngf-multiple="false"
                                 ngf-allow-dir="true"
                                 ngf-drag-over-class="'dragover'"
                                 ngf-pattern="'.doc,.docx,.xls,.xlsx,.ppt,.pptx,.txt,.odt,.ods,.odp,.pdf,image/*'"
                                 accept="'.doc,.docx,.xls,.xlsx,.ppt,.pptx,.txt,.odt,.ods,.odp,.pdf,image/*'"
                                 ngf-resize="{width: 250, height: 100, quality: 1}"
                                 ngf-drop-disabled="vm.deshabilitarAnexoDiligencia"
                                 style="padding: 20px 0 0 0; min-height: 100px;">
                                <div style="text-align: center;" ng-show="!vm.deshabilitarAnexoDiligencia">
                                    <p>Arrastre el archivo aquí o pulse para subir: <a style="cursor: pointer;" type="button" ngf-multiple="false"
                                                                                       ngf-select="vm.cargarAnexoCostos($file)">Seleccione archivo para carga</a></p>
                                </div>
                                <div class="row" ng-show="vm.anexoCostosNombre">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                                        <span class="attachment-item-container">
                                            <a ng-class="['attachment-item', {'deleted': vm.anexoCostosEliminado}]"  ng-click="vm.descargarAnexoCostos()" style="cursor: pointer;">
                                                @{{ vm.anexoCostosNombre }}
                                            </a>
                                            <a type="button" rel="tooltip" class="btn btn-xs btn-danger btn-simple attachment-item-btn-delete"
                                               title="Eliminar" ng-click="vm.eliminarAnexoCostos()" ng-show="!vm.anexoCostosEliminado">
                                                <i class="material-icons">delete</i>
                                            </a>
                                            <a type="button" rel="tooltip" class="btn btn-xs btn-primary btn-simple attachment-item-btn-restore"
                                               title="Restaurar" ng-click="vm.restaurarAnexoCostos()" ng-show="!!vm.anexoCostosEliminado">
                                                <i class="material-icons">restore</i>
                                            </a>
                                        </span>
                                    </div>
                                </div>
                                <div class="row" ng-show="!!vm.id && !vm.anexoCostosNombre">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                                        <span style="margin-top: 20px;">No hay archivos cargados.</span>
                                    </div>
                                </div>
                            </div>
                            <div ngf-no-file-drop>Arrastre de archivo no permitido en este navegador</div>
                        </div>
                    </div>
                    <div class="row" ng-if="vm.diligenciaTramite">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <label class="card-item-label">{{ __('common.anexo_recibido') }}</label>
                            <div class="attachment-img-drop-box"
                                 ngf-drop="vm.cargarArchivo($file)"
                                 ngf-multiple="false"
                                 ngf-allow-dir="true"
                                 ngf-drag-over-class="'dragover'"
                                 ngf-pattern="'.doc,.docx,.xls,.xlsx,.ppt,.pptx,.txt,.odt,.ods,.odp,.pdf,image/*'"
                                 accept="'.doc,.docx,.xls,.xlsx,.ppt,.pptx,.txt,.odt,.ods,.odp,.pdf,image/*'"
                                 ngf-resize="{width: 250, height: 100, quality: 1}"
                                 ngf-drop-disabled="vm.deshabilitarAnexoDiligencia"
                                 style="padding: 20px 0 0 0; min-height: 100px;">
                                <div style="text-align: center;" ng-show="!vm.deshabilitarAnexoDiligencia">
                                    <p>Arrastre el archivo aquí o pulse para subir: <a style="cursor: pointer;" type="button" ngf-multiple="false"
                                                                                       ngf-select="vm.cargarAnexoRecibido($file)">Seleccione archivo para carga</a></p>
                                </div>
                                <div class="row" ng-show="vm.anexoRecibidoNombre">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                                        <span class="attachment-item-container">
                                            <a ng-class="['attachment-item', {'deleted': vm.anexoRecibidoEliminado}]"  ng-click="vm.descargarAnexoRecibido()">
                                                @{{ vm.anexoRecibidoNombre }}
                                            </a>
                                            <a type="button" rel="tooltip" class="btn btn-xs btn-danger btn-simple attachment-item-btn-delete"
                                               title="Eliminar" ng-click="vm.eliminarAnexoRecibido()" ng-show="!vm.anexoRecibidoEliminado">
                                                <i class="material-icons">delete</i>
                                            </a>
                                            <a type="button" rel="tooltip" class="btn btn-xs btn-primary btn-simple attachment-item-btn-restore"
                                               title="Restaurar" ng-click="vm.restaurarAnexoRecibido()" ng-show="!!vm.anexoRecibidoEliminado">
                                                <i class="material-icons">restore</i>
                                            </a>
                                        </span>
                                    </div>
                                </div>
                                <div class="row" ng-show="!!vm.id && !vm.anexoRecibidoNombre">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                                        <span style="margin-top: 20px;">No hay archivos cargados.</span>
                                    </div>
                                </div>
                            </div>
                            <div ngf-no-file-drop>Arrastre de archivo no permitido en este navegador</div>
                        </div>
                    </div>
                </form>
            </div>
            <footer class="text-right">
                <button class="btn btn-primary" type="button" ng-click="vm.guardar()" ng-disabled="vm.deshabilitarGuardado">{{ __('common.guardar') }}</button>
                <button class="btn btn-default" type="button" ng-click="vm.volver()" ng-disabled="vm.deshabilitarGuardado">{{ __('common.cancelar') }}</button>
            </footer>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){$.material.init();});
</script>
