<div id="diligencia_historia" class="row" st-table="vm.coleccion" st-pipe="vm.obtenerResumen" st-persist="resumenDiligenciaHistoria" st-reset="isReset" refresh-table>
    <div class="card main-card">
        <div class="card-content">
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <strong class="control-label text-left">{{ __('common.numero_diligencia') }}:</strong>
                </div>
                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                    <span class="control-label text-left">@{{vm.diligencia.id}}</span>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <strong class="control-label text-left">{{ __('common.empresa') }}:</strong>
                </div>
                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                    <span class="control-label text-left">@{{vm.diligencia.empresa.nombre}}</span>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <strong class="control-label text-left">{{ __('common.proceso_') }}:</strong>
                </div>
                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                    <span class="control-label text-left">@{{vm.diligencia.proceso_judicial.numero_proceso}}</span>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <strong class="control-label text-left">{{ __('common.demandante') }}:</strong>
                </div>
                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                    <span class="control-label text-left">@{{vm.diligencia.proceso_judicial.demandante}}</span>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <strong class="control-label text-left">{{ __('common.demandado') }}:</strong>
                </div>
                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                    <span class="control-label text-left">@{{vm.diligencia.proceso_judicial.demandado}}</span>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <strong class="control-label text-left">{{ __('common.juzgado') }}:</strong>
                </div>
                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                    <span class="control-label text-left">@{{vm.diligencia.proceso_judicial.juzgado}}</span>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <strong class="control-label text-left">{{ __('common.fecha_diligencia') }}:</strong>
                </div>
                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                    <span class="control-label text-left">@{{vm.diligencia.fecha_diligencia}}</span>
                </div>
            </div>
            <hr>
            <div class="row" style="margin: 0;" ng-if="!vm.cargando && vm.coleccion.length > 0">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 hidden-xs text-left" style="padding: 0;">
                    <nav>
                        <ul class="pagination pagination-primary">
                            <li class="text-left">
                                <a style="padding: 0 5px;" href="" type="button" rel="tooltip" title="{{ __('common.limpiar_filtros') }}" ng-click="vm.limpiarFiltros()">
                                    <i class="material-icons">clear_all</i>
                                </a>
                            </li>
                            <li class="text-left">
                                <span style="text-transform: none; font-size: 14px;">
                                    @{{ 'Mostrando ' + vm.paginacion.desde + ' a ' + vm.paginacion.hasta + ' de ' + vm.paginacion.total + ' resultados - Página ' + vm.paginacion.paginaActual + ' de ' + vm.paginacion.ultimaPagina }}
                                </span>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding: 0;">
                    <div class="text-right" st-pagination="" st-items-by-page="vm.itemsPorPagina" st-displayed-pages="5" st-template="/paginacion-template" style="float: right;"></div>
                    <div class="text-right" style="float: right; margin: 26px 25px 26px 0;">
                        <label style="color: rgba(0,0,0,0.68);">
                            Filas:
                            <select ng-options="option for option in [5, 10, 25, 50, 100, 500] track by option" ng-model="vm.itemsPorPagina" class="select-grid"></select>
                        </label>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-hover table-sm">
                    <thead>
                        <tr>
                            <th class="text-right" st-multi-sort="secuencia" style="width: 70px;">{{ __('common.secuencia') }}</th>
                            <th class="text-right" st-multi-sort="fecha_actualizacion" style="width: 70px;">{{ __('common.fecha') }} <br> {{ __('common.actualizacion') }}</th>
                            <th class="text-left" st-multi-sort="estado_diligencia" style="width: 70px;">{{ __('common.estado') }}</th>
                            <th class="text-right" st-multi-sort="valor_diligencia">{{ __('common.valor') }} <br> {{ __('common.Diligencia') }}</th>
                            <th class="text-right" st-multi-sort="gastos_envio">{{ __('common.gastos') }} <br> {{ __('common.envio') }}</th>
                            <th class="text-right" st-multi-sort="otros_gastos">{{ __('common.otros') }} <br> {{ __('common.gastos') }}</th>
                            <th class="text-left" st-multi-sort="tipo_diligencia">{{ __('common.tipo') }} <br> {{ __('common.Diligencia') }}</th>
                            <th class="text-left" st-multi-sort="observaciones">{{ __('common.observaciones') }}</th>
                            @if(auth()->user()->can('ModificarDiligencia') || auth()->user()->can('EliminarDiligencia'))
                                <th class="text-right" ng-hide="vm.consulta">{{ __('common.acciones') }}</th>
                            @endif
                        </tr>
                    </thead>
                    <thead ng-show="vm.cargando">
                        <tr>
                            <td colspan="11" class="text-center" style="padding: 0px;">
                                <div class="progress progress-striped active" style="margin: 0px;">
                                    <div class="progress-bar" style="width: 100%"></div>
                                </div>
                            </td>
                        </tr>
                    </thead>
                    <tbody ng-show="!vm.cargando">
                        <tr ng-repeat="row in vm.coleccion">
                            <td class="text-right">@{{row.secuencia}}</td>
                            <td class="text-right">@{{row.updated_at.substring(0,10)}}</td>
                            <td class="text-left">
                                <span ng-if="(+row.estado_diligencia) == vm.EstadoDiligenciaEnum.SOLICITADO">@{{ 'SOLICITADO' | translate }}</span>
                                <span ng-if="(+row.estado_diligencia) == vm.EstadoDiligenciaEnum.COTIZADO">@{{ 'COTIZADO' | translate }}</span>
                                <span ng-if="(+row.estado_diligencia) == vm.EstadoDiligenciaEnum.APROBADO">@{{ 'APROBADO' | translate }}</span>
                                <span ng-if="(+row.estado_diligencia) == vm.EstadoDiligenciaEnum.TRAMITE">@{{ 'TRAMITE' | translate }}</span>
                                <span ng-if="(+row.estado_diligencia) == vm.EstadoDiligenciaEnum.TERMINADO">@{{ 'TERMINADO' | translate }}</span>
                                <span ng-if="(+row.estado_diligencia) == vm.EstadoDiligenciaEnum.CONTABILIZADO">@{{ 'CONTABILIZADO' | translate }}</span>
                                <span ng-if="(+row.estado_diligencia) == vm.EstadoDiligenciaEnum.FACTURADO">@{{ 'FACTURADO' | translate }}</span>
                            </td>
                            <td class="text-right">@{{  row.valor_diligencia | currency:$:0 }}</td>
                            <td class="text-right">@{{  row.gastos_envio | currency:$:0 }}</td>
                            <td class="text-right">@{{  row.otros_gastos | currency:$:0 }}</td>
                            <td class="text-left">@{{row.tipo_diligencia.nombre}}</td>
                            <td class="text-left">@{{row.observaciones_cliente}}</td>
                            @if(auth()->user()->can('ListarDiligencia'))
                                <td class="td-actions text-right" ng-hide="vm.consulta">
                                @can('ListarDiligencia')
                                    <a type="button" rel="tooltip" class="btn btn-primary btn-simple" title="{{ __('common.detalle') }}" href="#/diligencias/@{{ row.diligencia_id }}/historia/@{{ row.id }}/formulario">
                                        <i class="fa fa-camera"></i>
                                    </a>
                                @endcan
                                </td>
                            @endif
                        </tr>
                    </tbody>
                    <tfoot ng-if="!vm.cargando && vm.coleccion.length > 0">
                        <script>$(document).ready(function(){$.material.ripples('table .btn');});</script>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <script>$(document).ready(function(){$.material.init();});</script>
</div>
