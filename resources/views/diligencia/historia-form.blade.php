<div id="diligencia-create" class="row">
    <div class="card main-card">
        <div class="card-content">
            <header>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <h4 class="card-title" style="margin-bottom: 25px;">{{ __('common.detalle_historia_diligencia') }}</h4>
                </div>
            </header>
            <style>
                hr {
                    margin-top: 4px;
                    margin-bottom: 8px;
                }
            </style>
            <div class="card-body">
                <form name="vm.form" autocomplete="off" novalidate>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="row">
                                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                    <strong class="control-label text-left">{{ __('common.numero_diligencia') }}:</strong>
                                </div>
                                <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                                    <span class="control-label text-left">@{{vm.numeroDiligencia}}</span>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                    <strong class="control-label text-left">{{ __('common.numero_secuencia') }}:</strong>
                                </div>
                                <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                                    <span class="control-label text-left">@{{vm.secuencia}}</span>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                    <strong class="control-label text-left">{{ __('common.empresa') }}:</strong>
                                </div>
                                <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                                    <span class="control-label text-left">@{{vm.empresa}}</span>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                    <strong class="control-label text-left">{{ __('common.proceso_') }}:</strong>
                                </div>
                                <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                                    <span class="control-label text-left">@{{vm.numeroProceso}}</span>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                    <strong class="text-left">{{ __('common.clase_proceso') }}:</strong>
                                </div>
                                <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                                    <span class="text-left">@{{vm.claseProceso}} &nbsp;</span>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                    <strong class="text-left">{{ __('common.demandante') }}:</strong>
                                </div>
                                <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                                    <span class="text-left">@{{vm.demandante}} &nbsp;</span>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                    <strong class="text-left">{{ __('common.demandado') }}:</strong>
                                </div>
                                <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                                    <span class="text-left">@{{vm.demandado}} &nbsp</span>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                    <strong class="control-label text-left">{{ __('common.juzgado') }}:</strong>
                                </div>
                                <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                                    <span class="control-label text-left">@{{vm.juzgado}} &nbsp;</span>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                    <strong class="control-label text-left">{{ __('common.departamento') }}:</strong>
                                </div>
                                <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                                    <span class="control-label text-left">@{{vm.departamento}} &nbsp;</span>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                    <strong class="control-label text-left">{{ __('common.ciudad') }}:</strong>
                                </div>
                                <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                                    <span class="control-label text-left">@{{vm.ciudad}} &nbsp;</span>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                    <strong class="control-label text-left">{{ __('common.fecha_diligencia') }}:</strong>
                                </div>
                                <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                                    <span class="control-label text-left">@{{vm.fechaDiligencia}} &nbsp;</span>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                               <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                   <strong class="control-label text-left">{{ __('common.tipo_diligencia') }}:</strong>
                               </div>
                               <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                                   <span class="control-label text-left">@{{vm.tipoDiligencia}} &nbsp;</span>
                               </div>
                           </div>
                            <hr>
                            <div class="row">
                                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                    <strong class="control-label text-left">{{ __('common.estado') }}:</strong>
                                </div>
                                <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                                    <span ng-if="(+vm.estadoDiligencia) == vm.EstadoDiligenciaEnum.SOLICITADO">@{{ 'SOLICITADO' | translate }}</span>
                                    <span ng-if="(+vm.estadoDiligencia) == vm.EstadoDiligenciaEnum.COTIZADO">@{{ 'COTIZADO' | translate }}</span>
                                    <span ng-if="(+vm.estadoDiligencia) == vm.EstadoDiligenciaEnum.APROBADO">@{{ 'APROBADO' | translate }}</span>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                    <strong class="control-label text-left">{{ __('common.fecha_actualizacion') }}:</strong>
                                </div>
                                <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                                    <span class="control-label text-left">@{{vm.fechaModificacion.substring(0,10)}} &nbsp;</span>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                    <strong class="control-label text-left">{{ __('common.solicitud_autorizacion') }}:</strong>
                                </div>
                                <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                                    <span class="control-label text-left" ng-if="!!(+vm.solicitudAutorizacion)">{{ __('common.si') }}</span>
                                    <span class="control-label text-left" ng-if="!(+vm.solicitudAutorizacion)">{{ __('common.no') }}</span>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                    <strong class="control-label text-left">{{ __('common.indicativo_cobro') }}:</strong>
                                </div>
                                <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                                    <span class="control-label text-left" ng-if="!!(+vm.indicativoCobro)">{{ __('common.si') }}</span>
                                    <span class="control-label text-left" ng-if="!(+vm.indicativoCobro)">{{ __('common.no') }}</span>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                    <strong class="control-label text-left">{{ __('common.valor_diligencia') }}:</strong>
                                </div>
                                <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                                    <span class="control-label text-left">@{{vm.valorDiligencia | currency}} &nbsp;</span>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                    <strong class="control-label text-left">{{ __('common.gastos_envio') }}:</strong>
                                </div>
                                <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                                    <span class="control-label text-left">@{{vm.gastoEnvio | currency}} &nbsp;</span>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                    <strong class="control-label text-left">{{ __('common.otros_gastos') }}:</strong>
                                </div>
                                <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                                    <span class="control-label text-left">@{{vm.otrosGastos | currency}} &nbsp;</span>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                    <strong class="control-label text-left">{{ __('common.costo') }} {{ __('common.diligencia') }}:</strong>
                                </div>
                                <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                                    <span class="control-label text-left">@{{vm.costoDiligencia | currency}} &nbsp;</span>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                    <strong class="control-label text-left">{{ __('common.costo_envio') }}:</strong>
                                </div>
                                <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                                    <span class="control-label text-left">@{{vm.costoEnvio | currency}} &nbsp;</span>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                    <strong class="control-label text-left">{{ __('common.otros_costos') }}:</strong>
                                </div>
                                <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                                    <span class="control-label text-left">@{{vm.otrosCostos | currency}} &nbsp;</span>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                    <strong class="control-label text-left">{{ __('common.observaciones_cliente') }}:</strong>
                                </div>
                                <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                                    <span class="control-label text-left">@{{vm.observacionesCliente}} &nbsp;</span>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                    <strong class="control-label text-left">{{ __('common.observaciones_internas') }}:</strong>
                                </div>
                                <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                                    <span class="control-label text-left">@{{vm.observacionesInternas}} &nbsp;</span>
                                </div>
                            </div>
                            <hr>
                        </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                <strong class="control-label text-left">{{ __('common.anexo_diligencia') }}:</strong>
                            </div>
                                <div class="row" ng-show="vm.archivoDiligencia">
                                    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 text-left">
                                         <span>
                                            <a class="attachment-item" ng-click="vm.descargarArchivoDiligencia()" style="cursor: pointer;">
                                                @{{ vm.archivoDiligencia }}
                                            </a>
                                            <a type="button" rel="tooltip" class="btn btn-xs btn-primary btn-simple attachment-item-btn-delete"
                                               title="Descargar"  ng-click="vm.descargarArchivoDiligencia()">
                                                <i class="material-icons">get_app</i>
                                            </a>
                                        </span>
                                    </div>
                                </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                <strong class="control-label text-left">{{ __('common.anexo_costos') }}:</strong>
                            </div>
                            <div class="row" ng-show="vm.archivoCostos">
                                <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 text-left">
                                     <span>
                                        <a class="attachment-item"  ng-click="vm.descargarArchivoCostos()" style="cursor: pointer;">
                                            @{{ vm.archivoCostos }}
                                        </a>
                                        <a type="button" rel="tooltip" class="btn btn-xs btn-primary btn-simple attachment-item-btn-delete"
                                           title="Descargar"  ng-click="vm.descargarArchivoCostos()">
                                            <i class="material-icons">get_app</i>
                                        </a>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                <strong class="control-label text-left">{{ __('common.anexo_recibido') }}:</strong>
                            </div>
                            <div class="row" ng-show="vm.archivoRecibido">
                                <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 text-left">
                                     <span>
                                        <a class="attachment-item"  ng-click="vm.descargarArchivoRecibido()" style="cursor: pointer;">
                                            @{{ vm.archivoRecibido }}
                                        </a>
                                        <a type="button" rel="tooltip" class="btn btn-xs btn-primary btn-simple attachment-item-btn-delete"
                                           title="Descargar"  ng-click="vm.descargarArchivoRecibido()">
                                            <i class="material-icons">get_app</i>
                                        </a>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){$.material.init();});
</script>
