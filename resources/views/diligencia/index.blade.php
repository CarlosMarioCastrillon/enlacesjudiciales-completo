<div id="diligencia" class="row" st-table="vm.coleccion" st-pipe="vm.obtenerResumen" st-persist="resumenDiligencia" st-reset="isReset" refresh-table>
    <div class="card main-card">
        <div class="card-content">
            <h4 class="card-title">@{{ vm.titulo }}</h4>
            @can('CrearDiligencia')
                <a class="btn btn-primary btn-round btn-fab nex-btn-add" title="Nueva diligencia" href="#/diligencias/null/formulario?mode=1" ng-hide="vm.diligenciaTramite">
                    <i class="material-icons">add</i>
                </a>
            @endcan
                <button class="btn btn-success btn-fab nex-btn-add" title="Exportar a excel" ng-click="vm.obtenerExcel()" style="right: 65px;">
                    <i class="mdi mdi-file-excel"></i>
                </button>
                <button  class="btn btn-default btn-fab nex-btn-add" ng-style="{'top': !vm.diligenciaTramite ? '2.8em':'5px'}"
                         type="button" rel="tooltip" title="{{ __('common.limpiar_filtros') }}" ng-click="vm.limpiarFiltros()">
                    <i class="material-icons">clear_all</i>
                </button>
                <button class="btn btn-primary btn-fab nex-btn-add" title="{{ __('common.buscar') }}" ng-click="vm.buscar()"
                        ng-disabled="vm.deshabilitarBuscado" ng-style="{'top':  !vm.diligenciaTramite ? '5.5em' : '2.8em'}">
                    <i class="material-icons">search</i>
                </button>

            <form name="vm.form" autocomplete="off" novalidate>
            <div class="row">
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div ng-class="['form-group label-floating', {'is-empty': !vm.numeroDiligencia}]">
                        <label class="control-label">{{ __('common.numero_diligencia') }}</label>
                        <input st-search="numero_diligencia" ng-model="vm.numeroDiligencia" class="form-control" type="text"/>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <div ng-class="['form-group label-floating', {'is-empty': !vm.empresaCliente}]">
                        <label class="control-label">{{ __('common.empresa_cliente') }}</label>
                        <input st-search="empresa_cliente" ng-model="vm.empresaCliente" class="form-control" type="text"/>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4" ng-hide="vm.diligenciaTramite">
                    <div class="input-group">
                        <div ng-class="['form-group label-floating', {'is-empty': !vm.filtroFechaInicial, 'is-not-empty': vm.filtroFechaInicial}]">
                            <label class="control-label">{{ __('common.fecha_desde') }}</label>
                            <input st-search-datetimepicker search-model="vm.filtroFechaInicial" search-predicate="fecha_inicial"
                                   datetimepicker-st-table datetimepicker-options="{useCurrent: false, format: 'YYYY-MM-DD'}" ng-model="vm.filtroFechaInicial"
                                   class="form-control" type="text"/>
                            <i class="input-group-addon fa fa-calendar"></i>
                        </div>
                    </div>
                </div>
                <div ng-class="['col-xs-12 ', {'col-sm-3 col-md-3 col-lg-3': !vm.diligenciaTramite, 'col-sm-4 col-md-4 col-lg-4': vm.diligenciaTramite}]">
                    <div class="form-group label-floating">
                        <label class="control-label">{{ __('common.proceso_') }}</label>
                        <input st-search="numero_proceso" ng-model="vm.proceso" class="form-control" type="text"/>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4" ng-hide="vm.diligenciaTramite">
                    <div ng-class="['form-group label-floating', {'is-empty': !vm.estado, 'is-not-empty': vm.estado}]">
                        <label class="control-label">{{ __('common.estado') }}</label>
                        <select selectize="vm.configEstadoDiligencia" options="vm.opcionesEstadoDiligencia" ng-model="vm.filtroEstado"
                                search-model="vm.filtroEstado" search-predicate="estado_diligencia" class="form-control"></select>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4" ng-hide="vm.diligenciaTramite">
                    <div class="input-group">
                        <div ng-class="['form-group label-floating', {'is-empty': !vm.filtroFechaFinal, 'is-not-empty': vm.filtroFechaFinal}]">
                            <label class="control-label">{{ __('common.fecha_hasta') }}</label>
                            <input st-search-datetimepicker search-model="vm.filtroFechaFinal" search-predicate="fecha_final" datetimepicker-st-table
                                   datetimepicker-options="{useCurrent: false, format: 'YYYY-MM-DD'}" ng-model="vm.filtroFechaFinal" class="form-control" type="text"/>
                            <i class="input-group-addon fa fa-calendar"></i>
                        </div>
                    </div>
                </div>
            </div>
            </form>
            <hr>
            <div class="row" style="margin: 0;" ng-if="!vm.cargando && vm.coleccion.length > 0">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 hidden-xs text-left" style="padding: 0;">
                    <nav>
                        <ul class="pagination pagination-primary">
                            <li class="text-left">
                                <a style="padding: 0 5px;" href="" type="button" rel="tooltip" title="{{ __('common.limpiar_filtros') }}" ng-click="vm.limpiarFiltros()">
                                    <i class="material-icons">clear_all</i>
                                </a>
                            </li>
                            <li class="text-left">
                                <span style="text-transform: none; font-size: 14px;">
                                    @{{ 'Mostrando ' + vm.paginacion.desde + ' a ' + vm.paginacion.hasta + ' de ' + vm.paginacion.total + ' resultados - Página ' + vm.paginacion.paginaActual + ' de ' + vm.paginacion.ultimaPagina }}
                                </span>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding: 0;">
                    <div class="text-right" st-pagination="" st-items-by-page="vm.itemsPorPagina" st-displayed-pages="5" st-template="/paginacion-template" style="float: right;"></div>
                    <div class="text-right" style="float: right; margin: 26px 25px 26px 0;">
                        <label style="color: rgba(0,0,0,0.68);">
                            Filas:
                            <select ng-options="option for option in [5, 10, 25, 50, 100, 500] track by option" ng-model="vm.itemsPorPagina" class="select-grid"></select>
                        </label>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-hover table-sm">
                    <thead>
                        <tr>
                            <th class="text-right" st-multi-sort="diligencia" style="width: 80px;">{{ __('common.numero') }} <br> {{ __('common.Diligencia') }}</th>
                            <th class="text-right" st-multi-sort="fecha">{{ __('common.fecha') }}</th>
                            <th class="text-left" st-multi-sort="empresa_cliente" style="min-width: 110px;">{{ __('common.cliente') }}</th>
                            <th class="text-right" st-multi-sort="numero_proceso" style="width: 159px;">{{ __('common.proceso_') }}</th>
                            <th class="text-left" st-multi-sort="tipo_diligencia">{{ __('common.tipo') }} <br> {{ __('common.Diligencia') }}</th>
                            <th st-multi-sort="dependiente" ng-show="vm.diligenciaTramite">{{ __('common.dependiente') }}</th>
                            <th class="text-right" st-multi-sort="valor_total">{{ __('common.valor') }} <br> {{ __('common.total') }}</th>
                            <th class="text-right" st-multi-sort="fecha_actualizacion" style="width: 94px;">{{ __('common.fecha') }} <br> {{ __('common.actualizacion') }}</th>
                            <th class="text-left" st-multi-sort="estado_diligencia">{{ __('common.estado') }}</th>
                            <th st-multi-sort="usuario_creacion_nombre" class="text-left">{{ __('common.creado_por') }}</th>
                            @if(auth()->user()->can('ModificarDiligencia') || auth()->user()->can('EliminarDiligencia'))
                                <th class="text-right" ng-style="{'width': vm.diligenciaTramite? '65px': '122px'}">{{ __('common.acciones') }}</th>
                            @endif
                        </tr>
                    </thead>
                    <thead ng-show="vm.cargando">
                        <tr>
                            <td colspan="11" class="text-center" style="padding: 0px;">
                                <div class="progress progress-striped active" style="margin: 0px;">
                                    <div class="progress-bar" style="width: 100%"></div>
                                </div>
                            </td>
                        </tr>
                    </thead>
                    <tbody ng-show="!vm.cargando">
                        <tr ng-repeat="row in vm.coleccion">
                            <td class="text-right">@{{row.id}}</td>
                            <td class="text-right">@{{row.fecha_diligencia}}</td>
                            <td class="text-left">@{{row.empresa_nombre}}</td>
                            <td class="text-right">@{{row.numero_proceso}}</td>
                            <td class="text-left">@{{row.tipo_diligencia_nombre}}</td>
                            <td class="text-left" ng-show="vm.diligenciaTramite">@{{row.dependiente}}</td>
                            <td class="text-right">@{{  row.valor_diligencia + row.gastos_envio + row.otros_gastos | currency:$:0 }}</td>
                            <td class="text-right">@{{row.fecha_modificacion.substring(0,10)}}</td>
                            <td class="text-left">
                                <span ng-if="(+row.estado_diligencia) == vm.EstadoDiligenciaEnum.SOLICITADO">@{{ 'SOLICITADO' | translate }}</span>
                                <span ng-if="(+row.estado_diligencia) == vm.EstadoDiligenciaEnum.COTIZADO">@{{ 'COTIZADO' | translate }}</span>
                                <span ng-if="(+row.estado_diligencia) == vm.EstadoDiligenciaEnum.APROBADO">@{{ 'APROBADO' | translate }}</span>
                                <span ng-if="(+row.estado_diligencia) == vm.EstadoDiligenciaEnum.TRAMITE">@{{ 'TRAMITE' | translate }}</span>
                                <span ng-if="(+row.estado_diligencia) == vm.EstadoDiligenciaEnum.TERMINADO">@{{ 'TERMINADO' | translate }}</span>
                                <span ng-if="(+row.estado_diligencia) == vm.EstadoDiligenciaEnum.CONTABILIZADO">@{{ 'CONTABILIZADO' | translate }}</span>
                                <span ng-if="(+row.estado_diligencia) == vm.EstadoDiligenciaEnum.FACTURADO">@{{ 'FACTURADO' | translate }}</span>
                            </td>
                            <td>@{{ row.usuario_creacion_nombre }}</td>
                            @if(auth()->user()->can('ModificarDiligencia') || auth()->user()->can('EliminarDiligencia') || auth()->user()->can('ModificarDiligenciaTramite'))
                                <td class="td-actions text-right">
                                @can('ModificarDiligencia')
                                    <a type="button" rel="tooltip" class="btn btn-primary btn-simple" title="{{ __('common.modificar') }}"
                                       href="#/diligencias/@{{ row.id }}/formulario?modo=2">
                                        <i class="material-icons">mode_edit</i>
                                    </a>
                                    <a type="button" rel="tooltip" class="btn btn-success btn-simple" title="{{ __('common.cambiar_estado') }}"
                                       href="#/diligencias/@{{ row.id }}/formulario?modo=3">
                                        <i class="material-icons">play_circle_filled</i>
                                    </a>
                                    <a type="button" rel="tooltip" class="btn btn-secondary btn-simple" title="{{ __('common.consultar_historia') }}"
                                       href="#/diligencias/@{{ row.id }}/historia">
                                        <i class="material-icons">assignment</i>
                                    </a>
                                @endcan
                                @can('ModificarDiligenciaTramite')
                                    <a type="button" rel="tooltip" class="btn btn-primary btn-simple" title="{{ __('common.modificar') }}"
                                       href="#/diligencias-tramite/@{{ row.id }}/formulario?dependiente=1" ng-if="vm.diligenciaTramite">
                                        <i class="material-icons">mode_edit</i>
                                    </a>
                                @endcan
                                @can('EliminarDiligencia')
                                    <a type="button" rel="tooltip" class="btn btn-danger btn-simple" title="{{ __('common.eliminar') }}"
                                       ng-click="vm.confirmarEliminar(row)" ng-hide="vm.diligenciaTramite">
                                        <i class="material-icons">delete</i>
                                    </a>
                                @endcan
                                </td>
                            @endif
                        </tr>
                    </tbody>
                    <tfoot ng-if="!vm.cargando && vm.coleccion.length > 0">
                        <script>$(document).ready(function(){$.material.ripples('table .btn');});</script>
                    </tfoot>
                </table>
            </div>
            <div class="row" style="margin: 0;" ng-show="!vm.cargando && vm.coleccion.length == 0">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 0;">
                    <nav>
                        <ul class="pagination pagination-primary">
                            <li class="text-left">
                                <a style="padding: 0 5px;" href="" type="button" rel="tooltip" title="{{ __('common.limpiar_filtro') }}" ng-click="vm.limpiarFiltros()">
                                    <i class="material-icons">clear_all</i>
                                </a>
                                <span style="text-transform: none; font-size: 14px;">{{ __('common.sin_resultados') }}</span>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="row" style="margin: 0;" ng-if="!vm.cargando && vm.coleccion.length > 0">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 hidden-xs text-left" style="padding: 0;">
                    <nav>
                        <ul class="pagination pagination-primary">
                            <li class="text-left">
                                <a style="padding: 0 5px;" href="" type="button" rel="tooltip" title="{{ __('common.limpiar_filtros') }}" ng-click="vm.limpiarFiltros()">
                                    <i class="material-icons">clear_all</i>
                                </a>
                            </li>
                            <li class="text-left">
                                <span style="text-transform: none; font-size: 14px;">
                                    @{{ 'Mostrando ' + vm.paginacion.desde + ' a ' + vm.paginacion.hasta + ' de ' + vm.paginacion.total + ' resultados - Página ' + vm.paginacion.paginaActual + ' de ' + vm.paginacion.ultimaPagina }}
                                </span>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding: 0;">
                    <div class="text-right" st-pagination="" st-items-by-page="vm.itemsPorPagina" st-displayed-pages="5" st-template="/paginacion-template" style="float: right;"></div>
                    <div class="text-right" style="float: right; margin: 26px 25px 26px 0;">
                        <label style="color: rgba(0,0,0,0.68);">
                            Filas:
                            <select ng-options="option for option in [5, 10, 25, 50, 100, 500] track by option" ng-model="vm.itemsPorPagina" class="select-grid"></select>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>$(document).ready(function(){$.material.init();});</script>
</div>
