<div id="rol" class="row" st-table="vm.coleccion" st-pipe="vm.obtenerResumen" st-persist="resumenRol" st-reset="isReset">
    <div class="card main-card" style="margin-bottom: 0;">
        <div class="card-content">
            <h4 class="card-title">Auditorias</h4>
            @can('CrearRol')
            @endcan
            <div class="row">
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div class="form-group label-floating">
                        <label class="control-label">Nombre recurso</label>
                        <input st-search="nombre_recurso" ng-model="vm.filtroNombre" class="form-control" type="text"/>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div class="form-group label-floating">
                        <label class="control-label">Descripción recurso</label>
                        <input st-search="descripcion_recurso" ng-model="vm.fitroDescripcion" class="form-control" type="text"/>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div class="input-group">
                        <div ng-class="['form-group label-floating', {'is-empty': !vm.filtroFechaDesde, 'is-not-empty': vm.filtroFechaDesde}]">
                            <label class="control-label">Fecha desde</label>
                            <input datetimepicker-st-table datetimepicker-options="{useCurrent: false, format: 'YYYY-MM-DD'}" st-search-datetimepicker search-model="vm.filtroFechaDesde" search-predicate="fecha_desde" ng-model="vm.filtroFechaDesde" class="form-control" type="text"/>
                            <i class="input-group-addon fa fa-calendar"></i>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div class="input-group">
                        <div ng-class="['form-group label-floating', {'is-empty': !vm.filtroFechaHasta, 'is-not-empty': vm.filtroFechaHasta}]">
                            <label class="control-label">Fecha hasta</label>
                            <input datetimepicker-st-table datetimepicker-options="{useCurrent: false, format: 'YYYY-MM-DD'}" st-search-datetimepicker search-model="vm.filtroFechaHasta" search-predicate="fecha_hasta" ng-model="vm.filtroFechaHasta" class="form-control" type="text"/>
                            <i class="input-group-addon fa fa-calendar"></i>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row" style="margin: 0;" ng-if="!vm.cargando && vm.coleccion.length > 0">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 hidden-xs text-left" style="padding: 0;">
                    <nav>
                        <ul class="pagination pagination-primary">
                            <li class="text-left">
                                <a style="padding: 0 5px;" href="" type="button" rel="tooltip" title="{{ __('common.limpiar_filtros') }}" ng-click="vm.limpiarFiltros()">
                                    <i class="material-icons">clear_all</i>
                                </a>
                            </li>
                            <li class="text-left">
                                <span style="text-transform: none; font-size: 14px;">
                                    @{{ 'Mostrando ' + vm.paginacion.desde + ' a ' + vm.paginacion.hasta + ' de ' + vm.paginacion.total + ' resultados - Página ' + vm.paginacion.paginaActual + ' de ' + vm.paginacion.ultimaPagina }}
                                </span>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding: 0;">
                    <div class="text-right" st-pagination="" st-items-by-page="vm.itemsPorPagina" st-displayed-pages="5" st-template="/paginacion-template" style="float: right;"></div>
                    <div class="text-right" style="float: right; margin: 26px 25px 26px 0;">
                        <label style="color: rgba(0,0,0,0.68);">
                            Filas:
                            <select ng-options="option for option in [5, 10, 25, 50, 100, 500] track by option" ng-model="vm.itemsPorPagina" class="select-grid"></select>
                        </label>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-sm">
                    <thead>
                        <tr>
                            <th>ID recurso</th>
                            <th>Nombre recurso</th>
                            <th>Descripción recurso</th>
                            <th class="text-center">Acción</th>
                            <th>Responsable</th>
                            <th class="text-right">Fecha</th>
                            <th class="text-right">Acciones</th>
                        </tr>
                    </thead>
                    <thead ng-show="vm.cargando">
                        <tr>
                            <td colspan="7" class="text-center" style="padding: 0px;">
                                <div class="progress progress-striped active" style="margin: 0px;">
                                    <div class="progress-bar" style="width: 100%"></div>
                                </div>
                            </td>
                        </tr>
                    </thead>
                    <tbody ng-show="!vm.cargando">
                        <tr ng-repeat-start="row in vm.coleccion">
                            <td>@{{ row.id_recurso }}</td>
                            <td>@{{ row.nombre_recurso }}</td>
                            <td>@{{ row.descripcion_recurso }}</td>
                            <td class="text-center">@{{ row.accion  }}</td>
                            <td>@{{ row.responsable_nombre  }}</td>
                            <td class="text-right">@{{ row.fecha }}</td>
                            <td class="td-actions text-right">
                                <a type="button" rel="tooltip" class="btn btn-primary btn-simple" title="Ver" ng-click="row.visible = !row.visible;">
                                    <i class="material-icons">pageview</i>
                                </a>
                            </td>
                        </tr>
                        <tr ng-repeat-end ng-show="row.visible">
                            <td colspan="7">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                        <p><strong>Recurso original</strong></p>
                                        <table class="table table-hover table-sm">
                                            <tr>
                                                <td>
                                                    <strong>Campo</strong>
                                                </td>
                                                <td>
                                                    <strong>Valor</strong>
                                                </td>
                                            </tr>
                                            <tr ng-repeat="(clave, valor) in row.recurso">
                                                <td>@{{ clave }}</td>
                                                <td>@{{ valor }}</td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" ng-if="!!row.recurso_modificado">
                                        <p><strong>Recurso modificado</strong></p>
                                        <table class="table table-hover table-sm">
                                            <tr>
                                                <td>
                                                    <strong>Campo</strong>
                                                </td>
                                                <td>
                                                    <strong>Valor</strong>
                                                </td>
                                            </tr>
                                            <tr ng-repeat="(clave, valor) in row.recurso_modificado">
                                                <td>@{{ clave }}</td>
                                                <td>@{{ valor }}</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                    <tfoot ng-if="!vm.cargando && vm.coleccion.length > 0">
                        <script>$(document).ready(function(){$.material.ripples('table .btn');});</script>
                    </tfoot>
                </table>
            </div>
            <div class="row" style="margin: 0;" ng-show="!vm.cargando && vm.coleccion.length == 0">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left" style="padding: 0;">
                    <nav>
                        <ul class="pagination pagination-primary">
                            <li class="text-left">
                                <a style="padding: 0 5px;" href="" type="button" rel="tooltip" title="{{ __('common.limpiar_filtros') }}" ng-click="vm.limpiarFiltros()">
                                    <i class="material-icons">clear_all</i>
                                </a>
                                <span style="text-transform: none; font-size: 14px;">{{ __('common.sin_resultados') }}</span>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="row" style="margin: 0;" ng-if="!vm.cargando && vm.coleccion.length > 0">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 hidden-xs text-left" style="padding: 0;">
                    <nav>
                        <ul class="pagination pagination-primary">
                            <li class="text-left">
                                <a style="padding: 0 5px;" href="" type="button" rel="tooltip" title="{{ __('common.limpiar_filtros') }}" ng-click="vm.limpiarFiltros()">
                                    <i class="material-icons">clear_all</i>
                                </a>
                            </li>
                            <li class="text-left">
                                <span style="text-transform: none; font-size: 14px;">
                                    @{{ 'Mostrando ' + vm.paginacion.desde + ' a ' + vm.paginacion.hasta + ' de ' + vm.paginacion.total + ' resultados - Página ' + vm.paginacion.paginaActual + ' de ' + vm.paginacion.ultimaPagina }}
                                </span>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding: 0;">
                    <div class="text-right" st-pagination="" st-items-by-page="vm.itemsPorPagina" st-displayed-pages="5" st-template="/paginacion-template" style="float: right;"></div>
                    <div class="text-right" style="float: right; margin: 26px 25px 26px 0;">
                        <label style="color: rgba(0,0,0,0.68);">
                            Filas:
                            <select ng-options="option for option in [5, 10, 25, 50, 100, 500] track by option" ng-model="vm.itemsPorPagina" class="select-grid"></select>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>$(document).ready(function(){$.material.init();});</script>
</div>
