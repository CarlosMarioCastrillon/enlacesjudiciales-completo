<div class="modal-header">
    <h4 class="card-title">{{ __('common.datos_etapa_clase_proceso') }}</h4>
</div>
<div class="modal-body">
    <form name="vm.form" autocomplete="off" novalidate>
        <div class="row">
            <div class="col-x   s-12 col-sm-12 col-md-12 col-lg-12">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.claseProcesoId, 'is-not-empty': vm.claseProcesoId, 'with-error': vm.guardando && vm.form.claseProceso.$invalid}]">
                    <label class="control-label">{{ __('common.clase_de_proceso') }}</label>
                    <select selectize="vm.configClaseProceso" options="vm.opcionesClaseProceso"  disabled-typing="true" ng-model="vm.claseProcesoId" name="clase_proceso" class="form-control"></select>
                    <span class="help-block show" ng-show="vm.guardando && vm.form.claseProceso.$invalid">
                        <span ng-show="vm.form.claseProceso.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div ng-class="['form-group label-floating', {'is-empty': !vm.etapaProcesoId, 'is-not-empty': vm.etapaProcesoId, 'with-error': vm.guardando && vm.form.etapaProceso.$invalid}]">
                    <label class="control-label">{{ __('common.etapa_de_proceso') }}</label>
                    <select selectize="vm.configEtapaProceso" options="vm.opcionesEtapaProceso" disabled-typing="true" ng-model="vm.etapaProcesoId" name="etapa_proceso" class="form-control"></select>
                    <span class="help-block show" ng-show="vm.guardando && vm.form.etapaProceso.$invalid">
                        <span ng-show="vm.form.etapaProceso.$error.required">Requerido</span>
                    </span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <p style="margin-bottom: 0;">{{ __('common.estado') }}</p>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="vm.estado" ng-value="true">
                                {{ __('common.activo') }}
                            </label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                        <div class="radio">
                            <label>
                                <input type="radio" ng-model="vm.estado" ng-value="false">
                                {{ __('common.inactivo') }}
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button class="btn btn-primary" type="button" ng-click="vm.guardar()" ng-disabled="vm.deshabilitarGuardado">{{ __('common.guardar') }}</button>
    <button class="btn btn-default" type="button" ng-click="vm.cancelar()">{{ __('common.cancelar') }}</button>
</div>
<script>$(document).ready(function(){$.material.init();});</script>
