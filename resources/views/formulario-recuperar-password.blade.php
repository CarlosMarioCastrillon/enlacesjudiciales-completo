<!DOCTYPE html>
<html lang="en" ng-app="enlaces.login">
    <head>
        <meta charset="utf-8">
        <meta name="theme-color" content="#D0AD4C">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />

        <title>{{ env('APP_NAME') }}</title>
        <link rel="shortcut icon" href="/favicon.ico" />

        <!-- Styles -->
        <link rel="stylesheet" href="{{ mix('/css/login-' . env('APP_THEME', 'a') . '.css') }}" type='text/css' media="all">

        <!--  Fonts and icons  -->
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons"/>

        <style>
            button {
                width: 100%;
            }
            img {
                margin-left: -25px;
            }
            @media (min-width: 768px) {
                .login-container {
                    margin-top: 70px;
                }
            }
            #loading-bar .bar {
                background: red;
                height: 2px;
            }
            #loading-bar-spinner .spinner-icon {
                border: solid 2px transparent;
                border-top-color: red;
                border-left-color: red;
            }
        </style>
    </head>
    <body ng-controller="loginController as vm">
        <div class="container-fluid">
            <div class="row login-container">
                <div class="col-xs-12 col-sm-8 col-md-4 col-lg-4 col-sm-offset-2 col-md-offset-4 col-lg-offset-4">
                    @if($tokenValido)
                    <div class="card" ng-if="!vm.restablecimientoCompletado">
                        <div class="card-content">
                            <h4 class="card-title">{{ trans('common.restablecimiento_de_password') }}</h4>
                            <form name="vm.form" autocomplete="off" novalidate>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <p class="text-justify">
                                            {{ __('common.ingrese_su_nuevo_password') }}
                                        </p>
                                        <div ng-class="['form-group label-floating', {'is-empty': !vm.password, 'with-error': vm.submitted && vm.form.password.$invalid}]">
                                            <label class="control-label">{{ trans('common.contrasenia_nueva') }}</label>
                                            <input type="password" class="form-control" ng-model="vm.password" name="password" required maxlength="50">
                                            <span class="help-block show" ng-show="vm.submitted && vm.form.password.$invalid">
                                                <span ng-show="vm.form.password.$error.required">Requerido</span>
                                            </span>
                                        </div>
                                        <div ng-class="['form-group label-floating', {'is-empty': !vm.confirmacionPassword, 'with-error': vm.submitted && vm.form.confirmacionPassword.$invalid}]">
                                            <label class="control-label">{{ trans('common.confirmar_contrasenia') }}</label>
                                            <input type="password" class="form-control" ng-model="vm.confirmacionPassword" name="confirmacionPassword" required maxlength="50">
                                            <span class="help-block show" ng-show="vm.submitted && vm.form.confirmacionPassword.$invalid">
                                                <span ng-show="vm.form.confirmacionPassword.$error.required">Requerido</span>
                                            </span>
                                        </div>
                                        <input id="token" class="hidden" value="{{ $token }}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left">
                                        <button type="submit" class="btn btn-primary btn-fill" ng-click="vm.cambiarPassword()">{{ trans('common.cambiar_contrasenia') }}</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="card" ng-if="vm.restablecimientoCompletado">
                        <div class="card-content">
                            <h4 class="card-title">{{ trans('common.restablecimiento_de_contrasenia_completada') }}</h4>
                            <form name="vm.form" autocomplete="off" novalidate>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <p class="text-justify">
                                            {{ __('common.se_ha_establecido_la_contraseña') }}
                                        </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left">
                                        <a class="btn btn-primary btn-fill" href="/login">{{ trans('common.acceder') }}</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    @endif

                    @if(!$tokenValido)
                    <div class="card">
                        <div class="card-content">
                            <h4 class="card-title">{{ trans('common.restablecimiento_de_password') }}</h4>
                            <form name="vm.form" autocomplete="off" novalidate>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <p class="text-justify">
                                            {{ __('common.el_enlace_de_restablecimiento_de_contrasenia_no_es_valido') }}
                                        </p>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
        <script src={{ mix('/js/login.js') }}></script>
    </body>
</html>
<script>$(document).ready(function(){$.material.init();});</script>
