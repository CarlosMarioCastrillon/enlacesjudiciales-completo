const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass('resources/sass/tema_a/material-dashboard.scss', 'public/css/tema_a');
mix.sass('resources/sass/tema_b/material-dashboard.scss', 'public/css/tema_b');

// Star

mix.scripts([
    'resources/js/bower_components/lodash/dist/lodash.min.js',
    'resources/js/bower_components/angular/angular.min.js',
    'resources/js/bower_components/angular-jwt/dist/angular-jwt.min.js',
    'resources/js/start.controller.js'
], 'public/js/start.js').version();

// Login

mix.styles([
    'resources/css/common/material_design/bootstrap.min.css',
    'public/css/tema_a/material-dashboard.css',
    'resources/js/bower_components/angular-loading-bar/build/loading-bar.min.css',
    'resources/css/common/snackbar/md-snackbars.css',
    'resources/css/common/material-design.css'
], 'public/css/login-a.css').version();

mix.styles([
    'resources/css/common/material_design/bootstrap.min.css',
    'public/css/tema_b/material-dashboard.css',
    'resources/js/bower_components/angular-loading-bar/build/loading-bar.min.css',
    'resources/css/common/snackbar/md-snackbars.css',
    'resources/css/common/material-design.css'
], 'public/css/login-b.css').version();

mix.scripts([
    'resources/js/bower_components/lodash/dist/lodash.min.js',
    'resources/js/common/material_design/jquery-3.1.1.min.js',
    'resources/js/common/material_design/bootstrap.min.js',
    'resources/js/common/material_design/material.min.js',
    'resources/js/bower_components/angular/angular.min.js',
    'resources/js/bower_components/angular-route/angular-route.min.js',
    'resources/js/bower_components/angular-jwt/dist/angular-jwt.min.js',
    'resources/js/bower_components/angular-environment/dist/angular-environment.min.js',
    'resources/js/bower_components/angular-loading-bar/build/loading-bar.min.js',
    'resources/js/common/snackbar/md-snackbars.js',
    'resources/js/bower_components/selectize/dist/js/standalone/selectize.min.js',
    'resources/js/login.controller.js'
], 'public/js/login.js').version();

// App

mix.styles([
    // Bootstrap and material design
    'resources/css/common/material_design/bootstrap.min.css',
    'public/css/tema_a/material-dashboard.css',
    'resources/js/bower_components/angular-bootstrap/ui-bootstrap-csp.css',

    // Perfect Scrollbar
    'resources/js/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css',

    // MDI
    'resources/js/bower_components/mdi/css/materialdesignicons.min.css',

    // Angular loading bar
    'resources/js/bower_components/angular-loading-bar/build/loading-bar.min.css',

    // Angular busy
    'resources/js/bower_components/angular-busy/dist/angular-busy.min.css',

    // Selectize material design
    'resources/css/common/selectize-material-a.css',

    // Grid
    'resources/css/common/grid/grid-style.css',

    // Image management
    'resources/css/common/ng_upload_file/ng-upload-file-a.css',

    // MD Snackbar
    'resources/css/common/snackbar/md-snackbars.css',

    // Customized
    'resources/css/common/material-design.css',
    'resources/css/common/layout-a.css',
    'resources/css/common/panel-bi.css',
    'resources/css/permiso.css'
], 'public/css/app-a.css').version();

mix.styles([
    // Bootstrap and material design
    'resources/css/common/material_design/bootstrap.min.css',
    'public/css/tema_b/material-dashboard.css',
    'resources/js/bower_components/angular-bootstrap/ui-bootstrap-csp.css',

    // Perfect Scrollbar
    'resources/js/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css',

    // MDI
    'resources/js/bower_components/mdi/css/materialdesignicons.min.css',

    // Angular loading bar
    'resources/js/bower_components/angular-loading-bar/build/loading-bar.min.css',

    // Angular busy
    'resources/js/bower_components/angular-busy/dist/angular-busy.min.css',

    // Selectize material design
    'resources/css/common/selectize-material-b.css',

    // Grid
    'resources/css/common/grid/grid-style.css',

    // Image management
    'resources/css/common/ng_upload_file/ng-upload-file-b.css',

    // MD Snackbar
    'resources/css/common/snackbar/md-snackbars.css',

    // Customized
    'resources/css/common/material-design.css',
    'resources/css/common/layout-b.css',
    'resources/css/common/panel-bi.css',
    'resources/css/permiso.css'
], 'public/css/app-b.css').version();

mix.scripts([
    // Libraries
    'resources/js/bower_components/lodash/dist/lodash.min.js',
    'resources/js/common/material_design/jquery-3.1.1.min.js',
    'resources/js/common/material_design/moment.min.js',

    // Angular
    'resources//js/bower_components/angular/angular.min.js',
    'resources/js/bower_components/angular-route/angular-route.min.js',
    'resources/js/bower_components/angular-animate/angular-animate.min.js',
    'resources/js/bower_components/angular-touch/angular-touch.min.js',
    'resources/js/bower_components/angular-jwt/dist/angular-jwt.min.js',
    'resources/js/bower_components/angular-environment/dist/angular-environment.min.js',
    'resources/js/bower_components/angular-translate/angular-translate.min.js',
    'resources/js/bower_components/angular-translate-handler-log/angular-translate-handler-log.min.js',
    'resources/js/bower_components/angular-translate-loader-static-files/angular-translate-loader-static-files.min.js',
    'resources/js/bower_components/angular-sanitize/angular-sanitize.min.js',

    // Bootstrap and material design
    // 'resources/js/common/material_design/jquery-ui.min.js',
    'resources/js/common/material_design/bootstrap.min.js',
    'resources/js/common/material_design/perfect-scrollbar.jquery.min.js',
    // 'resources/assets/js/common/material_design/jquery.validate.min.js',
    'resources/js/common/material_design/material.min.js',
    // 'resources/assets/js/common/material_design/chartist.min.js',
    // 'resources/js/common/material_design/jquery.bootstrap-wizard.js',
    // 'resources/assets/js/common/material_design/bootstrap-notify.js',
    'resources/js/common/material_design/bootstrap-datetimepicker.js',
    // 'resources/assets/js/common/material_design/jquery-jvectormap.js',
    // 'resources/assets/js/common/material_design/nouislider.min.js',
    // 'resources/assets/js/common/material_design/jquery.dropdown.js',
    // 'resources/assets/js/common/material_design/jquery.datatables.js',
    // 'resources/assets/js/common/material_design/sweetalert2.js',
    // 'resources/assets/js/common/material_design/fullcalendar.min.js',
    // 'resources/assets/js/common/material_design/jquery.tagsinput.js',
    // 'resources/js/common/material_design/material-dashboard.js',
    'resources/js/bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js',

    // Perfect Scrollbar
    'resources/js/bower_components/perfect-scrollbar/js/perfect-scrollbar.min.js',
    'resources/js/bower_components/angular-perfect-scrollbar/src/angular-perfect-scrollbar.min.js',

    // MD Snackbar
    'resources/js/common/snackbar/md-snackbars.js',

    // Angular loading bar
    'resources/js/bower_components/angular-loading-bar/build/loading-bar.min.js',

    // Angular busy
    'resources/js/bower_components/angular-busy/dist/angular-busy.min.js',

    // Selectize
    'resources/js/bower_components/sifter/sifter.min..js',
    'resources/js/bower_components/microplugin/src/microplugin.js',
    'resources/js/bower_components/selectize/dist/js/standalone/selectize.min.js',

    // Smart table
    'resources/js/common/grid/smart-table.js',
    'resources/js/bower_components/st-multi-sort-lja/lib/index.js',

    // Image management
    'resources/js/bower_components/ng-file-upload/ng-file-upload.min.js',
    'resources/js/bower_components/ng-file-upload/ng-file-upload-shim.min.js'
], 'public/js/libraries.js').version();

mix.scripts([
    // Module and configuration
    'resources/js/app.module.js',
    'resources/js/app.config.js',
    'resources/js/app.routes.js',

    // Controllers
    'resources/js/nav.controller.js',
    'resources/js/menu.controller.js',
    'resources/js/common/confirmar.controller.js',
    'resources/js/info-empresa.controller.js',

    'resources/js/seguridad/rol.controller.js',
    'resources/js/seguridad/rol-edit.controller.js',
    'resources/js/seguridad/usuario.controller.js',
    'resources/js/seguridad/usuario-edit.controller.js',
    'resources/js/seguridad/usuario-cambio-clave.controller.js',
    'resources/js/seguridad/permiso-edit.controller.js',
    'resources/js/seguridad/auditoria-maestro.controller.js',
    'resources/js/seguridad/opcion-sistema.controller.js',
    'resources/js/seguridad/opcion-sistema-edit.controller.js',

    'resources/js/configuracion/servicio.controller.js',
    'resources/js/configuracion/servicio-edit.controller.js',
    'resources/js/configuracion/tipo-de-empresa.controller.js',
    'resources/js/configuracion/tipo-de-empresa-edit.controller.js',
    'resources/js/configuracion/clase-de-proceso.controller.js',
    'resources/js/configuracion/clase-de-proceso-edit.controller.js',
    'resources/js/configuracion/etapa-de-proceso.controller.js',
    'resources/js/configuracion/etapa-de-proceso-edit.controller.js',
    'resources/js/configuracion/departamento.controller.js',
    'resources/js/configuracion/departamento-edit.controller.js',
    'resources/js/configuracion/etapa-clase-proceso.controller.js',
    'resources/js/configuracion/etapa-clase-proceso-edit.controller.js',
    'resources/js/configuracion/zona.controller.js',
    'resources/js/configuracion/zona-edit.controller.js',
    'resources/js/configuracion/ciudad.controller.js',
    'resources/js/configuracion/ciudad-edit.controller.js',
    'resources/js/configuracion/juzgado.controller.js',
    'resources/js/configuracion/juzgado-edit.controller.js',
    'resources/js/configuracion/tipo-documento.controller.js',
    'resources/js/configuracion/tipo-documento-edit.controller.js',
    'resources/js/configuracion/despacho.controller.js',
    'resources/js/configuracion/despacho-edit.controller.js',
    'resources/js/configuracion/empresa.controller.js',
    'resources/js/configuracion/empresa-edit.controller.js',
    'resources/js/configuracion/area.controller.js',
    'resources/js/configuracion/area-edit.controller.js',
    'resources/js/configuracion/plan.controller.js',
    'resources/js/configuracion/plan-edit.controller.js',
    'resources/js/configuracion/concepto-economico.controller.js',
    'resources/js/configuracion/concepto-economico-edit.controller.js',
    'resources/js/configuracion/copia-concepto-economico.controller.js',
    'resources/js/configuracion/copia-concepto-economico-edit.controller.js',
    'resources/js/configuracion/cobertura.controller.js',
    'resources/js/configuracion/cobertura-edit.controller.js',
    'resources/js/configuracion/tipo-actuacion.controller.js',
    'resources/js/configuracion/tipo-actuacion-edit.controller.js',
    'resources/js/configuracion/tipo-notificacion.controller.js',
    'resources/js/configuracion/tipo-notificacion-edit.controller.js',
    'resources/js/configuracion/salario-minimo.controller.js',
    'resources/js/configuracion/salario-minimo-edit.controller.js',
    'resources/js/configuracion/TES.controller.js',
    'resources/js/configuracion/TES-edit.controller.js',
    'resources/js/configuracion/IPC.controller.js',
    'resources/js/configuracion/IPC-edit.controller.js',
    'resources/js/configuracion/auxiliar-justicia.controller.js',
    'resources/js/configuracion/auxiliar-justicia-edit.controller.js',
    'resources/js/configuracion/auxiliar-justicia-importar.controller.js',
    'resources/js/configuracion/proceso-judicial-importar.controller.js',
    'resources/js/administracion/actuacion-importar.controller.js',

    //administracion
    'resources/js/administracion/cliente.controller.js',
    'resources/js/administracion/cliente-edit.controller.js',
    'resources/js/administracion/proceso-judicial.controller.js',
    'resources/js/administracion/proceso-judicial-edit.controller.js',
    'resources/js/administracion/proceso-valor.controller.js',
    'resources/js/administracion/proceso-valor-edit.controller.js',
    'resources/js/administracion/proceso-cliente.controller.js',
    'resources/js/administracion/proceso-cliente-edit.controller.js',
    'resources/js/administracion/vigilancia-proceso.controller.js',
    'resources/js/administracion/proceso-hoy.controller.js',
    'resources/js/administracion/proceso-juzgado-fecha.controller.js',
    'resources/js/administracion/busqueda-demandante-demandado.controller.js',
    'resources/js/administracion/proceso-area.controller.js',
    'resources/js/administracion/consulta-auto.controller.js',
    'resources/js/administracion/consulta-remate.controller.js',
    'resources/js/administracion/consulta-remanente.controller.js',
    'resources/js/administracion/consulta-personalizado-digital.controller.js',
    'resources/js/administracion/reporte-digital.controller.js',
    'resources/js/administracion/parametro-constante.controller.js',
    'resources/js/administracion/parametro-constante-edit.controller.js',
    'resources/js/administracion/audiencia-pendiente.controller.js',
    'resources/js/administracion/audiencia-cumplida.controller.js',
    'resources/js/administracion/termino-actuacion.controller.js',
    'resources/js/administracion/actuacion-cumplida.controller.js',
    'resources/js/administracion/auditoria-proceso.controller.js',
    'resources/js/administracion/actuacion.controller.js',
    'resources/js/administracion/actuacion-edit.controller.js',

    //Diligencias
    'resources/js/configuracion/tipo-diligencia.controller.js',
    'resources/js/configuracion/tipo-diligencia-edit.controller.js',
    'resources/js/configuracion/tarifa-diligencia.controller.js',
    'resources/js/configuracion/tarifa-diligencia-edit.controller.js',
    'resources/js/administracion/diligencia.controller.js',
    'resources/js/administracion/diligencia-edit.controller.js',
    'resources/js/administracion/diligencia-historia.controller.js',
    'resources/js/administracion/diligencia-historia-edit.controller.js',
    'resources/js/administracion/diligencia-cliente.controller.js',
    'resources/js/administracion/diligencia-cliente-edit.controller.js',

    //monitoreo
    'resources/js/monitoreo/proceso-general.controller.js',

    // Services
    'resources/js/common/generic.service.js',

    // Utils
    'resources/js/common/file.util.js',
    'resources/js/common/message.util.js',
    'resources/js/common/session.util.js',
    'resources/js/common/lang.util.js',

    // Constantes
    'resources/js/common/constantes.util.js',

    // Directives
    'resources/js/common/grid/page-select.directive.js',
    'resources/js/common/grid/persist.directive.js',
    'resources/js/common/grid/reset.directive.js',
    'resources/js/common/grid/reset-secondary.directive.js',
    'resources/js/common/grid/search-toggle.directive.js',
    'resources/js/common/grid/refresh-table.directive.js',
    'resources/js/common/focus-me.directive.js',

    // DateTimePicker
    'resources/js/common/datetimepicker/angular-bootstrap-datetimepicker-directive.js',
    'resources/js/common/datetimepicker/angular-bootstrap-datetimepicker-st-table-directive.js',

    // Selectize directive [Problems XD]
    'resources/js/common/selectize/ng-selectize.js'
], 'public/js/app.js').version();

// Fuentes de bootstrap
mix.copy(
    'resources/js/bower_components/bootstrap/dist/fonts/**',
    'public/fonts'
);

// Fuentes iconos material design
mix.copy(
    'resources/js/bower_components/mdi/fonts/**',
    'public/fonts'
);
