<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOpcionesDelSistemaTableAddUrl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('opciones_del_sistema', function (Blueprint $table) {
            $table->string('url')->nullable();
            $table->string('url_ayuda')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('opciones_del_sistema', function (Blueprint $table) {
            $table->dropColumn(['url', 'url_ayuda']);
        });
    }
}
