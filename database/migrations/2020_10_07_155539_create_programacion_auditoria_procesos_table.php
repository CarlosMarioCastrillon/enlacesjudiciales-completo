<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramacionAuditoriaProcesosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programacion_auditoria_procesos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('fecha_programacion');
            $table->dateTime('fecha_programacion_pasada')->nullable();
            $table->dateTime('fecha_ejecucion')->nullable();
            $table->date('fecha_proceso')->nullable();
            $table->integer('lotes')->nullable();
            $table->integer('lotes_ejecutados')->nullable();
            $table->smallInteger('estado')->default(1)->comment("1: Sin ejecutar, 2: En ejecución, 3: Ejecutado");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programacion_auditoria_procesos');
    }
}
