<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProgramacionAuditoriaProcesosTableAddRecientes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('programacion_auditoria_procesos', function (Blueprint $table) {
            $table->boolean('recientes')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('programacion_auditoria_procesos', function (Blueprint $table) {
            $table->dropColumn('recientes');
        });
    }
}
