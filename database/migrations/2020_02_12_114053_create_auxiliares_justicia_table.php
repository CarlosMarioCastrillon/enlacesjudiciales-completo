<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuxiliaresJusticiaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auxiliares_justicia', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('numero_documento', 15);
            $table->string('nombres_auxiliar');
            $table->string('apellidos_auxiliar');
            $table->string('nombre_ciudad');
            $table->string('direccion_oficina');
            $table->string('telefono_oficina', 30);
            $table->string('cargo');
            $table->string('telefono_celular', 30);
            $table->string('direccion_residencia');
            $table->string('telefono_residencia', 30);
            $table->string('correo_electronico');
            $table->boolean('estado')->default(true);
            $table->unsignedBigInteger('empresa_id');
            $table->foreign('empresa_id')->references('id')->on('empresas');

            // Auditoria
            $table->bigInteger('usuario_creacion_id');
            $table->string('usuario_creacion_nombre');
            $table->bigInteger('usuario_modificacion_id');
            $table->string('usuario_modificacion_nombre');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auxiliares_justicia');
    }
}
