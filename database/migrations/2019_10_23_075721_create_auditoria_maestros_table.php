<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditoriaMaestrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auditoria_maestros', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_recurso');
            $table->string('nombre_recurso');
            $table->string('descripcion_recurso')->nullable();
            $table->string('accion');
            $table->bigInteger('responsable_id');
            $table->string('responsable_nombre');
            $table->text('recurso_original');
            $table->text('recurso_resultante')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auditoria_maestros');
    }
}
