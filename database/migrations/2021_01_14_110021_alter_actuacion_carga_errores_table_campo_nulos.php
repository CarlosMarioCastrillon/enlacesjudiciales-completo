<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterActuacionCargaErroresTableCampoNulos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('actuacion_carga_errores', function (Blueprint $table) {
            $table->string('numero_proceso')->nullable()->change();
            $table->string('nombres_demandantes')->nullable()->change();
            $table->string('nombres_demandados')->nullable()->change();
            $table->string('ciudad_nombre')->nullable()->change();
            $table->string('juzgado_nombre')->nullable()->change();
            $table->string('despacho_nombre')->nullable()->change();
            $table->bigInteger('usuario_creacion_id')->nullable()->change();
            $table->string('usuario_creacion_nombre')->nullable()->change();
            $table->bigInteger('usuario_modificacion_id')->nullable()->change();
            $table->string('usuario_modificacion_nombre')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('actuacion_carga_errores', function (Blueprint $table) {
            //
        });
    }
}
