<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProcesoJudicialCargaErroresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proceso_judicial_carga_errores', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('numero_proceso');
            $table->string('nombres_demandantes');
            $table->string('nombres_demandados');
            $table->string('ciudad_nombre');
            $table->string('juzgado_nombre');
            $table->string('juzgado_numero');
            $table->string('clase_proceso_nombre');
            $table->string('responsable_documento');
            $table->string('area_nombre');
            $table->mediumText('observacion')->nullable();
            $table->unsignedBigInteger('empresa_id');
            $table->foreign('empresa_id')->references('id')->on('empresas');

            // Auditoria
            $table->bigInteger('usuario_creacion_id');
            $table->string('usuario_creacion_nombre');
            $table->bigInteger('usuario_modificacion_id');
            $table->string('usuario_modificacion_nombre');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proceso_judicial_carga_errores');
    }
}
