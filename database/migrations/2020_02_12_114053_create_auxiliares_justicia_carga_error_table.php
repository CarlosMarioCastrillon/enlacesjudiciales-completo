<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuxiliaresJusticiaCargaErrorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auxiliar_justicia_carga_errores', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('numero_documento')->nullable();
            $table->string('nombres_auxiliar')->nullable();
            $table->string('apellidos_auxiliar')->nullable();
            $table->string('nombre_ciudad')->nullable();
            $table->string('direccion_oficina')->nullable();
            $table->string('telefono_oficina')->nullable();
            $table->string('cargo')->nullable();
            $table->string('telefono_celular')->nullable();
            $table->string('direccion_residencia')->nullable();
            $table->string('telefono_residencia')->nullable();
            $table->string('correo_electronico')->nullable();
            $table->mediumText('observacion')->nullable();
            $table->unsignedBigInteger('empresa_id');
            $table->foreign('empresa_id')->references('id')->on('empresas');

            // Auditoria
            $table->bigInteger('usuario_creacion_id');
            $table->string('usuario_creacion_nombre');
            $table->bigInteger('usuario_modificacion_id');
            $table->string('usuario_modificacion_nombre');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auxiliar_justicia_carga_errores');
    }
}
