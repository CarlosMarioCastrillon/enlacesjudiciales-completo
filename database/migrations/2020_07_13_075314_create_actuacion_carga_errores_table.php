<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActuacionCargaErroresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actuacion_carga_errores', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('numero_proceso');
            $table->string('descripcion_actuacion');
            $table->string('descripcion_anotacion');
            $table->string('fecha_registro');
            $table->string('fecha_actuacion');
            $table->string('fecha_inicio_termino');
            $table->string('fecha_vencimiento_termino');
            $table->string('identificacion_actuacion_rama');
            $table->string('nombres_demandantes');
            $table->string('nombres_demandados');
            $table->string('ciudad_nombre');
            $table->string('juzgado_nombre');
            $table->string('despacho_nombre');
            $table->mediumText('observacion')->nullable();

            // Auditoria
            $table->bigInteger('usuario_creacion_id');
            $table->string('usuario_creacion_nombre');
            $table->bigInteger('usuario_modificacion_id');
            $table->string('usuario_modificacion_nombre');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actuacion_carga_errores');
    }
}
