<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiligenciasHistoriaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diligencias_historia', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('secuencia');
            $table->integer('valor_diligencia')->default(0);
            $table->integer('gastos_envio')->nullable();
            $table->integer('otros_gastos')->nullable();
            $table->integer('costo_diligencia')->nullable();
            $table->integer('costo_envio')->nullable();
            $table->integer('otros_costos')->nullable();
            $table->integer('estado_diligencia');
            $table->string('detalle_valores')->nullable();
            $table->string('url_anexo_diligencia',256)->nullable();
            $table->string('archivo_anexo_diligencia',256)->nullable();
            $table->string('url_anexo_costos',256)->nullable();
            $table->string('archivo_anexo_costos',256)->nullable();
            $table->string('url_anexo_recibido',256)->nullable();
            $table->string('archivo_anexo_recibido',256)->nullable();
            $table->string('observaciones_cliente')->nullable();
            $table->string('observaciones_internas')->nullable();
            $table->boolean('indicativo_cobro')->default(true);
            $table->boolean('solicitud_autorizacion');
            $table->unsignedBigInteger('diligencia_id');
            $table->foreign('diligencia_id')->references('id')->on('diligencias');
            $table->unsignedBigInteger('empresa_id');
            $table->foreign('empresa_id')->references('id')->on('empresas');
            $table->unsignedBigInteger('proceso_id');
            $table->foreign('proceso_id')->references('id')->on('procesos_judiciales');
            $table->unsignedBigInteger('dependiente_id')->nullable();
            $table->foreign('dependiente_id')->references('id')->on('usuarios');
            $table->date('fecha_diligencia');
            $table->unsignedBigInteger('tipo_diligencia_id');
            $table->foreign('tipo_diligencia_id')->references('id')->on('tipos_de_diligencias');
            $table->unsignedBigInteger('departamento_id');
            $table->foreign('departamento_id')->references('id')->on('departamentos');
            $table->unsignedBigInteger('ciudad_id');
            $table->foreign('ciudad_id')->references('id')->on('ciudades');

            // Auditoria datos
            $table->bigInteger('usuario_creacion_id');
            $table->string('usuario_creacion_nombre');
            $table->bigInteger('usuario_modificacion_id');
            $table->string('usuario_modificacion_nombre');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('diligencias_historia');
    }
}
