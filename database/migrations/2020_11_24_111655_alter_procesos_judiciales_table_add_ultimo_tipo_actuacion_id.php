<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProcesosJudicialesTableAddUltimoTipoActuacionId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('procesos_judiciales', function (Blueprint $table) {
            $table->dropColumn(['indicativo', 'seguridad']);
            $table->unsignedBigInteger('ultimo_tipo_actuacion_id')->nullable()->after('ultima_etapa_proceso_id');
            $table->foreign('ultimo_tipo_actuacion_id')->references('id')->on('tipos_de_actuaciones');
            DB::statement("alter table procesos_judiciales modify descripcion_clase_proceso varchar(191) null after indicativo_estado");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('procesos_judiciales', function (Blueprint $table) {
            //
        });
    }
}
