<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterActuacionesTableAddRutaArchivoEstadoCartelera extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('actuaciones', function (Blueprint $table) {
            $table->string('numero_radicado_proceso_actual', 23)->nullable()->after('numero_proceso');
            DB::statement("alter table actuaciones modify ruta_archivo_anexo varchar(256) null after observaciones");
            DB::statement("alter table actuaciones modify nombre_archivo_anexo varchar(191) null after ruta_archivo_anexo");
            $table->string('ruta_archivo_estado_cartelera',256)->nullable()->after('nombre_archivo_anexo');
            $table->string('nombre_archivo_estado_cartelera')->nullable()->after('ruta_archivo_estado_cartelera');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('actuaciones', function (Blueprint $table) {
            //
        });
    }
}
