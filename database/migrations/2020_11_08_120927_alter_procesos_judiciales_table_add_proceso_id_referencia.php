<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProcesosJudicialesTableAddProcesoIdReferencia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('procesos_judiciales', function (Blueprint $table) {
            $table->string('descripcion_clase_proceso')->nullable()->after('seguridad')->change();
            $table->bigInteger('proceso_id_referencia')->nullable()->after('numero_radicado_proceso_actual');
            $table->string('instancia_judicial_actual', 1)->nullable()->after('proceso_id_referencia');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('procesos_judiciales', function (Blueprint $table) {
            $table->dropColumn(['proceso_id_referencia', 'instancia_judicial_actual']);
        });
    }
}
