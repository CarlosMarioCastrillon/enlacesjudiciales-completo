<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEtapasClaseProceso extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('etapa_clase_procesos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('clase_proceso_id');
            $table->foreign('clase_proceso_id')->references('id')->on('clases_de_procesos');
            $table->unsignedBigInteger('etapa_proceso_id');
            $table->foreign('etapa_proceso_id')->references('id')->on('etapas_de_procesos');
            $table->boolean('estado')->default(true);

            // Auditoria
            $table->bigInteger('usuario_creacion_id');
            $table->string('usuario_creacion_nombre');
            $table->bigInteger('usuario_modificacion_id');
            $table->string('usuario_modificacion_nombre');
            $table->timestamps();

            // Indices
            $table->unique(['clase_proceso_id', 'etapa_proceso_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('etapa_clase_procesos');
    }
}
