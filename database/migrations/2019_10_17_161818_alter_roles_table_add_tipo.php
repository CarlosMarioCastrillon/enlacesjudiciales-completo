<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterRolesTableAddTipo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('roles', function (Blueprint $table) {
            $table->enum('type', [1, 2, 3])->default(1);
            $table->boolean('status')->default(true);
            $table->bigInteger('creation_user_id')->nullable();
            $table->string('creation_user_name')->nullable();
            $table->bigInteger('modification_user_id')->nullable();
            $table->string('modification_user_name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('roles', function (Blueprint $table) {
            $table->dropColumn([
                'type', 'status', 'creation_user_id',
                'creation_user_name', 'modification_user_id', 'modification_user_name'
            ]);
        });
    }
}
