<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNovedadProcesosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('novedad_procesos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('fecha_ejecucion');
            $table->date('fecha_proceso');
            $table->string('numero_proceso');
            $table->string('ciudad')->nullable();
            $table->string('despacho')->nullable();
            $table->string('demandante')->nullable();
            $table->string('demandado')->nullable();
            $table->date('fecha_actuacion');
            $table->string('descripcion_actuacion', 1024);
            $table->string('descripcion_anotacion', 1024)->nullable();
            $table->date('fecha_inicio_termino')->nullable();
            $table->date('fecha_finaliza_termino')->nullable();
            $table->date('fecha_registro')->nullable();
            $table->unsignedBigInteger('programacion_auditoria_proceso_id');
            $table->foreign('programacion_auditoria_proceso_id', 'fk_novedades_programacion_auditorias')
                ->references('id')->on('programacion_auditoria_procesos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('novedad_procesos');
    }
}
