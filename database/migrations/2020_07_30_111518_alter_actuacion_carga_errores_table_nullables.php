<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterActuacionCargaErroresTableNullables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('actuacion_carga_errores', function (Blueprint $table) {
            $table->string('descripcion_actuacion')->nullable()->change();
            $table->string('descripcion_anotacion')->nullable()->change();
            $table->string('fecha_registro')->nullable()->change();
            $table->string('fecha_actuacion')->nullable()->change();
            $table->string('fecha_inicio_termino')->nullable()->change();
            $table->string('fecha_vencimiento_termino')->nullable()->change();
            $table->string('identificacion_actuacion_rama')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('actuacion_carga_errores', function (Blueprint $table) {
            //
        });
    }
}
