<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpcionesDelSistemaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opciones_del_sistema', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre');
            $table->string('icono_menu')->nullable();
            $table->string('clave_permiso')->nullable();
            $table->integer('posicion')->nullable();
            $table->boolean('estado')->default(true);
            $table->bigInteger('opcion_del_sistema_id')->unsigned()->nullable();
            $table->foreign('opcion_del_sistema_id')->references('id')
                ->on('opciones_del_sistema');

            // Auditoria
            $table->bigInteger('usuario_creacion_id');
            $table->string('usuario_creacion_nombre');
            $table->bigInteger('usuario_modificacion_id');
            $table->string('usuario_modificacion_nombre');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('opciones_del_sistema');
    }
}
