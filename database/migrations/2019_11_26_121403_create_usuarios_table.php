<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('documento');
            $table->string('nombre');
            $table->string('email_uno')->nullable();
            $table->string('email_dos')->nullable();
            $table->date('fecha_vencimiento')->nullable();
            $table->boolean('dependiente_principal')->default(false);
            $table->boolean('con_microportal')->default(false);
            $table->boolean('con_movimiento_proceso')->default(false);
            $table->boolean('con_montaje_actuacion')->default(false);
            $table->boolean('con_movimiento_proceso_dos')->default(false);
            $table->boolean('con_montaje_actuacion_dos')->default(false);
            $table->boolean('con_tarea')->default(false);
            $table->boolean('con_respuesta_tarea')->default(false);
            $table->boolean('con_ven_termino_tarea')->default(false);
            $table->boolean('con_ven_termino_actuacion_rama')->default(false);
            $table->boolean('con_ven_termino_actuacion_archivo')->default(false);
            $table->boolean('estado')->default(true);
            $table->unsignedBigInteger('tipo_documento_id');
            $table->foreign('tipo_documento_id')->references('id')->on('tipos_de_documentos');
            $table->unsignedBigInteger('empresa_id');
            $table->foreign('empresa_id')->references('id')->on('empresas');
            $table->unsignedBigInteger('departamento_id')->nullable();
            $table->foreign('departamento_id')->references('id')->on('departamentos');
            $table->unsignedBigInteger('cuidad_id')->nullable();
            $table->foreign('cuidad_id')->references('id')->on('ciudades');

            // Usuario de sesión
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');

            // Auditoria
            $table->bigInteger('usuario_creacion_id');
            $table->string('usuario_creacion_nombre');
            $table->bigInteger('usuario_modificacion_id');
            $table->string('usuario_modificacion_nombre');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}
