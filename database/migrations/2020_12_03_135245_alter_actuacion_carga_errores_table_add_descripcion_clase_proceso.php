<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterActuacionCargaErroresTableAddDescripcionClaseProceso extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('actuacion_carga_errores', function (Blueprint $table) {
            $table->dropColumn('identificacion_actuacion_rama');
            $table->string('descripcion_clase_proceso');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('actuacion_carga_errores', function (Blueprint $table) {
            //
        });
    }
}
