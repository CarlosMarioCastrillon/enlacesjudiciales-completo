<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditoriaProcesosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auditoria_procesos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('fecha_ejecucion');
            $table->date('fecha_proceso');
            $table->string('numero_proceso');
            $table->string('tipo_novedad', 1);
            $table->unsignedBigInteger('programacion_auditoria_proceso_id');
            $table->foreign('programacion_auditoria_proceso_id', 'fk_auditorias_programacion_auditorias')
                ->references('id')->on('programacion_auditoria_procesos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auditoria_procesos');
    }
}
