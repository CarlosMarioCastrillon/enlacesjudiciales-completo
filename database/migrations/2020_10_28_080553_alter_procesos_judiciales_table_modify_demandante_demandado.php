<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProcesosJudicialesTableModifyDemandanteDemandado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('procesos_judiciales', function (Blueprint $table) {
            $table->renameColumn('juzgado_rama', 'juzgado_actual');
            $table->renameColumn('tipo_proceso', 'descripcion_clase_proceso');
            DB::statement("alter table procesos_judiciales modify nombres_demandantes varchar(255) null");
            DB::statement("alter table procesos_judiciales modify nombres_demandados varchar(255) null");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('procesos_judiciales', function (Blueprint $table) {
            //
        });
    }
}
