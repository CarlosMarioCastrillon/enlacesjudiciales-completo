<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActuacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actuaciones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('numero_proceso',23);
            $table->string('actuacion');
            $table->string('anotacion',4000);
            $table->date('fecha_registro');
            $table->date('fecha_actuacion');
            $table->date('fecha_inicio_termino')->nullable();
            $table->date('fecha_vencimiento_termino')->nullable();
            $table->integer('identificacion_actuacion_rama')->nullable();
            $table->char('ind_actuacion_audiencia',2)->nullable();
            $table->char('tipo_movimiento',2);
            $table->string('nombres_demandantes');
            $table->string('nombres_demandados');
            $table->string('observaciones',500)->nullable();
            $table->unsignedBigInteger('proceso_id');
            $table->foreign('proceso_id')->references('id')->on('procesos_judiciales');
            $table->unsignedBigInteger('ciudad_id');
            $table->foreign('ciudad_id')->references('id')->on('ciudades');
            $table->unsignedBigInteger('juzgado_id');
            $table->foreign('juzgado_id')->references('id')->on('juzgados');
            $table->unsignedBigInteger('juzgado_origen_id');
            $table->foreign('juzgado_origen_id')->references('id')->on('juzgados');
            $table->unsignedBigInteger('despacho_id');
            $table->foreign('despacho_id')->references('id')->on('despachos');
            $table->unsignedBigInteger('despacho_origen_id');
            $table->foreign('despacho_origen_id')->references('id')->on('despachos');
            $table->unsignedBigInteger('empresa_id');
            $table->foreign('empresa_id')->references('id')->on('empresas');
            $table->unsignedBigInteger('etapa_proceso_id')->nullable();
            $table->foreign('etapa_proceso_id')->references('id')->on('etapas_de_procesos');
            $table->unsignedBigInteger('tipo_actuacion_id')->nullable();
            $table->foreign('tipo_actuacion_id')->references('id')->on('tipos_de_actuaciones');
            $table->unsignedBigInteger('tipo_notificacion_id')->nullable();
            $table->foreign('tipo_notificacion_id')->references('id')->on('tipos_notificaciones');
            $table->string('ruta_archivo_anexo',256)->nullable();
            $table->string('nombre_archivo_anexo')->nullable();
            $table->char('estado',1)->nullable();

            // Auditoria
            $table->bigInteger('usuario_creacion_id');
            $table->string('usuario_creacion_nombre');
            $table->bigInteger('usuario_modificacion_id');
            $table->string('usuario_modificacion_nombre');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actuaciones');
    }
}
