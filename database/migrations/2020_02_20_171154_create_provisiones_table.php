<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProvisionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('provisiones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('consecutivo_historico');
            $table->char('ind_prov_actual', 1);
            $table->char('numero_proceso',23);
            $table->date('fecha_calculo_provision');
            $table->bigInteger('val_preten_determinadas');
            $table->bigInteger('val_preten_SMMLV');
            $table->bigInteger('val_preten_indeterminadas');
            $table->bigInteger('val_preten_per_mensual')->nullable();
            $table->bigInteger('val_preten_per_lab')->nullable();
            $table->date('fecha_presentacion_demanda');
            $table->date('fecha_inic_preten_lab')->nullable();
            $table->date('fecha_estimada_finalizacion');
            $table->bigInteger('val_historico_condenas')->nullable();
            $table->bigInteger('val_historico_pretensiones')->nullable();
            $table->bigInteger('val_preten_indexado');
            $table->bigInteger('val_real_calculado');
            $table->float('porc_est_determinadas',9,5)->nullable();
            $table->float('porc_est_SMMLV',9,5)->nullable();
            $table->float('porc_est_indeterminadas',9,5)->nullable();
            $table->float('porc_est_per_lab',9,5)->nullable();
            $table->bigInteger('val_est_usuario')->nullable();
            $table->float('IPC_inicial',9,5);
            $table->float('IPC_final',9,5);
            $table->float('porc_inflacion',9,5);
            $table->float('porc_TES',9,5);
            $table->float('porc_perd_relev',9,5);
            $table->float('porc_perd_contundencia',9,5);
            $table->float('porc_perd_riesgos_proc',9,5);
            $table->float('porc_perd_jurisprudencia',9,5);
            $table->bigInteger('val_registro');
            $table->string('probabilidad_perdida');
            $table->char('reg_sugil',1);
            $table->unsignedBigInteger('empresa_id');
            $table->foreign('empresa_id')->references('id')->on('empresas');
            $table->unsignedBigInteger('proceso_id');
            $table->foreign('proceso_id')->references('id')->on('procesos_judiciales');

            // Auditoria
            $table->bigInteger('usuario_creacion_id');
            $table->string('usuario_creacion_nombre');
            $table->bigInteger('usuario_modificacion_id');
            $table->string('usuario_modificacion_nombre');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('provisiones');
    }
}
