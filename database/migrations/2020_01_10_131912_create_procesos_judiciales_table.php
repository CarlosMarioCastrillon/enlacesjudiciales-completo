<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProcesosJudicialesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('procesos_judiciales', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('departamento_dane', 2);
            $table->string('ciudad_dane', 5);
            $table->string('juzgado_rama', 9);
            $table->string('juzgado_origen_rama', 9);
            $table->string('despacho_numero', 3);
            $table->string('despacho_origen_numero', 3);
            $table->string('anio_proceso', 4);
            $table->string('numero_radicado', 5);
            $table->string('numero_consecutivo',2);
            $table->string('numero_proceso',23);
            $table->string('numero_radicado_proceso_actual', 23);
            $table->string('nombres_demandantes');
            $table->string('nombres_demandados');
            $table->string('observaciones')->nullable();
            $table->char('indicativo_pertenece_rama',1)->default(false);
            $table->char('indicativo_consulta_rama',1)->default(false);
            $table->char('indicativo_tiene_vigilancia',1)->default(false);
            $table->char('indicativo_estado',1)->default(false);
            $table->string('indicativo')->nullable();
            $table->integer('seguridad')->nullable();
            $table->unsignedBigInteger('empresa_id');
            $table->foreign('empresa_id')->references('id')->on('empresas');
            $table->unsignedBigInteger('zona_id');
            $table->foreign('zona_id')->references('id')->on('zonas');
            $table->unsignedBigInteger('area_id');
            $table->foreign('area_id')->references('id')->on('areas');
            $table->unsignedBigInteger('cliente_id')->nullable();
            $table->foreign('cliente_id')->references('id')->on('clientes');
            $table->unsignedBigInteger('abogado_responsable_id');
            $table->foreign('abogado_responsable_id')->references('id')->on('usuarios');
            $table->unsignedBigInteger('dependiente_asignado_id')->nullable();
            $table->foreign('dependiente_asignado_id')->references('id')->on('usuarios');
            $table->unsignedBigInteger('ultima_clase_proceso_id')->nullable();
            $table->foreign('ultima_clase_proceso_id')->references('id')->on('clases_de_procesos');
            $table->unsignedBigInteger('ultima_etapa_proceso_id')->nullable();
            $table->foreign('ultima_etapa_proceso_id')->references('id')->on('etapas_de_procesos');
            $table->unsignedBigInteger('departamento_id');
            $table->foreign('departamento_id')->references('id')->on('departamentos');
            $table->unsignedBigInteger('ciudad_id');
            $table->foreign('ciudad_id')->references('id')->on('ciudades');
            $table->unsignedBigInteger('juzgado_id');
            $table->foreign('juzgado_id')->references('id')->on('juzgados');
            $table->unsignedBigInteger('juzgado_origen_id');
            $table->foreign('juzgado_origen_id')->references('id')->on('juzgados');
            $table->unsignedBigInteger('despacho_id');
            $table->foreign('despacho_id')->references('id')->on('despachos');
            $table->unsignedBigInteger('despacho_origen_id');
            $table->foreign('despacho_origen_id')->references('id')->on('despachos');

            // Auditoria
            $table->bigInteger('usuario_creacion_id');
            $table->string('usuario_creacion_nombre');
            $table->bigInteger('usuario_modificacion_id');
            $table->string('usuario_modificacion_nombre');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('procesos_judiciales');
    }
}
