<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIPCTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('indice_IPC', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('fecha')->unique();
            $table->float('indice_IPC',9,5);
            $table->float('inflacion_anual',9,5);
            $table->float('inflacion_mensual',9,5);
            $table->float('inflacion_anio',9,5);
            $table->boolean('estado')->default(true);

            // Auditoria
            $table->bigInteger('usuario_creacion_id');
            $table->string('usuario_creacion_nombre');
            $table->bigInteger('usuario_modificacion_id');
            $table->string('usuario_modificacion_nombre');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('IPC');
    }
}
