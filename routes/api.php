<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', function (){
    return "Enlaces | " . \Carbon\Carbon::now()->format("Y-M-D H:m:s") . " | Estado: 200";
});

Route::post('/users/token', 'UserController@getToken');

Route::get('/confirmar-template', function (){
    return view('confirmar');
});

// Tareas automaticas
Route::group(["prefix" => "tareas-automaticas"],function(){
    Route::get('auditoria-procesos', 'TareasAutomaticasController@lanzarAuditoriaProcesos')->name('tareas.lanzarAuditoriaProcesos');
    Route::get('consulta-actuaciones', 'TareasAutomaticasController@lanzarConsultaActuaciones')->name('tareas.lanzarConsultaActuaciones');
});

Route::group(['middleware' => ['auth:api']], function (){

    /******************************************************************************************************************/
    /*******************************************           Seguridad            ***************************************/
    /******************************************************************************************************************/

    // User
    Route::group(["prefix" => "users"],function(){
        Route::get('current/session', 'UserController@getSession')->name('session.show');
    });

    // Roles
    Route::group(["prefix" => "roles"],function(){
        Route::get('create', 'RolController@create')->name('roles.create')
            ->middleware(['permission:CrearRol|ModificarRol']);
        Route::get('index-template', 'RolController@getIndexTemplate')->name('roles.indexTemplate')
            ->middleware(['permission:ListarRol']);

        // Permisos
        Route::get('{id}/permisos/create', 'RolController@createPermiso')->name('permisos.createPermiso');
        Route::get('{id}/permisos', 'RolController@obtenerPermisos')->name('permisos.obtener');
        Route::post('{id}/permisos', 'RolController@otorgarPermisos')->name('permisos.otorgar');
        Route::put('{id}/permisos', 'RolController@revocarPermisos')->name('permisos.revocar');
    });
    Route::get('roles', 'RolController@index')->name('roles.index');
    Route::get('roles/{id}', 'RolController@show')->name('roles.show')
        ->middleware(['permission:ListarRol']);
    Route::post('roles', 'RolController@store')->name('roles.store')
        ->middleware(['permission:CrearRol']);
    Route::put('roles/{id}', 'RolController@update')->name('roles.update')
        ->middleware(['permission:ModificarRol']);
    Route::delete('roles/{id}', 'RolController@destroy')->name('roles.delete')
        ->middleware(['permission:EliminarRol']);

    // Usuarios
    Route::group(["prefix" => "usuarios"],function(){
        Route::get('create', 'Seguridad\UsuarioController@create')->name('usuarios.create')
            ->middleware(['permission:CrearUsuario|ModificarUsuario|CrearUsuarioCliente|ModificarUsuarioCliente']);
        Route::get('index-template', 'Seguridad\UsuarioController@getIndexTemplate')->name('usuarios.indexTemplate')
            ->middleware(['permission:ListarUsuario']);
        Route::get('cliente/template', 'Seguridad\UsuarioController@getClienteTemplate')
            ->name('usuarios.clienteTemplate')->middleware(['permission:ListarUsuarioCliente']);
        Route::get('cambio-clave-template', 'Seguridad\UsuarioController@getCambioClaveTemplate')
            ->name('usuarios.cambioClaveTemplate')->middleware(['permission:ModificarUsuarioCliente']);

        //obtener dependiente principal
        Route::get('dependiente-principal', 'Seguridad\UsuarioController@obtenerDependientePrincipal')->name('usuarios.obtenerDependientePrincipal');
    });

    Route::get('usuarios', 'Seguridad\UsuarioController@index')->name('usuarios.index');
    Route::get('usuarios/{id}', 'Seguridad\UsuarioController@show')->name('usuarios.show')
        ->middleware(['permission:ListarUsuario|ListarUsuarioCliente']);
    Route::post('usuarios', 'Seguridad\UsuarioController@store')->name('usuarios.store')
        ->middleware(['permission:CrearUsuario|CrearUsuarioCliente']);
    Route::put('usuarios/{id}', 'Seguridad\UsuarioController@update')->name('usuarios.update')
        ->middleware(['permission:ModificarUsuario|ModificarUsuarioCliente']);
    Route::delete('usuarios/{id}', 'Seguridad\UsuarioController@destroy')->name('usuarios.delete')
        ->middleware(['permission:EliminarUsuario|EliminarUsuarioCliente']);

    // Auditoria maestros
    Route::group(["prefix" => "auditoria-maestros"],function(){
        Route::get('create', 'Seguridad\AuditoriaMaestroController@create')->name('auditoria-maestros.create');
        Route::get('index-template', 'Seguridad\AuditoriaMaestroController@getIndexTemplate')
            ->name('auditoria-maestros.indexTemplate');
    });
    Route::get('auditoria-maestros', 'Seguridad\AuditoriaMaestroController@index')->name('auditoria-maestros.index');
    Route::get('auditoria-maestros/{id}', 'Seguridad\AuditoriaMaestroController@show')->name('auditoria-maestros.show');
    Route::post('auditoria-maestros', 'Seguridad\AuditoriaMaestroController@store')->name('auditoria-maestros.store');
    Route::put('auditoria-maestros/{id}', 'Seguridad\AuditoriaMaestroController@update')
        ->name('auditoria-maestros.update');
    Route::delete('auditoria-maestros/{id}', 'Seguridad\AuditoriaMaestroController@destroy')
        ->name('auditoria-maestros.delete');

    // Opciones del sistema maestros
    Route::group(["prefix" => "opciones-del-sistema"],function(){
        Route::get('create', 'Seguridad\OpcionSistemaController@create')->name('opciones-del-sistema.create');
        Route::get('index-template', 'Seguridad\OpcionSistemaController@getIndexTemplate')
            ->name('opciones-del-sistema.indexTemplate');
    });
    Route::get('opciones-del-sistema', 'Seguridad\OpcionSistemaController@index')->name('opciones-del-sistema.index');
    Route::get('opciones-del-sistema/{id}', 'Seguridad\OpcionSistemaController@show')->name('opciones-del-sistema.show');
    Route::post('opciones-del-sistema', 'Seguridad\OpcionSistemaController@store')->name('opciones-del-sistema.store');
    Route::put('opciones-del-sistema/{id}', 'Seguridad\OpcionSistemaController@update')
        ->name('opciones-del-sistema.update');

    /******************************************************************************************************************/
    /*******************************************         Configuración          ***************************************/
    /******************************************************************************************************************/

    // Servicios
    Route::group(["prefix" => "servicios"],function(){
        Route::get('create', 'Configuracion\ServicioController@create')->name('servicios.create')
            ->middleware(['permission:CrearServicio|ModificarServicio']);
        Route::get('index-template', 'Configuracion\ServicioController@getIndexTemplate')->name('servicios.indexTemplate')
            ->middleware(['permission:ListarServicio']);
    });
    Route::get('servicios', 'Configuracion\ServicioController@index')->name('servicios.index');
    Route::get('servicios/{id}', 'Configuracion\ServicioController@show')->name('servicios.show')
        ->middleware(['permission:ListarServicio']);
    Route::post('servicios', 'Configuracion\ServicioController@store')->name('servicios.store')
        ->middleware(['permission:CrearServicio']);
    Route::put('servicios/{id}', 'Configuracion\ServicioController@update')->name('servicios.update')
        ->middleware(['permission:ModificarServicio']);
    Route::delete('servicios/{id}', 'Configuracion\ServicioController@destroy')->name('servicios.delete')
        ->middleware(['permission:EliminarServicio']);

    // Tipos de Empresas
    Route::group(["prefix" => "tipos-de-empresa"],function(){
        Route::get('create', 'Configuracion\TipoDeEmpresaController@create')->name('tipos-de-empresa.create')
            ->middleware(['permission:CrearTipoDeEmpresa|ModificarTipoDeEmpresa']);
        Route::get('index-template', 'Configuracion\TipoDeEmpresaController@getIndexTemplate')->name('tipos-de-empresa.indexTemplate')
            ->middleware(['permission:ListarTipoDeEmpresa']);
    });
    Route::get('tipos-de-empresa', 'Configuracion\TipoDeEmpresaController@index')->name('tipos-de-empresa.index');
    Route::get('tipos-de-empresa/{id}', 'Configuracion\TipoDeEmpresaController@show')->name('tipos-de-empresa.show')
        ->middleware(['permission:ListarTipoDeEmpresa']);
    Route::post('tipos-de-empresa', 'Configuracion\TipoDeEmpresaController@store')->name('tipos-de-empresa.store')
        ->middleware(['permission:CrearTipoDeEmpresa']);
    Route::put('tipos-de-empresa/{id}', 'Configuracion\TipoDeEmpresaController@update')->name('tipos-de-empresa.update')
        ->middleware(['permission:ModificarTipoDeEmpresa']);
    Route::delete('tipos-de-empresa/{id}', 'Configuracion\TipoDeEmpresaController@destroy')->name('tipos-de-empresa.delete')
        ->middleware(['permission:EliminarTipoDeEmpresa']);

    // Clases de Procesos
    Route::group(["prefix" => "clases-de-procesos"],function(){
        Route::get('create', 'Configuracion\ClaseProcesoController@create')->name('clases-de-procesos.create')
            ->middleware(['permission:CrearClaseProceso|ModificarClaseProceso']);
        Route::get('index-template', 'Configuracion\ClaseProcesoController@getIndexTemplate')->name('clases-de-procesos.indexTemplate')
            ->middleware(['permission:ListarClaseProceso']);
    });
    Route::get('clases-de-procesos', 'Configuracion\ClaseProcesoController@index')->name('clases-de-procesos.index');
    Route::get('clases-de-procesos/{id}', 'Configuracion\ClaseProcesoController@show')->name('clases-de-procesos.show')
        ->middleware(['permission:ListarClaseProceso']);
    Route::post('clases-de-procesos', 'Configuracion\ClaseProcesoController@store')->name('clases-de-procesos.store')
        ->middleware(['permission:CrearClaseProceso']);
    Route::put('clases-de-procesos/{id}', 'Configuracion\ClaseProcesoController@update')->name('clases-de-procesos.update')
        ->middleware(['permission:ModificarClaseProceso']);
    Route::delete('clases-de-procesos/{id}', 'Configuracion\ClaseProcesoController@destroy')->name('clases-de-procesos.delete')
        ->middleware(['permission:EliminarClaseProceso']);

    // Etapas de Procesos
    Route::group(["prefix" => "etapas-de-procesos"],function(){
        Route::get('create', 'Configuracion\EtapaProcesoController@create')->name('etapas-de-procesos.create')
            ->middleware(['permission:CrearEtapaProceso|ModificarEtapaProceso']);
        Route::get('index-template', 'Configuracion\EtapaProcesoController@getIndexTemplate')->name('etapas-de-procesos.indexTemplate')
            ->middleware(['permission:ListarEtapaProceso']);
    });
    Route::get('etapas-de-procesos', 'Configuracion\EtapaProcesoController@index')->name('etapas-de-procesos.index');
    Route::get('etapas-de-procesos/{id}', 'Configuracion\EtapaProcesoController@show')->name('etapas-de-procesos.show')
        ->middleware(['permission:ListarEtapaProceso']);
    Route::post('etapas-de-procesos', 'Configuracion\EtapaProcesoController@store')->name('etapas-de-procesos.store')
        ->middleware(['permission:CrearEtapaProceso']);
    Route::put('etapas-de-procesos/{id}', 'Configuracion\EtapaProcesoController@update')->name('etapas-de-procesos.update')
        ->middleware(['permission:ModificarEtapaProceso']);
    Route::delete('etapas-de-procesos/{id}', 'Configuracion\EtapaProcesoController@destroy')->name('etapas-de-procesos.delete')
        ->middleware(['permission:EliminarEtapaProceso']);

    // Departamentos
    Route::group(["prefix" => "departamentos"],function(){
        Route::get('create', 'Configuracion\DepartamentoController@create')->name('departamentos.create')
            ->middleware(['permission:CrearDepartamento|ModificarDepartamento']);
        Route::get('index-template', 'Configuracion\DepartamentoController@getIndexTemplate')->name('departamentos.indexTemplate')
            ->middleware(['permission:ListarDepartamento']);
    });
    Route::get('departamentos', 'Configuracion\DepartamentoController@index')->name('departamentos.index');
    Route::get('departamentos/{id}', 'Configuracion\DepartamentoController@show')->name('departamentos.show')
        ->middleware(['permission:ListarDepartamento']);
    Route::post('departamentos', 'Configuracion\DepartamentoController@store')->name('departamentos.store')
        ->middleware(['permission:CrearDepartamento']);
    Route::put('departamentos/{id}', 'Configuracion\DepartamentoController@update')->name('departamentos.update')
        ->middleware(['permission:ModificarDepartamento']);
    Route::delete('departamentos/{id}', 'Configuracion\DepartamentoController@destroy')->name('departamentos.delete')
        ->middleware(['permission:EliminarDepartamento']);

    // Etapas de Procesos
    Route::group(["prefix" => "etapa-clase-procesos"],function(){
        Route::get('create', 'Configuracion\EtapaClaseProcesoController@create')->name('etapa-clase-procesos.create')
            ->middleware(['permission:CrearEtapaClase|ModificarEtapaClase']);
        Route::get('index-template', 'Configuracion\EtapaClaseProcesoController@getIndexTemplate')->name('etapa-clase-procesos.indexTemplate')
            ->middleware(['permission:ListarEtapaClase']);
    });
    Route::get('etapa-clase-procesos', 'Configuracion\EtapaClaseProcesoController@index')->name('etapa-clase-procesos.index');
    Route::get('etapa-clase-procesos/{id}', 'Configuracion\EtapaClaseProcesoController@show')->name('etapa-clase-procesos.show')
        ->middleware(['permission:ListarEtapaClase']);
    Route::post('etapa-clase-procesos', 'Configuracion\EtapaClaseProcesoController@store')->name('etapa-clase-procesos.store')
        ->middleware(['permission:CrearEtapaClase']);
    Route::put('etapa-clase-procesos/{id}', 'Configuracion\EtapaClaseProcesoController@update')->name('etapa-clase-procesos.update')
        ->middleware(['permission:ModificarEtapaClase']);
    Route::delete('etapa-clase-procesos/{id}', 'Configuracion\EtapaClaseProcesoController@destroy')->name('etapa-clase-procesos.delete')
        ->middleware(['permission:EliminarEtapaClase']);

    // Zonas
    Route::group(["prefix" => "zonas"],function(){
        Route::get('create', 'Configuracion\ZonaController@create')->name('zonas.create')
            ->middleware(['permission:CrearZona|ModificarZona']);
        Route::get('index-template', 'Configuracion\ZonaController@getIndexTemplate')->name('zonas.indexTemplate')
            ->middleware(['permission:ListarZona']);
    });
    Route::get('zonas', 'Configuracion\ZonaController@index')->name('zonas.index');
    Route::get('zonas/{id}', 'Configuracion\ZonaController@show')->name('zonas.show')
        ->middleware(['permission:ListarZona']);
    Route::post('zonas', 'Configuracion\ZonaController@store')->name('zonas.store')
        ->middleware(['permission:CrearZona']);
    Route::put('zonas/{id}', 'Configuracion\ZonaController@update')->name('zonas.update')
        ->middleware(['permission:ModificarZona']);
    Route::delete('zonas/{id}', 'Configuracion\ZonaController@destroy')->name('zonas.delete')
        ->middleware(['permission:EliminarZona']);

    // Ciudades
    Route::group(["prefix" => "ciudades"],function(){
        Route::get('create', 'Configuracion\CiudadController@create')->name('ciudades.create')
            ->middleware(['permission:CrearCiudad|ModificarCiudad']);
        Route::get('index-template', 'Configuracion\CiudadController@getIndexTemplate')->name('ciudades.indexTemplate')
            ->middleware(['permission:ListarCiudad']);
    });
    Route::get('ciudades', 'Configuracion\CiudadController@index')->name('ciudades.index');
    Route::get('ciudades/{id}', 'Configuracion\CiudadController@show')->name('ciudades.show')
        ->middleware(['permission:ListarCiudad']);
    Route::post('ciudades', 'Configuracion\CiudadController@store')->name('ciudades.store')
        ->middleware(['permission:CrearCiudad']);
    Route::put('ciudades/{id}', 'Configuracion\CiudadController@update')->name('ciudades.update')
        ->middleware(['permission:ModificarCiudad']);
    Route::delete('ciudades/{id}', 'Configuracion\CiudadController@destroy')->name('ciudades.delete')
        ->middleware(['permission:EliminarCiudad']);

    // Juzgados
    Route::group(["prefix" => "juzgados"],function(){
        Route::get('create', 'Configuracion\JuzgadoController@create')->name('juzgados.create')
            ->middleware(['permission:CrearJuzgado|ModificarJuzgado']);
        Route::get('index-template', 'Configuracion\JuzgadoController@getIndexTemplate')->name('juzgados.indexTemplate')
            ->middleware(['permission:ListarJuzgado']);
    });
    Route::get('juzgados', 'Configuracion\JuzgadoController@index')->name('juzgados.index');
    Route::get('juzgados/{id}', 'Configuracion\JuzgadoController@show')->name('juzgados.show')
        ->middleware(['permission:ListarJuzgado']);
    Route::post('juzgados', 'Configuracion\JuzgadoController@store')->name('juzgados.store')
        ->middleware(['permission:CrearJuzgado']);
    Route::put('juzgados/{id}', 'Configuracion\JuzgadoController@update')->name('juzgados.update')
        ->middleware(['permission:ModificarJuzgado']);
    Route::delete('juzgados/{id}', 'Configuracion\JuzgadoController@destroy')->name('juzgados.delete')
        ->middleware(['permission:EliminarJuzgado']);

    // Tipos de documentos
    Route::group(["prefix" => "tipos-documentos"],function(){
        Route::get('create', 'Configuracion\TipoDocumentoController@create')->name('tipos-documentos.create')
            ->middleware(['permission:CrearTipoDocumento|ModificarTipoDocumento']);
        Route::get('index-template', 'Configuracion\TipoDocumentoController@getIndexTemplate')->name('tipos-documentos.indexTemplate')
            ->middleware(['permission:ListarTipoDocumento']);
    });
    Route::get('tipos-documentos', 'Configuracion\TipoDocumentoController@index')->name('tipos-documentos.index');
    Route::get('tipos-documentos/{id}', 'Configuracion\TipoDocumentoController@show')->name('tipos-documentos.show')
        ->middleware(['permission:ListarTipoDocumento']);
    Route::post('tipos-documentos', 'Configuracion\TipoDocumentoController@store')->name('tipos-documentos.store')
        ->middleware(['permission:CrearTipoDocumento']);
    Route::put('tipos-documentos/{id}', 'Configuracion\TipoDocumentoController@update')->name('tipos-documentos.update')
        ->middleware(['permission:ModificarTipoDocumento']);
    Route::delete('tipos-documentos/{id}', 'Configuracion\TipoDocumentoController@destroy')->name('tipos-documentos.delete')
        ->middleware(['permission:EliminarTipoDocumento']);

    // Despachos
    Route::group(["prefix" => "despachos"],function(){
        Route::get('create', 'Configuracion\DespachoController@create')->name('despachos.create')
            ->middleware(['permission:CrearDespacho|ModificarDespacho']);
        Route::get('index-template', 'Configuracion\DespachoController@getIndexTemplate')->name('despachos.indexTemplate')
            ->middleware(['permission:ListarDespacho']);
    });
    Route::get('despachos', 'Configuracion\DespachoController@index')->name('despachos.index');
    Route::get('despachos/{id}', 'Configuracion\DespachoController@show')->name('despachos.show')
        ->middleware(['permission:ListarDespacho']);
    Route::post('despachos', 'Configuracion\DespachoController@store')->name('despachos.store')
        ->middleware(['permission:CrearDespacho']);
    Route::put('despachos/{id}', 'Configuracion\DespachoController@update')->name('despachos.update')
        ->middleware(['permission:ModificarDespacho']);
    Route::delete('despachos/{id}', 'Configuracion\DespachoController@destroy')->name('despachos.delete')
        ->middleware(['permission:EliminarDespacho']);

    // Empresas
    Route::group(["prefix" => "empresas"],function(){
        Route::get('create', 'Configuracion\EmpresaController@create')->name('empresas.create')
            ->middleware(['permission:CrearEmpresa|ModificarEmpresa']);
        Route::get('index-template', 'Configuracion\EmpresaController@getIndexTemplate')->name('empresas.indexTemplate')
            ->middleware(['permission:ListarEmpresa']);
        Route::get('excel', 'Configuracion\EmpresaController@getExcel')->name('empresas.getExcel')
            ->middleware(['permission:CrearEmpresa|ModificarEmpresa']);

        // Coberturas
        Route::put('{id}/coberturas', 'Configuracion\EmpresaController@modificarCobertura')->name('coberturas.update')
            ->middleware(['permission:ModificarCobertura']);
    });
    Route::get('empresas', 'Configuracion\EmpresaController@index')->name('empresas.index');
    Route::get('empresas/{id}', 'Configuracion\EmpresaController@show')->name('empresas.show')
        ->middleware(['permission:ListarEmpresa|ListarArea']);
    Route::post('empresas', 'Configuracion\EmpresaController@store')->name('empresas.store')
        ->middleware(['permission:CrearEmpresa']);
    Route::put('empresas/{id}', 'Configuracion\EmpresaController@update')->name('empresas.update')
        ->middleware(['permission:ModificarEmpresa']);
    Route::delete('empresas/{id}', 'Configuracion\EmpresaController@destroy')->name('empresas.delete')
        ->middleware(['permission:EliminarEmpresa']);
    Route::get('infoEmpresa/template', 'Configuracion\EmpresaController@getInfoTemplate')->name('empresas.infoTemplate');

    // Areas
    Route::group(["prefix" => "areas"],function(){
        Route::get('create', 'Configuracion\AreaController@create')->name('areas.create')
            ->middleware(['permission:CrearArea|ModificarArea']);
        Route::get('index-template', 'Configuracion\AreaController@getIndexTemplate')->name('areas.indexTemplate')
            ->middleware(['permission:ListarArea']);
    });
    Route::get('areas', 'Configuracion\AreaController@index')->name('areas.index');
    Route::get('areas/{id}', 'Configuracion\AreaController@show')->name('areas.show')
        ->middleware(['permission:ListarArea']);
    Route::post('areas', 'Configuracion\AreaController@store')->name('areas.store')
        ->middleware(['permission:CrearArea']);
    Route::put('areas/{id}', 'Configuracion\AreaController@update')->name('areas.update')
        ->middleware(['permission:ModificarArea']);
    Route::delete('areas/{id}', 'Configuracion\AreaController@destroy')->name('areas.delete')
        ->middleware(['permission:EliminarArea']);

    // Planes
    Route::group(["prefix" => "planes"],function(){
        Route::get('create', 'Configuracion\PlanController@create')->name('planes.create')
            ->middleware(['permission:CrearPlan|ModificarPlan']);
        Route::get('index-template', 'Configuracion\PlanController@getIndexTemplate')->name('planes.indexTemplate')
            ->middleware(['permission:ListarPlan']);
    });
    Route::get('planes', 'Configuracion\PlanController@index')->name('planes.index');
    Route::get('planes/{id}', 'Configuracion\PlanController@show')->name('planes.show')
        ->middleware(['permission:ListarPlan']);
    Route::post('planes', 'Configuracion\PlanController@store')->name('planes.store')
        ->middleware(['permission:CrearPlan']);
    Route::put('planes/{id}', 'Configuracion\PlanController@update')->name('planes.update')
        ->middleware(['permission:ModificarPlan']);
    Route::delete('planes/{id}', 'Configuracion\PlanController@destroy')->name('planes.delete')
        ->middleware(['permission:EliminarPlan']);

    // Conceptos económicos
    Route::group(["prefix" => "conceptos-economicos"],function(){
        Route::get('create', 'Configuracion\ConceptoEconomicoController@create')->name('conceptos-economicos.create')
            ->middleware(['permission:CrearConceptoEconomico|ModificarConceptoEconomico']);
        Route::get('index-template', 'Configuracion\ConceptoEconomicoController@getIndexTemplate')->name('conceptos-economicos.indexTemplate')
            ->middleware(['permission:ListarConceptoEconomico']);
        Route::get('excel', 'Configuracion\ConceptoEconomicoController@getExcel')->name('conceptos-economicos.getExcel')
            ->middleware(['permission:CrearConceptoEconomico|ModificarConceptoEconomico']);

        // Valores de procesos
    });
    Route::get('conceptos-economicos', 'Configuracion\ConceptoEconomicoController@index')->name('conceptos-economicos.index');
    Route::get('conceptos-economicos/{id}', 'Configuracion\ConceptoEconomicoController@show')->name('conceptos-economicos.show')
        ->middleware(['permission:ListarConceptoEconomico']);
    Route::post('conceptos-economicos', 'Configuracion\ConceptoEconomicoController@store')->name('conceptos-economicos.store')
        ->middleware(['permission:CrearConceptoEconomico']);
    Route::put('conceptos-economicos/{id}', 'Configuracion\ConceptoEconomicoController@update')->name('conceptos-economicos.update')
        ->middleware(['permission:ModificarConceptoEconomico']);
    Route::delete('conceptos-economicos/{id}', 'Configuracion\ConceptoEconomicoController@destroy')->name('conceptos-economicos.delete')
        ->middleware(['permission:EliminarConceptoEconomico']);

    // Copia Conceptos económicos
    Route::group(["prefix" => "copia-conceptos-economicos"],function(){
        Route::get('create', 'Configuracion\CopiaConceptoEconomicoController@create')->name('copia-conceptos-economicos.create')
            ->middleware(['permission:CrearCopiaConceptoEconomico|ModificarCopiaConceptoEconomico']);
        Route::get('index-template', 'Configuracion\CopiaConceptoEconomicoController@getIndexTemplate')->name('copia-conceptos-economicos.indexTemplate')
            ->middleware(['permission:ListarCopiaConceptoEconomico']);
    });
    Route::get('copia-conceptos-economicos', 'Configuracion\CopiaConceptoEconomicoController@index')->name('copia-conceptos-economicos.index');
    Route::get('copia-conceptos-economicos/{id}', 'Configuracion\CopiaConceptoEconomicoController@show')->name('copia-conceptos-economicos.show')
        ->middleware(['permission:ListarCopiaConceptoEconomico']);
    Route::post('copia-conceptos-economicos', 'Configuracion\CopiaConceptoEconomicoController@store')->name('copia-conceptos-economicos.store')
        ->middleware(['permission:CrearCopiaConceptoEconomico']);
    Route::put('copia-conceptos-economicos/{id}', 'Configuracion\CopiaConceptoEconomicoController@update')->name('copia-conceptos-economicos.update')
        ->middleware(['permission:ModificarCopiaConceptoEconomico']);
    Route::delete('copia-conceptos-economicos/{id}', 'Configuracion\CopiaConceptoEconomicoController@destroy')->name('copia-conceptos-economicos.delete')
        ->middleware(['permission:EliminaCopiarConceptoEconomico']);

    // Tipos de diligencias
    Route::group(["prefix" => "tipos-diligencias"],function(){
        Route::get('create', 'Configuracion\TipoDiligenciaController@create')->name('tipos-diligencias.create')
            ->middleware(['permission:CrearTipoDiligencia|ModificarTipoDiligencia']);
        Route::get('index-template', 'Configuracion\TipoDiligenciaController@getIndexTemplate')->name('tipos-diligencias.indexTemplate')
            ->middleware(['permission:ListarTipoDiligencia']);
    });
    Route::get('tipos-diligencias', 'Configuracion\TipoDiligenciaController@index')->name('tipos-diligencias.index');
    Route::get('tipos-diligencias/{id}', 'Configuracion\TipoDiligenciaController@show')->name('tipos-diligencias.show')
        ->middleware(['permission:ListarTipoDiligencia']);
    Route::post('tipos-diligencias', 'Configuracion\TipoDiligenciaController@store')->name('tipos-diligencias.store')
        ->middleware(['permission:CrearTipoDiligencia']);
    Route::put('tipos-diligencias/{id}', 'Configuracion\TipoDiligenciaController@update')->name('tipos-diligencias.update')
        ->middleware(['permission:ModificarTipoDiligencia']);
    Route::delete('tipos-diligencias/{id}', 'Configuracion\TipoDiligenciaController@destroy')->name('tipos-diligencias.delete')
        ->middleware(['permission:EliminarTipoDiligencia']);

    // Coberturas
    Route::group(["prefix" => "coberturas"],function(){
        Route::get('create', 'Configuracion\CoberturaController@create')->name('coberturas.create')
            ->middleware(['permission:CrearCobertura|ModificarCobertura']);
        Route::get('index-template', 'Configuracion\CoberturaController@getIndexTemplate')->name('coberturas.indexTemplate')
            ->middleware(['permission:ListarCobertura']);
    });
    Route::get('coberturas', 'Configuracion\CoberturaController@index')->name('coberturas.index');

    // Tarifas de diligencias
    Route::group(["prefix" => "tarifas-diligencias"],function(){
        Route::get('create', 'Configuracion\TarifaDiligenciaController@create')->name('tarifas-diligencias.create')
            ->middleware(['permission:CrearTarifaDiligencia|ModificarTarifaDiligencia']);
        Route::get('index-template', 'Configuracion\TarifaDiligenciaController@getIndexTemplate')->name('tarifas-diligencias.indexTemplate')
            ->middleware(['permission:ListarTarifaDiligencia']);
    });
    Route::get('tarifas-diligencias', 'Configuracion\TarifaDiligenciaController@index')->name('tarifas-diligencias.index');
    Route::get('tarifas-diligencias/{id}', 'Configuracion\TarifaDiligenciaController@show')->name('tarifas-diligencias.show')
        ->middleware(['permission:ListarTarifaDiligencia']);
    Route::post('tarifas-diligencias', 'Configuracion\TarifaDiligenciaController@store')->name('tarifas-diligencias.store')
        ->middleware(['permission:CrearTarifaDiligencia']);
    Route::put('tarifas-diligencias/{id}', 'Configuracion\TarifaDiligenciaController@update')->name('tarifas-diligencias.update')
        ->middleware(['permission:ModificarTarifaDiligencia']);
    Route::delete('tarifas-diligencias/{id}', 'Configuracion\TarifaDiligenciaController@destroy')->name('tarifas-diligencias.delete')
        ->middleware(['permission:EliminarTarifaDiligencia']);

    // Clientes
    Route::group(["prefix" => "clientes"],function(){
        Route::get('create', 'Administracion\ClienteController@create')->name('clientes.create')
            ->middleware(['permission:CrearCliente|ModificarCliente']);
        Route::get('index-template', 'Administracion\ClienteController@getIndexTemplate')->name('clientes.indexTemplate')
            ->middleware(['permission:ListarCliente']);
        Route::get('excel', 'Administracion\ClienteController@getExcel')->name('clientes.obtenerExcel')
            ->middleware(['permission:CrearCliente|ModificarCliente']);
    });
    Route::get('clientes', 'Administracion\ClienteController@index')->name('clientes.index');
    Route::get('clientes/{id}', 'Administracion\ClienteController@show')->name('clientes.show')
        ->middleware(['permission:ListarCliente']);
    Route::post('clientes', 'Administracion\ClienteController@store')->name('clientes.store')
        ->middleware(['permission:CrearCliente']);
    Route::put('clientes/{id}', 'Administracion\ClienteController@update')->name('clientes.update')
        ->middleware(['permission:ModificarCliente']);
    Route::delete('clientes/{id}', 'Administracion\ClienteController@destroy')->name('clientes.delete')
        ->middleware(['permission:EliminarCliente']);

    // Tipos de actuaciones
    Route::group(["prefix" => "tipos-actuaciones"],function(){
        Route::get('create', 'Configuracion\TipoActuacionController@create')->name('tipos-actuaciones.create')
            ->middleware(['permission:CrearTipoActuacion|ModificarTipoActuacion']);
        Route::get('index-template', 'Configuracion\TipoActuacionController@getIndexTemplate')->name('tipos-actuaciones.indexTemplate')
            ->middleware(['permission:ListarTipoActuacion']);
    });
    Route::get('tipos-actuaciones', 'Configuracion\TipoActuacionController@index')->name('tipos-actuaciones.index');
    Route::get('tipos-actuaciones/{id}', 'Configuracion\TipoActuacionController@show')->name('tipos-actuaciones.show')
        ->middleware(['permission:ListarTipoActuacion']);
    Route::post('tipos-actuaciones', 'Configuracion\TipoActuacionController@store')->name('tipos-actuaciones.store')
        ->middleware(['permission:CrearTipoActuacion']);
    Route::put('tipos-actuaciones/{id}', 'Configuracion\TipoActuacionController@update')->name('tipos-actuaciones.update')
        ->middleware(['permission:ModificarTipoActuacion']);
    Route::delete('tipos-actuaciones/{id}', 'Configuracion\TipoActuacionController@destroy')->name('tipos-actuaciones.delete')
        ->middleware(['permission:EliminarTipoActuacion']);

    // Valores-Procesos
    Route::group(["prefix" => "procesos-valores"],function(){
        Route::get('create', 'Administracion\ProcesoValorController@create')->name('procesos-valores.create')
            ->middleware(['permission:CrearValorProceso|ModificarValorProceso']);
        Route::get('index-template', 'Administracion\ProcesoValorController@getIndexTemplate')->name('procesos-valores.indexTemplate')
            ->middleware(['permission:ListarValorProceso']);
    });
    Route::get('procesos-valores', 'Administracion\ProcesoValorController@index')->name('procesos-valores.index');
    Route::get('procesos-valores/{id}', 'Administracion\ProcesoValorController@show')->name('procesos-valores.show')
        ->middleware(['permission:ListarValorProceso']);
    Route::post('procesos-valores', 'Administracion\ProcesoValorController@store')->name('procesos-valores.store')
        ->middleware(['permission:CrearValorProceso']);
    Route::put('procesos-valores/{id}', 'Administracion\ProcesoValorController@update')->name('procesos-valores.update')
        ->middleware(['permission:ModificarValorProceso']);
    Route::delete('procesos-valores/{id}', 'Administracion\ProcesoValorController@destroy')->name('procesos-valores.delete')
        ->middleware(['permission:EliminarValorProceso']);

    // Procesos-Judiciales
    Route::group(["prefix" => "procesos-judiciales"],function(){
        Route::get('create', 'Administracion\ProcesoJudicialController@create')->name('procesos-judiciales.create')
            ->middleware(['permission:CrearProcesoJudicial|ModificarProcesoJudicial']);
        Route::get('index-template', 'Administracion\ProcesoJudicialController@getIndexTemplate')->name('procesos-judiciales.indexTemplate')
            ->middleware(['permission:ListarProcesoJudicial']);
        Route::get('procesos-generales/template', 'Administracion\ProcesoJudicialController@getProcesoGeneralTemplate')
            ->name('procesos-judiciales.procesoGeneralTemplate');

        // Validacion
        Route::post('validacion', 'Administracion\ProcesoJudicialController@getValidacionProceso')
            ->name('procesos-judiciales.validacionProceso');

        // Carga de datos
        Route::get('importar-template', 'Administracion\ProcesoJudicialController@getImportarTemplate')
            ->name('procesos-judiciales.importarTemplate')
            ->middleware(['permission:ImportarProcesoJudicial']);
        Route::get('importar-excel', 'Administracion\ProcesoJudicialController@getExcelErrorImportacion')
            ->name('procesos-judiciales.excelErrorImportacion')
            ->middleware(['permission:ImportarProcesoJudicial']);
        Route::post('importar-excel', 'Administracion\ProcesoJudicialController@importar')
            ->name('procesos-judiciales.importar')
            ->middleware(['permission:ImportarProcesoJudicial']);
        Route::post('importar-texto', 'Administracion\ProcesoJudicialController@importarTexto')
            ->name('procesos-judiciales.importarTexto')
            ->middleware(['permission:ImportarProcesoJudicial']);

        // Reportes
        Route::get('procesos-generales', 'Administracion\ProcesoJudicialController@obtenerProcesosPorRol')
            ->name('procesos-judiciales.procesosPorRol');
    });
    Route::get('procesos-judiciales', 'Administracion\ProcesoJudicialController@index')->name('procesos-judiciales.index');
    Route::get('procesos-judiciales/{id}', 'Administracion\ProcesoJudicialController@show')->name('procesos-judiciales.show')
        ->middleware(['permission:ListarProcesoJudicial|ListarDiligenciaTramite|ListarConsultaDiligencia']);
    Route::post('procesos-judiciales', 'Administracion\ProcesoJudicialController@store')->name('procesos-judiciales.store')
        ->middleware(['permission:CrearProcesoJudicial']);
    Route::put('procesos-judiciales/{id}', 'Administracion\ProcesoJudicialController@update')->name('procesos-judiciales.update')
        ->middleware(['permission:ModificarProcesoJudicial']);
    Route::delete('procesos-judiciales/{id}', 'Administracion\ProcesoJudicialController@destroy')->name('procesos-judiciales.delete')
        ->middleware(['permission:EliminarProcesoJudicial']);

    // Procesos-Clientes
    Route::group(["prefix" => "procesos-clientes"],function(){
        Route::get('create', 'Administracion\ProcesoClienteController@create')->name('procesos-clientes.create')
            ->middleware(['permission:CrearProcesoCliente|ModificarProcesoCliente']);
        Route::get('index-template', 'Administracion\ProcesoClienteController@getIndexTemplate')->name('procesos-clientes.indexTemplate')
            ->middleware(['permission:ListarProcesoCliente']);
    });
    Route::get('procesos-clientes', 'Administracion\ProcesoClienteController@index')->name('procesos-clientes.index');
    Route::get('procesos-clientes/{id}', 'Administracion\ProcesoClienteController@show')->name('procesos-clientes.show')
        ->middleware(['permission:ListarProcesoCliente']);
    Route::post('procesos-clientes', 'Administracion\ProcesoClienteController@store')->name('procesos-clientes.store')
        ->middleware(['permission:CrearProcesoCliente']);
    Route::put('procesos-clientes/{id}', 'Administracion\ProcesoClienteController@update')->name('procesos-clientes.update')
        ->middleware(['permission:ModificarProcesoCliente']);
    Route::delete('procesos-clientes/{id}', 'Administracion\ProcesoClienteController@destroy')->name('procesos-clientes.delete')
        ->middleware(['permission:EliminarProcesoCliente']);

    // Diligencias
    Route::group(["prefix" => "diligencias"],function(){
        Route::get('create', 'Administracion\DiligenciaController@create')->name('diligencias.create')
            ->middleware(['permission:CrearDiligencia|ModificarDiligencia|ModificarDiligenciaTramite|CrearConsultaDiligencia']);
        Route::get('index-template', 'Administracion\DiligenciaController@getIndexTemplate')->name('diligencias.indexTemplate')
            ->middleware(['permission:ListarDiligencia']);
        Route::get('historia/template', 'Administracion\DiligenciaController@getHistoriaTemplate')
            ->name('diligencias.historiaTemplate');
        Route::get('historia/create', 'Administracion\DiligenciaController@getHistoriaEditTemplate')
            ->name('diligencias.historiaEditTemplate');
        Route::get('consulta-cliente/template', 'Administracion\DiligenciaController@getConsultaClienteTemplate')
            ->name('diligencias.consultaClienteTemplate');
        Route::get('form-cliente/template', 'Administracion\DiligenciaController@getFormClienteTemplate')
            ->name('diligencias.formClienteTemplate');
        Route::get('tramite/template', 'Administracion\DiligenciaController@getTramiteTemplate')
            ->name('diligencias.tramiteTemplate')->middleware(['permission:ListarDiligenciaTramite']);;

        Route::get('excel', 'Administracion\DiligenciaController@getExcel')->name('diligencias.getExcel');
        Route::get('consulta-cliente/excel', 'Administracion\DiligenciaController@obtenerConsultaExcel')
            ->name('diligencias.consultaClienteExcel');

        // Archivo anexo
        Route::get('{id}/anexo-diligencia', 'Administracion\DiligenciaController@obtenerAnexoDiligencia')
            ->name('diligencias.anexoDiligencia');
        Route::get('{id}/historia/{historiaId}/anexo-diligencia', 'Administracion\DiligenciaController@obtenerAnexoDiligenciaHistoria')
            ->name('diligencias.anexoDiligenciaHistoria');
        Route::get('{id}/historia/{historiaId}/anexo-costos', 'Administracion\DiligenciaController@obtenerAnexoCostosHistoria')
            ->name('diligencias.anexoCostosHistoria');
        Route::get('{id}/historia/{historiaId}/anexo-recibido', 'Administracion\DiligenciaController@obtenerAnexoRecibidoHistoria')
            ->name('diligencias.anexoRecibidoHistoria');

        // Cargar historia
        Route::get('{id}/historia/{historiaId}', 'Administracion\DiligenciaController@cargarHistorico')
            ->name('diligencias.cargarHistorico');
        Route::get('{id}/anexo-costos', 'Administracion\DiligenciaController@obtenerAnexoCostos')
            ->name('diligencias.anexoCostos');
        Route::get('{id}/anexo-recibido', 'Administracion\DiligenciaController@obtenerAnexoRecibido')
            ->name('diligencias.anexoRecibido');

        //Consulta
        Route::get('consulta-cliente', 'Administracion\DiligenciaController@obtenerDiligenciasPorCliente')
            ->name('diligencias.consulta');
    });
    Route::get('diligencias', 'Administracion\DiligenciaController@index')->name('diligencias.index');
    Route::get('diligencias/{id}', 'Administracion\DiligenciaController@show')->name('diligencias.show')
        ->middleware(['permission:ListarDiligencia|ListarDiligenciaTramite|ListarConsultaDiligencia']);
    Route::post('diligencias', 'Administracion\DiligenciaController@store')->name('diligencias.store')
        ->middleware(['permission:CrearDiligencia|CrearConsultaDiligencia|ModificarDiligenciaTramite']);
    Route::put('diligencias/{id}', 'Administracion\DiligenciaController@update')->name('diligencias.update')
        ->middleware(['permission:ModificarDiligencia|ModificarDiligenciaTramite']);
    Route::delete('diligencias/{id}', 'Administracion\DiligenciaController@destroy')->name('diligencias.delete')
        ->middleware(['permission:EliminarDiligencia']);

    // Tipos de notificaciones
    Route::group(["prefix" => "tipos-notificaciones"],function(){
        Route::get('create', 'Configuracion\TipoNotificacionController@create')->name('tipos-notificaciones.create')
            ->middleware(['permission:CrearTipoNotificacion|ModificarTipoNotificacion']);
        Route::get('index-template', 'Configuracion\TipoNotificacionController@getIndexTemplate')->name('tipos-notificaciones.indexTemplate')
            ->middleware(['permission:ListarTipoNotificacion']);
    });
    Route::get('tipos-notificaciones', 'Configuracion\TipoNotificacionController@index')->name('tipos-notificaciones.index');
    Route::get('tipos-notificaciones/{id}', 'Configuracion\TipoNotificacionController@show')->name('tipos-notificaciones.show')
        ->middleware(['permission:ListarTipoNotificacion']);
    Route::post('tipos-notificaciones', 'Configuracion\TipoNotificacionController@store')->name('tipos-notificaciones.store')
        ->middleware(['permission:CrearTipoNotificacion']);
    Route::put('tipos-notificaciones/{id}', 'Configuracion\TipoNotificacionController@update')->name('tipos-notificaciones.update')
        ->middleware(['permission:ModificarTipoNotificacion']);
    Route::delete('tipos-notificaciones/{id}', 'Configuracion\TipoNotificacionController@destroy')->name('tipos-notificaciones.delete')
        ->middleware(['permission:EliminarTipoNotificacion']);

    // Vigilancia de procesos
    Route::group(["prefix" => "vigilancias-procesos"],function(){
        Route::get('index-template', 'Administracion\VigilanciaProcesoController@getIndexTemplate')->name('vigilancias-procesos.indexTemplate')
            ->middleware(['permission:ListarVigilanciaProceso']);
        Route::get('excel', 'Administracion\VigilanciaProcesoController@getExcel')->name('vigilancias-procesos.getExcel')
            ->middleware(['permission:ListarVigilanciaProceso']);
    });
    Route::get('vigilancias-procesos', 'Administracion\VigilanciaProcesoController@index')->name('vigilancias-procesos.index');

    // Salarios mínimos
    Route::group(["prefix" => "salarios-minimos"],function(){
        Route::get('create', 'Configuracion\SalarioMinimoController@create')->name('salarios-minimos.create')
            ->middleware(['permission:CrearSalarioMinimo|ModificarSalarioMinimo']);
        Route::get('index-template', 'Configuracion\SalarioMinimoController@getIndexTemplate')->name('salarios-minimos.indexTemplate')
            ->middleware(['permission:ListarSalarioMinimo']);
    });
    Route::get('salarios-minimos', 'Configuracion\SalarioMinimoController@index')->name('salarios-minimos.index');
    Route::get('salarios-minimos/{id}', 'Configuracion\SalarioMinimoController@show')->name('salarios-minimos.show')
        ->middleware(['permission:ListarSalarioMinimo']);
    Route::post('salarios-minimos', 'Configuracion\SalarioMinimoController@store')->name('salarios-minimos.store')
        ->middleware(['permission:CrearSalarioMinimo']);
    Route::put('salarios-minimos/{id}', 'Configuracion\SalarioMinimoController@update')->name('salarios-minimos.update')
        ->middleware(['permission:ModificarSalarioMinimo']);
    Route::delete('salarios-minimos/{id}', 'Configuracion\SalarioMinimoController@destroy')->name('salarios-minimos.delete')
        ->middleware(['permission:EliminarSalarioMinimo']);

    // TES
    Route::group(["prefix" => "TES"],function(){
        Route::get('create', 'Configuracion\TESController@create')->name('TES.create')
            ->middleware(['permission:CrearTES|ModificarTES']);
        Route::get('index-template', 'Configuracion\TESController@getIndexTemplate')->name('TES.indexTemplate')
            ->middleware(['permission:ListarTES']);
    });
    Route::get('TES', 'Configuracion\TESController@index')->name('TES.index');
    Route::get('TES/{id}', 'Configuracion\TESController@show')->name('TES.show')
        ->middleware(['permission:ListarTES']);
    Route::post('TES', 'Configuracion\TESController@store')->name('TES.store')
        ->middleware(['permission:CrearTES']);
    Route::put('TES/{id}', 'Configuracion\TESController@update')->name('TES.update')
        ->middleware(['permission:ModificarTES']);
    Route::delete('TES/{id}', 'Configuracion\TESController@destroy')->name('TES.delete')
        ->middleware(['permission:EliminarTES']);

    // IPC
    Route::group(["prefix" => "IPC"],function(){
        Route::get('create', 'Configuracion\IPCController@create')->name('IPC.create')
            ->middleware(['permission:CrearIPC|ModificarIPC']);
        Route::get('index-template', 'Configuracion\IPCController@getIndexTemplate')->name('IPC.indexTemplate')
            ->middleware(['permission:ListarIPC']);
    });
    Route::get('IPC', 'Configuracion\IPCController@index')->name('IPC.index');
    Route::get('IPC/{id}', 'Configuracion\IPCController@show')->name('IPC.show')
        ->middleware(['permission:ListarIPC']);
    Route::post('IPC', 'Configuracion\IPCController@store')->name('IPC.store')
        ->middleware(['permission:CrearIPC']);
    Route::put('IPC/{id}', 'Configuracion\IPCController@update')->name('IPC.update')
        ->middleware(['permission:ModificarIPC']);
    Route::delete('IPC/{id}', 'Configuracion\IPCController@destroy')->name('IPC.delete')
        ->middleware(['permission:EliminarIPC']);

    // Auxiliares de la justicia
    Route::group(["prefix" => "auxiliares-justicia"],function(){
        Route::get('create', 'Configuracion\AuxiliarJusticiaController@create')->name('auxiliares-justicia.create')
            ->middleware(['permission:CrearAuxiliarJusticia|ModificarAuxiliarJusticia']);
        Route::get('index-template', 'Configuracion\AuxiliarJusticiaController@getIndexTemplate')
            ->name('auxiliares-justicia.indexTemplate')
            ->middleware(['permission:ListarAuxiliarJusticia']);
        Route::get('importar-template', 'Configuracion\AuxiliarJusticiaController@getImportarTemplate')
            ->name('auxiliares-justicia.importarTemplate');

        // Carga de datos
        Route::post('excel', 'Configuracion\AuxiliarJusticiaController@importar')->name('auxiliares-justicia.importar')
            ->middleware(['permission:CrearAuxiliarJusticia']);
    });
    Route::get('auxiliares-justicia', 'Configuracion\AuxiliarJusticiaController@index')->name('auxiliares-justicia.index');
    Route::get('auxiliares-justicia/{id}', 'Configuracion\AuxiliarJusticiaController@show')->name('auxiliares-justicia.show')
        ->middleware(['permission:ListarAuxiliarJusticia']);
    Route::post('auxiliares-justicia', 'Configuracion\AuxiliarJusticiaController@store')->name('auxiliares-justicia.store')
        ->middleware(['permission:CrearAuxiliarJusticia']);
    Route::put('auxiliares-justicia/{id}', 'Configuracion\AuxiliarJusticiaController@update')->name('auxiliares-justicia.update')
        ->middleware(['permission:ModificarAuxiliarJusticia']);
    Route::delete('auxiliares-justicia/{id}', 'Configuracion\AuxiliarJusticiaController@destroy')->name('auxiliares-justicia.delete')
        ->middleware(['permission:EliminarAuxiliarJusticia']);

    // Procesos hoy
    Route::group(["prefix" => "procesos-hoy"],function(){
        Route::get('create', 'Administracion\ProcesoHoyController@create')->name('procesos-hoy.create')
            ->middleware(['permission:CrearProcesoHoy|ModificarProcesoHoy']);
        Route::get('index-template', 'Administracion\ProcesoHoyController@getIndexTemplate')
            ->name('procesos-hoy.indexTemplate')
            ->middleware(['permission:ListarProcesoHoy']);
        Route::get('importar-template', 'Administracion\ProcesoHoyController@getImportarTemplate')
            ->name('procesos-hoy.importarTemplate');
        Route::get('excel', 'Administracion\ProcesoHoyController@getExcel')->name('procesos-hoy.getExcel')
            ->middleware(['permission:ListarProcesoHoy']);
    });
    Route::get('procesos-hoy', 'Administracion\ProcesoHoyController@index')->name('procesos-hoy.index');
    Route::get('procesos-hoy/{id}', 'Administracion\ProcesoHoyController@show')->name('v.show')
        ->middleware(['permission:ListarProcesoHoy']);

    // Actuaciones
    Route::group(["prefix" => "actuaciones"],function(){
        Route::get('create', 'Administracion\ActuacionController@create')->name('actuaciones.create')
            ->middleware(['permission:CrearActuacion|ModificarActuacion']);
        Route::get('index-template', 'Administracion\ActuacionController@getIndexTemplate')
            ->name('actuaciones.indexTemplate')
            ->middleware(['permission:ListarActuacion']);
        Route::get('demandantes-demandados/template', 'Administracion\ActuacionController@getDemandanteDemandadoTemplate')
            ->name('actuaciones.demandanteDemandadoTemplate');
        Route::get('procesos-juzgado-fecha/template', 'Administracion\ActuacionController@getProcesoJuzgadoFechaTemplate')
            ->name('actuaciones.procesoJuzgadoFechaTemplate');
        Route::get('procesos-hoy/template', 'Administracion\ActuacionController@getProcesoHoyTemplate')
            ->name('actuaciones.procesoHoyTemplate');
        Route::get('autos/template', 'Administracion\ActuacionController@getAutoTemplate')
            ->name('actuaciones.autoTemplate');
        Route::get('personalizados-digitales/template', 'Administracion\ActuacionController@getPersonalizadoDigitalTemplate')
            ->name('actuaciones.personalizadoDigitalTemplate');
        Route::get('reportes-judiciales/template', 'Administracion\ActuacionController@getReporteJudicialTemplate')
            ->name('actuaciones.reporteJudicialTemplate');
        Route::get('audiencias-pendientes/template', 'Administracion\ActuacionController@getAudienciaPendienteTemplate')
            ->name('actuaciones.audienciaPendienteTemplate');
        Route::get('audiencias-cumplidas/template', 'Administracion\ActuacionController@getAudienciaCumplidaTemplate')
            ->name('actuaciones.audienciaCumplidaTemplate');
        Route::get('termino-actuacion/template', 'Administracion\ActuacionController@getTerminoActuacionTemplate')
            ->name('actuaciones.audienciaCumplidaTemplate');

        // Archivo anexo
        Route::get('{id}/archivo-anexo', 'Administracion\ActuacionController@obtenerArchivoAnexo')
            ->name('actuaciones.archivoAnexo');
        Route::get('{id}/archivo-estado-cartelera', 'Administracion\ActuacionController@obtenerArchivoEstadoCartelera')
            ->name('actuaciones.archivoEstadoCartelera');

        // Carga de datos
        Route::get('importar-template', 'Administracion\ActuacionController@getImportarTemplate')
            ->name('actuaciones.importarTemplate')
            ->middleware(['permission:ImportarActuaciones']);
        Route::get('importar-excel', 'Administracion\ActuacionController@getExcelErrorImportacion')
            ->name('actuaciones.excelErrorImportacion')
            ->middleware(['permission:ImportarActuaciones']);
        Route::post('importar-excel', 'Administracion\ActuacionController@importar')
            ->name('actuaciones.importar')
            ->middleware(['permission:ImportarActuaciones']);
        Route::post('importar-texto', 'Administracion\ActuacionController@importarTexto')
            ->name('actuaciones.importarTexto')
            ->middleware(['permission:ImportarActuaciones']);

        // Reportes
        Route::get('reporte', 'Administracion\ActuacionController@obtenerReporte')
            ->name('actuaciones.reporte');
        Route::get('reporte-por-empresa', 'Administracion\ActuacionController@obtenerReportePorEmpresa')
            ->name('actuaciones.reportePorEmpresa');
        Route::get('audiencia-pendiente', 'Administracion\ActuacionController@obtenerAudienciasPendientes')
            ->name('actuaciones.audienciaPendiente'); //->middleware(['permission:ListarAudienciaPendiente']);
        Route::get('audiencia-cumplida', 'Administracion\ActuacionController@obtenerAudienciasCumplidas')
            ->name('actuaciones.audienciaCumplida')->middleware(['permission:ListarAudienciaPendiente']);
        Route::get('actuaciones-pendientes', 'Administracion\ActuacionController@obtenerActuacionesPendientesYCumplidas')
            ->name('actuaciones.actuacionPendiente')->middleware(['permission:ListarTerminoActuacion']);
        Route::get('actuaciones-pendientes/excel', 'Administracion\ActuacionController@obtenerActuacionesPendientesExcel')
            ->name('actuaciones.actuacionPendiente')->middleware(['permission:ListarTerminoActuacion']);
        Route::put('actuaciones-pendientes', 'Administracion\ActuacionController@modificarActuacionesPendientes')
            ->middleware(['permission:ModificarTerminoActuacion']);
        Route::get('actuaciones-cumplidas', 'Administracion\ActuacionController@obtenerActuacionesPendientesYCumplidas')
            ->name('actuaciones.actuacionCumplida')->middleware(['permission:ListarTerminoActuacion']);
        Route::get('reporte/excel', 'Administracion\ActuacionController@obtenerReporteExcel')
            ->name('actuaciones.reporteExcel');
        Route::get('reporte-por-empresa/excel', 'Administracion\ActuacionController@obtenerReportePorEmpresaExcel')
            ->name('actuaciones.reportePorEmpresaExcel');
        Route::get('demandantes-demandados/excel', 'Administracion\ActuacionController@obtenerDemandanteDemandadoExcel')
            ->name('actuaciones.demandanteDemandadoExcel');
        Route::get('procesos-juzgado-fecha/excel', 'Administracion\ActuacionController@obtenerProcesoJuzgadoFechaExcel')
            ->name('actuaciones.procesoJuzgadoFechaExcel');
        Route::get('procesos-hoy/excel', 'Administracion\ActuacionController@obtenerProcesoHoyExcel')
            ->name('actuaciones.procesoHoyExcel');
        Route::get('autos/excel', 'Administracion\ActuacionController@obtenerAutoExcel')
            ->name('actuaciones.autoExcel');
        Route::get('personalizados-digitales/excel', 'Administracion\ActuacionController@obtenerPersonalizadoDigitalExcel')
            ->name('actuaciones.personalizadoDigitalExcel');
        Route::get('reportes-judiciales/excel', 'Administracion\ActuacionController@obtenerReporteJudicialExcel')
            ->name('actuaciones.reporteJudicialExcel');
        Route::get('audiencias-pendientes/excel', 'Administracion\ActuacionController@obtenerAudienciaPendienteExcel')
            ->name('actuaciones.audienciaPendienteExcel');
        Route::get('audiencias-cumplidas/excel', 'Administracion\ActuacionController@obtenerAudienciaCumplidaExcel')
            ->name('actuaciones.audienciaCumplidaExcel');
    });
    Route::get('actuaciones', 'Administracion\ActuacionController@index')->name('actuaciones.index');
    Route::get('actuaciones/{id}', 'Administracion\ActuacionController@show')->name('actuaciones.show')
        ->middleware(['permission:ListarActuacion']);
    Route::post('actuaciones', 'Administracion\ActuacionController@store')->name('actuaciones.store')
        ->middleware(['permission:CrearActuacion']);
    Route::put('actuaciones/{id}', 'Administracion\ActuacionController@update')->name('actuaciones.update')
        ->middleware(['permission:ModificarActuacion']);
    Route::delete('actuaciones/{id}', 'Administracion\ActuacionController@destroy')->name('actuaciones.delete')
        ->middleware(['permission:EliminarActuacion']);
    Route::put('actuaciones', 'Administracion\ActuacionController@updateMasivo')->name('actuaciones.updateMasivo')
        ->middleware(['permission:ModificarActuacion']);
    Route::put('actuaciones/todo/eliminar', 'Administracion\ActuacionController@destroyMasivo')->name('actuaciones.deleteMasivo')
        ->middleware(['permission:EliminarActuacion']);

    // Procesos por juzgado y fecha
    Route::group(["prefix" => "procesos-juzgados-fechas"],function(){
        Route::get('index-template', 'Administracion\ProcesoJuzgadoFechaController@getIndexTemplate')->name('procesos-juzgados-fechas.indexTemplate')
            ->middleware(['permission:ListarProcesoJuzgadoFecha']);
        Route::get('excel', 'Administracion\ProcesoJuzgadoFechaController@getExcel')->name('procesos-juzgados-fechas.getExcel')
            ->middleware(['permission:ListarProcesoJuzgadoFecha']);
    });
    Route::get('procesos-juzgados-fechas', 'Administracion\ProcesoJuzgadoFechaController@index')->name('procesos-juzgados-fechas.index');

    // Procesos por juzgado y fecha
    Route::group(["prefix" => "busquedas-demandantes-demandados"],function(){
        Route::get('index-template', 'Administracion\BusqDemandanteDemandadoController@getIndexTemplate')->name('busquedas-demandantes-demandados.indexTemplate')
            ->middleware(['permission:ListarBusqDemandanteDemandado']);
        Route::get('excel', 'Administracion\BusqDemandanteDemandadoController@getExcel')->name('busquedas-demandantes-demandados.getExcel')
            ->middleware(['permission:ListarBusqDemandanteDemandado']);
    });
    Route::get('busquedas-demandantes-demandados', 'Administracion\BusqDemandanteDemandadoController@index')->name('busquedas-demandantes-demandados.index');

    // Procesos por área
    Route::group(["prefix" => "procesos-areas"],function(){
        Route::get('index-template', 'Administracion\ProcesoAreaController@getIndexTemplate')->name('procesos-areas.indexTemplate')
            ->middleware(['permission:ListarProcesoArea']);
        Route::get('excel', 'Administracion\ProcesoAreaController@getExcel')->name('procesos-areas.getExcel')
            ->middleware(['permission:ListarProcesoArea']);
    });
    Route::get('procesos-areas', 'Administracion\ProcesoAreaController@index')->name('procesos-areas.index');

    // Información General de Procesos
    Route::group(["prefix" => "procesos-generales"],function(){
        Route::get('index-template', 'Monitoreo\ProcesoGeneralController@getIndexTemplate')->name('procesos-generales.indexTemplate')
            ->middleware(['permission:ListarProcesoGeneral']);
        Route::get('excel', 'Monitoreo\ProcesoGeneralController@getExcel')->name('procesos-generales.getExcel')
            ->middleware(['permission:ListarProcesoGeneral']);
    });
    Route::get('procesos-generales', 'Monitoreo\ProcesoGeneralController@index')->name('procesos-generales.index');
    Route::get('procesos-generales/{id}', 'Monitoreo\ProcesoGeneralController@show')->name('procesos-generales.show')
        ->middleware(['permission:ListarProcesoGeneral']);
    Route::put('procesos-generales/{id}', 'Monitoreo\ProcesoGeneralController@update')->name('procesos-generales.update')
        ->middleware(['permission:ModificarProcesoGeneral']);

    // Auxiliares de la justicia Errores
    Route::group(["prefix" => "auxiliares-justicia-errores"],function(){
        Route::get('index-template', 'Importacion\AuxiliarJusticiaCargaErrorController@getIndexTemplate')
            ->name('auxiliares-justicia-errores.indexTemplate')
            ->middleware(['permission:ListarAuxiliarJusticia']);
        Route::get('excel', 'Importacion\AuxiliarJusticiaCargaErrorController@getExcel')->name('auxiliares-justicia-errores.getExcel')
            ->middleware(['permission:ListarAuxiliarJusticia']);
        Route::get('importar-template', 'Importacion\AuxiliarJusticiaCargaErrorController@getImportarTemplate')
            ->name('auxiliares-justicia-errores.importarTemplate');
    });
    Route::get('auxiliares-justicia-errores', 'Importacion\AuxiliarJusticiaCargaErrorController@index')->name('auxiliares-justicia-errores.index');


    // Consulta de autos
    Route::group(["prefix" => "consultas-autos"],function(){
        Route::get('create', 'Administracion\ConsultaAutoController@create')->name('consultas-autos.create')
            ->middleware(['permission:CrearConsultaAuto|ModificarConsultaAuto']);
        Route::get('index-template', 'Administracion\ConsultaAutoController@getIndexTemplate')
            ->name('consultas-autos.indexTemplate')
            ->middleware(['permission:ListarConsultaAuto']);
        Route::get('importar-template', 'Administracion\ConsultaAutoController@getImportarTemplate')
            ->name('consultas-autos.importarTemplate');
        Route::get('excel', 'Administracion\ConsultaAutoController@getExcel')->name('consultas-autos.getExcel')
            ->middleware(['permission:ListarConsultaAuto']);
    });
    Route::get('consultas-autos', 'Administracion\ConsultaAutoController@index')->name('consultas-autos.index');
    Route::get('consultas-autos/{id}', 'Administracion\ConsultaAutoController@show')->name('v.show')
        ->middleware(['permission:ListarConsultaAuto']);

    // Consulta de remates
    Route::group(["prefix" => "consultas-remates"],function(){
        Route::get('index-template', 'Administracion\ConsultaRemateController@getIndexTemplate')
            ->name('consultas-remates.indexTemplate')
            ->middleware(['permission:ListarConsultaRemate']);
        Route::get('excel', 'Administracion\ConsultaRemateController@getExcel')->name('consultas-remates.getExcel')
            ->middleware(['permission:ListarConsultaRemate']);
    });
    Route::get('consultas-remates', 'Administracion\ConsultaRemateController@index')->name('consultas-remates.index');

    // Consulta de remanentes
    Route::group(["prefix" => "consultas-remanentes"],function(){
        Route::get('index-template', 'Administracion\ConsultaRemanenteController@getIndexTemplate')
            ->name('consultas-remanentes.indexTemplate')
            ->middleware(['permission:ListarConsultaRemanente']);
        Route::get('excel', 'Administracion\ConsultaRemanenteController@getExcel')->name('consultas-remanentes.getExcel')
            ->middleware(['permission:ListarConsultaRemanente']);
    });
    Route::get('consultas-remanentes', 'Administracion\ConsultaRemanenteController@index')->name('consultas-remanentes.index');

    // Consultas

    // Remates
    Route::group(["prefix" => "remates"],function(){
        Route::get('template', 'Administracion\ActuacionController@getRemateTemplate')
            ->name('actuaciones.remateTemplate');

        // Achivos excel
        Route::get('excel', 'Administracion\ActuacionController@obtenerRematesExcel')
            ->name('actuaciones.obtenerRematesExcel');
    });
    Route::get('remates', 'Administracion\ActuacionController@obtenerRemates')
        ->name('actuaciones.obtenerRemates');

    // Remanentes
    Route::group(["prefix" => "remanentes"],function(){
        Route::get('template', 'Administracion\ActuacionController@getRemanenteTemplate')
            ->name('actuaciones.remanenteTemplate');

        // Archivos excel
        Route::get('excel', 'Administracion\ActuacionController@obtenerRemanentesExcel')
            ->name('actuaciones.obtenerRemanentesExcel');
    });
    Route::get('remanentes', 'Administracion\ActuacionController@obtenerRemanentes')
        ->name('actuaciones.obtenerRemanentes');

    // Parámetros constantes
    Route::group(["prefix" => "parametros-constantes"],function(){
        Route::get('create', 'Administracion\ParametroConstanteController@create')->name('parametros-constantes.create')
            ->middleware(['permission:CrearParametroConstante|ModificarParametroConstante']);
        Route::get('index-template', 'Administracion\ParametroConstanteController@getIndexTemplate')->name('parametros-constantes.indexTemplate')
            ->middleware(['permission:ListarParametroConstante']);
        Route::get('excel', 'Administracion\ParametroConstanteController@getExcel')->name('parametros-constantes.obtenerExcel')
            ->middleware(['permission:CrearParametroConstante|ModificarParametroConstante']);
    });
    Route::get('parametros-constantes', 'Administracion\ParametroConstanteController@index')->name('parametros-constantes.index');
    Route::get('parametros-constantes/{id}', 'Administracion\ParametroConstanteController@show')->name('parametros-constantes.show')
        ->middleware(['permission:ListarParametroConstante']);
    Route::post('parametros-constantes', 'Administracion\ParametroConstanteController@store')->name('parametros-constantes.store')
        ->middleware(['permission:CrearParametroConstante']);
    Route::put('parametros-constantes/{id}', 'Administracion\ParametroConstanteController@update')->name('parametros-constantes.update')
        ->middleware(['permission:ModificarParametroConstante']);
    Route::delete('parametros-constantes/{id}', 'Administracion\ParametroConstanteController@destroy')->name('parametros-constantes.delete')
        ->middleware(['permission:EliminarParametroConstante']);

    // Auditoria procesos rama judicial
    Route::group(["prefix" => "auditoria-procesos"],function(){
        Route::get('index-template', 'Administracion\AuditoriaProcesoController@getIndexTemplate')->name('auditoria-procesos.indexTemplate')
            ->middleware(['permission:ListarAuditoriaProcesos']);
        Route::get('excel', 'Administracion\AuditoriaProcesoController@getExcelAuditorias')->name('auditoria-procesos.excelAuditorias')
            ->middleware(['permission:DescargarAuditoriaProcesos']);
        Route::get('novedades/excel', 'Administracion\AuditoriaProcesoController@getExcelNovedades')->name('auditoria-procesos.excelNovedades')
            ->middleware(['permission:DescargarNovedadProcesos']);
    });
    Route::get('auditoria-procesos', 'Administracion\AuditoriaProcesoController@index')->name('auditoria-procesos.index')
        ->middleware(['permission:ListarAuditoriaProcesos']);
    Route::post('auditoria-procesos', 'Administracion\AuditoriaProcesoController@store')->name('auditoria-procesos.store')
        ->middleware(['permission:ProgramarAuditoriaProcesos']);
});
