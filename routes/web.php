<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Exports\ConceptosEconomicosExport;
use App\Model\Seguridad\TokenDeAcceso;
use App\Model\Seguridad\Usuario;
use App\Repositories\ConectorRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;

Route::get('/conector', function () {
    $conectorService = new ConectorRepository();
    return $conectorService->get();
});

Route::get('/', function () {
    return view('start');
});

Route::get('/login', function () {
    return view('login');
});

Route::get('/app', function () {
    return view('welcome');
});

Route::get('/restablecer-password', function () {
    return view('recuperar-password');
});

Route::get('/restablecer-password/{token}', function ($token) {
    $currentTime = Carbon::now(new DateTimeZone('America/Bogota'));
    $tokenDeAcceso = TokenDeAcceso::where('token', $token)->first();
    $tokenValido = false;
    if($tokenDeAcceso->estado && $currentTime->lt(Carbon::parse($tokenDeAcceso->fecha_expiracion))){
        $tokenValido = true;
    }
    return view('formulario-recuperar-password', ['token' => $token, 'tokenValido' => $tokenValido]);
});

Route::post('/enviar-enlace-recuperacion-password', function (Request $request) {
    $usuario = null;
    $usuarios = Usuario::whereDocumento($request->username)->get();
    if(!isset($usuarios) || count($usuarios) == 0){
        return response([
            "messages" => [__("common.usuario_sin_registro_en_el_sistema")]
        ], Response::HTTP_BAD_REQUEST);
    }else{
        $usuario = Usuario::find($usuarios[0]->id);
        $today = Carbon::now(new DateTimeZone('America/Bogota'));
        if(
            isset($usuario) && $usuario->estado == false ||
            (isset($usuario->fecha_vencimiento) && Carbon::parse($usuario->fecha_vencimiento)->lt($today))
        ){
            return response([
                "messages" => [__("common.usuario_inactivo", ["identificacion" => $request->username])]
            ], Response::HTTP_BAD_REQUEST);
        }

        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $token = substr(str_shuffle(str_repeat($pool, 5)), 0, 32);
        $tokenDeAcceso = TokenDeAcceso::create([
            'fecha_expiracion' => Carbon::now(new DateTimeZone('America/Bogota'))->addHours(5),
            'usuario_id' => $usuario->id,
            'token' => $token
        ]);
        if(!isset($tokenDeAcceso)){
            return response([
                "messages" => [__("common.problema_inesperado_con_servidor_correos")]
            ], Response::HTTP_BAD_REQUEST);
        }else{
            Mail::send('mail.template', ['dto' => [
                'template' => 'mail.partials.restablecer-password',
                'titulo' => __('common.titulo_correo_restablecer_password'),
                'token' => $token
            ]], function ($mail) use ($usuario){
                $mail->subject(__('common.asunto_correo_restablecer_password'));
                $mail->to($usuario->email_uno, $usuario->nombre);
            });
        }

        return response(null, Response::HTTP_OK);
    }
});

Route::put('/usuarios/password', function (Request $request) {
    DB::beginTransaction();
    try{
        $currentTime = Carbon::now(new DateTimeZone('America/Bogota'));
        $tokenDeAcceso = TokenDeAcceso::where('token', $request->token)->first();
        if(!($tokenDeAcceso->estado && $currentTime->lt(Carbon::parse($tokenDeAcceso->fecha_expiracion)))){
            return response([
                "messages" => ["El enlace de recuperación de contraseña ha expirado."]
            ], Response::HTTP_BAD_REQUEST);
        }

        $usuario = Usuario::find($tokenDeAcceso->usuario_id);
        $user = $usuario->user;
        $user->fill([
            'password' => Hash::make($request->password)
        ]);
        $guardado = $user->save();
        if(!$guardado){
            throw new Exception;
        }

        $tokenDeAcceso->fill([
            'estado' => false
        ]);
        $guardado = $tokenDeAcceso->save();
        if(!$guardado){
            throw new Exception;
        }

        DB::commit();
        return response(null, Response::HTTP_OK);
    }catch (Exception $e){
        DB::rollback();
        return response(null, Response::HTTP_INTERNAL_SERVER_ERROR);
    }
});

Route::get('/paginacion-template', function () {
    return view('paginacion-template');
});

Route::get('/paginacion-basica-template', function () {
    return view('paginacion-basica-template');
});

Route::get('/excel', function () {
    return Excel::download(new ConceptosEconomicosExport, 'conceptosEconomicos.xlsx');
});

Route::get('welcome', function (){
    Mail::send('emails.welcome');
});
