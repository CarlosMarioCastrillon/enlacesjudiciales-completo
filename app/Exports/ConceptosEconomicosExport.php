<?php

namespace App\Exports;

use App\Model\Configuracion\ConceptoEconomico;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;

class ConceptosEconomicosExport implements FromQuery
{

    use Exportable;

    public function __construct($dto)
    {
        $this->dto = $dto;
    }

    public function query()
    {
        return ConceptoEconomico::query()->whereNombre($this->dto['nombre']);
    }

}
