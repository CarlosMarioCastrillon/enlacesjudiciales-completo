<?php

namespace App\Exports;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ActuacionPendienteExport implements FromQuery, WithHeadings, ShouldAutoSize
{

    use Exportable;

    public function __construct($dto)
    {
        $this->dto = $dto;
    }

   public function query()
   {
       $user = Auth::user();
       $empresa = $user->empresa();
       $this->dto['empresa_id'] = $empresa->id;
       $this->dto['fecha_actual'] = Carbon::today();
       $query = DB::table('actuaciones')
           ->where('actuaciones.empresa_id', '=', $this->dto['empresa_id'])
           ->where('actuaciones.ind_actuacion_audiencia', '=', 'AC')
           ->select(
               'actuaciones.numero_proceso',
               'actuaciones.nombres_demandantes',
               'actuaciones.nombres_demandados',
               'actuaciones.observaciones',
               'actuaciones.fecha_vencimiento_termino',
               'actuaciones.updated_at AS fecha_modificacion',
               'actuaciones.usuario_modificacion_nombre',
               DB::raw("DATEDIFF(fecha_vencimiento_termino, curdate()) as falta")
           );

       if (isset($this->dto['cumplimiento']) && $this->dto['cumplimiento'] == "true"){
           $query->where('actuaciones.estado', '=', '1');
       }else if(isset($this->dto['cumplimiento']) && $this->dto['cumplimiento'] == "false"){
           $query->where('actuaciones.fecha_vencimiento_termino', '>=', $this->dto['fecha_actual'])
               ->whereNull('actuaciones.estado');
       }

       if (isset($this->dto['proceso'])){
           $query->where('actuaciones.numero_proceso', 'like', '%' . $this->dto['proceso'] . '%');
       }
       if (isset($this->dto['actuacion'])){
           $query->where('actuaciones.observaciones', 'like', '%' . $this->dto['actuacion'] . '%');
       }
       if (isset($this->dto['demandante'])){
           $query->where('actuaciones.nombres_demandantes', 'like', '%' . $this->dto['demandante'] . '%');
       }
       if (isset($this->dto['demandado'])){
           $query->where('actuaciones.nombres_demandados', 'like', '%' . $this->dto['demandado'] . '%');
       }

       if(
           (isset($this->dto['vence_hoy']) && $this->dto['vence_hoy'] == "true") ||
           (isset($this->dto['por_vencer']) && $this->dto['por_vencer'] == "true") ||
           (isset($this->dto['pendientes']) && $this->dto['pendientes'] == "true") ||
           (isset($this->dto['mas_de_diz_dias']) && $this->dto['mas_de_diz_dias'] == "true")
       ){
           $dto = $this->dto;
           $query->where(function ($q) use($dto) {
               if(isset($dto['vence_hoy']) && $dto['vence_hoy'] == "true"){
                   $q->orWhere('actuaciones.fecha_vencimiento_termino', $dto['fecha_actual']);
               }
               if(isset($dto['por_vencer']) && $dto['por_vencer'] == "true"){
                   $q->orWhere(function($q){
                       $q->where('actuaciones.fecha_vencimiento_termino', '>=', Carbon::today()->addDays(1)->format("Y-m-d"))
                           ->where('actuaciones.fecha_vencimiento_termino', '<=', Carbon::today()->addDays(3)->format("Y-m-d"));
                   });
               }
               if(isset($dto['pendientes']) && $dto['pendientes'] == "true"){
                   $q->orWhere(function($q){
                       $q->where('actuaciones.fecha_vencimiento_termino', '>=', Carbon::today()->addDays(4)->format("Y-m-d"))
                           ->where('actuaciones.fecha_vencimiento_termino', '<=', Carbon::today()->addDays(10)->format("Y-m-d"));
                   });
               }
               if(isset($dto['mas_de_diz_dias']) && $dto['mas_de_diz_dias'] == "true"){
                   $q->orWhere('actuaciones.fecha_vencimiento_termino', '>', Carbon::today()->addDays(10)->format("Y-m-d"));
               }
           });
       }

       $query->orderBy('actuaciones.fecha_vencimiento_termino', 'desc');

       return $query;
   }

    public function headings(): array
    {
        return [
            "Proceso",
            "Demandante",
            "Demandado",
            "Actuación",
            "Fecha vencimiento",
            "Última actualización",
            "Actualizado por",
            "Falta",
        ];
    }
}
