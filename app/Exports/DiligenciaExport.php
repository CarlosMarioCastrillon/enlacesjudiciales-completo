<?php

namespace App\Exports;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class DiligenciaExport implements FromQuery, WithHeadings, ShouldAutoSize
{

    use Exportable;

    public function __construct($dto)
    {
        $this->dto = $dto;
    }

   public function query()
    {
        $user = Auth::user();
        $usuario = $user->usuario();
        $dto['usuario_id'] = $usuario->id;
        $query = DB::table('diligencias')
            ->join('ciudades', 'ciudades.id', '=', 'diligencias.ciudad_id')
            ->join('departamentos', 'departamentos.id', '=', 'ciudades.departamento_id')
            ->join('tipos_de_diligencias', 'tipos_de_diligencias.id', '=', 'diligencias.tipo_diligencia_id')
            ->join('empresas', 'empresas.id', '=', 'diligencias.empresa_id')
            ->join('procesos_judiciales', 'procesos_judiciales.id', '=', 'diligencias.proceso_id')
            //TODO Mirar con Carlos si este campo es obligatorio
            ->leftJoin('usuarios', 'usuarios.id', '=', 'diligencias.dependiente_id');
            if(isset($this->dto['dependiente'])){
                $query->select(
                    'diligencias.id AS numero_diligencia',
                    'empresas.nombre As empresa_nombre',
                    'diligencias.fecha_diligencia',
                    'diligencias.updated_at AS fecha_modificacion',
                    'procesos_judiciales.numero_proceso As numero_proceso',
                    'departamentos.nombre As departamento_nombre',
                    'ciudades.nombre As ciudad_nombre',
                    'tipos_de_diligencias.nombre As tipo_diligencia_nombre',
                    'diligencias.otros_gastos',
                    'diligencias.costo_diligencia',
                    'diligencias.costo_envio',
                    'diligencias.otros_costos',
                    'diligencias.detalle_valores',
                    'diligencias.observaciones_cliente',
                    'diligencias.observaciones_internas'
                )
                    ->where('diligencias.dependiente_id', '=', $dto['usuario_id'])
                    /*->where('diligencias.estado_diligencia', '=', 4)*/;
            }else {
                $query->select(
                    'diligencias.id AS numero_diligencia',
                    'empresas.nombre As empresa_nombre',
                    'diligencias.fecha_diligencia',
                    'diligencias.updated_at AS fecha_modificacion',
                    'procesos_judiciales.numero_proceso As numero_proceso',
                    'departamentos.nombre As departamento_nombre',
                    'ciudades.nombre As ciudad_nombre',
                    'procesos_judiciales.nombres_demandantes',
                    'procesos_judiciales.nombres_demandados',
                    'tipos_de_diligencias.nombre As tipo_diligencia_nombre',
                    DB::raw('(CASE WHEN diligencias.indicativo_cobro = 1 THEN "COBRAR" 
                    ELSE "NO COBRAR" END) AS indicativo_cobro'),
                    'diligencias.valor_diligencia',
                    'diligencias.gastos_envio',
                    'diligencias.otros_gastos',
                    'diligencias.costo_diligencia',
                    'diligencias.costo_envio',
                    'diligencias.otros_costos',
                    DB::raw('(CASE 
                    WHEN diligencias.estado_diligencia = 1 THEN "SOLICITADO" 
                    WHEN diligencias.estado_diligencia = 2 THEN "COTIZADO" 
                    WHEN diligencias.estado_diligencia = 3 THEN "APROBADO" 
                    WHEN diligencias.estado_diligencia = 4 THEN "TRÁMITE" 
                    WHEN diligencias.estado_diligencia = 5 THEN "TERMINADO" 
                    WHEN diligencias.estado_diligencia = 6 THEN "CONTABILIZADO" 
                    ELSE "FACTURADO" END) AS estado_diligencia'),
                    'usuarios.nombre AS dependiente',
                    'diligencias.detalle_valores',
                    'diligencias.observaciones_cliente',
                    'diligencias.observaciones_internas'
                );
            }
            $query->orderBy('diligencias.estado_diligencia', "desc")
            ->orderBy('diligencias.id', "asc");


        if(isset($this->dto['numero_diligencia'])){
            $query->where('diligencias.id', '=', $this->dto['numero_diligencia']);
        }
        if(isset($this->dto['empresa_cliente'])){
            $query->where('empresas.nombre', 'like', '%' . $this->dto['empresa_cliente'] . '%');
        }
        if(isset($this->dto['numero_proceso'])){
            $query->where('procesos_judiciales.numero_proceso', 'like', '%' . $this->dto['numero_proceso'] . '%');
        }
        if(isset($this->dto['estado_diligencia'])){
            $query->where('diligencias.estado_diligencia', '=', $this->dto['estado_diligencia']);
        }
        if (isset($dto['fecha_inicial'])){
            $query->where('diligencias.updated_at', '>=', $this->dto['fecha_inicial'] . ' 00:00:00');
        }
        if (isset($dto['fecha_final'])){
            $query->where('diligencias.updated_at', '<=', $this->dto['fecha_final'] . ' 23:59:59');
        }

        return $query;
    }
    public function headings(): array
    {
        if(isset($this->dto['dependiente'])){
            return [
                "Número de diligencia",
                "Empresa",
                "Fecha diligencia",
                "Fecha actualización",
                "Número proceso",
                "Departamento",
                "Ciudad",
                "Tipo diligencia",
                "Otros gastos",
                "Costo diligencia",
                "Costo envío",
                "Otros costos",
                "Detalle valores",
                "Observaciones cliente",
                "Observaciones internas",
            ];
        }else {
            return [
                "Número de diligencia",
                "Empresa",
                "Fecha diligencia",
                "Fecha actualización",
                "Número proceso",
                "Departamento",
                "Ciudad",
                "Demandante",
                "Demandado",
                "Tipo diligencia",
                "Indicativo Cobro",
                "Valor diligencia",
                "Gastos envío",
                "Otros gastos",
                "Costo diligencia",
                "Costo envío",
                "Otros costos",
                "Estado",
                "Dependiente",
                "Detalle valores",
                "Observaciones cliente",
                "Observaciones internas",
            ];
        }
    }
}
