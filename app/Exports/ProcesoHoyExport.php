<?php

namespace App\Exports;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ProcesoHoyExport implements FromQuery, WithHeadings, ShouldAutoSize
{

    use Exportable;

    public function __construct($dto)
    {
        $this->dto = $dto;
    }

    public function query()
    {
       // Datos de sesión
       $user = Auth::user();
       $empresa = $user->empresa();

       $query = DB::table('actuaciones')
           ->join('ciudades', 'ciudades.id', '=', 'actuaciones.ciudad_id')
           ->join('juzgados', 'juzgados.id', '=', 'actuaciones.juzgado_id')
           ->join('despachos', 'despachos.id', '=', 'actuaciones.despacho_id')
           ->join('procesos_judiciales', 'procesos_judiciales.id', '=', 'actuaciones.proceso_id')
           ->where('actuaciones.empresa_id', '=', $empresa->id)
           ->select(
               DB::raw('DATE(actuaciones.created_at) AS fecha_proceso'),
               'ciudades.nombre AS ciudad_nombre',
               'juzgados.nombre AS juzgado_nombre',
               'despachos.despacho AS despacho',
               'actuaciones.numero_proceso',
               'actuaciones.nombres_demandantes',
               'actuaciones.nombres_demandados',
               'actuaciones.anotacion',
               'actuaciones.actuacion',
               'actuaciones.fecha_registro',
               'actuaciones.fecha_actuacion',
               'actuaciones.fecha_inicio_termino',
               'actuaciones.fecha_vencimiento_termino',
               DB::raw("IF(actuaciones.tipo_movimiento = 'AR', 'Consulta rama', 'Estado cartelera') AS tipo_de_actuacion"),
               'actuaciones.created_at AS fecha_creacion'
           );

       if (isset($this->dto['fecha_inicial'])){
           $query->where('actuaciones.fecha_actuacion', '>=', $this->dto['fecha_inicial']);
       }
       if (isset($this->dto['fecha_final'])){
           $query->where('actuaciones.fecha_actuacion', '<=', $this->dto['fecha_final']);
       }

       $query->orderBy('actuaciones.fecha_actuacion', 'desc')
           ->orderBy("actuaciones.numero_proceso", "asc");

       return $query;
    }

    public function headings(): array
    {
        return [
            "Fecha proceso",
            "Ciudad",
            "Juzgado",
            "Despacho",
            "Número proceso",
            "Demandante",
            "Demandado",
            "Anotación",
            "Actuación",
            "Fecha registro",
            "Fecha actuación",
            "Fecha inicio término",
            "Fecha vencimiento término",
            "Tipo de actuación",
            "Fecha creación"
        ];
    }
}
