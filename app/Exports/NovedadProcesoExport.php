<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class NovedadProcesoExport implements FromCollection, WithHeadings, WithColumnFormatting, ShouldAutoSize
{

    use Exportable;

    protected $programacionId;

    public function __construct($programacionId)
    {
        $this->programacionId = $programacionId;
    }

    public function collection()
    {
        $novedades = DB::table('novedad_procesos')
            ->select(
                'ciudad',
                'despacho AS juzgado_actual',
                DB::raw('null AS numero_despacho'),
                'numero_proceso',
                'demandante',
                'demandado',
                DB::raw("CONCAT(descripcion_actuacion, ' - ', descripcion_anotacion) AS actuacion"),
                'fecha_registro',
                'fecha_actuacion',
                'fecha_inicio_termino',
                'fecha_finaliza_termino',
                'fecha_ejecucion',
                'fecha_proceso',
                'programacion_auditoria_proceso_id'
            )
            ->where('programacion_auditoria_proceso_id', $this->programacionId)
            ->orderBy('id', 'asc')->get();

        foreach ($novedades as &$novedad){
            $intVal = (int) filter_var($novedad->juzgado_actual, FILTER_SANITIZE_NUMBER_INT);
            if(isset($intVal)){
                $novedad->numero_despacho = sprintf("%03d", $intVal);
            }
        }

        return $novedades;
    }

    public function query()
    {

    }

    public function headings(): array
    {
        return [
            "Ciudad",
            "Juzgado actual",
            "Número despacho actual",
            "Número proceso",
            "Demandante",
            "Demandado",
            "Actuación",
            "Fecha registro",
            "Fecha actuación",
            "Fecha inicio termino",
            "Fecha vencimiento termino",
            'Fecha ejecución',
            'Fecha proceso',
            "Programación #"
        ];
    }

    public function columnFormats(): array
    {
        return [
            'H' => NumberFormat::FORMAT_DATE_YYYYMMDD,
            'I' => NumberFormat::FORMAT_DATE_YYYYMMDD,
            'J' => NumberFormat::FORMAT_DATE_YYYYMMDD,
            'K' => NumberFormat::FORMAT_DATE_YYYYMMDD,
            'L' => NumberFormat::FORMAT_DATE_YYYYMMDD,
            'M' => NumberFormat::FORMAT_DATE_YYYYMMDD
        ];
    }
}
