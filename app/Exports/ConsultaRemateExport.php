<?php

namespace App\Exports;

use App\Enum\TipoMovimientoEnum;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ConsultaRemateExport implements FromQuery, WithHeadings, ShouldAutoSize
{

    use Exportable;

    public function __construct($dto)
    {
        $this->dto = $dto;
    }

   public function query()
   {
       $query = DB::table('actuaciones')
           ->join('ciudades', 'ciudades.id', '=', 'actuaciones.ciudad_id')
           ->join('juzgados', 'juzgados.id', '=', 'actuaciones.juzgado_id')
           ->join('despachos', 'despachos.id', '=', 'actuaciones.despacho_id')
           ->where('actuaciones.tipo_movimiento', '=',TipoMovimientoEnum::REMATE)
           ->select(
               'actuaciones.fecha_actuacion',
               'actuaciones.anotacion',
               'actuaciones.nombres_demandantes',
               'actuaciones.nombres_demandados',
               'ciudades.nombre AS ciudad_nombre',
               'actuaciones.numero_proceso',
               'juzgados.nombre AS juzgado_nombre',
               'despachos.despacho AS despacho_numero'
           );

       if (isset($this->dto['fecha_inicial'])){
           $query->where('actuaciones.fecha_actuacion', '>=', $this->dto['fecha_inicial']);
       }
       if (isset($this->dto['fecha_final'])){
           $query->where('actuaciones.fecha_actuacion', '<=', $this->dto['fecha_final']);
       }
       if (isset($this->dto['ciudad'])){
           $query->where('actuaciones.ciudad_id', '=', $this->dto['ciudad']);
       }
       if (isset($this->dto['juzgado'])){
           $query->where('actuaciones.juzgado_id', '=', $this->dto['juzgado']);
       }
       if (isset($this->dto['despacho'])){
           $query->where('actuaciones.despacho_id', '=', $this->dto['despacho']);
       }

       $query->orderBy("actuaciones.id", "desc")
           ->orderBy("actuaciones.numero_proceso", "asc");

       return $query;
   }
    public function headings(): array
    {
        return [
            "Fecha actuación",
            "Detalle",
            "Demandante",
            "Demandado",
            "Ciudad",
            "Número proceso",
            "Juzgado",
            "Despacho",
        ];
    }
}
