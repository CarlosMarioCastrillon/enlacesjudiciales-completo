<?php

namespace App\Exports;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class AudienciaCumplidaExport implements FromQuery, WithHeadings, ShouldAutoSize
{

    use Exportable;

    public function __construct($dto)
    {
        $this->dto = $dto;
    }

   public function query()
   {
       $user = Auth::user();
       $empresa = $user->empresa();
       $dto['empresa_id'] = $empresa->id;
       $dto['fecha_actual'] = Carbon::today();
       $query = DB::table('actuaciones')
           ->where('actuaciones.empresa_id', '=', $dto['empresa_id'])
           ->where('actuaciones.ind_actuacion_audiencia', '=', 'U')
           // ->whereRaw('fecha_vencimiento_termino < curdate()')
           ->where('actuaciones.fecha_vencimiento_termino', '<', $dto['fecha_actual'])
           ->select(
               'actuaciones.numero_proceso',
               'actuaciones.nombres_demandantes',
               'actuaciones.nombres_demandados',
               'actuaciones.observaciones',
               'actuaciones.fecha_vencimiento_termino',
               'actuaciones.updated_at AS fecha_modificacion',
               'actuaciones.usuario_modificacion_nombre',
               DB::raw("DATEDIFF(fecha_vencimiento_termino, curdate()) as falta")

           );

       if (isset($this->dto['proceso'])){
           $query->where('actuaciones.numero_proceso', 'like', '%' . $this->dto['proceso'] . '%');
       }
       if (isset($this->dto['actuacion'])){
           $query->where('actuaciones.observaciones', 'like', '%' . $this->dto['actuacion'] . '%');
       }
       if (isset($this->dto['demandante'])){
           $query->where('actuaciones.nombres_demandantes', 'like', '%' . $this->dto['demandante'] . '%');
       }
       if (isset($this->dto['demandado'])){
           $query->where('actuaciones.nombres_demandados', 'like', '%' . $this->dto['demandado'] . '%');
       }

       $query->orderBy('actuaciones.fecha_vencimiento_termino', 'desc');

       return $query;
   }

    public function headings(): array
    {
        return [
            "Proceso",
            "Demandante",
            "Demandado",
            "Actuación",
            "Fecha vencimiento",
            "Última actualización",
            "Actualizado por",
            "Falta",
        ];
    }
}
