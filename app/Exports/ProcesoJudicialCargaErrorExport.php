<?php

namespace App\Exports;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ProcesoJudicialCargaErrorExport implements FromQuery, WithHeadings, ShouldAutoSize
{

    use Exportable;

   public function query()
   {
       $user = Auth::user();
       $empresa = $user->empresa();
       $dto['empresa_id'] = $empresa->id;
       $query = DB::table('proceso_judicial_carga_errores')
           ->select(
               'numero_proceso',
               'nombres_demandantes',
               'nombres_demandados',
               'ciudad_nombre',
               'juzgado_nombre',
               'juzgado_numero',
               'clase_proceso_nombre',
               'responsable_documento',
               'area_nombre',
               'observacion'
           )->where('empresa_id', '=', $dto['empresa_id'])
            ->orderBy('id', 'asc');

       return $query;
   }
    public function headings(): array
    {
        return [
            "Proceso actual",
            "Demandante",
            "Demandado",
            "Ciudad actual",
            "Juzgado actual",
            "Número Juzgado",
            "Clase proceso",
            "Responsable",
            "Area",
            "Observaciones"
        ];
    }
}
