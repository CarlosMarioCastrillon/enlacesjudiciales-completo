<?php

namespace App\Exports;

use App\Enum\TipoNovedadEnum;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class AuditoriaProcesoExport implements FromQuery, WithHeadings, WithColumnFormatting, ShouldAutoSize
{

    use Exportable;

    protected $programacionId;

    public function __construct($programacionId)
    {
        $this->programacionId = $programacionId;
    }

    public function query()
    {
        $novedad = ' CASE ';
        $novedad .= ' 	WHEN tipo_novedad = ? THEN ? ';
        $novedad .= ' 	WHEN tipo_novedad = ? THEN ? ';
        $novedad .= ' 	WHEN tipo_novedad = ? THEN ? ';
        $novedad .= ' 	WHEN tipo_novedad = ? THEN ? ';
        $novedad .= ' END ';

        $query = DB::table('auditoria_procesos')
            ->select(
               'programacion_auditoria_proceso_id',
               'fecha_ejecucion',
               'fecha_proceso',
               'numero_proceso',
               DB::raw($novedad)
            )
            ->addBinding(TipoNovedadEnum::CON_ACTUACION, 'select')
            ->addBinding(TipoNovedadEnum::obtenerDescripcion(TipoNovedadEnum::CON_ACTUACION), 'select')
            ->addBinding(TipoNovedadEnum::SIN_ACTUACION, 'select')
            ->addBinding(TipoNovedadEnum::obtenerDescripcion(TipoNovedadEnum::SIN_ACTUACION), 'select')
            ->addBinding(TipoNovedadEnum::ES_PRIVADO, 'select')
            ->addBinding(TipoNovedadEnum::obtenerDescripcion(TipoNovedadEnum::ES_PRIVADO), 'select')
            ->addBinding(TipoNovedadEnum::SIN_CONEXION, 'select')
            ->addBinding(TipoNovedadEnum::obtenerDescripcion(TipoNovedadEnum::SIN_CONEXION), 'select')
            ->where('programacion_auditoria_proceso_id', $this->programacionId)
            ->orderBy('id', 'asc');

        return $query;
    }

    public function headings(): array
    {
        return [
            "Programación #",
            "Fecha ejecución",
            "Fecha proceso",
            "Número proceso",
            "Novedad"
        ];
    }

    public function columnFormats(): array
    {
        return [
            'B' => NumberFormat::FORMAT_DATE_YYYYMMDD,
            'C' => NumberFormat::FORMAT_DATE_YYYYMMDD
        ];
    }
}
