<?php

namespace App\Exports;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class AuxiliarJusticiaCargaErrorExport implements FromQuery, WithHeadings, ShouldAutoSize
{

    use Exportable;

   public function query()
   {
       $user = Auth::user();
       $empresa = $user->empresa();
       $dto['empresa_id'] = $empresa->id;
       $query = DB::table('auxiliar_justicia_carga_errores')
           ->select(
               'auxiliar_justicia_carga_errores.numero_documento',
               'auxiliar_justicia_carga_errores.nombres_auxiliar',
               'auxiliar_justicia_carga_errores.apellidos_auxiliar',
               'auxiliar_justicia_carga_errores.nombre_ciudad',
               'auxiliar_justicia_carga_errores.direccion_oficina',
               'auxiliar_justicia_carga_errores.telefono_oficina',
               'auxiliar_justicia_carga_errores.cargo',
               'auxiliar_justicia_carga_errores.telefono_celular',
               'auxiliar_justicia_carga_errores.direccion_residencia',
               'auxiliar_justicia_carga_errores.telefono_residencia',
               'auxiliar_justicia_carga_errores.correo_electronico',
               'auxiliar_justicia_carga_errores.observacion',
           )->where('auxiliar_justicia_carga_errores.empresa_id', '=', $dto['empresa_id']);

       $query->orderBy('auxiliar_justicia_carga_errores.id', 'desc');

       return $query;
   }
    public function headings(): array
    {
        return [
            "Número documento",
            "Nombres Auxiliar",
            "Apellidos Auxiliar",
            "Ciudad",
            "Dirección oficina",
            "Teléfono oficina",
            "Cargo",
            "Teléfono celular",
            "Dirección residencia",
            "Teléfono residencia",
            "Correo electrónico",
            "Observaciones",
        ];
    }
}
