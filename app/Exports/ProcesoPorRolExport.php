<?php

namespace App\Exports;

use App\Model\Administracion\ParametroConstante;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ProcesoPorRolExport implements FromQuery, WithHeadings, ShouldAutoSize
{

    use Exportable;

    public function __construct($dto)
    {
        $this->dto = $dto;
    }

   public function query()
   {
       // Constantes
       $parametros = ParametroConstante::cargarParametros();

       $user = Auth::user();
       $rol = $user->rol();
       $empresa = $user->empresa();
       $dto['zona_id'] = $empresa->zona_id;
       $query = DB::table('procesos_judiciales')
           ->join('ciudades', 'ciudades.id', '=', 'procesos_judiciales.ciudad_id')
           ->join('juzgados', 'juzgados.id', '=', 'procesos_judiciales.juzgado_id')
           ->join('despachos', 'despachos.id', '=', 'procesos_judiciales.despacho_id')
           ->join('clases_de_procesos', 'clases_de_procesos.id', '=', 'procesos_judiciales.ultima_clase_proceso_id')
           ->join('empresas', 'empresas.id', '=', 'procesos_judiciales.empresa_id')

           ->select(
               'empresas.nombre AS empresa_nombre',
               'ciudades.nombre AS ciudad_nombre',
               'procesos_judiciales.numero_proceso',
               'juzgados.nombre AS juzgado_nombre',
               'procesos_judiciales.juzgado_actual',
               'despachos.nombre AS despacho_nombre',
               'procesos_judiciales.nombres_demandantes',
               'procesos_judiciales.nombres_demandados',
               'procesos_judiciales.created_at AS fecha_creacion',
               'procesos_judiciales.usuario_creacion_nombre',
           );
       if($rol->id != ($parametros['SUPERUSUARIO_ROL_ID'] ?? null)) {
           if ($rol->id == ($parametros['ADMINISTRADOR_ZONA_ROL_ID'] ?? null)){
               $query->where('empresas.zona_id', '=', $dto['zona_id']);
           }
       }
       if (isset($this->dto['empresa'])){
           $query->where('procesos_judiciales.empresa_id', '=', $this->dto['empresa']);
       }
       if (isset($this->dto['numero_proceso'])){
           $query->where('procesos_judiciales.numero_proceso', 'like','%'. $this->dto['numero_proceso'] .'%');
       }
       if (isset($this->dto['demandante'])){
           $query->where('procesos_judiciales.nombres_demandantes', 'like','%'. $this->dto['demandante'] .'%');
       }
       if (isset($this->dto['demandado'])){
           $query->where('procesos_judiciales.nombres_demandados', 'like','%'. $this->dto['demandado'] .'%');
       }

       $query->orderBy('procesos_judiciales.id', 'desc');

       return $query;
   }
    public function headings(): array
    {
        return [
            "Empresa",
            "Ciudad",
            "Número proceso",
            "Nombre juzgado",
            "Juzgado Actual",
            "Número despacho",
            "Demandante",
            "Demandado",
            "Fecha creación",
            "Usuario creación",
        ];
    }
}
