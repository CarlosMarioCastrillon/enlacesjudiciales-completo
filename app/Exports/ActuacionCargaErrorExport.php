<?php

namespace App\Exports;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ActuacionCargaErrorExport implements FromQuery, WithHeadings, ShouldAutoSize
{

    use Exportable;

    public function query()
    {
       $query = DB::table('actuacion_carga_errores')
           ->select(
               'ciudad_nombre',
               'juzgado_nombre',
               'despacho_nombre',
               'numero_proceso',
               'nombres_demandantes',
               'nombres_demandados',
               'descripcion_actuacion',
               'descripcion_anotacion',
               'fecha_registro',
               'fecha_actuacion',
               'fecha_inicio_termino',
               'fecha_vencimiento_termino',
               'descripcion_clase_proceso',
               'observacion'
           )->orderBy('id', 'asc');

       return $query;
    }

    public function headings(): array
    {
        return [
            "Ciudad",
            "Juzgado",
            "Despacho",
            "Número proceso",
            "Demandantes",
            "Demandados",
            "Actuación",
            "Anotación",
            "Fecha registro",
            "Fecha actuación",
            "Fecha inicio termino",
            "Fecha vencimiento termino",
            "Tipo/Clase de proceso",
            "Observaciones"
        ];
    }
}
