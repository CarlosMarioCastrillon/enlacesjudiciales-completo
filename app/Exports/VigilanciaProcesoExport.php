<?php

namespace App\Exports;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class VigilanciaProcesoExport implements FromQuery, WithHeadings, ShouldAutoSize
{

    use Exportable;

    public function __construct($dto)
    {
        $this->dto = $dto;
    }

   public function query()
   {
       $user = Auth::user();
       $empresa = $user->empresa();
       $dto['empresa_id'] = $empresa->id;
       $query = DB::table('procesos_judiciales')
           ->join('ciudades', 'ciudades.id', '=', 'procesos_judiciales.ciudad_id')
           ->join('juzgados', 'juzgados.id', '=', 'procesos_judiciales.juzgado_id')
           ->join('despachos', 'despachos.id', '=', 'procesos_judiciales.despacho_id')
           ->select(
               'procesos_judiciales.numero_proceso',
               'procesos_judiciales.numero_consecutivo',
               'ciudades.nombre AS ciudad_nombre',
               'juzgados.nombre AS juzgado_nombre',
               'despachos.despacho AS despacho_numero',
               'procesos_judiciales.nombres_demandantes',
               'procesos_judiciales.nombres_demandados',
               'procesos_judiciales.observaciones',
               'procesos_judiciales.created_at AS fecha_creacion',
               'procesos_judiciales.updated_at AS fecha_modificacion'
           )->where('procesos_judiciales.empresa_id', '=', $dto['empresa_id'])
           ->where('procesos_judiciales.indicativo_tiene_vigilancia', '=', 'V');

       if (isset($this->dto['estado'])){
           $query->where('procesos_judiciales.indicativo_estado', '=', $this->dto['estado']);
       }
       if (isset($this->dto['numero_proceso'])){
           $query->where('procesos_judiciales.numero_proceso', 'like','%'. $this->dto['numero_proceso'] .'%');
       }
       if (isset($this->dto['demandante'])){
           $query->where('procesos_judiciales.nombres_demandantes', 'like','%'. $this->dto['demandante'] .'%');
       }
       if (isset($this->dto['demandado'])){
           $query->where('procesos_judiciales.nombres_demandados', 'like','%'. $this->dto['demandado'] .'%');
       }
       $query->orderBy('procesos_judiciales.id', 'desc');

       return $query;
   }
    public function headings(): array
    {
        return [
            "Número proceso",
            "Consecutivo",
            "Ciudad",
            "Juzgado",
            "Número despacho",
            "Demandante",
            "Demandado",
            "Observaciones",
            "Fecha creación",
            "Fecha última actualización",
        ];
    }
}
