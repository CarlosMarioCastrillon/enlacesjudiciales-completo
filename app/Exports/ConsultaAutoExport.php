<?php

namespace App\Exports;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ConsultaAutoExport implements FromQuery, WithHeadings, ShouldAutoSize
{

    use Exportable;

    public function __construct($dto)
    {
        $this->dto = $dto;
    }

   public function query()
   {
       $user = Auth::user();
       $empresa = $user->empresa();
       $dto['empresa_id'] = $empresa->id;
       $query1 = DB::table('actuaciones')
           ->join('ciudades', 'ciudades.id', '=', 'actuaciones.ciudad_id')
           ->join('juzgados', 'juzgados.id', '=', 'actuaciones.juzgado_id')
           ->join('despachos', 'despachos.id', '=', 'actuaciones.despacho_id')
           ->join('procesos_judiciales', 'procesos_judiciales.id', '=', 'actuaciones.proceso_id')
           ->join('empresas', 'empresas.id', '=', 'actuaciones.empresa_id')

           ->select(
               'actuaciones.fecha_actuacion',
               'ciudades.nombre AS ciudad_nombre',
               'actuaciones.numero_proceso',
               DB::raw('(CASE 
               WHEN actuaciones.tipo_movimiento = "EX" THEN "Expediente"
               WHEN actuaciones.tipo_movimiento = "AC" THEN "Actuación"
               WHEN actuaciones.tipo_movimiento = "VG" THEN "Vigilancia")'),
               'juzgados.nombre AS juzgado_nombre',
               'despachos.despacho AS despacho',
               'actuaciones.nombres_demandantes',
               'actuaciones.nombres_demandados',
               'actuaciones.actuacion',
               'actuaciones.observaciones',
           )->where('actuaciones.empresa_id', '=', $dto['empresa_id']);

       if (isset($this->dto['fecha_inicial'])){
           $query1->where('actuaciones.fecha_actuacion', '>=', $this->dto['fecha_inicial']);
       }
       if (isset($this->dto['fecha_final'])){
           $query1->where('actuaciones.fecha_actuacion', '<=', $this->dto['fecha_final']);
       }
       if (isset($this->dto['fecha'])){
           $query1->where('actuaciones.fecha_actuacion', '>=', $this->dto['fecha']);
       }
       if (isset($this->dto['ciudad'])){
           $query1->where('actuaciones.ciudad_id', '=', $this->dto['ciudad']);
       }
       if (isset($this->dto['juzgado'])){
           $query1->where('actuaciones.juzgado_id', '=', $this->dto['juzgado']);
       }
       if (isset($this->dto['despacho'])){
           $query1->where('actuaciones.despacho_id', '=', $this->dto['despacho']);
       }
       if (isset($this->dto['proceso'])){
           $query1->where('actuaciones.numero_proceso', 'like', '%' . $this->dto['proceso'] . '%');
       }
       if (isset($his->dto['url'])){
           $query1->where('actuaciones.url_archivo_anexo', '<>', '');
       }
       if (isset($this->dto['tipo_movimiento'])){
           $query1->whereIn('actuaciones.tipo_movimiento', ['AC', 'EX', 'VG']);
       }
       $query1->orderBy('actuaciones.fecha_actuacion', 'desc')
           ->orderBy("actuaciones.numero_proceso", "asc");

       $query2 = DB::table('expedientes')
           ->join('ciudades', 'ciudades.id', '=', 'expedientes.ciudad_id')
           ->join('juzgados', 'juzgados.id', '=', 'expedientes.juzgado_id')
           ->join('despachos', 'despachos.id', '=', 'expedientes.despacho_id')
           ->join('procesos_judiciales', 'procesos_judiciales.id', '=', 'expedientes.proceso_id')
           ->join('empresas', 'empresas.id', '=', 'expedientes.empresa_id')

           ->select(
               'expedientes.fecha_actuacion',
               'ciudades.nombre AS ciudad_nombre',
               'expedientes.numero_proceso',
               DB::raw('(CASE 
               WHEN expedientes.tipo_movimiento = "EX" THEN "Expediente"
               WHEN expedientes.tipo_movimiento = "AC" THEN "Actuación"
               WHEN expedientes.tipo_movimiento = "VG" THEN "Vigilancia")'),
               'juzgados.nombre AS juzgado_nombre',
               'despachos.despacho AS despacho_numero',
               'expedientes.nombres_demandantes',
               'expedientes.nombres_demandados',
               'expedientes.actuacion',
               'expedientes.observaciones',
           )->where('expedientes.empresa_id', '=', $dto['empresa_id']);

       if (isset($this->dto['fecha_inicial'])){
           $query2->where('expedientes.fecha_actuacion', '>=', $this->dto['fecha_inicial']);
       }
       if (isset($this->dto['fecha_final'])){
           $query2->where('expedientes.fecha_actuacion', '<=', $this->dto['fecha_final']);
       }
       if (isset($this->dto['fecha'])){
           $query2->where('expedientes.fecha_actuacion', '>=', $this->dto['fecha']);
       }
       if (isset($this->dto['ciudad'])){
           $query2->where('expedientes.ciudad_id', '=', $this->dto['ciudad']);
       }
       if (isset($this->dto['juzgado'])){
           $query2->where('expedientes.juzgado_id', '=', $this->dto['juzgado']);
       }
       if (isset($this->dto['despacho'])){
           $query2->where('expedientes.despacho_id', '=', $this->dto['despacho']);
       }
       if (isset($this->dto['proceso'])){
           $query2->where('expedientes.numero_proceso', 'like', '%'. $this->dto['proceso'] . '%');
       }
       if (isset($this->dto['url'])){
           $query2->where('expedientes.url_archivo_anexo', '<>', '');
       }
       if (isset($this->dto['tipo_movimiento'])){
           $query2->whereIn('expedientes.tipo_movimiento', ['AC', 'EX', 'VG']);
       }

       $query2->orderBy("expedientes.fecha_actuacion", "desc")
           ->orderBy("expedientes.numero_proceso", "asc");

       return $query1->union($query2);
   }
    public function headings(): array
    {
        return [
            "Fecha actuación",
            "Ciudad",
            "Número proceso",
            "Tipo de movimiento",
            "Juzgado",
            "Despacho",
            "Demandante",
            "Demandado",
            "Actuación",
            "Observaciones",
        ];
    }
}
