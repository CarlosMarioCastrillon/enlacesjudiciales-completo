<?php

namespace App\Exports;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ConsultaPersonalizadoDigitalExport implements FromQuery, WithHeadings, ShouldAutoSize
{

    use Exportable;

    public function __construct($dto)
    {
        $this->dto = $dto;
    }

   public function query()
   {
       $user = Auth::user();
       $empresa = $user->empresa();
       $dto['empresa_id'] = $empresa->id;
       $query = DB::table('actuaciones')
           ->join('ciudades', 'ciudades.id', '=', 'actuaciones.ciudad_id')
           ->join('juzgados', 'juzgados.id', '=', 'actuaciones.juzgado_id')
           ->join('despachos', 'despachos.id', '=', 'actuaciones.despacho_id')
           ->join('procesos_judiciales', 'procesos_judiciales.id', '=', 'actuaciones.proceso_id')
           ->leftJoin('tipos_notificaciones', 'tipos_notificaciones.id', '=', 'actuaciones.tipo_notificacion_id')
           ->where('actuaciones.empresa_id', '=', $dto['empresa_id'])
           ->select(
               'actuaciones.numero_proceso',
               'procesos_judiciales.descripcion_clase_proceso AS descripcion_clase_proceso',
               'tipos_notificaciones.nombre AS tipo_notificacion',
               'actuaciones.nombres_demandantes',
               'actuaciones.nombres_demandados',
               'actuaciones.anotacion',
               'juzgados.nombre AS juzgado_nombre',
               'despachos.despacho AS despacho_numero',
               'ciudades.nombre AS ciudad_nombre',
               'actuaciones.fecha_actuacion'
           );

       if (isset($this->dto['ciudad'])){
           $query->where('actuaciones.ciudad_id', '=', $this->dto['ciudad']);
       }
       if (isset($this->dto['juzgado'])){
           $query->where('actuaciones.juzgado_id', '=', $this->dto['juzgado']);
       }
       if (isset($this->dto['despacho'])){
           $query->where('actuaciones.despacho_id', '=', $this->dto['despacho']);
       }
       if (isset($this->dto['tipo_notificacion'])){
           $query->where('actuaciones.tipo_notificacion_id', '=', $this->dto['tipo_notificacion']);
       }
       if (isset($this->dto['radicado'])){
           $query->where('actuaciones.numero_proceso', 'like', '%'. $this->dto['radicado'] .'%');
       }
       if (isset($this->dto['descripcion_clase_proceso'])){
           $query->where('procesos_judiciales.descripcion_clase_proceso', 'like', '%'. $this->dto['descripcion_clase_proceso'] . '%');
       }
       if (isset($this->dto['detalle'])){
           $query->where('actuaciones.anotacion', 'like', '%'. $this->dto['detalle'] . '%');
       }
       if (isset($this->dto['fecha_inicial'])){
           $query->where('actuaciones.fecha_actuacion', '>=', $this->dto['fecha_inicial']);
       }
       if (isset($this->dto['fecha_final'])){
           $query->where('actuaciones.fecha_actuacion', '<=', $this->dto['fecha_final']);
       }

       $query->orderBy("actuaciones.fecha_actuacion", "desc");

       return $query;
   }
    public function headings(): array
    {
        return [
            "Radicado",
            "Tipo proceso",
            "Tipo notificación",
            "Demandante",
            "Demandado",
            "Detalle actuación",
            "Juzgado",
            "Despacho",
            "Ciudad",
            "Fecha",
        ];
    }
}
