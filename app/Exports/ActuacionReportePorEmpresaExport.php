<?php

namespace App\Exports;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ActuacionReportePorEmpresaExport implements FromQuery, WithHeadings, ShouldAutoSize
{

    use Exportable;

    public function __construct($dto)
    {
        $this->dto = $dto;
    }

    public function query()
    {
       // Datos de sesión
       $user = Auth::user();
       $empresa = $user->empresa();

       $query = DB::table('actuaciones')
           ->join('ciudades', 'ciudades.id', '=', 'actuaciones.ciudad_id')
           ->join('juzgados', 'juzgados.id', '=', 'actuaciones.juzgado_id')
           ->join('despachos', 'despachos.id', '=', 'actuaciones.despacho_id')
           ->join('procesos_judiciales', 'procesos_judiciales.id', '=', 'actuaciones.proceso_id')
           ->join('empresas', 'empresas.id', '=', 'actuaciones.empresa_id')
           ->leftJoin('etapas_de_procesos', 'etapas_de_procesos.id', '=', 'actuaciones.etapa_proceso_id')
           ->leftJoin('tipos_de_actuaciones', 'tipos_de_actuaciones.id', '=', 'actuaciones.tipo_actuacion_id')
           ->leftJoin('tipos_notificaciones', 'tipos_notificaciones.id', '=', 'actuaciones.tipo_notificacion_id')
           ->where('actuaciones.empresa_id', '=', $empresa->id)
           ->select(
               'actuaciones.numero_proceso',
               'ciudades.nombre AS ciudad_nombre',
               'juzgados.nombre AS juzgado_nombre',
               'despachos.despacho AS despacho',
               'actuaciones.anotacion',
               'actuaciones.actuacion',
               'tipos_de_actuaciones.nombre AS tipo_actuacion_nombre',
               'etapas_de_procesos.nombre AS etapa_proceso_nombre',
               'actuaciones.fecha_registro',
               'actuaciones.fecha_actuacion',
               'actuaciones.fecha_inicio_termino',
               'actuaciones.fecha_vencimiento_termino',
               'actuaciones.nombres_demandantes',
               'actuaciones.nombres_demandados',
               'actuaciones.ruta_archivo_anexo',
               'actuaciones.observaciones',
               DB::raw('null AS consulta_rama'),
               'actuaciones.created_at AS fecha_creacion',
               'actuaciones.updated_at AS fecha_modificacion'
           );

       if (isset($this->dto['fecha_inicial'])){
           $query->where('actuaciones.fecha_actuacion', '>=', $this->dto['fecha_inicial']);
       }
       if (isset($this->dto['fecha_final'])){
           $query->where('actuaciones.fecha_actuacion', '<=', $this->dto['fecha_final']);
       }
       if (isset($this->dto['fecha'])){
           $query->where('actuaciones.fecha_actuacion', '>=', $this->dto['fecha']);
       }
       if (isset($this->dto['ciudad'])){
           $query->where('actuaciones.ciudad_id', '=', $this->dto['ciudad']);
       }
       if (isset($this->dto['juzgado'])){
           $query->where('actuaciones.juzgado_id', '=', $this->dto['juzgado']);
       }
       if (isset($this->dto['despacho'])){
           $query->where('actuaciones.despacho_id', '=', $this->dto['despacho']);
       }
       if (isset($this->dto['proceso'])){
           $query->where('actuaciones.numero_proceso', 'like', '%' . $this->dto['proceso'] . '%');
       }
       if (isset($this->dto['con_archivo_anexo']) && $this->dto['con_archivo_anexo']){
           $query->whereNotNull('actuaciones.ruta_archivo_anexo');
       }
       if (isset($this->dto['tipo_movimiento'])){
           $query->whereIn('actuaciones.tipo_movimiento', ['AC', 'EX', 'VG']);
       }

       $query->orderBy('actuaciones.fecha_actuacion', 'desc')
           ->orderBy("actuaciones.numero_proceso", "asc");

       return $query;
    }

    public function headings(): array
    {
        return [
            "Número proceso",
            "Ciudad",
            "Juzgado",
            "Despacho",
            "Anotación",
            "Actuación",
            "Tipo de actuación",
            "Etapa del proceso",
            "Fecha registro",
            "Fecha actuación",
            "Fecha inicio término",
            "Fecha vencimiento término",
            "Tipo de movimiento",
            "Demandante",
            "Demandado",
            "Observaciones",
            "Fecha creación",
            "Fecha última actualización",
        ];
    }
}
