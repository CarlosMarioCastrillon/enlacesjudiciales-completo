<?php

namespace App\Exports;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ProcesoAreaExport implements FromQuery, WithHeadings, ShouldAutoSize
{

    use Exportable;

    public function __construct($dto)
    {
        $this->dto = $dto;
    }

   public function query()
   {
       $user = Auth::user();
       $empresa = $user->empresa();
       $dto['empresa_id'] = $empresa->id;
       $query = DB::table('procesos_judiciales')
           ->join('ciudades', 'ciudades.id', '=', 'procesos_judiciales.ciudad_id')
           ->join('juzgados', 'juzgados.id', '=', 'procesos_judiciales.juzgado_id')
           ->join('despachos', 'despachos.id', '=', 'procesos_judiciales.despacho_id')
           ->join('areas', 'areas.id', '=', 'procesos_judiciales.area_id')
           ->select(
               'ciudades.nombre AS ciudad_nombre',
               'procesos_judiciales.numero_proceso',
               'juzgados.nombre AS juzgado_nombre',
               'despachos.despacho AS despacho_numero',
               'procesos_judiciales.nombres_demandantes',
               'procesos_judiciales.nombres_demandados',
               'areas.nombre'
           )->where('procesos_judiciales.empresa_id', '=', $dto['empresa_id']);


           $query->where('procesos_judiciales.area_id', '=', $this->dto['area']);
           $query->orderBy('procesos_judiciales.id', 'desc');

           return $query;
   }
    public function headings(): array
    {
        return [
            "Ciudad",
            "Número proceso",
            "Juzgado",
            "Número despacho",
            "Demandante",
            "Demandado",
            "Area",
        ];
    }
}
