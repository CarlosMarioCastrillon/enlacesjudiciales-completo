<?php

namespace App\Exports;

use App\Model\Administracion\ParametroConstante;
use App\Model\Configuracion\Empresa;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class EmpresaExport implements FromQuery, WithHeadings, ShouldAutoSize
{

    use Exportable;

    public function __construct($dto)
    {
        $this->dto = $dto;
    }

  public function query()
    {
        // Constantes
        $parametros = ParametroConstante::cargarParametros();

        $user = Auth::user();
        $rol = $user->rol();
        $empresa = $user->empresa();
        $dto['empresa_id'] = $empresa->id;
        $dto['zona_id'] = $empresa->zona_id;
        $query = DB::table('empresas')
            ->join('tipos_de_documentos','tipos_de_documentos.id','=','empresas.tipo_documento_id')
            ->join('tipos_de_empresa','tipos_de_empresa.id','=','empresas.tipo_empresa_id')
            ->join('departamentos','departamentos.id','=','empresas.departamento_id')
            ->join('ciudades','ciudades.id','=','empresas.ciudad_id')
            ->join('zonas','zonas.id','=','empresas.zona_id')
            ->select(
                'empresas.nombre',
                'tipos_de_documentos.codigo AS tipo_documento_codigo',
                'empresas.numero_documento',
                'tipos_de_empresa.nombre AS tipo_empresa_nombre',
                'ciudades.nombre AS ciudad_nombre',
                'departamentos.nombre AS departamento_nombre',
                'zonas.nombre AS zona_nombre',
                'empresas.direccion',
                'empresas.email',
                'empresas.telefono_uno',
                'empresas.telefono_dos',
                'empresas.telefono_tres',
                'empresas.fecha_vencimiento',
                DB::raw('(CASE WHEN empresas.con_actuaciones_digitales = 1 THEN "SI" ELSE "NO" END) AS actuaciones_digitales'),
                DB::raw('(CASE WHEN empresas.con_vigilancia = 1 THEN "SI" ELSE "NO" END) AS vigilancia'),
                DB::raw('(CASE WHEN empresas.con_data_procesos = 1 THEN "SI" ELSE "NO" END) AS data_procesos'),
                'empresas.observacion',
                DB::raw('(CASE WHEN empresas.estado = 1 THEN "ACTIVO" ELSE "INACTIVO" END) AS estado'),
                'empresas.usuario_creacion_nombre',
                'empresas.usuario_modificacion_nombre',
                'empresas.created_at AS fecha_creacion',
                'empresas.updated_at AS fecha_modificacion'
            );

        if($rol->id != ($parametros['SUPERUSUARIO_ROL_ID'] ?? null)) {
            if ($rol->id == ($parametros['ADMINISTRADOR_ZONA_ROL_ID'] ?? null)){
                $query->where('empresas.zona_id', '=', $dto['zona_id']);
            }else {
                $query->where('empresas.id', '=', $dto['empresa_id']);
            }
        }
        if(isset($this->dto['nombre'])){
            $query->where('empresas.nombre', 'like', '%' . $this->dto['nombre'] . '%');
        }
        if(isset($this->dto['numero_documento'])){
            $query->where('empresas.numero_documento', 'like', '%' . $this->dto['numero_documento'] . '%');
        }
        if(isset($this->dto['ciudad_nombre'])){
            $query->where('ciudades.nombre', 'like', '%' . $this->dto['ciudad_nombre'] . '%');
        }
        if(isset($this->dto['zona_nombre'])){
            $query->where('zonas.nombre', 'like', '%' . $this->dto['zona_nombre'] . '%');
        }
        if(isset($this->dto['estado'])) {
            $query->where('empresas.estado', '=', $this->dto['estado']);
        }
        $query->orderBy('empresas.id', 'desc');

        return $query;
    }
    public function headings(): array
    {
        return [
            "Empresas",
            "Tipo de documento",
            "Número de documento",
            "Tipo de empresa",
            "Ciudad",
            "Departamento",
            "Zona",
            "Dirección",
            "Email",
            "Teléfono uno",
            "Teléfono dos",
            "Teléfono tres",
            "Fecha vencimiento",
            "Act. Dig.",
            "Vigilancia",
            "Dataprocesos",
            "Observación",
            "Estado",
            "Usuario creación",
            "Usuario modificación",
            "Fecha creación",
            "Fecha modificación",
            ];
    }
}
