<?php

namespace App\Exports;

use App\Model\Administracion\Cliente;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ClienteExport implements FromQuery, WithHeadings, ShouldAutoSize
{

    use Exportable;

    public function __construct($dto)
    {
        $this->dto = $dto;
    }

   public function query()
    {
        $user = Auth::user();
        $empresa = $user->empresa();
        $dto['empresa_id'] = $empresa->id;
        $query = DB::table('clientes')
            ->join('tipos_de_documentos','tipos_de_documentos.id','=','clientes.tipo_documento_id')
            ->leftJoin('ciudades','ciudades.id','=','clientes.ciudad_id')
            ->leftJoin('departamentos','departamentos.id','=','ciudades.departamento_id')
            ->select(
                'clientes.nombre',
                'tipos_de_documentos.codigo AS tipo_documento_codigo',
                'clientes.numero_documento',
                'ciudades.nombre',
                'departamentos.nombre',
                'clientes.telefono_uno',
                'clientes.direccion',
                'clientes.telefono_dos',
                'clientes.email',
                DB::raw('(CASE WHEN clientes.estado = 1 THEN "ACTIVO" ELSE "INACTIVO" END) AS estado'),
                'clientes.usuario_creacion_nombre',
                'clientes.usuario_modificacion_nombre',
                'clientes.created_at AS fecha_creacion',
                'clientes.updated_at AS fecha_modificacion'
            )
            ->where('clientes.empresa_id', '=', $dto['empresa_id']);

        if(isset($this->dto['nombre'])){
            $query->where('clientes.nombre', 'like', '%' . $this->dto['nombre'] . '%');
        }
        if(isset($this->dto['numero_documento'])){
            $query->where('clientes.numero_documento', 'like', '%' . $this->dto['numero_documento'] . '%');
        }
        if(isset($this->dto['ciudad_nombre'])){
            $query->where('ciudades.nombre', 'like', '%' . $this->dto['ciudad_nombre'] . '%');
        }
        $query->orderBy('clientes.id', 'desc');
        return $query;
    }
    public function headings(): array
    {
        return [
            "Clientes",
            "Tipo de documento",
            "Número de documento",
            "Dirección",
            "Teléfono uno",
            "Teléfono dos",
            "E-mail",
            "Estado",
            "Usuario creación",
            "Usuario modificación",
            "Fecha creación",
            "Fecha modificación",
        ];
    }
}
