<?php

namespace App\Jobs;

use App\Contracts\Administracion\ActuacionService;
use App\Contracts\Administracion\AuditoriaProcesoService;
use Exception;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class LeerActuacionProcesoBroker extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $data;
    protected $actuacionService;

    /**
     * Create a new job instance.
     *
     * @param $data
     * @internal param Bunch $bunch
     */
    public function __construct($data)
    {
        $this->data = $data;
        $this->actuacionService = App::make(ActuacionService::class);
    }

    /**
     * Execute the job.
     *
     * @throws Exception
     * @internal param BunchService $bunchService
     */
    public function handle()
    {
        $this->actuacionService->ejecutarLecturaDeActuaciones($this->data);
    }
}
