<?php

namespace App\Jobs;

use App\Contracts\Administracion\AuditoriaProcesoService;
use Exception;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class AuditoriaProcesoBroker extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $data;
    protected $auditoriaProcesoService;

    /**
     * Create a new job instance.
     *
     * @param $data
     * @internal param Bunch $bunch
     */
    public function __construct($data)
    {
        $this->data = $data;
        $this->auditoriaProcesoService = App::make(AuditoriaProcesoService::class);
    }

    /**
     * Execute the job.
     *
     * @throws Exception
     * @internal param BunchService $bunchService
     */
    public function handle()
    {
        $this->auditoriaProcesoService->ejecutarAuditoria($this->data);
    }
}
