<?php


namespace App\Repositories\Importacion;

use App\Contracts\Importacion\AuxiliarJusticiaCargaErrorService;
use App\Contracts\Seguridad\AuditoriaMaestroService;
use App\Enum\AccionAuditoriaEnum;
use App\Exceptions\ModelException;
use App\Imports\AuxiliarJusticiaImport;
use App\Model\Importacion\AuxiliarJusticiaCargaError;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class AuxiliarJusticiaCargaErrorRepository implements AuxiliarJusticiaCargaErrorService
{

    protected  $auditoriaMaestroService;

    public function __construct(AuditoriaMaestroService $auditoriaMaestroService){
        $this->auditoriaMaestroService = $auditoriaMaestroService;
    }

    public function importar($dto){
        $errores = [];
        $file = base64_decode($dto['archivo_base_64']);
        try {
            (new AuxiliarJusticiaImport())
                ->import($file['basename'], 'local', \Maatwebsite\Excel\Excel::XLSX);
        } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            $failures = $e->failures();
            foreach ($failures as $failure) {
                array_push($errores, [
                    "row" => $failure->row(),
                    "attribute" => $failure->attribute(),
                    "errors" => $failure->errors(),
                    "values" => $failure->values()
                ]);
            }
        }

        return $errores;
    }

    public function obtenerColeccion($dto)
    {
        $data = [];
        $auxiliaresJusticiaErrores = AuxiliarJusticiaCargaError::obtenerColeccion($dto);
        foreach ($auxiliaresJusticiaErrores ?? [] as $auxiliarJusticiaError){
            array_push($data, $auxiliarJusticiaError);
        }

        $cantidadAuxiliaresJusticiaErrores = count($auxiliaresJusticiaErrores);
        $to = isset($auxiliaresJusticiaErrores) && $cantidadAuxiliaresJusticiaErrores > 0 ? $auxiliaresJusticiaErrores->currentPage() * $auxiliaresJusticiaErrores->perPage() : null;
        $to = isset($to) && isset($auxiliaresJusticiaErrores) && $to > $auxiliaresJusticiaErrores->total() && $cantidadAuxiliaresJusticiaErrores > 0 ? $auxiliaresJusticiaErrores->total() : $to;
        $from = isset($to) && isset($auxiliaresJusticiaErrores) && $cantidadAuxiliaresJusticiaErrores > 0 ?
            $auxiliaresJusticiaErrores->perPage() > $to ? 1 : ($to - $auxiliaresJusticiaErrores->perPage()) + 1
            : null;
        return [
            'datos' => $data,
            'desde' => $from,
            'hasta' => $to,
            'por_pagina' => isset($auxiliaresJusticiaErrores) && $cantidadAuxiliaresJusticiaErrores > 0 ? +$auxiliaresJusticiaErrores->perPage() : 0,
            'pagina_actual' => isset($auxiliaresJusticiaErrores) && $cantidadAuxiliaresJusticiaErrores > 0 ? $auxiliaresJusticiaErrores->currentPage() : 1,
            'ultima_pagina' => isset($auxiliaresJusticiaErrores) && $cantidadAuxiliaresJusticiaErrores > 0 ? $auxiliaresJusticiaErrores->lastPage() : 0,
            'total' => isset($auxiliaresJusticiaErrores) && $cantidadAuxiliaresJusticiaErrores > 0 ? $auxiliaresJusticiaErrores->total() : 0
        ];
    }

}
