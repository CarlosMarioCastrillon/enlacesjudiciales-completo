<?php


namespace App\Repositories\Administracion;


use App\Contracts\Administracion\ActuacionService;
use App\Contracts\ConectorService;
use App\Contracts\Seguridad\AuditoriaMaestroService;
use App\Enum\AccionAuditoriaEnum;
use App\Enum\TipoSujetoProcesalEnum;
use App\Exceptions\ModelException;
use App\Imports\ActuacionImport;
use App\Imports\ActuacionTextoExport;
use App\Jobs\LeerActuacionProcesoBroker;
use App\Model\Administracion\Actuacion;
use App\Model\Administracion\ParametroConstante;
use App\Model\Administracion\ProcesoJudicial;
use App\Model\Importacion\ActuacionCargaError;
use App\Model\Seguridad\Usuario;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class ActuacionRepository implements ActuacionService
{

    protected $auditoriaMaestroService;
    protected $conectorService;

    public function __construct(
        AuditoriaMaestroService $auditoriaMaestroService, ConectorService $conectorService
    ){
        $this->auditoriaMaestroService = $auditoriaMaestroService;
        $this->conectorService = $conectorService;
    }

    // CRUD

    public function cargar($id)
    {
        return Actuacion::with([
            'ciudad.departamento',
            'juzgado',
            'juzgadoOrigen',
            'despacho',
            'despachoOrigen',
            'etapaProceso',
            'tipoNotificacion'
        ])->where('id', $id)->first();
    }

    public function modificarOCrear($dto)
    {
        $user = Auth::user();
        $usuario = $user->usuario();
        if (!isset($dto['id'])) {
            $dto['usuario_creacion_id'] = $usuario->id ?? ($dto['usuario_creacion_id'] ?? null);
            $dto['usuario_creacion_nombre'] = $usuario->nombre ?? ($dto['usuario_creacion_nombre'] ?? null);
        }
        if (isset($usuario) || isset($dto['usuario_modificacion_id'])) {
            $dto['usuario_modificacion_id'] = $usuario->id ?? ($dto['usuario_modificacion_id'] ?? null);
            $dto['usuario_modificacion_nombre'] = $usuario->nombre ?? ($dto['usuario_modificacion_nombre'] ?? null);
        }

        // Consultar el actuacion
        $actuacion = isset($dto['id']) ? Actuacion::find($dto['id']) : new Actuacion();

        // Guardar objeto original para auditoria
        $actuacionOriginal = $actuacion->toJson();

        $actuacion->fill($dto);
        $guardado = $actuacion->save();
        if(!$guardado){
            throw new ModelException("Ocurrió un error al intentar guardar el actuacion.", $actuacion);
        }

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $actuacion->id,
            'nombre_recurso' => Actuacion::class,
            'descripcion_recurso' => $actuacion->numero_radicado_proceso_actual . '-' . $actuacion->id,
            'accion' => isset($dto['id']) ? AccionAuditoriaEnum::MODIFICAR : AccionAuditoriaEnum::CREAR,
            'recurso_original' => isset($dto['id']) ? $actuacionOriginal : $actuacion->toJson(),
            'recurso_resultante' => isset($dto['id']) ? $actuacion->toJson() : null
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $this->cargar($actuacion->id);
    }

    public function modificarMasivo($dto)
    {
        $user = Auth::user();
        $usuario = $user->usuario();
        if (!isset($dto['id'])) {
            $dto['usuario_creacion_id'] = $usuario->id ?? ($dto['usuario_creacion_id'] ?? null);
            $dto['usuario_creacion_nombre'] = $usuario->nombre ?? ($dto['usuario_creacion_nombre'] ?? null);
        }
        if (isset($usuario) || isset($dto['usuario_modificacion_id'])) {
            $dto['usuario_modificacion_id'] = $usuario->id ?? ($dto['usuario_modificacion_id'] ?? null);
            $dto['usuario_modificacion_nombre'] = $usuario->nombre ?? ($dto['usuario_modificacion_nombre'] ?? null);
        }

        // Consultar actuaciones y modificar
        $actuaciones = Actuacion::obtenerTodo($dto);
        foreach ($actuaciones as $actuacion){
            // Guardar objeto original para auditoria
            $actuacionOriginal = $actuacion->toJson();

            $actuacion->fill($dto);
            $guardado = $actuacion->save();
            if(!$guardado){
                throw new ModelException("Ocurrió un error al intentar guardar el actuacion.", $actuacion);
            }

            // Guardar auditoria
            $auditoriaDto = array(
                'id_recurso' => $actuacion->id,
                'nombre_recurso' => Actuacion::class,
                'descripcion_recurso' => $actuacion->numero_radicado_proceso_actual . '-' . $actuacion->id,
                'accion' => isset($dto['id']) ? AccionAuditoriaEnum::MODIFICAR : AccionAuditoriaEnum::CREAR,
                'recurso_original' => isset($dto['id']) ? $actuacionOriginal : $actuacion->toJson(),
                'recurso_resultante' => isset($dto['id']) ? $actuacion->toJson() : null
            );
            $this->auditoriaMaestroService->crear($auditoriaDto);
        }

        return true;
    }

    public function eliminar($id)
    {
        // Connsultar el objeto
        $actuacion = Actuacion::find($id);

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $actuacion->id,
            'nombre_recurso' => Actuacion::class,
            'descripcion_recurso' => $actuacion->numero_radicado_proceso_actual . '-' . $actuacion->id,
            'accion' => AccionAuditoriaEnum::ELIMINAR,
            'recurso_original' => $actuacion->toJson()
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $actuacion->delete();
    }

    public function eliminarMasivo($dto)
    {
        // Consultar y eliminar actuaciones
        $actuaciones = Actuacion::obtenerTodo($dto);
        foreach ($actuaciones as $actuacion){
            // No se pueden eliminar las actuaciones con autos asociados
            if(isset($actuacion->ruta_archivo_anexo)){
                continue;
            }

            // Guardar auditoria
            $auditoriaDto = array(
                'id_recurso' => $actuacion->id,
                'nombre_recurso' => Actuacion::class,
                'descripcion_recurso' => $actuacion->numero_radicado_proceso_actual . '-' . $actuacion->id,
                'accion' => AccionAuditoriaEnum::ELIMINAR,
                'recurso_original' => $actuacion->toJson()
            );
            $this->auditoriaMaestroService->crear($auditoriaDto);

            $eliminado = $actuacion->delete();
            if(!$eliminado){
                throw new ModelException("Ocurrió un error al intentar guardar el actuacion.", $actuacion);
            }
        }

        return true;
    }

    public function obtenerColeccion($dto)
    {
        $data = [];
        $actuaciones = Actuacion::obtenerColeccion($dto);
        foreach ($actuaciones ?? [] as $actuacion){
            array_push($data, $actuacion);
        }

        $cantidadActuaciones = count($actuaciones);
        $to = isset($actuaciones) && $cantidadActuaciones > 0 ? $actuaciones->currentPage() * $actuaciones->perPage() : null;
        $to = isset($to) && isset($actuaciones) && $to > $actuaciones->total() && $cantidadActuaciones> 0 ? $actuaciones->total() : $to;
        $from = isset($to) && isset($actuaciones) && $cantidadActuaciones > 0 ?
            $actuaciones->perPage() > $to ? 1 : ($to - $actuaciones->perPage()) + 1
            : null;
        return [
            'datos' => $data,
            'desde' => $from,
            'hasta' => $to,
            'por_pagina' => isset($actuaciones) && $cantidadActuaciones > 0 ? +$actuaciones->perPage() : 0,
            'pagina_actual' => isset($actuaciones) && $cantidadActuaciones > 0 ? $actuaciones->currentPage() : 1,
            'ultima_pagina' => isset($actuaciones) && $cantidadActuaciones > 0 ? $actuaciones->lastPage() : 0,
            'total' => isset($actuaciones) && $cantidadActuaciones > 0 ? $actuaciones->total() : 0
        ];
    }

    public function obtenerColeccionLigera($dto)
    {
        $actuacion = Actuacion::obtenerColeccionLigera($dto);
        return $actuacion;
    }

    // Consultas

    //Traer las consultas de todas las actuaciones y expedientes sin distinción por empresa
    public function obtenerReporte($dto)
    {
        $data = [];
        $movimientos = Actuacion::obtenerReporte($dto);
        foreach ($movimientos ?? [] as $movimiento){
            array_push($data, $movimiento);
        }

        $cantidadMovimientos = count($movimientos);
        $to = isset($movimientos) && $cantidadMovimientos > 0 ? $movimientos->currentPage() * $movimientos->perPage() : null;
        $to = isset($to) && isset($movimientos) && $to > $movimientos->total() && $cantidadMovimientos> 0 ? $movimientos->total() : $to;
        $from = isset($to) && isset($movimientos) && $cantidadMovimientos > 0 ?
            $movimientos->perPage() > $to ? 1 : ($to - $movimientos->perPage()) + 1
            : null;
        return [
            'datos' => $data,
            'desde' => $from,
            'hasta' => $to,
            'por_pagina' => isset($movimientos) && $cantidadMovimientos > 0 ? +$movimientos->perPage() : 0,
            'pagina_actual' => isset($movimientos) && $cantidadMovimientos > 0 ? $movimientos->currentPage() : 1,
            'ultima_pagina' => isset($movimientos) && $cantidadMovimientos > 0 ? $movimientos->lastPage() : 0,
            'total' => isset($movimientos) && $cantidadMovimientos > 0 ? $movimientos->total() : 0
        ];
    }

    public function obtenerReportePorEmpresa($dto)
    {
        $data = [];
        $movimientos = Actuacion::obtenerReportePorEmpresa($dto);
        foreach ($movimientos ?? [] as $movimiento){
            array_push($data, $movimiento);
        }

        $cantidadMovimientos = count($movimientos);
        $to = isset($movimientos) && $cantidadMovimientos > 0 ? $movimientos->currentPage() * $movimientos->perPage() : null;
        $to = isset($to) && isset($movimientos) && $to > $movimientos->total() && $cantidadMovimientos> 0 ? $movimientos->total() : $to;
        $from = isset($to) && isset($movimientos) && $cantidadMovimientos > 0 ?
            $movimientos->perPage() > $to ? 1 : ($to - $movimientos->perPage()) + 1
            : null;
        return [
            'datos' => $data,
            'desde' => $from,
            'hasta' => $to,
            'por_pagina' => isset($movimientos) && $cantidadMovimientos > 0 ? +$movimientos->perPage() : 0,
            'pagina_actual' => isset($movimientos) && $cantidadMovimientos > 0 ? $movimientos->currentPage() : 1,
            'ultima_pagina' => isset($movimientos) && $cantidadMovimientos > 0 ? $movimientos->lastPage() : 0,
            'total' => isset($movimientos) && $cantidadMovimientos > 0 ? $movimientos->total() : 0
        ];
    }

    public function obtenerRemates($dto)
    {
        $data = [];
        $movimientos = Actuacion::obtenerRemates($dto);
        foreach ($movimientos ?? [] as $movimiento){
            array_push($data, $movimiento);
        }

        $cantidadMovimientos = count($movimientos);
        $to = isset($movimientos) && $cantidadMovimientos > 0 ? $movimientos->currentPage() * $movimientos->perPage() : null;
        $to = isset($to) && isset($movimientos) && $to > $movimientos->total() && $cantidadMovimientos> 0 ? $movimientos->total() : $to;
        $from = isset($to) && isset($movimientos) && $cantidadMovimientos > 0 ?
            $movimientos->perPage() > $to ? 1 : ($to - $movimientos->perPage()) + 1
            : null;
        return [
            'datos' => $data,
            'desde' => $from,
            'hasta' => $to,
            'por_pagina' => isset($movimientos) && $cantidadMovimientos > 0 ? +$movimientos->perPage() : 0,
            'pagina_actual' => isset($movimientos) && $cantidadMovimientos > 0 ? $movimientos->currentPage() : 1,
            'ultima_pagina' => isset($movimientos) && $cantidadMovimientos > 0 ? $movimientos->lastPage() : 0,
            'total' => isset($movimientos) && $cantidadMovimientos > 0 ? $movimientos->total() : 0
        ];
    }

    public function obtenerRemanentes($dto)
    {
        $data = [];
        $movimientos = Actuacion::obtenerRemanentes($dto);
        foreach ($movimientos ?? [] as $movimiento){
            array_push($data, $movimiento);
        }

        $cantidadMovimientos = count($movimientos);
        $to = isset($movimientos) && $cantidadMovimientos > 0 ? $movimientos->currentPage() * $movimientos->perPage() : null;
        $to = isset($to) && isset($movimientos) && $to > $movimientos->total() && $cantidadMovimientos> 0 ? $movimientos->total() : $to;
        $from = isset($to) && isset($movimientos) && $cantidadMovimientos > 0 ?
            $movimientos->perPage() > $to ? 1 : ($to - $movimientos->perPage()) + 1
            : null;
        return [
            'datos' => $data,
            'desde' => $from,
            'hasta' => $to,
            'por_pagina' => isset($movimientos) && $cantidadMovimientos > 0 ? +$movimientos->perPage() : 0,
            'pagina_actual' => isset($movimientos) && $cantidadMovimientos > 0 ? $movimientos->currentPage() : 1,
            'ultima_pagina' => isset($movimientos) && $cantidadMovimientos > 0 ? $movimientos->lastPage() : 0,
            'total' => isset($movimientos) && $cantidadMovimientos > 0 ? $movimientos->total() : 0
        ];
    }

    public function obtenerAudienciasPendientes($dto)
    {
        $data = [];
        $movimientos = Actuacion::obtenerAudienciasPendientes($dto);
        foreach ($movimientos ?? [] as $movimiento){
            array_push($data, $movimiento);
        }

        $cantidadMovimientos = count($movimientos);
        $to = isset($movimientos) && $cantidadMovimientos > 0 ? $movimientos->currentPage() * $movimientos->perPage() : null;
        $to = isset($to) && isset($movimientos) && $to > $movimientos->total() && $cantidadMovimientos> 0 ? $movimientos->total() : $to;
        $from = isset($to) && isset($movimientos) && $cantidadMovimientos > 0 ?
            $movimientos->perPage() > $to ? 1 : ($to - $movimientos->perPage()) + 1
            : null;
        return [
            'datos' => $data,
            'desde' => $from,
            'hasta' => $to,
            'por_pagina' => isset($movimientos) && $cantidadMovimientos > 0 ? +$movimientos->perPage() : 0,
            'pagina_actual' => isset($movimientos) && $cantidadMovimientos > 0 ? $movimientos->currentPage() : 1,
            'ultima_pagina' => isset($movimientos) && $cantidadMovimientos > 0 ? $movimientos->lastPage() : 0,
            'total' => isset($movimientos) && $cantidadMovimientos > 0 ? $movimientos->total() : 0
        ];
    }

    public function obtenerAudienciasCumplidas($dto)
    {
        $data = [];
        $movimientos = Actuacion::obtenerAudienciasCumplidas($dto);
        foreach ($movimientos ?? [] as $movimiento){
            array_push($data, $movimiento);
        }

        $cantidadMovimientos = count($movimientos);
        $to = isset($movimientos) && $cantidadMovimientos > 0 ? $movimientos->currentPage() * $movimientos->perPage() : null;
        $to = isset($to) && isset($movimientos) && $to > $movimientos->total() && $cantidadMovimientos> 0 ? $movimientos->total() : $to;
        $from = isset($to) && isset($movimientos) && $cantidadMovimientos > 0 ?
            $movimientos->perPage() > $to ? 1 : ($to - $movimientos->perPage()) + 1
            : null;
        return [
            'datos' => $data,
            'desde' => $from,
            'hasta' => $to,
            'por_pagina' => isset($movimientos) && $cantidadMovimientos > 0 ? +$movimientos->perPage() : 0,
            'pagina_actual' => isset($movimientos) && $cantidadMovimientos > 0 ? $movimientos->currentPage() : 1,
            'ultima_pagina' => isset($movimientos) && $cantidadMovimientos > 0 ? $movimientos->lastPage() : 0,
            'total' => isset($movimientos) && $cantidadMovimientos > 0 ? $movimientos->total() : 0
        ];
    }

    public function obtenerActuacionesPendientesYCumplidas($dto)
    {
        $data = [];
        $movimientos = Actuacion::obtenerActuacionesPendientesYCumplidas($dto);
        foreach ($movimientos ?? [] as $movimiento){
            array_push($data, $movimiento);
        }

        $cantidadMovimientos = count($movimientos);
        $to = isset($movimientos) && $cantidadMovimientos > 0 ? $movimientos->currentPage() * $movimientos->perPage() : null;
        $to = isset($to) && isset($movimientos) && $to > $movimientos->total() && $cantidadMovimientos> 0 ? $movimientos->total() : $to;
        $from = isset($to) && isset($movimientos) && $cantidadMovimientos > 0 ?
            $movimientos->perPage() > $to ? 1 : ($to - $movimientos->perPage()) + 1
            : null;
        return [
            'datos' => $data,
            'desde' => $from,
            'hasta' => $to,
            'por_pagina' => isset($movimientos) && $cantidadMovimientos > 0 ? +$movimientos->perPage() : 0,
            'pagina_actual' => isset($movimientos) && $cantidadMovimientos > 0 ? $movimientos->currentPage() : 1,
            'ultima_pagina' => isset($movimientos) && $cantidadMovimientos > 0 ? $movimientos->lastPage() : 0,
            'total' => isset($movimientos) && $cantidadMovimientos > 0 ? $movimientos->total() : 0
        ];
    }

    // Importación de datos

    public function importar($archivo){
        // Usuario en sesión
        $user = Auth::user();
        $usuario = $user->usuario();

        // Borrar datos historicos
        // TODO validar si se borra para todas las empresas
        ActuacionCargaError::query()->delete();

        $errores = [];
        $import = new ActuacionImport($usuario->id, $usuario->nombre);
        Excel::import($import, $archivo);

        if($import->getWithErrors()){
            throw new ModelException("Revisar archivo de carga. Estructura de información no corresponde.");
        }

        foreach ($import->failures() as $failure) {
            array_push($errores, [
                "fila" => $failure->row(),
                "columna" => $failure->attribute(),
                "errores" => $failure->errors(),
                "datos" => $failure->values()
            ]);
        }

        // Procesar errores personalizados
        $erroresReporte = [];
        $actuacionesErrores = [];
        $erroresPersonalizados = $import->getCustomErrors();
        if(count($erroresPersonalizados) > 0){
            $erroresPersonalizadosColeccion = collect($erroresPersonalizados)->groupBy('key');
            foreach ($erroresPersonalizadosColeccion ?? [] as $errores){
                $errorNumeral = 0;
                $observaciones = [];
                foreach ($errores as $error){
                    $errorNumeral++;
                    array_push($observaciones, $errorNumeral . ". ". $error['observacion']);
                }

                $datosFila = $errores[0];
                if(
                    (isset($datosFila[0]) && $datosFila[0] != '') ||
                    (isset($datosFila[1]) && $datosFila[1] != '') ||
                    (isset($datosFila[2]) && $datosFila[2] != '') ||
                    (isset($datosFila[3]) && $datosFila[3] != '')
                ){
                    array_push($erroresReporte, [
                        'numero_proceso' => $datosFila[3],
                        'descripcion_actuacion' => $datosFila[6],
                        'descripcion_anotacion' => $datosFila[7],
                        'fecha_registro' => $datosFila[8],
                        'fecha_actuacion' => $datosFila[9],
                        'fecha_inicio_termino' => $datosFila[10],
                        'fecha_vencimiento_termino' => $datosFila[11],
                        'descripcion_clase_proceso' => $datosFila[12],
                        'nombres_demandantes' => $datosFila[4],
                        'nombres_demandados' => $datosFila[5],
                        'ciudad_nombre' => $datosFila[0],
                        'juzgado_nombre' => $datosFila[1],
                        'despacho_nombre' => $datosFila[2],
                        'observacion' => join("<br>", $observaciones)
                    ]);
                    array_push($actuacionesErrores, [
                        'numero_proceso' => $datosFila[3],
                        'descripcion_actuacion' => $datosFila[6],
                        'descripcion_anotacion' => $datosFila[7],
                        'fecha_registro' => $datosFila[8],
                        'fecha_actuacion' => $datosFila[9],
                        'fecha_inicio_termino' => $datosFila[10],
                        'fecha_vencimiento_termino' => $datosFila[11],
                        'descripcion_clase_proceso' => $datosFila[12],
                        'nombres_demandantes' => $datosFila[4],
                        'nombres_demandados' => $datosFila[5],
                        'ciudad_nombre' => $datosFila[0],
                        'juzgado_nombre' => $datosFila[1],
                        'despacho_nombre' => $datosFila[2],
                        'observacion' => join("\n", $observaciones),
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'usuario_creacion_id' => $usuario->id,
                        'usuario_creacion_nombre' => $usuario->nombre,
                        'usuario_modificacion_id' => $usuario->id,
                        'usuario_modificacion_nombre' => $usuario->nombre
                    ]);
                }
            }
        }

        // Cantidad de registros fallidos, OJO contabilizar antes de agregar los procesos cargados
        $registrosFallidos = count($actuacionesErrores ?? []);

        // Procesar registros importados
        $procesosImportados = $import->getImported();
        $registrosCargados = count($procesosImportados ?? []);
        if(count($procesosImportados ?? []) > 0){
            $erroresReporte = array_merge($erroresReporte, $procesosImportados);
            $actuacionesErrores = array_merge($actuacionesErrores, $procesosImportados);
        }

        // Insertar errores encontrados
        if($actuacionesErrores){
            ActuacionCargaError::insert($actuacionesErrores);
        }

        return [
            "errores" => $erroresReporte,
            "registros_fallidos" => $registrosFallidos,
            "registros_cargados" => $registrosCargados,
            "registros_procesados" => $registrosFallidos + $registrosCargados
        ];
    }

    public function importarTexto($texto)
    {
        $archivo = "actuaciones.xlsx";
        Excel::store(new ActuacionTextoExport($texto), $archivo);

        return $this->importar($archivo);
    }

    // Consultar en la rama judicial

    public function lanzarLecturaActuaciones(){
        // Parametros
        $parametros = ParametroConstante::cargarParametros();
        $horaCierreEjecucion = $parametros['CIERRE_EJECUCION_AUDITORIA'] ?? 10;
        $horaEjecucionStr = $parametros['HORA_CONSULTA_AUTOMATICA_RAMA'] ?? '01:00';
        $horaEjecucionArray = explode(":", $horaEjecucionStr);

        // Fecha actual
        $fechaActual = Carbon::now();

        // Fecha ejecución
        $fechaProgramada = Carbon::now()->hour($horaEjecucionArray[0])->minute($horaEjecucionArray[1])->second(0);
        if(abs($fechaActual->diffInMinutes($fechaProgramada)) <= 10) {
            // Fechas de referencia
            $fechaEjecucion = $fechaProgramada;
            $fechaProceso = clone $fechaEjecucion;
            if($fechaEjecucion->hour <= $horaCierreEjecucion){
                $fechaProceso = $fechaProceso->subDays(1);
            }

            // Consultar los procesos
            $procesos = DB::table('procesos_judiciales')
                ->join('ciudades', function ($join){
                    $join->on('ciudades.id', '=', 'procesos_judiciales.ciudad_id')
                        ->where('ciudades.rama_judicial', 1);
                })
                ->where('procesos_judiciales.indicativo_estado', 1)
                ->where(function($query) {
                    $query->where('procesos_judiciales.indicativo_consulta_rama', '1')
                        ->orWhere('procesos_judiciales.indicativo_tiene_vigilancia', '1');
                })
                ->select('procesos_judiciales.numero_radicado_proceso_actual')
                ->get();
            if(count($procesos ?? []) == 0){
                return false;
            }

            // Unificar procesos
            $procesos = $procesos->unique('numero_radicado_proceso_actual');

            // Enviar a la cola en lotes de 100
            $chunks = $procesos->chunk(100);
            foreach ($chunks as $key => $chunk){
                $procesos = array_map(function ($proceso){
                    return $proceso->numero_radicado_proceso_actual;
                }, $chunk->toArray());

                $job = (new LeerActuacionProcesoBroker([
                    'lote' => $key + 1,
                    'fecha_ejecucion' => $fechaEjecucion->format('Y-m-d H:i:s'),
                    'fecha_proceso' => $fechaProceso->format('Y-m-d H:i:s'),
                    'procesos' => $procesos
                ]));
                dispatch($job);
            }
        }

        return true;
    }

    public function ejecutarLecturaDeActuaciones($dto){
        // Parametros
        $parametros = ParametroConstante::cargarParametros();

        // Super usuario para auditoria
        $superUsuario = Usuario::query()
            ->join('model_has_roles', function ($join) use($parametros) {
                $join->on('model_has_roles.model_id', '=', 'usuarios.user_id')
                    ->where('model_has_roles.role_id', $parametros['SUPERUSUARIO_ROL_ID'] ?? null);
            })
            ->select('usuarios.*')
            ->where('usuarios.empresa_id','=', $parametros['EMPRESA_PRESTADORA_SERVICIO_ID'])
            ->first();

        // Fechas de referencia
        $fechaEjecucion = Carbon::parse($dto['fecha_ejecucion']);
        $fechaProceso = Carbon::parse($dto['fecha_proceso']);

        $novedades = [];
        foreach ($dto['procesos'] ?? [] as $proceso){
            $infoProceso = $this->conectorService->get([
                'timeout' => $parametros['TIMEOUT_EJECUCION_AUD_PROCESO'] ?? 10,
                'proceso_numero' => $proceso,
                'solo_activos' => true
            ]);

            // Sujetos procesales
            $demandante = "";
            $demandado = "";
            foreach ($infoProceso['sujetos_procesales'] ?? [] as $sujetoProcesales){
                if($sujetoProcesales['tipo'] == TipoSujetoProcesalEnum::DEMANDANTE){
                    $demandante .= " - " . $sujetoProcesales['nombre'] . ".";
                }
                if($sujetoProcesales['tipo'] == TipoSujetoProcesalEnum::DEMANDADO){
                    $demandado .= " - " . $sujetoProcesales['nombre'] . ".";
                }
            }

            $fechaProcesoSiguienteDia = (clone $fechaProceso)->addDays(1);
            foreach ($infoProceso["actuaciones"] ?? [] as $actuacion){
                if(
                    !isset($actuacion["fecha_actuacion"]) || $actuacion["fecha_actuacion"] == "0000-00-00" ||
                    !isset($actuacion["fecha_registro"]) || $actuacion["fecha_registro"] == "0000-00-00"
                ){
                    continue;
                }

                $fechaActuacion = Carbon::parse($actuacion["fecha_actuacion"]);
                $fechaRegistro = Carbon::parse($actuacion["fecha_registro"]);
                if(
                    (
                        isset($fechaActuacion) && $fechaActuacion->year == $fechaProceso->year &&
                        $fechaActuacion->dayOfYear == $fechaProceso->dayOfYear
                    ) ||
                    (
                        isset($fechaActuacion) && $fechaActuacion->year == $fechaProcesoSiguienteDia->year &&
                        $fechaActuacion->dayOfYear == $fechaProcesoSiguienteDia->dayOfYear
                    ) ||
                    (
                        isset($fechaRegistro) && $fechaRegistro->year == $fechaProceso->year &&
                        $fechaRegistro->dayOfYear == $fechaProceso->dayOfYear
                    ) ||
                    (
                        isset($fechaRegistro) && $fechaRegistro->year == $fechaProcesoSiguienteDia->year &&
                        $fechaRegistro->dayOfYear == $fechaProcesoSiguienteDia->dayOfYear
                    )
                ){
                    array_push($novedades, [
                        'fecha_ejecucion' => $fechaEjecucion,
                        'fecha_proceso' => $fechaProceso,
                        'numero_proceso' => $proceso,
                        'demandante' => $demandante,
                        'demandado' => $demandado,
                        'descripcion_actuacion' => html_entity_decode($actuacion["actuacion"]),
                        'descripcion_anotacion' => html_entity_decode($actuacion["anotacion"]),
                        'fecha_actuacion' => $actuacion["fecha_actuacion"] ?? null,
                        'fecha_inicio_termino' => $actuacion["fecha_inicial"],
                        'fecha_finaliza_termino' => $actuacion["fecha_final"],
                        'fecha_registro' => $actuacion["fecha_registro"]
                    ]);
                }
            }
        }

        $actuaciones = [];
        foreach ($novedades as $novedad){
            // Consultar los procesos
            $procesosJudiciales = ProcesoJudicial::query()
                ->where('procesos_judiciales.numero_radicado_proceso_actual', $novedad['numero_proceso'])
                ->where('procesos_judiciales.indicativo_estado', 1)
                ->where(function($query) {
                    $query->where('procesos_judiciales.indicativo_consulta_rama', '1')
                        ->orWhere('procesos_judiciales.indicativo_tiene_vigilancia', '1');
                })
                ->get();
            foreach ($procesosJudiciales ?? [] as $procesoJudicial){
                array_push($actuaciones, [
                    'numero_proceso' => $procesoJudicial->numero_proceso,
                    'numero_radicado_proceso_actual' => $procesoJudicial->numero_radicado_proceso_actual,
                    'actuacion' => $novedad['descripcion_actuacion'],
                    'anotacion' => $novedad['descripcion_anotacion'],
                    'fecha_registro' => $novedad['fecha_registro'],
                    'fecha_actuacion' => $novedad['fecha_actuacion'],
                    'fecha_inicio_termino' => $novedad['fecha_inicio_termino'],
                    'fecha_vencimiento_termino' => $novedad['fecha_finaliza_termino'],
                    'descripcion_clase_proceso' => $procesoJudicial->descripcion_clase_proceso,
                    'ind_actuacion_audiencia' => null,
                    'tipo_movimiento' => 'AR',
                    'nombres_demandantes' => $novedad['demandante'] != "" ?
                        $novedad['demandante'] : $procesoJudicial->nombres_demandantes,
                    'nombres_demandados' => $novedad['demandado'] != "" ?
                        $novedad['demandado'] : $procesoJudicial->nombres_demandados,
                    'observaciones' => null,
                    'proceso_id' => $procesoJudicial->proceso_id_referencia,
                    'ciudad_id' => $procesoJudicial->ciudad_id,
                    'juzgado_id' => $procesoJudicial->juzgado_id,
                    'juzgado_origen_id' => $procesoJudicial->juzgado_origen_id,
                    'despacho_id' => $procesoJudicial->despacho_id,
                    'despacho_origen_id' => $procesoJudicial->despacho_origen_id,
                    'empresa_id' => $procesoJudicial->empresa_id,
                    'etapa_proceso_id' => null,
                    'tipo_actuacion_id' => null,
                    'tipo_notificacion_id' => null,
                    'nombre_archivo_anexo' => null,
                    'ruta_archivo_anexo' => null,
                    'ruta_archivo_estado_cartelera' => null,
                    'nombre_archivo_estado_cartelera' => null,
                    'estado' => '1',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'usuario_creacion_id' => $superUsuario->id ?? 1,
                    'usuario_creacion_nombre' => $superUsuario->nombre ?? 'Super usuario',
                    'usuario_modificacion_id' => $superUsuario->id ?? 1,
                    'usuario_modificacion_nombre' => $superUsuario->nombre ?? 'Super usuario'
                ]);
            }
        }

        if(count($actuaciones) > 0){
            Actuacion::insert($actuaciones);
        }
    }

}
