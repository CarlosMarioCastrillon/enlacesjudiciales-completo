<?php


namespace App\Repositories\Administracion;

use App\Contracts\Administracion\AuditoriaProcesoService;
use App\Contracts\ConectorService;
use App\Enum\EstadoEjecucionEnum;
use App\Enum\TipoNovedadEnum;
use App\Enum\TipoSujetoProcesalEnum;
use App\Exports\AuditoriaProcesoExport;
use App\Exports\NovedadProcesoExport;
use App\Imports\ProcesoRamaJudicialImport;
use App\Jobs\AuditoriaProcesoBroker;
use App\Model\Administracion\AuditoriaProceso;
use App\Model\Administracion\NovedadProceso;
use App\Model\Administracion\ParametroConstante;
use App\Model\Administracion\ProcesoRamaJudicial;
use App\Model\Administracion\ProgramacionAuditoriaProceso;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;

class AuditoriaProcesoRepository implements AuditoriaProcesoService
{

    protected  $conectorService;

    public function __construct(ConectorService $conectorService){
        $this->conectorService = $conectorService;
    }

    public function cargarProcesos($archivo)
    {
        // Borrar los procesos actuales
        ProcesoRamaJudicial::query()->delete();

        // Importar procesos
        $import = new ProcesoRamaJudicialImport();
        Excel::import($import, $archivo);

        return $import->getImportedRows();
    }

    public function programar($dto)
    {
        $dto['recientes'] = is_string($dto['recientes']) ?
            filter_var($dto['recientes'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) :
            (bool) $dto['recientes'];
        $programacion = ProgramacionAuditoriaProceso::create($dto);
        return $programacion;
    }

    public function getColeccion($dto)
    {
        $data = [];
        $programaciones = ProgramacionAuditoriaProceso::obtenerColeccion($dto);
        foreach ($programaciones ?? [] as $programacion){
            array_push($data, $programacion);
        }

        $cantidadProgramaciones = count($programaciones);
        $to = isset($programaciones) && $cantidadProgramaciones > 0 ? $programaciones->currentPage() * $programaciones->perPage() : null;
        $to = isset($to) && isset($programaciones) && $to > $programaciones->total() && $cantidadProgramaciones > 0 ? $programaciones->total() : $to;
        $from = isset($to) && isset($programaciones) && $cantidadProgramaciones > 0 ?
            $programaciones->perPage() > $to ? 1 : ($to - $programaciones->perPage()) + 1
            : null;
        return [
            'datos' => $data,
            'desde' => $from,
            'hasta' => $to,
            'por_pagina' => isset($programaciones) && $cantidadProgramaciones > 0 ? +$programaciones->perPage() : 0,
            'pagina_actual' => isset($programaciones) && $cantidadProgramaciones > 0 ? $programaciones->currentPage() : 1,
            'ultima_pagina' => isset($programaciones) && $cantidadProgramaciones > 0 ? $programaciones->lastPage() : 0,
            'total' => isset($programaciones) && $cantidadProgramaciones > 0 ? $programaciones->total() : 0
        ];
    }

    public function getExcelAuditorias($programacionId)
    {
        return (new AuditoriaProcesoExport($programacionId));
    }

    public function getExcelNovedades($programacionId)
    {
        return (new NovedadProcesoExport($programacionId));
    }

    public function lanzarAuditoria(){
        // Parametros
        $parametros = ParametroConstante::cargarParametros();
        $horaCierreEjecucion = $parametros['CIERRE_EJECUCION_AUDITORIA'] ?? 10;

        // Fecha actual
        $fechaActual = Carbon::now();

        // Obtener programación pendiente
        $programacion = ProgramacionAuditoriaProceso::orderBy('id', 'desc')
            ->where('estado', EstadoEjecucionEnum::SIN_EJECUTAR)
            ->first();
        if(!isset($programacion->fecha_programacion)){
            return true;
        }

        // Enviar procesos a ejecución
        $fechaProgramada = Carbon::parse($programacion->fecha_programacion);
        if(abs($fechaActual->diffInMinutes($fechaProgramada)) <= 30) {
            // Fechas de referencia
            $fechaEjecucion = isset($programacion->fecha_programacion_pasada) ?
                Carbon::parse($programacion->fecha_programacion_pasada) : $fechaProgramada;
            $fechaProceso = clone $fechaEjecucion;
            if($fechaEjecucion->hour <= $horaCierreEjecucion){
                $fechaProceso = $fechaProceso->subDays(1);
            }

            // Enviar mensajes a la cola
            $procesos = ProcesoRamaJudicial::get();
            $procesos = $procesos->unique('numero_proceso');
            if(count($procesos) == 0){
                return false;
            }

            // Lotes de 100
            $chunks = $procesos->chunk(100);
            $lotes = count($chunks);

            // Actualizar estado de la programación
            $programacion->estado = EstadoEjecucionEnum::EN_EJECUCION;
            $programacion->fecha_ejecucion = $fechaEjecucion;
            $programacion->fecha_proceso = $fechaProceso;
            $programacion->lotes = $lotes;
            $programacion->lotes_ejecutados = 0;
            $programacion->save();

            foreach ($chunks as $key => $chunk){
                $procesos = array_map(function ($proceso){
                    return $proceso['numero_proceso'];
                }, $chunk->toArray());

                $job = (new AuditoriaProcesoBroker([
                    'id' => $programacion->id,
                    'lote' => $key + 1,
                    'procesos' => $procesos
                ]));
                dispatch($job);
            }
        }

        return true;
    }

    public function ejecutarAuditoria($dto){
        // Parametros
        $parametros = ParametroConstante::cargarParametros();

        // Obtener progración
        $programacion = ProgramacionAuditoriaProceso::find($dto['id']);

        // Fechas de referencia
        $fechaEjecucion = Carbon::parse($programacion->fecha_ejecucion);
        $fechaProceso = Carbon::parse($programacion->fecha_proceso);

        // Consultar procesos en la rama judicial
        $auditorias = [];
        $novedades = [];
        foreach ($dto['procesos'] ?? [] as $proceso){
            $infoProceso = $this->conectorService->get([
                'timeout' => $parametros['TIMEOUT_EJECUCION_AUD_PROCESO'] ?? 10,
                'proceso_numero' => $proceso,
                'solo_activos' => !!$programacion->recientes,
                'consulta_reducida' => true
            ]);

            $conNovedadesInicial = false;
            $fechaProcesoSiguienteDia = (clone $fechaProceso)->addDays(1);
            foreach ($infoProceso["actuaciones"] ?? [] as $actuacion){
                if(
                    !isset($actuacion["fecha_actuacion"]) || $actuacion["fecha_actuacion"] == "0000-00-00" ||
                    !isset($actuacion["fecha_registro"]) || $actuacion["fecha_registro"] == "0000-00-00"
                ){
                    continue;
                }

                $fechaActuacion = Carbon::parse($actuacion["fecha_actuacion"]);
                $fechaRegistro = Carbon::parse($actuacion["fecha_registro"]);
                if(
                    (
                        isset($fechaActuacion) && $fechaActuacion->year == $fechaProceso->year &&
                        $fechaActuacion->dayOfYear == $fechaProceso->dayOfYear
                    ) ||
                    (
                        isset($fechaActuacion) && $fechaActuacion->year == $fechaProcesoSiguienteDia->year &&
                        $fechaActuacion->dayOfYear == $fechaProcesoSiguienteDia->dayOfYear
                    ) ||
                    (
                        isset($fechaRegistro) && $fechaRegistro->year == $fechaProceso->year &&
                        $fechaRegistro->dayOfYear == $fechaProceso->dayOfYear
                    ) ||
                    (
                        isset($fechaRegistro) && $fechaRegistro->year == $fechaProcesoSiguienteDia->year &&
                        $fechaRegistro->dayOfYear == $fechaProcesoSiguienteDia->dayOfYear
                    )
                ){
                    $conNovedadesInicial = true;
                }
            }

            $conNovedades = false;
            if($conNovedadesInicial){
                $infoProceso = $this->conectorService->get([
                    'proceso_numero' => $proceso,
                    'solo_activos' => !!$programacion->recientes
                ]);

                // Ciudad
                $ciudad = null;
                if(isset($infoProceso['despacho'])){
                    $despachoArray = explode(" DE ", $infoProceso['despacho']);
                    if(count($despachoArray) > 0){
                        $ciudad = trim($despachoArray[count($despachoArray) - 1]);
                    }
                }

                // Sujetos procesales
                $demandante = "";
                $demandado = "";
                foreach ($infoProceso['sujetos_procesales'] ?? [] as $sujetoProcesales){
                    if($sujetoProcesales['tipo'] == TipoSujetoProcesalEnum::DEMANDANTE){
                        $demandante .= " - " . $sujetoProcesales['nombre'] . ".";
                    }
                    if($sujetoProcesales['tipo'] == TipoSujetoProcesalEnum::DEMANDADO){
                        $demandado .= " - " . $sujetoProcesales['nombre'] . ".";
                    }
                }

                $fechaProcesoSiguienteDia = (clone $fechaProceso)->addDays(1);
                foreach ($infoProceso["actuaciones"] ?? [] as $actuacion){
                    if(
                        !isset($actuacion["fecha_actuacion"]) || $actuacion["fecha_actuacion"] == "0000-00-00" ||
                        !isset($actuacion["fecha_registro"]) || $actuacion["fecha_registro"] == "0000-00-00"
                    ){
                        continue;
                    }

                    $fechaActuacion = Carbon::parse($actuacion["fecha_actuacion"]);
                    $fechaRegistro = Carbon::parse($actuacion["fecha_registro"]);
                    if(
                        (
                            isset($fechaActuacion) && $fechaActuacion->year == $fechaProceso->year &&
                            $fechaActuacion->dayOfYear == $fechaProceso->dayOfYear
                        ) ||
                        (
                            isset($fechaActuacion) && $fechaActuacion->year == $fechaProcesoSiguienteDia->year &&
                            $fechaActuacion->dayOfYear == $fechaProcesoSiguienteDia->dayOfYear
                        ) ||
                        (
                            isset($fechaRegistro) && $fechaRegistro->year == $fechaProceso->year &&
                            $fechaRegistro->dayOfYear == $fechaProceso->dayOfYear
                        ) ||
                        (
                            isset($fechaRegistro) && $fechaRegistro->year == $fechaProcesoSiguienteDia->year &&
                            $fechaRegistro->dayOfYear == $fechaProcesoSiguienteDia->dayOfYear
                        )
                    ){
                        $conNovedades = true;
                        array_push($novedades, [
                            'fecha_ejecucion' => $fechaEjecucion,
                            'fecha_proceso' => $fechaProceso,
                            'numero_proceso' => $proceso,
                            'departamento' => $infoProceso["departamento"] ?? null,
                            'ciudad' => $ciudad,
                            'despacho' => $infoProceso["despacho"] ?? null,
                            'demandante' => $demandante,
                            'demandado' => $demandado,
                            'fecha_actuacion' => $actuacion["fecha_actuacion"] ?? null,
                            'descripcion_actuacion' => html_entity_decode($actuacion["actuacion"]),
                            'descripcion_anotacion' => html_entity_decode($actuacion["anotacion"]),
                            'fecha_inicio_termino' => $actuacion["fecha_inicial"],
                            'fecha_finaliza_termino' => $actuacion["fecha_final"],
                            'fecha_registro' => $actuacion["fecha_registro"],
                            'programacion_auditoria_proceso_id' => $programacion->id
                        ]);
                    }
                }
            }

            if($conNovedades){
                // Si presenta novedades para el día de proceso
                $tipoNovedad = TipoNovedadEnum::CON_ACTUACION;
            }else{
                // Si no presenta novedades para el día de proceso
                $tipoNovedad = TipoNovedadEnum::SIN_ACTUACION;
            }

            // Si el proceso es privado
            if($infoProceso['es_privado']){
                $tipoNovedad = TipoNovedadEnum::ES_PRIVADO;
            }

            //  Si no hay conexción con la rama
            if(!$infoProceso['con_conexion']){
                $tipoNovedad = TipoNovedadEnum::SIN_CONEXION;
            }

            array_push($auditorias, [
                'fecha_ejecucion' => $fechaEjecucion,
                'fecha_proceso' => $fechaProceso,
                'numero_proceso' => $proceso,
                'tipo_novedad' => $tipoNovedad,
                'programacion_auditoria_proceso_id' => $programacion->id
            ]);
        }

        if(count($auditorias) > 0){
            AuditoriaProceso::insert($auditorias);
        }
        if(count($novedades) > 0){
            NovedadProceso::insert($novedades);
        }

        // Actualizar la programación
        $programacion->lotes_ejecutados += 1;
        if($programacion->lotes_ejecutados == $programacion->lotes){
            $programacion->estado = EstadoEjecucionEnum::EJECUTADO;
        }
        $programacion->save();
    }

}
