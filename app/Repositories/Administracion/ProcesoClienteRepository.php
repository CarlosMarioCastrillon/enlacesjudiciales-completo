<?php


namespace App\Repositories\Administracion;


use App\Contracts\Administracion\ProcesoClienteService;
use App\Contracts\Seguridad\AuditoriaMaestroService;
use App\Enum\AccionAuditoriaEnum;
use App\Exceptions\ModelException;
use App\Model\Administracion\ProcesoCliente;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class ProcesoClienteRepository implements ProcesoClienteService
{

    protected  $auditoriaMaestroService;

    public function __construct(AuditoriaMaestroService $auditoriaMaestroService){
        $this->auditoriaMaestroService = $auditoriaMaestroService;
    }

    public function cargar($id)
    {
        $procesoCliente = ProcesoCliente::find($id);
        $ciudad = $procesoCliente->ciudad;
        $procesoJudicial = $procesoCliente->procesoJudicial;
        $juzgado = $procesoCliente->juzgado;

        return [
            'id' => $procesoCliente->id,
            'numero_proceso' => $procesoCliente->numero_proceso,
            'estado' => $procesoCliente->estado,
            'usuario_creacion_id' => $procesoCliente->usuario_creacion_id,
            'usuario_creacion_nombre' => $procesoCliente->usuario_creacion_nombre,
            'usuario_modificacion_id' => $procesoCliente->usuario_modificacion_id,
            'usuario_modificacion_nombre' => $procesoCliente->usuario_modificacion_nombre,
            'fecha_creacion' => (new Carbon($procesoCliente->created_at))->format("Y-m-d H:i:s"),
            'fecha_modificacion' => (new Carbon($procesoCliente->updated_at))->format("Y-m-d H:i:s"),
            'ciudad' => isset($ciudad) ? [
                'id' => $ciudad->id,
                'nombre' => $ciudad->nombre,
                'ciudad_codigo' => $ciudad->codigo_dane,
            ] : null,
            'juzgado' => isset($juzgado) ? [
                'id' => $juzgado->id,
                'nombre' => $juzgado->nombre,
                'juzgado_actual' => $juzgado->juzgado_rama,
            ] : null,
            'procesoJudicial' => isset($procesoJudicial) ? [
                'id' => $procesoJudicial->id,
                'numero_proceso' => $procesoJudicial->numero_proceso,
                'demandante' => $procesoJudicial->nombres_demandantes,
                'demandado' => $procesoJudicial->nombres_demandados,
            ] : null
        ];
    }

    public function modificarOCrear($dto)
    {
        $user = Auth::user();
        $usuario = $user->usuario();
        if (!isset($dto['id'])) {
            $dto['usuario_creacion_id'] = $usuario->id ?? ($dto['usuario_creacion_id'] ?? null);
            $dto['usuario_creacion_nombre'] = $usuario->nombre ?? ($dto['usuario_creacion_nombre'] ?? null);
        }
        if (isset($usuario) || isset($dto['usuario_modificacion_id'])) {
            $dto['usuario_modificacion_id'] = $usuario->id ?? ($dto['usuario_modificacion_id'] ?? null);
            $dto['usuario_modificacion_nombre'] = $usuario->nombre ?? ($dto['usuario_modificacion_nombre'] ?? null);
        }

        // Consultar el proceso por cliente
        $procesoCliente = isset($dto['id']) ? ProcesoCliente::find($dto['id']) : new ProcesoCliente();

        // Guardar objeto original para auditoria
        $procesoClienteOriginal = $procesoCliente->toJson();

        $procesoCliente->fill($dto);
        $procesoCliente->empresa_id = $user->empresa()->id;
        $guardado = $procesoCliente->save();

        if(!$guardado){
            throw new ModelException("Ocurrió un error al intentar guardar el Cliente.", $procesoCliente);
        }

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $procesoCliente->id,
            'nombre_recurso' => ProcesoCliente::class,
            'descripcion_recurso' => $procesoCliente->procesoJudicial->numero_proceso,
            'accion' => isset($dto['id']) ? AccionAuditoriaEnum::MODIFICAR : AccionAuditoriaEnum::CREAR,
            'recurso_original' => isset($dto['id']) ? $procesoClienteOriginal : $procesoCliente->toJson(),
            'recurso_resultante' => isset($dto['id']) ? $procesoCliente->toJson() : null
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $this->cargar($procesoCliente->id);
    }

    public function eliminar($id)
    {
        // Connsultar el objeto
        $procesoCliente = ProcesoCliente::find($id);

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $procesoCliente->id,
            'nombre_recurso' => ProcesoCliente::class,
            'descripcion_recurso' =>  $procesoCliente->procesoJudicial->numero_proceso,
            'accion' => AccionAuditoriaEnum::ELIMINAR,
            'recurso_original' => $procesoCliente->toJson()
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $procesoCliente->delete();
    }

    public function obtenerColeccion($dto)
    {
        $data = [];
        $procesosClientes = ProcesoCliente::obtenerColeccion($dto);
        foreach ($procesosClientes ?? [] as $shipping) {
            array_push($data, $shipping);
        }

        $cantidadProcesosClientes = count($procesosClientes);
        $to = isset($procesosClientes) && $cantidadProcesosClientes > 0 ? $procesosClientes->currentPage() * $procesosClientes->perPage() : null;
        $to = isset($to) && isset($procesosClientes) && $to > $procesosClientes->total() && $cantidadProcesosClientes > 0 ? $procesosClientes->total() : $to;
        $from = isset($to) && isset($procesosClientes) && $cantidadProcesosClientes > 0 ?
            $procesosClientes->perPage() > $to ? 1 : ($to - $procesosClientes->perPage()) + 1
            : null;
        return [
            'datos' => $data,
            'desde' => $from,
            'hasta' => $to,
            'por_pagina' => isset($procesosClientes) && $cantidadProcesosClientes > 0 ? +$procesosClientes->perPage() : 0,
            'pagina_actual' => isset($procesosClientes) && $cantidadProcesosClientes > 0 ? $procesosClientes->currentPage() : 1,
            'ultima_pagina' => isset($procesosClientes) && $cantidadProcesosClientes > 0 ? $procesosClientes->lastPage() : 0,
            'total' => isset($procesosClientes) && $cantidadProcesosClientes > 0 ? $procesosClientes->total() : 0
        ];
    }
}
