<?php


namespace App\Repositories\Administracion;


use App\Contracts\Administracion\ProcesoValorService;
use App\Contracts\Seguridad\AuditoriaMaestroService;
use App\Enum\AccionAuditoriaEnum;
use App\Exceptions\ModelException;
use App\Model\Administracion\ProcesoValor;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class ProcesoValorRepository implements ProcesoValorService
{

    protected  $auditoriaMaestroService;

    public function __construct(AuditoriaMaestroService $auditoriaMaestroService){
        $this->auditoriaMaestroService = $auditoriaMaestroService;
    }

    public function cargar($id)
    {
        $procesoValor = ProcesoValor::find($id);
        $concepto = $procesoValor->concepto;

        return [
            'id' => $procesoValor->id,
            'concepto_id' => $procesoValor->concepto_id,
            'fecha' => $procesoValor->fecha,
            'valor' => $procesoValor->valor,
            'observacion' => $procesoValor->observacion,
            'estado' => $procesoValor->estado,
            'usuario_creacion_id' => $procesoValor->usuario_creacion_id,
            'usuario_creacion_nombre' => $procesoValor->usuario_creacion_nombre,
            'usuario_modificacion_id' => $procesoValor->usuario_modificacion_id,
            'usuario_modificacion_nombre' => $procesoValor->usuario_modificacion_nombre,
            'fecha_creacion' => (new Carbon($procesoValor->created_at))->format("Y-m-d H:i:s"),
            'fecha_modificacion' => (new Carbon($procesoValor->updated_at))->format("Y-m-d H:i:s"),
            'concepto' => isset($concepto) ? [
                'id' => $concepto->id,
                'nombre' => $concepto->nombre,
                'tipo_concepto' => $concepto->tipo_concepto,
            ] : null
        ];
    }

    public function modificarOCrear($dto)
    {
        $user = Auth::user();
        $usuario = $user->usuario();
        if (!isset($dto['id'])) {
            $dto['usuario_creacion_id'] = $usuario->id ?? ($dto['usuario_creacion_id'] ?? null);
            $dto['usuario_creacion_nombre'] = $usuario->nombre ?? ($dto['usuario_creacion_nombre'] ?? null);
        }
        if (isset($usuario) || isset($dto['usuario_modificacion_id'])) {
            $dto['usuario_modificacion_id'] = $usuario->id ?? ($dto['usuario_modificacion_id'] ?? null);
            $dto['usuario_modificacion_nombre'] = $usuario->nombre ?? ($dto['usuario_modificacion_nombre'] ?? null);
        }

        // Consultar el procesoValor
        $procesoValor = isset($dto['id']) ? ProcesoValor::find($dto['id']) : new ProcesoValor();

        // Guardar objeto original para auditoria
        $procesoValorOriginal = $procesoValor->toJson();

        $procesoValor->fill($dto);
        $procesoValor->empresa_id = $user->empresa()->id;
        $guardado = $procesoValor->save();

        if(!$guardado){
            throw new ModelException("Ocurrió un error al intentar guardar el valor.", $procesoValor);
        }

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $procesoValor->id,
            'nombre_recurso' => ProcesoValor::class,
            'descripcion_recurso' => $procesoValor->concepto->nombre,
            'accion' => isset($dto['id']) ? AccionAuditoriaEnum::MODIFICAR : AccionAuditoriaEnum::CREAR,
            'recurso_original' => isset($dto['id']) ? $procesoValorOriginal : $procesoValor->toJson(),
            'recurso_resultante' => isset($dto['id']) ? $procesoValor->toJson() : null
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $this->cargar($procesoValor->id);
    }

    public function eliminar($id)
    {
        // Connsultar el objeto
        $procesoValor = ProcesoValor::find($id);

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $procesoValor->id,
            'nombre_recurso' => ProcesoValor::class,
            'descripcion_recurso' =>  $procesoValor->concepto->nombre,
            'accion' => AccionAuditoriaEnum::ELIMINAR,
            'recurso_original' => $procesoValor->toJson()
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $procesoValor->delete();
    }

    public function obtener($dto)
    {
        $user = Auth::user();
        $empresa = $user->empresa();
        $procesosValor = ProcesoValor::with('concepto')->where('empresa_id', $empresa->id)->where('proceso_id', $dto['proceso_id'])->orderBy('fecha')->get();

        // Agrupar por tipo concepto
        $procesosValorPorTipoConcepto = $procesosValor->groupBy(function ($procesoValor, $key) {
            return $procesoValor->concepto->tipo_concepto;
        });

        // Carcular totales
        $reporteProcesosValor = [];

        foreach ($procesosValorPorTipoConcepto as $key => $procesosValor){
            $totalCredito = $procesosValor->where('estado', '=', 1)->sum(function ($procesoValor) {
                return $procesoValor->concepto->clase_concepto == 'CR' ? $procesoValor->valor : 0;
            });
            $totalDebito = $procesosValor->where('estado', '=', 1)->sum(function ($procesoValor) {
                return $procesoValor->concepto->clase_concepto == 'DB' ? $procesoValor->valor : 0;
            });
            $saldo = $totalCredito - $totalDebito;
            array_push($reporteProcesosValor, [
                'tipo_concepto' => $key,
                'total_credito' => $totalCredito,
                'total_debito' => $totalDebito,
                'saldo' => $saldo,
                'procesos_valor' => $procesosValor
            ]);
        }

        return $reporteProcesosValor;
    }
}
