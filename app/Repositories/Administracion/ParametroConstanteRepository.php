<?php


namespace App\Repositories\Administracion;


use App\Contracts\Administracion\ParametroConstanteService;
use App\Contracts\Seguridad\AuditoriaMaestroService;
use App\Enum\AccionAuditoriaEnum;
use App\Exceptions\ModelException;
use App\Model\Administracion\ParametroConstante;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class ParametroConstanteRepository implements ParametroConstanteService
{

    protected  $auditoriaMaestroService;

    public function __construct(AuditoriaMaestroService $auditoriaMaestroService){
        $this->auditoriaMaestroService = $auditoriaMaestroService;
    }

    public function cargar($id)
    {
        $parametro = ParametroConstante::find($id);

        return [
            'id' => $parametro->id,
            'codigo_parametro' => $parametro->codigo_parametro,
            'descripcion_parametro' =>$parametro->descripcion_parametro,
            'valor_parametro' => $parametro->valor_parametro,
            'estado_registro' => $parametro->estado_registro,
            'usuario_creacion_id' => $parametro->usuario_creacion_id,
            'usuario_creacion_nombre' => $parametro->usuario_creacion_nombre,
            'usuario_modificacion_id' => $parametro->usuario_modificacion_id,
            'usuario_modificacion_nombre' => $parametro->usuario_modificacion_nombre,
            'fecha_creacion' => (new Carbon($parametro->created_at))->format("Y-m-d H:i:s"),
            'fecha_modificacion' => (new Carbon($parametro->updated_at))->format("Y-m-d H:i:s"),
        ];
    }

    public function modificarOCrear($dto)
    {
        $user = Auth::user();
        $usuario = $user->usuario();
        if (!isset($dto['id'])) {
            $dto['usuario_creacion_id'] = $usuario->id ?? ($dto['usuario_creacion_id'] ?? null);
            $dto['usuario_creacion_nombre'] = $usuario->nombre ?? ($dto['usuario_creacion_nombre'] ?? null);
        }
        if (isset($usuario) || isset($dto['usuario_modificacion_id'])) {
            $dto['usuario_modificacion_id'] = $usuario->id ?? ($dto['usuario_modificacion_id'] ?? null);
            $dto['usuario_modificacion_nombre'] = $usuario->nombre ?? ($dto['usuario_modificacion_nombre'] ?? null);
        }

        // Consultar el servicio
        $parametro = isset($dto['id']) ? ParametroConstante::find($dto['id']) : new ParametroConstante();

        // Guardar objeto original para auditoria
        $parametroOriginal = $parametro->toJson();

        $parametro->fill($dto);
        $guardado = $parametro->save();
        if(!$guardado){
            throw new ModelException("Ocurrió un error al intentar guardar el parametro.", $parametro);
        }

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $parametro->id,
            'nombre_recurso' => ParametroConstante::class,
            'descripcion_recurso' => $parametro->nombre,
            'accion' => isset($dto['id']) ? AccionAuditoriaEnum::MODIFICAR : AccionAuditoriaEnum::CREAR,
            'recurso_original' => isset($dto['id']) ? $parametroOriginal : $parametro->toJson(),
            'recurso_resultante' => isset($dto['id']) ? $parametro->toJson() : null
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $this->cargar($parametro->id);
    }

    public function eliminar($id)
    {
        // Connsultar el objeto
        $parametro = ParametroConstante::find($id);

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $parametro->id,
            'nombre_recurso' => ParametroConstante::class,
            'descripcion_recurso' => $parametro->nombre,
            'accion' => AccionAuditoriaEnum::ELIMINAR,
            'recurso_original' => $parametro->toJson()
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $parametro->delete();
    }

    public function obtenerColeccion($dto)
    {
        $data = [];
        $parametros = ParametroConstante::obtenerColeccion($dto);
        foreach ($parametros ?? [] as $shipping){
            array_push($data, $shipping);
        }

        $cantidadParametros = count($parametros);
        $to = isset($parametros) && $cantidadParametros > 0 ? $parametros->currentPage() * $parametros->perPage() : null;
        $to = isset($to) && isset($parametros) && $to > $parametros->total() && $cantidadParametros> 0 ? $parametros->total() : $to;
        $from = isset($to) && isset($parametros) && $cantidadParametros > 0 ?
            $parametros->perPage() > $to ? 1 : ($to - $parametros->perPage()) + 1
            : null;
        return [
            'datos' => $data,
            'desde' => $from,
            'hasta' => $to,
            'por_pagina' => isset($parametros) && $cantidadParametros > 0 ? +$parametros->perPage() : 0,
            'pagina_actual' => isset($parametros) && $cantidadParametros > 0 ? $parametros->currentPage() : 1,
            'ultima_pagina' => isset($parametros) && $cantidadParametros > 0 ? $parametros->lastPage() : 0,
            'total' => isset($parametros) && $cantidadParametros > 0 ? $parametros->total() : 0
        ];
    }
}
