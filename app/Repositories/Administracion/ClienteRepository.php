<?php


namespace App\Repositories\Administracion;


use App\Contracts\Administracion\ClienteService;
use App\Contracts\Seguridad\AuditoriaMaestroService;
use App\Enum\AccionAuditoriaEnum;
use App\Exceptions\ModelException;
use App\Exports\ClienteExport;
use App\Model\Administracion\Cliente;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class ClienteRepository implements ClienteService
{

    protected  $auditoriaMaestroService;

    public function __construct(AuditoriaMaestroService $auditoriaMaestroService){
        $this->auditoriaMaestroService = $auditoriaMaestroService;
    }

    public function cargar($id)
    {
        $cliente = Cliente::find($id);
        $tipoDocumento = $cliente->tipoDocumento;
        $ciudad =$cliente->ciudad;
        $departamento = isset($ciudad) ? $ciudad->departamento : null;

        return [
            'id' => $cliente->id,
            'numero_documento' => $cliente->numero_documento,
            'nombre' => $cliente->nombre,
            'direccion' => $cliente->direccion,
            'email' => $cliente->email,
            'telefono_uno' => $cliente->telefono_uno,
            'telefono_dos' => $cliente->telefono_dos,
            'estado' => $cliente->estado,
            'usuario_creacion_id' => $cliente->usuario_creacion_id,
            'usuario_creacion_nombre' => $cliente->usuario_creacion_nombre,
            'usuario_modificacion_id' => $cliente->usuario_modificacion_id,
            'usuario_modificacion_nombre' => $cliente->usuario_modificacion_nombre,
            'fecha_creacion' => (new Carbon($cliente->created_at))->format("Y-m-d H:i:s"),
            'fecha_modificacion' => (new Carbon($cliente->updated_at))->format("Y-m-d H:i:s"),
            'tipo_documento' => isset($tipoDocumento) ? [
                'id' => $tipoDocumento->id,
                'codigo' => $tipoDocumento->codigo
            ] : null,
            'departamento' => isset($departamento) ? [
                'id' => $departamento->id,
                'nombre' => $departamento->nombre
            ] : null,
            'ciudad' => isset($ciudad) ? [
                'id' => $ciudad->id,
                'nombre' => $ciudad->nombre
            ] : null
        ];
    }

    public function modificarOCrear($dto)
    {
        $user = Auth::user();
        $usuario = $user->usuario();
        if (!isset($dto['id'])) {
            $dto['usuario_creacion_id'] = $usuario->id ?? ($dto['usuario_creacion_id'] ?? null);
            $dto['usuario_creacion_nombre'] = $usuario->nombre ?? ($dto['usuario_creacion_nombre'] ?? null);
        }
        if (isset($usuario) || isset($dto['usuario_modificacion_id'])) {
            $dto['usuario_modificacion_id'] = $usuario->id ?? ($dto['usuario_modificacion_id'] ?? null);
            $dto['usuario_modificacion_nombre'] = $usuario->nombre ?? ($dto['usuario_modificacion_nombre'] ?? null);
        }

        // Consultar el servicio
        $cliente = isset($dto['id']) ? Cliente::find($dto['id']) : new Cliente();

        // Guardar objeto original para auditoria
        $clienteOriginal = $cliente->toJson();

        $cliente->fill($dto);
        $cliente->empresa_id = $user->empresa()->id;
        $guardado = $cliente->save();
        if(!$guardado){
            throw new ModelException("Ocurrió un error al intentar guardar el cliente.", $cliente);
        }

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $cliente->id,
            'nombre_recurso' => Cliente::class,
            'descripcion_recurso' => $cliente->nombre,
            'accion' => isset($dto['id']) ? AccionAuditoriaEnum::MODIFICAR : AccionAuditoriaEnum::CREAR,
            'recurso_original' => isset($dto['id']) ? $clienteOriginal : $cliente->toJson(),
            'recurso_resultante' => isset($dto['id']) ? $cliente->toJson() : null
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $this->cargar($cliente->id);
    }

    public function eliminar($id)
    {
        // Connsultar el objeto
        $cliente = Cliente::find($id);

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $cliente->id,
            'nombre_recurso' => Cliente::class,
            'descripcion_recurso' => $cliente->nombre,
            'accion' => AccionAuditoriaEnum::ELIMINAR,
            'recurso_original' => $cliente->toJson()
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $cliente->delete();
    }

    public function obtenerColeccion($dto)
    {
        $data = [];
        $clientes = Cliente::obtenerColeccion($dto);
        foreach ($clientes ?? [] as $shipping){
            array_push($data, $shipping);
        }

        $cantidadClientes = count($clientes);
        $to = isset($clientes) && $cantidadClientes > 0 ? $clientes->currentPage() * $clientes->perPage() : null;
        $to = isset($to) && isset($clientes) && $to > $clientes->total() && $cantidadClientes> 0 ? $clientes->total() : $to;
        $from = isset($to) && isset($clientes) && $cantidadClientes > 0 ?
            $clientes->perPage() > $to ? 1 : ($to - $clientes->perPage()) + 1
            : null;
        return [
            'datos' => $data,
            'desde' => $from,
            'hasta' => $to,
            'por_pagina' => isset($clientes) && $cantidadClientes > 0 ? +$clientes->perPage() : 0,
            'pagina_actual' => isset($clientes) && $cantidadClientes > 0 ? $clientes->currentPage() : 1,
            'ultima_pagina' => isset($clientes) && $cantidadClientes > 0 ? $clientes->lastPage() : 0,
            'total' => isset($clientes) && $cantidadClientes > 0 ? $clientes->total() : 0
        ];
    }

    public function obtenerColeccionLigera($dto)
    {
        $cliente = Cliente::obtenerColeccionLigera($dto);
        return $cliente;
    }

    public function getExcel($dto){
        return (new ClienteExport($dto));
    }

}
