<?php


namespace App\Repositories\Administracion;


use App\Contracts\Administracion\ConsultaRemanenteService;
use App\Contracts\Seguridad\AuditoriaMaestroService;
use App\Model\Administracion\ConsultaRemanente;

class ConsultaRemanenteRepository implements ConsultaRemanenteService
{

    protected  $auditoriaMaestroService;

    public function __construct(AuditoriaMaestroService $auditoriaMaestroService){
        $this->auditoriaMaestroService = $auditoriaMaestroService;
    }

    public function obtenerColeccion($dto)
    {
        $data = [];
        $movimientos = ConsultaRemanente::obtenerColeccion($dto);
        foreach ($movimientos ?? [] as $movimiento){
            array_push($data, $movimiento);
        }

        $cantidadMovimientos = count($movimientos);
        $to = isset($movimientos) && $cantidadMovimientos > 0 ? $movimientos->currentPage() * $movimientos->perPage() : null;
        $to = isset($to) && isset($movimientos) && $to > $movimientos->total() && $cantidadMovimientos> 0 ? $movimientos->total() : $to;
        $from = isset($to) && isset($movimientos) && $cantidadMovimientos > 0 ?
            $movimientos->perPage() > $to ? 1 : ($to - $movimientos->perPage()) + 1
            : null;
        return [
            'datos' => $data,
            'desde' => $from,
            'hasta' => $to,
            'por_pagina' => isset($movimientos) && $cantidadMovimientos > 0 ? +$movimientos->perPage() : 0,
            'pagina_actual' => isset($movimientos) && $cantidadMovimientos > 0 ? $movimientos->currentPage() : 1,
            'ultima_pagina' => isset($movimientos) && $cantidadMovimientos > 0 ? $movimientos->lastPage() : 0,
            'total' => isset($movimientos) && $cantidadMovimientos > 0 ? $movimientos->total() : 0
        ];
    }
}
