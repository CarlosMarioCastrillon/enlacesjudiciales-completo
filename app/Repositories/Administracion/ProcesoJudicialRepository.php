<?php


namespace App\Repositories\Administracion;


use App\Contracts\Administracion\ActuacionService;
use App\Contracts\Administracion\ProcesoJudicialService;
use App\Contracts\ConectorService;
use App\Contracts\Seguridad\AuditoriaMaestroService;
use App\Enum\AccionAuditoriaEnum;
use App\Enum\CambioInstanciaEnum;
use App\Enum\TipoSujetoProcesalEnum;
use App\Exceptions\ModelException;
use App\Imports\ProcesoJudicialImport;
use App\Imports\ProcesoJudicialTextoExport;
use App\Model\Administracion\Actuacion;
use App\Model\Administracion\ParametroConstante;
use App\Model\Configuracion\Area;
use App\Model\Configuracion\Ciudad;
use App\Model\Configuracion\ClaseProceso;
use App\Model\Configuracion\Departamento;
use App\Model\Administracion\ProcesoJudicial;
use App\Model\Configuracion\Despacho;
use App\Model\Configuracion\Juzgado;
use App\Model\Importacion\ProcesoJudicialCargaError;
use App\Model\Seguridad\Usuario;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class ProcesoJudicialRepository implements ProcesoJudicialService
{

    protected $auditoriaMaestroService;
    protected $conectorService;
    protected $actuacionService;

    public function __construct(
        AuditoriaMaestroService $auditoriaMaestroService,
        ConectorService $conectorService, ActuacionService $actuacionService
    ){
        $this->auditoriaMaestroService = $auditoriaMaestroService;
        $this->conectorService = $conectorService;
        $this->actuacionService = $actuacionService;
    }

    public function cargar($id)
    {
        $procesoJudicial = ProcesoJudicial::with([
            'empresa',
            'zona',
            'area',
            'cliente',
            'abogado',
            'dependiente',
            'departamento',
            'ciudad',
            'juzgado',
            'juzgadoOrigen',
            'despacho',
            'despachoOrigen',
            'claseProceso',
            'etapaProceso',
            'tipoActuacion'
        ])->where('id', $id)->first();

        return $procesoJudicial;
    }

    public function modificarOCrear($dto)
    {
        // Información de sesión
        $user = Auth::user();
        $usuario = $user->usuario();
        $empresa = $user->empresa();

        // Auditoria
        if (!isset($dto['id'])) {
            $dto['usuario_creacion_id'] = $usuario->id ?? ($dto['usuario_creacion_id'] ?? null);
            $dto['usuario_creacion_nombre'] = $usuario->nombre ?? ($dto['usuario_creacion_nombre'] ?? null);
        }
        if (isset($usuario) || isset($dto['usuario_modificacion_id'])) {
            $dto['usuario_modificacion_id'] = $usuario->id ?? ($dto['usuario_modificacion_id'] ?? null);
            $dto['usuario_modificacion_nombre'] = $usuario->nombre ?? ($dto['usuario_modificacion_nombre'] ?? null);
        }

        // Parametros
        $parametros = ParametroConstante::cargarParametros();

        // Consultar el procesoJudicial
        $procesoJudicial = isset($dto['id']) ? ProcesoJudicial::find($dto['id']) : new ProcesoJudicial();

        // Guardar objeto original para auditoria
        $procesoJudicialOriginal = $procesoJudicial->toJson();

        // Obtener departamento, ciudad, juzgado, despacho y clase de proceso
        $departamento = Departamento::find($dto['departamento_id']);
        $ciudad = Ciudad::find($dto['ciudad_id']);
        $juzgado = Juzgado::find($dto['juzgado_id']);
        $despacho = Despacho::find($dto['despacho_id']);

        // Desglosar redicado actual
        $departamentoOrigenCodigo = substr($dto['numero_radicado_proceso_actual'],0,2);
        $ciudadOrigenCodigo = substr($dto['numero_radicado_proceso_actual'],0,5);
        $juzgadoOrigenRama = substr($dto['numero_radicado_proceso_actual'],0,9);
        $despachoOrigenNumero = substr($dto['numero_radicado_proceso_actual'],9,3);
        $numeroConsecutivo = substr($dto['numero_radicado_proceso_actual'],21,2);
        $numeroJuzgado = substr($dto['numero_radicado_proceso_actual'],5,2);
        $numeroSala = substr($dto['numero_radicado_proceso_actual'],7,2);

        // Ciudad y departamento de origen
        $departamentoOrigen = Departamento::findBy($departamentoOrigenCodigo);
        $ciudadOrigen = Ciudad::findBy($ciudadOrigenCodigo);

        // Consultar juzgado y despacho de origen
        $juzgadoOrigen = Juzgado::where('juzgado_rama', $juzgadoOrigenRama)->first();
        $despachoOrigen = Despacho::where([
            'departamento_id' => $departamentoOrigen->id,
            'ciudad_id' => $ciudadOrigen->id,
            'juzgado' => $numeroJuzgado,
            'sala' => $numeroSala,
            'despacho' => $despachoOrigenNumero
        ])->first();

        // Cargar la clase proceso
        if(isset($dto['ultima_clase_proceso_id'])){
            $claseProceso = ClaseProceso::find($dto['ultima_clase_proceso_id']);
            $dto['descripcion_clase_proceso'] = $claseProceso->nombre ?? null;
        }

        // Cargar area por defecto
        if(!isset($dto['area_id']) && !isset($dto['con_cambio_instancia'])){
            $area = Area::where('nombre', $parametros['AREA_EMPRESA_POR_DEFECTO'] ?? null)
                ->where('empresa_id', $empresa->id)->first();
            $dto['area_id'] = $area->id ?? null;
        }

        // Cargar responsable del proceso
        if(!isset($dto['abogado_responsable_id']) && !isset($dto['con_cambio_instancia'])){
            $administrador = Usuario::query()
                ->join('model_has_roles', function ($join) use($parametros) {
                    $join->on('model_has_roles.model_id', '=', 'usuarios.user_id')
                        ->where('model_has_roles.role_id', $parametros['ADMINISTRADOR_EMPRESA_ROL_ID'] ?? null);
                })
                ->select('usuarios.*')
                ->where('usuarios.empresa_id','=', $empresa->id)->first();
            $dto['abogado_responsable_id'] = $administrador->id ?? null;
        }

        // Validaciones modo edición
        if(isset($dto['id']) && isset($procesoJudicial)){
            // Numero consecutivo actual
            $numeroConsecutivoActual = substr($procesoJudicial->numero_radicado_proceso_actual,21,2);

            if(isset($dto['con_cambio_instancia']) && $procesoJudicial->juzgado_id == $dto['juzgado_id']){
                throw new ModelException(
                    "Por cambio de instancia se debe modificar el juzgado actual.", $procesoJudicial
                );
            }

            // Cambio de instancia superior
            if(
                isset($dto['con_cambio_instancia']) && $dto['con_cambio_instancia'] == CambioInstanciaEnum::SUPERIOR &&
                intval($numeroConsecutivoActual) > intval($numeroConsecutivo)
            ){
                throw new ModelException(
                    "Consecutivo de proceso debe ser igual o mayor al actual.", $procesoJudicial
                );
            }

            // Cambio de instancia anterior
            if(
                isset($dto['con_cambio_instancia']) && $dto['con_cambio_instancia'] == CambioInstanciaEnum::ANTERIOR &&
                intval($numeroConsecutivoActual) < intval($numeroConsecutivo)
            ){
                throw new ModelException(
                    "Consecutivo de proceso debe ser igual o menor al actual.", $procesoJudicial
                );
            }
        }

        // Número de proceso
        $numeroProceso = $ciudad->codigo_dane . $juzgado->juzgado . $juzgado->sala . $despacho->despacho .
            $dto['anio_proceso'] . $dto['numero_radicado'] . '00';

        // Datos del proceso
        $dto['departamento_dane'] = $departamento->codigo_dane;
        $dto['ciudad_dane'] = $ciudad->codigo_dane;
        $dto['juzgado_actual'] = $juzgado->juzgado_rama;
        $dto['juzgado_origen_rama'] = $juzgadoOrigenRama;
        $dto['despacho_numero'] = $despacho->despacho;
        $dto['despacho_origen_numero'] = $despachoOrigenNumero;
        $dto['numero_consecutivo'] = $numeroConsecutivo;
        $dto['numero_proceso'] = $numeroProceso;
        $dto['indicativo_pertenece_rama'] = $ciudad->rama_judicial;
        $dto['indicativo_consulta_rama'] = 0;
        $dto['indicativo_tiene_vigilancia'] = 0;
        $dto['indicativo_estado'] = 1;
        $dto['empresa_id'] = $empresa->id;
        $dto['zona_id'] = $empresa->zona_id;
        $dto['juzgado_origen_id'] = $juzgadoOrigen->id;
        $dto['despacho_origen_id'] = $despachoOrigen->id;

        // Marcar actuaciones como a actualizar
        $actualizarActuaciones = false;
        if(
            isset($dto['id']) && isset($procesoJudicial) &&
            $procesoJudicial->ciudad_id != $dto['ciudad_id'] &&
            $procesoJudicial->juzgado_id != $dto['juzgado_id'] &&
            $procesoJudicial->juzgado_origen_id != $juzgadoOrigen->id &&
            $procesoJudicial->despacho_id != $dto['despacho_id'] &&
            $procesoJudicial->despacho_origen_id != $despachoOrigen->id &&
            $procesoJudicial->numero_proceso != $numeroProceso &&
            $procesoJudicial->numero_radicado_proceso_actual != $dto['numero_radicado_proceso_actual']
        ){
            $actualizarActuaciones = true;
        }

        // Lógica para modificaciones
        if(!isset($dto['con_cambio_instancia'])){
            // Crear o modificar proceso
            $procesoJudicial->fill($dto);
            $guardado = $procesoJudicial->save();
            if(!$guardado){
                throw new ModelException("Ocurrió un error al intentar guardar el proceso judicial.", $procesoJudicial);
            }

            // Datos para control de duplicados
            $procesoJudicialReferencia =
                ProcesoJudicial::whereRaw('SUBSTRING(numero_radicado_proceso_actual, 1, 21) = ?', [
                    substr($dto['numero_radicado_proceso_actual'],0,21)
                ])
                ->where('instancia_judicial_actual', 'S')
                ->where('empresa_id', $empresa->id)->first();
            if(isset($procesoJudicialReferencia)){
                // Auditoria
                $procesoJudicialOriginalReferencia = $procesoJudicialReferencia->toJson();

                // Modificar proceso referencia
                $procesoJudicial->instancia_judicial_actual = 'S';
                $procesoJudicial->proceso_id_referencia = $procesoJudicialReferencia->proceso_id_referencia;
                $procesoJudicialReferencia->instancia_judicial_actual = null;
                $guardado = $procesoJudicialReferencia->save();
                if(!$guardado){
                    throw new ModelException(
                        "Ocurrió un error al intentar guardar el proceso judicial.", $procesoJudicialReferencia
                    );
                }

                // Guardar auditoria
                $dtoProcesoReferencia = [
                    'id_recurso' => $procesoJudicialReferencia->id,
                    'nombre_recurso' => ProcesoJudicial::class,
                    'descripcion_recurso' => $procesoJudicialReferencia->numero_radicado_proceso_actual,
                    'accion' => AccionAuditoriaEnum::MODIFICAR,
                    'recurso_original' => $procesoJudicialOriginalReferencia,
                    'recurso_resultante' => $procesoJudicialReferencia->toJson()
                ];
                $this->auditoriaMaestroService->crear($dtoProcesoReferencia);
            }else{
                $procesoJudicial->instancia_judicial_actual = 'S';
                $procesoJudicial->proceso_id_referencia = $procesoJudicial->id;
            }
            // Guardar referencias para control de duplicados
            $guardado = $procesoJudicial->save();
            if(!$guardado){
                throw new ModelException(
                    "Ocurrió un error al intentar guardar el proceso judicial.", $procesoJudicial
                );
            }
        }else if(isset($dto['con_cambio_instancia']) && isset($dto['id']) && isset($procesoJudicial)){
            $procesoJudicialNuevaInstancia =
                ProcesoJudicial::where('numero_radicado_proceso_actual', $dto['numero_radicado_proceso_actual'])
                    ->where('empresa_id', $empresa->id)->first();
            if(isset($procesoJudicialNuevaInstancia)){
                // Auditoria
                $procesoJudicialOriginalNuevaInstancia = $procesoJudicialNuevaInstancia->toJson();

                // Actualizar la instancia actual para el proceso
                $procesoJudicialNuevaInstancia->fill($dto);
                $procesoJudicialNuevaInstancia->instancia_judicial_actual = 'S';
                $guardado = $procesoJudicialNuevaInstancia->save();
                if(!$guardado){
                    throw new ModelException(
                        "Ocurrió un error al intentar cambiar la instancia del proceso judicial.",
                        $procesoJudicialNuevaInstancia
                    );
                }

                // Guardar auditoria
                $dtoNuevaInstancia = [
                    'id_recurso' => $procesoJudicialNuevaInstancia->id,
                    'nombre_recurso' => ProcesoJudicial::class,
                    'descripcion_recurso' => $procesoJudicialNuevaInstancia->numero_radicado_proceso_actual,
                    'accion' => AccionAuditoriaEnum::MODIFICAR,
                    'recurso_original' => $procesoJudicialOriginalNuevaInstancia,
                    'recurso_resultante' => $procesoJudicialNuevaInstancia->toJson()
                ];
                $this->auditoriaMaestroService->crear($dtoNuevaInstancia);
            }else{
                // Guardar el proceso con cambio de instancia
                $dtoNuevaInstancia = [
                    'departamento_dane' => $departamento->codigo_dane,
                    'ciudad_dane' => $ciudad->codigo_dane,
                    'juzgado_actual' => $juzgado->juzgado_rama,
                    'juzgado_origen_rama' => $juzgadoOrigenRama,
                    'despacho_numero' => $despacho->despacho,
                    'despacho_origen_numero' => $despachoOrigenNumero,
                    'anio_proceso' => $dto['anio_proceso'],
                    'numero_radicado' => $dto['numero_radicado'],
                    'numero_consecutivo' => $numeroConsecutivo,
                    'numero_proceso' => $numeroProceso,
                    'numero_radicado_proceso_actual' => $dto['numero_radicado_proceso_actual'],
                    'proceso_id_referencia' => $procesoJudicial->proceso_id_referencia,
                    'instancia_judicial_actual' => 'S',
                    'nombres_demandantes' => $dto['nombres_demandantes'],
                    'nombres_demandados' => $dto['nombres_demandados'],
                    'observaciones' => $dto['observaciones'],
                    'indicativo_pertenece_rama' => $procesoJudicial->indicativo_pertenece_rama,
                    'indicativo_consulta_rama' => $procesoJudicial->indicativo_consulta_rama,
                    'indicativo_tiene_vigilancia' => $procesoJudicial->indicativo_tiene_vigilancia,
                    'indicativo_estado' => $procesoJudicial->indicativo_estado,
                    'descripcion_clase_proceso' => $dto['descripcion_clase_proceso'] ?? null,
                    'empresa_id' => $empresa->id,
                    'zona_id' => $procesoJudicial->zona_id,
                    'area_id' => $dto['area_id'] ?? null,
                    'cliente_id' => $dto['cliente_id'] ?? null,
                    'abogado_responsable_id' => $dto['abogado_responsable_id'] ?? null,
                    'dependiente_asignado_id' => $procesoJudicial->dependiente_asignado_id,
                    'ultima_clase_proceso_id' => $dto['ultima_clase_proceso_id'] ?? null,
                    'ultima_etapa_proceso_id' => $dto['ultima_etapa_proceso_id'] ?? null,
                    'ultimo_tipo_actuacion_id' => $dto['ultimo_tipo_actuacion_id'] ?? null,
                    'departamento_id' => $dto['departamento_id'],
                    'ciudad_id' => $dto['ciudad_id'],
                    'juzgado_id' => $dto['juzgado_id'],
                    'juzgado_origen_id' => $juzgadoOrigen->id,
                    'despacho_id' => $dto['despacho_id'],
                    'despacho_origen_id' => $despachoOrigen->id,
                    'usuario_creacion_id' => $usuario->id ?? ($dto['usuario_creacion_id'] ?? null),
                    'usuario_creacion_nombre' => $usuario->nombre ?? ($dto['usuario_creacion_nombre'] ?? null),
                    'usuario_modificacion_id' => $usuario->id ?? ($dto['usuario_modificacion_id'] ?? null),
                    'usuario_modificacion_nombre' => $usuario->nombre ?? ($dto['usuario_modificacion_nombre'] ?? null)
                ];
                $procesoJudicialNuevaInstancia = new ProcesoJudicial();
                $procesoJudicialNuevaInstancia->fill($dtoNuevaInstancia);
                $guardado = $procesoJudicialNuevaInstancia->save();
                if(!$guardado){
                    throw new ModelException(
                        "Ocurrió un error al intentar cambiar la instancia del proceso judicial.",
                        $procesoJudicialNuevaInstancia
                    );
                }

                // Guardar auditoria
                $auditoriaDtoNuevaInstancia = [
                    'id_recurso' => $procesoJudicialNuevaInstancia->id,
                    'nombre_recurso' => ProcesoJudicial::class,
                    'descripcion_recurso' => $procesoJudicialNuevaInstancia->numero_radicado_proceso_actual,
                    'accion' => AccionAuditoriaEnum::CREAR,
                    'recurso_original' => $procesoJudicialNuevaInstancia->toJson(),
                    'recurso_resultante' => null
                ];
                $this->auditoriaMaestroService->crear($auditoriaDtoNuevaInstancia);
            }

            // Actualizar la instancia actual para el proceso
            $procesoJudicial->instancia_judicial_actual = null;
            $guardado = $procesoJudicial->save();
            if(!$guardado){
                throw new ModelException(
                    "Ocurrió un error al intentar cambiar la instancia del proceso judicial.", $procesoJudicial
                );
            }
        }

        // Modificar actuaciones
        if($actualizarActuaciones && isset($procesoJudicial->proceso_id_referencia)){
            $actuaciones = Actuacion::whereEmpresaId($empresa->id)
                ->whereProcesoId($procesoJudicial->proceso_id_referencia)->get();
            foreach ($actuaciones as $actuacion){
                $this->actuacionService->modificarOCrear([
                    'id' => $actuacion->id,
                    'ciudad_id' => $procesoJudicial->ciudad_id,
                    'juzgado_id' => $procesoJudicial->juzgado_id,
                    'juzgado_origen_id' => $procesoJudicial->juzgado_origen_id,
                    'despacho_id' => $procesoJudicial->despacho_id,
                    'despacho_origen_id' => $procesoJudicial->despacho_origen_id,
                    'numero_proceso' => $procesoJudicial->numero_proceso,
                    'numero_radicado_proceso_actual' => $procesoJudicial->numero_radicado_proceso_actual
                ]);
            }
        }

        // Guardar auditoria
        $auditoriaDto = [
            'id_recurso' => $procesoJudicial->id,
            'nombre_recurso' => ProcesoJudicial::class,
            'descripcion_recurso' => $procesoJudicial->numero_radicado_proceso_actual,
            'accion' => isset($dto['id']) ? AccionAuditoriaEnum::MODIFICAR : AccionAuditoriaEnum::CREAR,
            'recurso_original' => isset($dto['id']) ? $procesoJudicialOriginal : $procesoJudicial->toJson(),
            'recurso_resultante' => isset($dto['id']) ? $procesoJudicial->toJson() : null
        ];
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $this->cargar($procesoJudicial->id);
    }

    public function validarProcesoRama($dto) {
        // Información de sesión
        $user = Auth::user();
        $empresa = $user->empresa();

        // Parametros
        $parametros = ParametroConstante::cargarParametros();

        // Consultar el procesoJudicial
        $procesoJudicial = isset($dto['id']) ? ProcesoJudicial::find($dto['id']) : null;

        // Obtener departamento, ciudad, juzgado, despacho y clase de proceso
        $ciudad = Ciudad::find($dto['ciudad_id']);
        $juzgado = Juzgado::find($dto['juzgado_id']);
        $despacho = Despacho::find($dto['despacho_id']);
        $numeroConsecutivo = substr($dto['numero_radicado_proceso_actual'],21,2);

        // Construir numero de proceso
        $numeroProceso = $ciudad->codigo_dane . $juzgado->juzgado . $juzgado->sala . $despacho->despacho .
        $dto['anio_proceso'] . $dto['numero_radicado'] . '00';

        // Validaciones modo edición
        if(isset($dto['id']) && isset($procesoJudicial)){
            // Numero consecutivo actual
            $numeroConsecutivoActual = substr($procesoJudicial->numero_radicado_proceso_actual,21,2);

            // Cambio de instancia superior
            if(
                isset($dto['con_cambio_instancia']) && $dto['con_cambio_instancia'] == CambioInstanciaEnum::SUPERIOR &&
                intval($numeroConsecutivoActual) > intval($numeroConsecutivo)
            ){
                throw new ModelException(
                    "Consecutivo de proceso debe ser igual o mayor al actual.", $procesoJudicial
                );
            }

            // Cambio de instancia anterior
            if(
                isset($dto['con_cambio_instancia']) && $dto['con_cambio_instancia'] == CambioInstanciaEnum::ANTERIOR &&
                intval($numeroConsecutivoActual) < intval($numeroConsecutivo)
            ){
                throw new ModelException(
                    "Consecutivo de proceso debe ser igual o menor al actual.", $procesoJudicial
                );
            }
        }

        // Validar que el proceso no exista con el numero construido
        /*$proceso = ProcesoJudicial::where('numero_proceso', $numeroProceso)
            ->where('juzgado_id', $juzgado->id)
            ->where('empresa_id', $empresa->id)->first();
        if(isset($proceso)){
            throw new ModelException("Proceso ya registrado para este cliente.");
        }*/

        // Validar que el proceso no exista con el radicado actual
        if(!isset($dto['id'])){
            $proceso = ProcesoJudicial::where('numero_radicado_proceso_actual', $dto['numero_radicado_proceso_actual'])
                ->where('empresa_id', $empresa->id)->first();
            if(isset($proceso)){
                throw new ModelException("Proceso ya registrado para este cliente.");
            }
        }

        // Obtener la ultima actuación del proceso con el numero construido
        $reporte = [
            'demandante' => null,
            'demandado' => null,
            'mensaje' => "No se encontró informacion en la rama judicial para este proceso. Continuar con la grabacion del proceso."
        ];
        $procesoJudicialGeneral =
            ProcesoJudicial::where('numero_radicado_proceso_actual', $dto['numero_radicado_proceso_actual'])
                ->where('empresa_id', $parametros['EMPRESA_PRESTADORA_SERVICIO_ID'])->first();
        if(isset($procesoJudicialGeneral)) {
            $reporte['demandante'] = $procesoJudicialGeneral->nombres_demandantes;
            $reporte['demandado'] = $procesoJudicialGeneral->nombres_demandados;
        }else if($ciudad->rama_judicial) {
            $infoProceso = $this->conectorService->get([
                'timeout' => $parametros['TIMEOUT_VALIDACION_PROCESO'] ?? 10,
                'proceso_numero' => $dto['numero_radicado_proceso_actual'],
                'solo_activos' => false
            ]);
            if($infoProceso['con_conexion'] && !$infoProceso['es_privado']){
                // Sujetos procesales
                $demandante = "";
                $demandado = "";
                foreach ($infoProceso['sujetos_procesales'] ?? [] as $sujetoProcesales){
                    if($sujetoProcesales['tipo'] == TipoSujetoProcesalEnum::DEMANDANTE){
                        $demandante .= " - " . $sujetoProcesales['nombre'] . ".";
                    }
                    if($sujetoProcesales['tipo'] == TipoSujetoProcesalEnum::DEMANDADO){
                        $demandado .= " - " . $sujetoProcesales['nombre'] . ".";
                    }
                }

                $reporte['demandante'] = $demandante != "" ? $demandante : null;
                $reporte['demandado'] = $demandado != "" ? $demandado : null;
            }
        }

        return $reporte;
    }

    public function importar($archivo){
        // Usuario en sesión
        $user = Auth::user();
        $usuario = $user->usuario();
        $empresa = $user->empresa();

        // Obtener último proceso insertado
        $ultimoProcesoCreado = ProcesoJudicial::query()->orderBy('id', 'desc')->first();

        // Borrar datos historicos
        ProcesoJudicialCargaError::query()->where('empresa_id', $empresa->id)->delete();

        $errores = [];
        $import = new ProcesoJudicialImport($empresa->id, $usuario->id, $usuario->nombre);
        Excel::import($import, $archivo);

        if($import->getWithErrors()){
            throw new ModelException("Revisar archivo de carga. Estructura de información no corresponde.");
        }

        foreach ($import->failures() as $failure) {
            array_push($errores, [
                "fila" => $failure->row(),
                "columna" => $failure->attribute(),
                "errores" => $failure->errors(),
                "datos" => $failure->values()
            ]);
        }

        $erroresReporte = [];
        $procesosJudicialesErrores = [];
        if(count($errores) > 0){
            $coleccionDeErrores = collect($errores)->groupBy('fila');
            foreach($coleccionDeErrores as $fila => $erroresFila){
                // Formatear observaciones
                $errorNumeral = 0;
                $observaciones = [];
                foreach ($erroresFila as $errorFila){
                    foreach ($errorFila['errores'] as $error){
                        if (strpos($error, '||') !== false) {
                            $erroresAux = explode('||', $error);
                            foreach ($erroresAux ?? [] as $errorAux){
                                $errorNumeral++;
                                array_push($observaciones, $errorNumeral . ". ". $errorAux);
                            }
                        }else{
                            $errorNumeral++;
                            array_push($observaciones, $errorNumeral . ". ". $error);
                        }
                    }
                }

                // Datos de la fila con errores
                $datosFila = $erroresFila[0]["datos"];
                array_push($erroresReporte, [
                    'numero_proceso' => $datosFila[0],
                    'nombres_demandantes' => $datosFila[1],
                    'nombres_demandados' => $datosFila[2],
                    'ciudad_nombre' => $datosFila[3],
                    'juzgado_nombre' => $datosFila[4],
                    'juzgado_numero' => $datosFila[5],
                    'clase_proceso_nombre' => $datosFila[6],
                    'responsable_documento' => $datosFila[7],
                    'area_nombre' => $datosFila[8],
                    'observacion' => join("<br>", $observaciones)
                ]);
                array_push($procesosJudicialesErrores, [
                    'numero_proceso' => $datosFila[0],
                    'nombres_demandantes' => $datosFila[1],
                    'nombres_demandados' => $datosFila[2],
                    'ciudad_nombre' => $datosFila[3],
                    'juzgado_nombre' => $datosFila[4],
                    'juzgado_numero' => $datosFila[5],
                    'clase_proceso_nombre' => $datosFila[6],
                    'responsable_documento' => $datosFila[7],
                    'area_nombre' => $datosFila[8],
                    'observacion' => join("\n", $observaciones),
                    'empresa_id' => $empresa->id,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'usuario_creacion_id' => $usuario->id,
                    'usuario_creacion_nombre' => $usuario->nombre,
                    'usuario_modificacion_id' => $usuario->id,
                    'usuario_modificacion_nombre' => $usuario->nombre
                ]);
            }
        }

        // Procesar errores personalizados
        $erroresPersonalizados = $import->getCustomErrors();
        if(count($erroresPersonalizados) > 0){
            $erroresPersonalizadosColeccion = collect($erroresPersonalizados)->groupBy('0');
            foreach ($erroresPersonalizadosColeccion ?? [] as $errores){
                $errorNumeral = 0;
                $observaciones = [];
                foreach ($errores as $error){
                    $errorNumeral++;
                    array_push($observaciones, $errorNumeral . ". ". $error['observacion']);
                }

                $datosFila = $errores[0];
                array_push($erroresReporte, [
                    'numero_proceso' => $datosFila[0],
                    'nombres_demandantes' => $datosFila[1],
                    'nombres_demandados' => $datosFila[2],
                    'ciudad_nombre' => $datosFila[3],
                    'juzgado_nombre' => $datosFila[4],
                    'juzgado_numero' => $datosFila[5],
                    'clase_proceso_nombre' => $datosFila[6],
                    'responsable_documento' => $datosFila[7],
                    'area_nombre' => $datosFila[8],
                    'observacion' => join("<br>", $observaciones)
                ]);
                array_push($procesosJudicialesErrores, [
                    'numero_proceso' => $datosFila[0],
                    'nombres_demandantes' => $datosFila[1],
                    'nombres_demandados' => $datosFila[2],
                    'ciudad_nombre' => $datosFila[3],
                    'juzgado_nombre' => $datosFila[4],
                    'juzgado_numero' => $datosFila[5],
                    'clase_proceso_nombre' => $datosFila[6],
                    'responsable_documento' => $datosFila[7],
                    'area_nombre' => $datosFila[8],
                    'observacion' => join("\n", $observaciones),
                    'empresa_id' => $empresa->id,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'usuario_creacion_id' => $usuario->id,
                    'usuario_creacion_nombre' => $usuario->nombre,
                    'usuario_modificacion_id' => $usuario->id,
                    'usuario_modificacion_nombre' => $usuario->nombre
                ]);
            }
        }

        // Cantidad de registros fallidos, OJO contabilizar antes de agregar los procesos cargados
        $registrosFallidos = count($procesosJudicialesErrores ?? []);

        // Procesar registros importados
        $procesosImportados = $import->getImported();
        if(count($procesosImportados ?? []) > 0){
            $erroresReporte = array_merge($erroresReporte, $procesosImportados);
            $procesosJudicialesErrores = array_merge($procesosJudicialesErrores, $procesosImportados);
        }

        if($procesosJudicialesErrores){
            // Insertar errores encontrados
            ProcesoJudicialCargaError::insert($procesosJudicialesErrores);
        }

        // Logica para datos requeridos para control de duplicado
        if(count($procesosImportados ?? []) > 0){
            DB::table('procesos_judiciales')
            ->where('id', '>', $ultimoProcesoCreado->id ?? 1)
            ->whereNull('proceso_id_referencia')
            ->where('empresa_id', $empresa->id)
            ->update([
                'proceso_id_referencia' => DB::raw('id')
            ]);
        }

        return [
            "errores" => $erroresReporte,
            "registros_fallidos" => $registrosFallidos,
            "registros_cargados" => $import->getImportedRows(),
            "registros_procesados" => $registrosFallidos + $import->getImportedRows()
        ];
    }

    public function importarTexto($texto){
        $archivo = "procesos.xlsx";
        Excel::store(new ProcesoJudicialTextoExport($texto), $archivo);

        return $this->importar($archivo);
    }

    public function eliminar($id)
    {
        // Connsultar el objeto
        $procesoJudicial = ProcesoJudicial::find($id);

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $procesoJudicial->id,
            'nombre_recurso' => ProcesoJudicial::class,
            'descripcion_recurso' => $procesoJudicial->nombre,
            'accion' => AccionAuditoriaEnum::ELIMINAR,
            'recurso_original' => $procesoJudicial->toJson()
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $procesoJudicial->delete();
    }

    public function obtenerColeccion($dto)
    {
        $data = [];
        $procesosJudiciales = ProcesoJudicial::obtenerColeccion($dto);
        foreach ($procesosJudiciales ?? [] as $shipping){
            array_push($data, $shipping);
        }

        $cantidadProcesosJudiciales = count($procesosJudiciales);
        $to = isset($procesosJudiciales) && $cantidadProcesosJudiciales > 0 ? $procesosJudiciales->currentPage() * $procesosJudiciales->perPage() : null;
        $to = isset($to) && isset($procesosJudiciales) && $to > $procesosJudiciales->total() && $cantidadProcesosJudiciales> 0 ? $procesosJudiciales->total() : $to;
        $from = isset($to) && isset($procesosJudiciales) && $cantidadProcesosJudiciales > 0 ?
            $procesosJudiciales->perPage() > $to ? 1 : ($to - $procesosJudiciales->perPage()) + 1
            : null;
        return [
            'datos' => $data,
            'desde' => $from,
            'hasta' => $to,
            'por_pagina' => isset($procesosJudiciales) && $cantidadProcesosJudiciales > 0 ? +$procesosJudiciales->perPage() : 0,
            'pagina_actual' => isset($procesosJudiciales) && $cantidadProcesosJudiciales > 0 ? $procesosJudiciales->currentPage() : 1,
            'ultima_pagina' => isset($procesosJudiciales) && $cantidadProcesosJudiciales > 0 ? $procesosJudiciales->lastPage() : 0,
            'total' => isset($procesosJudiciales) && $cantidadProcesosJudiciales > 0 ? $procesosJudiciales->total() : 0
        ];
    }

    public function obtenerColeccionLigera($dto)
    {
        $procesoJudicial = ProcesoJudicial::obtenerColeccionLigera($dto);
        return $procesoJudicial;
    }

    public function obtenerColeccionReferencia($dto)
    {
        $procesoJudicial = ProcesoJudicial::obtenerColeccionReferencia($dto);
        return $procesoJudicial;
    }

    public function obtenerProcesosPorRol($dto)
    {
        $data = [];
        $procesosJudiciales = ProcesoJudicial::obtenerProcesosPorRol($dto);
        foreach ($procesosJudiciales ?? [] as $shipping){
            array_push($data, $shipping);
        }

        $cantidadProcesosJudiciales = count($procesosJudiciales);
        $to = isset($procesosJudiciales) && $cantidadProcesosJudiciales > 0 ? $procesosJudiciales->currentPage() * $procesosJudiciales->perPage() : null;
        $to = isset($to) && isset($procesosJudiciales) && $to > $procesosJudiciales->total() && $cantidadProcesosJudiciales> 0 ? $procesosJudiciales->total() : $to;
        $from = isset($to) && isset($procesosJudiciales) && $cantidadProcesosJudiciales > 0 ?
            $procesosJudiciales->perPage() > $to ? 1 : ($to - $procesosJudiciales->perPage()) + 1
            : null;
        return [
            'datos' => $data,
            'desde' => $from,
            'hasta' => $to,
            'por_pagina' => isset($procesosJudiciales) && $cantidadProcesosJudiciales > 0 ? +$procesosJudiciales->perPage() : 0,
            'pagina_actual' => isset($procesosJudiciales) && $cantidadProcesosJudiciales > 0 ? $procesosJudiciales->currentPage() : 1,
            'ultima_pagina' => isset($procesosJudiciales) && $cantidadProcesosJudiciales > 0 ? $procesosJudiciales->lastPage() : 0,
            'total' => isset($procesosJudiciales) && $cantidadProcesosJudiciales > 0 ? $procesosJudiciales->total() : 0
        ];
    }

}
