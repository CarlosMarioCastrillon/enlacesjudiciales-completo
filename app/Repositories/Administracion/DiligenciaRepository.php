<?php


namespace App\Repositories\Administracion;


use App\CloudStorageService;
use App\Contracts\Administracion\DiligenciaService;
use App\Contracts\Seguridad\AuditoriaMaestroService;
use App\Enum\AccionAuditoriaEnum;
use App\Enum\EstadoDiligenciaEnum;
use App\Enum\SistemaArchivoEnum;
use App\Exceptions\ModelException;
use App\Exports\ConsultaDiligenciaExport;
use App\Exports\DiligenciaExport;
use App\Mail\AutorizacionDiligenciaEmail;
use App\Model\Administracion\Diligencia;
use App\Model\Administracion\DiligenciaHistoria;
use App\Model\Administracion\ParametroConstante;
use App\Model\Configuracion\TipoDiligencia;
use App\Model\Seguridad\Usuario;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class DiligenciaRepository implements DiligenciaService
{

    protected  $auditoriaMaestroService;
    protected  $cloudStorageService;

    public function __construct(AuditoriaMaestroService $auditoriaMaestroService, CloudStorageService $cloudStorageService){
        $this->auditoriaMaestroService = $auditoriaMaestroService;
        $this->cloudStorageService = $cloudStorageService;
    }

    public function cargar($id)
    {
        $diligencia = Diligencia::with([
            'ciudad',
            'departamento',
            'tipoDiligencia',
            'empresa',
            'procesoJudicial',

            // Historial
            'diligenciaHistoria.ciudad',
            'diligenciaHistoria.departamento',
            'diligenciaHistoria.tipoDiligencia',
            'diligenciaHistoria.empresa',
            'diligenciaHistoria.procesoJudicial',
        ])->where('diligencias.id', '=', $id)->first();

        $ciudad = $diligencia->ciudad;
        $departamento = $diligencia->departamento;
        $tipoDiligencia = $diligencia->tipoDiligencia;
        $empresa = $diligencia->empresa;
        $procesoJudicial = $diligencia->procesoJudicial;
        $dependiente = $diligencia->dependiente;
        $diligenciaHistoria = $diligencia->diligenciaHistoria;

        return [
            'id' => $diligencia->id,
            'fecha_diligencia' => $diligencia->fecha_diligencia,
            'valor_diligencia' => $diligencia->valor_diligencia,
            'gastos_envio' => $diligencia->gastos_envio,
            'otros_gastos' => $diligencia->otros_gastos,
            'costo_diligencia' => $diligencia->costo_diligencia,
            'costo_envio' => $diligencia->costo_envio,
            'otros_costos' => $diligencia->otros_costos,
            'detalle_valores' => $diligencia->detalle_valores,
            'estado_diligencia' => $diligencia->estado_diligencia,
            'indicativo_cobro' => $diligencia->indicativo_cobro,
            'solicitud_autorizacion' => $diligencia->solicitud_autorizacion,
            'archivo_anexo_diligencia' => $diligencia->archivo_anexo_diligencia,
            'archivo_anexo_costos' => $diligencia->archivo_anexo_costos,
            'archivo_anexo_recibido' => $diligencia->archivo_anexo_recibido,
            'observaciones_cliente' => $diligencia->observaciones_cliente,
            'observaciones_internas' => $diligencia->observaciones_internas,
            'usuario_creacion_id' => $diligencia->usuario_creacion_id,
            'usuario_creacion_nombre' => $diligencia->usuario_creacion_nombre,
            'usuario_modificacion_id' => $diligencia->usuario_modificacion_id,
            'usuario_modificacion_nombre' => $diligencia->usuario_modificacion_nombre,
            'fecha_creacion' => (new Carbon())->parse($diligencia->created_at)->format("Y-m-d H:i:s"),
            'fecha_modificacion' => (new Carbon())->parse($diligencia->updated_at)->format("Y-m-d H:i:s"),
            'ciudad' => isset($ciudad) ? [
                'id' => $ciudad->id,
                'nombre' => $ciudad->nombre
            ] : null,
            'departamento' => isset($departamento) ? [
                'id' => $departamento->id,
                'nombre' => $departamento->nombre
            ] : null,
            'tipo_diligencia' => isset($tipoDiligencia) ? [
                'id' => $tipoDiligencia->id,
                'nombre' => $tipoDiligencia->nombre
            ] : null,
            'proceso_judicial' => isset($procesoJudicial) ? [
                'id' => $procesoJudicial->id,
                'numero_proceso' => $procesoJudicial->numero_proceso,
                'clase_proceso' => $procesoJudicial->claseProceso,
                'ciudad' => $procesoJudicial->ciudad_id,
                'departamento' => $procesoJudicial->departamento_id,
                'demandante' => $procesoJudicial->nombres_demandantes,
                'demandado' => $procesoJudicial->nombres_demandados,
                'juzgado' => $procesoJudicial->juzgado->nombre
            ] : null,
            'empresa' => isset($empresa) ? [
                'id' => $empresa->id,
                'nombre' => $empresa->nombre
            ] : null,
            'dependiente' => isset($dependiente) ? [
                'id' => $dependiente->id,
                'nombre' => $dependiente->nombre
            ] : null,
            'diligencia_historia' => $diligenciaHistoria
        ];
    }

    public function cargarHistorico($historiaId){
        $diligenciaHistoria = DiligenciaHistoria::with([
            'ciudad',
            'departamento',
            'tipoDiligencia',
            'empresa',
            'procesoJudicial',
        ])->where('diligencias_historia.id', '=', $historiaId)->first();
        $ciudad = $diligenciaHistoria->ciudad;
        $departamento = $diligenciaHistoria->departamento;
        $tipoDiligencia = $diligenciaHistoria->tipoDiligencia;
        $empresa = $diligenciaHistoria->empresa;
        $procesoJudicial = $diligenciaHistoria->procesoJudicial;

        return [
            'id' => $diligenciaHistoria->id,
            'secuencia' => $diligenciaHistoria->secuencia,
            'fecha_diligencia' => $diligenciaHistoria->fecha_diligencia,
            'valor_diligencia' => $diligenciaHistoria->valor_diligencia,
            'gastos_envio' => $diligenciaHistoria->gastos_envio,
            'otros_gastos' => $diligenciaHistoria->otros_gastos,
            'costo_diligencia' => $diligenciaHistoria->costo_diligencia,
            'costo_envio' => $diligenciaHistoria->costo_envio,
            'otros_costos' => $diligenciaHistoria->otros_costos,
            'solicitud_autorizacion' => $diligenciaHistoria->solicitud_autorizacion,
            'indicativo_cobro' => $diligenciaHistoria->indicativo_cobro,
            'estado_diligencia' => $diligenciaHistoria->estado_diligencia,
            'archivo_anexo_diligencia' => $diligenciaHistoria->archivo_anexo_diligencia,
            'archivo_anexo_costos' => $diligenciaHistoria->archivo_anexo_costos,
            'archivo_anexo_recibido' => $diligenciaHistoria->archivo_anexo_recibido,
            'observaciones_cliente' => $diligenciaHistoria->observaciones_cliente,
            'observaciones_internas' => $diligenciaHistoria->observaciones_internas,
            'usuario_creacion_id' => $diligenciaHistoria->usuario_creacion_id,
            'usuario_creacion_nombre' => $diligenciaHistoria->usuario_creacion_nombre,
            'usuario_modificacion_id' => $diligenciaHistoria->usuario_modificacion_id,
            'usuario_modificacion_nombre' => $diligenciaHistoria->usuario_modificacion_nombre,
            'fecha_creacion' => (new Carbon($diligenciaHistoria->created_at))->format("Y-m-d H:i:s"),
            'fecha_modificacion' => (new Carbon($diligenciaHistoria->updated_at))->format("Y-m-d H:i:s"),
            'ciudad' => isset($ciudad) ? [
                'id' => $ciudad->id,
                'nombre' => $ciudad->nombre,
            ] : null,
            'departamento' => isset($departamento) ? [
                'id' => $departamento->id,
                'nombre' => $departamento->nombre,
            ] : null,
            'tipo_diligencia' => isset($tipoDiligencia) ? [
                'id' => $tipoDiligencia->id,
                'nombre' => $tipoDiligencia->nombre,
            ] : null,
            'proceso_judicial' => isset($procesoJudicial) ? [
                'id' => $procesoJudicial->id,
                'numero_proceso' => $procesoJudicial->numero_proceso,
                'clase_proceso' => $procesoJudicial->claseProceso->nombre,
                'ciudad' => $procesoJudicial->ciudad_id,
                'departamento' => $procesoJudicial->departamento_id,
                'demandante' => $procesoJudicial->nombres_demandantes,
                'demandado' => $procesoJudicial->nombres_demandados,
                'juzgado' => $procesoJudicial->juzgado->nombre,
            ] : null,
            'empresa' => isset($empresa) ? [
                'id' => $empresa->id,
                'nombre' => $empresa->nombre,
            ] : null,
        ];
    }

    public function modificarOCrear($dto)
    {
        // Constantes
        $parametros = ParametroConstante::cargarParametros();

        $user = Auth::user();
        $usuario = $user->usuario();
        $empresa = $user->empresa();
        if (!isset($dto['id'])) {
            $dto['usuario_creacion_id'] = $usuario->id ?? ($dto['usuario_creacion_id'] ?? null);
            $dto['usuario_creacion_nombre'] = $usuario->nombre ?? ($dto['usuario_creacion_nombre'] ?? null);
        }
        if (isset($usuario) || isset($dto['usuario_modificacion_id'])) {
            $dto['usuario_modificacion_id'] = $usuario->id ?? ($dto['usuario_modificacion_id'] ?? null);
            $dto['usuario_modificacion_nombre'] = $usuario->nombre ?? ($dto['usuario_modificacion_nombre'] ?? null);
        }

        // Consultar el diligencia
        $diligencia = isset($dto['id']) ? Diligencia::find($dto['id']) : new Diligencia();

        // Guardar objeto original para auditoria
        $diligenciaOriginal = $diligencia->toJson();
        $diligenciaOriginalObj = $diligencia->toArray();
        $dto['valor_diligencia'] = $dto['valor_diligencia'] ?? 0;
        $diligencia->fill($dto);
        if(!isset($diligencia->estado_diligencia)){
            if (                (isset($diligencia->valor_diligencia) || isset($diligencia->gastos_envio)) &&

                (
                    !isset($diligencia->valor_diligencia) && !isset($diligencia->gastos_envio) &&
                    !isset($diligencia->otros_gastos)
                ) ||
                ($diligencia->valor_diligencia == 0 && $diligencia->gastos_envio == 0 && $diligencia->otros_gastos == 0)
            ){
                $diligencia->estado_diligencia = EstadoDiligenciaEnum::SOLICITADO;
            } else if (
                ($diligencia->valor_diligencia > 0 || $diligencia->gastos_envio > 0)
            ) {
                if ($diligencia->solicitud_autorizacion == 1){
                    $diligencia->estado_diligencia = EstadoDiligenciaEnum::COTIZADO;
                }else {
                    $diligencia->estado_diligencia = EstadoDiligenciaEnum::APROBADO;
                }
            }else {
                $diligencia->estado_diligencia = EstadoDiligenciaEnum::SOLICITADO;
            }
        }

        // Validación para que cuando se ingrese una diligencia en trámite que tenga solicitud de autorización y
        // otro gasto obligue a ingresar un soporte del gasto
        if (
            $dto['solicitud_autorizacion'] == 1 && !isset($dto['nombre_anexo_costos']) &&
            isset($dto['diligencia_tramite']) && isset($dto['otros_gastos'])
        ){
            throw new ModelException("Debe cargarse archivo anexo de costos.", $diligencia);
        }

        $guardado = $diligencia->save();
        if(!$guardado){
            throw new ModelException("Ocurrió un error al intentar guardar la diligencia.", $diligencia);
        }

        // Eliminar anexo diligencia
        if(
            isset($diligencia->id) && isset($diligencia->url_anexo_diligencia) &&
            isset($dto['eliminar_anexo_diligenica']) && $dto['eliminar_anexo_diligenica']
        ){
            $deleted = $this->cloudStorageService->delete($diligencia->url_anexo_diligencia);
            if(!$deleted){
                throw new ModelException("Ocurrió un error al intentar borrar el anexo de la diligencia.", $diligencia);
            }

            $diligencia->url_anexo_diligencia = null;
            $diligencia->archivo_anexo_diligencia = null;
            $guardado = $diligencia->save();
            if(!$guardado){
                throw new ModelException("Ocurrió un error al intentar borrar el anexo de la diligencia.", $diligencia);
            }
        }

        // Eliminar anexo costos
        if(
            isset($diligencia->id) && isset($diligencia->url_anexo_costos) &&
            isset($dto['eliminar_anexo_costos']) && $dto['eliminar_anexo_costos']
        ){
            if (
                $dto['solicitud_autorizacion'] == 1 && isset($dto['diligencia_tramite']) && isset($dto['otros_gastos'])
            ){
                throw new ModelException("Debe cargarse archivo anexo de costos.", $diligencia);
            }
            $deleted = $this->cloudStorageService->delete($diligencia->url_anexo_costos);
            if(!$deleted){
                throw new ModelException("Ocurrió un error al intentar borrar el anexo de los costos de la diligencia.", $diligencia);
            }

            $diligencia->url_anexo_costos = null;
            $diligencia->archivo_anexo_costos = null;
            $guardado = $diligencia->save();
            if(!$guardado){
                throw new ModelException("Ocurrió un error al intentar borrar el anexo de los costos de la diligencia.", $diligencia);
            }
        }

        // Eliminar anexo recibido
        if(
            isset($diligencia->id) && isset($diligencia->url_anexo_recibido) &&
            isset($dto['eliminar_anexo_recibido']) && $dto['eliminar_anexo_recibido']
        ){
            $deleted = $this->cloudStorageService->delete($diligencia->url_anexo_recibido);
            if(!$deleted){
                throw new ModelException("Ocurrió un error al intentar borrar el anexo del recibido de la diligencia.", $diligencia);
            }

            $diligencia->url_anexo_recibido = null;
            $diligencia->archivo_anexo_recibido = null;
            $guardado = $diligencia->save();
            if(!$guardado){
                throw new ModelException("Ocurrió un error al intentar borrar el anexo del recibido de la diligencia.", $diligencia);
            }
        }

        // Cargar anexo diligencia
        if(isset($dto['base_64_anexo_diligencia']) && trim($dto['base_64_anexo_diligencia']) !== ''){
            if(isset($diligencia->id) && isset($diligencia->url_anexo_diligencia)){
                $deleted = $this->cloudStorageService->delete($diligencia->url_anexo_diligencia);
                if(!$deleted){
                    throw new ModelException("Ocurrió un error al intentar borrar el anexo de la diligencia.", $diligencia);
                }
            }

            $fileB64 = preg_split("/[\\s,]+/", $dto['base_64_anexo_diligencia']);
            $file = base64_decode($fileB64[1]);
            $ruta = $empresa->id . "/" . SistemaArchivoEnum::ANEXO_DILIGENCIA . "/" . $diligencia->id . "/" . $dto['nombre_anexo_diligencia'];
            $ruta = $this->cloudStorageService->put($file, $ruta);
            if(isset($ruta)){
                $diligencia->url_anexo_diligencia = $ruta;
                $diligencia->archivo_anexo_diligencia = $dto['nombre_anexo_diligencia'];
                $guardado = $diligencia->save();
                if(!$guardado){
                    throw new ModelException("Ocurrió un error al intentar guardar la diligencia.", $diligencia);
                }
            }
        }

        // Cargar anexo costos
        if(isset($dto['base_64_anexo_costos']) && trim($dto['base_64_anexo_costos']) !== ''){
            if(isset($diligencia->id) && isset($diligencia->url_anexo_costos)){
                $deleted = $this->cloudStorageService->delete($diligencia->url_anexo_costos);
                if(!$deleted){
                    throw new ModelException("Ocurrió un error al intentar borrar el anexo de los costos.", $diligencia);
                }
            }

            $fileCostosB64 = preg_split("/[\\s,]+/", $dto['base_64_anexo_costos']);
            $fileCostos = base64_decode($fileCostosB64[1]);
            $rutaCostos= $empresa->id . "/" . SistemaArchivoEnum::ANEXO_COSTOS_DILIGENCIA . "/" . $diligencia->id . "/" . $dto['nombre_anexo_costos'];
            $rutaCostos = $this->cloudStorageService->put($fileCostos, $rutaCostos);
            if(isset($rutaCostos)){
                $diligencia->url_anexo_costos = $rutaCostos;
                $diligencia->archivo_anexo_costos = $dto['nombre_anexo_costos'];
                $guardado = $diligencia->save();
                if(!$guardado){
                    throw new ModelException("Ocurrió un error al intentar guardar el anexo de costos de la diligencia.", $diligencia);
                }
            }
        }

        // Cargar anexo Recibido
        if(isset($dto['base_64_anexo_recibido']) && trim($dto['base_64_anexo_recibido']) !== ''){
            if(isset($diligencia->id) && isset($diligencia->url_anexo_recibido)){
                $deleted = $this->cloudStorageService->delete($diligencia->url_anexo_recibido);
                if(!$deleted){
                    throw new ModelException("Ocurrió un error al intentar borrar el anexo del recibido.", $diligencia);
                }
            }

            $fileRecibidoB64 = preg_split("/[\\s,]+/", $dto['base_64_anexo_recibido']);
            $fileRecibido = base64_decode($fileRecibidoB64[1]);
            $rutaRecibido= $empresa->id . "/" . SistemaArchivoEnum::ANEXO_RECIBIDO_DILIGENCIA . "/" . $diligencia->id . "/" . $dto['nombre_anexo_recibido'];
            $rutaRecibido = $this->cloudStorageService->put($fileRecibido, $rutaRecibido);
            if(isset($rutaRecibido)){
                $diligencia->url_anexo_recibido = $rutaRecibido;
                $diligencia->archivo_anexo_recibido = $dto['nombre_anexo_recibido'];
                $guardado = $diligencia->save();
                if(!$guardado){
                    throw new ModelException("Ocurrió un error al intentar guardar el anexo de recibido de la diligencia.", $diligencia);
                }
            }
        }

        // Historico
        if(isset($dto['id'])){
            $diligenciaOriginalObj['id'] = null;
            $diligenciaOriginalObj['created_at'] = null;
            $diligenciaOriginalObj['updated_at'] = null;
            $diligenciaOriginalObj['diligencia_id'] = $diligencia->id;
            $diligenciaOriginalObj['secuencia'] = DiligenciaHistoria::obtenerSecuencia($diligencia->id);
            $diligenciaOriginalObj['usuario_modificacion_nombre'] =
                $usuario->nombre ?? ($dto['usuario_modificacion_nombre'] ?? null);

            // Crear el historial de la diligencia
            $diligenciaHistoria = new DiligenciaHistoria();
            $diligenciaHistoria->fill($diligenciaOriginalObj);
            $saved = $diligenciaHistoria->save();
            if(!$saved){
                throw new ModelException(
                    "Ocurrió un error al intentar guardar el historico de la diligencia.", $diligenciaHistoria
                );
            }

            // Cargar anexo diligencia
            if(isset($diligenciaOriginalObj['url_anexo_diligencia'])){
                $ruta = $empresa->id . "/" . SistemaArchivoEnum::ANEXO_DILIGENCIA_HISTORIA . "/" .
                    $diligenciaHistoria->id . "/" . $diligenciaOriginalObj['archivo_anexo_diligencia'];
                $ruta = $this->cloudStorageService->copy($diligenciaOriginalObj['url_anexo_diligencia'], $ruta);
                if(isset($ruta)){
                    $diligenciaHistoria->url_anexo_diligencia = $ruta;
                    $diligenciaHistoria->archivo_anexo_diligencia = $diligenciaOriginalObj['archivo_anexo_diligencia'];
                    $guardado = $diligenciaHistoria->save();
                    if(!$guardado){
                        throw new ModelException(
                            "Ocurrió un error al intentar guardar el historico de la diligencia.",
                            $diligenciaHistoria
                        );
                    }
                }
            }

            // Cargar anexo costos
            if(isset($diligenciaOriginalObj['url_anexo_costos'])){
                $ruta = $empresa->id . "/" . SistemaArchivoEnum::ANEXO_COSTOS_DILIGENCIA_HISTORIA . "/" .
                    $diligenciaHistoria->id . "/" . $diligenciaOriginalObj['archivo_anexo_costos'];
                $ruta = $this->cloudStorageService->copy($diligenciaOriginalObj['url_anexo_costos'], $ruta);
                if(isset($ruta)){
                    $diligenciaHistoria->url_anexo_costos = $ruta;
                    $diligenciaHistoria->archivo_anexo_costos = $diligenciaOriginalObj['archivo_anexo_costos'];
                    $guardado = $diligenciaHistoria->save();
                    if(!$guardado){
                        throw new ModelException(
                            "Ocurrió un error al intentar guardar el historico de la diligencia.",
                            $diligenciaHistoria
                        );
                    }
                }
            }

            // Cargar anexo recibido
            if(isset($diligenciaOriginalObj['url_anexo_recibido'])){
                $ruta = $empresa->id . "/" . SistemaArchivoEnum::ANEXO_RECIBIDO_DILIGENCIA_HISTORIA . "/" .
                    $diligenciaHistoria->id . "/" . $diligenciaOriginalObj['archivo_anexo_recibido'];
                $ruta = $this->cloudStorageService->copy($diligenciaOriginalObj['url_anexo_recibido'], $ruta);
                if(isset($ruta)){
                    $diligenciaHistoria->url_anexo_recibido = $ruta;
                    $diligenciaHistoria->archivo_anexo_recibido = $diligenciaOriginalObj['archivo_anexo_recibido'];
                    $guardado = $diligenciaHistoria->save();
                    if(!$guardado){
                        throw new ModelException(
                            "Ocurrió un error al intentar guardar el historico de la diligencia.",
                            $diligenciaHistoria
                        );
                    }
                }
            }
        }

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $diligencia->id,
            'nombre_recurso' => Diligencia::class,
            'descripcion_recurso' => $diligencia->id,
            'accion' => isset($dto['id']) ? AccionAuditoriaEnum::MODIFICAR : AccionAuditoriaEnum::CREAR,
            'recurso_original' => isset($dto['id']) ? $diligenciaOriginal : $diligencia->toJson(),
            'recurso_resultante' => isset($dto['id']) ? $diligencia->toJson() : null
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        // Cargar la diligencia actualizada
        $diligenciaReport = $this->cargar($diligencia->id);

        // Enviar notificaciones
        if(
            (
                !isset($diligenciaOriginalObj['estado_diligencia']) ||
                $diligenciaOriginalObj['estado_diligencia'] != EstadoDiligenciaEnum::COTIZADO
            ) &&
            $diligencia->estado_diligencia == EstadoDiligenciaEnum::COTIZADO
        ){
            $administradoresEmpresa = Usuario::query()
                ->join('users', 'users.id', '=', 'usuarios.user_id')
                ->join('model_has_roles', function ($join) use($parametros) {
                    $join->on('model_has_roles.model_id', '=', 'users.id')
                        ->where('model_has_roles.model_type', User::class)
                        ->where('model_has_roles.role_id', $parametros['ADMINISTRADOR_EMPRESA_ROL_ID'] ?? null);
                })
                ->where('usuarios.empresa_id', $diligencia->empresa_id)->get();

            $emailsUsuarios = [];
            foreach($administradoresEmpresa ?? [] as $administradorEmpresa){
                array_push($emailsUsuarios, $administradorEmpresa->email_uno);
            }

            $emailsCC = [];
            $emailTo = $emailsUsuarios[0] ?? null;
            foreach ($emailsUsuarios ?? [] as $key => $email){
                if($key == 0){
                    continue;
                }

                // Validar que los emails estén bien formados
                if(filter_var($email, FILTER_VALIDATE_EMAIL)){
                    array_push($emailsCC, $email);
                }
            }

            if(isset($emailTo)){
                // Email de envío principal
                $mail = Mail::to($emailTo);

                // Copias
                foreach ($emailsCC as $emailCC){
                    $mail->cc($emailCC);
                }

                // Enviar el email
                $mail->send(new AutorizacionDiligenciaEmail($empresa, $diligenciaReport));
            }
        }

        return $diligenciaReport;
    }

    public function eliminar($id)
    {
        // Connsultar el objeto
        $diligencia = Diligencia::find($id);

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $diligencia->id,
            'nombre_recurso' => Diligencia::class,
            'descripcion_recurso' =>  $diligencia->id,
            'accion' => AccionAuditoriaEnum::ELIMINAR,
            'recurso_original' => $diligencia->toJson()
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $diligencia->delete();
    }

    public function obtenerColeccion($dto)
    {
        $data = [];
        $diligencias = Diligencia::obtenerColeccion($dto);
        foreach ($diligencias ?? [] as $shipping){
            array_push($data, $shipping);
        }

        $cantidadDiligencias = count($diligencias);
        $to = isset($diligencias) && $cantidadDiligencias > 0 ? $diligencias->currentPage() * $diligencias->perPage() : null;
        $to = isset($to) && isset($diligencias) && $to > $diligencias->total() && $cantidadDiligencias> 0 ? $diligencias->total() : $to;
        $from = isset($to) && isset($diligencias) && $cantidadDiligencias > 0 ?
            $diligencias->perPage() > $to ? 1 : ($to - $diligencias->perPage()) + 1
            : null;
        return [
            'datos' => $data,
            'desde' => $from,
            'hasta' => $to,
            'por_pagina' => isset($diligencias) && $cantidadDiligencias > 0 ? +$diligencias->perPage() : 0,
            'pagina_actual' => isset($diligencias) && $cantidadDiligencias > 0 ? $diligencias->currentPage() : 1,
            'ultima_pagina' => isset($diligencias) && $cantidadDiligencias > 0 ? $diligencias->lastPage() : 0,
            'total' => isset($diligencias) && $cantidadDiligencias > 0 ? $diligencias->total() : 0
        ];
    }

    public function obtenerDiligenciaPorCliente($dto)
    {
        $data = [];
        $diligencias = Diligencia::obtenerDiligenciaPorCliente($dto);
        foreach ($diligencias ?? [] as $shipping){
            array_push($data, $shipping);
        }

        $cantidadDiligencias = count($diligencias);
        $to = isset($diligencias) && $cantidadDiligencias > 0 ? $diligencias->currentPage() * $diligencias->perPage() : null;
        $to = isset($to) && isset($diligencias) && $to > $diligencias->total() && $cantidadDiligencias> 0 ? $diligencias->total() : $to;
        $from = isset($to) && isset($diligencias) && $cantidadDiligencias > 0 ?
            $diligencias->perPage() > $to ? 1 : ($to - $diligencias->perPage()) + 1
            : null;
        return [
            'datos' => $data,
            'desde' => $from,
            'hasta' => $to,
            'por_pagina' => isset($diligencias) && $cantidadDiligencias > 0 ? +$diligencias->perPage() : 0,
            'pagina_actual' => isset($diligencias) && $cantidadDiligencias > 0 ? $diligencias->currentPage() : 1,
            'ultima_pagina' => isset($diligencias) && $cantidadDiligencias > 0 ? $diligencias->lastPage() : 0,
            'total' => isset($diligencias) && $cantidadDiligencias > 0 ? $diligencias->total() : 0
        ];
    }


    public function getExcel($dto){
        return (new DiligenciaExport($dto));
    }

    public function obtenerConsultaExcel($dto){
        return (new ConsultaDiligenciaExport($dto));
    }
}
