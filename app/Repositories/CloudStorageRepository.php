<?php

namespace App;

use App\CloudStorageService;
use Illuminate\Support\Facades\Storage;
use Exception;

class CloudStorageRepository implements CloudStorageService
{

    public function get($pathName){
        try{
            return Storage::disk('s3')->get($pathName);
        }catch(Exception $e){
            return null;
        }
    }

    public function put($file, $pathName, $makePublic = false){
        try{
            if($makePublic){
                Storage::disk('s3')->put($pathName, $file, 'public');
                return Storage::url($pathName);
            }else{
                Storage::disk('s3')->put($pathName, $file);
                return $pathName;
            }
        }catch(Exception $e){
            return null;
        }
    }

    public function copy($pathName, $newPathName, $makePublic = false){
        try{
            Storage::copy($pathName, $newPathName);
            if($makePublic){
                return Storage::url($pathName);
            }else{
                return $newPathName;
            }
        }catch(Exception $e){
            return false;
        }
    }

    public function delete($pathName){
        try{
            return Storage::disk('s3')->delete($pathName);
        }catch(Exception $e){
            return false;
        }
    }

}
