<?php

namespace App\Repositories\Seguridad;

use App\Contracts\Seguridad\AuditoriaMaestroService;
use App\Contracts\Seguridad\UsuarioService;
use App\Enum\AccionAuditoriaEnum;
use App\Exceptions\ModelException;
use App\Model\Administracion\ParametroConstante;
use App\Model\Seguridad\Usuario;
use App\Rol;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UsuarioRepository implements UsuarioService
{

    protected  $auditoriaMaestroService;

    public function __construct(AuditoriaMaestroService $auditoriaMaestroService){
        $this->auditoriaMaestroService = $auditoriaMaestroService;
    }

    public function cargar($id)
    {
        $usuario = Usuario::find($id);
        $rol = $usuario->rol();
        return [
            'id' => $usuario->id,
            'documento' => $usuario->documento,
            'nombre' => $usuario->nombre,
            'email_uno' => $usuario->email_uno,
            'email_dos' => $usuario->email_dos,
            'fecha_vencimiento' => $usuario->fecha_vencimiento,
            'dependiente_principal' => $usuario->dependiente_principal,
            'con_microportal' => $usuario->con_microportal,
            'con_movimiento_proceso' => $usuario->con_movimiento_proceso,
            'con_montaje_actuacion' => $usuario->con_montaje_actuacion,
            'con_movimiento_proceso_dos' => $usuario->con_movimiento_proceso_dos,
            'con_montaje_actuacion_dos' => $usuario->con_montaje_actuacion_dos,
            'con_tarea' => $usuario->con_tarea,
            'con_respuesta_tarea' => $usuario->con_respuesta_tarea,
            'con_ven_termino_tarea' => $usuario->con_vencimiento_termino_tarea,
            'con_ven_termino_actuacion_rama' => $usuario->con_vencimiento_termino_actuacion_rama,
            'con_ven_termino_actuacion_archivo' => $usuario->con_vencimiento_termino_actuacion_archivo,
            'estado' => $usuario->estado,
            'tipo_documento' => isset($usuario->tipoDocumento) ? $usuario->tipoDocumento->toArray() : null,
            'empresa' => isset($usuario->empresa) ? $usuario->empresa->toArray() : null,
            'departamento' => isset($usuario->departamento) ? $usuario->departamento->toArray() : null,
            'ciudad' => isset($usuario->ciudad) ? $usuario->ciudad->toArray() : null,
            'area' => isset($usuario->area) ? $usuario->area->toArray() : null,
            'rol' => isset($rol) ? [
                'id' => $rol->id,
                'nombre' => $rol->name
            ] : null,
            'usuario_creacion_id' => $usuario->usuario_creacion_id,
            'usuario_creacion_nombre' => $usuario->usuario_creacion_nombre,
            'usuario_modificacion_id' => $usuario->usuario_modificacion_id,
            'usuario_modificacion_nombre' => $usuario->usuario_modificacion_nombre,
            'fecha_creacion' => (new Carbon($usuario->created_at))->format("Y-m-d H:i:s"),
            'fecha_modificacion' => (new Carbon($usuario->updated_at))->format("Y-m-d H:i:s")
        ];
    }

    public function modificarOCrear($dto)
    {
        // Constantes
        $parametros = ParametroConstante::cargarParametros();

        $user = Auth::user();
        $usuario = $user->usuario();
        $rol = $user->rol();
        $empresa = $user->empresa();

        if (!isset($dto['id'])) {
            $dto['usuario_creacion_id'] = $usuario->id ?? ($dto['usuario_creacion_id'] ?? null);
            $dto['usuario_creacion_nombre'] = $usuario->nombre ?? ($dto['usuario_creacion_nombre'] ?? null);
        }
        if (isset($usuario) || isset($dto['usuario_modificacion_id'])) {
            $dto['usuario_modificacion_id'] = $usuario->id ?? ($dto['usuario_modificacion_id'] ?? null);
            $dto['usuario_modificacion_nombre'] = $usuario->nombre ?? ($dto['usuario_modificacion_nombre'] ?? null);
        }

        // Si no es super usuario se asigna a su propia empresa
        if($rol->id != ($parametros['SUPERUSUARIO_ROL_ID'] ?? null)){
            $dto['empresa_id'] = $empresa->id;
        }

        // Fecha vencimiento
        if(!isset($dto['fecha_vencimiento'])){
            $dto['fecha_vencimiento'] = Carbon::now()->addDays($parametros['DIAS_VENCIMIENTO_USUARIO'] ?? 15);
        }

        // Consultar usuario
        $usuario = isset($dto['id']) ? Usuario::find($dto['id']) : new Usuario();

        // Crear usuario de sesión
        if(!isset($dto['id'])){
            $userSesion = User::create([
                'name' => $dto['nombre'],
                'email' => $dto['documento'],
                'password' => Hash::make($dto['clave'])
            ]);
            if(isset($userSesion)){
                // Asignar el rol
                $dto['user_id'] = $userSesion->id;
                $rol = Rol::find($dto['rol_id']);
                $userSesion->assignRole($rol->name);
            }else{
                throw new ModelException("Ocurrió un error al intentar guardar el usuario.", $userSesion);
            }
        }else{
            $userSesion = $usuario->user;
            $userSesion->fill([
                'name' => $dto['nombre'],
                'email' => $dto['documento']
            ]);
            $guardado = $userSesion->save();
            if(!$guardado){
                throw new ModelException("Ocurrió un error al intentar guardar el usuario.", $userSesion);
            }

            // Eliminar rol actual
            $roles = $userSesion->getRoleNames();
            foreach ($roles AS $rolActual){
                $userSesion->removeRole($rolActual);
            }

            // Asignar nuevo rol
            $rol = Rol::find($dto['rol_id']);
            $userSesion->assignRole($rol->name);
        }

        // Guardar objeto original para auditoria
        $usuarioOriginal = $usuario->toJson();

        $usuario->fill($dto);
        $guardado = $usuario->save();
        if(!$guardado){
            throw new ModelException("Ocurrió un error al intentar guardar el usuario.", $usuario);
        }

        // Guardar auditoria
        $auditoriaDto = [
            'id_recurso' => $usuario->id,
            'nombre_recurso' => Usuario::class,
            'descripcion_recurso' => $usuario->nombre,
            'accion' => isset($dto['id']) ? AccionAuditoriaEnum::MODIFICAR : AccionAuditoriaEnum::CREAR,
            'recurso_original' => isset($dto['id']) ? $usuarioOriginal : $usuario->toJson(),
            'recurso_resultante' => isset($dto['id']) ? $usuario->toJson() : null
        ];
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $this->cargar($usuario->id);
    }

    public function cambiarClave($id, $dto){
        $usuario = Usuario::find($id);
        $user = $usuario->user;
        $user->fill([
            'id' => $id,
            'password' => Hash::make($dto['nueva_clave'])
        ]);
        return $user->save();
    }

    public function eliminar($id)
    {
        $usuario = Usuario::find($id);

        // Guardar auditoria
        $auditoriaDto = [
            'id_recurso' => $usuario->id,
            'nombre_recurso' => Usuario::class,
            'descripcion_recurso' => $usuario->nombre,
            'accion' => AccionAuditoriaEnum::ELIMINAR,
            'recurso_original' => $usuario->toJson()
        ];
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $usuario->delete();
    }

    public function obtenerColeccion($dto)
    {
        $datos = [];
        $usuarios = Usuario::obtenerColeccion($dto);
        foreach ($usuarios ?? [] as $rol){
            array_push($datos, $rol);
        }

        $cantidadUsuarios = count($usuarios ?? []);
        $to = isset($usuarios) && $cantidadUsuarios > 0 ? $usuarios->currentPage() * $usuarios->perPage() : null;
        $to = isset($to) && isset($usuarios) && $to > $usuarios->total() && $cantidadUsuarios > 0 ? $usuarios->total() : $to;
        $from = isset($to) && isset($usuarios) && $cantidadUsuarios > 0 ?
            $usuarios->perPage() > $to ? 1 : ($to - $usuarios->perPage()) + 1
            : null;
        return [
            'datos' => $datos,
            'desde' => $from,
            'hasta' => $to,
            'por_pagina' => isset($usuarios) && $cantidadUsuarios > 0 ? +$usuarios->perPage() : 0,
            'pagina_actual' => isset($usuarios) && $cantidadUsuarios > 0 ? $usuarios->currentPage() : 1,
            'ultima_pagina' => isset($usuarios) && $cantidadUsuarios > 0 ? $usuarios->lastPage() : 0,
            'total' => isset($usuarios) && $cantidadUsuarios > 0 ? $usuarios->total() : 0
        ];
    }

    public function obtenerColeccionLigera($dto)
    {
        // Información de sesion
        $user = Auth::user();
        $empresa = $user->empresa();
        $dto['empresa_id'] = $empresa->id;
        $usuarios = Usuario::obtenerColeccionLigera($dto);
        return $usuarios;
    }

    public function obtener($dto)
    {
        return Usuario::obtener($dto);
    }

    public function obtenerDependientePrincipal($dto)
    {
        $usuario = Usuario::obtenerDependientePrincipal($dto);
        return $usuario;
    }
}
