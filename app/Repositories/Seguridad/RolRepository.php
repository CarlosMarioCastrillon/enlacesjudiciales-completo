<?php

namespace App\Repositories\Seguridad;

use App\Contracts\Seguridad\AuditoriaMaestroService;
use App\Contracts\Seguridad\RolService;
use App\Enum\AccionAuditoriaEnum;
use App\Exceptions\ModelException;
use App\Rol;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use Exception;

class RolRepository implements RolService
{

    protected  $auditoriaMaestroService;

    public function __construct(AuditoriaMaestroService $auditoriaMaestroService){
        $this->auditoriaMaestroService = $auditoriaMaestroService;
    }

    public function cargar($id)
    {
        $rol = Rol::find($id);
        return [
            'id' => $rol->id,
            'nombre' => $rol->name,
            'tipo' => $rol->type,
            'estado' => $rol->status,
            'usuario_creacion_id' => $rol->creation_user_id,
            'usuario_creacion_nombre' => $rol->creation_user_name,
            'usuario_modificacion_id' => $rol->modification_user_id,
            'usuario_modificacion_nombre' => $rol->modification_user_name,
            'fecha_creacion' => (new Carbon($rol->created_at))->format("Y-m-d H:i:s"),
            'fecha_modificacion' => (new Carbon($rol->updated_at))->format("Y-m-d H:i:s")
        ];
    }

    public function modificarOCrear($dto)
    {
        $rolDto = [
            'id' => $dto['id'],
            'name' => $dto['nombre'],
            'type' =>  $dto['tipo'],
            'status' => $dto['estado'],
            'guard_name' => 'api'
        ];

        $user = Auth::user();
        $usuario = $user->usuario();
        if(!isset($dto['id'])){
            $rolDto['creation_user_id'] = $usuario->id ?? ($dto['usuario_creacion_id'] ?? null);
            $rolDto['creation_user_name'] = $usuario->nombre ?? ($dto['usuario_creacion_nombre'] ?? null);
        }
        if(isset($usuario) || isset($dto['usuario_modificacion_id'])){
            $rolDto['modification_user_id'] = $usuario->id ?? ($dto['usuario_modificacion_id'] ?? null);
            $rolDto['modification_user_name'] = $usuario->nombre ?? ($dto['usuario_modificacion_nombre'] ?? null);
        }

        // Consultar rol
        $rol = isset($rolDto['id']) ? Rol::find($rolDto['id']) : new Rol();

        // Guardar objeto original para auditoria
        $rolOriginal = $rol->toJson();

        $rol->fill($rolDto);
        $guardado = $rol->save();
        if(!$guardado){
            throw new ModelException("Ocurrió un error al intentar guardar el rol.", $rol);
        }

        // Guardar auditoria
        $auditoriaDto = [
            'id_recurso' => $rol->id,
            'nombre_recurso' => Rol::class,
            'descripcion_recurso' => $rol->name,
            'accion' => isset($dto['id']) ? AccionAuditoriaEnum::MODIFICAR : AccionAuditoriaEnum::CREAR,
            'recurso_original' => isset($dto['id']) ? $rolOriginal : $rol->toJson(),
            'recurso_resultante' => isset($dto['id']) ? $rol->toJson() : null
        ];
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $this->cargar($rol->id);
    }

    public function eliminar($id)
    {
        $rol = Rol::find($id);

        // Guardar auditoria
        $auditoriaDto = [
            'id_recurso' => $rol->id,
            'nombre_recurso' => Rol::class,
            'descripcion_recurso' => $rol->name,
            'accion' => AccionAuditoriaEnum::ELIMINAR,
            'recurso_original' => $rol->toJson()
        ];
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $rol->delete();
    }

    public function obtenerColeccion($dto)
    {
        $datos = [];
        $roles = Rol::obtenerColeccion($dto);
        foreach ($roles ?? [] as $rol){
            array_push($datos, $rol);
        }

        $cantidadRoles = count($roles);
        $to = isset($roles) && $cantidadRoles > 0 ? $roles->currentPage() * $roles->perPage() : null;
        $to = isset($to) && isset($roles) && $to > $roles->total() && $cantidadRoles > 0 ? $roles->total() : $to;
        $from = isset($to) && isset($roles) && $cantidadRoles > 0 ?
            $roles->perPage() > $to ? 1 : ($to - $roles->perPage()) + 1
            : null;
        return [
            'datos' => $datos,
            'desde' => $from,
            'hasta' => $to,
            'por_pagina' => isset($roles) && $cantidadRoles > 0 ? +$roles->perPage() : 0,
            'pagina_actual' => isset($roles) && $cantidadRoles > 0 ? $roles->currentPage() : 1,
            'ultima_pagina' => isset($roles) && $cantidadRoles > 0 ? $roles->lastPage() : 0,
            'total' => isset($roles) && $cantidadRoles > 0 ? $roles->total() : 0
        ];
    }

    public function obtenerColeccionLigera($dto)
    {
        return Rol::obtenerColeccionLigera($dto);
    }

    public function obtenerPermisos($id, $dto)
    {
        // Obtener todos los permisos
        $permisos = Rol::obtenerPermisos($id, $dto);

        $permisosPorModuloDto = [];
        $permisosPorModulo = $permisos->groupBy('modulo');
        foreach ($permisosPorModulo as $modulo => $permisosDelModulo){
            $permisosPorSubmoduloDto = [];
            $permisosPorSubmodulo = $permisosDelModulo->groupBy('submodulo');
            foreach ($permisosPorSubmodulo as $submodulo => $permisosDelSubmodulo){
                $permisosDto = [];
                foreach ($permisosDelSubmodulo as $permiso){
                    array_push($permisosDto, [
                        "nombre" => $permiso->titulo,
                        "clave" => $permiso->nombre,
                        "permitido" => !!$permiso->permitido
                    ]);
                }

                array_push($permisosPorSubmoduloDto, [
                    "nombre" => $submodulo,
                    "permisos" => $permisosDto
                ]);
            }

            array_push($permisosPorModuloDto, [
                "nombre" => $modulo,
                "submodulos" => $permisosPorSubmoduloDto
            ]);
        }

        return $permisosPorModuloDto;
    }

    public function otorgarPermisos($id, $permisos){
        $rol = Role::find($id);
        foreach ($permisos as $permiso){
            try{
                $rol->givePermissionTo($permiso);
            }catch (Exception $e){}
        }
    }

    public function revocarPermisos($id, $permisos){
        $rol = Role::find($id);
        foreach ($permisos as $permiso){
            try{
                $rol->revokePermissionTo($permiso);
            }catch (Exception $e){}
        }
    }

}
