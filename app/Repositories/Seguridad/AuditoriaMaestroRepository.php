<?php

namespace App\Repositories\Seguridad;

use App\Contracts\Seguridad\AuditoriaMaestroService;
use App\Exceptions\ModelException;
use App\Model\Seguridad\AuditoriaMaestro;
use App\Rol;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class AuditoriaMaestroRepository implements AuditoriaMaestroService
{

    public function cargar($id)
    {
        //
    }

    public function crear($dto)
    {
        $user = Auth::user();
        $usuario = $user->usuario();
        $dto['responsable_id'] = $usuario->id ?? ($dto['responsable_id'] ?? null);
        $dto['responsable_nombre'] = $usuario->nombre ?? ($dto['responsable_nombre'] ?? null);

        $auditoria = AuditoriaMaestro::create($dto);
        if(!isset($auditoria)){
            throw new ModelException(
                "Ocurrió un error al intentar guardar la auditoria del recurso.",
                $auditoria
            );
        }

        return $this->cargar($auditoria->id);
    }

    public function eliminar($id)
    {
        //
    }

    public function obtenerColeccion($dto)
    {
        $datos = [];
        $auditorias = AuditoriaMaestro::obtenerColeccion($dto);
        foreach ($auditorias ?? [] as $auditoria){
            array_push($datos, $auditoria);
        }

        $cantidadAuditorias = count($auditorias ?? []);
        $to = isset($auditorias) && $cantidadAuditorias > 0 ? $auditorias->currentPage() * $auditorias->perPage() : null;
        $to = isset($to) && isset($auditorias) && $to > $auditorias->total() && $cantidadAuditorias > 0 ? $auditorias->total() : $to;
        $from = isset($to) && isset($auditorias) && $cantidadAuditorias > 0 ?
            $auditorias->perPage() > $to ? 1 : ($to - $auditorias->perPage()) + 1
            : null;
        return [
            'datos' => $datos,
            'desde' => $from,
            'hasta' => $to,
            'por_pagina' => isset($auditorias) && $cantidadAuditorias > 0 ? +$auditorias->perPage() : 0,
            'pagina_actual' => isset($auditorias) && $cantidadAuditorias > 0 ? $auditorias->currentPage() : 1,
            'ultima_pagina' => isset($auditorias) && $cantidadAuditorias > 0 ? $auditorias->lastPage() : 0,
            'total' => isset($auditorias) && $cantidadAuditorias > 0 ? $auditorias->total() : 0
        ];
    }

}
