<?php

namespace App\Repositories\Seguridad;

use App\Contracts\Seguridad\AuditoriaMaestroService;
use App\Contracts\Seguridad\OpcionSistemaService;
use App\Enum\AccionAuditoriaEnum;
use App\Exceptions\ModelException;
use App\Model\Seguridad\OpcionSistema;
use App\Rol;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Exception;

class OpcionSistemaRepository implements OpcionSistemaService
{

    protected  $auditoriaMaestroService;

    public function __construct(AuditoriaMaestroService $auditoriaMaestroService){
        $this->auditoriaMaestroService = $auditoriaMaestroService;
    }

    public function cargar($id)
    {
        $opcionSistema = OpcionSistema::find($id);
        return [
            'id' => $opcionSistema->id,
            'nombre' => $opcionSistema->nombre,
            'icono_menu' => $opcionSistema->icono_menu,
            'clave_permiso' => $opcionSistema->clave_permiso,
            'posicion' => $opcionSistema->posicion,
            'url' => $opcionSistema->url,
            'url_ayuda' => $opcionSistema->url_ayuda,
            'estado' => $opcionSistema->estado,
            'opcion_del_sistema_id' => $opcionSistema->opcion_del_sistema_id,
            'usuario_creacion_id' => $opcionSistema->creation_user_id,
            'usuario_creacion_nombre' => $opcionSistema->creation_user_name,
            'usuario_modificacion_id' => $opcionSistema->modification_user_id,
            'usuario_modificacion_nombre' => $opcionSistema->modification_user_name,
            'fecha_creacion' => (new Carbon($opcionSistema->created_at))->format("Y-m-d H:i:s"),
            'fecha_modificacion' => (new Carbon($opcionSistema->updated_at))->format("Y-m-d H:i:s")
        ];
    }

    public function modificarOCrear($dto)
    {
        $user = Auth::user();
        $usuario = $user->usuario();
        if (!isset($dto['id'])) {
            $dto['usuario_creacion_id'] = $usuario->id ?? ($dto['usuario_creacion_id'] ?? null);
            $dto['usuario_creacion_nombre'] = $usuario->nombre ?? ($dto['usuario_creacion_nombre'] ?? null);
        }
        if (isset($usuario) || isset($dto['usuario_modificacion_id'])) {
            $dto['usuario_modificacion_id'] = $usuario->id ?? ($dto['usuario_modificacion_id'] ?? null);
            $dto['usuario_modificacion_nombre'] = $usuario->nombre ?? ($dto['usuario_modificacion_nombre'] ?? null);
        }

        // Consultar la opcion del sistema
        $opcionSistema = isset($dto['id']) ? OpcionSistema::find($dto['id']) : new OpcionSistema();

        // Guardar objeto original para auditoria
        $opcionSistemaOriginal = $opcionSistema->toJson();

        $opcionSistema->fill($dto);
        $guardado = $opcionSistema->save();
        if(!$guardado){
            throw new ModelException("Ocurrió un error al intentar guardar la opción del sistema.",
                $opcionSistema
            );
        }

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $opcionSistema->id,
            'nombre_recurso' => OpcionSistema::class,
            'descripcion_recurso' => $opcionSistema->nombre,
            'accion' => isset($dto['id']) ? AccionAuditoriaEnum::MODIFICAR : AccionAuditoriaEnum::CREAR,
            'recurso_original' => isset($dto['id']) ? $opcionSistemaOriginal : $opcionSistema->toJson(),
            'recurso_resultante' => isset($dto['id']) ? $opcionSistema->toJson() : null
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $this->cargar($opcionSistema->id);
    }

    public function obtenerColeccion($dto)
    {
        $datos = [];
        $opcionesSistema = OpcionSistema::obtenerColeccion($dto);
        foreach ($opcionesSistema ?? [] as $opcionSistema){
            array_push($datos, $opcionSistema);
        }

        $cantidadOpcionesSistema = count($opcionesSistema);
        $to = isset($opcionesSistema) && $cantidadOpcionesSistema > 0 ? $opcionesSistema->currentPage() * $opcionesSistema->perPage() : null;
        $to = isset($to) && isset($opcionesSistema) && $to > $opcionesSistema->total() && $cantidadOpcionesSistema > 0 ? $opcionesSistema->total() : $to;
        $from = isset($to) && isset($opcionesSistema) && $cantidadOpcionesSistema > 0 ?
            $opcionesSistema->perPage() > $to ? 1 : ($to - $opcionesSistema->perPage()) + 1
            : null;
        return [
            'datos' => $datos,
            'desde' => $from,
            'hasta' => $to,
            'por_pagina' => isset($opcionesSistema) && $cantidadOpcionesSistema > 0 ? +$opcionesSistema->perPage() : 0,
            'pagina_actual' => isset($opcionesSistema) && $cantidadOpcionesSistema > 0 ? $opcionesSistema->currentPage() : 1,
            'ultima_pagina' => isset($opcionesSistema) && $cantidadOpcionesSistema > 0 ? $opcionesSistema->lastPage() : 0,
            'total' => isset($opcionesSistema) && $cantidadOpcionesSistema > 0 ? $opcionesSistema->total() : 0
        ];
    }

    public function obtenerColeccionLigera($dto)
    {
        $opcionesDelSistema = OpcionSistema::obtenerColeccionLigera($dto);
        return $opcionesDelSistema;
    }

    public function obtenerArbolDeOpciones(){
        return OpcionSistema::with(array('hijos' => function($query) {
            $query->whereEstado(true)->orderBy('posicion', 'ASC');
        }))->whereNull('opcion_del_sistema_id')->whereEstado(true)->orderBy('posicion', 'ASC')->get();
    }

}
