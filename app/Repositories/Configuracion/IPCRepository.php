<?php


namespace App\Repositories\Configuracion;


use App\Contracts\Configuracion\IPCService;
use App\Contracts\Seguridad\AuditoriaMaestroService;
use App\Enum\AccionAuditoriaEnum;
use App\Exceptions\ModelException;
use App\Model\Configuracion\IPC;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class IPCRepository implements IPCService
{

    protected  $auditoriaMaestroService;

    public function __construct(AuditoriaMaestroService $auditoriaMaestroService){
        $this->auditoriaMaestroService = $auditoriaMaestroService;
    }

    public function cargar($id)
    {
        $IPC = IPC::find($id);

        return [
            'id' => $IPC->id,
            'fecha' => $IPC->fecha,
            'indice_IPC' => $IPC->indice_IPC,
            'inflacion_anual' => $IPC->inflacion_anual,
            'inflacion_mensual' => $IPC->inflacion_mensual,
            'inflacion_anio' => $IPC->inflacion_anio,
            'estado' => $IPC->estado,
            'usuario_creacion_id' => $IPC->usuario_creacion_id,
            'usuario_creacion_nombre' => $IPC->usuario_creacion_nombre,
            'usuario_modificacion_id' => $IPC->usuario_modificacion_id,
            'usuario_modificacion_nombre' => $IPC->usuario_modificacion_nombre,
            'fecha_creacion' => (new Carbon($IPC->created_at))->format("Y-m-d H:i:s"),
            'fecha_modificacion' => (new Carbon($IPC->updated_at))->format("Y-m-d H:i:s")
        ];
    }

    public function modificarOCrear($dto)
    {
        $user = Auth::user();
        $usuario = $user->usuario();
        if (!isset($dto['id'])) {
            $dto['usuario_creacion_id'] = $usuario->id ?? ($dto['usuario_creacion_id'] ?? null);
            $dto['usuario_creacion_nombre'] = $usuario->nombre ?? ($dto['usuario_creacion_nombre'] ?? null);
        }
        if (isset($usuario) || isset($dto['usuario_modificacion_id'])) {
            $dto['usuario_modificacion_id'] = $usuario->id ?? ($dto['usuario_modificacion_id'] ?? null);
            $dto['usuario_modificacion_nombre'] = $usuario->nombre ?? ($dto['usuario_modificacion_nombre'] ?? null);
        }

        // Consultar el servicio
        $IPC = isset($dto['id']) ? IPC::find($dto['id']) : new IPC();

        // Guardar objeto original para auditoria
        $IPCOriginal = $IPC->toJson();

        $IPC->fill($dto);
        $guardado = $IPC->save();
        if(!$guardado){
            throw new ModelException("Ocurrió un error al intentar guardar el IPC", $IPC);
        }

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $IPC->id,
            'nombre_recurso' => IPC::class,
            'descripcion_recurso' => $IPC->nombre,
            'accion' => isset($dto['id']) ? AccionAuditoriaEnum::MODIFICAR : AccionAuditoriaEnum::CREAR,
            'recurso_original' => isset($dto['id']) ? $IPCOriginal : $IPC->toJson(),
            'recurso_resultante' => isset($dto['id']) ? $IPC->toJson() : null
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $this->cargar($IPC->id);
    }

    public function eliminar($id)
    {
        // Connsultar el objeto
        $IPC = IPC::find($id);

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $IPC->id,
            'nombre_recurso' => IPC::class,
            'descripcion_recurso' => $IPC->nombre,
            'accion' => AccionAuditoriaEnum::ELIMINAR,
            'recurso_original' => $IPC->toJson()
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $IPC->delete();
    }

    public function obtenerColeccion($dto)
    {
        $data = [];
        $IPCs = IPC::obtenerColeccion($dto);
        foreach ($IPCs ?? [] as $IPC){
            array_push($data, $IPC);
        }

        $cantidadIPCs = count($IPCs);
        $to = isset($IPCs) && $cantidadIPCs > 0 ? $IPCs->currentPage() * $IPCs->perPage() : null;
        $to = isset($to) && isset($IPCs) && $to > $IPCs->total() && $cantidadIPCs > 0 ? $IPCs->total() : $to;
        $from = isset($to) && isset($IPCs) && $cantidadIPCs > 0 ?
            $IPCs->perPage() > $to ? 1 : ($to - $IPCs->perPage()) + 1
            : null;
        return [
            'datos' => $data,
            'desde' => $from,
            'hasta' => $to,
            'por_pagina' => isset($IPCs) && $cantidadIPCs > 0 ? +$IPCs->perPage() : 0,
            'pagina_actual' => isset($IPCs) && $cantidadIPCs > 0 ? $IPCs->currentPage() : 1,
            'ultima_pagina' => isset($IPCs) && $cantidadIPCs > 0 ? $IPCs->lastPage() : 0,
            'total' => isset($IPCs) && $cantidadIPCs > 0 ? $IPCs->total() : 0
        ];
    }

    public function obtenerColeccionLigera($dto)
    {
        $IPC = IPC::obtenerColeccionLigera($dto);
        return $IPC;
    }

}
