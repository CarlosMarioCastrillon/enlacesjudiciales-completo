<?php


namespace App\Repositories\Configuracion;


use App\Contracts\Configuracion\TipoActuacionService;
use App\Contracts\Seguridad\AuditoriaMaestroService;
use App\Enum\AccionAuditoriaEnum;
use App\Exceptions\ModelException;
use App\Model\Configuracion\TipoActuacion;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class TipoActuacionRepository implements TipoActuacionService
{

    protected  $auditoriaMaestroService;

    public function __construct(AuditoriaMaestroService $auditoriaMaestroService){
        $this->auditoriaMaestroService = $auditoriaMaestroService;
    }

    public function cargar($id)
    {
        $tipoActuacion = TipoActuacion::find($id);

        return [
            'id' => $tipoActuacion->id,
            'nombre' => $tipoActuacion->nombre,
            'estado' => $tipoActuacion->estado,
            'usuario_creacion_id' => $tipoActuacion->usuario_creacion_id,
            'usuario_creacion_nombre' => $tipoActuacion->usuario_creacion_nombre,
            'usuario_modificacion_id' => $tipoActuacion->usuario_modificacion_id,
            'usuario_modificacion_nombre' => $tipoActuacion->usuario_modificacion_nombre,
            'fecha_creacion' => (new Carbon($tipoActuacion->created_at))->format("Y-m-d H:i:s"),
            'fecha_modificacion' => (new Carbon($tipoActuacion->updated_at))->format("Y-m-d H:i:s")
        ];
    }

    public function modificarOCrear($dto)
    {
        $user = Auth::user();
        $usuario = $user->usuario();
        if (!isset($dto['id'])) {
            $dto['usuario_creacion_id'] = $usuario->id ?? ($dto['usuario_creacion_id'] ?? null);
            $dto['usuario_creacion_nombre'] = $usuario->nombre ?? ($dto['usuario_creacion_nombre'] ?? null);
        }
        if (isset($usuario) || isset($dto['usuario_modificacion_id'])) {
            $dto['usuario_modificacion_id'] = $usuario->id ?? ($dto['usuario_modificacion_id'] ?? null);
            $dto['usuario_modificacion_nombre'] = $usuario->nombre ?? ($dto['usuario_modificacion_nombre'] ?? null);
        }

        // Consultar el tipo de empresa
        $tipoActuacion = isset($dto['id']) ? TipoActuacion::find($dto['id']) : new TipoActuacion();

        // Guardar objeto original para auditoria
        $tipoActuacionOriginal = $tipoActuacion->toJson();

        $tipoActuacion->fill($dto);
        $guardado = $tipoActuacion->save();
        if(!$guardado){
            throw new ModelException("Ocurrió un error al intentar guardar el tipo de empresa.", $tipoActuacion);
        }

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $tipoActuacion->id,
            'nombre_recurso' => TipoActuacion::class,
            'descripcion_recurso' => $tipoActuacion->nombre,
            'accion' => isset($dto['id']) ? AccionAuditoriaEnum::MODIFICAR : AccionAuditoriaEnum::CREAR,
            'recurso_original' => isset($dto['id']) ? $tipoActuacionOriginal : $tipoActuacion->toJson(),
            'recurso_resultante' => isset($dto['id']) ? $tipoActuacion->toJson() : null
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $this->cargar($tipoActuacion->id);
    }

    public function eliminar($id)
    {
        // Connsultar el objeto
        $tipoActuacion = TipoActuacion::find($id);

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $tipoActuacion->id,
            'nombre_recurso' => TipoActuacion::class,
            'descripcion_recurso' => $tipoActuacion->nombre,
            'accion' => AccionAuditoriaEnum::ELIMINAR,
            'recurso_original' => $tipoActuacion->toJson()
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $tipoActuacion->delete();
    }

    public function obtenerColeccion($dto)
    {
        $data = [];
        $tiposActuacion = TipoActuacion::obtenerColeccion($dto);
        foreach ($tiposActuacion ?? [] as $shipping){
            array_push($data, $shipping);
        }

        $cantidadTiposActuacion = count($tiposActuacion);
        $to = isset($tiposActuacion) && $cantidadTiposActuacion > 0 ? $tiposActuacion->currentPage() * $tiposActuacion->perPage() : null;
        $to = isset($to) && isset($tiposActuacion) && $to > $tiposActuacion->total() && $cantidadTiposActuacion > 0 ? $tiposActuacion->total() : $to;
        $from = isset($to) && isset($tiposActuacion) && $cantidadTiposActuacion > 0 ?
            $tiposActuacion->perPage() > $to ? 1 : ($to - $tiposActuacion->perPage()) + 1
            : null;
        return [
            'datos' => $data,
            'desde' => $from,
            'hasta' => $to,
            'por_pagina' => isset($tiposActuacion) && $cantidadTiposActuacion > 0 ? +$tiposActuacion->perPage() : 0,
            'pagina_actual' => isset($tiposActuacion) && $cantidadTiposActuacion > 0 ? $tiposActuacion->currentPage() : 1,
            'ultima_pagina' => isset($tiposActuacion) && $cantidadTiposActuacion > 0 ? $tiposActuacion->lastPage() : 0,
            'total' => isset($tiposActuacion) && $cantidadTiposActuacion > 0 ? $tiposActuacion->total() : 0
        ];
    }

    public function obtenerColeccionLigera($dto)
    {
        $tipoActuacion = TipoActuacion::obtenerColeccionLigera($dto);
        return $tipoActuacion;
    }
}
