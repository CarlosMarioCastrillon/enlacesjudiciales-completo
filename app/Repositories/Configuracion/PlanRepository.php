<?php


namespace App\Repositories\Configuracion;


use App\Contracts\Configuracion\PlanService;
use App\Contracts\Seguridad\AuditoriaMaestroService;
use App\Enum\AccionAuditoriaEnum;
use App\Exceptions\ModelException;
use App\Model\Configuracion\Plan;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class PlanRepository implements PlanService
{

    protected  $auditoriaMaestroService;

    public function __construct(AuditoriaMaestroService $auditoriaMaestroService){
        $this->auditoriaMaestroService = $auditoriaMaestroService;
    }

    public function cargar($id)
    {
        $plan = Plan::find($id);
        $ciudad = $plan->ciudad;
        $departamento = $plan->departamento;
        $servicio = $plan->servicio;

        return [
            'id' => $plan->id,
            'servicio_id' => $plan->servicio_id,
            'departamento_id' => $plan->departamento_id,
            'ciudad_id' => $plan->ciudad_id,
            'empresa_id' => $plan->empresa_id,
            'cantidad' => $plan->cantidad,
            'valor' => $plan->valor,
            'observacion' => $plan->observacion,
            'estado' => $plan->estado,
            'usuario_creacion_id' => $plan->usuario_creacion_id,
            'usuario_creacion_nombre' => $plan->usuario_creacion_nombre,
            'usuario_modificacion_id' => $plan->usuario_modificacion_id,
            'usuario_modificacion_nombre' => $plan->usuario_modificacion_nombre,
            'fecha_creacion' => (new Carbon($plan->created_at))->format("Y-m-d H:i:s"),
            'fecha_modificacion' => (new Carbon($plan->updated_at))->format("Y-m-d H:i:s"),
            'ciudad' => isset($ciudad) ? [
                'id' => $ciudad->id,
                'nombre' => $ciudad->nombre,
            ] : null,
            'departamento' => isset($departamento) ? [
                'id' => $departamento->id,
                'nombre' => $departamento->nombre,
            ] : null,
            'servicio' => isset($servicio) ? [
                'id' => $servicio->id,
                'nombre' => $servicio->nombre,
            ] : null
        ];
    }

    public function modificarOCrear($dto)
    {
        $user = Auth::user();
        $usuario = $user->usuario();
        if (!isset($dto['id'])) {
            $dto['usuario_creacion_id'] = $usuario->id ?? ($dto['usuario_creacion_id'] ?? null);
            $dto['usuario_creacion_nombre'] = $usuario->nombre ?? ($dto['usuario_creacion_nombre'] ?? null);
        }
        if (isset($usuario) || isset($dto['usuario_modificacion_id'])) {
            $dto['usuario_modificacion_id'] = $usuario->id ?? ($dto['usuario_modificacion_id'] ?? null);
            $dto['usuario_modificacion_nombre'] = $usuario->nombre ?? ($dto['usuario_modificacion_nombre'] ?? null);
        }

        // Consultar el plan
        $plan = isset($dto['id']) ? Plan::find($dto['id']) : new Plan();

        // Guardar objeto original para auditoria
        $planOriginal = $plan->toJson();

        $plan->fill($dto);
        $departamento = $plan->departamento;
        $ciudad = $plan->ciudad;

        if ( $departamento == null Xor $ciudad == null){
            throw new ModelException("Por favor elija la ciudad o vacíe la casilla del departamento.", $plan);
        }else{
            $guardado = $plan->save();
        }
        if(!$guardado){
            throw new ModelException("Ocurrió un error al intentar guardar el plan.", $plan);
        }

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $plan->id,
            'nombre_recurso' => Plan::class,
            'descripcion_recurso' => $plan->servicio->nombre,
            'accion' => isset($dto['id']) ? AccionAuditoriaEnum::MODIFICAR : AccionAuditoriaEnum::CREAR,
            'recurso_original' => isset($dto['id']) ? $planOriginal : $plan->toJson(),
            'recurso_resultante' => isset($dto['id']) ? $plan->toJson() : null
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $this->cargar($plan->id);
    }

    public function eliminar($id)
    {
        // Connsultar el objeto
        $plan = Plan::find($id);

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $plan->id,
            'nombre_recurso' => Plan::class,
            'descripcion_recurso' =>  $plan->servicio->nombre,
            'accion' => AccionAuditoriaEnum::ELIMINAR,
            'recurso_original' => $plan->toJson()
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $plan->delete();
    }

    public function obtenerColeccion($dto)
    {
        $data = [];
        $planes = Plan::obtenerColeccion($dto);
        foreach ($planes ?? [] as $plan){
            array_push($data, $plan);
        }

        $cantidadPlanes = count($planes);
        $to = isset($planes) && $cantidadPlanes > 0 ? $planes->currentPage() * $planes->perPage() : null;
        $to = isset($to) && isset($planes) && $to > $planes->total() && $cantidadPlanes> 0 ? $planes->total() : $to;
        $from = isset($to) && isset($planes) && $cantidadPlanes > 0 ?
            $planes->perPage() > $to ? 1 : ($to - $planes->perPage()) + 1
            : null;
        return [
            'datos' => $data,
            'desde' => $from,
            'hasta' => $to,
            'por_pagina' => isset($planes) && $cantidadPlanes > 0 ? +$planes->perPage() : 0,
            'pagina_actual' => isset($planes) && $cantidadPlanes > 0 ? $planes->currentPage() : 1,
            'ultima_pagina' => isset($planes) && $cantidadPlanes > 0 ? $planes->lastPage() : 0,
            'total' => isset($planes) && $cantidadPlanes > 0 ? $planes->total() : 0
        ];
    }

    public function obtener($dto)
    {
       return $planes = Plan::obtener($dto);
    }
}
