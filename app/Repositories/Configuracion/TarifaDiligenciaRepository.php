<?php


namespace App\Repositories\Configuracion;


use App\Contracts\Configuracion\TarifaDiligenciaService;
use App\Contracts\Seguridad\AuditoriaMaestroService;
use App\Enum\AccionAuditoriaEnum;
use App\Exceptions\ModelException;
use App\Model\Configuracion\TarifaDiligencia;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class TarifaDiligenciaRepository implements TarifaDiligenciaService
{

    protected  $auditoriaMaestroService;

    public function __construct(AuditoriaMaestroService $auditoriaMaestroService){
        $this->auditoriaMaestroService = $auditoriaMaestroService;
    }

    public function cargar($id)
    {
        $tarifaDiligencia = TarifaDiligencia::find($id);
        $ciudad = $tarifaDiligencia->ciudad;
        $departamento = $tarifaDiligencia->departamento;
        $tipo_diligencia = $tarifaDiligencia->tipo_diligencia;

        return [
            'id' => $tarifaDiligencia->id,
            'tarifa_diligencia_id' => $tarifaDiligencia->servicio_id,
            'departamento_id' => $tarifaDiligencia->departamento_id,
            'ciudad_id' => $tarifaDiligencia->ciudad_id,
            'valor_diligencia' => $tarifaDiligencia->valor_diligencia,
            'gasto_envio' => $tarifaDiligencia->gasto_envio,
            'otros_gastos' => $tarifaDiligencia->otros_gastos,
            'estado' => $tarifaDiligencia->estado,
            'usuario_creacion_id' => $tarifaDiligencia->usuario_creacion_id,
            'usuario_creacion_nombre' => $tarifaDiligencia->usuario_creacion_nombre,
            'usuario_modificacion_id' => $tarifaDiligencia->usuario_modificacion_id,
            'usuario_modificacion_nombre' => $tarifaDiligencia->usuario_modificacion_nombre,
            'fecha_creacion' => (new Carbon())->parse($tarifaDiligencia->created_at)->format("Y-m-d H:i:s"),
            'fecha_modificacion' => (new Carbon())->parse($tarifaDiligencia->updated_at)->format("Y-m-d H:i:s"),
            'ciudad' => isset($ciudad) ? [
                'id' => $ciudad->id,
                'nombre' => $ciudad->nombre,
            ] : null,
            'departamento' => isset($departamento) ? [
                'id' => $departamento->id,
                'nombre' => $departamento->nombre,
            ] : null,
            'tipo_diligencia' => isset($tipo_diligencia) ? [
                'id' => $tipo_diligencia->id,
                'nombre' => $tipo_diligencia->nombre,
            ] : null
        ];
    }

    public function modificarOCrear($dto)
    {
        $user = Auth::user();
        $usuario = $user->usuario();
        if (!isset($dto['id'])) {
            $dto['usuario_creacion_id'] = $usuario->id ?? ($dto['usuario_creacion_id'] ?? null);
            $dto['usuario_creacion_nombre'] = $usuario->nombre ?? ($dto['usuario_creacion_nombre'] ?? null);
        }
        if (isset($usuario) || isset($dto['usuario_modificacion_id'])) {
            $dto['usuario_modificacion_id'] = $usuario->id ?? ($dto['usuario_modificacion_id'] ?? null);
            $dto['usuario_modificacion_nombre'] = $usuario->nombre ?? ($dto['usuario_modificacion_nombre'] ?? null);
        }

        // Consultar el tarifaDiligencia
        $tarifaDiligencia = isset($dto['id']) ? TarifaDiligencia::find($dto['id']) : new TarifaDiligencia();

        // Guardar objeto original para auditoria
        $tarifaDiligenciaOriginal = $tarifaDiligencia->toJson();

        $tarifaDiligencia->fill($dto);
        $guardado = $tarifaDiligencia->save();

        if(!$guardado){
            throw new ModelException("Ocurrió un error al intentar guardar el tarifaDiligencia.", $tarifaDiligencia);
        }

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $tarifaDiligencia->id,
            'nombre_recurso' => TarifaDiligencia::class,
            'descripcion_recurso' => $tarifaDiligencia->tipo_diligencia->nombre,
            'accion' => isset($dto['id']) ? AccionAuditoriaEnum::MODIFICAR : AccionAuditoriaEnum::CREAR,
            'recurso_original' => isset($dto['id']) ? $tarifaDiligenciaOriginal : $tarifaDiligencia->toJson(),
            'recurso_resultante' => isset($dto['id']) ? $tarifaDiligencia->toJson() : null
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $this->cargar($tarifaDiligencia->id);
    }

    public function eliminar($id)
    {
        // Connsultar el objeto
        $tarifaDiligencia = TarifaDiligencia::find($id);

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $tarifaDiligencia->id,
            'nombre_recurso' => TarifaDiligencia::class,
            'descripcion_recurso' =>  $tarifaDiligencia->tipo_diligencia->nombre,
            'accion' => AccionAuditoriaEnum::ELIMINAR,
            'recurso_original' => $tarifaDiligencia->toJson()
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $tarifaDiligencia->delete();
    }

    public function obtenerColeccion($dto)
    {
        $data = [];
        $tarifasDiligencias = TarifaDiligencia::obtenerColeccion($dto);
        foreach ($tarifasDiligencias ?? [] as $dato){
            array_push($data, $dato);
        }

        $cantidadTarifasDiligencias = count($tarifasDiligencias);
        $to = isset($tarifasDiligencias) && $cantidadTarifasDiligencias > 0 ? $tarifasDiligencias->currentPage() * $tarifasDiligencias->perPage() : null;
        $to = isset($to) && isset($tarifasDiligencias) && $to > $tarifasDiligencias->total() && $cantidadTarifasDiligencias> 0 ? $tarifasDiligencias->total() : $to;
        $from = isset($to) && isset($tarifasDiligencias) && $cantidadTarifasDiligencias > 0 ?
            $tarifasDiligencias->perPage() > $to ? 1 : ($to - $tarifasDiligencias->perPage()) + 1
            : null;
        return [
            'datos' => $data,
            'desde' => $from,
            'hasta' => $to,
            'por_pagina' => isset($tarifasDiligencias) && $cantidadTarifasDiligencias > 0 ? +$tarifasDiligencias->perPage() : 0,
            'pagina_actual' => isset($tarifasDiligencias) && $cantidadTarifasDiligencias > 0 ? $tarifasDiligencias->currentPage() : 1,
            'ultima_pagina' => isset($tarifasDiligencias) && $cantidadTarifasDiligencias > 0 ? $tarifasDiligencias->lastPage() : 0,
            'total' => isset($tarifasDiligencias) && $cantidadTarifasDiligencias > 0 ? $tarifasDiligencias->total() : 0
        ];
    }

    public function obtener($dto)
    {
        $tarifaDiligencia = TarifaDiligencia::obtener($dto);
        return $tarifaDiligencia;
    }
}
