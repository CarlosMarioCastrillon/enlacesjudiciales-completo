<?php


namespace App\Repositories\Configuracion;


use App\Contracts\Configuracion\ClaseProcesoService;
use App\Contracts\Seguridad\AuditoriaMaestroService;
use App\Enum\AccionAuditoriaEnum;
use App\Exceptions\ModelException;
use App\Model\Configuracion\ClaseProceso;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class ClaseProcesoRepository implements ClaseProcesoService
{

    protected  $auditoriaMaestroService;

    public function __construct(AuditoriaMaestroService $auditoriaMaestroService){
        $this->auditoriaMaestroService = $auditoriaMaestroService;
    }

    public function cargar($id)
    {
        $claseProceso = ClaseProceso::find($id);

        return [
            'id' => $claseProceso->id,
            'nombre' => $claseProceso->nombre,
            'estado' => $claseProceso->estado,
            'usuario_creacion_id' => $claseProceso->usuario_creacion_id,
            'usuario_creacion_nombre' => $claseProceso->usuario_creacion_nombre,
            'usuario_modificacion_id' => $claseProceso->usuario_modificacion_id,
            'usuario_modificacion_nombre' => $claseProceso->usuario_modificacion_nombre,
            'fecha_creacion' => (new Carbon($claseProceso->created_at))->format("Y-m-d H:i:s"),
            'fecha_modificacion' => (new Carbon($claseProceso->updated_at))->format("Y-m-d H:i:s")
        ];
    }

    public function modificarOCrear($dto)
    {
        $user = Auth::user();
        $usuario = $user->usuario();
        if (!isset($dto['id'])) {
            $dto['usuario_creacion_id'] = $usuario->id ?? ($dto['usuario_creacion_id'] ?? null);
            $dto['usuario_creacion_nombre'] = $usuario->nombre ?? ($dto['usuario_creacion_nombre'] ?? null);
        }
        if (isset($usuario) || isset($dto['usuario_modificacion_id'])) {
            $dto['usuario_modificacion_id'] = $usuario->id ?? ($dto['usuario_modificacion_id'] ?? null);
            $dto['usuario_modificacion_nombre'] = $usuario->nombre ?? ($dto['usuario_modificacion_nombre'] ?? null);
        }

        // Consultar la clase de proceso
        $claseProceso = isset($dto['id']) ? ClaseProceso::find($dto['id']) : new ClaseProceso();

        // Guardar objeto original para auditoria
        $claseProcesoOriginal = $claseProceso->toJson();

        $claseProceso->fill($dto);
        $guardado = $claseProceso->save();
        if(!$guardado){
            throw new ModelException("Ocurrió un error al intentar guardar la clase de proceso.", $claseProceso);
        }

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $claseProceso->id,
            'nombre_recurso' => ClaseProceso::class,
            'descripcion_recurso' => $claseProceso->nombre,
            'accion' => isset($dto['id']) ? AccionAuditoriaEnum::MODIFICAR : AccionAuditoriaEnum::CREAR,
            'recurso_original' => isset($dto['id']) ? $claseProcesoOriginal : $claseProceso->toJson(),
            'recurso_resultante' => isset($dto['id']) ? $claseProceso->toJson() : null
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $this->cargar($claseProceso->id);
    }

    public function eliminar($id)
    {
        // Connsultar el objeto
        $claseProceso = ClaseProceso::find($id);

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $claseProceso->id,
            'nombre_recurso' => ClaseProceso::class,
            'descripcion_recurso' => $claseProceso->nombre,
            'accion' => AccionAuditoriaEnum::ELIMINAR,
            'recurso_original' => $claseProceso->toJson()
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $claseProceso->delete();
    }

    public function obtenerColeccion($dto)
    {
        $data = [];
        $clasesProcesos = ClaseProceso::obtenerColeccion($dto);
        foreach ($clasesProcesos ?? [] as $shipping){
            array_push($data, $shipping);
        }

        $cantidadClasesProcesos = count($clasesProcesos);
        $to = isset($clasesProcesos) && $cantidadClasesProcesos > 0 ? $clasesProcesos->currentPage() * $clasesProcesos->perPage() : null;
        $to = isset($to) && isset($clasesProcesos) && $to > $clasesProcesos->total() && $cantidadClasesProcesos> 0 ? $clasesProcesos->total() : $to;
        $from = isset($to) && isset($clasesProcesos) && $cantidadClasesProcesos > 0 ?
            $clasesProcesos->perPage() > $to ? 1 : ($to - $clasesProcesos->perPage()) + 1
            : null;
        return [
            'datos' => $data,
            'desde' => $from,
            'hasta' => $to,
            'por_pagina' => isset($clasesProcesos) && $cantidadClasesProcesos > 0 ? +$clasesProcesos->perPage() : 0,
            'pagina_actual' => isset($clasesProcesos) && $cantidadClasesProcesos > 0 ? $clasesProcesos->currentPage() : 1,
            'ultima_pagina' => isset($clasesProcesos) && $cantidadClasesProcesos > 0 ? $clasesProcesos->lastPage() : 0,
            'total' => isset($clasesProcesos) && $cantidadClasesProcesos > 0 ? $clasesProcesos->total() : 0
        ];
    }

    public function obtenerColeccionLigera($dto)
    {
        $claseProceso = ClaseProceso::obtenerColeccionLigera($dto);
        return $claseProceso;
    }
}
