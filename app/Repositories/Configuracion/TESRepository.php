<?php


namespace App\Repositories\Configuracion;


use App\Contracts\Configuracion\TESService;
use App\Contracts\Seguridad\AuditoriaMaestroService;
use App\Enum\AccionAuditoriaEnum;
use App\Exceptions\ModelException;
use App\Model\Configuracion\TES;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class TESRepository implements TESService
{

    protected  $auditoriaMaestroService;

    public function __construct(AuditoriaMaestroService $auditoriaMaestroService){
        $this->auditoriaMaestroService = $auditoriaMaestroService;
    }

    public function cargar($id)
    {
        $TES = TES::find($id);

        return [
            'id' => $TES->id,
            'fecha' => $TES->fecha,
            'porcentaje_1anio' => $TES->porcentaje_1anio,
            'porcentaje_5anio' => $TES->porcentaje_5anio,
            'porcentaje_10anio' => $TES->porcentaje_10anio,
            'estado' => $TES->estado,
            'usuario_creacion_id' => $TES->usuario_creacion_id,
            'usuario_creacion_nombre' => $TES->usuario_creacion_nombre,
            'usuario_modificacion_id' => $TES->usuario_modificacion_id,
            'usuario_modificacion_nombre' => $TES->usuario_modificacion_nombre,
            'fecha_creacion' => (new Carbon($TES->created_at))->format("Y-m-d H:i:s"),
            'fecha_modificacion' => (new Carbon($TES->updated_at))->format("Y-m-d H:i:s")
        ];
    }

    public function modificarOCrear($dto)
    {
        $user = Auth::user();
        $usuario = $user->usuario();
        if (!isset($dto['id'])) {
            $dto['usuario_creacion_id'] = $usuario->id ?? ($dto['usuario_creacion_id'] ?? null);
            $dto['usuario_creacion_nombre'] = $usuario->nombre ?? ($dto['usuario_creacion_nombre'] ?? null);
        }
        if (isset($usuario) || isset($dto['usuario_modificacion_id'])) {
            $dto['usuario_modificacion_id'] = $usuario->id ?? ($dto['usuario_modificacion_id'] ?? null);
            $dto['usuario_modificacion_nombre'] = $usuario->nombre ?? ($dto['usuario_modificacion_nombre'] ?? null);
        }

        // Consultar el servicio
        $TES = isset($dto['id']) ? TES::find($dto['id']) : new TES();

        // Guardar objeto original para auditoria
        $TESOriginal = $TES->toJson();

        $TES->fill($dto);
        $guardado = $TES->save();
        if(!$guardado){
            throw new ModelException("Ocurrió un error al intentar guardar el servicio.", $TES);
        }

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $TES->id,
            'nombre_recurso' => TES::class,
            'descripcion_recurso' => $TES->nombre,
            'accion' => isset($dto['id']) ? AccionAuditoriaEnum::MODIFICAR : AccionAuditoriaEnum::CREAR,
            'recurso_original' => isset($dto['id']) ? $TESOriginal : $TES->toJson(),
            'recurso_resultante' => isset($dto['id']) ? $TES->toJson() : null
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $this->cargar($TES->id);
    }

    public function eliminar($id)
    {
        // Connsultar el objeto
        $TES = TES::find($id);

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $TES->id,
            'nombre_recurso' => TES::class,
            'descripcion_recurso' => $TES->nombre,
            'accion' => AccionAuditoriaEnum::ELIMINAR,
            'recurso_original' => $TES->toJson()
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $TES->delete();
    }

    public function obtenerColeccion($dto)
    {
        $data = [];
        $TESs = TES::obtenerColeccion($dto);
        foreach ($TESs ?? [] as $TES){
            array_push($data, $TES);
        }

        $cantidadTESs = count($TESs);
        $to = isset($TESs) && $cantidadTESs > 0 ? $TESs->currentPage() * $TESs->perPage() : null;
        $to = isset($to) && isset($TESs) && $to > $TESs->total() && $cantidadTESs > 0 ? $TESs->total() : $to;
        $from = isset($to) && isset($TESs) && $cantidadTESs > 0 ?
            $TESs->perPage() > $to ? 1 : ($to - $TESs->perPage()) + 1
            : null;
        return [
            'datos' => $data,
            'desde' => $from,
            'hasta' => $to,
            'por_pagina' => isset($TESs) && $cantidadTESs > 0 ? +$TESs->perPage() : 0,
            'pagina_actual' => isset($TESs) && $cantidadTESs > 0 ? $TESs->currentPage() : 1,
            'ultima_pagina' => isset($TESs) && $cantidadTESs > 0 ? $TESs->lastPage() : 0,
            'total' => isset($TESs) && $cantidadTESs > 0 ? $TESs->total() : 0
        ];
    }

    public function obtenerColeccionLigera($dto)
    {
        $TES = TES::obtenerColeccionLigera($dto);
        return $TES;
    }

}
