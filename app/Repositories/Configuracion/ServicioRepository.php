<?php


namespace App\Repositories\Configuracion;


use App\Contracts\Configuracion\ServicioService;
use App\Contracts\Seguridad\AuditoriaMaestroService;
use App\Enum\AccionAuditoriaEnum;
use App\Exceptions\ModelException;
use App\Model\Configuracion\Servicio;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class ServicioRepository implements ServicioService
{

    protected  $auditoriaMaestroService;

    public function __construct(AuditoriaMaestroService $auditoriaMaestroService){
        $this->auditoriaMaestroService = $auditoriaMaestroService;
    }

    public function cargar($id)
    {
        $servicio = Servicio::find($id);

        return [
            'id' => $servicio->id,
            'nombre' => $servicio->nombre,
            'estado' => $servicio->estado,
            'usuario_creacion_id' => $servicio->usuario_creacion_id,
            'usuario_creacion_nombre' => $servicio->usuario_creacion_nombre,
            'usuario_modificacion_id' => $servicio->usuario_modificacion_id,
            'usuario_modificacion_nombre' => $servicio->usuario_modificacion_nombre,
            'fecha_creacion' => (new Carbon($servicio->created_at))->format("Y-m-d H:i:s"),
            'fecha_modificacion' => (new Carbon($servicio->updated_at))->format("Y-m-d H:i:s")
        ];
    }

    public function modificarOCrear($dto)
    {
        $user = Auth::user();
        $usuario = $user->usuario();
        if (!isset($dto['id'])) {
            $dto['usuario_creacion_id'] = $usuario->id ?? ($dto['usuario_creacion_id'] ?? null);
            $dto['usuario_creacion_nombre'] = $usuario->nombre ?? ($dto['usuario_creacion_nombre'] ?? null);
        }
        if (isset($usuario) || isset($dto['usuario_modificacion_id'])) {
            $dto['usuario_modificacion_id'] = $usuario->id ?? ($dto['usuario_modificacion_id'] ?? null);
            $dto['usuario_modificacion_nombre'] = $usuario->nombre ?? ($dto['usuario_modificacion_nombre'] ?? null);
        }

        // Consultar el servicio
        $servicio = isset($dto['id']) ? Servicio::find($dto['id']) : new Servicio();

        // Guardar objeto original para auditoria
        $servicioOriginal = $servicio->toJson();

        $servicio->fill($dto);
        $guardado = $servicio->save();
        if(!$guardado){
            throw new ModelException("Ocurrió un error al intentar guardar el servicio.", $servicio);
        }

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $servicio->id,
            'nombre_recurso' => Servicio::class,
            'descripcion_recurso' => $servicio->nombre,
            'accion' => isset($dto['id']) ? AccionAuditoriaEnum::MODIFICAR : AccionAuditoriaEnum::CREAR,
            'recurso_original' => isset($dto['id']) ? $servicioOriginal : $servicio->toJson(),
            'recurso_resultante' => isset($dto['id']) ? $servicio->toJson() : null
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $this->cargar($servicio->id);
    }

    public function eliminar($id)
    {
        // Connsultar el objeto
        $servicio = Servicio::find($id);

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $servicio->id,
            'nombre_recurso' => Servicio::class,
            'descripcion_recurso' => $servicio->nombre,
            'accion' => AccionAuditoriaEnum::ELIMINAR,
            'recurso_original' => $servicio->toJson()
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $servicio->delete();
    }

    public function obtenerColeccion($dto)
    {
        $data = [];
        $servicios = Servicio::obtenerColeccion($dto);
        foreach ($servicios ?? [] as $shipping){
            array_push($data, $shipping);
        }

        $cantidadServicios = count($servicios);
        $to = isset($servicios) && $cantidadServicios > 0 ? $servicios->currentPage() * $servicios->perPage() : null;
        $to = isset($to) && isset($servicios) && $to > $servicios->total() && $cantidadServicios > 0 ? $servicios->total() : $to;
        $from = isset($to) && isset($servicios) && $cantidadServicios > 0 ?
            $servicios->perPage() > $to ? 1 : ($to - $servicios->perPage()) + 1
            : null;
        return [
            'datos' => $data,
            'desde' => $from,
            'hasta' => $to,
            'por_pagina' => isset($servicios) && $cantidadServicios > 0 ? +$servicios->perPage() : 0,
            'pagina_actual' => isset($servicios) && $cantidadServicios > 0 ? $servicios->currentPage() : 1,
            'ultima_pagina' => isset($servicios) && $cantidadServicios > 0 ? $servicios->lastPage() : 0,
            'total' => isset($servicios) && $cantidadServicios > 0 ? $servicios->total() : 0
        ];
    }

    public function obtenerColeccionLigera($dto)
    {
        $servicio = Servicio::obtenerColeccionLigera($dto);
        return $servicio;
    }

}
