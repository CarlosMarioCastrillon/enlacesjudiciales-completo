<?php


namespace App\Repositories\Configuracion;


use App\Contracts\Configuracion\CiudadService;
use App\Contracts\Seguridad\AuditoriaMaestroService;
use App\Enum\AccionAuditoriaEnum;
use App\Exceptions\ModelException;
use App\Model\Configuracion\Ciudad;
use App\Model\Configuracion\Departamento;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class CiudadRepository implements CiudadService
{

    protected  $auditoriaMaestroService;

    public function __construct(AuditoriaMaestroService $auditoriaMaestroService){
        $this->auditoriaMaestroService = $auditoriaMaestroService;
    }

    public function cargar($id)
    {
        $ciudad = Ciudad::find($id);

        return [
            'id' => $ciudad->id,
            'nombre' => $ciudad->nombre,
            'codigo_dane' => $ciudad->codigo_dane,
            'departamento_id' => $ciudad->departamento_id,
            'rama_judicial' => $ciudad->rama_judicial,
            'estado' => $ciudad->estado,
            'usuario_creacion_id' => $ciudad->usuario_creacion_id,
            'usuario_creacion_nombre' => $ciudad->usuario_creacion_nombre,
            'usuario_modificacion_id' => $ciudad->usuario_modificacion_id,
            'usuario_modificacion_nombre' => $ciudad->usuario_modificacion_nombre,
            'fecha_creacion' => (new Carbon($ciudad->created_at))->format("Y-m-d H:i:s"),
            'fecha_modificacion' => (new Carbon($ciudad->updated_at))->format("Y-m-d H:i:s"),
             'departamento' => isset($departamento) ? [
                    'id' => $departamento->id,
                    'nombre' => $departamento->nombre
             ] : null
        ];
    }

    public function modificarOCrear($dto)
    {
        $user = Auth::user();
        $usuario = $user->usuario();
        if (!isset($dto['id'])) {
            $dto['usuario_creacion_id'] = $usuario->id ?? ($dto['usuario_creacion_id'] ?? null);
            $dto['usuario_creacion_nombre'] = $usuario->nombre ?? ($dto['usuario_creacion_nombre'] ?? null);
        }
        if (isset($usuario) || isset($dto['usuario_modificacion_id'])) {
            $dto['usuario_modificacion_id'] = $usuario->id ?? ($dto['usuario_modificacion_id'] ?? null);
            $dto['usuario_modificacion_nombre'] = $usuario->nombre ?? ($dto['usuario_modificacion_nombre'] ?? null);
        }

        // Consultar la ciudad
        $ciudad = isset($dto['id']) ? Ciudad::find($dto['id']) : new Ciudad();

        // Guardar objeto original para auditoria
        $ciudadOriginal = $ciudad->toJson();

        $ciudad->fill($dto);
        $guardado = $ciudad->save();
        if(!$guardado){
            throw new ModelException("Ocurrió un error al intentar guardar la ciudad.", $ciudad);
        }

        //Consultar código DANE departamento y código DANE de la ciudad
        $departamento = Departamento::find($dto['departamento_id']);
        $codigoDaneCorto = substr($ciudad->codigo_dane,0,2);
        //Validar si el código departamento coincide con los dos primero dígitos del código ciudad
        if($departamento->codigo_dane != $codigoDaneCorto){
            throw new ModelException("El código DANE no coincide con el departamento.", $ciudad);
        }


        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $ciudad->id,
            'nombre_recurso' => Ciudad::class,
            'descripcion_recurso' => $ciudad->nombre,
            'accion' => isset($dto['id']) ? AccionAuditoriaEnum::MODIFICAR : AccionAuditoriaEnum::CREAR,
            'recurso_original' => isset($dto['id']) ? $ciudadOriginal : $ciudad->toJson(),
            'recurso_resultante' => isset($dto['id']) ? $ciudad->toJson() : null
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $this->cargar($ciudad->id);
    }

    public function eliminar($id)
    {
        // Connsultar el objeto
        $ciudad = Ciudad::find($id);

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $ciudad->id,
            'nombre_recurso' => Ciudad::class,
            'descripcion_recurso' => $ciudad->nombre,
            'accion' => AccionAuditoriaEnum::ELIMINAR,
            'recurso_original' => $ciudad->toJson()
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $ciudad->delete();
    }

    public function obtenerColeccion($dto)
    {
        $data = [];
        $ciudades = Ciudad::obtenerColeccion($dto);
        foreach ($ciudades ?? [] as $shipping){
            array_push($data, $shipping);
        }

        $cantidadCiudades = count($ciudades);
        $to = isset($ciudades) && $cantidadCiudades > 0 ? $ciudades->currentPage() * $ciudades->perPage() : null;
        $to = isset($to) && isset($ciudades) && $to > $ciudades->total() && $cantidadCiudades> 0 ? $ciudades->total() : $to;
        $from = isset($to) && isset($ciudades) && $cantidadCiudades > 0 ?
            $ciudades->perPage() > $to ? 1 : ($to - $ciudades->perPage()) + 1
            : null;
        return [
            'datos' => $data,
            'desde' => $from,
            'hasta' => $to,
            'por_pagina' => isset($ciudades) && $cantidadCiudades > 0 ? +$ciudades->perPage() : 0,
            'pagina_actual' => isset($ciudades) && $cantidadCiudades > 0 ? $ciudades->currentPage() : 1,
            'ultima_pagina' => isset($ciudades) && $cantidadCiudades > 0 ? $ciudades->lastPage() : 0,
            'total' => isset($ciudades) && $cantidadCiudades > 0 ? $ciudades->total() : 0
        ];
    }

    public function obtenerColeccionLigera($dto)
    {
        $user = Auth::user();
        $empresa = $user->empresa();
        $dto['empresa_id'] = $empresa->id;
        $ciudad = Ciudad::obtenerColeccionLigera($dto);
        return $ciudad;
    }
}
