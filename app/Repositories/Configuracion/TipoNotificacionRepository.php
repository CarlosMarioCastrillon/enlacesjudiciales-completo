<?php


namespace App\Repositories\Configuracion;


use App\Contracts\Configuracion\TipoNotificacionService;
use App\Contracts\Seguridad\AuditoriaMaestroService;
use App\Enum\AccionAuditoriaEnum;
use App\Exceptions\ModelException;
use App\Model\Configuracion\TipoNotificacion;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class TipoNotificacionRepository implements TipoNotificacionService
{

    protected  $auditoriaMaestroService;

    public function __construct(AuditoriaMaestroService $auditoriaMaestroService){
        $this->auditoriaMaestroService = $auditoriaMaestroService;
    }

    public function cargar($id)
    {
        $tipoNotificacion = TipoNotificacion::find($id);

        return [
            'id' => $tipoNotificacion->id,
            'nombre' => $tipoNotificacion->nombre,
            'estado' => $tipoNotificacion->estado,
            'usuario_creacion_id' => $tipoNotificacion->usuario_creacion_id,
            'usuario_creacion_nombre' => $tipoNotificacion->usuario_creacion_nombre,
            'usuario_modificacion_id' => $tipoNotificacion->usuario_modificacion_id,
            'usuario_modificacion_nombre' => $tipoNotificacion->usuario_modificacion_nombre,
            'fecha_creacion' => (new Carbon($tipoNotificacion->created_at))->format("Y-m-d H:i:s"),
            'fecha_modificacion' => (new Carbon($tipoNotificacion->updated_at))->format("Y-m-d H:i:s")
        ];
    }

    public function modificarOCrear($dto)
    {
        $user = Auth::user();
        $usuario = $user->usuario();
        if (!isset($dto['id'])) {
            $dto['usuario_creacion_id'] = $usuario->id ?? ($dto['usuario_creacion_id'] ?? null);
            $dto['usuario_creacion_nombre'] = $usuario->nombre ?? ($dto['usuario_creacion_nombre'] ?? null);
        }
        if (isset($usuario) || isset($dto['usuario_modificacion_id'])) {
            $dto['usuario_modificacion_id'] = $usuario->id ?? ($dto['usuario_modificacion_id'] ?? null);
            $dto['usuario_modificacion_nombre'] = $usuario->nombre ?? ($dto['usuario_modificacion_nombre'] ?? null);
        }

        // Consultar el tipo de notificacion
        $tipoNotificacion = isset($dto['id']) ? TipoNotificacion::find($dto['id']) : new TipoNotificacion();

        // Guardar objeto original para auditoria
        $tipoNotificacionOriginal = $tipoNotificacion->toJson();

        $tipoNotificacion->fill($dto);
        $guardado = $tipoNotificacion->save();
        if(!$guardado){
            throw new ModelException("Ocurrió un error al intentar guardar el tipo de notificación.", $tipoNotificacion);
        }

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $tipoNotificacion->id,
            'nombre_recurso' => TipoNotificacion::class,
            'descripcion_recurso' => $tipoNotificacion->nombre,
            'accion' => isset($dto['id']) ? AccionAuditoriaEnum::MODIFICAR : AccionAuditoriaEnum::CREAR,
            'recurso_original' => isset($dto['id']) ? $tipoNotificacionOriginal : $tipoNotificacion->toJson(),
            'recurso_resultante' => isset($dto['id']) ? $tipoNotificacion->toJson() : null
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $this->cargar($tipoNotificacion->id);
    }

    public function eliminar($id)
    {
        // Connsultar el objeto
        $tipoNotificacion = TipoNotificacion::find($id);

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $tipoNotificacion->id,
            'nombre_recurso' => TipoNotificacion::class,
            'descripcion_recurso' => $tipoNotificacion->nombre,
            'accion' => AccionAuditoriaEnum::ELIMINAR,
            'recurso_original' => $tipoNotificacion->toJson()
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $tipoNotificacion->delete();
    }

    public function obtenerColeccion($dto)
    {
        $data = [];
        $tiposNotificaciones = TipoNotificacion::obtenerColeccion($dto);
        foreach ($tiposNotificaciones ?? [] as $shipping){
            array_push($data, $shipping);
        }

        $cantidadtiposNotificaciones = count($tiposNotificaciones);
        $to = isset($tiposNotificaciones) && $cantidadtiposNotificaciones > 0 ? $tiposNotificaciones->currentPage() * $tiposNotificaciones->perPage() : null;
        $to = isset($to) && isset($tiposNotificaciones) && $to > $tiposNotificaciones->total() && $cantidadtiposNotificaciones > 0 ? $tiposNotificaciones->total() : $to;
        $from = isset($to) && isset($tiposNotificaciones) && $cantidadtiposNotificaciones > 0 ?
            $tiposNotificaciones->perPage() > $to ? 1 : ($to - $tiposNotificaciones->perPage()) + 1
            : null;
        return [
            'datos' => $data,
            'desde' => $from,
            'hasta' => $to,
            'por_pagina' => isset($tiposNotificaciones) && $cantidadtiposNotificaciones > 0 ? +$tiposNotificaciones->perPage() : 0,
            'pagina_actual' => isset($tiposNotificaciones) && $cantidadtiposNotificaciones > 0 ? $tiposNotificaciones->currentPage() : 1,
            'ultima_pagina' => isset($tiposNotificaciones) && $cantidadtiposNotificaciones > 0 ? $tiposNotificaciones->lastPage() : 0,
            'total' => isset($tiposNotificaciones) && $cantidadtiposNotificaciones > 0 ? $tiposNotificaciones->total() : 0
        ];
    }

    public function obtenerColeccionLigera($dto)
    {
        $tipoNotificacion = TipoNotificacion::obtenerColeccionLigera($dto);
        return $tipoNotificacion;
    }

}
