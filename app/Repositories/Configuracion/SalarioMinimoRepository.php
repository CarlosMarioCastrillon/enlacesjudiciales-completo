<?php


namespace App\Repositories\Configuracion;


use App\Contracts\Configuracion\SalarioMinimoService;
use App\Contracts\Seguridad\AuditoriaMaestroService;
use App\Enum\AccionAuditoriaEnum;
use App\Exceptions\ModelException;
use App\Model\Configuracion\SalarioMinimo;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class SalarioMinimoRepository implements SalarioMinimoService
{

    protected  $auditoriaMaestroService;

    public function __construct(AuditoriaMaestroService $auditoriaMaestroService){
        $this->auditoriaMaestroService = $auditoriaMaestroService;
    }

    public function cargar($id)
    {
        $salarioMinimo = SalarioMinimo::find($id);

        return [
            'id' => $salarioMinimo->id,
            'fecha' => $salarioMinimo->fecha,
            'valor_salario_min' => $salarioMinimo->valor_salario_min,
            'estado' => $salarioMinimo->estado,
            'usuario_creacion_id' => $salarioMinimo->usuario_creacion_id,
            'usuario_creacion_nombre' => $salarioMinimo->usuario_creacion_nombre,
            'usuario_modificacion_id' => $salarioMinimo->usuario_modificacion_id,
            'usuario_modificacion_nombre' => $salarioMinimo->usuario_modificacion_nombre,
            'fecha_creacion' => (new Carbon($salarioMinimo->created_at))->format("Y-m-d H:i:s"),
            'fecha_modificacion' => (new Carbon($salarioMinimo->updated_at))->format("Y-m-d H:i:s")
        ];
    }

    public function modificarOCrear($dto)
    {
        $user = Auth::user();
        $usuario = $user->usuario();
        if (!isset($dto['id'])) {
            $dto['usuario_creacion_id'] = $usuario->id ?? ($dto['usuario_creacion_id'] ?? null);
            $dto['usuario_creacion_nombre'] = $usuario->nombre ?? ($dto['usuario_creacion_nombre'] ?? null);
        }
        if (isset($usuario) || isset($dto['usuario_modificacion_id'])) {
            $dto['usuario_modificacion_id'] = $usuario->id ?? ($dto['usuario_modificacion_id'] ?? null);
            $dto['usuario_modificacion_nombre'] = $usuario->nombre ?? ($dto['usuario_modificacion_nombre'] ?? null);
        }

        // Consultar el servicio
        $salarioMinimo = isset($dto['id']) ? SalarioMinimo::find($dto['id']) : new SalarioMinimo();

        // Guardar objeto original para auditoria
        $salarioMinimoOriginal = $salarioMinimo->toJson();

        $salarioMinimo->fill($dto);
        $guardado = $salarioMinimo->save();
        if(!$guardado){
            throw new ModelException("Ocurrió un error al intentar guardar el servicio.", $salarioMinimo);
        }

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $salarioMinimo->id,
            'nombre_recurso' => SalarioMinimo::class,
            'descripcion_recurso' => $salarioMinimo->nombre,
            'accion' => isset($dto['id']) ? AccionAuditoriaEnum::MODIFICAR : AccionAuditoriaEnum::CREAR,
            'recurso_original' => isset($dto['id']) ? $salarioMinimoOriginal : $salarioMinimo->toJson(),
            'recurso_resultante' => isset($dto['id']) ? $salarioMinimo->toJson() : null
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $this->cargar($salarioMinimo->id);
    }

    public function eliminar($id)
    {
        // Connsultar el objeto
        $salarioMinimo = SalarioMinimo::find($id);

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $salarioMinimo->id,
            'nombre_recurso' => SalarioMinimo::class,
            'descripcion_recurso' => $salarioMinimo->nombre,
            'accion' => AccionAuditoriaEnum::ELIMINAR,
            'recurso_original' => $salarioMinimo->toJson()
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $salarioMinimo->delete();
    }

    public function obtenerColeccion($dto)
    {
        $data = [];
        $salariosMinimos = SalarioMinimo::obtenerColeccion($dto);
        foreach ($salariosMinimos ?? [] as $salarioMinimo){
            array_push($data, $salarioMinimo);
        }

        $cantidadSalariosMinimos = count($salariosMinimos);
        $to = isset($salariosMinimos) && $cantidadSalariosMinimos > 0 ? $salariosMinimos->currentPage() * $salariosMinimos->perPage() : null;
        $to = isset($to) && isset($salariosMinimos) && $to > $salariosMinimos->total() && $cantidadSalariosMinimos > 0 ? $salariosMinimos->total() : $to;
        $from = isset($to) && isset($salariosMinimos) && $cantidadSalariosMinimos > 0 ?
            $salariosMinimos->perPage() > $to ? 1 : ($to - $salariosMinimos->perPage()) + 1
            : null;
        return [
            'datos' => $data,
            'desde' => $from,
            'hasta' => $to,
            'por_pagina' => isset($salariosMinimos) && $cantidadSalariosMinimos > 0 ? +$salariosMinimos->perPage() : 0,
            'pagina_actual' => isset($salariosMinimos) && $cantidadSalariosMinimos > 0 ? $salariosMinimos->currentPage() : 1,
            'ultima_pagina' => isset($salariosMinimos) && $cantidadSalariosMinimos > 0 ? $salariosMinimos->lastPage() : 0,
            'total' => isset($salariosMinimos) && $cantidadSalariosMinimos > 0 ? $salariosMinimos->total() : 0
        ];
    }

    public function obtenerColeccionLigera($dto)
    {
        $salarioMinimo = SalarioMinimo::obtenerColeccionLigera($dto);
        return $salarioMinimo;
    }

}
