<?php


namespace App\Repositories\Configuracion;


use App\Contracts\Configuracion\EmpresaService;
use App\Contracts\Seguridad\AuditoriaMaestroService;
use App\Contracts\Seguridad\UsuarioService;
use App\Enum\AccionAuditoriaEnum;
use App\Exceptions\ModelException;
use App\Exports\EmpresaExport;
use App\Model\Administracion\ParametroConstante;
use App\Model\Configuracion\Area;
use App\Model\Configuracion\Cobertura;
use App\Model\Configuracion\Empresa;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class EmpresaRepository implements EmpresaService
{

    protected  $usuarioService;
    protected  $auditoriaMaestroService;

    public function __construct(UsuarioService $usuarioService, AuditoriaMaestroService $auditoriaMaestroService){
        $this->usuarioService = $usuarioService;
        $this->auditoriaMaestroService = $auditoriaMaestroService;
    }

    public function cargar($id)
    {
        $empresa = Empresa::find($id);
        $tipoDocumento = $empresa->tipoDocumento;
        $tipoEmpresa = $empresa->tipoEmpresa;
        $departamento = $empresa->departamento;
        $ciudad = $empresa->ciudad;
        $zona = $empresa->zona;
        $coberturas = $empresa->coberturas;
        $ciudadesEnCobertura = [];
        foreach ($coberturas ?? [] as $cobertura){
           array_push($ciudadesEnCobertura, $cobertura->ciudad_id);
        }

        return [
            'id' => $empresa->id,
            'numero_documento' => $empresa->numero_documento,
            'nombre' => $empresa->nombre,
            'direccion' => $empresa->direccion,
            'email' => $empresa->email,
            'telefono_uno' => $empresa->telefono_uno,
            'telefono_dos' => $empresa->telefono_dos,
            'telefono_tres' => $empresa->telefono_tres,
            'fecha_vencimiento' => (new Carbon($empresa->fecha_vencimiento))->format("Y-m-d H:i:s"),
            'actuaciones_digitales' => $empresa->con_actuaciones_digitales,
            'vigilancia' => $empresa->con_vigilancia,
            'dataprocesos' => $empresa->con_data_procesos,
            'observacion' => $empresa->observacion,
            'estado' => $empresa->estado,
            'usuario_creacion_id' => $empresa->usuario_creacion_id,
            'usuario_creacion_nombre' => $empresa->usuario_creacion_nombre,
            'usuario_modificacion_id' => $empresa->usuario_modificacion_id,
            'usuario_modificacion_nombre' => $empresa->usuario_modificacion_nombre,
            'fecha_creacion' => (new Carbon($empresa->created_at))->format("Y-m-d H:i:s"),
            'fecha_modificacion' => (new Carbon($empresa->updated_at))->format("Y-m-d H:i:s"),
            'tipo_documento' => isset($tipoDocumento) ? [
                'id' => $tipoDocumento->id,
                'codigo' => $tipoDocumento->codigo
            ] : null,
            'tipo_empresa' => isset($tipoEmpresa) ? [
                'id' => $tipoEmpresa->id,
                'nombre' => $tipoEmpresa->nombre
            ] : null,
            'departamento' => isset($departamento) ? [
                'id' => $departamento->id,
                'nombre' => $departamento->nombre
            ] : null,
            'ciudad' => isset($ciudad) ? [
                'id' => $ciudad->id,
                'nombre' => $ciudad->nombre
            ] : null,
            'zona' => isset($zona) ? [
                'id' => $zona->id,
                'nombre' => $zona->nombre
            ] : null,
            'ciudades_en_cobertura' => $ciudadesEnCobertura
        ];
    }

    public function modificarOCrear($dto)
    {
        // Cargar parametros
        $parametros = ParametroConstante::cargarParametros();

        // Cargar informacion de la empresa
        $user = Auth::user();
        $usuario = $user->usuario();
        if (!isset($dto['id'])) {
            $dto['usuario_creacion_id'] = $usuario->id ?? ($dto['usuario_creacion_id'] ?? null);
            $dto['usuario_creacion_nombre'] = $usuario->nombre ?? ($dto['usuario_creacion_nombre'] ?? null);
        }
        if (isset($usuario) || isset($dto['usuario_modificacion_id'])) {
            $dto['usuario_modificacion_id'] = $usuario->id ?? ($dto['usuario_modificacion_id'] ?? null);
            $dto['usuario_modificacion_nombre'] = $usuario->nombre ?? ($dto['usuario_modificacion_nombre'] ?? null);
        }

        // Consultar el empresa
        $empresa = isset($dto['id']) ? Empresa::find($dto['id']) : new Empresa();

        // Guardar objeto original para auditoria
        $empresaOriginal = $empresa->toJson();

        $empresa->fill($dto);
        $guardado = $empresa->save();
        if(!$guardado){
            throw new ModelException("Ocurrió un error al intentar guardar la empresa.", $empresa);
        }

        if (!isset($dto['id'])){
            // Crear area por defecto
            $area = new Area();
            $area->nombre = $parametros['AREA_EMPRESA_POR_DEFECTO'];
            $area->empresa_id = $empresa->id;
            $area->usuario_id = 1;
            $area->estado = 1;
            $area->usuario_creacion_id = $dto['usuario_creacion_id'];
            $area->usuario_creacion_nombre = $dto['usuario_creacion_nombre'];
            $area->usuario_modificacion_id = $dto['usuario_modificacion_id'];
            $area->usuario_modificacion_nombre = $dto['usuario_modificacion_nombre'];
            $area->save();
            // Guardar auditoria área
            $auditoriaDto = array(
                'id_recurso' => $area->id,
                'nombre_recurso' => Area::class,
                'descripcion_recurso' => $area->nombre,
                'accion' => AccionAuditoriaEnum::CREAR,
                'recurso_original' => $area->toJson(),
                'recurso_resultante' => null
            );
            $this->auditoriaMaestroService->crear($auditoriaDto);

            // Crear usuario administrador
            $this->usuarioService->modificarOCrear([
                'documento' => $empresa->numero_documento,
                'nombre' => $empresa->nombre,
                'clave' => $dto['clave'] ?? "clave@temporal",
                'email_uno' => $empresa->email,
                'email_dos' => null,
                'fecha_vencimiento' =>
                    Carbon::now()->addDays($parametros['DIAS_VENCIMIENTO_USUARIO'] ?? 15)->format('Y-m-d'),
                'dependiente_principal' => false,
                'con_microportal' => false,
                'con_movimiento_proceso' => true,
                'con_montaje_actuacion' => true,
                'con_movimiento_proceso_dos' => false,
                'con_montaje_actuacion_dos' => false,
                'con_tarea' => true,
                'con_respuesta_tarea' => true,
                'con_ven_termino_tarea' => true,
                'con_ven_termino_actuacion_rama' => true,
                'con_ven_termino_actuacion_archivo' => true,
                'estado' => true,
                'tipo_documento_id' => $empresa->tipo_documento_id,
                'empresa_id' => $empresa->id,
                'rol_id' => $parametros['ADMINISTRADOR_EMPRESA_ROL_ID'],
                'departamento_id' => $empresa->departamento_id,
                'ciudad_id' => $empresa->ciudad_id,
                'area_id' => $area->id
            ]);
        }

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $empresa->id,
            'nombre_recurso' => Empresa::class,
            'descripcion_recurso' => $empresa->nombre,
            'accion' => isset($dto['id']) ? AccionAuditoriaEnum::MODIFICAR : AccionAuditoriaEnum::CREAR,
            'recurso_original' => isset($dto['id']) ? $empresaOriginal : $empresa->toJson(),
            'recurso_resultante' => isset($dto['id']) ? $empresa->toJson() : null
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $this->cargar($empresa->id);
    }

    public function modificarCobertura($dto){
        // Usuario para auditorias
        $user = Auth::user();
        $usuario = $user->usuario();

        // Obtener la empresa a modificar
        $empresa = Empresa::find($dto['id']);

        // Guardar objeto original para auditoria
        $empresaOriginal = $this->cargar($empresa->id);

        // Borrar las coberturas actuales del departamento en modificación
        $empresa->coberturas()
            ->join('ciudades', function ($join) use($dto) {
                $join->on('ciudades.id', '=', 'coberturas.ciudad_id')
                    ->where('ciudades.departamento_id', $dto['departamento_id']);
            })->delete();

        // Agragar las nuevas coberturas
        $coberturas = [];
        foreach ($dto['ciudad_ids'] as $ciudadId){
            array_push($coberturas, new Cobertura([
                'ciudad_id' => $ciudadId,
                'usuario_creacion_id' => $usuario->id ?? ($dto['usuario_creacion_id'] ?? null),
                'usuario_creacion_nombre' => $usuario->nombre ?? ($dto['usuario_creacion_nombre'] ?? null),
                'usuario_modificacion_id' => $usuario->id ?? ($dto['usuario_modificacion_id'] ?? null),
                'usuario_modificacion_nombre' => $usuario->nombre ?? ($dto['usuario_modificacion_nombre'] ?? null)
            ]));
        }

        // Guardar las nuevas coberturas del departamento
        if(count($coberturas) > 0){
            $nuevasCoberturas = $empresa->coberturas()->saveMany($coberturas);
            if(!(isset($nuevasCoberturas) && isset($nuevasCoberturas[0]->id))){
                throw new ModelException("No se guardaron las nuevas coberturas.", $coberturas);
            }
        }

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $empresa->id,
            'nombre_recurso' => isset($coberturas) ? Cobertura::class : Empresa::class,
            'descripcion_recurso' => $empresa->nombre,
            'accion' => AccionAuditoriaEnum::MODIFICAR,
            'recurso_original' => json_encode($empresaOriginal),
            'recurso_resultante' => json_encode($this->cargar($empresa->id))
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $this->cargar($empresa->id);
    }

    public function eliminar($id)
    {
        // Connsultar el objeto
        $empresa = Empresa::with([
            'usuarios',
            'coberturas',
            'clientes',
            'areas',
            'auxiliarJusticias',
            'conceptosEconomicos',
            'planes',
            'actuaciones',
            'diligencias',
            'diligenciasHistoria',
            'procesosCliente',
            'procesosJudiciales',
            'procesosValor',
            'procesoJudicialCargaErrores',
            'auxiliarJusticiaCargaErrores'
        ])->whereId($id)->first();

        // Validar eliminación
        if(
            count($empresa->usuarios ?? []) > 0 || count($empresa->coberturas ?? []) > 0 ||
            count($empresa->clientes ?? []) > 0 || count($empresa->areas ?? []) > 0 ||
            count($empresa->auxiliarJusticias ?? []) > 0 || count($empresa->conceptosEconomicos ?? []) > 0 ||
            count($empresa->planes ?? []) > 0 || count($empresa->actuaciones ?? []) > 0 ||
            count($empresa->diligencias ?? []) > 0 || count($empresa->diligenciasHistoria ?? []) > 0 ||
            count($empresa->procesosCliente ?? []) > 0 || count($empresa->procesosJudiciales ?? []) > 0 ||
            count($empresa->procesosValor ?? []) > 0 || count($empresa->procesoJudicialCargaErrores ?? []) > 0 ||
            count($empresa->auxiliarJusticiaCargaErrores ?? []) > 0
        ){
            throw new ModelException("No puede eliminarse la empresa porque tiene información asociada.", $empresa);
        }

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $empresa->id,
            'nombre_recurso' => Empresa::class,
            'descripcion_recurso' => $empresa->nombre,
            'accion' => AccionAuditoriaEnum::ELIMINAR,
            'recurso_original' => $empresa->toJson()
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $empresa->delete();
    }

    public function obtenerColeccion($dto)
    {
        $data = [];
        $empresas = Empresa::obtenerColeccion($dto);
        foreach ($empresas ?? [] as $empresa){
            array_push($data, $empresa);
        }

        $cantidadEmpresas = count($empresas);
        $to = isset($empresas) && $cantidadEmpresas > 0 ? $empresas->currentPage() * $empresas->perPage() : null;
        $to = isset($to) && isset($empresas) && $to > $empresas->total() && $cantidadEmpresas> 0 ? $empresas->total() : $to;
        $from = isset($to) && isset($empresas) && $cantidadEmpresas > 0 ?
            $empresas->perPage() > $to ? 1 : ($to - $empresas->perPage()) + 1
            : null;
        return [
            'datos' => $data,
            'desde' => $from,
            'hasta' => $to,
            'por_pagina' => isset($empresas) && $cantidadEmpresas > 0 ? +$empresas->perPage() : 0,
            'pagina_actual' => isset($empresas) && $cantidadEmpresas > 0 ? $empresas->currentPage() : 1,
            'ultima_pagina' => isset($empresas) && $cantidadEmpresas > 0 ? $empresas->lastPage() : 0,
            'total' => isset($empresas) && $cantidadEmpresas > 0 ? $empresas->total() : 0
        ];
    }

    public function obtenerColeccionLigera($dto)
    {
        return Empresa::obtenerColeccionLigera($dto);
    }

    public function getExcel($dto){
        return (new EmpresaExport($dto));
    }
}
