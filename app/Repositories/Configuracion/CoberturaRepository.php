<?php


namespace App\Repositories\Configuracion;


use App\Contracts\Configuracion\CoberturaService;
use App\Contracts\Seguridad\AuditoriaMaestroService;
use App\Enum\AccionAuditoriaEnum;
use App\Exceptions\ModelException;
use App\Model\Configuracion\Cobertura;
use App\Model\Configuracion\Empresa;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class CoberturaRepository implements CoberturaService
{

    protected  $auditoriaMaestroService;

    public function __construct(AuditoriaMaestroService $auditoriaMaestroService){
        $this->auditoriaMaestroService = $auditoriaMaestroService;
    }

    public function cargar($dto)
    {
        // TODO Borrar en Repositry, Service, Controller, URLs
    }

    public function modificarOCrear($dto)
    {
        $user = Auth::user();
        $usuario = $user->usuario();
        if (!isset($dto['id'])) {
            $dto['usuario_creacion_id'] = $usuario->id ?? ($dto['usuario_creacion_id'] ?? null);
            $dto['usuario_creacion_nombre'] = $usuario->nombre ?? ($dto['usuario_creacion_nombre'] ?? null);
        }
        if (isset($usuario) || isset($dto['usuario_modificacion_id'])) {
            $dto['usuario_modificacion_id'] = $usuario->id ?? ($dto['usuario_modificacion_id'] ?? null);
            $dto['usuario_modificacion_nombre'] = $usuario->nombre ?? ($dto['usuario_modificacion_nombre'] ?? null);
        }

        // Consultar el cobertura
        $cobertura = isset($dto['id']) ? Cobertura::find($dto['id']) : new Cobertura();

        // Guardar objeto original para auditoria
        $coberturaOriginal = $cobertura->toJson();

        $cobertura->fill($dto);
        $guardado = $cobertura->save();
        if(!$guardado){
            throw new ModelException("Ocurrió un error al intentar guardar el cobertura.", $cobertura);
        }

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $cobertura->id,
            'nombre_recurso' => Cobertura::class,
            'descripcion_recurso' => $cobertura->empresa_id,
            'accion' => isset($dto['id']) ? AccionAuditoriaEnum::MODIFICAR : AccionAuditoriaEnum::CREAR,
            'recurso_original' => isset($dto['id']) ? $coberturaOriginal : $cobertura->toJson(),
            'recurso_resultante' => isset($dto['id']) ? $cobertura->toJson() : null
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $this->cargar($cobertura->id);
    }

    public function eliminar($id)
    {
        // Connsultar el objeto
        $cobertura = Cobertura::find($id);

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $cobertura->id,
            'nombre_recurso' => Cobertura::class,
            'descripcion_recurso' =>  $cobertura->empresa_id,
            'accion' => AccionAuditoriaEnum::ELIMINAR,
            'recurso_original' => $cobertura->toJson()
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $cobertura->delete();
    }

    public function obtenerColeccion($dto)
    {
        $data = [];
        $coberturas = Cobertura::obtenerColeccion($dto);
        foreach ($coberturas ?? [] as $shipping){
            array_push($data, $shipping);
        }

        $cantidadCoberturas = count($coberturas);
        $to = isset($coberturas) && $cantidadCoberturas > 0 ? $coberturas->currentPage() * $coberturas->perPage() : null;
        $to = isset($to) && isset($coberturas) && $to > $coberturas->total() && $cantidadCoberturas> 0 ? $coberturas->total() : $to;
        $from = isset($to) && isset($coberturas) && $cantidadCoberturas > 0 ?
            $coberturas->perPage() > $to ? 1 : ($to - $coberturas->perPage()) + 1
            : null;
        return [
            'datos' => $data,
            'desde' => $from,
            'hasta' => $to,
            'por_pagina' => isset($coberturas) && $cantidadCoberturas > 0 ? +$coberturas->perPage() : 0,
            'pagina_actual' => isset($coberturas) && $cantidadCoberturas > 0 ? $coberturas->currentPage() : 1,
            'ultima_pagina' => isset($coberturas) && $cantidadCoberturas > 0 ? $coberturas->lastPage() : 0,
            'total' => isset($coberturas) && $cantidadCoberturas > 0 ? $coberturas->total() : 0
        ];
    }
}
