<?php


namespace App\Repositories\Configuracion;


use App\Contracts\Configuracion\TipoDiligenciaService;
use App\Contracts\Seguridad\AuditoriaMaestroService;
use App\Enum\AccionAuditoriaEnum;
use App\Exceptions\ModelException;
use App\Model\Configuracion\TipoDiligencia;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class TipoDiligenciaRepository implements TipoDiligenciaService
{

    protected  $auditoriaMaestroService;

    public function __construct(AuditoriaMaestroService $auditoriaMaestroService){
        $this->auditoriaMaestroService = $auditoriaMaestroService;
    }

    public function cargar($id)
    {
        $tipoDiligencia = TipoDiligencia::find($id);

        return [
            'id' => $tipoDiligencia->id,
            'nombre' => $tipoDiligencia->nombre,
            'solicitud_autorizacion' => $tipoDiligencia->solicitud_autorizacion,
            'estado' => $tipoDiligencia->estado,
            'usuario_creacion_id' => $tipoDiligencia->usuario_creacion_id,
            'usuario_creacion_nombre' => $tipoDiligencia->usuario_creacion_nombre,
            'usuario_modificacion_id' => $tipoDiligencia->usuario_modificacion_id,
            'usuario_modificacion_nombre' => $tipoDiligencia->usuario_modificacion_nombre,
            'fecha_creacion' => (new Carbon())->parse($tipoDiligencia->created_at)->format("Y-m-d H:i:s"),
            'fecha_modificacion' => (new Carbon())->parse($tipoDiligencia->updated_at)->format("Y-m-d H:i:s")
        ];
    }

    public function modificarOCrear($dto)
    {
        $user = Auth::user();
        $usuario = $user->usuario();
        if (!isset($dto['id'])) {
            $dto['usuario_creacion_id'] = $usuario->id ?? ($dto['usuario_creacion_id'] ?? null);
            $dto['usuario_creacion_nombre'] = $usuario->nombre ?? ($dto['usuario_creacion_nombre'] ?? null);
        }
        if (isset($usuario) || isset($dto['usuario_modificacion_id'])) {
            $dto['usuario_modificacion_id'] = $usuario->id ?? ($dto['usuario_modificacion_id'] ?? null);
            $dto['usuario_modificacion_nombre'] = $usuario->nombre ?? ($dto['usuario_modificacion_nombre'] ?? null);
        }

        // Consultar el tipo de diligencia
        $tipoDiligencia = isset($dto['id']) ? TipoDiligencia::find($dto['id']) : new TipoDiligencia();

        // Guardar objeto original para auditoria
        $tipoDiligenciaOriginal = $tipoDiligencia->toJson();

        $tipoDiligencia->fill($dto);
        $guardado = $tipoDiligencia->save();
        if(!$guardado){
            throw new ModelException("Ocurrió un error al intentar guardar el tipo de diligencia.", $tipoDiligencia);
        }

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $tipoDiligencia->id,
            'nombre_recurso' => TipoDiligencia::class,
            'descripcion_recurso' => $tipoDiligencia->nombre,
            'accion' => isset($dto['id']) ? AccionAuditoriaEnum::MODIFICAR : AccionAuditoriaEnum::CREAR,
            'recurso_original' => isset($dto['id']) ? $tipoDiligenciaOriginal : $tipoDiligencia->toJson(),
            'recurso_resultante' => isset($dto['id']) ? $tipoDiligencia->toJson() : null
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $this->cargar($tipoDiligencia->id);
    }

    public function eliminar($id)
    {
        // Connsultar el objeto
        $tipoDiligencia = TipoDiligencia::find($id);

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $tipoDiligencia->id,
            'nombre_recurso' => TipoDiligencia::class,
            'descripcion_recurso' => $tipoDiligencia->nombre,
            'accion' => AccionAuditoriaEnum::ELIMINAR,
            'recurso_original' => $tipoDiligencia->toJson()
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $tipoDiligencia->delete();
    }

    public function obtenerColeccion($dto)
    {
        $data = [];
        $tiposDiligencia = TipoDiligencia::obtenerColeccion($dto);
        foreach ($tiposDiligencia ?? [] as $shipping){
            array_push($data, $shipping);
        }

        $cantidadTiposDiligencia = count($tiposDiligencia);
        $to = isset($tiposDiligencia) && $cantidadTiposDiligencia > 0 ? $tiposDiligencia->currentPage() * $tiposDiligencia->perPage() : null;
        $to = isset($to) && isset($tiposDiligencia) && $to > $tiposDiligencia->total() && $cantidadTiposDiligencia > 0 ? $tiposDiligencia->total() : $to;
        $from = isset($to) && isset($tiposDiligencia) && $cantidadTiposDiligencia > 0 ?
            $tiposDiligencia->perPage() > $to ? 1 : ($to - $tiposDiligencia->perPage()) + 1
            : null;
        return [
            'datos' => $data,
            'desde' => $from,
            'hasta' => $to,
            'por_pagina' => isset($tiposDiligencia) && $cantidadTiposDiligencia > 0 ? +$tiposDiligencia->perPage() : 0,
            'pagina_actual' => isset($tiposDiligencia) && $cantidadTiposDiligencia > 0 ? $tiposDiligencia->currentPage() : 1,
            'ultima_pagina' => isset($tiposDiligencia) && $cantidadTiposDiligencia > 0 ? $tiposDiligencia->lastPage() : 0,
            'total' => isset($tiposDiligencia) && $cantidadTiposDiligencia > 0 ? $tiposDiligencia->total() : 0
        ];
    }

    public function obtenerColeccionLigera($dto)
    {
        $tipoDiligencia = TipoDiligencia::obtenerColeccionLigera($dto);
        return $tipoDiligencia;
    }
}
