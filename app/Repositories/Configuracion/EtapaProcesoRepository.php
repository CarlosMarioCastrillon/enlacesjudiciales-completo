<?php


namespace App\Repositories\Configuracion;


use App\Contracts\Configuracion\EtapaProcesoService;
use App\Contracts\Seguridad\AuditoriaMaestroService;
use App\Enum\AccionAuditoriaEnum;
use App\Exceptions\ModelException;
use App\Model\Configuracion\EtapaProceso;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class EtapaProcesoRepository implements EtapaProcesoService
{

    protected  $auditoriaMaestroService;

    public function __construct(AuditoriaMaestroService $auditoriaMaestroService){
        $this->auditoriaMaestroService = $auditoriaMaestroService;
    }

    public function cargar($id)
    {
        $etapaProceso = EtapaProceso::find($id);

        return [
            'id' => $etapaProceso->id,
            'nombre' => $etapaProceso->nombre,
            'estado' => $etapaProceso->estado,
            'usuario_creacion_id' => $etapaProceso->usuario_creacion_id,
            'usuario_creacion_nombre' => $etapaProceso->usuario_creacion_nombre,
            'usuario_modificacion_id' => $etapaProceso->usuario_modificacion_id,
            'usuario_modificacion_nombre' => $etapaProceso->usuario_modificacion_nombre,
            'fecha_creacion' => (new Carbon($etapaProceso->created_at))->format("Y-m-d H:i:s"),
            'fecha_modificacion' => (new Carbon($etapaProceso->updated_at))->format("Y-m-d H:i:s")
        ];
    }

    public function modificarOCrear($dto)
    {
        $user = Auth::user();
        $usuario = $user->usuario();
        if (!isset($dto['id'])) {
            $dto['usuario_creacion_id'] = $usuario->id ?? ($dto['usuario_creacion_id'] ?? null);
            $dto['usuario_creacion_nombre'] = $usuario->nombre ?? ($dto['usuario_creacion_nombre'] ?? null);
        }
        if (isset($usuario) || isset($dto['usuario_modificacion_id'])) {
            $dto['usuario_modificacion_id'] = $usuario->id ?? ($dto['usuario_modificacion_id'] ?? null);
            $dto['usuario_modificacion_nombre'] = $usuario->nombre ?? ($dto['usuario_modificacion_nombre'] ?? null);
        }

        // Consultar la etapa de proceso
        $etapaProceso = isset($dto['id']) ? EtapaProceso::find($dto['id']) : new EtapaProceso();

        // Guardar objeto original para auditoria
        $etapaProcesoOriginal = $etapaProceso->toJson();

        $etapaProceso->fill($dto);
        $guardado = $etapaProceso->save();
        if(!$guardado){
            throw new ModelException("Ocurrió un error al intentar guardar la etapa de proceso.", $etapaProceso);
        }

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $etapaProceso->id,
            'nombre_recurso' => EtapaProceso::class,
            'descripcion_recurso' => $etapaProceso->nombre,
            'accion' => isset($dto['id']) ? AccionAuditoriaEnum::MODIFICAR : AccionAuditoriaEnum::CREAR,
            'recurso_original' => isset($dto['id']) ? $etapaProcesoOriginal : $etapaProceso->toJson(),
            'recurso_resultante' => isset($dto['id']) ? $etapaProceso->toJson() : null
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $this->cargar($etapaProceso->id);
    }

    public function eliminar($id)
    {
        // Connsultar el objeto
        $etapaProceso = EtapaProceso::find($id);

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $etapaProceso->id,
            'nombre_recurso' => EtapaProceso::class,
            'descripcion_recurso' => $etapaProceso->nombre,
            'accion' => AccionAuditoriaEnum::ELIMINAR,
            'recurso_original' => $etapaProceso->toJson()
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $etapaProceso->delete();
    }

    public function obtenerColeccion($dto)
    {
        $data = [];
        $etapasProcesos = EtapaProceso::obtenerColeccion($dto);
        foreach ($etapasProcesos ?? [] as $shipping){
            array_push($data, $shipping);
        }

        $cantidadetapasProcesos = count($etapasProcesos);
        $to = isset($etapasProcesos) && $cantidadetapasProcesos > 0 ? $etapasProcesos->currentPage() * $etapasProcesos->perPage() : null;
        $to = isset($to) && isset($etapasProcesos) && $to > $etapasProcesos->total() && $cantidadetapasProcesos> 0 ? $etapasProcesos->total() : $to;
        $from = isset($to) && isset($etapasProcesos) && $cantidadetapasProcesos > 0 ?
            $etapasProcesos->perPage() > $to ? 1 : ($to - $etapasProcesos->perPage()) + 1
            : null;
        return [
            'datos' => $data,
            'desde' => $from,
            'hasta' => $to,
            'por_pagina' => isset($etapasProcesos) && $cantidadetapasProcesos > 0 ? +$etapasProcesos->perPage() : 0,
            'pagina_actual' => isset($etapasProcesos) && $cantidadetapasProcesos > 0 ? $etapasProcesos->currentPage() : 1,
            'ultima_pagina' => isset($etapasProcesos) && $cantidadetapasProcesos > 0 ? $etapasProcesos->lastPage() : 0,
            'total' => isset($etapasProcesos) && $cantidadetapasProcesos > 0 ? $etapasProcesos->total() : 0
        ];
    }

    public function obtenerColeccionLigera($dto)
    {
        $etapaProceso = EtapaProceso::obtenerColeccionLigera($dto);
        return $etapaProceso;
    }
}
