<?php


namespace App\Repositories\Configuracion;


use App\Contracts\Configuracion\JuzgadoService;
use App\Contracts\Seguridad\AuditoriaMaestroService;
use App\Enum\AccionAuditoriaEnum;
use App\Exceptions\ModelException;
use App\Model\Configuracion\Ciudad;
use App\Model\Configuracion\Departamento;
use App\Model\Configuracion\Juzgado;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class JuzgadoRepository implements JuzgadoService
{

    protected  $auditoriaMaestroService;

    public function __construct(AuditoriaMaestroService $auditoriaMaestroService){
        $this->auditoriaMaestroService = $auditoriaMaestroService;
    }

    public function cargar($id)
    {
        $juzgado = Juzgado::find($id);
        $ciudad = $juzgado->ciudad;
        $departamento = $juzgado->departamento;


        return [
            'id' => $juzgado->id,
            'nombre' => $juzgado->nombre,
            'departamento_id' => $juzgado->departamento_id,
            'ciudad_id' => $juzgado->ciudad_id,
            'juzgado' => $juzgado->juzgado,
            'sala' => $juzgado->sala,
            'juzgado_rama' => $juzgado->juzgado_rama,
            'juzgado_actual' => $juzgado->juzgado_actual,
            'estado' => $juzgado->estado,
            'usuario_creacion_id' => $juzgado->usuario_creacion_id,
            'usuario_creacion_nombre' => $juzgado->usuario_creacion_nombre,
            'usuario_modificacion_id' => $juzgado->usuario_modificacion_id,
            'usuario_modificacion_nombre' => $juzgado->usuario_modificacion_nombre,
            'fecha_creacion' => (new Carbon($juzgado->created_at))->format("Y-m-d H:i:s"),
            'fecha_modificacion' => (new Carbon($juzgado->updated_at))->format("Y-m-d H:i:s"),
            'ciudad' => isset($ciudad) ? [
                    'id' => $ciudad->id,
                    'nombre' => $ciudad->nombre,
                    'codigo_ciudad' => $ciudad->codigo_dane,
             ] : null,
            'departamento' => isset($departamento) ? [
                    'id' => $departamento->id,
                    'nombre' => $departamento->nombre,
             ] : null
        ];
    }

    public function modificarOCrear($dto)
    {
        $user = Auth::user();
        $usuario = $user->usuario();
        if (!isset($dto['id'])) {
            $dto['usuario_creacion_id'] = $usuario->id ?? ($dto['usuario_creacion_id'] ?? null);
            $dto['usuario_creacion_nombre'] = $usuario->nombre ?? ($dto['usuario_creacion_nombre'] ?? null);
        }
        if (isset($usuario) || isset($dto['usuario_modificacion_id'])) {
            $dto['usuario_modificacion_id'] = $usuario->id ?? ($dto['usuario_modificacion_id'] ?? null);
            $dto['usuario_modificacion_nombre'] = $usuario->nombre ?? ($dto['usuario_modificacion_nombre'] ?? null);
        }

        // Consultar ciudad y departamento
        $ciudad = Ciudad::find($dto['ciudad_id']);

        // Validar que el juzgado no exista
        if (!isset($dto['id'])) {
            $juzgadoAux = Juzgado::where('departamento_id', $dto['departamento_id'])
                ->where('ciudad_id', $dto['ciudad_id'])
                ->where('juzgado', $dto['juzgado'])
                ->where('sala', $dto['sala'])
                ->first();
            if(isset($juzgadoAux)){
                throw new ModelException("El juzgado ya está registrado.");
            }
        }

        // Consultar el juzgado
        $juzgado = isset($dto['id']) ? Juzgado::find($dto['id']) : new Juzgado();

        //Constructor del código de la rama o juzgado
        $dto['juzgado_rama'] = $ciudad->codigo_dane . $dto['juzgado'] . $dto['sala'];

        // Guardar objeto original para auditoria
        $juzgadoOriginal = $juzgado->toJson();

        $juzgado->fill($dto);
        $guardado = $juzgado->save();
        if(!$guardado){
            throw new ModelException("Ocurrió un error al intentar guardar el juzgado.", $juzgado);
        }

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $juzgado->id,
            'nombre_recurso' => Juzgado::class,
            'descripcion_recurso' => $juzgado->nombre,
            'accion' => isset($dto['id']) ? AccionAuditoriaEnum::MODIFICAR : AccionAuditoriaEnum::CREAR,
            'recurso_original' => isset($dto['id']) ? $juzgadoOriginal : $juzgado->toJson(),
            'recurso_resultante' => isset($dto['id']) ? $juzgado->toJson() : null
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $this->cargar($juzgado->id);
    }

    public function eliminar($id)
    {
        // Connsultar el objeto
        $juzgado = Juzgado::find($id);

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $juzgado->id,
            'nombre_recurso' => Juzgado::class,
            'descripcion_recurso' => $juzgado->nombre,
            'accion' => AccionAuditoriaEnum::ELIMINAR,
            'recurso_original' => $juzgado->toJson()
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $juzgado->delete();
    }

    public function obtenerColeccion($dto)
    {
        $data = [];
        $juzgados = Juzgado::obtenerColeccion($dto);
        foreach ($juzgados ?? [] as $shipping){
            array_push($data, $shipping);
        }

        $cantidadJuzgados = count($juzgados);
        $to = isset($juzgados) && $cantidadJuzgados > 0 ? $juzgados->currentPage() * $juzgados->perPage() : null;
        $to = isset($to) && isset($juzgados) && $to > $juzgados->total() && $cantidadJuzgados> 0 ? $juzgados->total() : $to;
        $from = isset($to) && isset($juzgados) && $cantidadJuzgados > 0 ?
            $juzgados->perPage() > $to ? 1 : ($to - $juzgados->perPage()) + 1
            : null;
        return [
            'datos' => $data,
            'desde' => $from,
            'hasta' => $to,
            'por_pagina' => isset($juzgados) && $cantidadJuzgados > 0 ? +$juzgados->perPage() : 0,
            'pagina_actual' => isset($juzgados) && $cantidadJuzgados > 0 ? $juzgados->currentPage() : 1,
            'ultima_pagina' => isset($juzgados) && $cantidadJuzgados > 0 ? $juzgados->lastPage() : 0,
            'total' => isset($juzgados) && $cantidadJuzgados > 0 ? $juzgados->total() : 0
        ];
    }

    public function obtenerColeccionLigera($dto)
    {
        $juzgado = Juzgado::obtenerColeccionLigera($dto);
        return $juzgado;
    }

    public function obtenerColeccionReferencia($dto)
    {
        $juzgado = Juzgado::obtenerColeccionReferencia($dto);
        return $juzgado;
    }
}
