<?php


namespace App\Repositories\Configuracion;


use App\Contracts\Configuracion\ZonaService;
use App\Contracts\Seguridad\AuditoriaMaestroService;
use App\Enum\AccionAuditoriaEnum;
use App\Exceptions\ModelException;
use App\Model\Configuracion\Zona;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class ZonaRepository implements ZonaService
{

    protected  $auditoriaMaestroService;

    public function __construct(AuditoriaMaestroService $auditoriaMaestroService){
        $this->auditoriaMaestroService = $auditoriaMaestroService;
    }

    public function cargar($id)
    {
        $zona = Zona::find($id);

        return [
            'id' => $zona->id,
            'nombre' => $zona->nombre,
            'coordinador_vigilancia' => $zona->coordinador_vigilancia,
            'estado' => $zona->estado,
            'usuario_creacion_id' => $zona->usuario_creacion_id,
            'usuario_creacion_nombre' => $zona->usuario_creacion_nombre,
            'usuario_modificacion_id' => $zona->usuario_modificacion_id,
            'usuario_modificacion_nombre' => $zona->usuario_modificacion_nombre,
            'fecha_creacion' => (new Carbon($zona->created_at))->format("Y-m-d H:i:s"),
            'fecha_modificacion' => (new Carbon($zona->updated_at))->format("Y-m-d H:i:s")
        ];
    }

    public function modificarOCrear($dto)
    {
        $user = Auth::user();
        $usuario = $user->usuario();
        if (!isset($dto['id'])) {
            $dto['usuario_creacion_id'] = $usuario->id ?? ($dto['usuario_creacion_id'] ?? null);
            $dto['usuario_creacion_nombre'] = $usuario->nombre ?? ($dto['usuario_creacion_nombre'] ?? null);
        }
        if (isset($usuario) || isset($dto['usuario_modificacion_id'])) {
            $dto['usuario_modificacion_id'] = $usuario->id ?? ($dto['usuario_modificacion_id'] ?? null);
            $dto['usuario_modificacion_nombre'] = $usuario->nombre ?? ($dto['usuario_modificacion_nombre'] ?? null);
        }

        // Consultar la zona
        $zona = isset($dto['id']) ? Zona::find($dto['id']) : new zona();

        // Guardar objeto original para auditoria
        $zonaOriginal = $zona->toJson();

        $zona->fill($dto);
        $guardado = $zona->save();
        if(!$guardado){
            throw new ModelException("Ocurrió un error al intentar guardar la zona.", $zona);
        }

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $zona->id,
            'nombre_recurso' => Zona::class,
            'descripcion_recurso' => $zona->nombre,
            'accion' => isset($dto['id']) ? AccionAuditoriaEnum::MODIFICAR : AccionAuditoriaEnum::CREAR,
            'recurso_original' => isset($dto['id']) ? $zonaOriginal : $zona->toJson(),
            'recurso_resultante' => isset($dto['id']) ? $zona->toJson() : null
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $this->cargar($zona->id);
    }

    public function eliminar($id)
    {
        // Connsultar el objeto
        $zona = Zona::find($id);

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $zona->id,
            'nombre_recurso' => Zona::class,
            'descripcion_recurso' => $zona->nombre,
            'accion' => AccionAuditoriaEnum::ELIMINAR,
            'recurso_original' => $zona->toJson()
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $zona->delete();
    }

    public function obtenerColeccion($dto)
    {
        $data = [];
        $zonas = Zona::obtenerColeccion($dto);
        foreach ($zonas ?? [] as $shipping){
            array_push($data, $shipping);
        }

        $cantidadZonas = count($zonas);
        $to = isset($zonas) && $cantidadZonas > 0 ? $zonas->currentPage() * $zonas->perPage() : null;
        $to = isset($to) && isset($zonas) && $to > $zonas->total() && $cantidadZonas> 0 ? $zonas->total() : $to;
        $from = isset($to) && isset($zonas) && $cantidadZonas > 0 ?
            $zonas->perPage() > $to ? 1 : ($to - $zonas->perPage()) + 1
            : null;
        return [
            'datos' => $data,
            'desde' => $from,
            'hasta' => $to,
            'por_pagina' => isset($zonas) && $cantidadZonas > 0 ? +$zonas->perPage() : 0,
            'pagina_actual' => isset($zonas) && $cantidadZonas > 0 ? $zonas->currentPage() : 1,
            'ultima_pagina' => isset($zonas) && $cantidadZonas > 0 ? $zonas->lastPage() : 0,
            'total' => isset($zonas) && $cantidadZonas > 0 ? $zonas->total() : 0
        ];
    }

    public function obtenerColeccionLigera($dto)
    {
        $zona = Zona::obtenerColeccionLigera($dto);
        return $zona;
    }

}
