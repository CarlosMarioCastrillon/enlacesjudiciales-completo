<?php


namespace App\Repositories\Configuracion;


use App\Contracts\Configuracion\DepartamentoService;
use App\Contracts\Seguridad\AuditoriaMaestroService;
use App\Enum\AccionAuditoriaEnum;
use App\Exceptions\ModelException;
use App\Model\Configuracion\Departamento;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class DepartamentoRepository implements DepartamentoService
{

    protected  $auditoriaMaestroService;

    public function __construct(AuditoriaMaestroService $auditoriaMaestroService){
        $this->auditoriaMaestroService = $auditoriaMaestroService;
    }

    public function cargar($id)
    {
        $departamento = Departamento::find($id);

        return [
            'id' => $departamento->id,
            'nombre' => $departamento->nombre,
            'codigo_dane' => $departamento->codigo_dane,
            'estado' => $departamento->estado,
            'usuario_creacion_id' => $departamento->usuario_creacion_id,
            'usuario_creacion_nombre' => $departamento->usuario_creacion_nombre,
            'usuario_modificacion_id' => $departamento->usuario_modificacion_id,
            'usuario_modificacion_nombre' => $departamento->usuario_modificacion_nombre,
            'fecha_creacion' => (new Carbon($departamento->created_at))->format("Y-m-d H:i:s"),
            'fecha_modificacion' => (new Carbon($departamento->updated_at))->format("Y-m-d H:i:s")
        ];
    }

    public function modificarOCrear($dto)
    {
        $user = Auth::user();
        $usuario = $user->usuario();
        if (!isset($dto['id'])) {
            $dto['usuario_creacion_id'] = $usuario->id ?? ($dto['usuario_creacion_id'] ?? null);
            $dto['usuario_creacion_nombre'] = $usuario->nombre ?? ($dto['usuario_creacion_nombre'] ?? null);
        }
        if (isset($usuario) || isset($dto['usuario_modificacion_id'])) {
            $dto['usuario_modificacion_id'] = $usuario->id ?? ($dto['usuario_modificacion_id'] ?? null);
            $dto['usuario_modificacion_nombre'] = $usuario->nombre ?? ($dto['usuario_modificacion_nombre'] ?? null);
        }

        // Consultar el departamento
        $departamento = isset($dto['id']) ? Departamento::find($dto['id']) : new Departamento();

        // Guardar objeto original para auditoria
        $departamentoOriginal = $departamento->toJson();

        $departamento->fill($dto);
        $guardado = $departamento->save();
        if(!$guardado){
            throw new ModelException("Ocurrió un error al intentar guardar el departamento.", $departamento);
        }

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $departamento->id,
            'nombre_recurso' => Departamento::class,
            'descripcion_recurso' => $departamento->nombre,
            'accion' => isset($dto['id']) ? AccionAuditoriaEnum::MODIFICAR : AccionAuditoriaEnum::CREAR,
            'recurso_original' => isset($dto['id']) ? $departamentoOriginal : $departamento->toJson(),
            'recurso_resultante' => isset($dto['id']) ? $departamento->toJson() : null
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $this->cargar($departamento->id);
    }

    public function eliminar($id)
    {
        // Connsultar el objeto
        $departamento = Departamento::find($id);

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $departamento->id,
            'nombre_recurso' => Departamento::class,
            'descripcion_recurso' => $departamento->nombre,
            'accion' => AccionAuditoriaEnum::ELIMINAR,
            'recurso_original' => $departamento->toJson()
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $departamento->delete();
    }

    public function obtenerColeccion($dto)
    {
        $data = [];
        $departamentos = Departamento::obtenerColeccion($dto);
        foreach ($departamentos ?? [] as $shipping){
            array_push($data, $shipping);
        }

        $cantidadDepartamentos = count($departamentos);
        $to = isset($departamentos) && $cantidadDepartamentos > 0 ? $departamentos->currentPage() * $departamentos->perPage() : null;
        $to = isset($to) && isset($departamentos) && $to > $departamentos->total() && $cantidadDepartamentos> 0 ? $departamentos->total() : $to;
        $from = isset($to) && isset($departamentos) && $cantidadDepartamentos > 0 ?
            $departamentos->perPage() > $to ? 1 : ($to - $departamentos->perPage()) + 1
            : null;
        return [
            'datos' => $data,
            'desde' => $from,
            'hasta' => $to,
            'por_pagina' => isset($departamentos) && $cantidadDepartamentos > 0 ? +$departamentos->perPage() : 0,
            'pagina_actual' => isset($departamentos) && $cantidadDepartamentos > 0 ? $departamentos->currentPage() : 1,
            'ultima_pagina' => isset($departamentos) && $cantidadDepartamentos > 0 ? $departamentos->lastPage() : 0,
            'total' => isset($departamentos) && $cantidadDepartamentos > 0 ? $departamentos->total() : 0
        ];
    }
    public function obtenerColeccionLigera($dto)
    {
        $departamento = Departamento::obtenerColeccionLigera($dto);
        return $departamento;
    }
}
