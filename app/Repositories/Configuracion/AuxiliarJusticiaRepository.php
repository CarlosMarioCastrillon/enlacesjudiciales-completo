<?php


namespace App\Repositories\Configuracion;


use App\Contracts\Configuracion\AuxiliarJusticiaService;
use App\Contracts\Seguridad\AuditoriaMaestroService;
use App\Enum\AccionAuditoriaEnum;
use App\Exceptions\ModelException;
use App\Imports\AuxiliarJusticiaImport;
use App\Model\Configuracion\AuxiliarJusticia;
use App\Model\Importacion\AuxiliarJusticiaCargaError;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class AuxiliarJusticiaRepository implements AuxiliarJusticiaService
{

    protected  $auditoriaMaestroService;

    public function __construct(AuditoriaMaestroService $auditoriaMaestroService){
        $this->auditoriaMaestroService = $auditoriaMaestroService;
    }

    public function cargar($id)
    {
        $auxiliarJusticia = AuxiliarJusticia::find($id);

        return [
            'id' => $auxiliarJusticia->id,
            'numero_documento' => $auxiliarJusticia->numero_documento,
            'nombres_auxiliar' => $auxiliarJusticia->nombres_auxiliar,
            'apellidos_auxiliar' => $auxiliarJusticia->apellidos_auxiliar,
            'nombre_ciudad' => $auxiliarJusticia->nombre_ciudad,
            'direccion_oficina' => $auxiliarJusticia->direccion_oficina,
            'telefono_oficina' => $auxiliarJusticia->telefono_oficina,
            'cargo' => $auxiliarJusticia->cargo,
            'telefono_celular' => $auxiliarJusticia->telefono_celular,
            'direccion_residencia' => $auxiliarJusticia->direccion_residencia,
            'telefono_residencia' => $auxiliarJusticia->telefono_residencia,
            'correo_electronico' => $auxiliarJusticia->correo_electronico,
            'estado' => $auxiliarJusticia->estado,
            'usuario_creacion_id' => $auxiliarJusticia->usuario_creacion_id,
            'usuario_creacion_nombre' => $auxiliarJusticia->usuario_creacion_nombre,
            'usuario_modificacion_id' => $auxiliarJusticia->usuario_modificacion_id,
            'usuario_modificacion_nombre' => $auxiliarJusticia->usuario_modificacion_nombre,
            'fecha_creacion' => (new Carbon($auxiliarJusticia->created_at))->format("Y-m-d H:i:s"),
            'fecha_modificacion' => (new Carbon($auxiliarJusticia->updated_at))->format("Y-m-d H:i:s")
        ];
    }

    public function modificarOCrear($dto)
    {
        $user = Auth::user();
        $usuario = $user->usuario();
        if (!isset($dto['id'])) {
            $dto['usuario_creacion_id'] = $usuario->id ?? ($dto['usuario_creacion_id'] ?? null);
            $dto['usuario_creacion_nombre'] = $usuario->nombre ?? ($dto['usuario_creacion_nombre'] ?? null);
        }
        if (isset($usuario) || isset($dto['usuario_modificacion_id'])) {
            $dto['usuario_modificacion_id'] = $usuario->id ?? ($dto['usuario_modificacion_id'] ?? null);
            $dto['usuario_modificacion_nombre'] = $usuario->nombre ?? ($dto['usuario_modificacion_nombre'] ?? null);
        }

        // Consultar el auxiliar de la justicia
        $auxiliarJusticia = isset($dto['id']) ? AuxiliarJusticia::find($dto['id']) : new AuxiliarJusticia();

        // Guardar objeto original para auditoria
        $auxiliarJusticiaOriginal = $auxiliarJusticia->toJson();

        $auxiliarJusticia->fill($dto);
        $auxiliarJusticia->empresa_id = $user->empresa()->id;
        $guardado = $auxiliarJusticia->save();
        if(!$guardado){
            throw new ModelException("Ocurrió un error al intentar guardar el auxiliar de la justicia.", $auxiliarJusticia);
        }

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $auxiliarJusticia->id,
            'nombre_recurso' => AuxiliarJusticia::class,
            'descripcion_recurso' => $auxiliarJusticia->nombre,
            'accion' => isset($dto['id']) ? AccionAuditoriaEnum::MODIFICAR : AccionAuditoriaEnum::CREAR,
            'recurso_original' => isset($dto['id']) ? $auxiliarJusticiaOriginal : $auxiliarJusticia->toJson(),
            'recurso_resultante' => isset($dto['id']) ? $auxiliarJusticia->toJson() : null
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $this->cargar($auxiliarJusticia->id);
    }

    public function importar($archivo){
        // Usuario en sesión
        $user = Auth::user();
        $usuario = $user->usuario();
        $empresa = $user->empresa();

        // Se borran los datos actuales por que la carga en no es incremental
        AuxiliarJusticia::query()->where('empresa_id', $empresa->id)->delete();

        // Borrar datos historicos
        AuxiliarJusticiaCargaError::query()->where('empresa_id', $empresa->id)->delete();

        $errores = [];
        $import = new AuxiliarJusticiaImport($empresa->id, $usuario->id, $usuario->nombre);
        Excel::import($import, $archivo);

        if($import->getWithErrors()){
            throw new ModelException("Revisar archivo de carga. Estructura de información no corresponde.");
        }

        foreach ($import->failures() as $failure) {
            array_push($errores, [
                "fila" => $failure->row(),
                "columna" => $failure->attribute(),
                "errores" => $failure->errors(),
                "datos" => $failure->values()
            ]);
        }

        $registrosFallidos = 0;
        $erroresReporte = [];
        $auxiliarJusticiaErrores = [];
        if(count($errores) > 0){
            $coleccionDeErrores = collect($errores)->groupBy('fila');
            foreach($coleccionDeErrores as $fila => $erroresFila){
                // Mapeo de atributos
                /*$mapeoDeColumnas =[
                    'CIUDAD' => 'nombre_ciudad',
                    'CARGO' => 'cargo',
                    'CEDULA' => 'numero_documento',
                    'APELLIDOS' => 'apellidos_auxiliar',
                    'NOMBRES' => 'nombres_auxiliar',
                    'DIR. OFICINA' => 'direccion_oficina',
                    'TELEFONO OFICINA' => 'telefono_oficina',
                    'DIRECCION RESIDENCIA' => 'direccion_residencia',
                    'TELEFONO RESIDENCIA' => 'telefono_residencia',
                    'CELULAR' => 'telefono_celular',
                    'E-MAIL' => 'correo_electronico'
                ];*/

                // Formatear observaciones
                //$columnas = [];
                $errorNumeral = 0;
                $observaciones = [];
                foreach ($erroresFila as $errorFila){
                    foreach ($errorFila['errores'] as $error){
                        $errorNumeral++;
                        array_push($observaciones, $errorNumeral . ". ". $error);
                    }
                    //array_push($columnas, $mapeoDeColumnas[$errorFila['columna']]);
                }

                // Datos de la fila con errores
                $datosFila = $erroresFila[0]["datos"];
                array_push($erroresReporte, [
                    'fila' => $fila,
                    //'columnas' => join(",", $columnas),
                    'nombre_ciudad' => $datosFila[0],
                    'cargo' => $datosFila[1],
                    'numero_documento' => $datosFila[2],
                    'apellidos_auxiliar' => $datosFila[3],
                    'nombres_auxiliar' => $datosFila[4],
                    'direccion_oficina' => $datosFila[5],
                    'telefono_oficina' => $datosFila[6],
                    'direccion_residencia' => $datosFila[7],
                    'telefono_residencia' => $datosFila[8],
                    'telefono_celular' => $datosFila[9],
                    'correo_electronico' => $datosFila[10],
                    'observacion' => join("<br>", $observaciones)
                ]);
                array_push($auxiliarJusticiaErrores, [
                    'nombre_ciudad' => $datosFila[0],
                    'cargo' => $datosFila[1],
                    'numero_documento' => $datosFila[2],
                    'apellidos_auxiliar' => $datosFila[3],
                    'nombres_auxiliar' => $datosFila[4],
                    'direccion_oficina' => $datosFila[5],
                    'telefono_oficina' => $datosFila[6],
                    'direccion_residencia' => $datosFila[7],
                    'telefono_residencia' => $datosFila[8],
                    'telefono_celular' => $datosFila[9],
                    'correo_electronico' => $datosFila[10],
                    'observacion' => join("\n", $observaciones),
                    'empresa_id' => $empresa->id,
                    'usuario_creacion_id' => $usuario->id,
                    'usuario_creacion_nombre' => $usuario->nombre,
                    'usuario_modificacion_id' => $usuario->id,
                    'usuario_modificacion_nombre' => $usuario->nombre
                ]);
            }

            $registrosFallidos = count($coleccionDeErrores ?? []);
        }

        if($auxiliarJusticiaErrores){
            // Insertar errores encontrados
            AuxiliarJusticiaCargaError::insert($auxiliarJusticiaErrores);
        }

        return [
            "errores" => $erroresReporte,
            "registros_fallidos" => $registrosFallidos,
            "registros_cargados" => $import->getImportedRows(),
            "registros_procesados" => $registrosFallidos + $import->getImportedRows()
        ];
    }

    public function eliminar($id)
    {
        // Connsultar el objeto
        $auxiliarJusticia = AuxiliarJusticia::find($id);

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $auxiliarJusticia->id,
            'nombre_recurso' => AuxiliarJusticia::class,
            'descripcion_recurso' => $auxiliarJusticia->nombre,
            'accion' => AccionAuditoriaEnum::ELIMINAR,
            'recurso_original' => $auxiliarJusticia->toJson()
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $auxiliarJusticia->delete();
    }

    public function obtenerColeccion($dto)
    {
        $data = [];
        $AuxiliaresJusticia = AuxiliarJusticia::obtenerColeccion($dto);
        foreach ($AuxiliaresJusticia ?? [] as $auxiliarJusticia){
            array_push($data, $auxiliarJusticia);
        }

        $cantidadAuxiliaresJusticia = count($AuxiliaresJusticia);
        $to = isset($AuxiliaresJusticia) && $cantidadAuxiliaresJusticia > 0 ? $AuxiliaresJusticia->currentPage() * $AuxiliaresJusticia->perPage() : null;
        $to = isset($to) && isset($AuxiliaresJusticia) && $to > $AuxiliaresJusticia->total() && $cantidadAuxiliaresJusticia > 0 ? $AuxiliaresJusticia->total() : $to;
        $from = isset($to) && isset($AuxiliaresJusticia) && $cantidadAuxiliaresJusticia > 0 ?
            $AuxiliaresJusticia->perPage() > $to ? 1 : ($to - $AuxiliaresJusticia->perPage()) + 1
            : null;
        return [
            'datos' => $data,
            'desde' => $from,
            'hasta' => $to,
            'por_pagina' => isset($AuxiliaresJusticia) && $cantidadAuxiliaresJusticia > 0 ? +$AuxiliaresJusticia->perPage() : 0,
            'pagina_actual' => isset($AuxiliaresJusticia) && $cantidadAuxiliaresJusticia > 0 ? $AuxiliaresJusticia->currentPage() : 1,
            'ultima_pagina' => isset($AuxiliaresJusticia) && $cantidadAuxiliaresJusticia > 0 ? $AuxiliaresJusticia->lastPage() : 0,
            'total' => isset($AuxiliaresJusticia) && $cantidadAuxiliaresJusticia > 0 ? $AuxiliaresJusticia->total() : 0
        ];
    }

    public function obtenerColeccionLigera($dto)
    {
        $auxiliarJusticia = AuxiliarJusticia::obtenerColeccionLigera($dto);
        return $auxiliarJusticia;
    }

}
