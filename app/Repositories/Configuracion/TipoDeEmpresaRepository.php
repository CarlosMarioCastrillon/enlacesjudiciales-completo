<?php


namespace App\Repositories\Configuracion;


use App\Contracts\Configuracion\TipoDeEmpresaService;
use App\Contracts\Seguridad\AuditoriaMaestroService;
use App\Enum\AccionAuditoriaEnum;
use App\Exceptions\ModelException;
use App\Model\Configuracion\TipoDeEmpresa;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class TipoDeEmpresaRepository implements TipoDeEmpresaService
{

    protected  $auditoriaMaestroService;

    public function __construct(AuditoriaMaestroService $auditoriaMaestroService){
        $this->auditoriaMaestroService = $auditoriaMaestroService;
    }

    public function cargar($id)
    {
        $tipoDeEmpresa = TipoDeEmpresa::find($id);

        return [
            'id' => $tipoDeEmpresa->id,
            'nombre' => $tipoDeEmpresa->nombre,
            'estado' => $tipoDeEmpresa->estado,
            'usuario_creacion_id' => $tipoDeEmpresa->usuario_creacion_id,
            'usuario_creacion_nombre' => $tipoDeEmpresa->usuario_creacion_nombre,
            'usuario_modificacion_id' => $tipoDeEmpresa->usuario_modificacion_id,
            'usuario_modificacion_nombre' => $tipoDeEmpresa->usuario_modificacion_nombre,
            'fecha_creacion' => (new Carbon($tipoDeEmpresa->created_at))->format("Y-m-d H:i:s"),
            'fecha_modificacion' => (new Carbon($tipoDeEmpresa->updated_at))->format("Y-m-d H:i:s")
        ];
    }

    public function modificarOCrear($dto)
    {
        $user = Auth::user();
        $usuario = $user->usuario();
        if (!isset($dto['id'])) {
            $dto['usuario_creacion_id'] = $usuario->id ?? ($dto['usuario_creacion_id'] ?? null);
            $dto['usuario_creacion_nombre'] = $usuario->nombre ?? ($dto['usuario_creacion_nombre'] ?? null);
        }
        if (isset($usuario) || isset($dto['usuario_modificacion_id'])) {
            $dto['usuario_modificacion_id'] = $usuario->id ?? ($dto['usuario_modificacion_id'] ?? null);
            $dto['usuario_modificacion_nombre'] = $usuario->nombre ?? ($dto['usuario_modificacion_nombre'] ?? null);
        }

        // Consultar el tipo de empresa
        $tipoDeEmpresa = isset($dto['id']) ? TipoDeEmpresa::find($dto['id']) : new TipoDeEmpresa();

        // Guardar objeto original para auditoria
        $tipoDeEmpresaOriginal = $tipoDeEmpresa->toJson();

        $tipoDeEmpresa->fill($dto);
        $guardado = $tipoDeEmpresa->save();
        if(!$guardado){
            throw new ModelException("Ocurrió un error al intentar guardar el tipo de empresa.", $tipoDeEmpresa);
        }

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $tipoDeEmpresa->id,
            'nombre_recurso' => TipoDeEmpresa::class,
            'descripcion_recurso' => $tipoDeEmpresa->nombre,
            'accion' => isset($dto['id']) ? AccionAuditoriaEnum::MODIFICAR : AccionAuditoriaEnum::CREAR,
            'recurso_original' => isset($dto['id']) ? $tipoDeEmpresaOriginal : $tipoDeEmpresa->toJson(),
            'recurso_resultante' => isset($dto['id']) ? $tipoDeEmpresa->toJson() : null
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $this->cargar($tipoDeEmpresa->id);
    }

    public function eliminar($id)
    {
        // Connsultar el objeto
        $tipoDeEmpresa = TipoDeEmpresa::find($id);

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $tipoDeEmpresa->id,
            'nombre_recurso' => TipoDeEmpresa::class,
            'descripcion_recurso' => $tipoDeEmpresa->nombre,
            'accion' => AccionAuditoriaEnum::ELIMINAR,
            'recurso_original' => $tipoDeEmpresa->toJson()
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $tipoDeEmpresa->delete();
    }

    public function obtenerColeccion($dto)
    {
        $data = [];
        $tiposDeEmpresa = TipoDeEmpresa::obtenerColeccion($dto);
        foreach ($tiposDeEmpresa ?? [] as $shipping){
            array_push($data, $shipping);
        }

        $cantidadTiposDeEmpresa = count($tiposDeEmpresa);
        $to = isset($tiposDeEmpresa) && $cantidadTiposDeEmpresa > 0 ? $tiposDeEmpresa->currentPage() * $tiposDeEmpresa->perPage() : null;
        $to = isset($to) && isset($tiposDeEmpresa) && $to > $tiposDeEmpresa->total() && $cantidadTiposDeEmpresa > 0 ? $tiposDeEmpresa->total() : $to;
        $from = isset($to) && isset($tiposDeEmpresa) && $cantidadTiposDeEmpresa > 0 ?
            $tiposDeEmpresa->perPage() > $to ? 1 : ($to - $tiposDeEmpresa->perPage()) + 1
            : null;
        return [
            'datos' => $data,
            'desde' => $from,
            'hasta' => $to,
            'por_pagina' => isset($tiposDeEmpresa) && $cantidadTiposDeEmpresa > 0 ? +$tiposDeEmpresa->perPage() : 0,
            'pagina_actual' => isset($tiposDeEmpresa) && $cantidadTiposDeEmpresa > 0 ? $tiposDeEmpresa->currentPage() : 1,
            'ultima_pagina' => isset($tiposDeEmpresa) && $cantidadTiposDeEmpresa > 0 ? $tiposDeEmpresa->lastPage() : 0,
            'total' => isset($tiposDeEmpresa) && $cantidadTiposDeEmpresa > 0 ? $tiposDeEmpresa->total() : 0
        ];
    }

    public function obtenerColeccionLigera($dto)
    {
        $tipoDeEmpresa = TipoDeEmpresa::obtenerColeccionLigera($dto);
        return $tipoDeEmpresa;
    }
}
