<?php


namespace App\Repositories\Configuracion;


use App\Contracts\Configuracion\DespachoService;
use App\Contracts\Seguridad\AuditoriaMaestroService;
use App\Enum\AccionAuditoriaEnum;
use App\Exceptions\ModelException;
use App\Model\Configuracion\Ciudad;
use App\Model\Configuracion\Departamento;
use App\Model\Configuracion\Despacho;
use App\Model\Configuracion\Juzgado;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class DespachoRepository implements DespachoService
{

    protected  $auditoriaMaestroService;

    public function __construct(AuditoriaMaestroService $auditoriaMaestroService){
        $this->auditoriaMaestroService = $auditoriaMaestroService;
    }

    public function cargar($id)
    {
        $despacho = Despacho::find($id);
        $ciudad = $despacho->ciudad;
        $departamento = $despacho->departamento;


        return [
            'id' => $despacho->id,
            'nombre' => $despacho->nombre,
            'departamento_id' => $despacho->departamento_id,
            'ciudad_id' => $despacho->ciudad_id,
            'juzgado' => $despacho->juzgado,
            'sala' => $despacho->sala,
            'despacho' => $despacho->despacho,
            'estado' => $despacho->estado,
            'usuario_creacion_id' => $despacho->usuario_creacion_id,
            'usuario_creacion_nombre' => $despacho->usuario_creacion_nombre,
            'usuario_modificacion_id' => $despacho->usuario_modificacion_id,
            'usuario_modificacion_nombre' => $despacho->usuario_modificacion_nombre,
            'fecha_creacion' => (new Carbon($despacho->created_at))->format("Y-m-d H:i:s"),
            'fecha_modificacion' => (new Carbon($despacho->updated_at))->format("Y-m-d H:i:s"),
            'ciudad' => isset($ciudad) ? [
                    'id' => $ciudad->id,
                    'nombre' => $ciudad->nombre,
                    'codigo_ciudad' => $ciudad->codigo_dane,
             ] : null,
            'departamento' => isset($departamento) ? [
                    'id' => $departamento->id,
                    'nombre' => $departamento->nombre,
             ] : null
        ];
    }

    public function modificarOCrear($dto)
    {
        $user = Auth::user();
        $usuario = $user->usuario();
        if (!isset($dto['id'])) {
            $dto['usuario_creacion_id'] = $usuario->id ?? ($dto['usuario_creacion_id'] ?? null);
            $dto['usuario_creacion_nombre'] = $usuario->nombre ?? ($dto['usuario_creacion_nombre'] ?? null);
        }
        if (isset($usuario) || isset($dto['usuario_modificacion_id'])) {
            $dto['usuario_modificacion_id'] = $usuario->id ?? ($dto['usuario_modificacion_id'] ?? null);
            $dto['usuario_modificacion_nombre'] = $usuario->nombre ?? ($dto['usuario_modificacion_nombre'] ?? null);
        }

        // Validamos que el juzgado exista
        $juzgado = Juzgado::where([
            ['departamento_id', '=', $dto['departamento_id']],
            ['ciudad_id', '=', $dto['ciudad_id']],
            ['juzgado', '=', $dto['juzgado']],
            ['sala', '=', $dto['sala']]
        ])->first();
        if(!isset($juzgado)){
            throw new ModelException("El juzgado ingresado no está registrado.", $dto);
        }

        // Validamos que no haya despachos con datos iguales
        if(!isset($dto['id'])) {
            $despacho = Despacho::where([
                ['departamento_id', '=', $dto['departamento_id']],
                ['ciudad_id', '=', $dto['ciudad_id']],
                ['juzgado', '=', $dto['juzgado']],
                ['sala', '=', $dto['sala']],
                ['despacho', '=', $dto['despacho']]
            ])->first();
            if(isset($despacho)){
                throw new ModelException("El despacho ingresado ya existe.", $despacho);
            }
        }

        // Consultar el despacho
        $despacho = isset($dto['id']) ? Despacho::find($dto['id']) : new despacho();

        // Guardar objeto original para auditoria
        $despachoOriginal = $despacho->toJson();

        $despacho->fill($dto);
        $guardado = $despacho->save();
        if(!$guardado){
            throw new ModelException("Ocurrió un error al intentar guardar el despacho.", $despacho);
        }

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $despacho->id,
            'nombre_recurso' => Despacho::class,
            'descripcion_recurso' => $despacho->nombre,
            'accion' => isset($dto['id']) ? AccionAuditoriaEnum::MODIFICAR : AccionAuditoriaEnum::CREAR,
            'recurso_original' => isset($dto['id']) ? $despachoOriginal : $despacho->toJson(),
            'recurso_resultante' => isset($dto['id']) ? $despacho->toJson() : null
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $this->cargar($despacho->id);
    }

    public function eliminar($id)
    {
        // Connsultar el objeto
        $despacho = Despacho::find($id);

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $despacho->id,
            'nombre_recurso' => Despacho::class,
            'descripcion_recurso' => $despacho->nombre,
            'accion' => AccionAuditoriaEnum::ELIMINAR,
            'recurso_original' => $despacho->toJson()
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $despacho->delete();
    }

    public function obtenerColeccion($dto)
    {
        $data = [];
          $despachos = Despacho::obtenerColeccion($dto);
        foreach ($despachos ?? [] as $shipping){
            array_push($data, $shipping);
        }

        $cantidadDespachos = count($despachos);
        $to = isset($despachos) && $cantidadDespachos > 0 ? $despachos->currentPage() * $despachos->perPage() : null;
        $to = isset($to) && isset($despachos) && $to > $despachos->total() && $cantidadDespachos> 0 ? $despachos->total() : $to;
        $from = isset($to) && isset($despachos) && $cantidadDespachos > 0 ?
            $despachos->perPage() > $to ? 1 : ($to - $despachos->perPage()) + 1
            : null;
        return [
            'datos' => $data,
            'desde' => $from,
            'hasta' => $to,
            'por_pagina' => isset($despachos) && $cantidadDespachos > 0 ? +$despachos->perPage() : 0,
            'pagina_actual' => isset($despachos) && $cantidadDespachos > 0 ? $despachos->currentPage() : 1,
            'ultima_pagina' => isset($despachos) && $cantidadDespachos > 0 ? $despachos->lastPage() : 0,
            'total' => isset($despachos) && $cantidadDespachos > 0 ? $despachos->total() : 0
        ];
    }

    public function obtenerColeccionLigera($dto)
    {
        $despacho = Despacho::obtenerColeccionLigera($dto);
        return $despacho;
    }

    public function obtenerColeccionReferencia($dto)
    {
        $despacho = Despacho::obtenerColeccionReferencia($dto);
        return $despacho;
    }
}
