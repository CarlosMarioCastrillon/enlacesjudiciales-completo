<?php


namespace App\Repositories\Configuracion;


use App\Contracts\Configuracion\AreaService;
use App\Contracts\Seguridad\AuditoriaMaestroService;
use App\Enum\AccionAuditoriaEnum;
use App\Exceptions\ModelException;
use App\Model\Configuracion\Area;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class AreaRepository implements AreaService
{

    protected  $auditoriaMaestroService;

    public function __construct(AuditoriaMaestroService $auditoriaMaestroService){
        $this->auditoriaMaestroService = $auditoriaMaestroService;
    }

    public function cargar($id)
    {
        if(isset($id)){
            $area = Area::find($id);
        }else{
            $user = Auth::user();
            $empresa = $user->empresa();
            $area = Area::find($empresa->id);
        }
        $usuario = $area->usuario;

        return [
            'id' => $area->id,
            'nombre' => $area->nombre,
            'empresa_id' => $area->empresa_id,
            'usuario_id' => $area->usuario_id,
            'estado' => $area->estado,
            'usuario_creacion_id' => $area->usuario_creacion_id,
            'usuario_creacion_nombre' => $area->usuario_creacion_nombre,
            'usuario_modificacion_id' => $area->usuario_modificacion_id,
            'usuario_modificacion_nombre' => $area->usuario_modificacion_nombre,
            'fecha_creacion' => (new Carbon($area->created_at))->format("Y-m-d H:i:s"),
            'fecha_modificacion' => (new Carbon($area->updated_at))->format("Y-m-d H:i:s"),
            'usuario' => isset($usuario) ? [
                'id' => $usuario->id,
                'nombre' => $usuario->nombre,
            ] : null
        ];
    }

    public function modificarOCrear($dto)
    {
        $user = Auth::user();
        $usuario = $user->usuario();
        if (!isset($dto['id'])) {
            $dto['usuario_creacion_id'] = $usuario->id ?? ($dto['usuario_creacion_id'] ?? null);
            $dto['usuario_creacion_nombre'] = $usuario->nombre ?? ($dto['usuario_creacion_nombre'] ?? null);
        }
        if (isset($usuario) || isset($dto['usuario_modificacion_id'])) {
            $dto['usuario_modificacion_id'] = $usuario->id ?? ($dto['usuario_modificacion_id'] ?? null);
            $dto['usuario_modificacion_nombre'] = $usuario->nombre ?? ($dto['usuario_modificacion_nombre'] ?? null);
        }

        // Consultar el area
        $area = isset($dto['id']) ? Area::find($dto['id']) : new Area();

        // Guardar objeto original para auditoria
        $areaOriginal = $area->toJson();

        $area->fill($dto);
        $guardado = $area->save();
        if(!$guardado){
            throw new ModelException("Ocurrió un error al intentar guardar el area.", $area);
        }

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $area->id,
            'nombre_recurso' => Area::class,
            'descripcion_recurso' => $area->nombre,
            'accion' => isset($dto['id']) ? AccionAuditoriaEnum::MODIFICAR : AccionAuditoriaEnum::CREAR,
            'recurso_original' => isset($dto['id']) ? $areaOriginal : $area->toJson(),
            'recurso_resultante' => isset($dto['id']) ? $area->toJson() : null
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $this->cargar($area->id);
    }

    public function eliminar($id)
    {
        // Connsultar el objeto
        $area = Area::find($id);

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $area->id,
            'nombre_recurso' => Area::class,
            'descripcion_recurso' => $area->nombre,
            'accion' => AccionAuditoriaEnum::ELIMINAR,
            'recurso_original' => $area->toJson()
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $area->delete();
    }

    public function obtenerColeccion($dto)
    {
        $data = [];
        $areas = Area::obtenerColeccion($dto);
        foreach ($areas ?? [] as $area){
            array_push($data, $area);
        }

        $cantidadAreas = count($areas);
        $to = isset($areas) && $cantidadAreas > 0 ? $areas->currentPage() * $areas->perPage() : null;
        $to = isset($to) && isset($areas) && $to > $areas->total() && $cantidadAreas> 0 ? $areas->total() : $to;
        $from = isset($to) && isset($areas) && $cantidadAreas > 0 ?
            $areas->perPage() > $to ? 1 : ($to - $areas->perPage()) + 1
            : null;
        return [
            'datos' => $data,
            'desde' => $from,
            'hasta' => $to,
            'por_pagina' => isset($areas) && $cantidadAreas > 0 ? +$areas->perPage() : 0,
            'pagina_actual' => isset($areas) && $cantidadAreas > 0 ? $areas->currentPage() : 1,
            'ultima_pagina' => isset($areas) && $cantidadAreas > 0 ? $areas->lastPage() : 0,
            'total' => isset($areas) && $cantidadAreas > 0 ? $areas->total() : 0
        ];
    }

    public function obtenerColeccionLigera($dto)
    {
        // Datos de sesión
        $user = Auth::user();
        $empresa = $user->empresa();
        $dto['empresa_id'] = $empresa->id;

        $area = Area::obtenerColeccionLigera($dto);
        return $area;
    }

    public function obtener($dto)
    {
        return Area::obtener($dto);
    }
}
