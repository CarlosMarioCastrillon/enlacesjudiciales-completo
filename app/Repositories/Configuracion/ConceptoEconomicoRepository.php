<?php


namespace App\Repositories\Configuracion;


use App\Contracts\Configuracion\ConceptoEconomicoService;
use App\Contracts\Seguridad\AuditoriaMaestroService;
use App\Enum\AccionAuditoriaEnum;
use App\Exceptions\ModelException;
use App\Model\Configuracion\ConceptoEconomico;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class ConceptoEconomicoRepository implements ConceptoEconomicoService
{

    protected  $auditoriaMaestroService;

    public function __construct(AuditoriaMaestroService $auditoriaMaestroService){
        $this->auditoriaMaestroService = $auditoriaMaestroService;
    }

    public function cargar($id)
    {
        $conceptoEconomico = ConceptoEconomico::find($id);

        return [
            'id' => $conceptoEconomico->id,
            'nombre' => $conceptoEconomico->nombre,
            'tipo_concepto' => $conceptoEconomico->tipo_concepto,
            'clase_concepto' => $conceptoEconomico->clase_concepto,
            'estado' => $conceptoEconomico->estado,
            'usuario_creacion_id' => $conceptoEconomico->usuario_creacion_id,
            'usuario_creacion_nombre' => $conceptoEconomico->usuario_creacion_nombre,
            'usuario_modificacion_id' => $conceptoEconomico->usuario_modificacion_id,
            'usuario_modificacion_nombre' => $conceptoEconomico->usuario_modificacion_nombre,
            'fecha_creacion' => (new Carbon($conceptoEconomico->created_at))->format("Y-m-d H:i:s"),
            'fecha_modificacion' => (new Carbon($conceptoEconomico->updated_at))->format("Y-m-d H:i:s")
        ];
    }

    public function modificarOCrear($dto)
    {
        $user = Auth::user();
        $usuario = $user->usuario();
        if (!isset($dto['id'])) {
            $dto['usuario_creacion_id'] = $usuario->id ?? ($dto['usuario_creacion_id'] ?? null);
            $dto['usuario_creacion_nombre'] = $usuario->nombre ?? ($dto['usuario_creacion_nombre'] ?? null);
        }
        if (isset($usuario) || isset($dto['usuario_modificacion_id'])) {
            $dto['usuario_modificacion_id'] = $usuario->id ?? ($dto['usuario_modificacion_id'] ?? null);
            $dto['usuario_modificacion_nombre'] = $usuario->nombre ?? ($dto['usuario_modificacion_nombre'] ?? null);
        }

        // Consultar el concepto económico
        $conceptoEconomico = isset($dto['id']) ? ConceptoEconomico::find($dto['id']) : new ConceptoEconomico();

        // Guardar objeto original para auditoria
        $conceptoEconomicoOriginal = $conceptoEconomico->toJson();

        $conceptoEconomico->fill($dto);
        $conceptoEconomico->empresa_id = $user->empresa()->id;
        $guardado = $conceptoEconomico->save();
        if(!$guardado){
            throw new ModelException("Ocurrió un error al intentar guardar el concepto económico.", $conceptoEconomico);
        }

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $conceptoEconomico->id,
            'nombre_recurso' => ConceptoEconomico::class,
            'descripcion_recurso' => $conceptoEconomico->nombre,
            'accion' => isset($dto['id']) ? AccionAuditoriaEnum::MODIFICAR : AccionAuditoriaEnum::CREAR,
            'recurso_original' => isset($dto['id']) ? $conceptoEconomicoOriginal : $conceptoEconomico->toJson(),
            'recurso_resultante' => isset($dto['id']) ? $conceptoEconomico->toJson() : null
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $this->cargar($conceptoEconomico->id);
    }

    public function eliminar($id)
    {
        // Connsultar el objeto
        $conceptoEconomico = ConceptoEconomico::find($id);

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $conceptoEconomico->id,
            'nombre_recurso' => ConceptoEconomico::class,
            'descripcion_recurso' => $conceptoEconomico->nombre,
            'accion' => AccionAuditoriaEnum::ELIMINAR,
            'recurso_original' => $conceptoEconomico->toJson()
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $conceptoEconomico->delete();
    }

    public function obtenerColeccion($dto)
    {
        $data = [];
        $conceptosEconomicos = ConceptoEconomico::obtenerColeccion($dto);
        foreach ($conceptosEconomicos ?? [] as $shipping){
            array_push($data, $shipping);
        }

        $cantidadConceptosEconomicos = count($conceptosEconomicos);
        $to = isset($conceptosEconomicos) && $cantidadConceptosEconomicos > 0 ? $conceptosEconomicos->currentPage() * $conceptosEconomicos->perPage() : null;
        $to = isset($to) && isset($conceptosEconomicos) && $to > $conceptosEconomicos->total() && $cantidadConceptosEconomicos > 0 ? $conceptosEconomicos->total() : $to;
        $from = isset($to) && isset($conceptosEconomicos) && $cantidadConceptosEconomicos > 0 ?
            $conceptosEconomicos->perPage() > $to ? 1 : ($to - $conceptosEconomicos->perPage()) + 1
            : null;
        return [
            'datos' => $data,
            'desde' => $from,
            'hasta' => $to,
            'por_pagina' => isset($conceptosEconomicos) && $cantidadConceptosEconomicos > 0 ? +$conceptosEconomicos->perPage() : 0,
            'pagina_actual' => isset($conceptosEconomicos) && $cantidadConceptosEconomicos > 0 ? $conceptosEconomicos->currentPage() : 1,
            'ultima_pagina' => isset($conceptosEconomicos) && $cantidadConceptosEconomicos > 0 ? $conceptosEconomicos->lastPage() : 0,
            'total' => isset($conceptosEconomicos) && $cantidadConceptosEconomicos > 0 ? $conceptosEconomicos->total() : 0
        ];
    }

    public function obtenerColeccionLigera($dto)
    {
        $conceptoEconomico = ConceptoEconomico::obtenerColeccionLigera($dto);
        return $conceptoEconomico;
    }

}
