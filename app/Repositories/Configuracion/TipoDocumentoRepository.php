<?php


namespace App\Repositories\Configuracion;


use App\Contracts\Configuracion\TipoDocumentoService;
use App\Contracts\Seguridad\AuditoriaMaestroService;
use App\Enum\AccionAuditoriaEnum;
use App\Exceptions\ModelException;
use App\Model\Configuracion\TipoDocumento;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class TipoDocumentoRepository implements TipoDocumentoService
{

    protected  $auditoriaMaestroService;

    public function __construct(AuditoriaMaestroService $auditoriaMaestroService){
        $this->auditoriaMaestroService = $auditoriaMaestroService;
    }

    public function cargar($id)
    {
        $tipoDocumento = TipoDocumento::find($id);

        return [
            'id' => $tipoDocumento->id,
            'codigo' => $tipoDocumento->codigo,
            'nombre' => $tipoDocumento->nombre,
            'estado' => $tipoDocumento->estado,
            'usuario_creacion_id' => $tipoDocumento->usuario_creacion_id,
            'usuario_creacion_nombre' => $tipoDocumento->usuario_creacion_nombre,
            'usuario_modificacion_id' => $tipoDocumento->usuario_modificacion_id,
            'usuario_modificacion_nombre' => $tipoDocumento->usuario_modificacion_nombre,
            'fecha_creacion' => (new Carbon($tipoDocumento->created_at))->format("Y-m-d H:i:s"),
            'fecha_modificacion' => (new Carbon($tipoDocumento->updated_at))->format("Y-m-d H:i:s")
        ];
    }

    public function modificarOCrear($dto)
    {
        $user = Auth::user();
        $usuario = $user->usuario();
        if (!isset($dto['id'])) {
            $dto['usuario_creacion_id'] = $usuario->id ?? ($dto['usuario_creacion_id'] ?? null);
            $dto['usuario_creacion_nombre'] = $usuario->nombre ?? ($dto['usuario_creacion_nombre'] ?? null);
        }
        if (isset($usuario) || isset($dto['usuario_modificacion_id'])) {
            $dto['usuario_modificacion_id'] = $usuario->id ?? ($dto['usuario_modificacion_id'] ?? null);
            $dto['usuario_modificacion_nombre'] = $usuario->nombre ?? ($dto['usuario_modificacion_nombre'] ?? null);
        }

        // Consultar el tipo de empresa
        $tipoDocumento = isset($dto['id']) ? TipoDocumento::find($dto['id']) : new TipoDocumento();

        // Guardar objeto original para auditoria
        $tipoDocumentoOriginal = $tipoDocumento->toJson();

        $tipoDocumento->fill($dto);
        $guardado = $tipoDocumento->save();
        if(!$guardado){
            throw new ModelException("Ocurrió un error al intentar guardar el tipo de empresa.", $tipoDocumento);
        }

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $tipoDocumento->id,
            'nombre_recurso' => TipoDocumento::class,
            'descripcion_recurso' => $tipoDocumento->nombre,
            'accion' => isset($dto['id']) ? AccionAuditoriaEnum::MODIFICAR : AccionAuditoriaEnum::CREAR,
            'recurso_original' => isset($dto['id']) ? $tipoDocumentoOriginal : $tipoDocumento->toJson(),
            'recurso_resultante' => isset($dto['id']) ? $tipoDocumento->toJson() : null
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $this->cargar($tipoDocumento->id);
    }

    public function eliminar($id)
    {
        // Connsultar el objeto
        $tipoDocumento = TipoDocumento::find($id);

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $tipoDocumento->id,
            'nombre_recurso' => TipoDocumento::class,
            'descripcion_recurso' => $tipoDocumento->nombre,
            'accion' => AccionAuditoriaEnum::ELIMINAR,
            'recurso_original' => $tipoDocumento->toJson()
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $tipoDocumento->delete();
    }

    public function obtenerColeccion($dto)
    {
        $data = [];
        $tipoDocumentos = TipoDocumento::obtenerColeccion($dto);
        foreach ($tipoDocumentos ?? [] as $shipping){
            array_push($data, $shipping);
        }

        $cantidadTipoDocumentos = count($tipoDocumentos);
        $to = isset($tipoDocumentos) && $cantidadTipoDocumentos > 0 ? $tipoDocumentos->currentPage() * $tipoDocumentos->perPage() : null;
        $to = isset($to) && isset($tipoDocumentos) && $to > $tipoDocumentos->total() && $cantidadTipoDocumentos > 0 ? $tipoDocumentos->total() : $to;
        $from = isset($to) && isset($tipoDocumentos) && $cantidadTipoDocumentos > 0 ?
            $tipoDocumentos->perPage() > $to ? 1 : ($to - $tipoDocumentos->perPage()) + 1
            : null;
        return [
            'datos' => $data,
            'desde' => $from,
            'hasta' => $to,
            'por_pagina' => isset($tipoDocumentos) && $cantidadTipoDocumentos > 0 ? +$tipoDocumentos->perPage() : 0,
            'pagina_actual' => isset($tipoDocumentos) && $cantidadTipoDocumentos > 0 ? $tipoDocumentos->currentPage() : 1,
            'ultima_pagina' => isset($tipoDocumentos) && $cantidadTipoDocumentos > 0 ? $tipoDocumentos->lastPage() : 0,
            'total' => isset($tipoDocumentos) && $cantidadTipoDocumentos > 0 ? $tipoDocumentos->total() : 0
        ];
    }

    public function obtenerColeccionLigera($dto)
    {
        $tipoDocumento = TipoDocumento::obtenerColeccionLigera($dto);
        return $tipoDocumento;
    }

}
