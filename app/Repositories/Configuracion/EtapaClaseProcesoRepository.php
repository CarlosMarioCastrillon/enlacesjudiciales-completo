<?php


namespace App\Repositories\Configuracion;


use App\Contracts\Configuracion\EtapaClaseProcesoService;
use App\Contracts\Seguridad\AuditoriaMaestroService;
use App\Enum\AccionAuditoriaEnum;
use App\Exceptions\ModelException;
use App\Model\Configuracion\EtapaClaseProceso;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class EtapaClaseProcesoRepository implements EtapaClaseProcesoService
{

    protected  $auditoriaMaestroService;

    public function __construct(AuditoriaMaestroService $auditoriaMaestroService){
        $this->auditoriaMaestroService = $auditoriaMaestroService;
    }

    public function cargar($id)
    {
        $etapaClaseProceso = EtapaClaseProceso::find($id);
        $etapaProceso = $etapaClaseProceso->etapaProceso;
        $claseProceso = $etapaClaseProceso->claseProceso;

        return [
            'id' => $etapaClaseProceso->id,
            'estado' => $etapaClaseProceso->estado,
            'usuario_creacion_id' => $etapaClaseProceso->usuario_creacion_id,
            'usuario_creacion_nombre' => $etapaClaseProceso->usuario_creacion_nombre,
            'usuario_modificacion_id' => $etapaClaseProceso->usuario_modificacion_id,
            'usuario_modificacion_nombre' => $etapaClaseProceso->usuario_modificacion_nombre,
            'fecha_creacion' => (new Carbon($etapaClaseProceso->created_at))->format("Y-m-d H:i:s"),
            'fecha_modificacion' => (new Carbon($etapaClaseProceso->updated_at))->format("Y-m-d H:i:s"),
            'etapa_proceso' => isset($etapaProceso) ? [
                'id' => $etapaProceso->id,
                'nombre' => $etapaProceso->nombre
            ] : null,
            'clase_proceso' => isset($claseProceso) ? [
                'id' => $claseProceso->id,
                'nombre' => $claseProceso->nombre
            ] : null
        ];
    }

    public function modificarOCrear($dto)
    {
        $user = Auth::user();
        $usuario = $user->usuario();
        if (!isset($dto['id'])) {
            $dto['usuario_creacion_id'] = $usuario->id ?? ($dto['usuario_creacion_id'] ?? null);
            $dto['usuario_creacion_nombre'] = $usuario->nombre ?? ($dto['usuario_creacion_nombre'] ?? null);
        }
        if (isset($usuario) || isset($dto['usuario_modificacion_id'])) {
            $dto['usuario_modificacion_id'] = $usuario->id ?? ($dto['usuario_modificacion_id'] ?? null);
            $dto['usuario_modificacion_nombre'] = $usuario->nombre ?? ($dto['usuario_modificacion_nombre'] ?? null);
        }

        // Consultar la etapa de clase de proceso
        $etapaClaseProceso = isset($dto['id']) ? EtapaClaseProceso::find($dto['id']) : new EtapaClaseProceso();

        // Guardar objeto original para auditoria
        $etapaClaseProcesoOriginal = $etapaClaseProceso->toJson();

        $etapaClaseProceso->fill($dto);
        $guardado = $etapaClaseProceso->save();
        if(!$guardado){
            throw new ModelException("Ocurrió un error al intentar guardar la etapa de la clase de proceso.", $etapaClaseProceso);
        }

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $etapaClaseProceso->id,
            'nombre_recurso' => EtapaClaseProceso::class,
            'descripcion_recurso' => $etapaClaseProceso->nombre,
            'accion' => isset($dto['id']) ? AccionAuditoriaEnum::MODIFICAR : AccionAuditoriaEnum::CREAR,
            'recurso_original' => isset($dto['id']) ? $etapaClaseProcesoOriginal : $etapaClaseProceso->toJson(),
            'recurso_resultante' => isset($dto['id']) ? $etapaClaseProceso->toJson() : null
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $this->cargar($etapaClaseProceso->id);
    }

    public function eliminar($id)
    {
        // Connsultar el objeto
        $etapaClaseProceso = EtapaClaseProceso::find($id);

        // Guardar auditoria
        $auditoriaDto = array(
            'id_recurso' => $etapaClaseProceso->id,
            'nombre_recurso' => EtapaClaseProceso::class,
            'descripcion_recurso' => $etapaClaseProceso->nombre,
            'accion' => AccionAuditoriaEnum::ELIMINAR,
            'recurso_original' => $etapaClaseProceso->toJson()
        );
        $this->auditoriaMaestroService->crear($auditoriaDto);

        return $etapaClaseProceso->delete();
    }

    public function obtenerColeccion($dto)
    {
        $data = [];
        $etapaClaseProcesos = EtapaClaseProceso::obtenerColeccion($dto);
        foreach ($etapaClaseProcesos ?? [] as $shipping){
            array_push($data, $shipping);
        }

        $cantidadetapaClaseProcesos = count($etapaClaseProcesos);
        $to = isset($etapaClaseProcesos) && $cantidadetapaClaseProcesos > 0 ? $etapaClaseProcesos->currentPage() * $etapaClaseProcesos->perPage() : null;
        $to = isset($to) && isset($etapaClaseProcesos) && $to > $etapaClaseProcesos->total() && $cantidadetapaClaseProcesos> 0 ? $etapaClaseProcesos->total() : $to;
        $from = isset($to) && isset($etapaClaseProcesos) && $cantidadetapaClaseProcesos > 0 ?
            $etapaClaseProcesos->perPage() > $to ? 1 : ($to - $etapaClaseProcesos->perPage()) + 1
            : null;
        return [
            'datos' => $data,
            'desde' => $from,
            'hasta' => $to,
            'por_pagina' => isset($etapaClaseProcesos) && $cantidadetapaClaseProcesos > 0 ? +$etapaClaseProcesos->perPage() : 0,
            'pagina_actual' => isset($etapaClaseProcesos) && $cantidadetapaClaseProcesos > 0 ? $etapaClaseProcesos->currentPage() : 1,
            'ultima_pagina' => isset($etapaClaseProcesos) && $cantidadetapaClaseProcesos > 0 ? $etapaClaseProcesos->lastPage() : 0,
            'total' => isset($etapaClaseProcesos) && $cantidadetapaClaseProcesos > 0 ? $etapaClaseProcesos->total() : 0
        ];
    }
}
