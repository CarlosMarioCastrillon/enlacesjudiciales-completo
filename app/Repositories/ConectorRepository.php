<?php

namespace App\Repositories;

use App\Contracts\ConectorService;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Http\Response;

class ConectorRepository implements ConectorService
{

    public function get($dto)
    {
        // Reporte
        $reporte = [
            'con_conexion' => true,
            'es_privado' => false
        ];

        // Consultar proceso
        $procesos = null;
        try {
            $procesos = $this->getProcesosIds([
                'timeout' => $dto['timeout'] ?? null,
                'proceso_numero' => $dto['proceso_numero'],
                'solo_activos' => $dto['solo_activos'],
                'pagina' => 1
            ]);
        }catch (Exception $e){
            $reporte['con_conexion'] = false;
        }

        foreach ($procesos ?? [] as $proceso){
            if($proceso['es_privado']){
                $reporte['es_privado'] = true;
                continue;
            }

            // Actuaciones
            try {
                $actuaciones = $this->getActuacionesProceso([
                    'timeout' => $dto['timeout'] ?? null,
                    'proceso_id' => $proceso['id'],
                    'pagina' => 1
                ]);
                $reporte['actuaciones'] = $actuaciones;
            }catch (Exception $e){
                $reporte['con_conexion'] = false;
            }

            if(isset($dto['consulta_reducida']) && $dto['consulta_reducida']){
                continue;
            }

            // Datos del proceso
            try {
                $datos = $this->getDatosProceso([
                    'timeout' => $dto['timeout'] ?? null,
                    'proceso_id' => $proceso['id']
                ]);
                $reporte['id'] = $datos['id'] ?? null;
                $reporte['proceso_numero'] = $datos['proceso_numero'] ?? null;
                $reporte['fecha'] = $datos['fecha'] ?? null;
                $reporte['tipo'] = $datos['tipo'] ?? null;
                $reporte['clase'] = $datos['clase'] ?? null;
                $reporte['subclase'] = $datos['subclase'] ?? null;
                $reporte['recurso'] = $datos['recurso'] ?? null;
                $reporte['ubicacion'] = $datos['ubicacion'] ?? null;
                $reporte['despacho'] = $datos['despacho'] ?? null;
                $reporte['ponente'] = $datos['ponente'] ?? null;
                $reporte['departamento'] = $datos['departamento'] ?? null;
                $reporte['con_conexion'] = true;
                $reporte['es_privado'] = false;
            }catch (Exception $e){
                $reporte['con_conexion'] = false;
            }

            // Sujetos procesales
            try {
                $sujetos = $this->getSujetosProceso([
                    'timeout' => $dto['timeout'] ?? null,
                    'proceso_id' => $proceso['id'],
                    'pagina' => 1
                ]);
                $reporte['sujetos_procesales'] = $sujetos;
            }catch (Exception $e){
                $reporte['con_conexion'] = false;
            }

            break;
        }

        return $reporte;
    }

    /**
     * Obtener identificadores de los procesos coincidentes con el número ingresado
     * @param $dto
     * @return mixed
     */
    private function getProcesosIds($dto, $procesoIds = [])
    {
        // Formatear url
        $url = "https://consultaprocesos.ramajudicial.gov.co:448/api/v2/Procesos/Consulta/NumeroRadicacion?";
        $url .= "numero=" . $dto['proceso_numero'];
        $url .= "&SoloActivos=" . ($dto['solo_activos'] ? "true" : "false");
        $url .= "&pagina=" . $dto['pagina'];

        // Consultar datos
        $http = new Client(['verify' => false ]);
        $options = [
            'headers' => [
                'Content-Type' => 'application/json'
            ]
        ];
        if(isset($dto['timeout'])){
            $options['timeout'] = $dto['timeout'];
            $options['connect_timeout'] = $dto['timeout'];
        }
        $externalResponse = $http->get($url, $options);
        if($externalResponse->getStatusCode() != Response::HTTP_OK){
            return null;
        }
        $respuesta = json_decode((string) $externalResponse->getBody(), true);

        /*$cURLConnection = curl_init();
        curl_setopt($cURLConnection, CURLOPT_URL, $url);
        curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
        if(isset($dto['timeout'])){
            curl_setopt($cURLConnection, CURLOPT_TIMEOUT, $dto['timeout']);
        }
        $respuesta = curl_exec($cURLConnection);
        $httpCode = curl_getinfo($cURLConnection, CURLINFO_HTTP_CODE);
        curl_close($cURLConnection);
        $respuesta = json_decode($respuesta);
        if($httpCode != 200){
            throw new Exception;
        }*/

        $cantidadPaginas = $respuesta['paginacion']['cantidadPaginas'] ?? 1;
        $cantidadRegistros = $respuesta['paginacion']['cantidadRegistros'] ?? 0;
        if($cantidadPaginas > 1){
            // Formatear ids
            $procesos = array_map(function ($proceso){
                return [
                    'id' => $proceso['idProceso'] ?? null,
                    'proceso_numero' => $proceso['llaveProceso'] ?? null,
                    'fecha' => $proceso['fechaProceso'] ?? null,
                    'despacho' => $proceso['despacho'] ?? null,
                    'departamento' => $proceso['departamento'] ?? null,
                    'es_privado' => $proceso['esPrivado'] ?? true
                ];
            }, $respuesta['procesos']);

            // Concatenar el reporte
            $procesoIds = array_merge($procesoIds, $procesos);

            // Procesar siguientes paguinas
            $siguientePagina = ($respuesta['paginacion']['cantidadPaginas'] ?? 1) + 1;
            if($siguientePagina > $cantidadPaginas){
                return $procesoIds;
            }else{
                $dto['pagina'] = $siguientePagina;
                $this->getProcesosIds($dto, $procesoIds);
            }
        }else if($cantidadPaginas == 1){
            if($cantidadRegistros > 0){
                // Formatear ids
                $procesos = array_map(function ($proceso){
                    return [
                        'id' => $proceso['idProceso'] ?? null,
                        'proceso_numero' => $proceso['llaveProceso'] ?? null,
                        'fecha' => $proceso['fechaProceso'] ?? null,
                        'despacho' => $proceso['despacho'] ?? null,
                        'departamento' => $proceso['departamento'] ?? null,
                        'es_privado' => $proceso['esPrivado'] ?? true
                    ];
                }, $respuesta['procesos']);

                // Concatenar el reporte
                $procesoIds = array_merge($procesoIds, $procesos);
            }

            return $procesoIds;
        }

        return $procesoIds;
    }

    /**
     * Obetener los datos principales del proceso
     * @param $dto
     * @return mixed
     */
    private function getDatosProceso($dto)
    {
        // Formatear url
        $url = "https://consultaprocesos.ramajudicial.gov.co:448/api/v2/Proceso/Detalle/" . $dto['proceso_id'];

        // Consultar datos
        $http = new Client(['verify' => false ]);
        $options = [
            'headers' => [
                'Content-Type' => 'application/json'
            ]
        ];
        if(isset($dto['timeout'])){
            $options['timeout'] = $dto['timeout'];
            $options['connect_timeout'] = $dto['timeout'];
        }
        $externalResponse = $http->get($url, $options);
        if($externalResponse->getStatusCode() != Response::HTTP_OK){
            return null;
        }
        $respuesta = json_decode((string) $externalResponse->getBody(), true);

        /*$cURLConnection = curl_init();
        curl_setopt($cURLConnection, CURLOPT_URL, $url);
        curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
        if(isset($dto['timeout'])){
            curl_setopt($cURLConnection, CURLOPT_TIMEOUT, $dto['timeout']);
        }
        $respuesta = curl_exec($cURLConnection);
        $httpCode = curl_getinfo($cURLConnection, CURLINFO_HTTP_CODE);
        curl_close($cURLConnection);
        $respuesta = json_decode($respuesta);
        if($httpCode != 200){
            throw new Exception;
        }*/

        $procesoDatos = [
            'id' => $respuesta['idRegProceso'] ?? null,
            'proceso_numero' => $respuesta['nombreRazonSocial'] ?? null,
            'fecha' => $respuesta['fechaProceso'] ?? null,
            'tipo' => $respuesta['tipoProceso'] ?? null,
            'clase' => $respuesta['claseProceso'] ?? null,
            'subclase' => $respuesta['subclaseProceso'] ?? null,
            'recurso' => $respuesta['recurso'] ?? null,
            'ubicacion' => $respuesta['ubicacion'] ?? null,
            'despacho' => $respuesta['despacho'] ?? null,
            'ponente' => $respuesta['ponente'] ?? null
        ];
        return $procesoDatos;
    }

    /**
     * Obetener los sujetos procesales del proceso
     * @param $dto
     * @return mixed
     */
    private function getSujetosProceso($dto, $procesoSujetos = [])
    {
        // Formatear url
        $url = "https://consultaprocesos.ramajudicial.gov.co:448/api/v2/Proceso/Sujetos/" . $dto['proceso_id'];
        $url .= "?pagina=" . $dto['pagina'];

        // Consultar datos
        $http = new Client(['verify' => false ]);
        $options = [
            'headers' => [
                'Content-Type' => 'application/json'
            ]
        ];
        if(isset($dto['timeout'])){
            $options['timeout'] = $dto['timeout'];
            $options['connect_timeout'] = $dto['timeout'];
        }
        $externalResponse = $http->get($url, $options);
        if($externalResponse->getStatusCode() != Response::HTTP_OK){
            return null;
        }
        $respuesta = json_decode((string) $externalResponse->getBody(), true);

        /*$cURLConnection = curl_init();
        curl_setopt($cURLConnection, CURLOPT_URL, $url);
        curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
        if(isset($dto['timeout'])){
            curl_setopt($cURLConnection, CURLOPT_TIMEOUT, $dto['timeout']);
        }
        $respuesta = curl_exec($cURLConnection);
        $httpCode = curl_getinfo($cURLConnection, CURLINFO_HTTP_CODE);
        curl_close($cURLConnection);
        $respuesta = json_decode($respuesta);
        if($httpCode != 200){
            throw new Exception;
        }*/

        $cantidadPaginas = $respuesta['paginacion']['cantidadPaginas'] ?? 1;
        $cantidadRegistros = $respuesta['paginacion']['cantidadRegistros'] ?? 0;
        if($cantidadPaginas > 1){
            // Formatear sujetos
            $sujetos = array_map(function ($sujeto){
                return ['tipo' => $sujeto['tipoSujeto'], 'nombre' => $sujeto['nombreRazonSocial']];
            }, $respuesta['sujetos']);

            // Concatenar el reporte
            $procesoSujetos = array_merge($procesoSujetos, $sujetos);

            // Procesar siguientes paguinas
            $siguientePagina = ($respuesta['paginacion']['cantidadPaginas'] ?? 1) + 1;
            if($siguientePagina > $cantidadPaginas){
                return $procesoSujetos;
            }else{
                $dto['pagina'] = $siguientePagina;
                $this->getSujetosProceso($dto, $procesoSujetos);
            }
        }else if($cantidadPaginas == 1){
            if($cantidadRegistros > 0){
                // Formatear sujetos
                $sujetos = array_map(function ($sujeto){
                    return ['tipo' => $sujeto['tipoSujeto'], 'nombre' => $sujeto['nombreRazonSocial']];
                }, $respuesta['sujetos']);

                // Concatenar el reporte
                $procesoSujetos = array_merge($procesoSujetos, $sujetos);
            }

            return $procesoSujetos;
        }

        return $procesoSujetos;
    }

    /**
     * Obetener las actuaciones del proceso
     * @param $dto
     * @return mixed
     */
    private function getActuacionesProceso($dto, $procesoActuaciones = [])
    {
        // Formatear url
        $url = "https://consultaprocesos.ramajudicial.gov.co:448/api/v2/Proceso/Actuaciones/" . $dto['proceso_id'];
        $url .= "?pagina=" . $dto['pagina'];

        // Consultar datos
        $http = new Client(['verify' => false ]);
        $options = [
            'headers' => [
                'Content-Type' => 'application/json'
            ]
        ];
        if(isset($dto['timeout'])){
            $options['timeout'] = $dto['timeout'];
            $options['connect_timeout'] = $dto['timeout'];
        }
        $externalResponse = $http->get($url, $options);
        if($externalResponse->getStatusCode() != Response::HTTP_OK){
            return null;
        }
        $respuesta = json_decode((string) $externalResponse->getBody(), true);

        /*$cURLConnection = curl_init();
        curl_setopt($cURLConnection, CURLOPT_URL, $url);
        curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
        if(isset($dto['timeout'])){
            curl_setopt($cURLConnection, CURLOPT_TIMEOUT, $dto['timeout']);
        }
        $respuesta = curl_exec($cURLConnection);
        $httpCode = curl_getinfo($cURLConnection, CURLINFO_HTTP_CODE);
        curl_close($cURLConnection);
        $respuesta = json_decode($respuesta);
        if($httpCode != 200){
            throw new Exception;
        }*/

        $cantidadPaginas = $respuesta['paginacion']['cantidadPaginas'] ?? 1;
        $cantidadRegistros = $respuesta['paginacion']['cantidadRegistros'] ?? 0;
        if($cantidadPaginas > 1){
            // Formatear sujetos
            $actuaciones = array_map(function ($actuacion){
                return [
                    'fecha_actuacion' => $actuacion['fechaActuacion'] ?? null,
                    'fecha_inicial' => $actuacion['fechaInicial'] ?? null,
                    'fecha_final' => $actuacion['fechaFinal'] ?? null,
                    'fecha_registro' => $actuacion['fechaRegistro'] ?? null,
                    'actuacion' => $actuacion['actuacion'] ?? null,
                    'anotacion' => $actuacion['anotacion'] ?? null
                ];
            }, $respuesta['actuaciones']);

            // Concatenar el reporte
            $procesoActuaciones = array_merge($procesoActuaciones, $actuaciones);

            // Procesar siguientes paguinas
            $siguientePagina = ($respuesta['paginacion']['cantidadPaginas'] ?? 1) + 1;
            if($siguientePagina > $cantidadPaginas){
                return $procesoActuaciones;
            }else{
                $dto['pagina'] = $siguientePagina;
                $this->getActuacionesProceso($dto, $procesoActuaciones);
            }
        }else if($cantidadPaginas == 1){
            if($cantidadRegistros > 0){
                // Formatear sujetos
                $actuaciones = array_map(function ($actuacion){
                    return [
                        'fecha_actuacion' => $actuacion['fechaActuacion'] ?? null,
                        'fecha_inicial' => $actuacion['fechaInicial'] ?? null,
                        'fecha_final' => $actuacion['fechaFinal'] ?? null,
                        'fecha_registro' => $actuacion['fechaRegistro'] ?? null,
                        'actuacion' => $actuacion['actuacion'] ?? null,
                        'anotacion' => $actuacion['anotacion'] ?? null
                    ];
                }, $respuesta['actuaciones']);

                // Concatenar el reporte
                $procesoActuaciones = array_merge($procesoActuaciones, $actuaciones);
            }

            return $procesoActuaciones;
        }

        return $procesoActuaciones;
    }

}
