<?php

namespace App\Model\Seguridad;

use Illuminate\Database\Eloquent\Model;

class TokenDeAcceso extends Model
{

    protected $table = "tokens_de_acceso";

    protected $fillable = [
        'token',
        'fecha_expiracion',
        'usuario_id',
        'estado'
    ];

}
