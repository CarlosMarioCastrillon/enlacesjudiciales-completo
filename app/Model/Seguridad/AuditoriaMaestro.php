<?php

namespace App\Model\Seguridad;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class AuditoriaMaestro extends Model
{

    protected $fillable = [
        'id_recurso',
        'nombre_recurso',
        'descripcion_recurso',
        'accion',
        'responsable_id',
        'responsable_nombre',
        'recurso_original',
        'recurso_resultante'
    ];

    public static function obtenerColeccion($dto){
        $query = DB::table('auditoria_maestros')
            ->select(
                'id_recurso',
                'nombre_recurso',
                'descripcion_recurso',
                'accion',
                'responsable_id',
                'responsable_nombre',
                'recurso_original',
                'recurso_resultante',
                'created_at AS fecha'
            );

        if(isset($dto['nombre_recurso'])){
            $query->where('nombre_recurso', 'like', "%" . $dto['nombre_recurso'] . "%");
        }
        if(isset($dto['descripcion_recurso'])){
            $query->where('descripcion_recurso', 'like', '%' . $dto['descripcion_recurso'] . '%');
        }
        if(isset($dto['accion'])){
            $query->where('accion', $dto['accion']);
        }
        if(isset($dto['fecha_desde'])){
            $query->where('created_at', '>=', $dto['fecha_desde'] . ' 00:00:00');
        }
        if(isset($dto['fecha_hasta'])){
            $query->where('created_at', '<=', $dto['fecha_hasta'] . ' 23:59:59');
        }

        if (isset($dto['ordernar_por']) && count($dto['ordernar_por']) > 0){
            foreach ($dto['ordernar_por'] as $attribute => $value){
                if($attribute == 'nombre_recurso'){
                    $query->orderBy('nombre_recurso', $value);
                }
                if($attribute == 'descripcion_recurso'){
                    $query->orderBy('descripcion_recurso', $value);
                }
                if($attribute == 'accion'){
                    $query->orderBy('accion', $value);
                }
                if($attribute == 'fecha'){
                    $query->orderBy('created_at', $value);
                }
            }
        }else{
            $query->orderBy("id", "desc");
        }

        $auditorias = $query->paginate($dto['limite'] ?? 100);
        return $auditorias;
    }

}
