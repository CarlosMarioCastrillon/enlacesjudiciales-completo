<?php

namespace App\Model\Seguridad;

use App\Model\Administracion\ParametroConstante;
use App\Model\Configuracion\Area;
use App\Model\Configuracion\Ciudad;
use App\Model\Configuracion\Departamento;
use App\Model\Configuracion\Empresa;
use App\Model\Configuracion\TipoDocumento;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Usuario extends Model
{

    protected $fillable = [
        'documento',
        'nombre',
        'email_uno',
        'email_dos',
        'fecha_vencimiento',
        'dependiente_principal',
        'con_microportal',
        'con_movimiento_proceso',
        'con_montaje_actuacion',
        'con_movimiento_proceso_dos',
        'con_montaje_actuacion_dos',
        'con_tarea',
        'con_respuesta_tarea',
        'con_ven_termino_tarea',
        'con_ven_termino_actuacion_rama',
        'con_ven_termino_actuacion_archivo',
        'estado',
        'tipo_documento_id',
        'empresa_id',
        'departamento_id',
        'ciudad_id',
        'area_id',
        'user_id',
        'usuario_creacion_id',
        'usuario_creacion_nombre',
        'usuario_modificacion_id',
        'usuario_modificacion_nombre'
    ];

    public function tipoDocumento(){
        return $this->belongsTo(TipoDocumento::class);
    }

    public function empresa(){
        return $this->belongsTo(Empresa::class);
    }

    public function departamento(){
        return $this->belongsTo(Departamento::class);
    }

    public function ciudad(){
        return $this->belongsTo(Ciudad::class);
    }

    public function area(){
        return $this->belongsTo(Area::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function rol(){
        $user = $this->user;
        return DB::table('roles')
            ->join('model_has_roles', 'model_has_roles.role_id', '=', 'roles.id')
            ->where('model_has_roles.model_type', User::class)
            ->where('model_has_roles.model_id', $user->id)
            ->select('roles.*')
            ->first();
    }

    public static function findBy($documento){
        return Usuario::where('documento', $documento)->first();
    }

    public static function obtenerColeccionLigera($dto){
        $query = DB::table('usuarios')
            ->select(
                'usuarios.id', 'usuarios.nombre'
            );

        if(isset($dto['es_dependiente']) && $dto['es_dependiente']){
            // Constantes
            $parametros = ParametroConstante::cargarParametros();
            $dto['rol_id'] = $parametros['DEPENDIENTE_ROL_ID'];
            $dto['empresa_id'] = $parametros['EMPRESA_PRESTADORA_SERVICIO_ID'];

            $query->join('users', 'users.id', '=', 'usuarios.user_id')
                ->join('model_has_roles', function ($join) use($dto) {
                    $join->on('model_has_roles.model_id', '=', 'users.id')
                        ->where('model_has_roles.role_id', $dto['rol_id'] );})
                ->join('roles', function ($join) use($dto){
                    $join->on('roles.id', '=', 'model_has_roles.role_id');
                })
            ->where('usuarios.empresa_id','=', $dto['empresa_id']);
        }

        if(isset($dto['ciudad_id'])){
            $query->where('ciudad_id', $dto['ciudad_id']);
        }

        if(isset($dto['empresa_id'])){
            $query->where('usuarios.empresa_id', $dto['empresa_id']);
        }

        return $query->get();
    }

    public static function obtenerDependientePrincipal($dto){
        $parametros = ParametroConstante::cargarParametros();
        $dto['empresa_id'] = $parametros['EMPRESA_PRESTADORA_SERVICIO_ID'];
        $query = DB::table('usuarios')
            ->select(
                'usuarios.id', 'usuarios.nombre'
            )->where('usuarios.dependiente_principal', '=', 1)
            ->where('usuarios.empresa_id','=', $dto['empresa_id']);

        return $query->get();
    }

    public static function obtenerColeccion($dto){
        // Constantes
        $parametros = ParametroConstante::cargarParametros();

        $user = Auth::user();
        $rol = $user->rol();
        $empresa = $user->empresa();
        $dto['empresa_cliente_id'] = $empresa->id;
        $dto['tipo_rol'] = 3;

        // Cargar solo las empresas de la zona
        if($rol->id == ($parametros['ADMINISTRADOR_ZONA_ROL_ID'] ?? null)){
            $dto['zona_id'] = $empresa->zona_id;
        }

        // Cargar solo los usuarios de mi empresa
        if(!(
            $rol->id == ($parametros['SUPERUSUARIO_ROL_ID'] ?? null) ||
            $rol->id == ($parametros['ADMINISTRADOR_ZONA_ROL_ID'] ?? null)
        )){
            $dto['empresa_id'] = $empresa->id;
        }

        $query = DB::table('usuarios')
            ->join('empresas', 'empresas.id', '=', 'usuarios.empresa_id')
            ->join('tipos_de_documentos', 'tipos_de_documentos.id', '=', 'usuarios.tipo_documento_id')
            ->join('model_has_roles', function ($join) use($dto) {
                $join->on('model_has_roles.model_id', '=', 'usuarios.user_id');
            })
            ->join('roles', function ($join) use($dto){
                $join->on('roles.id', '=', 'model_has_roles.role_id');
            })
            ->select(
                'usuarios.id',
                'tipos_de_documentos.codigo as tipo_documento',
                'usuarios.nombre',
                'usuarios.documento',
                'usuarios.email_uno',
                'usuarios.fecha_vencimiento',
                'usuarios.estado',
                'usuarios.usuario_creacion_id',
                'usuarios.usuario_creacion_nombre',
                'usuarios.usuario_modificacion_id',
                'usuarios.usuario_modificacion_nombre',
                'usuarios.created_at AS fecha_creacion',
                'usuarios.updated_at AS fecha_modificacion',
                'empresas.nombre AS empresa_nombre',
                'roles.name AS rol_nombre'
            );

        if(isset($dto['usuario_cliente']) && $dto['usuario_cliente']){
            $query->join('users', 'users.id', '=', 'usuarios.user_id')
                ->join('model_has_roles', 'model_has_roles.model_id', '=', 'users.id')
                ->join('roles', function ($join) use($dto) {
                    $join->on('roles.id', '=', 'model_has_roles.role_id')
                        ->where('roles.type', '=', $dto['tipo_rol']);
                })->where('usuarios.empresa_id', '=', $dto['empresa_cliente_id'])
                ->get([0]);
        }

        if(isset($dto['zona_id'])){
            $query->where('empresas.zona_id', '=', $dto['zona_id']);
        }

        if(isset($dto['empresa_id'])){
            $query->where('empresas.id', '=', $dto['empresa_id']);
        }

        if(isset($dto['nombre'])){
            $query->where('usuarios.nombre', 'like', '%' . $dto['nombre'] . '%');
        }

        if (isset($dto['ordernar_por']) && count($dto['ordernar_por']) > 0){
            foreach ($dto['ordernar_por'] as $attribute => $value){
                if($attribute == 'id'){
                    $query->orderBy('usuarios.id', $value);
                }
                if($attribute == 'nombre'){
                    $query->orderBy('usuarios.nombre', $value);
                }
                if($attribute == 'usuario_creacion_nombre'){
                    $query->orderBy('usuarios.usuario_creacion_nombre', $value);
                }
                if($attribute == 'usuario_modificacion_nombre'){
                    $query->orderBy('usuarios.usuario_modificacion_nombre', $value);
                }
                if($attribute == 'fecha_creacion'){
                    $query->orderBy('usuarios.created_at', $value);
                }
                if($attribute == 'fecha_modificacion'){
                    $query->orderBy('usuarios.updated_at', $value);
                }
            }
        }else{
            $query->orderBy("usuarios.id", "desc");
        }

        $usuarios = $query->paginate($dto['limite'] ?? 100);
        return $usuarios;
    }

    public static function obtener($dto){
        $query = DB::table('usuarios')
            ->join('empresas', 'empresas.id', '=', 'usuarios.empresa_id')
            ->join('tipos_de_documentos', 'tipos_de_documentos.id', '=', 'usuarios.tipo_documento_id')
            ->join('users', 'users.id', '=', 'usuarios.user_id')
            ->join('model_has_roles', 'model_has_roles.model_id', '=', 'users.id')
            ->join('roles', 'roles.id', '=', 'model_has_roles.role_id')
            ->select(
                'usuarios.id',
                'tipos_de_documentos.codigo as tipo_documento',
                'usuarios.nombre',
                'roles.name as rol',
                'usuarios.documento',
                'usuarios.email_uno',
                'usuarios.fecha_vencimiento',
                'usuarios.estado',
                'usuarios.usuario_creacion_id',
                'usuarios.usuario_creacion_nombre',
                'usuarios.usuario_modificacion_id',
                'usuarios.usuario_modificacion_nombre',
                'usuarios.created_at AS fecha_creacion',
                'usuarios.updated_at AS fecha_modificacion',
                'empresas.nombre AS empresa_nombre'
            )->where('empresas.id', '=', $dto['empresa_id'])
            ->orderBy("usuarios.id", "desc")
            ->get();

        return $query;
    }

}
