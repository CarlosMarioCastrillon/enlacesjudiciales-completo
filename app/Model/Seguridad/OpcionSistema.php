<?php

namespace App\Model\Seguridad;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class OpcionSistema extends Model
{

    protected $table = "opciones_del_sistema";

    protected $fillable = [
        'nombre',
        'icono_menu',
        'clave_permiso',
        'posicion',
        'url',
        'url_ayuda',
        'estado',
        'opcion_del_sistema_id',
        'usuario_creacion_id',
        'usuario_creacion_nombre',
        'usuario_modificacion_id',
        'usuario_modificacion_nombre',
    ];

    public function padre()
    {
        return $this->belongsTo(OpcionSistema::class, 'opcion_del_sistema_id');
    }

    public function hijos()
    {
        return $this->hasMany(OpcionSistema::class, 'opcion_del_sistema_id');
    }

    public static function obtenerColeccionLigera($dto){
        $query = DB::table('opciones_del_sistema')
            ->select(
                'id',
                'nombre'
            );

        if(isset($dto['padre_id'])){
            $query->where('opcion_del_sistema_id', $dto['padre_id']);
        }

        if(isset($dto['sin_padre']) && ($dto['sin_padre'] || $dto['sin_padre'] == "true")){
            $query->whereNull('opcion_del_sistema_id');
        }

        return $query->get();
    }

    public static function obtenerColeccion($dto){
        $query = DB::table('opciones_del_sistema')
            ->select(
                'opciones_del_sistema.id',
                'opciones_del_sistema.nombre',
                'opciones_del_sistema.icono_menu',
                'opciones_del_sistema.clave_permiso',
                'opciones_del_sistema.posicion',
                'opciones_del_sistema.url',
                'opciones_del_sistema.url_ayuda',
                'opciones_del_sistema.estado',
                'opciones_del_sistema.opcion_del_sistema_id',
                'opciones_del_sistema.usuario_creacion_id',
                'opciones_del_sistema.usuario_creacion_nombre',
                'opciones_del_sistema.usuario_modificacion_id',
                'opciones_del_sistema.usuario_modificacion_nombre',
                'opciones_del_sistema.created_at AS fecha_creacion',
                'opciones_del_sistema.updated_at AS fecha_modificacion'
            );

        if(isset($dto['nombre'])){
            $query->where('opciones_del_sistema.nombre', 'like', '%' . $dto['nombre'] . '%');
        }

        if (isset($dto['ordernar_por']) && count($dto['ordernar_por']) > 0){
            foreach ($dto['ordernar_por'] as $attribute => $value){
                if($attribute == 'id'){
                    $query->orderBy('opciones_del_sistema.id', $value);
                }
                if($attribute == 'nombre'){
                    $query->orderBy('opciones_del_sistema.nombre', $value);
                }
                if($attribute == 'usuario_creacion_nombre'){
                    $query->orderBy('opciones_del_sistema.usuario_creacion_nombre', $value);
                }
                if($attribute == 'usuario_modificacion_nombre'){
                    $query->orderBy('opciones_del_sistema.usuario_modificacion_nombre', $value);
                }
                if($attribute == 'fecha_creacion'){
                    $query->orderBy('opciones_del_sistema.created_at', $value);
                }
                if($attribute == 'fecha_modificacion'){
                    $query->orderBy('opciones_del_sistema.updated_at', $value);
                }
            }
        }else{
            $query->orderBy("opciones_del_sistema.id", "desc");
        }

        $opcionesSistema = $query->paginate($dto['limite'] ?? 100);
        return $opcionesSistema;
    }

}
