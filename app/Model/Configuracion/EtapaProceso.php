<?php

namespace App\Model\Configuracion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class EtapaProceso extends Model
{

    protected $table = 'etapas_de_procesos';

    protected $fillable = [
        'nombre',
        'estado',
        'usuario_creacion_id',
        'usuario_creacion_nombre',
        'usuario_modificacion_id',
        'usuario_modificacion_nombre',
    ];

    public static function obtenerColeccionLigera($dto){
        $query = DB::table('etapas_de_procesos')
            ->select(
                'id',
                'nombre')
            ->where('estado','=','1');

        return $query->get();
    }

    public static function obtenerColeccion($dto){
        $query = DB::table('etapas_de_procesos')
            ->select(
                'etapas_de_procesos.id',
                'etapas_de_procesos.nombre',
                'etapas_de_procesos.estado',
                'etapas_de_procesos.usuario_creacion_id',
                'etapas_de_procesos.usuario_creacion_nombre',
                'etapas_de_procesos.usuario_modificacion_id',
                'etapas_de_procesos.usuario_modificacion_nombre',
                'etapas_de_procesos.created_at AS fecha_creacion',
                'etapas_de_procesos.updated_at AS fecha_modificacion'
            );

        if(isset($dto['nombre'])){
            $query->where('etapas_de_procesos.nombre', 'like', '%' . $dto['nombre'] . '%');
        }

        if (isset($dto['ordenar_por']) && count($dto['ordenar_por']) > 0){
            foreach ($dto['ordenar_por'] as $attribute => $value){
                if($attribute == 'nombre'){
                    $query->orderBy('etapas_de_procesos.nombre', $value);
                }
                if($attribute == 'estado'){
                    $query->orderBy('etapas_de_procesos.estado', $value);
                }
                if($attribute == 'usuario_creacion_nombre'){
                    $query->orderBy('etapas_de_procesos.usuario_creacion_nombre', $value);
                }
                if($attribute == 'usuario_modificacion_nombre'){
                    $query->orderBy('etapas_de_procesos.usuario_modificacion_nombre', $value);
                }
                if($attribute == 'fecha_creacion'){
                    $query->orderBy('etapas_de_procesos.created_at', $value);
                }
                if($attribute == 'fecha_modificacion'){
                    $query->orderBy('etapas_de_procesos.updated_at', $value);
                }
            }
        }else{
            $query->orderBy("etapas_de_procesos.id", "desc");
        }

        $etapasProceso = $query->paginate($dto['limite'] ?? 100);
        return $etapasProceso;
    }
}
