<?php

namespace App\Model\Configuracion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Plan extends Model
{

    protected $table = 'planes';

    public function ciudad(){
        return $this->belongsTo(Ciudad::class);
    }
    public function departamento(){
        return $this->belongsTo(Departamento::class);
    }
    public function servicio(){
        return $this->belongsTo(Servicio::class);
    }

    protected $fillable = [
        'servicio_id',
        'departamento_id',
        'ciudad_id',
        'empresa_id',
        'cantidad',
        'valor',
        'observacion',
        'estado',
        'usuario_creacion_id',
        'usuario_creacion_nombre',
        'usuario_modificacion_id',
        'usuario_modificacion_nombre',
    ];

    public static function obtener($dto){
        $query = DB::table('planes')
            ->leftJoin('ciudades', 'ciudades.id', '=', 'planes.ciudad_id')
            ->leftJoin('departamentos', 'departamentos.id', '=', 'ciudades.departamento_id')
            ->join('servicios', 'servicios.id', '=', 'planes.servicio_id')
            ->select(
                'planes.id',
                'servicios.nombre As servicio_nombre',
                'departamentos.nombre As departamento_nombre',
                'ciudades.nombre As ciudad_nombre',
                'planes.empresa_id',
                'planes.cantidad',
                'planes.valor',
                'planes.observacion',
                'planes.estado',
                'planes.usuario_creacion_id',
                'planes.usuario_creacion_nombre',
                'planes.usuario_modificacion_id',
                'planes.usuario_modificacion_nombre',
                'planes.created_at AS fecha_creacion',
                'planes.updated_at AS fecha_modificacion'
            )
            ->where('empresa_id', '=', $dto['empresa_id'])
            ->orderBy("planes.id", "desc");

        return $query->get();
    }

    public static function obtenerColeccion($dto){
        $query = DB::table('planes')
            ->leftJoin('ciudades', 'ciudades.id', '=', 'planes.ciudad_id')
            ->leftJoin('departamentos', 'departamentos.id', '=', 'ciudades.departamento_id')
            ->join('servicios', 'servicios.id', '=', 'planes.servicio_id')
            ->select(
                'planes.id',
                'servicios.nombre As servicio_nombre',
                'departamentos.nombre As departamento_nombre',
                'ciudades.nombre As ciudad_nombre',
                'planes.empresa_id',
                'planes.cantidad',
                'planes.valor',
                'planes.observacion',
                'planes.estado',
                'planes.usuario_creacion_id',
                'planes.usuario_creacion_nombre',
                'planes.usuario_modificacion_id',
                'planes.usuario_modificacion_nombre',
                'planes.created_at AS fecha_creacion',
                'planes.updated_at AS fecha_modificacion'
            )
            ->where('empresa_id', '=', $dto['empresa_id']);

        if (isset($dto['ordenar_por']) && count($dto['ordenar_por']) > 0){
            foreach ($dto['ordenar_por'] as $attribute => $value){
                if($attribute == 'servicio'){
                    $query->orderBy('planes.servicio_id', $value);
                }
                if($attribute == 'ciudad'){
                    $query->orderBy('planes.ciudad_id', $value);
                }
                if($attribute == 'cantidad'){
                    $query->orderBy('planes.cantidad', $value);
                }
                if($attribute == 'valor'){
                    $query->orderBy('planes.valor', $value);
                }
                if($attribute == 'observacion'){
                    $query->orderBy('planes.observacion', $value);
                }
                if($attribute == 'estado'){
                    $query->orderBy('planes.estado', $value);
                }
                if($attribute == 'usuario_creacion_nombre'){
                    $query->orderBy('planes.usuario_creacion_nombre', $value);
                }
                if($attribute == 'usuario_modificacion_nombre'){
                    $query->orderBy('planes.usuario_modificacion_nombre', $value);
                }
                if($attribute == 'fecha_creacion'){
                    $query->orderBy('planes.created_at', $value);
                }
                if($attribute == 'fecha_modificacion'){
                    $query->orderBy('planes.updated_at', $value);
                }
            }
        }else{
            $query->orderBy("planes.id", "desc");
        }

        $planes = $query->paginate($dto['limite'] ?? 100);
        return $planes;
    }
}
