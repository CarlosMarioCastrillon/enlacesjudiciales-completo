<?php

namespace App\Model\Configuracion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Juzgado extends Model
{

    protected $table = 'juzgados';

    protected $fillable = [
        'nombre',
        'departamento_id',
        'ciudad_id',
        'juzgado',
        'sala',
        'juzgado_rama',
        'juzgado_actual',
        'estado',
        'usuario_creacion_id',
        'usuario_creacion_nombre',
        'usuario_modificacion_id',
        'usuario_modificacion_nombre',
    ];

    public function ciudad(){
        return $this->belongsTo(Ciudad::class);
    }
    public function departamento(){
        return $this->belongsTo(Departamento::class);
    }

    public static function findBy($juzgadoRama){
        return Juzgado::where('juzgado_rama', $juzgadoRama)->first();
    }

    public static function obtenerColeccionLigera($dto){
        $query = DB::table('juzgados')
            ->select(
                'id',
                'nombre',
                'juzgado_rama',
                'juzgado',
                'sala'
            )
            ->where('estado','=',true);

        if(isset($dto['juzgado_actual']) && $dto['juzgado_actual']){
            $query->where('juzgado_actual', $dto['juzgado_actual']);
        }

        if(isset($dto['ciudad_id']) && $dto['ciudad_id']){
            $query->where('ciudad_id', $dto['ciudad_id']);
        }

        return $query->get();
    }

    public static function obtenerColeccionReferencia($dto){
        $query = DB::table('juzgados')
            ->select(
                'nombre', 'juzgado_rama'
            )
            ->where('estado','=',true);

        return $query
            ->groupBy('nombre')
            ->groupBy('juzgado_rama')
            ->get();
    }

    public static function obtenerColeccion($dto){
        $query = DB::table('juzgados')
            ->join('ciudades', 'ciudades.id', '=', 'juzgados.ciudad_id')
            ->join('departamentos', 'departamentos.id', '=', 'ciudades.departamento_id')
            ->select(
                'juzgados.id',
                'juzgados.nombre',
                'departamentos.nombre AS departamento_nombre',
                'ciudades.nombre AS ciudad_nombre',
                'juzgados.juzgado',
                'juzgados.sala',
                'juzgados.juzgado_rama',
                'juzgados.juzgado_actual',
                'juzgados.estado',
                'juzgados.usuario_creacion_id',
                'juzgados.usuario_creacion_nombre',
                'juzgados.usuario_modificacion_id',
                'juzgados.usuario_modificacion_nombre',
                'juzgados.created_at AS fecha_creacion',
                'juzgados.updated_at AS fecha_modificacion'
            );

        //filtros
        if(isset($dto['nombre'])){
            $query->where('juzgados.nombre', 'like', '%' . $dto['nombre'] . '%');
        }
        if(isset($dto['ciudad_nombre'])){
            $query->where('ciudades.nombre', 'like', '%' . $dto['ciudad_nombre'] . '%');
        }
        if(isset($dto['departamento_nombre'])){
            $query->where('departamentos.nombre', 'like', '%' . $dto['departamento_nombre'] . '%');
        }
        if(isset($dto['juzgado_corporacion'])){
            $query->where('juzgados.juzgado', '=', $dto['juzgado_corporacion']);
        }
        if(isset($dto['sala_especialidad'])){
            $query->where('juzgados.sala', '=', $dto['sala_especialidad']);
        }

        if (isset($dto['ordenar_por']) && count($dto['ordenar_por']) > 0){
            foreach ($dto['ordenar_por'] as $attribute => $value){
                if($attribute == 'nombre'){
                    $query->orderBy('juzgados.nombre', $value);
                }
                if($attribute == 'departamento_nombre'){
                    $query->orderBy('juzgados.departamento_id', $value);
                }
                if($attribute == 'ciudad_nombre'){
                    $query->orderBy('juzgados.ciudad_id', $value);
                }
                if($attribute == 'juzgado'){
                    $query->orderBy('juzgados.juzgado', $value);
                }
                if($attribute == 'sala'){
                    $query->orderBy('juzgados.sala', $value);
                }
                if($attribute == 'juzgado_rama'){
                    $query->orderBy('juzgados.juzgado_rama', $value);
                }
                if($attribute == 'juzgado_actual'){
                    $query->orderBy('juzgados.juzgado_actual', $value);
                }
                if($attribute == 'estado'){
                    $query->orderBy('juzgados.estado', $value);
                }
            }
        }else{
            $query->orderBy("juzgados.id", "desc");
        }

        $juzgados = $query->paginate($dto['limite'] ?? 100);
        return $juzgados;
    }
}
