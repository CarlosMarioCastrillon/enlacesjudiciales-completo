<?php

namespace App\Model\Configuracion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class IPC extends Model
{

    protected $table = 'indice_IPC';

    protected $fillable = [
        'fecha',
        'indice_IPC',
        'inflacion_anual',
        'inflacion_mensual',
        'inflacion_anio',
        'estado',
        'usuario_creacion_id',
        'usuario_creacion_nombre',
        'usuario_modificacion_id',
        'usuario_modificacion_nombre',
    ];

    public static function obtenerColeccionLigera($dto){
        $query = DB::table('indice_IPC')
            ->select(
                'id','fecha', 'indice_IPC', 'inflacion_anual', 'inflacion_mensual', 'inflacion_anio'
            )
            ->where('estado','=','1');
        return $query->get();
    }

    public static function obtenerColeccion($dto){
        $query = DB::table('indice_IPC')
            ->select(
                'indice_IPC.id',
                'indice_IPC.fecha',
                'indice_IPC.indice_IPC',
                'indice_IPC.inflacion_anual',
                'indice_IPC.inflacion_mensual',
                'indice_IPC.inflacion_anio',
                'indice_IPC.estado',
                'indice_IPC.usuario_creacion_id',
                'indice_IPC.usuario_creacion_nombre',
                'indice_IPC.usuario_modificacion_id',
                'indice_IPC.usuario_modificacion_nombre',
                'indice_IPC.created_at AS fecha_creacion',
                'indice_IPC.updated_at AS fecha_modificacion'
            );

        if(isset($dto['fecha'])){
            $query->where('indice_IPC.fecha', '>=', $dto['fecha']);
        }

        if (isset($dto['ordenar_por']) && count($dto['ordenar_por']) > 0){
            foreach ($dto['ordenar_por'] as $attribute => $value){
                if($attribute == 'fecha'){
                    $query->orderBy('indice_IPC.fecha', $value);
                }
                if($attribute == 'indice_IPC'){
                    $query->orderBy('indice_IPC.indice_IPC', $value);
                }
                if($attribute == 'inflacion_anual'){
                    $query->orderBy('indice_IPC.inflacion_anual', $value);
                }
                if($attribute == 'inflacion_mensual'){
                    $query->orderBy('indice_IPC.inflacion_mensual', $value);
                }
                if($attribute == 'inflacion_anio'){
                    $query->orderBy('indice_IPC.inflacion_anio', $value);
                }
                if($attribute == 'estado'){
                    $query->orderBy('indice_IPC.estado', $value);
                }
                if($attribute == 'usuario_creacion_nombre'){
                    $query->orderBy('indice_IPC.usuario_creacion_nombre', $value);
                }
                if($attribute == 'usuario_modificacion_nombre'){
                    $query->orderBy('indice_IPC.usuario_modificacion_nombre', $value);
                }
                if($attribute == 'fecha_creacion'){
                    $query->orderBy('indice_IPC.created_at', $value);
                }
                if($attribute == 'fecha_modificacion'){
                    $query->orderBy('indice_IPC.updated_at', $value);
                }
            }
        }else{
            $query->orderBy("indice_IPC.id", "desc");
        }

        $indice_IPC = $query->paginate($dto['limite'] ?? 100);
        return $indice_IPC;
    }

}
