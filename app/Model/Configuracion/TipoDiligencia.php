<?php

namespace App\Model\Configuracion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TipoDiligencia extends Model
{

    protected $table = 'tipos_de_diligencias';

    protected $fillable = [
        'nombre',
        'solicitud_autorizacion',
        'estado',
        'usuario_creacion_id',
        'usuario_creacion_nombre',
        'usuario_modificacion_id',
        'usuario_modificacion_nombre',
    ];

    public static function obtenerColeccionLigera($dto){
        $query = DB::table('tipos_de_diligencias')
            ->select(
                'id',
                'nombre')
            ->where('estado','=','1');

        return $query->get();
    }

    public static function obtenerColeccion($dto){
        $query = DB::table('tipos_de_diligencias')
            ->select(
                'tipos_de_diligencias.id',
                'tipos_de_diligencias.nombre',
                'tipos_de_diligencias.solicitud_autorizacion',
                'tipos_de_diligencias.estado',
                'tipos_de_diligencias.usuario_creacion_id',
                'tipos_de_diligencias.usuario_creacion_nombre',
                'tipos_de_diligencias.usuario_modificacion_id',
                'tipos_de_diligencias.usuario_modificacion_nombre',
                'tipos_de_diligencias.created_at AS fecha_creacion',
                'tipos_de_diligencias.updated_at AS fecha_modificacion'
            );

        if(isset($dto['nombre'])){
            $query->where('tipos_de_diligencias.nombre', 'like', '%' . $dto['nombre'] . '%');
        }

        if (isset($dto['ordenar_por']) && count($dto['ordenar_por']) > 0){
            foreach ($dto['ordenar_por'] as $attribute => $value){
                if($attribute == 'nombre'){
                    $query->orderBy('tipos_de_diligencias.nombre', $value);
                }
                if($attribute == 'solicitud_autorizacion'){
                    $query->orderBy('tipos_de_diligencias.solicitud_autorizacion', $value);
                }
                if($attribute == 'estado'){
                    $query->orderBy('tipos_de_diligencias.estado', $value);
                }
                if($attribute == 'usuario_creacion_nombre'){
                    $query->orderBy('tipos_de_diligencias.usuario_creacion_nombre', $value);
                }
                if($attribute == 'usuario_modificacion_nombre'){
                    $query->orderBy('tipos_de_diligencias.usuario_modificacion_nombre', $value);
                }
                if($attribute == 'fecha_creacion'){
                    $query->orderBy('tipos_de_diligencias.created_at', $value);
                }
                if($attribute == 'fecha_modificacion'){
                    $query->orderBy('tipos_de_diligencias.updated_at', $value);
                }
            }
        }else{
            $query->orderBy("tipos_de_diligencias.id", "desc");
        }

        $tiposDeDiligencia = $query->paginate($dto['limite'] ?? 100);
        return $tiposDeDiligencia;
    }
}
