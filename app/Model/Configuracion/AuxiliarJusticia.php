<?php

namespace App\Model\Configuracion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AuxiliarJusticia extends Model
{

    protected $table = 'auxiliares_justicia';

    protected $fillable = [
        'numero_documento',
        'nombres_auxiliar',
        'apellidos_auxiliar',
        'nombre_ciudad',
        'direccion_oficina',
        'telefono_oficina',
        'cargo',
        'telefono_celular',
        'direccion_residencia',
        'telefono_residencia',
        'correo_electronico',
        'estado',
        'empresa_id',
        'usuario_creacion_id',
        'usuario_creacion_nombre',
        'usuario_modificacion_id',
        'usuario_modificacion_nombre',
    ];

    public static function obtenerColeccionLigera($dto){
        $user = Auth::user();
        $empresa = $user->empresa();
        $dto['empresa_id'] = $empresa->id;
        $query = DB::table('auxiliares_justicia')
            ->select(
                'id','nombre'
            )
            ->where('auxiliares_justicia.empresa_id', '=',  $dto['empresa_id'])
            ->where('estado','=','1');
        return $query->get();
    }

    public static function obtenerColeccion($dto){
        $user = Auth::user();
        $empresa = $user->empresa();
        $dto['empresa_id'] = $empresa->id;
        $query = DB::table('auxiliares_justicia')
            ->select(
                'auxiliares_justicia.id',
                'auxiliares_justicia.numero_documento',
                'auxiliares_justicia.nombres_auxiliar',
                'auxiliares_justicia.apellidos_auxiliar',
                'auxiliares_justicia.nombre_ciudad',
                'auxiliares_justicia.direccion_oficina',
                'auxiliares_justicia.telefono_oficina',
                'auxiliares_justicia.cargo',
                'auxiliares_justicia.telefono_celular',
                'auxiliares_justicia.direccion_residencia',
                'auxiliares_justicia.telefono_residencia',
                'auxiliares_justicia.correo_electronico',
                'auxiliares_justicia.estado',
                'auxiliares_justicia.empresa_id',
                'auxiliares_justicia.usuario_creacion_id',
                'auxiliares_justicia.usuario_creacion_nombre',
                'auxiliares_justicia.usuario_modificacion_id',
                'auxiliares_justicia.usuario_modificacion_nombre',
                'auxiliares_justicia.created_at AS fecha_creacion',
                'auxiliares_justicia.updated_at AS fecha_modificacion'
            )->where('auxiliares_justicia.empresa_id', '=',  $dto['empresa_id']);

        if(isset($dto['numero_documento'])){
            $query->where('auxiliares_justicia.numero_documento', 'like', '%' . $dto['numero_documento'] . '%');
        }
        if(isset($dto['nombres_auxiliar'])){
            $query->where('auxiliares_justicia.nombres_auxiliar', 'like', '%' . $dto['nombres_auxiliar'] . '%');
        }
        if(isset($dto['apellidos_auxiliar'])){
            $query->where('auxiliares_justicia.apellidos_auxiliar', 'like', '%' . $dto['apellidos_auxiliar'] . '%');
        }
        if(isset($dto['nombre_ciudad'])){
            $query->where('auxiliares_justicia.nombre_ciudad', 'like', '%' . $dto['nombre_ciudad'] . '%');
        }
        if(isset($dto['estado'])){
            $query->where('auxiliares_justicia.estado', 'like', '%' . $dto['estado'] . '%');
        }

        if (isset($dto['ordenar_por']) && count($dto['ordenar_por']) > 0){
            foreach ($dto['ordenar_por'] as $attribute => $value){
                if($attribute == 'numero_documento'){
                    $query->orderBy('auxiliares_justicia.numero_documento', $value);
                }
                if($attribute == 'nombres_auxiliar'){
                    $query->orderBy('auxiliares_justicia.nombres_auxiliar', $value);
                }
                if($attribute == 'apellidos_auxiliar'){
                    $query->orderBy('auxiliares_justicia.apellidos_auxiliar', $value);
                }
                if($attribute == 'nombre_ciudad'){
                    $query->orderBy('auxiliares_justicia.nombre_ciudad', $value);
                }
                if($attribute == 'direccion_oficina'){
                    $query->orderBy('auxiliares_justicia.direccion_oficina', $value);
                }
                if($attribute == 'telefono_oficina'){
                    $query->orderBy('auxiliares_justicia.telefono_oficina', $value);
                }
                if($attribute == 'cargo'){
                    $query->orderBy('auxiliares_justicia.cargo', $value);
                }
                if($attribute == 'telefono_celular'){
                    $query->orderBy('auxiliares_justicia.telefono_celular', $value);
                }
                if($attribute == 'direccion_residencia'){
                    $query->orderBy('auxiliares_justicia.direccion_oficina', $value);
                }
                if($attribute == 'telefono_residencia'){
                    $query->orderBy('auxiliares_justicia.telefono_residencia', $value);
                }
                if($attribute == 'correo_electronico'){
                    $query->orderBy('auxiliares_justicia.correo_electronico', $value);
                }
                if($attribute == 'estado'){
                    $query->orderBy('auxiliares_justicia.estado', $value);
                }
                if($attribute == 'usuario_creacion_nombre'){
                    $query->orderBy('auxiliares_justicia.usuario_creacion_nombre', $value);
                }
                if($attribute == 'usuario_modificacion_nombre'){
                    $query->orderBy('auxiliares_justicia.usuario_modificacion_nombre', $value);
                }
                if($attribute == 'fecha_creacion'){
                    $query->orderBy('auxiliares_justicia.created_at', $value);
                }
                if($attribute == 'fecha_modificacion'){
                    $query->orderBy('auxiliares_justicia.updated_at', $value);
                }
            }
        }else{
            $query->orderBy("auxiliares_justicia.id", "desc");
        }

        $auxiliares_justicia = $query->paginate($dto['limite'] ?? 100);
        return $auxiliares_justicia;
    }

}
