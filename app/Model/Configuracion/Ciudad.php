<?php

namespace App\Model\Configuracion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Ciudad extends Model
{

    protected $table = 'ciudades';

    protected $fillable = [
        'nombre',
        'codigo_dane',
        'departamento_id',
        'departamento_nombre',
        'rama_judicial',
        'estado',
        'usuario_creacion_id',
        'usuario_creacion_nombre',
        'usuario_modificacion_id',
        'usuario_modificacion_nombre',
    ];

    public function departamento(){
        return $this->belongsTo(Departamento::class);
    }

    public static function findBy($codigoDane){
        return Ciudad::where('codigo_dane', $codigoDane)->first();
    }

    public static function inactivarCiudadesDepartamento($dto){
        $query = DB::table('ciudades')
            ->where('ciudades.departamento_id', '=',$dto['departamento_id'])
            ->update(['estado' => 0]);
    }

    public static function obtenerColeccionLigera($dto){
        $query = DB::table('ciudades');
        if(isset($dto['con_cobertura']) && $dto['con_cobertura'] == "true"){
            $query->join('coberturas', function($join) use($dto) {
                $join->on('coberturas.ciudad_id', '=', 'ciudades.id')
                    ->where('empresa_id', $dto['empresa_id']);
            });
        }

        if(isset($dto['departamento_id'])){
            $query->where('ciudades.departamento_id','=', $dto['departamento_id']);
        }

        $query->select(
            'ciudades.id',
            'ciudades.nombre',
            'ciudades.codigo_dane'
        )->where('ciudades.estado','=',true);

        return $query->get();
    }
    public static function obtenerColeccion($dto){
        $query = DB::table('ciudades')
            ->join('departamentos', 'departamentos.id', '=', 'ciudades.departamento_id')
            ->select(
                'ciudades.id',
                'ciudades.nombre',
                'ciudades.codigo_dane',
                'departamentos.nombre AS departamento_nombre',
                'ciudades.rama_judicial',
                'ciudades.estado',
                'ciudades.usuario_creacion_id',
                'ciudades.usuario_creacion_nombre',
                'ciudades.usuario_modificacion_id',
                'ciudades.usuario_modificacion_nombre',
                'ciudades.created_at AS fecha_creacion',
                'ciudades.updated_at AS fecha_modificacion'
            );

        //filtros
        if(isset($dto['nombre'])){
            $query->where('ciudades.nombre', 'like', '%' . $dto['nombre'] . '%');
        }
        if(isset($dto['departamento'])){
            $query->where('departamentos.nombre', 'like', '%' . $dto['departamento'] . '%');
        }

        if (isset($dto['ordenar_por']) && count($dto['ordenar_por'] ?? []) > 0){
            foreach ($dto['ordenar_por'] as $attribute => $value){
                if($attribute == 'nombre'){
                    $query->orderBy('ciudades.nombre', $value);
                }
                if($attribute == 'codigo_dane'){
                    $query->orderBy('ciudades.codigo_dane', $value);
                }
                if($attribute == 'departamento_nombre'){
                    $query->orderBy('departamentos.nombre', $value);
                }
                if($attribute == 'rama_judicial'){
                    $query->orderBy('ciudades.rama_judicial', $value);
                }
                if($attribute == 'estado'){
                    $query->orderBy('ciudades.estado', $value);
                }
                if($attribute == 'usuario_creacion_nombre'){
                    $query->orderBy('ciudades.usuario_creacion_nombre', $value);
                }
                if($attribute == 'usuario_modificacion_nombre'){
                    $query->orderBy('ciudades.usuario_modificacion_nombre', $value);
                }
                if($attribute == 'fecha_creacion'){
                    $query->orderBy('ciudades.created_at', $value);
                }
                if($attribute == 'fecha_modificacion'){
                    $query->orderBy('ciudades.updated_at', $value);
                }
            }
        }else{
            $query->orderBy("ciudades.id", "desc");
        }

        $ciudades = $query->paginate($dto['limite'] ?? 100);
        return $ciudades;
    }
}
