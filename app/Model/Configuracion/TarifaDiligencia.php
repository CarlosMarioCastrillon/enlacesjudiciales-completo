<?php

namespace App\Model\Configuracion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TarifaDiligencia extends Model
{

    protected $table = 'tarifas_diligencias';

    protected $fillable = [
        'tipo_diligencia_id',
        'departamento_id',
        'ciudad_id',
        'valor_diligencia',
        'gasto_envio',
        'otros_gastos',
        'estado',
        'usuario_creacion_id',
        'usuario_creacion_nombre',
        'usuario_modificacion_id',
        'usuario_modificacion_nombre',
    ];

    public function ciudad(){
        return $this->belongsTo(Ciudad::class);
    }
    public function departamento(){
        return $this->belongsTo(Departamento::class);
    }
    public function tipo_diligencia(){
        return $this->belongsTo(TipoDiligencia::class);
    }

    public static function obtenerColeccion($dto){
        $query = DB::table('tarifas_diligencias')
            ->join('ciudades', 'ciudades.id', '=', 'tarifas_diligencias.ciudad_id')
            ->join('departamentos', 'departamentos.id', '=', 'ciudades.departamento_id')
            ->join('tipos_de_diligencias', 'tipos_de_diligencias.id', '=', 'tarifas_diligencias.tipo_diligencia_id')
            ->select(
                'tarifas_diligencias.id',
                'tipos_de_diligencias.nombre As tipo_diligencia_nombre',
                'departamentos.nombre As departamento_nombre',
                'ciudades.nombre As ciudad_nombre',
                'tarifas_diligencias.valor_diligencia',
                'tarifas_diligencias.gasto_envio',
                'tarifas_diligencias.otros_gastos',
                'tarifas_diligencias.estado',
                'tarifas_diligencias.usuario_creacion_id',
                'tarifas_diligencias.usuario_creacion_nombre',
                'tarifas_diligencias.usuario_modificacion_id',
                'tarifas_diligencias.usuario_modificacion_nombre',
                'tarifas_diligencias.created_at AS fecha_creacion',
                'tarifas_diligencias.updated_at AS fecha_modificacion'
            );

        if(isset($dto['ciudad_nombre'])){
            $query->where('ciudades.nombre', 'like', '%' . $dto['ciudad_nombre'] . '%');
        }
        if(isset($dto['tipo_diligencia_nombre'])){
            $query->where('tipos_de_diligencias.nombre', 'like', '%' . $dto['tipo_diligencia_nombre'] . '%');
        }

        if (isset($dto['ordenar_por']) && count($dto['ordenar_por']) > 0){
            foreach ($dto['ordenar_por'] as $attribute => $value){
                if($attribute == 'tipo_diligencia'){
                    $query->orderBy('tarifas_diligencias.tipo_diligencia_id', $value);
                }
                if($attribute == 'ciudad'){
                    $query->orderBy('tarifas_diligencias.ciudad_id', $value);
                }
                if($attribute == 'departamento'){
                    $query->orderBy('tarifas_diligencias.departamento_id', $value);
                }
                if($attribute == 'valor_diligencia'){
                    $query->orderBy('tarifas_diligencias.valor_diligencia', $value);
                }
                if($attribute == 'gasto_envio'){
                    $query->orderBy('tarifas_diligencias.gasto_envio', $value);
                }
                if($attribute == 'estado'){
                    $query->orderBy('tarifas_diligencias.estado', $value);
                }
                if($attribute == 'usuario_creacion_nombre'){
                    $query->orderBy('tarifas_diligencias.usuario_creacion_nombre', $value);
                }
                if($attribute == 'usuario_modificacion_nombre'){
                    $query->orderBy('tarifas_diligencias.usuario_modificacion_nombre', $value);
                }
                if($attribute == 'fecha_creacion'){
                    $query->orderBy('tarifas_diligencias.created_at', $value);
                }
                if($attribute == 'fecha_modificacion'){
                    $query->orderBy('tarifas_diligencias.updated_at', $value);
                }
            }
        }else{
            $query->orderBy("tarifas_diligencias.id", "desc");
        }

        $tarifas_diligencias = $query->paginate($dto['limite'] ?? 100);
        return $tarifas_diligencias;
    }

    public static function obtener($dto){
        $query = DB::table('tarifas_diligencias')
            ->join('ciudades', 'ciudades.id', '=', 'tarifas_diligencias.ciudad_id')
            ->join('departamentos', 'departamentos.id', '=', 'ciudades.departamento_id')
            ->join('tipos_de_diligencias', 'tipos_de_diligencias.id', '=', 'tarifas_diligencias.tipo_diligencia_id')
            ->select(
                'tarifas_diligencias.valor_diligencia',
                'tarifas_diligencias.gasto_envio',
                'tarifas_diligencias.otros_gastos'
            )
            ->where('tipos_de_diligencias.id', '=', $dto['tipo_diligencia_id'])
            ->where('tarifas_diligencias.estado', '=', true);
            if(isset($dto['departamento_id'])) {
                $query->where('departamentos.id', '=', $dto['departamento_id']);
            }
            if(isset($dto['ciudad_id'])) {
                $query->where('ciudades.id', '=', $dto['ciudad_id']);
            }

        return $query->get();
    }
}
