<?php

namespace App\Model\Configuracion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TipoDocumento extends Model
{

    protected $table = 'tipos_de_documentos';

    protected $fillable = [
        'nombre',
        'codigo',
        'estado',
        'usuario_creacion_id',
        'usuario_creacion_nombre',
        'usuario_modificacion_id',
        'usuario_modificacion_nombre',
    ];

    public static function obtenerColeccionLigera($dto){
        $query = DB::table('tipos_de_documentos')
            ->select(
                'id',
                'codigo',
                'nombre'
            )
            ->where('estado','=',true);

        return $query
            ->orderBy('nombre', 'asc')
            ->get();
    }

    public static function obtenerColeccion($dto){
        $query = DB::table('tipos_de_documentos')
            ->select(
                'tipos_de_documentos.id',
                'tipos_de_documentos.codigo',
                'tipos_de_documentos.nombre',
                'tipos_de_documentos.estado',
                'tipos_de_documentos.usuario_creacion_id',
                'tipos_de_documentos.usuario_creacion_nombre',
                'tipos_de_documentos.usuario_modificacion_id',
                'tipos_de_documentos.usuario_modificacion_nombre',
                'tipos_de_documentos.created_at AS fecha_creacion',
                'tipos_de_documentos.updated_at AS fecha_modificacion'
            );

        if(isset($dto['nombre'])){
            $query->where('tipos_de_documentos.nombre', 'like', '%' . $dto['nombre'] . '%');
        }

        if (isset($dto['ordenar_por']) && count($dto['ordenar_por']) > 0){
            foreach ($dto['ordenar_por'] as $attribute => $value){
                if($attribute == 'codigo'){
                    $query->orderBy('tipos_de_documentos.codigo', $value);
                }
                if($attribute == 'nombre'){
                    $query->orderBy('tipos_de_documentos.nombre', $value);
                }
                if($attribute == 'estado'){
                    $query->orderBy('tipos_de_documentos.estado', $value);
                }
                if($attribute == 'usuario_creacion_nombre'){
                    $query->orderBy('tipos_de_documentos.usuario_creacion_nombre', $value);
                }
                if($attribute == 'usuario_modificacion_nombre'){
                    $query->orderBy('tipos_de_documentos.usuario_modificacion_nombre', $value);
                }
                if($attribute == 'fecha_creacion'){
                    $query->orderBy('tipos_de_documentos.created_at', $value);
                }
                if($attribute == 'fecha_modificacion'){
                    $query->orderBy('tipos_de_documentos.updated_at', $value);
                }
            }
        }else{
            $query->orderBy("tipos_de_documentos.id", "desc");
        }

        $tiposDeEmpresa = $query->paginate($dto['limite'] ?? 100);
        return $tiposDeEmpresa;
    }
}
