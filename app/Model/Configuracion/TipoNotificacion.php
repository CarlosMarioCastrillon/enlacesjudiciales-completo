<?php

namespace App\Model\Configuracion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TipoNotificacion extends Model
{

    protected $table = 'tipos_notificaciones';

    protected $fillable = [
        'nombre',
        'estado',
        'usuario_creacion_id',
        'usuario_creacion_nombre',
        'usuario_modificacion_id',
        'usuario_modificacion_nombre',
    ];

    public static function obtenerColeccionLigera($dto){
        $query = DB::table('tipos_notificaciones')
            ->select(
                'id','nombre'
            )
            ->where('estado','=','1');
        return $query->get();
    }

    public static function obtenerColeccion($dto){
        $query = DB::table('tipos_notificaciones')
            ->select(
                'tipos_notificaciones.id',
                'tipos_notificaciones.nombre',
                'tipos_notificaciones.estado',
                'tipos_notificaciones.usuario_creacion_id',
                'tipos_notificaciones.usuario_creacion_nombre',
                'tipos_notificaciones.usuario_modificacion_id',
                'tipos_notificaciones.usuario_modificacion_nombre',
                'tipos_notificaciones.created_at AS fecha_creacion',
                'tipos_notificaciones.updated_at AS fecha_modificacion'
            );

        if(isset($dto['nombre'])){
            $query->where('tipos_notificaciones.nombre', 'like', '%' . $dto['nombre'] . '%');
        }

        if (isset($dto['ordenar_por']) && count($dto['ordenar_por']) > 0){
            foreach ($dto['ordenar_por'] as $attribute => $value){
                if($attribute == 'nombre'){
                    $query->orderBy('tipos_notificaciones.nombre', $value);
                }
                if($attribute == 'estado'){
                    $query->orderBy('tipos_notificaciones.estado', $value);
                }
                if($attribute == 'usuario_creacion_nombre'){
                    $query->orderBy('tipos_notificaciones.usuario_creacion_nombre', $value);
                }
                if($attribute == 'usuario_modificacion_nombre'){
                    $query->orderBy('tipos_notificaciones.usuario_modificacion_nombre', $value);
                }
                if($attribute == 'fecha_creacion'){
                    $query->orderBy('tipos_notificaciones.created_at', $value);
                }
                if($attribute == 'fecha_modificacion'){
                    $query->orderBy('tipos_notificaciones.updated_at', $value);
                }
            }
        }else{
            $query->orderBy("tipos_notificaciones.id", "desc");
        }

        $tiposNotificaciones = $query->paginate($dto['limite'] ?? 100);
        return $tiposNotificaciones;
    }

}
