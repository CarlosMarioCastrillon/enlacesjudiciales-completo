<?php

namespace App\Model\Configuracion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ClaseProceso extends Model
{

    protected $table = 'clases_de_procesos';

    protected $fillable = [
        'nombre',
        'estado',
        'usuario_creacion_id',
        'usuario_creacion_nombre',
        'usuario_modificacion_id',
        'usuario_modificacion_nombre',
    ];

    public static function obtenerColeccionLigera($dto){
        $query = DB::table('clases_de_procesos')
            ->select(
                'id',
                'nombre')
            ->where('estado','=','1');

        return $query->get();
    }

    public static function obtenerColeccion($dto){
        $query = DB::table('clases_de_procesos')
            ->select(
                'clases_de_procesos.id',
                'clases_de_procesos.nombre',
                'clases_de_procesos.estado',
                'clases_de_procesos.usuario_creacion_id',
                'clases_de_procesos.usuario_creacion_nombre',
                'clases_de_procesos.usuario_modificacion_id',
                'clases_de_procesos.usuario_modificacion_nombre',
                'clases_de_procesos.created_at AS fecha_creacion',
                'clases_de_procesos.updated_at AS fecha_modificacion'
            );

        if(isset($dto['nombre'])){
            $query->where('clases_de_procesos.nombre', 'like', '%' . $dto['nombre'] . '%');
        }

        if (isset($dto['ordenar_por']) && count($dto['ordenar_por']) > 0){
            foreach ($dto['ordenar_por'] as $attribute => $value){
                if($attribute == 'nombre'){
                    $query->orderBy('clases_de_procesos.nombre', $value);
                }
                if($attribute == 'estado'){
                    $query->orderBy('clases_de_procesos.estado', $value);
                }
                if($attribute == 'usuario_creacion_nombre'){
                    $query->orderBy('clases_de_procesos.usuario_creacion_nombre', $value);
                }
                if($attribute == 'usuario_modificacion_nombre'){
                    $query->orderBy('clases_de_procesos.usuario_modificacion_nombre', $value);
                }
                if($attribute == 'fecha_creacion'){
                    $query->orderBy('clases_de_procesos.created_at', $value);
                }
                if($attribute == 'fecha_modificacion'){
                    $query->orderBy('clases_de_procesos.updated_at', $value);
                }
            }
        }else{
            $query->orderBy("clases_de_procesos.id", "desc");
        }

        $clasesProcesos = $query->paginate($dto['limite'] ?? 100);
        return $clasesProcesos;
    }
}
