<?php

namespace App\Model\Configuracion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TipoActuacion extends Model
{

    protected $table = 'tipos_de_actuaciones';

    protected $fillable = [
        'nombre',
        'estado',
        'usuario_creacion_id',
        'usuario_creacion_nombre',
        'usuario_modificacion_id',
        'usuario_modificacion_nombre',
    ];

    public static function obtenerColeccionLigera($dto){
        $query = DB::table('tipos_de_actuaciones')
            ->select(
                'id',
                'nombre')
            ->where('estado','=','1');

        return $query->get();
    }

    public static function obtenerColeccion($dto){
        $query = DB::table('tipos_de_actuaciones')
            ->select(
                'tipos_de_actuaciones.id',
                'tipos_de_actuaciones.nombre',
                'tipos_de_actuaciones.estado',
                'tipos_de_actuaciones.usuario_creacion_id',
                'tipos_de_actuaciones.usuario_creacion_nombre',
                'tipos_de_actuaciones.usuario_modificacion_id',
                'tipos_de_actuaciones.usuario_modificacion_nombre',
                'tipos_de_actuaciones.created_at AS fecha_creacion',
                'tipos_de_actuaciones.updated_at AS fecha_modificacion'
            );

        if(isset($dto['nombre'])){
            $query->where('tipos_de_actuaciones.nombre', 'like', '%' . $dto['nombre'] . '%');
        }

        if (isset($dto['ordenar_por']) && count($dto['ordenar_por']) > 0){
            foreach ($dto['ordenar_por'] as $attribute => $value){
                if($attribute == 'nombre'){
                    $query->orderBy('tipos_de_actuaciones.nombre', $value);
                }
                if($attribute == 'estado'){
                    $query->orderBy('tipos_de_actuaciones.estado', $value);
                }
                if($attribute == 'usuario_creacion_nombre'){
                    $query->orderBy('tipos_de_actuaciones.usuario_creacion_nombre', $value);
                }
                if($attribute == 'usuario_modificacion_nombre'){
                    $query->orderBy('tipos_de_actuaciones.usuario_modificacion_nombre', $value);
                }
                if($attribute == 'fecha_creacion'){
                    $query->orderBy('tipos_de_actuaciones.created_at', $value);
                }
                if($attribute == 'fecha_modificacion'){
                    $query->orderBy('tipos_de_actuaciones.updated_at', $value);
                }
            }
        }else{
            $query->orderBy("tipos_de_actuaciones.id", "desc");
        }

        $tiposDeactuaciones = $query->paginate($dto['limite'] ?? 100);
        return $tiposDeactuaciones;
    }
}
