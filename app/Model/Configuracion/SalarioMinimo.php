<?php

namespace App\Model\Configuracion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SalarioMinimo extends Model
{

    protected $table = 'salarios_minimos';

    protected $fillable = [
        'fecha',
        'valor_salario_min',
        'estado',
        'usuario_creacion_id',
        'usuario_creacion_nombre',
        'usuario_modificacion_id',
        'usuario_modificacion_nombre',
    ];

    public static function obtenerColeccionLigera($dto){
        $query = DB::table('salarios_minimos')
            ->select(
                'id','fecha', 'valor_salario_min'
            )
            ->where('estado','=','1');
        return $query->get();
    }

    public static function obtenerColeccion($dto){
        $query = DB::table('salarios_minimos')
            ->select(
                'salarios_minimos.id',
                'salarios_minimos.fecha',
                'salarios_minimos.valor_salario_min',
                'salarios_minimos.estado',
                'salarios_minimos.usuario_creacion_id',
                'salarios_minimos.usuario_creacion_nombre',
                'salarios_minimos.usuario_modificacion_id',
                'salarios_minimos.usuario_modificacion_nombre',
                'salarios_minimos.created_at AS fecha_creacion',
                'salarios_minimos.updated_at AS fecha_modificacion'
            );

        if(isset($dto['fecha'])){
            $query->where('salarios_minimos.fecha', '>=', $dto['fecha']);
        }

        if (isset($dto['ordenar_por']) && count($dto['ordenar_por']) > 0){
            foreach ($dto['ordenar_por'] as $attribute => $value){
                if($attribute == 'fecha'){
                    $query->orderBy('salarios_minimos.fecha', $value);
                }
                if($attribute == 'valor'){
                    $query->orderBy('salarios_minimos.valor_salario_min', $value);
                }
                if($attribute == 'estado'){
                    $query->orderBy('salarios_minimos.estado', $value);
                }
                if($attribute == 'usuario_creacion_nombre'){
                    $query->orderBy('salarios_minimos.usuario_creacion_nombre', $value);
                }
                if($attribute == 'usuario_modificacion_nombre'){
                    $query->orderBy('salarios_minimos.usuario_modificacion_nombre', $value);
                }
                if($attribute == 'fecha_creacion'){
                    $query->orderBy('salarios_minimos.created_at', $value);
                }
                if($attribute == 'fecha_modificacion'){
                    $query->orderBy('salarios_minimos.updated_at', $value);
                }
            }
        }else{
            $query->orderBy("salarios_minimos.id", "desc");
        }

        $salarios_minimos = $query->paginate($dto['limite'] ?? 100);
        return $salarios_minimos;
    }

}
