<?php

namespace App\Model\Configuracion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Cobertura extends Model
{

    protected $table = 'coberturas';

    protected $fillable = [
        'empresa_id',
        'ciudad_id',
        'usuario_creacion_id',
        'usuario_creacion_nombre',
        'usuario_modificacion_id',
        'usuario_modificacion_nombre'
    ];

    public function ciudad(){
        return $this->belongsTo(Ciudad::class);
    }
    public function departamento(){
        return $this->belongsTo(Departamento::class);
    }

    public static function obtenerColeccion($dto){
        $query = DB::table('coberturas')
            ->join('ciudades', 'ciudades.id', '=', 'coberturas.ciudad_id')
            ->join('departamentos', 'departamentos.id', '=', 'ciudades.departamento_id')
            ->select(
                'coberturas.id',
                'departamentos.id AS departamento_id',
                'departamentos.nombre AS departamento_nombre',
                'ciudades.nombre AS ciudad_nombre',
                'coberturas.empresa_id',
                'coberturas.usuario_creacion_id',
                'coberturas.usuario_creacion_nombre',
                'coberturas.usuario_modificacion_id',
                'coberturas.usuario_modificacion_nombre',
                'coberturas.created_at AS fecha_creacion',
                'coberturas.updated_at AS fecha_modificacion'
            )->where('coberturas.empresa_id', '=',  $dto['empresa_id'] );

        if(isset($dto['departamento'])){
            $query->where('departamentos.nombre', '=',  $dto['departamento'] );
        }


        if (isset($dto['ordenar_por']) && count($dto['ordenar_por']) > 0){
            foreach ($dto['ordenar_por'] as $attribute => $value){
                if($attribute == 'departamento_nombre'){
                    $query->orderBy('departamentos.nombre', $value);
                }
                if($attribute == 'ciudad_nombre'){
                    $query->orderBy('ciudades.nombre', $value);
                }
                if($attribute == 'usuario_creacion_nombre'){
                    $query->orderBy('coberturas.usuario_creacion_nombre', $value);
                }
                if($attribute == 'usuario_modificacion_nombre'){
                    $query->orderBy('coberturas.usuario_modificacion_nombre', $value);
                }
                if($attribute == 'fecha_creacion'){
                    $query->orderBy('coberturas.created_at', $value);
                }
                if($attribute == 'fecha_modificacion'){
                    $query->orderBy('coberturas.updated_at', $value);
                }
            }
        }else{
            $query->orderBy("coberturas.id", "desc");
        }

        $coberturas = $query->paginate($dto['limite'] ?? 100);
        return $coberturas;
    }
}
