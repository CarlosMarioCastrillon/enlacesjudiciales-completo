<?php

namespace App\Model\Configuracion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TES extends Model
{

    protected $table = 'TES';

    protected $fillable = [
        'fecha',
        'porcentaje_1anio',
        'porcentaje_5anio',
        'porcentaje_10anio',
        'estado',
        'usuario_creacion_id',
        'usuario_creacion_nombre',
        'usuario_modificacion_id',
        'usuario_modificacion_nombre',
    ];

    public static function obtenerColeccionLigera($dto){
        $query = DB::table('TES')
            ->select(
                'id','fecha', 'porcentaje_1anio', 'porcentaje_5anio', 'porcentaje_10anio'
            )
            ->where('estado','=','1');
        return $query->get();
    }

    public static function obtenerColeccion($dto){
        $query = DB::table('TES')
            ->select(
                'TES.id',
                'TES.fecha',
                'TES.porcentaje_1anio',
                'TES.porcentaje_5anio',
                'TES.porcentaje_10anio',
                'TES.estado',
                'TES.usuario_creacion_id',
                'TES.usuario_creacion_nombre',
                'TES.usuario_modificacion_id',
                'TES.usuario_modificacion_nombre',
                'TES.created_at AS fecha_creacion',
                'TES.updated_at AS fecha_modificacion'
            );

        if(isset($dto['fecha'])){
            $query->where('TES.fecha', '>=', $dto['fecha']);
        }

        if (isset($dto['ordenar_por']) && count($dto['ordenar_por']) > 0){
            foreach ($dto['ordenar_por'] as $attribute => $value){
                if($attribute == 'fecha'){
                    $query->orderBy('TES.fecha', $value);
                }
                if($attribute == 'porcentaje_1anio'){
                    $query->orderBy('TES.porcentaje_1anio', $value);
                }
                if($attribute == 'porcentaje_5anio'){
                    $query->orderBy('TES.porcentaje_5anio', $value);
                }
                if($attribute == 'porcentaje_10anio'){
                    $query->orderBy('TES.porcentaje_10anio', $value);
                }
                if($attribute == 'estado'){
                    $query->orderBy('TES.estado', $value);
                }
                if($attribute == 'usuario_creacion_nombre'){
                    $query->orderBy('TES.usuario_creacion_nombre', $value);
                }
                if($attribute == 'usuario_modificacion_nombre'){
                    $query->orderBy('TES.usuario_modificacion_nombre', $value);
                }
                if($attribute == 'fecha_creacion'){
                    $query->orderBy('TES.created_at', $value);
                }
                if($attribute == 'fecha_modificacion'){
                    $query->orderBy('TES.updated_at', $value);
                }
            }
        }else{
            $query->orderBy("TES.id", "desc");
        }

        $TES = $query->paginate($dto['limite'] ?? 100);
        return $TES;
    }

}
