<?php

namespace App\Model\Configuracion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Departamento extends Model
{

    protected $table = 'departamentos';

    protected $fillable = [
        'nombre',
        'codigo_dane',
        'estado',
        'usuario_creacion_id',
        'usuario_creacion_nombre',
        'usuario_modificacion_id',
        'usuario_modificacion_nombre',
    ];

    public static function findBy($codigoDane){
        return Departamento::where('codigo_dane', $codigoDane)->first();
    }

    public static function obtenerColeccionLigera($dto){
        $query = DB::table('departamentos')
            ->select(
                'id',
                'nombre',
                'codigo_dane'
            )
            ->where('estado','=','1');

        return $query->get();
    }

    public static function obtenerValorIndividual($dto){
        $query = DB::table('departamentos')
            ->select('codigo_dane')
            ->where('id', '=', '' .$dto['id']. '');
    }

    public static function obtenerColeccion($dto){
        $query = DB::table('departamentos')
            ->select(
                'departamentos.id',
                'departamentos.nombre',
                'departamentos.codigo_dane',
                'departamentos.estado',
                'departamentos.usuario_creacion_id',
                'departamentos.usuario_creacion_nombre',
                'departamentos.usuario_modificacion_id',
                'departamentos.usuario_modificacion_nombre',
                'departamentos.created_at AS fecha_creacion',
                'departamentos.updated_at AS fecha_modificacion'
            );

        if(isset($dto['nombre'])){
            $query->where('departamentos.nombre', 'like', '%' . $dto['nombre'] . '%');
        }

        if (isset($dto['ordenar_por']) && count($dto['ordenar_por']) > 0){
            foreach ($dto['ordenar_por'] as $attribute => $value){
                if($attribute == 'nombre'){
                    $query->orderBy('departamentos.nombre', $value);
                }
                if($attribute == 'codigo_dane'){
                    $query->orderBy('departamentos.codigo_dane', $value);
                }
                if($attribute == 'estado'){
                    $query->orderBy('departamentos.estado', $value);
                }
                if($attribute == 'usuario_creacion_nombre'){
                    $query->orderBy('departamentos.usuario_creacion_nombre', $value);
                }
                if($attribute == 'usuario_modificacion_nombre'){
                    $query->orderBy('departamentos.usuario_modificacion_nombre', $value);
                }
                if($attribute == 'fecha_creacion'){
                    $query->orderBy('departamentos.created_at', $value);
                }
                if($attribute == 'fecha_modificacion'){
                    $query->orderBy('departamentos.updated_at', $value);
                }
            }
        }else{
            $query->orderBy("departamentos.id", "desc");
        }

        $departamentos = $query->paginate($dto['limite'] ?? 100);
        return $departamentos;
    }
}
