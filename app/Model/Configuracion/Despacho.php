<?php

namespace App\Model\Configuracion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Despacho extends Model
{

    protected $table = 'despachos';

    public function ciudad(){
        return $this->belongsTo(Ciudad::class);
    }
    public function departamento(){
        return $this->belongsTo(Departamento::class);
    }

    protected $fillable = [
        'nombre',
        'departamento_id',
        'ciudad_id',
        'juzgado',
        'sala',
        'despacho',
        'estado',
        'usuario_creacion_id',
        'usuario_creacion_nombre',
        'usuario_modificacion_id',
        'usuario_modificacion_nombre',
    ];

    public static function obtenerColeccionLigera($dto){
        $query = DB::table('despachos')
            ->select(
                'id', 'nombre', 'sala', 'juzgado', 'despacho'
            )
            ->where('estado','=',true);

        if(isset($dto['ciudad_id'])){
            $query->where('ciudad_id', $dto['ciudad_id']);
        }
        if(isset($dto['juzgado_id'])){
            $juzgado = Juzgado::find($dto['juzgado_id']);
            $dto['sala'] = $juzgado->sala;
            $dto['juzgado'] = $juzgado->juzgado;
            $query->where('juzgado','=', $dto['juzgado'])
                    ->where('sala','=', $dto['sala']);
        }

        return $query->get();
    }

    public static function obtenerColeccionReferencia($dto){
        $query = DB::table('despachos')
            ->select(
                'id', 'nombre', 'juzgado', 'sala', 'despacho'
            )
            ->where('estado','=',true);

        return $query
            ->groupBy('nombre')
            ->groupBy('juzgado')
            ->groupBy('sala')
            ->groupBy('despacho')
            ->get();
    }

    public static function obtenerColeccion($dto){
        $query = DB::table('despachos')
            ->join('ciudades', 'ciudades.id', '=', 'despachos.ciudad_id')
            ->join('departamentos', 'departamentos.id', '=', 'ciudades.departamento_id')
            ->select(
                'despachos.id',
                'despachos.nombre',
                'departamentos.nombre AS departamento_nombre',
                'ciudades.nombre AS ciudad_nombre',
                'despachos.juzgado',
                'despachos.sala',
                'despachos.despacho',
                'despachos.estado',
                'despachos.usuario_creacion_id',
                'despachos.usuario_creacion_nombre',
                'despachos.usuario_modificacion_id',
                'despachos.usuario_modificacion_nombre',
                'despachos.created_at AS fecha_creacion',
                'despachos.updated_at AS fecha_modificacion'
            );

        //filtros
        if(isset($dto['nombre'])){
            $query->where('despachos.nombre', 'like', '%' . $dto['nombre'] . '%');
        }
        if(isset($dto['ciudad_nombre'])){
            $query->where('ciudades.nombre', 'like', '%' . $dto['ciudad_nombre'] . '%');
        }
        if(isset($dto['departamento_nombre'])){
            $query->where('departamentos.nombre', 'like', '%' . $dto['departamento_nombre'] . '%');
        }
        if(isset($dto['juzgado_corporacion'])){
            $query->where('despachos.juzgado', '=', $dto['juzgado_corporacion']);
        }
        if(isset($dto['sala_especialidad'])){
            $query->where('despachos.sala', '=', $dto['sala_especialidad']);
        }

        if (isset($dto['ordenar_por']) && count($dto['ordenar_por']) > 0){
            foreach ($dto['ordenar_por'] as $attribute => $value){
                if($attribute == 'nombre'){
                    $query->orderBy('despachos.nombre', $value);
                }
                if($attribute == 'departamento_nombre'){
                    $query->orderBy('despachos.departamento_id', $value);
                }
                if($attribute == 'ciudad'){
                    $query->orderBy('despachos.ciudad_id', $value);
                }
                if($attribute == 'juzgado'){
                    $query->orderBy('despachos.juzgado', $value);
                }
                if($attribute == 'sala'){
                    $query->orderBy('despachos.sala', $value);
                }
                if($attribute == 'despacho'){
                    $query->orderBy('despachos.despacho', $value);
                }
                if($attribute == 'estado'){
                    $query->orderBy('despachos.estado', $value);
                }
            }
        }else{
            $query->orderBy("despachos.id", "desc");
        }

        $juzgados = $query->paginate($dto['limite'] ?? 100);
        return $juzgados;
    }
}
