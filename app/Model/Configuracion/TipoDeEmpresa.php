<?php

namespace App\Model\Configuracion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TipoDeEmpresa extends Model
{

    protected $table = 'tipos_de_empresa';

    protected $fillable = [
        'nombre',
        'estado',
        'usuario_creacion_id',
        'usuario_creacion_nombre',
        'usuario_modificacion_id',
        'usuario_modificacion_nombre',
    ];

    public static function obtenerColeccionLigera($dto){
        $query = DB::table('tipos_de_empresa')
            ->select(
                'id',
                'nombre')
            ->where('estado','=','1');

        return $query->get();
    }

    public static function obtenerColeccion($dto){
        $query = DB::table('tipos_de_empresa')
            ->select(
                'tipos_de_empresa.id',
                'tipos_de_empresa.nombre',
                'tipos_de_empresa.estado',
                'tipos_de_empresa.usuario_creacion_id',
                'tipos_de_empresa.usuario_creacion_nombre',
                'tipos_de_empresa.usuario_modificacion_id',
                'tipos_de_empresa.usuario_modificacion_nombre',
                'tipos_de_empresa.created_at AS fecha_creacion',
                'tipos_de_empresa.updated_at AS fecha_modificacion'
            );

        if(isset($dto['nombre'])){
            $query->where('tipos_de_empresa.nombre', 'like', '%' . $dto['nombre'] . '%');
        }

        if (isset($dto['ordenar_por']) && count($dto['ordenar_por']) > 0){
            foreach ($dto['ordenar_por'] as $attribute => $value){
                if($attribute == 'nombre'){
                    $query->orderBy('tipos_de_empresa.nombre', $value);
                }
                if($attribute == 'estado'){
                    $query->orderBy('tipos_de_empresa.estado', $value);
                }
                if($attribute == 'usuario_creacion_nombre'){
                    $query->orderBy('tipos_de_empresa.usuario_creacion_nombre', $value);
                }
                if($attribute == 'usuario_modificacion_nombre'){
                    $query->orderBy('tipos_de_empresa.usuario_modificacion_nombre', $value);
                }
                if($attribute == 'fecha_creacion'){
                    $query->orderBy('tipos_de_empresa.created_at', $value);
                }
                if($attribute == 'fecha_modificacion'){
                    $query->orderBy('tipos_de_empresa.updated_at', $value);
                }
            }
        }else{
            $query->orderBy("tipos_de_empresa.id", "desc");
        }

        $tiposDeEmpresa = $query->paginate($dto['limite'] ?? 100);
        return $tiposDeEmpresa;
    }
}
