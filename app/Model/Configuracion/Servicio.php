<?php

namespace App\Model\Configuracion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Servicio extends Model
{

    protected $fillable = [
        'nombre',
        'estado',
        'usuario_creacion_id',
        'usuario_creacion_nombre',
        'usuario_modificacion_id',
        'usuario_modificacion_nombre',
    ];

    public static function obtenerColeccionLigera($dto){
        $query = DB::table('servicios')
            ->select(
                'id','nombre'
            )
            ->where('estado','=','1');
        return $query->get();
    }

    public static function obtenerColeccion($dto){
        $query = DB::table('servicios')
            ->select(
                'servicios.id',
                'servicios.nombre',
                'servicios.estado',
                'servicios.usuario_creacion_id',
                'servicios.usuario_creacion_nombre',
                'servicios.usuario_modificacion_id',
                'servicios.usuario_modificacion_nombre',
                'servicios.created_at AS fecha_creacion',
                'servicios.updated_at AS fecha_modificacion'
            );

        if(isset($dto['nombre'])){
            $query->where('servicios.nombre', 'like', '%' . $dto['nombre'] . '%');
        }

        if (isset($dto['ordenar_por']) && count($dto['ordenar_por']) > 0){
            foreach ($dto['ordenar_por'] as $attribute => $value){
                if($attribute == 'nombre'){
                    $query->orderBy('servicios.nombre', $value);
                }
                if($attribute == 'estado'){
                    $query->orderBy('servicios.estado', $value);
                }
                if($attribute == 'usuario_creacion_nombre'){
                    $query->orderBy('servicios.usuario_creacion_nombre', $value);
                }
                if($attribute == 'usuario_modificacion_nombre'){
                    $query->orderBy('servicios.usuario_modificacion_nombre', $value);
                }
                if($attribute == 'fecha_creacion'){
                    $query->orderBy('servicios.created_at', $value);
                }
                if($attribute == 'fecha_modificacion'){
                    $query->orderBy('servicios.updated_at', $value);
                }
            }
        }else{
            $query->orderBy("servicios.id", "desc");
        }

        $servicios = $query->paginate($dto['limite'] ?? 100);
        return $servicios;
    }

}
