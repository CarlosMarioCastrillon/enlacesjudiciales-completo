<?php

namespace App\Model\Configuracion;

use App\Model\Administracion\Actuacion;
use App\Model\Administracion\Cliente;
use App\Model\Administracion\Diligencia;
use App\Model\Administracion\DiligenciaHistoria;
use App\Model\Administracion\ParametroConstante;
use App\Model\Administracion\ProcesoCliente;
use App\Model\Administracion\ProcesoJudicial;
use App\Model\Administracion\ProcesoValor;
use App\Model\Importacion\AuxiliarJusticiaCargaError;
use App\Model\Importacion\ProcesoJudicialCargaError;
use App\Model\Seguridad\Usuario;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Empresa extends Model
{

    protected $fillable = [
        'numero_documento',
        'nombre',
        'direccion',
        'email',
        'telefono_uno',
        'telefono_dos',
        'telefono_tres',
        'fecha_vencimiento',
        'con_actuaciones_digitales',
        'con_vigilancia',
        'con_data_procesos',
        'observacion',
        'estado',
        'tipo_documento_id',
        'tipo_empresa_id',
        'departamento_id',
        'ciudad_id',
        'zona_id',
        'usuario_creacion_id',
        'usuario_creacion_nombre',
        'usuario_modificacion_id',
        'usuario_modificacion_nombre'
    ];

    public function tipoEmpresa(){
        return $this->belongsTo(TipoDeEmpresa::class);
    }

    public function tipoDocumento(){
        return $this->belongsTo(TipoDocumento::class);
    }

    public function departamento(){
        return $this->belongsTo(Departamento::class);
    }

    public function ciudad(){
        return $this->belongsTo(Ciudad::class);
    }

    public function zona(){
        return $this->belongsTo(Zona::class);
    }

    public function usuarios(){
        return $this->hasMany(Usuario::class);
    }

    public function coberturas(){
        return $this->hasMany(Cobertura::class);
    }

    public function clientes(){
        return $this->hasMany(Cliente::class);
    }

    public function areas(){
        return $this->hasMany(Area::class);
    }

    public function auxiliarJusticias(){
        return $this->hasMany(AuxiliarJusticia::class);
    }

    public function conceptosEconomicos(){
        return $this->hasMany(ConceptoEconomico::class);
    }

    public function planes(){
        return $this->hasMany(Plan::class);
    }

    public function actuaciones(){
        return $this->hasMany(Actuacion::class);
    }

    public function diligencias(){
        return $this->hasMany(Diligencia::class);
    }

    public function diligenciasHistoria(){
        return $this->hasMany(DiligenciaHistoria::class);
    }

    public function procesosCliente(){
        return $this->hasMany(ProcesoCliente::class);
    }

    public function procesosJudiciales(){
        return $this->hasMany(ProcesoJudicial::class);
    }

    public function procesosValor(){
        return $this->hasMany(ProcesoValor::class);
    }

    public function procesoJudicialCargaErrores(){
        return $this->hasMany(ProcesoJudicialCargaError::class);
    }

    public function auxiliarJusticiaCargaErrores(){
        return $this->hasMany(AuxiliarJusticiaCargaError::class);
    }

    public static function obtenerColeccion($dto){
        // Constantes
        $parametros = ParametroConstante::cargarParametros();

        $user = Auth::user();
        $rol = $user->rol();
        $empresa = $user->empresa();
        $dto['empresa_id'] = $empresa->id;
        $dto['zona_id'] = $empresa->zona_id;
        $query = DB::table('empresas')
            ->join('tipos_de_documentos','tipos_de_documentos.id','=','empresas.tipo_documento_id')
            ->join('tipos_de_empresa','tipos_de_empresa.id','=','empresas.tipo_empresa_id')
            ->join('departamentos','departamentos.id','=','empresas.departamento_id')
            ->join('ciudades','ciudades.id','=','empresas.ciudad_id')
            ->join('zonas','zonas.id','=','empresas.zona_id')
            ->select(
                'empresas.id',
                'empresas.nombre',
                'empresas.numero_documento',
                'empresas.direccion',
                'empresas.email',
                'empresas.telefono_uno',
                'empresas.telefono_dos',
                'empresas.telefono_tres',
                'empresas.fecha_vencimiento',
                'empresas.con_actuaciones_digitales',
                'empresas.con_vigilancia',
                'empresas.con_data_procesos',
                'empresas.observacion',
                'empresas.estado',
                'tipos_de_documentos.codigo AS tipo_documento_codigo',
                'tipos_de_empresa.nombre AS tipo_empresa_nombre',
                'departamentos.nombre AS departamento_nombre',
                'ciudades.nombre AS ciudad_nombre',
                'zonas.nombre AS zona_nombre',
                'empresas.usuario_creacion_id',
                'empresas.usuario_creacion_nombre',
                'empresas.usuario_modificacion_id',
                'empresas.usuario_modificacion_nombre',
                'empresas.created_at AS fecha_creacion',
                'empresas.updated_at AS fecha_modificacion'
            );

        if($rol->id != ($parametros['SUPERUSUARIO_ROL_ID'] ?? null)) {
            if ($rol->id == ($parametros['ADMINISTRADOR_ZONA_ROL_ID'] ?? null)){
                $query->where('empresas.zona_id', '=', $dto['zona_id']);
            }else {
                $query->where('empresas.id', '=', $dto['empresa_id']);
            }
        }
        if(isset($dto['nombre'])){
            $query->where('empresas.nombre', 'like', '%' . $dto['nombre'] . '%');
        }
        if(isset($dto['numero_documento'])){
            $query->where('empresas.numero_documento', 'like', '%' . $dto['numero_documento'] . '%');
        }
        if(isset($dto['ciudad_nombre'])){
            $query->where('ciudades.nombre', 'like', '%' . $dto['ciudad_nombre'] . '%');
        }
        if(isset($dto['zona_nombre'])){
            $query->where('zonas.nombre', 'like', '%' . $dto['zona_nombre'] . '%');
        }
        if(isset($dto['estado'])){
            $query->where('empresas.estado', '=', $dto['estado']);
        }

        if (isset($dto['ordenar_por']) && count($dto['ordenar_por']) > 0){
            foreach ($dto['ordenar_por'] as $attribute => $value){
                if($attribute == 'nombre'){
                    $query->orderBy('empresas.nombre', $value);
                }
                if($attribute == 'tipo_documento'){
                    $query->orderBy('tipos_de_documentos.codigo', $value);
                }
                if($attribute == 'numero_documento'){
                    $query->orderBy('empresas.numero_documento', $value);
                }
                if($attribute == 'tipo_empresa'){
                    $query->orderBy('tipos_de_empresa.nombre', $value);
                }
                if($attribute == 'ciudad'){
                    $query->orderBy('ciudades.nombre', $value);
                }
                if($attribute == 'actuacion_digital'){
                    $query->orderBy('empresas.con_actuaciones_digitales', $value);
                }
                if($attribute == 'vigilancia_proceso'){
                    $query->orderBy('empresas.con_vigilancia', $value);
                }
                if($attribute == 'dataprocesos'){
                    $query->orderBy('empresas.con_data_procesos', $value);
                }
                if($attribute == 'estado'){
                    $query->orderBy('empresas.estado', $value);
                }
            }
        }else{
            $query->orderBy("empresas.id", "desc");
        }

        $empresas = $query->paginate($dto['limite'] ?? 100);

        return $empresas;
    }

    public static function obtenerColeccionLigera($dto){
        // Constantes
        $parametros = ParametroConstante::cargarParametros();

        $user = Auth::user();
        $rol = $user->rol();
        $empresa = $user->empresa();

        // Cargar solo las empresas de la zona
        if($rol->id == ($parametros['ADMINISTRADOR_ZONA_ROL_ID'] ?? null)){
            $dto['zona_id'] = $empresa->zona_id;
        }

        $query = DB::table('empresas')
            ->select(
                'id',
                'nombre'
            );

        if(isset($dto['zona_id'])){
            $query->where('zona_id', '=', $dto['zona_id']);
        }

        return $query
            ->orderBy('nombre', 'asc')
            ->get();
    }

}
