<?php

namespace App\Model\Configuracion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Zona extends Model
{

    protected $table = 'zonas';

    protected $fillable = [
        'nombre',
        'coordinador_vigilancia',
        'estado',
        'usuario_creacion_id',
        'usuario_creacion_nombre',
        'usuario_modificacion_id',
        'usuario_modificacion_nombre',
    ];

    public static function obtenerColeccionLigera($dto){
        $query = DB::table('zonas')
            ->select(
                'id',
                'nombre')
            ->where('estado','=','1');

        return $query->get();
    }

    public static function obtenerColeccion($dto){
        $query = DB::table('zonas')
            ->select(
                'zonas.id',
                'zonas.nombre',
                'zonas.coordinador_vigilancia',
                'zonas.estado',
                'zonas.usuario_creacion_id',
                'zonas.usuario_creacion_nombre',
                'zonas.usuario_modificacion_id',
                'zonas.usuario_modificacion_nombre',
                'zonas.created_at AS fecha_creacion',
                'zonas.updated_at AS fecha_modificacion'
            );

        if(isset($dto['nombre'])){
            $query->where('zonas.nombre', 'like', '%' . $dto['nombre'] . '%');
        }

        if (isset($dto['ordenar_por']) && count($dto['ordenar_por']) > 0){
            foreach ($dto['ordenar_por'] as $attribute => $value){
                if($attribute == 'nombre'){
                    $query->orderBy('zonas.nombre', $value);
                }
                if($attribute == 'coordinador_vigilancia'){
                    $query->orderBy('zonas.coordinador_vigilancia', $value);
                }
                if($attribute == 'estado'){
                    $query->orderBy('zonas.estado', $value);
                }
                if($attribute == 'usuario_creacion_nombre'){
                    $query->orderBy('zonas.usuario_creacion_nombre', $value);
                }
                if($attribute == 'usuario_modificacion_nombre'){
                    $query->orderBy('zonas.usuario_modificacion_nombre', $value);
                }
                if($attribute == 'fecha_creacion'){
                    $query->orderBy('zonas.created_at', $value);
                }
                if($attribute == 'fecha_modificacion'){
                    $query->orderBy('zonas.updated_at', $value);
                }
            }
        }else{
            $query->orderBy("zonas.id", "desc");
        }

        $zonas = $query->paginate($dto['limite'] ?? 100);
        return $zonas;
    }
}
