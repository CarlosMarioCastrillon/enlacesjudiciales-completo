<?php

namespace App\Model\Configuracion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class EtapaClaseProceso extends Model
{

    protected $fillable = [
        'etapa_proceso_id',
        'clase_proceso_id',
        'etapa_proceso_nombre',
        'clase_proceso_nombre',
        'estado',
        'usuario_creacion_id',
        'usuario_creacion_nombre',
        'usuario_modificacion_id',
        'usuario_modificacion_nombre',
    ];

    public function etapaProceso(){
        return $this->belongsTo(EtapaProceso::class);
    }

    public function claseProceso(){
        return $this->belongsTo(ClaseProceso::class);
    }

    public static function obtenerColeccion($dto){
        $query = DB::table('etapa_clase_procesos')
            ->join('etapas_de_procesos', 'etapas_de_procesos.id', '=', 'etapa_clase_procesos.etapa_proceso_id')
            ->join('clases_de_procesos', 'clases_de_procesos.id', '=', 'etapa_clase_procesos.clase_proceso_id')
            ->select(
                'etapa_clase_procesos.id',
                'etapas_de_procesos.nombre AS etapa_proceso_nombre',
                'clases_de_procesos.nombre AS clase_proceso_nombre',
                'etapa_clase_procesos.estado',
                'etapa_clase_procesos.usuario_creacion_id',
                'etapa_clase_procesos.usuario_creacion_nombre',
                'etapa_clase_procesos.usuario_modificacion_id',
                'etapa_clase_procesos.usuario_modificacion_nombre',
                'etapa_clase_procesos.created_at AS fecha_creacion',
                'etapa_clase_procesos.updated_at AS fecha_modificacion'
            );

        //Filtros
        if(isset($dto['claseProceso'])){
            $query->where('clases_de_procesos.nombre', 'like', '%' . $dto['claseProceso'] . '%');
        }
        if(isset($dto['etapaProceso'])){
            $query->where('etapas_de_procesos.nombre', 'like', '%' . $dto['etapaProceso'] . '%');
        }

        if (isset($dto['ordenar_por']) && count($dto['ordenar_por'] ?? []) > 0){
            foreach ($dto['ordenar_por'] as $attribute => $value){
                if($attribute == 'clase_proceso_nombre'){
                    $query->orderBy('clases_de_procesos.nombre', $value);
                }
                if($attribute == 'etapa_proceso_nombre'){
                    $query->orderBy('etapas_de_procesos.nombre', $value);
                }
                if($attribute == 'estado'){
                    $query->orderBy('etapa_clase_procesos.estado', $value);
                }
                if($attribute == 'usuario_creacion_nombre'){
                    $query->orderBy('etapa_clase_procesos.usuario_creacion_nombre', $value);
                }
                if($attribute == 'fecha_creacion'){
                    $query->orderBy('etapa_clase_procesos.created_at', $value);
                }
                if($attribute == 'usuario_modificacion_nombre'){
                    $query->orderBy('etapa_clase_procesos.usuario_modificacion_nombre', $value);
                }
                if($attribute == 'fecha_modificacion'){
                    $query->orderBy('etapa_clase_procesos.updated_at', $value);
                }
            }
        }else{
            $query->orderBy("etapa_clase_procesos.id", "desc");
        }

        $EtapaClaseProcesos = $query->paginate($dto['limite'] ?? 100);
        return $EtapaClaseProcesos;
    }
}
