<?php

namespace App\Model\Configuracion;

use App\Model\Seguridad\Usuario;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Area extends Model
{

    protected $table = 'areas';

    public function usuario(){
        return $this->belongsTo(Usuario::class);
    }

    protected $fillable = [
        'nombre',
        'empresa_id',
        'usuario_id',
        'estado',
        'usuario_creacion_id',
        'usuario_creacion_nombre',
        'usuario_modificacion_id',
        'usuario_modificacion_nombre',
    ];

    public static function obtenerColeccionLigera($dto){
        $query = DB::table('areas')
            ->select(
                'id','nombre'
            )
            ->where('estado','=','1')
            ->where('areas.empresa_id','=', $dto['empresa_id']);

        return $query->get();
    }

    public static function obtenerColeccion($dto){
        $query = DB::table('areas')
            ->join('empresas', 'empresas.id', '=', 'areas.empresa_id')
            ->leftJoin('usuarios', 'usuarios.id', '=', 'areas.usuario_id')
            ->select(
                'areas.id',
                'areas.nombre',
                'usuarios.nombre As usuario_nombre',
                'areas.estado',
                'areas.usuario_creacion_id',
                'areas.usuario_creacion_nombre',
                'areas.usuario_modificacion_id',
                'areas.usuario_modificacion_nombre',
                'areas.created_at AS fecha_creacion',
                'areas.updated_at AS fecha_modificacion'
            )->where('areas.empresa_id', '=' , $dto['empresa_id']);

        if(isset($dto['nombre'])){
            $query->where('areas.nombre', 'like', '%' . $dto['nombre'] . '%');
        }

        if (isset($dto['ordenar_por']) && count($dto['ordenar_por']) > 0){
            foreach ($dto['ordenar_por'] as $attribute => $value){
                if($attribute == 'nombre'){
                    $query->orderBy('areas.nombre', $value);
                }
                if($attribute == 'usuario_nombre'){
                    $query->orderBy('areas.usuario_id', $value);
                }
                if($attribute == 'estado'){
                    $query->orderBy('areas.estado', $value);
                }
                if($attribute == 'usuario_creacion_nombre'){
                    $query->orderBy('areas.usuario_creacion_nombre', $value);
                }
                if($attribute == 'usuario_modificacion_nombre'){
                    $query->orderBy('areas.usuario_modificacion_nombre', $value);
                }
                if($attribute == 'fecha_creacion'){
                    $query->orderBy('areas.created_at', $value);
                }
                if($attribute == 'fecha_modificacion'){
                    $query->orderBy('areas.updated_at', $value);
                }
            }
        }else{
            $query->orderBy("areas.id", "desc");
        }

        $areas = $query->paginate($dto['limite'] ?? 100);
        return $areas;
    }

    public static function obtener($dto){
        $query = DB::table('areas')
            ->join('empresas', 'empresas.id', '=', 'areas.empresa_id')
            ->leftJoin('usuarios', 'usuarios.id', '=', 'areas.usuario_id')
            ->select(
                'areas.id',
                'areas.nombre',
                'usuarios.nombre As usuario_nombre',
                'areas.estado',
                'areas.usuario_creacion_id',
                'areas.usuario_creacion_nombre',
                'areas.usuario_modificacion_id',
                'areas.usuario_modificacion_nombre',
                'areas.created_at AS fecha_creacion',
                'areas.updated_at AS fecha_modificacion'
            )->where('areas.empresa_id', '=' , $dto['empresa_id'])
            ->orderBy("areas.id", "desc");

        return $query->get();
    }
}
