<?php

namespace App\Model\Configuracion;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ConceptoEconomico extends Model
{

    protected $table = 'conceptos_economicos';

    protected $fillable = [
        'nombre',
        'tipo_concepto',
        'clase_concepto',
        'empresa_id',
        'estado',
        'usuario_creacion_id',
        'usuario_creacion_nombre',
        'usuario_modificacion_id',
        'usuario_modificacion_nombre',
    ];

    public static function obtenerColeccionLigera($dto){
        $user = Auth::user();
        $empresa = $user->empresa();
        $dto['empresa_id'] = $empresa->id;
        $query = DB::table('conceptos_economicos')
            ->select(
                'id','nombre'
            )
            ->where('estado','=','1')
            ->where('conceptos_economicos.tipo_concepto','=', $dto['tipo_concepto_id'])
            ->where('conceptos_economicos.empresa_id','=', $dto['empresa_id']);
        return $query->get();
    }

    public static function obtenerColeccion($dto){
        $user = Auth::user();
        $empresa = $user->empresa();
        $dto['empresa_id'] = $empresa->id;
        $query = DB::table('conceptos_economicos')
            ->select(
                'conceptos_economicos.id',
                'conceptos_economicos.tipo_concepto',
                'conceptos_economicos.nombre',
                'conceptos_economicos.clase_concepto',
                'conceptos_economicos.empresa_id',
                'conceptos_economicos.estado',
                'conceptos_economicos.usuario_creacion_id',
                'conceptos_economicos.usuario_creacion_nombre',
                'conceptos_economicos.usuario_modificacion_id',
                'conceptos_economicos.usuario_modificacion_nombre',
                'conceptos_economicos.created_at AS fecha_creacion',
                'conceptos_economicos.updated_at AS fecha_modificacion'
            )
            ->where('conceptos_economicos.empresa_id', '=', $dto['empresa_id']);

        if(isset($dto['tipo_concepto'])){
            $query->where('conceptos_economicos.tipo_concepto', '=', $dto['tipo_concepto']);
        }

        if(isset($dto['nombre'])){
            $query->where('conceptos_economicos.nombre', 'like', '%' . $dto['nombre'] . '%');
        }

        if (isset($dto['ordenar_por']) && count($dto['ordenar_por']) > 0){
            foreach ($dto['ordenar_por'] as $attribute => $value){
                if($attribute == 'nombre'){
                    $query->orderBy('conceptos_economicos.nombre', $value);
                }
                if($attribute == 'tipo_concepto_nombre'){
                    $query->orderBy('conceptos_economicos.tipo_concepto', $value);
                }
                if($attribute == 'clase_concepto_nombre'){
                    $query->orderBy('conceptos_economicos.clase_concepto', $value);
                }
                if($attribute == 'estado'){
                    $query->orderBy('conceptos_economicos.estado', $value);
                }
                if($attribute == 'usuario_creacion_nombre'){
                    $query->orderBy('conceptos_economicos.usuario_creacion_nombre', $value);
                }
                if($attribute == 'usuario_modificacion_nombre'){
                    $query->orderBy('conceptos_economicos.usuario_modificacion_nombre', $value);
                }
                if($attribute == 'fecha_creacion'){
                    $query->orderBy('conceptos_economicos.created_at', $value);
                }
                if($attribute == 'fecha_modificacion'){
                    $query->orderBy('conceptos_economicos.updated_at', $value);
                }
            }
        }else{
            $query->orderBy("conceptos_economicos.tipo_concepto", "asc");
        }

        $conceptosEconomicos = $query->paginate($dto['limite'] ?? 100);
        return $conceptosEconomicos;
    }

}
