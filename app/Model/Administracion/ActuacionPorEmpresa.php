<?php

namespace App\Model\Administracion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ActuacionPorEmpresa extends Model
{

    public static function obtenerColeccion($dto)
    {
        $user = Auth::user();
        $empresa = $user->empresa();
        $dto['empresa_id'] = $empresa->id;
        $query1 = DB::table('actuaciones')
            ->join('ciudades', 'ciudades.id', '=', 'actuaciones.ciudad_id')
            ->join('juzgados', 'juzgados.id', '=', 'actuaciones.juzgado_id')
            ->join('despachos', 'despachos.id', '=', 'actuaciones.despacho_id')
            ->join('procesos_judiciales', 'procesos_judiciales.id', '=', 'actuaciones.proceso_id')
            ->leftJoin('etapas_de_procesos', 'etapas_de_procesos.id', '=', 'actuaciones.etapa_proceso_id')
            ->leftJoin('tipos_de_actuaciones', 'tipos_de_actuaciones.id', '=', 'actuaciones.tipo_actuacion_id')
            ->leftJoin('tipos_notificaciones', 'tipos_notificaciones.id', '=', 'actuaciones.tipo_notificacion_id')
            ->join('empresas', 'empresas.id', '=', 'actuaciones.empresa_id')
            ->select(
                'actuaciones.id',
                'ciudades.nombre AS ciudad_nombre',
                'juzgados.nombre AS juzgado_nombre',
                'despachos.despacho AS despacho_numero',
                'tipos_de_actuaciones.nombre AS tipo_actuacion_nombre',
                'etapas_de_procesos.nombre AS etapa_proceso_nombre',
                'actuaciones.proceso_id',
                'actuaciones.numero_proceso',
                'actuaciones.actuacion',
                'actuaciones.anotacion',
                DB::raw("0 as tipo_actuacion"),
                'actuaciones.fecha_registro',
                'actuaciones.fecha_actuacion',
                'actuaciones.fecha_inicio_termino',
                'actuaciones.fecha_vencimiento_termino',
                'actuaciones.ind_actuacion_audiencia',
                'actuaciones.tipo_movimiento',
                'actuaciones.nombres_demandantes',
                'actuaciones.nombres_demandados',
                'actuaciones.url_archivo_anexo',
                'actuaciones.observaciones',
                'actuaciones.usuario_creacion_id',
                'actuaciones.usuario_creacion_nombre',
                'actuaciones.usuario_modificacion_id',
                'actuaciones.usuario_modificacion_nombre',
                'actuaciones.created_at AS fecha_creacion',
                'actuaciones.updated_at AS fecha_modificacion'
            )->where('actuaciones.empresa_id', '=', $dto['empresa_id']);

        if (isset($dto['fecha_inicial'])){
            $query1->where('actuaciones.fecha_actuacion', '>=', $dto['fecha_inicial']);
        }
        if (isset($dto['fecha_final'])){
            $query1->where('actuaciones.fecha_actuacion', '<=', $dto['fecha_final']);
        }
        if (isset($dto['fecha'])){
            $query1->where('actuaciones.fecha_actuacion', '>=', $dto['fecha']);
        }
        if (isset($dto['ciudad'])){
            $query1->where('actuaciones.ciudad_id', '=', $dto['ciudad']);
        }
        if (isset($dto['juzgado'])){
            $query1->where('actuaciones.juzgado_id', '=', $dto['juzgado']);
        }
        if (isset($dto['despacho'])){
            $query1->where('actuaciones.despacho_id', '=', $dto['despacho']);
        }
        if (isset($dto['proceso'])){
            $query1->where('actuaciones.numero_proceso', 'like', '%' . $dto['proceso'] . '%');
        }
        if (isset($dto['url'])){
            $query1->where('actuaciones.url_archivo_anexo', '<>', null);
        }
        if (isset($dto['tipo_movimiento'])){
            $query1->whereIn('actuaciones.tipo_movimiento', ['AC', 'EX', 'VG']);
        }

        if (isset($dto['ordenar_por']) && count($dto['ordenar_por']) > 0){
            foreach ($dto['ordenar_por'] as $attribute => $value){
                if($attribute == 'nombre_ciudad'){
                    $query1->orderBy('ciudades.nombre', $value);
                }
                if($attribute == 'fecha'){
                    $query1->orderBy('actuaciones.fecha_actuacion', $value);
                }
                if($attribute == 'proceso'){
                    $query1->orderBy('actuaciones.numero_proceso', $value);
                }
            }
        }else{
            $query1->orderBy("actuaciones.id", "desc")
                ->orderBy("actuaciones.numero_proceso", "asc");
        }

        $query2 = DB::table('expedientes')
            ->join('ciudades', 'ciudades.id', '=', 'expedientes.ciudad_id')
            ->join('juzgados', 'juzgados.id', '=', 'expedientes.juzgado_id')
            ->join('despachos', 'despachos.id', '=', 'expedientes.despacho_id')
            ->join('procesos_judiciales', 'procesos_judiciales.id', '=', 'expedientes.proceso_id')
            ->leftJoin('etapas_de_procesos', 'etapas_de_procesos.id', '=', 'expedientes.etapa_proceso_id')
            ->leftJoin('tipos_de_actuaciones', 'tipos_de_actuaciones.id', '=', 'expedientes.tipo_actuacion_id')
            ->leftJoin('tipos_notificaciones', 'tipos_notificaciones.id', '=', 'expedientes.tipo_notificacion_id')
            ->join('empresas', 'empresas.id', '=', 'expedientes.empresa_id')
            ->select(
                'expedientes.id',
                'ciudades.nombre AS ciudad_nombre',
                'juzgados.nombre AS juzgado_nombre',
                'despachos.despacho AS despacho_numero',
                'tipos_de_actuaciones.nombre AS tipo_actuacion_nombre',
                'etapas_de_procesos.nombre AS etapa_proceso_nombre',
                'expedientes.proceso_id',
                'expedientes.numero_proceso',
                'expedientes.actuacion',
                'expedientes.anotacion',
                DB::raw("1 as tipo_actuacion"),
                'expedientes.fecha_registro',
                'expedientes.fecha_actuacion',
                'expedientes.fecha_inicio_termino',
                'expedientes.fecha_vencimiento_termino',
                'expedientes.ind_actuacion_audiencia',
                'expedientes.tipo_movimiento',
                'expedientes.nombres_demandantes',
                'expedientes.nombres_demandados',
                'expedientes.url_archivo_anexo',
                'expedientes.observaciones',
                'expedientes.usuario_creacion_id',
                'expedientes.usuario_creacion_nombre',
                'expedientes.usuario_modificacion_id',
                'expedientes.usuario_modificacion_nombre',
                'expedientes.created_at AS fecha_creacion',
                'expedientes.updated_at AS fecha_modificacion'
            )->where('expedientes.empresa_id', '=', $dto['empresa_id']);

        if (isset($dto['fecha_inicial'])){
            $query2->where('expedientes.fecha_actuacion', '>=', $dto['fecha_inicial']);
        }
        if (isset($dto['fecha_final'])){
            $query2->where('expedientes.fecha_actuacion', '<=', $dto['fecha_final']);
        }
        if (isset($dto['fecha'])){
            $query2->where('expedientes.fecha_actuacion', '>=', $dto['fecha']);
        }
        if (isset($dto['ciudad'])){
            $query2->where('expedientes.ciudad_id', '=', $dto['ciudad']);
        }
        if (isset($dto['juzgado'])){
            $query2->where('expedientes.juzgado_id', '=', $dto['juzgado']);
        }
        if (isset($dto['despacho'])){
            $query2->where('expedientes.despacho_id', '=', $dto['despacho']);
        }
        if (isset($dto['proceso'])){
            $query2->where('expedientes.numero_proceso', 'like', '%' . $dto['proceso'] . '%');
        }
        if (isset($dto['url'])){
            $query2->where('expedientes.url_archivo_anexo', '<>', null);
        }
        if (isset($dto['tipo_movimiento'])){
            $query2->whereIn('expedientes.tipo_movimiento', ['AC', 'EX', 'VG']);
        }

        if (isset($dto['ordenar_por']) && count($dto['ordenar_por']) > 0){
            foreach ($dto['ordenar_por'] as $attribute => $value){
                if($attribute == 'nombre_ciudad'){
                    $query2->orderBy('ciudades.nombre', $value);
                }
                if($attribute == 'fecha'){
                    $query2->orderBy('expedientes.fecha_actuacion', $value);
                }
                if($attribute == 'proceso'){
                    $query2->orderBy('expedientes.numero_proceso', $value);
                }
            }
        }else{
            $query2->orderBy("expedientes.fecha_actuacion", "desc")
                ->orderBy("expedientes.numero_proceso", "asc");
        }

        $query = $query1->union($query2);

        $movimientos = $query->paginate($dto['limite'] ?? 100);
        return $movimientos;
    }

}
