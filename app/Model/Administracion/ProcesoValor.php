<?php

namespace App\Model\Administracion;

use App\Model\Configuracion\ConceptoEconomico;
use Illuminate\Database\Eloquent\Model;

class ProcesoValor extends Model
{

    protected $table = 'procesos_valores';

    public function concepto(){
        return $this->belongsTo(ConceptoEconomico::class);
    }

    protected $fillable = [
        'concepto_id',
        'fecha',
        'valor',
        'observacion',
        'proceso_id',
        'empresa_id',
        'estado',
        'usuario_creacion_id',
        'usuario_creacion_nombre',
        'usuario_modificacion_id',
        'usuario_modificacion_nombre',
    ];
}
