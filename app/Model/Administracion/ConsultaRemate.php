<?php

namespace App\Model\Administracion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ConsultaRemate extends Model
{

    public static function obtenerColeccion($dto)
    {
        $query1 = DB::table('actuaciones')
            ->join('ciudades', 'ciudades.id', '=', 'actuaciones.ciudad_id')
            ->join('juzgados', 'juzgados.id', '=', 'actuaciones.juzgado_id')
            ->join('despachos', 'despachos.id', '=', 'actuaciones.despacho_id')
            ->select(
                'actuaciones.id',
                'juzgados.nombre AS juzgado_nombre',
                'despachos.despacho AS despacho_numero',
                'actuaciones.numero_proceso',
                'actuaciones.anotacion',
                'actuaciones.fecha_actuacion',
                'actuaciones.nombres_demandantes',
                'actuaciones.nombres_demandados',
                'actuaciones.url_archivo_anexo',
                'actuaciones.usuario_creacion_id',
                'actuaciones.usuario_creacion_nombre',
                'actuaciones.usuario_modificacion_id',
                'actuaciones.usuario_modificacion_nombre',
                'actuaciones.created_at AS fecha_creacion',
                'actuaciones.updated_at AS fecha_modificacion'
            )->where('actuaciones.tipo_movimiento', '=','RM');

        if (isset($dto['fecha_inicial'])){
            $query1->where('actuaciones.fecha_actuacion', '>=', $dto['fecha_inicial']);
        }
        if (isset($dto['fecha_final'])){
            $query1->where('actuaciones.fecha_actuacion', '<=', $dto['fecha_final']);
        }
        if (isset($dto['ciudad'])){
            $query1->where('actuaciones.ciudad_id', '=', $dto['ciudad']);
        }
        if (isset($dto['juzgado'])){
            $query1->where('actuaciones.juzgado_id', '=', $dto['juzgado']);
        }
        if (isset($dto['despacho'])){
            $query1->where('actuaciones.despacho_id', '=', $dto['despacho']);
        }

        if (isset($dto['ordenar_por']) && count($dto['ordenar_por']) > 0){
            foreach ($dto['ordenar_por'] as $attribute => $value){
                if($attribute == 'nombre_ciudad'){
                    $query1->orderBy('ciudades.nombre', $value);
                }
                if($attribute == 'fecha'){
                    $query1->orderBy('actuaciones.fecha_actuacion', $value);
                }
                if($attribute == 'proceso'){
                    $query1->orderBy('actuaciones.numero_proceso', $value);
                }
                if($attribute == 'demandante'){
                    $query1->orderBy('actuaciones.nombres_demandantes', $value);
                }
                if($attribute == 'demandado'){
                    $query1->orderBy('actuaciones.nombres_demandados', $value);
                }
                if($attribute == 'anotacion'){
                    $query1->orderBy('actuaciones.anotacion', $value);
                }
            }
        }else{
            $query1->orderBy("actuaciones.id", "desc")
                ->orderBy("actuaciones.numero_proceso", "asc");
        }

        $query2 = DB::table('expedientes')
            ->join('ciudades', 'ciudades.id', '=', 'expedientes.ciudad_id')
            ->join('juzgados', 'juzgados.id', '=', 'expedientes.juzgado_id')
            ->join('despachos', 'despachos.id', '=', 'expedientes.despacho_id')
            ->select(
                'expedientes.id',
                'juzgados.nombre AS juzgado_nombre',
                'despachos.despacho AS despacho_numero',
                'expedientes.numero_proceso',
                'expedientes.anotacion',
                'expedientes.fecha_actuacion',
                'expedientes.nombres_demandantes',
                'expedientes.nombres_demandados',
                'expedientes.url_archivo_anexo',
                'expedientes.usuario_creacion_id',
                'expedientes.usuario_creacion_nombre',
                'expedientes.usuario_modificacion_id',
                'expedientes.usuario_modificacion_nombre',
                'expedientes.created_at AS fecha_creacion',
                'expedientes.updated_at AS fecha_modificacion'
            )->where('expedientes.tipo_movimiento', '=','RM');

        if (isset($dto['fecha_inicial'])){
            $query2->where('expedientes.fecha_actuacion', '>=', $dto['fecha_inicial']);
        }
        if (isset($dto['fecha_final'])){
            $query2->where('expedientes.fecha_actuacion', '<=', $dto['fecha_final']);
        }
        if (isset($dto['ciudad'])){
            $query2->where('expedientes.ciudad_id', '=', $dto['ciudad']);
        }
        if (isset($dto['juzgado'])){
            $query2->where('expedientes.juzgado_id', '=', $dto['juzgado']);
        }
        if (isset($dto['despacho'])){
            $query2->where('expedientes.despacho_id', '=', $dto['despacho']);
        }

        if (isset($dto['ordenar_por']) && count($dto['ordenar_por']) > 0){
            foreach ($dto['ordenar_por'] as $attribute => $value){
                if($attribute == 'nombre_ciudad'){
                    $query2->orderBy('ciudades.nombre', $value);
                }
                if($attribute == 'fecha'){
                    $query2->orderBy('expedientes.fecha_actuacion', $value);
                }
                if($attribute == 'proceso'){
                    $query2->orderBy('expedientes.numero_proceso', $value);
                }
                if($attribute == 'demandante'){
                    $query2->orderBy('expedientes.nombres_demandantes', $value);
                }
                if($attribute == 'demandado'){
                    $query2->orderBy('expedientes.nombres_demandados', $value);
                }
                if($attribute == 'anotacion'){
                    $query2->orderBy('expedientes.anotacion', $value);
                }
            }
        }else{
            $query2->orderBy("expedientes.fecha_actuacion", "desc")
                ->orderBy("expedientes.numero_proceso", "asc");
        }

        $query = $query1->union($query2);

        $movimientos = $query->paginate($dto['limite'] ?? 100);
        return $movimientos;
    }

}
