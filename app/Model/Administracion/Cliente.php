<?php

namespace App\Model\Administracion;

use App\Model\Configuracion\Ciudad;
use App\Model\Configuracion\Departamento;
use App\Model\Configuracion\TipoDeEmpresa;
use App\Model\Configuracion\TipoDocumento;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Cliente extends Model
{

    protected $table = 'clientes';

    protected $fillable = [
        'numero_documento',
        'nombre',
        'direccion',
        'email',
        'telefono_uno',
        'telefono_dos',
        'estado',
        'tipo_documento_id',
        'ciudad_id',
        'empresa_id',
        'usuario_creacion_id',
        'usuario_creacion_nombre',
        'usuario_modificacion_id',
        'usuario_modificacion_nombre'
    ];

    public function tipoDocumento(){
        return $this->belongsTo(TipoDocumento::class);
    }

    public function ciudad(){
        return $this->belongsTo(Ciudad::class);
    }

    public static function obtenerColeccion($dto){
        $user = Auth::user();
        $empresa = $user->empresa();
        $dto['empresa_id'] = $empresa->id;
        $query = DB::table('clientes')
            ->join('tipos_de_documentos','tipos_de_documentos.id','=','clientes.tipo_documento_id')
            ->leftJoin('ciudades','ciudades.id','=','clientes.ciudad_id')
            ->leftJoin('departamentos','departamentos.id','=','ciudades.departamento_id')
            ->select(
                'clientes.id',
                'clientes.nombre',
                'clientes.numero_documento',
                'clientes.direccion',
                'clientes.email',
                'clientes.telefono_uno',
                'clientes.telefono_dos',
                'clientes.estado',
                'tipos_de_documentos.codigo AS tipo_documento_codigo',
                'departamentos.nombre AS departamento_nombre',
                'departamentos.id AS departamento_id',
                'ciudades.nombre AS ciudad_nombre',
                'clientes.usuario_creacion_id',
                'clientes.usuario_creacion_nombre',
                'clientes.usuario_modificacion_id',
                'clientes.usuario_modificacion_nombre',
                'clientes.created_at AS fecha_creacion',
                'clientes.updated_at AS fecha_modificacion'
            )
            ->where('clientes.empresa_id', '=', $dto['empresa_id']);

        if(isset($dto['nombre'])){
            $query->where('clientes.nombre', 'like', '%' . $dto['nombre'] . '%');
        }
        if(isset($dto['numero_documento'])){
            $query->where('clientes.numero_documento', 'like', '%' . $dto['numero_documento'] . '%');
        }
        if(isset($dto['ciudad_nombre'])){
            $query->where('ciudades.nombre', 'like', '%' . $dto['ciudad_nombre'] . '%');
        }

        if (isset($dto['ordenar_por']) && count($dto['ordenar_por']) > 0){
            foreach ($dto['ordenar_por'] as $attribute => $value){
                if($attribute == 'nombre'){
                    $query->orderBy('clientes.nombre', $value);
                }
                if($attribute == 'tipo_documento'){
                    $query->orderBy('tipos_de_documentos.codigo', $value);
                }
                if($attribute == 'numero_documento'){
                    $query->orderBy('clientes.numero_documento', $value);
                }
                if($attribute == 'ciudad'){
                    $query->orderBy('ciudades.nombre', $value);
                }
                if($attribute == 'estado'){
                    $query->orderBy('clientes.estado', $value);
                }
                if($attribute == 'usuario_creacion_nombre'){
                    $query->orderBy('clientes.usuario_creacion_nombre', $value);
                }
                if($attribute == 'usuario_modificacion_nombre'){
                    $query->orderBy('clientes.usuario_modificacion_nombre', $value);
                }
                if($attribute == 'fecha_creacion'){
                    $query->orderBy('clientes.created_at', $value);
                }
                if($attribute == 'fecha_modificacion'){
                    $query->orderBy('clientes.updated_at', $value);
                }
            }
        }else{
            $query->orderBy("clientes.id", "desc");
        }

        $clientes = $query->paginate($dto['limite'] ?? 100);
        return $clientes;
    }

    public static function obtenerColeccionLigera($dto){
        $user = Auth::user();
        $empresa = $user->empresa();
        $query = DB::table('clientes')
            ->where('clientes.empresa_id', '=', $empresa->id)
            ->select(
                'id',
                'nombre')
            ->where('estado','=','1');

        return $query->get();
    }

}
