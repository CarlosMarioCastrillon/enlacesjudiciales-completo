<?php

namespace App\Model\Administracion;

use Illuminate\Database\Eloquent\Model;

class AuditoriaProceso extends Model
{

    protected $table = 'auditoria_procesos';

    protected $fillable = [
        'fecha_ejecucion',
        'fecha_proceso',
        'numero_proceso',
        'tipo_novedad',
        'programacion_auditoria_proceso_id'
    ];

    public $timestamps = false;

}
