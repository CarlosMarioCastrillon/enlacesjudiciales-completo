<?php

namespace App\Model\Administracion;

use Illuminate\Database\Eloquent\Model;

class ProcesoRamaJudicial extends Model
{

    protected $table = 'procesos_rama_judicial';

    protected $fillable = [
        'numero_proceso'
    ];

    public $timestamps = false;

}
