<?php

namespace App\Model\Administracion;

use App\Model\Administracion\ProcesoJudicial;
use App\Model\Configuracion\Ciudad;
use App\Model\Configuracion\Departamento;
use App\Model\Configuracion\Empresa;
use App\Model\Configuracion\TipoDiligencia;
use App\Model\Seguridad\Usuario;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;

class DiligenciaHistoria extends Model
{

    protected $table = 'diligencias_historia';

    protected $fillable = [
        'diligencia_id',
        'empresa_id',
        'proceso_id',
        'secuencia',
        'fecha_diligencia',
        'tipo_diligencia_id',
        'departamento_id',
        'ciudad_id',
        'dependiente_id',
        'valor_diligencia',
        'gastos_envio',
        'otros_gastos',
        'costo_diligencia',
        'costo_envio',
        'otros_costos',
        'detalle_valores',
        'indicativo_cobro',
        'solicitud_autorizacion',
        'estado_diligencia',
        'observaciones_cliente',
        'observaciones_internas',
        'url_anexo_diligencia',
        'archivo_anexo_diligencia',
        'url_anexo_costos',
        'archivo_anexo_costos',
        'url_anexo_recibido',
        'archivo_anexo_recibido',
        'usuario_creacion_id',
        'usuario_creacion_nombre',
        'usuario_modificacion_id',
        'usuario_modificacion_nombre',
    ];

    public function ciudad(){
        return $this->belongsTo(Ciudad::class);
    }
    public function departamento(){
        return $this->belongsTo(Departamento::class);
    }
    public function tipoDiligencia(){
        return $this->belongsTo(TipoDiligencia::class);
    }
    public function empresa(){
        return $this->belongsTo(Empresa::class, 'empresa_id', 'id');
    }
    public function dependiente(){
        return $this->belongsTo(Usuario::class, 'dependiente_id', 'id');
    }
    public function procesoJudicial(){
        return $this->belongsTo(ProcesoJudicial::class, 'proceso_id');
    }

    public static function obtenerSecuencia($diligenciaId){
        $historia = DB::table('diligencias_historia')
            ->where('diligencia_id', $diligenciaId)
            ->select(DB::raw('MAX(secuencia) AS ultima_secuencia'))
            ->first();

        if(isset($historia->ultima_secuencia)){
            $secuencia = $historia->ultima_secuencia + 1;
        }else{
            $secuencia = 1;
        }

        return $secuencia;
    }

    public static function obtenerColeccion($dto){
        $query = DB::table('diligencias_historia')
            ->join('ciudades', 'ciudades.id', '=', 'diligencias_historia.ciudad_id')
            ->join('departamentos', 'departamentos.id', '=', 'ciudades.departamento_id')
            ->join('tipos_de_diligencias', 'tipos_de_diligencias.id', '=', 'diligencias_historia.tipo_diligencia_id')
            ->join('empresas', 'empresas.id', '=', 'diligencias_historia.empresa_id')
            ->join('procesos_judiciales', 'procesos_judiciales.id', '=', 'diligencias_historia.proceso_id')
            ->select(
                'diligencias_historia.id',
                'tipos_de_diligencias.nombre As tipo_diligencia_nombre',
                'departamentos.nombre As departamento_nombre',
                'ciudades.nombre As ciudad_nombre',
                'empresas.nombre As empresa_nombre',
                'procesos_judiciales.numero_proceso As numero_proceso',
                'diligencias_historia.empresa_id',
                'diligencias_historia.proceso_id',
                'diligencias_historia.tipo_diligencia_id',
                'diligencias_historia.fecha_diligencia',
                'diligencias_historia.valor_diligencia',
                'diligencias_historia.gastos_envio',
                'diligencias_historia.otros_gastos',
                'diligencias_historia.costo_diligencia',
                'diligencias_historia.costo_envio',
                'diligencias_historia.otros_costos',
                'diligencias_historia.observaciones_cliente',
                'diligencias_historia.observaciones_internas',
                'diligencias_historia.indicativo_cobro',
                'diligencias_historia.url_anexo_diligencia',
                'diligencias_historia.archivo_anexo_diligencia',
                'diligencias_historia.estado_diligencia',
                'diligencias_historia.usuario_creacion_id',
                'diligencias_historia.usuario_creacion_nombre',
                'diligencias_historia.usuario_modificacion_id',
                'diligencias_historia.usuario_modificacion_nombre',
                'diligencias_historia.created_at AS fecha_creacion',
                'diligencias_historia.updated_at AS fecha_modificacion'
            );

        if(isset($dto['numero_diligencia'])){
            $query->where('diligencias_historia.id', '=', $dto['numero_diligencia']);
        }
        if(isset($dto['empresa_cliente'])){
            $query->where('empresas.nombre', 'like', '%' . $dto['empresa_cliente'] . '%');
        }
        if(isset($dto['numero_proceso'])){
            $query->where('procesos_judiciales.numero_proceso', 'like', '%' . $dto['numero_proceso'] . '%');
        }
        if(isset($dto['estado_diligencia'])){
            $query->where('diligencias_historia.estado_diligencia', '=', $dto['estado_diligencia']);
        }
        if (isset($dto['fecha_inicial'])){
            $query->where('diligencias_historia.updated_at', '>=', $dto['fecha_inicial'] . ' 00:00:00');
        }
        if (isset($dto['fecha_final'])){
            $query->where('diligencias_historia.updated_at', '<=', $dto['fecha_final'] . ' 23:59:59');
        }

        if (isset($dto['ordenar_por']) && count($dto['ordenar_por']) > 0){
            foreach ($dto['ordenar_por'] as $attribute => $value){
                if($attribute == 'numero_diligencia'){
                    $query->orderBy('diligencias_historia.id', $value);
                }
                if($attribute == 'fecha'){
                    $query->orderBy('diligencias_historia.fecha_diligencia', $value);
                }
                if($attribute == 'empresa_cliente'){
                    $query->orderBy('empresas.nombre', $value);
                }
                if($attribute == 'proceso'){
                    $query->orderBy('procesos_judiciales.valor_diligencia', $value);
                }
                if($attribute == 'ciudad'){
                    $query->orderBy('ciudades.nombre', $value);
                }
                if($attribute == 'tipo_diligencia'){
                    $query->orderBy('tipos_de_diligencias_historia.nombre', $value);
                }
                if($attribute == 'valor_total'){
                    $query->orderByRaw('(diligencias_historia.valor_diligencia + diligencias_historia.gastos_envio + diligencias_historia.valor_diligencia) ' . $value);
                }
                if($attribute == 'estado_diligencia'){
                    $query->orderBy('diligencias_historia.estado_diligencia', $value);
                }
                if($attribute == 'usuario_creacion_nombre'){
                    $query->orderBy('diligencias_historia.usuario_creacion_nombre', $value);
                }
                if($attribute == 'usuario_modificacion_nombre'){
                    $query->orderBy('diligencias_historia.usuario_modificacion_nombre', $value);
                }
                if($attribute == 'fecha_creacion'){
                    $query->orderBy('diligencias_historia.created_at', $value);
                }
                if($attribute == 'fecha_modificacion'){
                    $query->orderBy('diligencias_historia.updated_at', $value);
                }
            }
        }else{
            $query->orderBy('diligencias_historia.secuencia', "asc");
        }

        $diligencias = $query->paginate($dto['limite'] ?? 100);
        return $diligencias;
    }

}
