<?php

namespace App\Model\Administracion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ConsultaRemanente extends Model
{

    public static function obtenerColeccion($dto)
    {
        $user = Auth::user();
        $empresa = $user->empresa();
        $dto['empresa_id'] = $empresa->id;
        $query1 = DB::table('actuaciones')
            ->join('ciudades', 'ciudades.id', '=', 'actuaciones.ciudad_id')
            ->join('juzgados', 'juzgados.id', '=', 'actuaciones.juzgado_id')
            ->join('despachos', 'despachos.id', '=', 'actuaciones.despacho_id')
            ->join('procesos_judiciales', 'procesos_judiciales.id', '=', 'actuaciones.proceso_id')
            ->leftJoin('tipos_notificaciones', 'tipos_notificaciones.id', '=', 'actuaciones.tipo_notificacion_id')
            ->select(
                'actuaciones.id',
                'actuaciones.numero_proceso',
                'procesos_judiciales.descripcion_clase_proceso AS descripcion_clase_proceso',
                'tipos_notificaciones.nombre AS tipo_notificacion',
                'actuaciones.nombres_demandantes',
                'actuaciones.nombres_demandados',
                'actuaciones.anotacion',
                'juzgados.nombre AS juzgado_nombre',
                'despachos.despacho AS despacho_numero',
                'actuaciones.fecha_actuacion',
                'actuaciones.url_archivo_anexo',
                'actuaciones.usuario_creacion_id',
                'actuaciones.usuario_creacion_nombre',
                'actuaciones.usuario_modificacion_id',
                'actuaciones.usuario_modificacion_nombre',
                'actuaciones.created_at AS fecha_creacion',
                'actuaciones.updated_at AS fecha_modificacion'
            )
            ->where('actuaciones.empresa_id', '=', $dto['empresa_id']);

        if (isset($dto['ciudad'])){
            $query1->where('actuaciones.ciudad_id', '=', $dto['ciudad']);
        }
        if (isset($dto['juzgado'])){
            $query1->where('actuaciones.juzgado_id', '=', $dto['juzgado']);
        }
        if (isset($dto['despacho'])){
            $query1->where('actuaciones.despacho_id', '=', $dto['despacho']);
        }
        if (isset($dto['tipo_notificacion'])){
            $query1->where('actuaciones.tipo_notificacion_id', '=', $dto['tipo_notificacion']);
        }
        if (isset($dto['radicado'])){
            $query1->where('actuaciones.numero_proceso', 'like', '%'. $dto['radicado'] .'%');
        }
        if (isset($dto['descripcion_clase_proceso'])){
            $query1->where('procesos_judiciales.descripcion_clase_proceso', 'like', '%'. $dto['descripcion_clase_proceso'] . '%');
        }
        if (isset($dto['detalle'])){
            $query1->where('actuaciones.anotacion', 'like', '%'. $dto['detalle'] . '%');
        }
        if (isset($dto['demandante'])){
            $query1->where('actuaciones.nombres_demandantes', 'like', '%'. $dto['demandante'] . '%');
        }
        if (isset($dto['demandado'])){
            $query1->where('actuaciones.nombres_demandados', 'like', '%'. $dto['demandado'] . '%');
        }
        if (isset($dto['fecha_inicial'])){
            $query1->where('actuaciones.fecha_actuacion', '>=', $dto['fecha_inicial']);
        }
        if (isset($dto['fecha_final'])){
            $query1->where('actuaciones.fecha_actuacion', '<=', $dto['fecha_final']);
        }

        if (isset($dto['ordenar_por']) && count($dto['ordenar_por']) > 0){
            foreach ($dto['ordenar_por'] as $attribute => $value){
                if($attribute == 'radicado'){
                    $query1->orderBy('actuaciones.numero_proceso', $value);
                }
                if($attribute == 'descripcion_clase_proceso'){
                    $query1->orderBy('procesos_judiciales.descripcion_clase_proceso', $value);
                }
                if($attribute == 'tipo_notificacion'){
                    $query1->orderBy('tipos_notificaciones.nombre', $value);
                }
                if($attribute == 'demandante'){
                    $query1->orderBy('actuaciones.nombres_demandantes', $value);
                }
                if($attribute == 'demandado'){
                    $query1->orderBy('actuaciones.nombres_demandados', $value);
                }
                if($attribute == 'juzgado'){
                    $query1->orderBy('juzgados.nombre', $value);
                }
                if($attribute == 'despacho'){
                    $query1->orderBy('despachos.nombre', $value);
                }
                if($attribute == 'fecha'){
                    $query1->orderBy('actuaciones.fecha_actuacion', $value);
                }
            }
        }else{
            $query1->orderBy("actuaciones.id", "desc")
                ->orderBy("actuaciones.numero_proceso", "asc");
        }

        $query2 = DB::table('expedientes')
            ->join('ciudades', 'ciudades.id', '=', 'expedientes.ciudad_id')
            ->join('juzgados', 'juzgados.id', '=', 'expedientes.juzgado_id')
            ->join('despachos', 'despachos.id', '=', 'expedientes.despacho_id')
            ->join('procesos_judiciales', 'procesos_judiciales.id', '=', 'expedientes.proceso_id')
            ->leftJoin('tipos_notificaciones', 'tipos_notificaciones.id', '=', 'expedientes.tipo_notificacion_id')
            ->select(
                'expedientes.id',
                'expedientes.numero_proceso',
                'procesos_judiciales.descripcion_clase_proceso AS descripcion_clase_proceso',
                'tipos_notificaciones.nombre AS tipo_notificacion',
                'expedientes.nombres_demandantes',
                'expedientes.nombres_demandados',
                'expedientes.anotacion',
                'juzgados.nombre AS juzgado_nombre',
                'despachos.despacho AS despacho_numero',
                'expedientes.fecha_actuacion',
                'expedientes.url_archivo_anexo',
                'expedientes.usuario_creacion_id',
                'expedientes.usuario_creacion_nombre',
                'expedientes.usuario_modificacion_id',
                'expedientes.usuario_modificacion_nombre',
                'expedientes.created_at AS fecha_creacion',
                'expedientes.updated_at AS fecha_modificacion'
            )
            ->where('expedientes.empresa_id', '=', $dto['empresa_id']);

        if (isset($dto['ciudad'])){
            $query2->where('expedientes.ciudad_id', '=', $dto['ciudad']);
        }
        if (isset($dto['juzgado'])){
            $query2->where('expedientes.juzgado_id', '=', $dto['juzgado']);
        }
        if (isset($dto['despacho'])){
            $query2->where('expedientes.despacho_id', '=', $dto['despacho']);
        }
        if (isset($dto['tipo_notificacion'])){
            $query2->where('expedientes.tipo_notificacion_id', '=', $dto['tipo_notificacion']);
        }
        if (isset($dto['radicado'])){
            $query2->where('expedientes.numero_proceso', 'like', '%'. $dto['radicado'] .'%');
        }
        if (isset($dto['descripcion_clase_proceso'])){
            $query2->where('procesos_judiciales.descripcion_clase_proceso', 'like', '%'. $dto['descripcion_clase_proceso'] . '%');
        }
        if (isset($dto['detalle'])){
            $query2->where('expedientes.anotacion', 'like', '%'. $dto['detalle'] . '%');
        }
        if (isset($dto['demandante'])){
            $query2->where('expedientes.nombres_demandantes', 'like', '%'. $dto['demandante'] . '%');
        }
        if (isset($dto['demandado'])){
            $query2->where('expedientes.nombres_demandados', 'like', '%'. $dto['demandado'] . '%');
        }
        if (isset($dto['fecha_inicial'])){
            $query2->where('expedientes.fecha_actuacion', '>=', $dto['fecha_inicial']);
        }
        if (isset($dto['fecha_final'])){
            $query2->where('expedientes.fecha_actuacion', '<=', $dto['fecha_final']);
        }

        if (isset($dto['ordenar_por']) && count($dto['ordenar_por']) > 0){
            foreach ($dto['ordenar_por'] as $attribute => $value){
                if($attribute == 'radicado'){
                    $query2->orderBy('expedientes.numero_proceso', $value);
                }
                if($attribute == 'descripcion_clase_proceso'){
                    $query2->orderBy('procesos_judiciales.descripcion_clase_proceso', $value);
                }
                if($attribute == 'tipo_notificacion'){
                    $query2->orderBy('tipos_notificaciones.nombre', $value);
                }
                if($attribute == 'demandante'){
                    $query2->orderBy('expedientes.nombres_demandantes', $value);
                }
                if($attribute == 'demandado'){
                    $query2->orderBy('expedientes.nombres_demandados', $value);
                }
                if($attribute == 'juzgado'){
                    $query2->orderBy('juzgados.nombre', $value);
                }
                if($attribute == 'despacho'){
                    $query2->orderBy('despachos.nombre', $value);
                }
                if($attribute == 'fecha'){
                    $query2->orderBy('expedientes.fecha_actuacion', $value);
                }
            }
        }else{
            $query2->orderBy("expedientes.fecha_actuacion", "desc")
                ->orderBy("expedientes.numero_proceso", "asc");
        }
        //Unión de las dos búsquedas
        $query = $query1->union($query2);

        $movimientos = $query->paginate($dto['limite'] ?? 100);
        return $movimientos;
    }

}
