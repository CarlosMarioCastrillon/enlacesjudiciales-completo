<?php

namespace App\Model\Administracion;

use App\Enum\TipoMovimientoEnum;
use App\Model\Configuracion\Ciudad;
use App\Model\Configuracion\ClaseProceso;
use App\Model\Configuracion\Despacho;
use App\Model\Configuracion\Empresa;
use App\Model\Configuracion\Juzgado;
use App\Model\Configuracion\TipoNotificacion;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Actuacion extends Model
{

    protected $table = 'actuaciones';

    protected $fillable = [
        'numero_proceso',
        'numero_radicado_proceso_actual',
        'actuacion',
        'anotacion',
        'fecha_registro',
        'fecha_actuacion',
        'fecha_inicio_termino',
        'fecha_vencimiento_termino',
        'ind_actuacion_audiencia',
        'tipo_movimiento',
        'nombres_demandantes',
        'nombres_demandados',
        'url_archivo_anexo',
        'observaciones',
        'ruta_archivo_anexo',
        'nombre_archivo_anexo',
        'ruta_archivo_estado_cartelera',
        'nombre_archivo_estado_cartelera',
        'descripcion_clase_proceso',
        'proceso_id',
        'ciudad_id',
        'juzgado_id',
        'juzgado_origen_id',
        'despacho_id',
        'despacho_origen_id',
        'empresa_id',
        'etapa_proceso_id',
        'tipo_actuacion_id',
        'tipo_notificacion_id',
        'estado'
    ];

    public function empresa(){
        return $this->belongsTo(Empresa::class);
    }

    public function ciudad(){
        return $this->belongsTo(Ciudad::class);
    }

    public function juzgado(){
        return $this->belongsTo(Juzgado::class);
    }

    public function juzgadoOrigen(){
        return $this->belongsTo(Juzgado::class);
    }

    public function despacho(){
        return $this->belongsTo(Despacho::class);
    }

    public function despachoOrigen(){
        return $this->belongsTo(Despacho::class);
    }

    public function etapaProceso(){
        return $this->belongsTo(ClaseProceso::class, 'etapa_proceso_id','id');
    }

    public function procesoJudicial(){
        return $this->belongsTo(ProcesoJudicial::class, 'proceso_id');
    }

    public function tipoNotificacion(){
        return $this->belongsTo(TipoNotificacion::class, 'tipo_notificacion_id');
    }

    /**
     * Obtener la colección de actuaciones
     * @param $dto
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function obtenerColeccion($dto)
    {
        $user = Auth::user();
        $empresa = $user->empresa();
        $dto['empresa_id'] = $empresa->id;
        $query = DB::table('actuaciones')
            ->join('ciudades', 'ciudades.id', '=', 'actuaciones.ciudad_id')
            ->join('departamentos', 'departamentos.id', '=', 'ciudades.departamento_id')
            ->join('juzgados', 'juzgados.id', '=', 'actuaciones.juzgado_id')
            ->join('despachos', 'despachos.id', '=', 'actuaciones.despacho_id')
            ->join('procesos_judiciales', 'procesos_judiciales.id', '=', 'actuaciones.proceso_id')
            ->leftJoin('etapas_de_procesos', 'etapas_de_procesos.id', '=', 'actuaciones.etapa_proceso_id')
            ->leftJoin('tipos_de_actuaciones', 'tipos_de_actuaciones.id', '=', 'actuaciones.tipo_actuacion_id')
            ->leftJoin('tipos_notificaciones', 'tipos_notificaciones.id', '=', 'actuaciones.tipo_notificacion_id')
            ->join('empresas', 'empresas.id', '=', 'actuaciones.empresa_id')
            ->select(
                'actuaciones.id',
                'departamentos.id AS departamento_id',
                'departamentos.nombre AS departamento_nombre',
                'departamentos.codigo_dane AS departamento_codigo_dane',
                'ciudades.id AS ciudad_id',
                'ciudades.nombre AS ciudad_nombre',
                'ciudades.codigo_dane AS ciudad_codigo_dane',
                'juzgados.id AS juzgado_id',
                'juzgados.nombre AS juzgado_nombre',
                'juzgados.juzgado AS juzgado_numero',
                'despachos.id AS despacho_id',
                'despachos.despacho AS despacho_numero',
                'actuaciones.numero_radicado_proceso_actual',
                'actuaciones.nombres_demandantes AS demandantes',
                'actuaciones.nombres_demandados AS demandados',
                'actuaciones.anotacion',
                'actuaciones.fecha_registro',
                'tipos_notificaciones.id AS tipo_notificacion_id',
                'tipos_notificaciones.nombre AS tipo_notificacion_nombre',
                'actuaciones.descripcion_clase_proceso'
            );
        //->where('actuaciones.empresa_id', '=', $dto['empresa_id']);

        if (isset($dto['departamento_id'])){
            $query->where('ciudades.departamento_id', $dto['departamento_id']);
        }
        if (isset($dto['ciudad_id'])){
            $query->where('actuaciones.ciudad_id', $dto['ciudad_id']);
        }
        if (isset($dto['juzgado_id'])){
            $query->where('actuaciones.juzgado_id', $dto['juzgado_id']);
        }
        if (isset($dto['despacho_id'])){
            $query->where('actuaciones.despacho_id', $dto['despacho_id']);
        }
        if (isset($dto['tipo_notificacion_id'])){
            $query->where('actuaciones.tipo_notificacion_id', $dto['tipo_notificacion_id']);
        }
        if (isset($dto['numero_radicado_actual'])){
            $query->where('actuaciones.numero_radicado_proceso_actual', 'like', '%' . $dto['numero_radicado_actual'] . '%');
        }
        if (isset($dto['fecha_desde'])){
            $query->where('actuaciones.fecha_registro', '>=', $dto['fecha_desde']);
        }
        if (isset($dto['fecha_hasta'])){
            $query->where('actuaciones.fecha_registro', '<=', $dto['fecha_hasta']);
        }

        if (isset($dto['ordenar_por']) && count($dto['ordenar_por']) > 0){
            foreach ($dto['ordenar_por'] as $attribute => $value){
                if($attribute == 'ciudad_nombre'){
                    $query->orderBy('ciudades.nombre', $value);
                }
                if($attribute == 'juzgado_nombre'){
                    $query->orderBy('juzgados.nombre', $value);
                }
                if($attribute == 'despacho_numero'){
                    $query->orderBy('despachos.despacho', $value);
                }
                if($attribute == 'numero_radicado_actual'){
                    $query->orderBy('actuaciones.numero_radicado_proceso_actual', $value);
                }
                if($attribute == 'demandante'){
                    $query->orderBy('actuaciones.nombres_demandantes', $value);
                }
                if($attribute == 'demandado'){
                    $query->orderBy('actuaciones.nombres_demandados', $value);
                }
                if($attribute == 'anotacion'){
                    $query->orderBy('actuaciones.anotacion', $value);
                }
                if($attribute == 'fecha_registro'){
                    $query->orderBy('actuaciones.fecha_registro', $value);
                }
                if($attribute == 'tipo_notificacion_nombre'){
                    $query->orderBy('tipos_notificaciones.nombre', $value);
                }
                if($attribute == 'descripcion_clase_proceso'){
                    $query->orderBy('actuaciones.descripcion_clase_proceso', $value);
                }
            }
        }else{
            $query->orderBy("actuaciones.id", "desc");
        }

        $actuaciones = $query->paginate($dto['limite'] ?? 100);
        return $actuaciones;
    }

    public static function obtenerTodo($dto)
    {
        $user = Auth::user();
        $empresa = $user->empresa();
        $dto['empresa_id'] = $empresa->id;
        $query = Actuacion::query()
            ->join('ciudades', 'ciudades.id', '=', 'actuaciones.ciudad_id')
            ->select(
                'actuaciones.*'
            );
        //->where('actuaciones.empresa_id', '=', $dto['empresa_id']);

        if(
            !(isset($dto['filtro_todo']) && $dto['filtro_todo']) &&
            isset($dto['filtro_actuacion_ids']) && count($dto['filtro_actuacion_ids']) > 0
        ){
            $query->whereIn('actuaciones.id', $dto['filtro_actuacion_ids']);
        }else{
            if (isset($dto['filtro_departamento_id'])){
                $query->where('ciudades.departamento_id', $dto['filtro_departamento_id']);
            }
            if (isset($dto['filtro_ciudad_id'])){
                $query->where('actuaciones.ciudad_id', $dto['filtro_ciudad_id']);
            }
            if (isset($dto['filtro_juzgado_id'])){
                $query->where('actuaciones.juzgado_id', $dto['filtro_juzgado_id']);
            }
            if (isset($dto['filtro_despacho_id'])){
                $query->where('actuaciones.despacho_id', $dto['filtro_despacho_id']);
            }
            if (isset($dto['filtro_tipo_notificacion_id'])){
                $query->where('actuaciones.tipo_notificacion_id', $dto['filtro_tipo_notificacion_id']);
            }
            if (isset($dto['filtro_numero_radicado_actual'])){
                $query->where('actuaciones.numero_radicado_proceso_actual', 'like', '%' . $dto['filtro_numero_radicado_actual'] . '%');
            }
            if (isset($dto['filtro_fecha_desde'])){
                $query->where('actuaciones.fecha_registro', '>=', $dto['filtro_fecha_desde']);
            }
            if (isset($dto['filtro_fecha_hasta'])){
                $query->where('actuaciones.fecha_registro', '<=', $dto['filtro_fecha_hasta']);
            }
        }

        $actuaciones = $query->get();
        return $actuaciones;
    }

    /**
     * Obtener el reporte de actuaciones
     * @param $dto
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function obtenerReporte($dto){
        $query = DB::table('actuaciones')
            ->join('ciudades', 'ciudades.id', '=', 'actuaciones.ciudad_id')
            ->join('juzgados', 'juzgados.id', '=', 'actuaciones.juzgado_id')
            ->join('despachos', 'despachos.id', '=', 'actuaciones.despacho_id')
            ->join('procesos_judiciales', 'procesos_judiciales.id', '=', 'actuaciones.proceso_id')
            ->join('empresas', 'empresas.id', '=', 'actuaciones.empresa_id')
            ->leftJoin('etapas_de_procesos', 'etapas_de_procesos.id', '=', 'actuaciones.etapa_proceso_id')
            ->leftJoin('tipos_de_actuaciones', 'tipos_de_actuaciones.id', '=', 'actuaciones.tipo_actuacion_id')
            ->leftJoin('tipos_notificaciones', 'tipos_notificaciones.id', '=', 'actuaciones.tipo_notificacion_id')
            ->select(
                'actuaciones.id',
                'ciudades.nombre AS ciudad_nombre',
                'juzgados.nombre AS juzgado_nombre',
                'despachos.despacho AS despacho_numero',
                'tipos_de_actuaciones.nombre AS tipo_actuacion_nombre',
                'etapas_de_procesos.nombre AS etapa_proceso_nombre',
                'actuaciones.proceso_id',
                'actuaciones.numero_proceso',
                'actuaciones.actuacion',
                'actuaciones.anotacion',
                DB::raw("0 as tipo_actuacion"),
                'actuaciones.fecha_registro',
                'actuaciones.fecha_actuacion',
                'actuaciones.fecha_inicio_termino',
                'actuaciones.fecha_vencimiento_termino',
                'actuaciones.ind_actuacion_audiencia',
                'actuaciones.tipo_movimiento',
                'actuaciones.nombres_demandantes',
                'actuaciones.nombres_demandados',
                'actuaciones.nombre_archivo_anexo',
                'actuaciones.observaciones',
                'actuaciones.usuario_creacion_id',
                'actuaciones.usuario_creacion_nombre',
                'actuaciones.usuario_modificacion_id',
                'actuaciones.usuario_modificacion_nombre',
                'actuaciones.created_at AS fecha_creacion',
                'actuaciones.updated_at AS fecha_modificacion'
            );

        if (isset($dto['ciudad'])){
            $query->where('actuaciones.ciudad_id', '=', $dto['ciudad']);
        }
        if (isset($dto['proceso'])){
            $query->where('actuaciones.numero_proceso', 'like', '%' . $dto['proceso'] . '%');
        }
        if (isset($dto['demandante'])){
            $query->where('actuaciones.nombres_demandantes', 'like', '%' . $dto['demandante'] . '%');
        }
        if (isset($dto['demandado'])){
            $query->where('actuaciones.nombres_demandados', 'like', '%' . $dto['demandado'] . '%');
        }
        if (isset($dto['fecha'])){
            $query->where('actuaciones.fecha_actuacion', '=', $dto['fecha']);
        }
        if (isset($dto['juzgado'])){
            $query->where('actuaciones.juzgado_id', '=', $dto['juzgado']);
        }
        if (isset($dto['despacho'])){
            $query->where('actuaciones.despacho_id', '=', $dto['despacho']);
        }

        if (isset($dto['ordenar_por']) && count($dto['ordenar_por']) > 0){
            foreach ($dto['ordenar_por'] as $attribute => $value){
                if($attribute == 'nombre_ciudad'){
                    $query->orderBy('ciudades.nombre', $value);
                }
                if($attribute == 'proceso'){
                    $query->orderBy('actuaciones.numero_proceso', $value);
                }
            }
        }else{
            $query->orderBy("actuaciones.id", "desc");
        }

        $movimientos = $query->paginate($dto['limite'] ?? 100);
        return $movimientos;
    }

    /**
     * Obtener las actuaciones y expedientes por empresa
     * @param $dto
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function obtenerReportePorEmpresa($dto)
    {
        $user = Auth::user();
        $empresa = $user->empresa();
        $dto['empresa_id'] = $empresa->id;
        $query = DB::table('actuaciones')
            ->join('ciudades', 'ciudades.id', '=', 'actuaciones.ciudad_id')
            ->join('juzgados', 'juzgados.id', '=', 'actuaciones.juzgado_id')
            ->join('despachos', 'despachos.id', '=', 'actuaciones.despacho_id')
            ->join('procesos_judiciales', 'procesos_judiciales.id', '=', 'actuaciones.proceso_id')
            ->join('empresas', 'empresas.id', '=', 'actuaciones.empresa_id')
            ->leftJoin('etapas_de_procesos', 'etapas_de_procesos.id', '=', 'actuaciones.etapa_proceso_id')
            ->leftJoin('tipos_de_actuaciones', 'tipos_de_actuaciones.id', '=', 'actuaciones.tipo_actuacion_id')
            ->leftJoin('tipos_notificaciones', 'tipos_notificaciones.id', '=', 'actuaciones.tipo_notificacion_id')
            ->where('actuaciones.empresa_id', '=', $dto['empresa_id'])
            ->select(
                'actuaciones.id',
                'ciudades.nombre AS ciudad_nombre',
                'juzgados.nombre AS juzgado_nombre',
                'despachos.despacho AS despacho_numero',
                'tipos_de_actuaciones.nombre AS tipo_actuacion_nombre',
                'etapas_de_procesos.nombre AS etapa_proceso_nombre',
                'actuaciones.proceso_id',
                'actuaciones.numero_proceso',
                'actuaciones.actuacion',
                'actuaciones.anotacion',
                DB::raw("0 as tipo_actuacion"),
                'actuaciones.fecha_registro',
                'actuaciones.fecha_actuacion',
                'actuaciones.fecha_inicio_termino',
                'actuaciones.fecha_vencimiento_termino',
                'actuaciones.ind_actuacion_audiencia',
                'actuaciones.tipo_movimiento',
                'actuaciones.nombres_demandantes',
                'actuaciones.nombres_demandados',
                'actuaciones.nombre_archivo_anexo',
                'actuaciones.observaciones',
                'actuaciones.usuario_creacion_id',
                'actuaciones.usuario_creacion_nombre',
                'actuaciones.usuario_modificacion_id',
                'actuaciones.usuario_modificacion_nombre',
                'actuaciones.created_at AS fecha_creacion',
                'actuaciones.updated_at AS fecha_modificacion'
            );

        if (isset($dto['fecha_inicial'])){
            $query->where('actuaciones.fecha_actuacion', '>=', $dto['fecha_inicial']);
        }
        if (isset($dto['fecha_final'])){
            $query->where('actuaciones.fecha_actuacion', '<=', $dto['fecha_final']);
        }
        if (isset($dto['fecha'])){
            $query->where('actuaciones.fecha_actuacion', '>=', $dto['fecha']);
        }
        if (isset($dto['ciudad'])){
            $query->where('actuaciones.ciudad_id', '=', $dto['ciudad']);
        }
        if (isset($dto['juzgado'])){
            $query->where('actuaciones.juzgado_id', '=', $dto['juzgado']);
        }
        if (isset($dto['despacho'])){
            $query->where('actuaciones.despacho_id', '=', $dto['despacho']);
        }
        if (isset($dto['proceso'])){
            $query->where('actuaciones.numero_proceso', 'like', '%' . $dto['proceso'] . '%');
        }
        if (isset($dto['con_achivo_anexo']) && $dto['con_achivo_anexo']){
            $query->whereNotNull('actuaciones.ruta_archivo_anexo');
        }
        if (isset($dto['tipo_movimiento'])){
            $query->whereIn('actuaciones.tipo_movimiento', ['AC', 'EX', 'VG']);
        }

        if (isset($dto['ordenar_por']) && count($dto['ordenar_por']) > 0){
            foreach ($dto['ordenar_por'] as $attribute => $value){
                if($attribute == 'nombre_ciudad'){
                    $query->orderBy('ciudades.nombre', $value);
                }
                if($attribute == 'fecha'){
                    $query->orderBy('actuaciones.fecha_actuacion', $value);
                }
                if($attribute == 'proceso'){
                    $query->orderBy('actuaciones.numero_proceso', $value);
                }
            }
        }else{
            $query->orderBy("actuaciones.id", "desc")
                ->orderBy("actuaciones.numero_proceso", "asc");
        }

        $movimientos = $query->paginate($dto['limite'] ?? 100);
        return $movimientos;
    }

    /**
     * Obtener los remates
     * @param $dto
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function obtenerRemates($dto)
    {
        $query = DB::table('actuaciones')
            ->join('ciudades', 'ciudades.id', '=', 'actuaciones.ciudad_id')
            ->join('juzgados', 'juzgados.id', '=', 'actuaciones.juzgado_id')
            ->join('despachos', 'despachos.id', '=', 'actuaciones.despacho_id')
            ->where('actuaciones.tipo_movimiento', '=',TipoMovimientoEnum::REMATE)
            ->select(
                'actuaciones.id',
                'juzgados.nombre AS juzgado_nombre',
                'despachos.despacho AS despacho_numero',
                'actuaciones.numero_proceso',
                'actuaciones.anotacion',
                'actuaciones.fecha_actuacion',
                'actuaciones.nombres_demandantes',
                'actuaciones.nombres_demandados',
                'actuaciones.nombre_archivo_anexo',
                'actuaciones.usuario_creacion_id',
                'actuaciones.usuario_creacion_nombre',
                'actuaciones.usuario_modificacion_id',
                'actuaciones.usuario_modificacion_nombre',
                'actuaciones.created_at AS fecha_creacion',
                'actuaciones.updated_at AS fecha_modificacion'
            );

        if (isset($dto['fecha_inicial'])){
            $query->where('actuaciones.fecha_actuacion', '>=', $dto['fecha_inicial']);
        }
        if (isset($dto['fecha_final'])){
            $query->where('actuaciones.fecha_actuacion', '<=', $dto['fecha_final']);
        }
        if (isset($dto['ciudad'])){
            $query->where('actuaciones.ciudad_id', '=', $dto['ciudad']);
        }
        if (isset($dto['juzgado'])){
            $query->where('actuaciones.juzgado_id', '=', $dto['juzgado']);
        }
        if (isset($dto['despacho'])){
            $query->where('actuaciones.despacho_id', '=', $dto['despacho']);
        }

        if (isset($dto['ordenar_por']) && count($dto['ordenar_por']) > 0){
            foreach ($dto['ordenar_por'] as $attribute => $value){
                if($attribute == 'nombre_ciudad'){
                    $query->orderBy('ciudades.nombre', $value);
                }
                if($attribute == 'fecha'){
                    $query->orderBy('actuaciones.fecha_actuacion', $value);
                }
                if($attribute == 'proceso'){
                    $query->orderBy('actuaciones.numero_proceso', $value);
                }
                if($attribute == 'demandante'){
                    $query->orderBy('actuaciones.nombres_demandantes', $value);
                }
                if($attribute == 'demandado'){
                    $query->orderBy('actuaciones.nombres_demandados', $value);
                }
                if($attribute == 'anotacion'){
                    $query->orderBy('actuaciones.anotacion', $value);
                }
            }
        }else{
            $query->orderBy("actuaciones.id", "desc")
                ->orderBy("actuaciones.numero_proceso", "asc");
        }

        $movimientos = $query->paginate($dto['limite'] ?? 100);
        return $movimientos;
    }

    /**
     * Obtener los remanentes
     * @param $dto
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function obtenerRemanentes($dto)
    {
        $user = Auth::user();
        $empresa = $user->empresa();
        $dto['empresa_id'] = $empresa->id;
        $query = DB::table('actuaciones')
            ->join('ciudades', 'ciudades.id', '=', 'actuaciones.ciudad_id')
            ->join('juzgados', 'juzgados.id', '=', 'actuaciones.juzgado_id')
            ->join('despachos', 'despachos.id', '=', 'actuaciones.despacho_id')
            ->leftJoin('procesos_judiciales', 'procesos_judiciales.id', '=', 'actuaciones.proceso_id')
            ->leftJoin('tipos_notificaciones', 'tipos_notificaciones.id', '=', 'actuaciones.tipo_notificacion_id')
            ->where('actuaciones.empresa_id', '=', $dto['empresa_id'])
            ->select(
                'actuaciones.id',
                'actuaciones.numero_proceso',
                'procesos_judiciales.descripcion_clase_proceso AS descripcion_clase_proceso',
                'tipos_notificaciones.nombre AS tipo_notificacion',
                'actuaciones.nombres_demandantes',
                'actuaciones.nombres_demandados',
                'actuaciones.anotacion',
                'juzgados.nombre AS juzgado_nombre',
                'despachos.despacho AS despacho_numero',
                'ciudades.nombre AS ciudad_nombre',
                'actuaciones.fecha_actuacion',
                'actuaciones.nombre_archivo_anexo',
                'actuaciones.usuario_creacion_id',
                'actuaciones.usuario_creacion_nombre',
                'actuaciones.usuario_modificacion_id',
                'actuaciones.usuario_modificacion_nombre',
                'actuaciones.created_at AS fecha_creacion',
                'actuaciones.updated_at AS fecha_modificacion'
            );

        if (isset($dto['ciudad'])){
            $query->where('actuaciones.ciudad_id', '=', $dto['ciudad']);
        }
        if (isset($dto['juzgado'])){
            $query->where('actuaciones.juzgado_id', '=', $dto['juzgado']);
        }
        if (isset($dto['despacho'])){
            $query->where('actuaciones.despacho_id', '=', $dto['despacho']);
        }
        if (isset($dto['tipo_notificacion'])){
            $query->where('actuaciones.tipo_notificacion_id', '=', $dto['tipo_notificacion']);
        }
        if (isset($dto['radicado'])){
            $query->where('actuaciones.numero_proceso', 'like', '%'. $dto['radicado'] .'%');
        }
        if (isset($dto['descripcion_clase_proceso'])){
            $query->where('procesos_judiciales.descripcion_clase_proceso', 'like', '%'. $dto['descripcion_clase_proceso'] . '%');
        }
        if (isset($dto['detalle'])){
            $query->where('actuaciones.anotacion', 'like', '%'. $dto['detalle'] . '%');
        }
        if (isset($dto['demandante'])){
            $query->where('actuaciones.nombres_demandantes', 'like', '%'. $dto['demandante'] . '%');
        }
        if (isset($dto['demandado'])){
            $query->where('actuaciones.nombres_demandados', 'like', '%'. $dto['demandado'] . '%');
        }
        if (isset($dto['fecha_inicial'])){
            $query->where('actuaciones.fecha_actuacion', '>=', $dto['fecha_inicial']);
        }
        if (isset($dto['fecha_final'])){
            $query->where('actuaciones.fecha_actuacion', '<=', $dto['fecha_final']);
        }

        if (isset($dto['ordenar_por']) && count($dto['ordenar_por']) > 0){
            foreach ($dto['ordenar_por'] as $attribute => $value){
                if($attribute == 'radicado'){
                    $query->orderBy('actuaciones.numero_proceso', $value);
                }
                if($attribute == 'descripcion_clase_proceso'){
                    $query->orderBy('procesos_judiciales.descripcion_clase_proceso', $value);
                }
                if($attribute == 'tipo_notificacion'){
                    $query->orderBy('tipos_notificaciones.nombre', $value);
                }
                if($attribute == 'demandante'){
                    $query->orderBy('actuaciones.nombres_demandantes', $value);
                }
                if($attribute == 'demandado'){
                    $query->orderBy('actuaciones.nombres_demandados', $value);
                }
                if($attribute == 'detalle'){
                    $query->orderBy('actuaciones.anotacion', $value);
                }
                if($attribute == 'juzgado'){
                    $query->orderBy('juzgados.nombre', $value);
                }
                if($attribute == 'despacho'){
                    $query->orderBy('despachos.nombre', $value);
                }
                if($attribute == 'ciudad'){
                    $query->orderBy('ciudades.nombre', $value);
                }
                if($attribute == 'fecha'){
                    $query->orderBy('actuaciones.fecha_actuacion', $value);
                }
            }
        }else{
            $query->orderBy("actuaciones.fecha_actuacion", "desc");
        }

        $movimientos = $query->paginate($dto['limite'] ?? 100);
        return $movimientos;
    }

    /**
     * Obtener las audiencias pendientes
     * @param $dto
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function obtenerAudienciasPendientes($dto)
    {
        $user = Auth::user();
        $empresa = $user->empresa();
        $dto['empresa_id'] = $empresa->id;
        $dto['fecha_actual'] = Carbon::today()->format("Y-m-d");
        $query = DB::table('actuaciones')
            ->where('actuaciones.empresa_id', '=', $dto['empresa_id'])
            ->where('actuaciones.ind_actuacion_audiencia', '=', 'AU')
            ->where('actuaciones.fecha_vencimiento_termino', '>=', $dto['fecha_actual'])
            ->select(
                'actuaciones.id',
                'actuaciones.numero_proceso',
                'actuaciones.nombres_demandantes',
                'actuaciones.nombres_demandados',
                'actuaciones.anotacion',
                'actuaciones.fecha_actuacion',
                'actuaciones.fecha_vencimiento_termino',
                DB::raw("DATEDIFF(actuaciones.fecha_vencimiento_termino, curdate()) as falta"),
                'actuaciones.nombre_archivo_anexo',
                'actuaciones.observaciones',
                'actuaciones.usuario_creacion_id',
                'actuaciones.usuario_creacion_nombre',
                'actuaciones.usuario_modificacion_id',
                'actuaciones.usuario_modificacion_nombre',
                'actuaciones.created_at AS fecha_creacion',
                'actuaciones.updated_at AS fecha_modificacion'
            );

        if (isset($dto['proceso'])){
            $query->where('actuaciones.numero_proceso', 'like', '%'. $dto['proceso'] .'%');
        }
        if (isset($dto['actuacion'])){
            $query->where('actuaciones.observaciones', 'like', '%'. $dto['actuacion'] . '%');
        }
        if (isset($dto['demandante'])){
            $query->where('actuaciones.nombres_demandantes', 'like', '%'. $dto['demandante'] . '%');
        }
        if (isset($dto['demandado'])){
            $query->where('actuaciones.nombres_demandados', 'like', '%'. $dto['demandado'] . '%');
        }

        if(
            (isset($dto['vence_hoy']) && $dto['vence_hoy'] == "true") ||
            (isset($dto['por_vencer']) && $dto['por_vencer'] == "true") ||
            (isset($dto['pendientes']) && $dto['pendientes'] == "true") ||
            (isset($dto['mas_de_diz_dias']) && $dto['mas_de_diz_dias'] == "true")
        ){
            $query->where(function ($q) use($dto) {
                if(isset($dto['vence_hoy']) && $dto['vence_hoy'] == "true"){
                    $q->orWhere('actuaciones.fecha_vencimiento_termino', $dto['fecha_actual']);
                }
                if(isset($dto['por_vencer']) && $dto['por_vencer'] == "true"){
                    $q->orWhere(function($q){
                        $q->where('actuaciones.fecha_vencimiento_termino', '>=', Carbon::today()->addDays(1)->format("Y-m-d"))
                            ->where('actuaciones.fecha_vencimiento_termino', '<=', Carbon::today()->addDays(3)->format("Y-m-d"));
                    });
                }
                if(isset($dto['pendientes']) && $dto['pendientes'] == "true"){
                    $q->orWhere(function($q){
                        $q->where('actuaciones.fecha_vencimiento_termino', '>=', Carbon::today()->addDays(4)->format("Y-m-d"))
                            ->where('actuaciones.fecha_vencimiento_termino', '<=', Carbon::today()->addDays(10)->format("Y-m-d"));
                    });
                }
                if(isset($dto['mas_de_diz_dias']) && $dto['mas_de_diz_dias'] == "true"){
                    $q->orWhere('actuaciones.fecha_vencimiento_termino', '>', Carbon::today()->addDays(10)->format("Y-m-d"));
                }
            });
        }

        if (isset($dto['ordenar_por']) && count($dto['ordenar_por']) > 0){
            foreach ($dto['ordenar_por'] as $attribute => $value){
                if($attribute == 'radicado'){
                    $query->orderBy('actuaciones.numero_proceso', $value);
                }
                if($attribute == 'demandante'){
                    $query->orderBy('actuaciones.nombres_demandantes', $value);
                }
                if($attribute == 'demandado'){
                    $query->orderBy('actuaciones.nombres_demandados', $value);
                }
                if($attribute == 'actuacion'){
                    $query->orderBy('actuaciones.observaciones', $value);
                }
                if($attribute == 'fecha_vencimiento'){
                    $query->orderBy('actuaciones.fecha_vencimiento_termino', $value);
                }
                if($attribute == 'ultima_actualizacion'){
                    $query->orderBy('actuaciones.updated_at', $value);
                }
                if($attribute == 'actualizado_por'){
                    $query->orderBy('actuaciones.usuario_modificacion_nombre', $value);
                }
                if($attribute == 'falta'){
                    $query->orderBy('actuaciones.fecha_vencimiento_termino', $value);
                }
            }
        }else{
            $query->orderBy("actuaciones.fecha_vencimiento_termino", "asc");
        }

        $movimientos = $query->paginate($dto['limite'] ?? 100);
        return $movimientos;
    }

    /**
     * Obtener las audiencias cumplidas
     * @param $dto
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function obtenerAudienciasCumplidas($dto)
    {
        $user = Auth::user();
        $empresa = $user->empresa();
        $dto['empresa_id'] = $empresa->id;
        $dto['fecha_actual'] = Carbon::today();
        $query = DB::table('actuaciones')
            ->where('actuaciones.empresa_id', '=', $dto['empresa_id'])
            ->where('actuaciones.ind_actuacion_audiencia', '=', 'AU')
            ->where('actuaciones.fecha_vencimiento_termino', '<', $dto['fecha_actual'])
            ->select(
                'actuaciones.id',
                'actuaciones.numero_proceso',
                'actuaciones.nombres_demandantes',
                'actuaciones.nombres_demandados',
                'actuaciones.anotacion',
                'actuaciones.fecha_actuacion',
                'actuaciones.fecha_vencimiento_termino',
                DB::raw("DATEDIFF(fecha_vencimiento_termino, curdate()) as falta"),
                'actuaciones.nombre_archivo_anexo',
                'actuaciones.observaciones',
                'actuaciones.usuario_creacion_id',
                'actuaciones.usuario_creacion_nombre',
                'actuaciones.usuario_modificacion_id',
                'actuaciones.usuario_modificacion_nombre',
                'actuaciones.created_at AS fecha_creacion',
                'actuaciones.updated_at AS fecha_modificacion'
            );

        if (isset($dto['proceso'])){
            $query->where('actuaciones.numero_proceso', 'like', '%'. $dto['proceso'] .'%');
        }
        if (isset($dto['actuacion'])){
            $query->where('actuaciones.observaciones', 'like', '%'. $dto['actuacion'] . '%');
        }
        if (isset($dto['demandante'])){
            $query->where('actuaciones.nombres_demandantes', 'like', '%'. $dto['demandante'] . '%');
        }
        if (isset($dto['demandado'])){
            $query->where('actuaciones.nombres_demandados', 'like', '%'. $dto['demandado'] . '%');
        }

        if (isset($dto['ordenar_por']) && count($dto['ordenar_por']) > 0){
            foreach ($dto['ordenar_por'] as $attribute => $value){
                if($attribute == 'radicado'){
                    $query->orderBy('actuaciones.numero_proceso', $value);
                }
                if($attribute == 'demandante'){
                    $query->orderBy('actuaciones.nombres_demandantes', $value);
                }
                if($attribute == 'demandado'){
                    $query->orderBy('actuaciones.nombres_demandados', $value);
                }
                if($attribute == 'actuacion'){
                    $query->orderBy('actuaciones.observaciones', $value);
                }
                if($attribute == 'fecha_vencimiento'){
                    $query->orderBy('actuaciones.fecha_vencimiento_termino', $value);
                }
                if($attribute == 'ultima_actualizacion'){
                    $query->orderBy('actuaciones.updated_at', $value);
                }
                if($attribute == 'actualizado_por'){
                    $query->orderBy('actuaciones.usuario_modificacion_nombre', $value);
                }
                if($attribute == 'falta'){
                    $query->orderBy('actuaciones.fecha_vencimiento_termino', $value);
                }
            }
        }else{
            $query->orderBy("actuaciones.fecha_vencimiento_termino", "desc");
        }

        $movimientos = $query->paginate($dto['limite'] ?? 100);
        return $movimientos;
    }

    /**
     * Obtener las actuaciones pendientes
     * @param $dto
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function obtenerActuacionesPendientesYCumplidas($dto)
    {
        $user = Auth::user();
        $empresa = $user->empresa();
        $dto['empresa_id'] = $empresa->id;
        $dto['fecha_actual'] = Carbon::today()->format("Y-m-d");
        $query = DB::table('actuaciones')
            ->where('actuaciones.empresa_id', '=', $dto['empresa_id'])
            ->where('actuaciones.ind_actuacion_audiencia', '=', 'AC')
            ->select(
                'actuaciones.id',
                'actuaciones.numero_proceso',
                'actuaciones.nombres_demandantes',
                'actuaciones.nombres_demandados',
                'actuaciones.anotacion',
                'actuaciones.fecha_actuacion',
                'actuaciones.fecha_vencimiento_termino',
                DB::raw("DATEDIFF(fecha_vencimiento_termino, curdate()) as falta"),
                'actuaciones.nombre_archivo_anexo',
                'actuaciones.observaciones',
                'actuaciones.usuario_creacion_id',
                'actuaciones.usuario_creacion_nombre',
                'actuaciones.usuario_modificacion_id',
                'actuaciones.usuario_modificacion_nombre',
                'actuaciones.created_at AS fecha_creacion',
                'actuaciones.updated_at AS fecha_modificacion'
            );

        if (isset($dto['cumplimiento']) && $dto['cumplimiento'] == "true"){
            $query->where('actuaciones.estado', '=', '1');
        }else if(isset($dto['cumplimiento']) && $dto['cumplimiento'] == "false"){
            $query->where('actuaciones.fecha_vencimiento_termino', '>=', $dto['fecha_actual'])
                ->whereNull('actuaciones.estado');
        }

        if (isset($dto['proceso'])){
            $query->where('actuaciones.numero_proceso', 'like', '%'. $dto['proceso'] .'%');
        }
        if (isset($dto['actuacion'])){
            $query->where('actuaciones.observaciones', 'like', '%'. $dto['actuacion'] . '%');
        }
        if (isset($dto['demandante'])){
            $query->where('actuaciones.nombres_demandantes', 'like', '%'. $dto['demandante'] . '%');
        }
        if (isset($dto['demandado'])){
            $query->where('actuaciones.nombres_demandados', 'like', '%'. $dto['demandado'] . '%');
        }

        if(
            (isset($dto['vence_hoy']) && $dto['vence_hoy'] == "true") ||
            (isset($dto['por_vencer']) && $dto['por_vencer'] == "true") ||
            (isset($dto['pendientes']) && $dto['pendientes'] == "true") ||
            (isset($dto['mas_de_diz_dias']) && $dto['mas_de_diz_dias'] == "true")
        ){
            $query->where(function ($q) use($dto) {
                if(isset($dto['vence_hoy']) && $dto['vence_hoy'] == "true"){
                    $q->orWhere('actuaciones.fecha_vencimiento_termino', $dto['fecha_actual']);
                }
                if(isset($dto['por_vencer']) && $dto['por_vencer'] == "true"){
                    $q->orWhere(function($q){
                        $q->where('actuaciones.fecha_vencimiento_termino', '>=', Carbon::today()->addDays(1)->format("Y-m-d"))
                            ->where('actuaciones.fecha_vencimiento_termino', '<=', Carbon::today()->addDays(3)->format("Y-m-d"));
                    });
                }
                if(isset($dto['pendientes']) && $dto['pendientes'] == "true"){
                    $q->orWhere(function($q){
                        $q->where('actuaciones.fecha_vencimiento_termino', '>=', Carbon::today()->addDays(4)->format("Y-m-d"))
                            ->where('actuaciones.fecha_vencimiento_termino', '<=', Carbon::today()->addDays(10)->format("Y-m-d"));
                    });
                }
                if(isset($dto['mas_de_diz_dias']) && $dto['mas_de_diz_dias'] == "true"){
                    $q->orWhere('actuaciones.fecha_vencimiento_termino', '>', Carbon::today()->addDays(10)->format("Y-m-d"));
                }
            });
        }

        if (isset($dto['ordenar_por']) && count($dto['ordenar_por']) > 0){
            foreach ($dto['ordenar_por'] as $attribute => $value){
                if($attribute == 'radicado'){
                    $query->orderBy('actuaciones.numero_proceso', $value);
                }
                if($attribute == 'demandante'){
                    $query->orderBy('actuaciones.nombres_demandantes', $value);
                }
                if($attribute == 'demandado'){
                    $query->orderBy('actuaciones.nombres_demandados', $value);
                }
                if($attribute == 'actuacion'){
                    $query->orderBy('actuaciones.observaciones', $value);
                }
                if($attribute == 'fecha_vencimiento'){
                    $query->orderBy('actuaciones.fecha_vencimiento_termino', $value);
                }
                if($attribute == 'ultima_actualizacion'){
                    $query->orderBy('actuaciones.updated_at', $value);
                }
                if($attribute == 'actualizado_por'){
                    $query->orderBy('actuaciones.usuario_modificacion_nombre', $value);
                }
                if($attribute == 'falta'){
                    $query->orderBy('actuaciones.fecha_vencimiento_termino', $value);
                }
            }
        }else{
            $query->orderBy("actuaciones.fecha_vencimiento_termino", "desc");
        }

        $movimientos = $query->paginate($dto['limite'] ?? 100);
        return $movimientos;
    }

}
