<?php

namespace App\Model\Administracion;

use Illuminate\Database\Eloquent\Model;

class NovedadProceso extends Model
{

    protected $table = 'novedad_procesos';

    protected $fillable = [
        'fecha_ejecucion',
        'fecha_proceso',
        'numero_proceso',
        'departamento',
        'ciudad',
        'despacho',
        'demandante',
        'demandado',
        'fecha_actuacion',
        'descripcion_actuacion',
        'descripcion_anotacion',
        'fecha_inicio_termino',
        'fecha_finaliza_termino',
        'fecha_registro',
        'programacion_auditoria_proceso_id'
    ];

    public $timestamps = false;

}
