<?php

namespace App\Model\Administracion;

use App\Model\Administracion\ProcesoJudicial;
use App\Model\Configuracion\Ciudad;
use App\Model\Configuracion\Departamento;
use App\Model\Configuracion\Empresa;
use App\Model\Configuracion\TipoDiligencia;
use App\Model\Seguridad\Usuario;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Diligencia extends Model
{

    protected $table = 'diligencias';

    protected $fillable = [
        'empresa_id',
        'proceso_id',
        'fecha_diligencia',
        'tipo_diligencia_id',
        'departamento_id',
        'ciudad_id',
        'dependiente_id',
        'valor_diligencia',
        'gastos_envio',
        'otros_gastos',
        'costo_diligencia',
        'costo_envio',
        'otros_costos',
        'detalle_valores',
        'indicativo_cobro',
        'solicitud_autorizacion',
        'estado_diligencia',
        'observaciones_cliente',
        'observaciones_internas',
        'url_anexo_diligencia',
        'archivo_anexo_diligencia',
        'url_anexo_costos',
        'archivo_anexo_costos',
        'url_anexo_recibido',
        'archivo_anexo_recibido',
        'usuario_creacion_id',
        'usuario_creacion_nombre',
        'usuario_modificacion_id',
        'usuario_modificacion_nombre',
    ];

    public function ciudad(){
        return $this->belongsTo(Ciudad::class);
    }
    public function departamento(){
        return $this->belongsTo(Departamento::class);
    }
    public function tipoDiligencia(){
        return $this->belongsTo(TipoDiligencia::class);
    }
    public function empresa(){
        return $this->belongsTo(Empresa::class, 'empresa_id', 'id');
    }
    public function dependiente(){
        return $this->belongsTo(Usuario::class, 'dependiente_id', 'id');
    }
    public function procesoJudicial(){
        return $this->belongsTo(ProcesoJudicial::class, 'proceso_id');
    }
    public function diligenciaHistoria(){
        return $this->hasMany(DiligenciaHistoria::class, 'diligencia_id');
    }

    public static function obtenerColeccion($dto){
        $user = Auth::user();
        $usuario = $user->usuario();
        $parametros = ParametroConstante::cargarParametros();
        $rol = $user->rol();
        $dto['usuario_id'] = $usuario->id;
        $query = DB::table('diligencias')
            ->join('ciudades', 'ciudades.id', '=', 'diligencias.ciudad_id')
            ->join('departamentos', 'departamentos.id', '=', 'ciudades.departamento_id')
            ->join('tipos_de_diligencias', 'tipos_de_diligencias.id', '=', 'diligencias.tipo_diligencia_id')
            ->join('empresas', 'empresas.id', '=', 'diligencias.empresa_id')
            ->join('procesos_judiciales', 'procesos_judiciales.id', '=', 'diligencias.proceso_id')
            ->leftJoin('usuarios', 'usuarios.id', '=', 'diligencias.dependiente_id')
            ->select(
                'diligencias.id',
                'tipos_de_diligencias.nombre As tipo_diligencia_nombre',
                'departamentos.nombre As departamento_nombre',
                'ciudades.nombre As ciudad_nombre',
                'empresas.nombre As empresa_nombre',
                'usuarios.nombre As dependiente',
                'procesos_judiciales.numero_proceso As numero_proceso',
                'diligencias.empresa_id',
                'diligencias.proceso_id',
                'diligencias.tipo_diligencia_id',
                'diligencias.fecha_diligencia',
                'diligencias.valor_diligencia',
                'diligencias.gastos_envio',
                'diligencias.otros_gastos',
                'diligencias.costo_diligencia',
                'diligencias.costo_envio',
                'diligencias.otros_costos',
                'diligencias.observaciones_cliente',
                'diligencias.observaciones_internas',
                'diligencias.indicativo_cobro',
                'diligencias.url_anexo_diligencia',
                'diligencias.archivo_anexo_diligencia',
                'diligencias.estado_diligencia',
                'diligencias.usuario_creacion_id',
                'diligencias.usuario_creacion_nombre',
                'diligencias.usuario_modificacion_id',
                'diligencias.usuario_modificacion_nombre',
                'diligencias.created_at AS fecha_creacion',
                'diligencias.updated_at AS fecha_modificacion'
            );


        if(isset($dto['numero_diligencia'])){
            $query->where('diligencias.id', '=', $dto['numero_diligencia']);
        }
        if(isset($dto['empresa_cliente'])){
            $query->where('empresas.nombre', 'like', '%' . $dto['empresa_cliente'] . '%');
        }
        if(isset($dto['numero_proceso'])){
            $query->where('procesos_judiciales.numero_proceso', 'like', '%' . $dto['numero_proceso'] . '%');
        }
        if(isset($dto['estado_diligencia'])){
            $query->where('diligencias.estado_diligencia', '=', $dto['estado_diligencia']);
        }
        if (isset($dto['fecha_inicial'])){
            $query->where('diligencias.updated_at', '>=', $dto['fecha_inicial'] . ' 00:00:00');
        }
        if (isset($dto['fecha_final'])){
            $query->where('diligencias.updated_at', '<=', $dto['fecha_final'] . ' 23:59:59');
        }
        if(isset($dto['dependiente'])){
            if(!(
                $rol->id == $parametros['SUPERUSUARIO_ROL_ID'] ||
                $rol->id == $parametros['COORDINADOR_DEPENDENCIA_ROL_ID']

            )) {
                $query->where('diligencias.dependiente_id', '=', $dto['usuario_id']);
            }
            $query->where('diligencias.estado_diligencia', '=', 4);
        }

        if (isset($dto['ordenar_por']) && count($dto['ordenar_por']) > 0){
            foreach ($dto['ordenar_por'] as $attribute => $value){
                if($attribute == 'diligencia'){
                    $query->orderBy('diligencias.id', $value);
                }
                if($attribute == 'fecha'){
                    $query->orderBy('diligencias.fecha_diligencia', $value);
                }
                if($attribute == 'empresa_cliente'){
                    $query->orderBy('empresas.nombre', $value);
                }
                if($attribute == 'proceso'){
                    $query->orderBy('procesos_judiciales.valor_diligencia', $value);
                }
                if($attribute == 'ciudad'){
                    $query->orderBy('ciudades.nombre', $value);
                }
                if($attribute == 'tipo_diligencia'){
                    $query->orderBy('tipos_de_diligencias.nombre', $value);
                }
                if($attribute == 'valor_total'){
                    $query->orderByRaw('(diligencias.valor_diligencia + diligencias.gastos_envio + diligencias.valor_diligencia) ' . $value);
                }
                if($attribute == 'estado_diligencia'){
                    $query->orderBy('diligencias.estado_diligencia', $value);
                }
                if($attribute == 'usuario_creacion_nombre'){
                    $query->orderBy('diligencias.usuario_creacion_nombre', $value);
                }
                if($attribute == 'usuario_modificacion_nombre'){
                    $query->orderBy('diligencias.usuario_modificacion_nombre', $value);
                }
                if($attribute == 'fecha_creacion'){
                    $query->orderBy('diligencias.created_at', $value);
                }
                if($attribute == 'fecha_modificacion'){
                    $query->orderBy('diligencias.updated_at', $value);
                }
            }
        }else{
            $query->orderBy('diligencias.estado_diligencia', "asc")
                ->orderBy('diligencias.id', "asc");
        }

        $diligencias = $query->paginate($dto['limite'] ?? 100);
        return $diligencias;
    }

    public static function obtenerDiligenciaPorCliente($dto){
        $user = Auth::user();
        $empresa = $user->empresa();
        $dto['empresa_id'] = $empresa->id;
        $query = DB::table('diligencias')
            ->join('ciudades', 'ciudades.id', '=', 'diligencias.ciudad_id')
            ->join('departamentos', 'departamentos.id', '=', 'ciudades.departamento_id')
            ->join('tipos_de_diligencias', 'tipos_de_diligencias.id', '=', 'diligencias.tipo_diligencia_id')
            ->join('empresas', 'empresas.id', '=', 'diligencias.empresa_id')
            ->join('procesos_judiciales', 'procesos_judiciales.id', '=', 'diligencias.proceso_id')
            ->leftJoin('usuarios', 'usuarios.id', '=', 'diligencias.dependiente_id')
            ->select(
                'diligencias.id',
                'tipos_de_diligencias.nombre As tipo_diligencia_nombre',
                'departamentos.nombre As departamento_nombre',
                'ciudades.nombre As ciudad_nombre',
                'empresas.nombre As empresa_nombre',
                'usuarios.nombre As dependiente',
                'procesos_judiciales.numero_proceso As numero_proceso',
                'diligencias.empresa_id',
                'diligencias.proceso_id',
                'diligencias.tipo_diligencia_id',
                'diligencias.fecha_diligencia',
                'diligencias.valor_diligencia',
                'diligencias.gastos_envio',
                'diligencias.otros_gastos',
                'diligencias.costo_diligencia',
                'diligencias.costo_envio',
                'diligencias.otros_costos',
                'diligencias.observaciones_cliente',
                'diligencias.observaciones_internas',
                'diligencias.indicativo_cobro',
                'diligencias.url_anexo_diligencia',
                'diligencias.archivo_anexo_diligencia',
                'diligencias.estado_diligencia',
                'diligencias.usuario_creacion_id',
                'diligencias.usuario_creacion_nombre',
                'diligencias.usuario_modificacion_id',
                'diligencias.usuario_modificacion_nombre',
                'diligencias.created_at AS fecha_creacion',
                'diligencias.updated_at AS fecha_modificacion'
            )->where('diligencias.empresa_id', '=', $dto['empresa_id'])
            ->where('diligencias.indicativo_cobro', '=', 1);

        if(isset($dto['numero_diligencia'])){
            $query->where('diligencias.id', '=', $dto['numero_diligencia']);
        }
        if(isset($dto['numero_proceso'])){
            $query->where('procesos_judiciales.numero_proceso', 'like', '%' . $dto['numero_proceso'] . '%');
        }
        if(isset($dto['estado_diligencia'])){
            $query->where('diligencias.estado_diligencia', '=', $dto['estado_diligencia']);
        }
        if (isset($dto['fecha_inicial'])){
            $query->where('diligencias.updated_at', '>=', $dto['fecha_inicial'] . ' 00:00:00');
        }
        if (isset($dto['fecha_final'])){
            $query->where('diligencias.updated_at', '<=', $dto['fecha_final'] . ' 23:59:59');
        }
        if(isset($dto['dependiente'])){
            $query->where('diligencias.dependiente_id', '=', $dto['usuario_id'])
            ->where('diligencias.estado_diligencia', '=', 4);
        }

        if (isset($dto['ordenar_por']) && count($dto['ordenar_por']) > 0){
            foreach ($dto['ordenar_por'] as $attribute => $value){
                if($attribute == 'numero_diligencia'){
                    $query->orderBy('diligencias.id', $value);
                }
                if($attribute == 'fecha'){
                    $query->orderBy('diligencias.fecha_diligencia', $value);
                }
                if($attribute == 'empresa_cliente'){
                    $query->orderBy('empresas.nombre', $value);
                }
                if($attribute == 'proceso'){
                    $query->orderBy('procesos_judiciales.valor_diligencia', $value);
                }
                if($attribute == 'ciudad'){
                    $query->orderBy('ciudades.nombre', $value);
                }
                if($attribute == 'tipo_diligencia'){
                    $query->orderBy('tipos_de_diligencias.nombre', $value);
                }
                if($attribute == 'valor_total'){
                    $query->orderByRaw('(diligencias.valor_diligencia + diligencias.gastos_envio + diligencias.valor_diligencia) ' . $value);
                }
                if($attribute == 'estado_diligencia'){
                    $query->orderBy('diligencias.estado_diligencia', $value);
                }
                if($attribute == 'usuario_creacion_nombre'){
                    $query->orderBy('diligencias.usuario_creacion_nombre', $value);
                }
                if($attribute == 'usuario_modificacion_nombre'){
                    $query->orderBy('diligencias.usuario_modificacion_nombre', $value);
                }
                if($attribute == 'fecha_creacion'){
                    $query->orderBy('diligencias.created_at', $value);
                }
                if($attribute == 'fecha_modificacion'){
                    $query->orderBy('diligencias.updated_at', $value);
                }
            }
        }else{
            $query->orderBy('diligencias.estado_diligencia', "asc")
                ->orderBy('diligencias.id', "asc");
        }

        $diligencias = $query->paginate($dto['limite'] ?? 100);
        return $diligencias;
    }

}
