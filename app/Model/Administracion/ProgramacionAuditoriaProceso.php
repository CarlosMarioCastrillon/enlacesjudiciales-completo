<?php

namespace App\Model\Administracion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ProgramacionAuditoriaProceso extends Model
{

    protected $table = 'programacion_auditoria_procesos';

    protected $fillable = [
        'fecha_programacion',
        'fecha_programacion_pasada',
        'fecha_ejecucion',
        'fecha_proceso',
        'lotes',
        'lotes_ejecutados',
        'recientes',
        'estado'
    ];

    public $timestamps = false;

    public static function obtenerColeccion($dto){
        $query = DB::table('programacion_auditoria_procesos')
            ->select(
                'id',
                'fecha_programacion',
                'fecha_programacion_pasada',
                'fecha_ejecucion',
                'fecha_proceso',
                'lotes',
                'lotes_ejecutados',
                'recientes',
                'estado'
            );

        if (isset($dto['ordenar_por']) && count($dto['ordenar_por']) > 0){
            foreach ($dto['ordenar_por'] as $attribute => $value){
                $query->orderBy($attribute, $value);
            }
        }else{
            $query->orderBy("programacion_auditoria_procesos.id", "desc");
        }

        $programaciones = $query->paginate($dto['limite'] ?? 100);
        return $programaciones;
    }

}
