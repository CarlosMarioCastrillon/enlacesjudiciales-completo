<?php

namespace App\Model\Administracion;

use App\Model\Configuracion\Area;
use App\Model\Configuracion\Ciudad;
use App\Model\Configuracion\ClaseProceso;
use App\Model\Configuracion\Departamento;
use App\Model\Configuracion\Despacho;
use App\Model\Configuracion\Empresa;
use App\Model\Configuracion\EtapaProceso;
use App\Model\Configuracion\Juzgado;
use App\Model\Configuracion\TipoActuacion;
use App\Model\Configuracion\Zona;
use App\Model\Seguridad\Usuario;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ProcesoJudicial extends Model
{
    protected $table = 'procesos_judiciales';

    protected $fillable = [
        'departamento_dane',
        'ciudad_dane',
        'juzgado_actual',
        'juzgado_origen_rama',
        'despacho_numero',
        'despacho_origen_numero',
        'anio_proceso',
        'numero_radicado',
        'numero_consecutivo',
        'numero_proceso',
        'numero_radicado_proceso_actual',
        'proceso_id_referencia',
        'instancia_judicial_actual',
        'nombres_demandantes',
        'nombres_demandados',
        'observaciones',
        'indicativo_pertenece_rama',
        'indicativo_consulta_rama',
        'indicativo_tiene_vigilancia',
        'indicativo_estado',
        'descripcion_clase_proceso',
        'empresa_id',
        'zona_id',
        'area_id',
        'cliente_id',
        'abogado_responsable_id',
        'dependiente_asignado_id',
        'ultima_clase_proceso_id',
        'ultima_etapa_proceso_id',
        'ultimo_tipo_actuacion_id',
        'departamento_id',
        'ciudad_id',
        'juzgado_id',
        'juzgado_origen_id',
        'despacho_id',
        'despacho_origen_id',
        'usuario_creacion_id',
        'usuario_creacion_nombre',
        'usuario_modificacion_id',
        'usuario_modificacion_nombre'
    ];

    public function empresa(){
        return $this->belongsTo(Empresa::class);
    }

    public function zona(){
        return $this->belongsTo(Zona::class);
    }

    public function area(){
        return $this->belongsTo(Area::class);
    }

    public function cliente(){
        return $this->belongsTo(Cliente::class);
    }

    public function abogado(){
        return $this->belongsTo(Usuario::class, 'abogado_responsable_id');
    }

    public function dependiente(){
        return $this->belongsTo(Usuario::class, 'dependiente_asignado_id');
    }

    public function departamento(){
        return $this->belongsTo(Departamento::class);
    }

    public function ciudad(){
        return $this->belongsTo(Ciudad::class);
    }

    public function juzgado(){
        return $this->belongsTo(Juzgado::class);
    }

    public function juzgadoOrigen(){
        return $this->belongsTo(Juzgado::class);
    }

    public function despacho(){
        return $this->belongsTo(Despacho::class);
    }

    public function despachoOrigen(){
        return $this->belongsTo(Despacho::class);
    }

    public function claseProceso(){
        return $this->belongsTo(ClaseProceso::class, 'ultima_clase_proceso_id','id');
    }

    public function etapaProceso(){
        return $this->belongsTo(EtapaProceso::class, 'ultima_etapa_proceso_id','id');
    }

    public function tipoActuacion(){
        return $this->belongsTo(TipoActuacion::class, 'ultimo_tipo_actuacion_id','id');
    }

    public static function findBy($numeroProceso){
        return ProcesoJudicial::where('numero_proceso', $numeroProceso)->first();
    }

    public static function obtenerColeccionLigera($dto){
        $user = Auth::user();
        $empresa = $user->empresa();
        $dto['empresa_id'] = $empresa->id;
        $query = DB::table('procesos_judiciales')
            ->select(
                'procesos_judiciales.id',
                'procesos_judiciales.numero_proceso'
            );
            if(isset($dto['ciudad_id']) && $dto['ciudad_id']){
                $query->where('procesos_judiciales.ciudad_id', $dto['ciudad_id']);
            }
            if (isset($dto['empresa_cliente'])) {
                $query->where('procesos_judiciales.empresa_id', '=', $dto['empresa_cliente']);
            }else{
                $query->where('procesos_judiciales.empresa_id', '=', $dto['empresa_id']);
            }

        if(isset($dto['q'])){
            $query->where('procesos_judiciales.numero_proceso', 'like', "%" . $dto['q'] . "%");
        }

        return $query->get($dto['limite']);
    }

    public static function obtenerColeccion($dto)
    {
        $user = Auth::user();
        $empresa = $user->empresa();
        $dto['empresa_id'] = $empresa->id;
        $query = DB::table('procesos_judiciales')
            ->join('ciudades', 'ciudades.id', '=', 'procesos_judiciales.ciudad_id')
            ->join('juzgados', 'juzgados.id', '=', 'procesos_judiciales.juzgado_id')
            ->join('despachos', 'despachos.id', '=', 'procesos_judiciales.despacho_id')
            ->join('empresas', 'empresas.id', '=', 'procesos_judiciales.empresa_id')
            ->leftjoin('clases_de_procesos', 'clases_de_procesos.id', '=', 'procesos_judiciales.ultima_clase_proceso_id')
            ->select(
                'procesos_judiciales.id',
                'ciudades.codigo_dane AS ciudad_codigo_dane',
                'ciudades.nombre AS ciudad_nombre',
                'juzgados.nombre AS juzgado_nombre',
                'despachos.nombre AS despacho_nombre',
                'despachos.despacho AS despacho_numero',
                'empresas.nombre AS empresa_nombre',
                'clases_de_procesos.nombre AS clase_proceso_nombre',
                'procesos_judiciales.numero_proceso',
                'procesos_judiciales.numero_consecutivo',
                'procesos_judiciales.juzgado_actual',
                'procesos_judiciales.nombres_demandantes',
                'procesos_judiciales.nombres_demandados',
                'procesos_judiciales.observaciones',
                'procesos_judiciales.usuario_creacion_id',
                'procesos_judiciales.usuario_creacion_nombre',
                'procesos_judiciales.usuario_modificacion_id',
                'procesos_judiciales.usuario_modificacion_nombre',
                'procesos_judiciales.created_at AS fecha_creacion',
                'procesos_judiciales.updated_at AS fecha_modificacion'
            )->where('procesos_judiciales.empresa_id', '=', $dto['empresa_id'])
        ;

        if (isset($dto['vigilancia'])){
            $query->where('procesos_judiciales.indicativo_tiene_vigilancia', '=', 'V');
        }
        if (isset($dto['estado'])){
            $query->where('procesos_judiciales.indicativo_estado', '=', $dto['estado']);
        }
        if (isset($dto['area'])){
            $query->where('procesos_judiciales.area_id', '=', $dto['area']);
        }
        if (isset($dto['numero_proceso'])){
            $query->where('procesos_judiciales.numero_proceso', 'like','%'. $dto['numero_proceso'] .'%');
        }
        if (isset($dto['demandante'])){
            $query->where('procesos_judiciales.nombres_demandantes', 'like','%'. $dto['demandante'] .'%');
        }
        if (isset($dto['demandado'])){
            $query->where('procesos_judiciales.nombres_demandados', 'like','%'. $dto['demandado'] .'%');
        }

        if (isset($dto['ordenar_por']) && count($dto['ordenar_por']) > 0){
            foreach ($dto['ordenar_por'] as $attribute => $value){
                if($attribute == 'numero_proceso'){
                    $query->orderBy('procesos_judiciales.numero_proceso', $value);
                }
                if($attribute == 'ciudad_codigo_dane'){
                    $query->orderBy('ciudades.codigo_dane', $value);
                }
                if($attribute == 'ciudad'){
                    $query->orderBy('ciudades.nombre', $value);
                }
            }
        }else{
            $query->orderBy("procesos_judiciales.id", "desc");
        }

        $procesosJudiciales = $query->paginate($dto['limite'] ?? 100);
        return $procesosJudiciales;
    }

    public static function obtenerProcesosPorRol($dto)
    {
        // Constantes
        $parametros = ParametroConstante::cargarParametros();

        $user = Auth::user();
        $rol = $user->rol();
        $empresa = $user->empresa();
        $dto['zona_id'] = $empresa->zona_id;
        $query = DB::table('procesos_judiciales')
            ->join('ciudades', 'ciudades.id', '=', 'procesos_judiciales.ciudad_id')
            ->join('juzgados', 'juzgados.id', '=', 'procesos_judiciales.juzgado_id')
            ->join('despachos', 'despachos.id', '=', 'procesos_judiciales.despacho_id')
            ->join('clases_de_procesos', 'clases_de_procesos.id', '=', 'procesos_judiciales.ultima_clase_proceso_id')
            ->join('empresas', 'empresas.id', '=', 'procesos_judiciales.empresa_id')

            ->select(
                'procesos_judiciales.id',
                'ciudades.codigo_dane AS ciudad_codigo_dane',
                'ciudades.nombre AS ciudad_nombre',
                'juzgados.nombre AS juzgado_nombre',
                'despachos.nombre AS despacho_nombre',
                'despachos.despacho AS despacho_numero',
                'empresas.nombre AS empresa_nombre',
                'clases_de_procesos.nombre AS clase_proceso_nombre',
                'procesos_judiciales.numero_proceso',
                'procesos_judiciales.numero_consecutivo',
                'procesos_judiciales.juzgado_actual',
                'procesos_judiciales.nombres_demandantes',
                'procesos_judiciales.nombres_demandados',
                'procesos_judiciales.observaciones',
                'procesos_judiciales.usuario_creacion_id',
                'procesos_judiciales.usuario_creacion_nombre',
                'procesos_judiciales.usuario_modificacion_id',
                'procesos_judiciales.usuario_modificacion_nombre',
                'procesos_judiciales.created_at AS fecha_creacion',
                'procesos_judiciales.updated_at AS fecha_modificacion'
            )
        ;
        if(
            $rol->id != ($parametros['SUPERUSUARIO_ROL_ID'] ?? null) &&
            $rol->id == ($parametros['ADMINISTRADOR_ZONA_ROL_ID'] ?? null)
        ) {
            $query->where('empresas.zona_id', '=', $dto['zona_id']);
        }
        if (isset($dto['empresa'])){
            $query->where('procesos_judiciales.empresa_id', '=', $dto['empresa']);
        }
        if (isset($dto['numero_proceso'])){
            $query->where('procesos_judiciales.numero_proceso', 'like','%'. $dto['numero_proceso'] .'%');
        }
        if (isset($dto['demandante'])){
            $query->where('procesos_judiciales.nombres_demandantes', 'like','%'. $dto['demandante'] .'%');
        }
        if (isset($dto['demandado'])){
            $query->where('procesos_judiciales.nombres_demandados', 'like','%'. $dto['demandado'] .'%');
        }

        if (isset($dto['ordenar_por']) && count($dto['ordenar_por']) > 0){
            foreach ($dto['ordenar_por'] as $attribute => $value){
                if($attribute == 'empresa'){
                    $query->orderBy('empresas.nombre', $value);
                }
                if($attribute == 'ciudad'){
                    $query->orderBy('ciudades.nombre', $value);
                }
            }
        }else{
            $query->orderBy("procesos_judiciales.id", "desc");
        }

        $procesosJudiciales = $query->paginate($dto['limite'] ?? 100);
        return $procesosJudiciales;
    }

}
