<?php

namespace App\Model\Administracion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ParametroConstante extends Model
{

    protected $table = 'parametros_constantes';

    protected $fillable = [
        'codigo_parametro',
        'descripcion_parametro',
        'valor_parametro',
        'estado_registro',
        'usuario_creacion_id',
        'usuario_creacion_nombre',
        'usuario_modificacion_id',
        'usuario_modificacion_nombre'
    ];

    public static function obtenerColeccion($dto){
        $query = DB::table('parametros_constantes')
            ->select(
                'parametros_constantes.id',
                'parametros_constantes.codigo_parametro',
                'parametros_constantes.descripcion_parametro',
                'parametros_constantes.valor_parametro',
                'parametros_constantes.estado_registro',
                'parametros_constantes.usuario_creacion_id',
                'parametros_constantes.usuario_creacion_nombre',
                'parametros_constantes.usuario_modificacion_id',
                'parametros_constantes.usuario_modificacion_nombre',
                'parametros_constantes.created_at AS fecha_creacion',
                'parametros_constantes.updated_at AS fecha_modificacion'
            );

        if(isset($dto['codigo'])){
            $query->where('parametros_constantes.codigo_parametro', 'like', '%' . $dto['codigo'] . '%');
        }
        if(isset($dto['descripcion'])){
            $query->where('parametros_constantes.descripcion_parametro', 'like', '%' . $dto['descripcion'] . '%');
        }
        if(isset($dto['valor'])){
            $query->where('ciudades.valor_parametro', 'like', '%' . $dto['valor'] . '%');
        }
        if(isset($dto['estado'])){
            $query->where('ciudades.estado_registro', '=', $dto['estado']);
        }

        if (isset($dto['ordenar_por']) && count($dto['ordenar_por']) > 0){
            foreach ($dto['ordenar_por'] as $attribute => $value){
                if($attribute == 'codigo'){
                    $query->orderBy('parametros_constantes.codigo_parametro', $value);
                }
                if($attribute == 'descripcion'){
                    $query->orderBy('tipos_de_documentos.descripcion_parametro', $value);
                }
                if($attribute == 'valor'){
                    $query->orderBy('parametros_constantes.valor_parametro', $value);
                }
                if($attribute == 'estado'){
                    $query->orderBy('ciudades.estado_registro', $value);
                }
                if($attribute == 'usuario_creacion_nombre'){
                    $query->orderBy('parametros_constantes.usuario_creacion_nombre', $value);
                }
                if($attribute == 'usuario_modificacion_nombre'){
                    $query->orderBy('parametros_constantes.usuario_modificacion_nombre', $value);
                }
                if($attribute == 'fecha_creacion'){
                    $query->orderBy('parametros_constantes.created_at', $value);
                }
                if($attribute == 'fecha_modificacion'){
                    $query->orderBy('parametros_constantes.updated_at', $value);
                }
            }
        }else{
            $query->orderBy("parametros_constantes.id", "desc");
        }

        $parametros_constantes = $query->paginate($dto['limite'] ?? 100);
        return $parametros_constantes;
    }

    /**
     * Cargar los parametros del sistema
     * @return array
     */
    public static function cargarParametros(){
        $parametros = [];
        $variables = ParametroConstante::get();
        foreach ($variables ?? [] as $variable){
            $parametros[$variable->codigo_parametro] = $variable->valor_parametro;
        }

        return $parametros;
    }

}
