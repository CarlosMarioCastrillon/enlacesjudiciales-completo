<?php

namespace App\Model\Administracion;

use App\Model\Configuracion\Ciudad;
use App\Model\Configuracion\Empresa;
use App\Model\Configuracion\Juzgado;
use App\Model\Administracion\ProcesoJudicial;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ProcesoCliente extends Model
{

    protected $table = 'procesos_clientes';

    protected $fillable = [
        'proceso_id',
        'empresa_id',
        'usuario_id',
        'estado',
        'usuario_creacion_id',
        'usuario_creacion_nombre',
        'usuario_modificacion_id',
        'usuario_modificacion_nombre',
    ];

    public function empresa(){
        return $this->belongsTo(Empresa::class);
    }

    public function cliente(){
        return $this->belongsTo(Cliente::class);
    }
    public function ciudad(){
        return $this->belongsTo(Ciudad::class);
    }
    public function juzgado(){
        return $this->belongsTo(Juzgado::class, 'procesos_judiciales.juzgado_id');
    }
    public function procesoJudicial(){
        return $this->belongsTo(ProcesoJudicial::class, 'proceso_id');
    }

    public static function obtenerColeccion($dto)
    {
        $user = Auth::user();
        $empresa = $user->empresa();
        $dto['empresa_id'] = $empresa->id;
        $rol = $user->rol();
        $query = DB::table('procesos_clientes')
            ->join('procesos_judiciales', 'procesos_judiciales.id', '=', 'procesos_clientes.proceso_id')
            ->join('ciudades', 'ciudades.id', '=', 'procesos_judiciales.ciudad_id')
            ->join('despachos', 'despachos.id', '=', 'procesos_judiciales.despacho_id')

            ->select(
                'procesos_clientes.id',
                'ciudades.nombre AS ciudad_nombre',
                'procesos_judiciales.numero_consecutivo AS consecutivo',
                'procesos_judiciales.numero_proceso AS numero_proceso',
                'despachos.nombre AS despacho_nombre',
                'procesos_judiciales.nombres_demandantes AS demandantes',
                'procesos_judiciales.nombres_demandados AS demandados',
                'procesos_clientes.estado',
                'procesos_clientes.usuario_creacion_id',
                'procesos_clientes.usuario_creacion_nombre',
                'procesos_clientes.usuario_modificacion_id',
                'procesos_clientes.usuario_modificacion_nombre',
                'procesos_clientes.created_at AS fecha_creacion',
                'procesos_clientes.updated_at AS fecha_modificacion'
            )
            ->where('procesos_clientes.usuario_id', '=', $dto['cliente_id']);

        if(!($rol->id == env('SUPERUSUARIO_ROL_ID') || $rol->id == env('ADMINISTRADOR_ZONA_ROL_ID'))){
            $query->where('procesos_clientes.empresa_id', '=', $dto['empresa_id']);
        }

        if (isset($dto['ordenar_por']) && count($dto['ordenar_por']) > 0){
            foreach ($dto['ordenar_por'] as $attribute => $value){
                if($attribute == 'numero_proceso'){
                    $query->orderBy('procesos_clientes.numero_proceso', $value);
                }
                if($attribute == 'ciudad'){
                    $query->orderBy('ciudades.nombre', $value);
                }
                if($attribute == 'consecutivo'){
                    $query->orderBy('procesos_judiciales.numero_consecutivo', $value);
                }
                if($attribute == 'despacho'){
                    $query->orderBy('despachos.nombre', $value);
                }
                if($attribute == 'demandante'){
                    $query->orderBy('procesos_judiciales.nombres_demandantes', $value);
                }
                if($attribute == 'demandado'){
                    $query->orderBy('procesos_judiciales.nombres_demandados', $value);
                }
            }
        }else{
            $query->orderBy("procesos_clientes.id", "desc");
        }

        $procesosclientes = $query->paginate($dto['limite'] ?? 100);
        return $procesosclientes;
    }
}
