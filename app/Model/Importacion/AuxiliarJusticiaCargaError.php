<?php

namespace App\Model\Importacion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AuxiliarJusticiaCargaError extends Model
{

    protected $table = 'auxiliar_justicia_carga_errores';

    protected $fillable = [
        'numero_documento',
        'nombres_auxiliar',
        'apellidos_auxiliar',
        'nombre_ciudad',
        'direccion_oficina',
        'telefono_oficina',
        'cargo',
        'telefono_celular',
        'direccion_residencia',
        'telefono_residencia',
        'correo_electronico',
        'observacion',
        'empresa_id',
        'usuario_creacion_id',
        'usuario_creacion_nombre',
        'usuario_modificacion_id',
        'usuario_modificacion_nombre',
    ];

    public static function obtenerColeccion($dto){
        $user = Auth::user();
        $empresa = $user->empresa();
        $dto['empresa_id'] = $empresa->id;
        $query = DB::table('auxiliar_justicia_carga_errores')
            ->select(
                'auxiliar_justicia_carga_errores.id',
                'auxiliar_justicia_carga_errores.lote',
                'auxiliar_justicia_carga_errores.numero_documento',
                'auxiliar_justicia_carga_errores.nombres_auxiliar',
                'auxiliar_justicia_carga_errores.apellidos_auxiliar',
                'auxiliar_justicia_carga_errores.nombre_ciudad',
                'auxiliar_justicia_carga_errores.direccion_oficina',
                'auxiliar_justicia_carga_errores.telefono_oficina',
                'auxiliar_justicia_carga_errores.cargo',
                'auxiliar_justicia_carga_errores.telefono_celular',
                'auxiliar_justicia_carga_errores.direccion_residencia',
                'auxiliar_justicia_carga_errores.telefono_residencia',
                'auxiliar_justicia_carga_errores.correo_electronico',
                'auxiliar_justicia_carga_errores.observacion',
                'auxiliar_justicia_carga_errores.empresa_id'
            )->where('auxiliar_justicia_carga_errores.empresa_id', '=',  $dto['empresa_id']);

        if(isset($dto['lote'])){
            $query->where('auxiliar_justicia_carga_errores.lote', $dto['lote']);
        }
        /*if(isset($dto['numero_documento'])){
            $query->where('auxiliar_justicia_carga_errores.numero_documento', 'like', '%' . $dto['numero_documento'] . '%');
        }
        if(isset($dto['nombres_auxiliar'])){
            $query->where('auxiliar_justicia_carga_errores.nombres_auxiliar', 'like', '%' . $dto['nombres_auxiliar'] . '%');
        }
        if(isset($dto['apellidos_auxiliar'])){
            $query->where('auxiliar_justicia_carga_errores.apellidos_auxiliar', 'like', '%' . $dto['apellidos_auxiliar'] . '%');
        }
        if(isset($dto['nombre_ciudad'])){
            $query->where('auxiliar_justicia_carga_errores.nombre_ciudad', 'like', '%' . $dto['nombre_ciudad'] . '%');
        }
        if(isset($dto['observacion'])){
            $query->where('auxiliar_justicia_carga_errores.observacion', 'like', '%' . $dto['estado'] . '%');
        }*/

        if (isset($dto['ordenar_por']) && count($dto['ordenar_por']) > 0){
            foreach ($dto['ordenar_por'] as $attribute => $value){
                if($attribute == 'numero_documento'){
                    $query->orderBy('auxiliar_justicia_carga_errores.numero_documento', $value);
                }
                if($attribute == 'nombres_auxiliar'){
                    $query->orderBy('auxiliar_justicia_carga_errores.nombres_auxiliar', $value);
                }
                if($attribute == 'apellidos_auxiliar'){
                    $query->orderBy('auxiliar_justicia_carga_errores.apellidos_auxiliar', $value);
                }
                if($attribute == 'nombre_ciudad'){
                    $query->orderBy('auxiliar_justicia_carga_errores.nombre_ciudad', $value);
                }
                if($attribute == 'direccion_oficina'){
                    $query->orderBy('auxiliar_justicia_carga_errores.direccion_oficina', $value);
                }
                if($attribute == 'telefono_oficina'){
                    $query->orderBy('auxiliar_justicia_carga_errores.telefono_oficina', $value);
                }
                if($attribute == 'cargo'){
                    $query->orderBy('auxiliar_justicia_carga_errores.cargo', $value);
                }
                if($attribute == 'telefono_celular'){
                    $query->orderBy('auxiliar_justicia_carga_errores.telefono_celular', $value);
                }
                if($attribute == 'direccion_residencia'){
                    $query->orderBy('auxiliar_justicia_carga_errores.direccion_oficina', $value);
                }
                if($attribute == 'telefono_residencia'){
                    $query->orderBy('auxiliar_justicia_carga_errores.telefono_residencia', $value);
                }
                if($attribute == 'correo_electronico'){
                    $query->orderBy('auxiliar_justicia_carga_errores.correo_electronico', $value);
                }
                if($attribute == 'observacion'){
                    $query->orderBy('auxiliar_justicia_carga_errores.observacion', $value);
                }
            }
        }else{
            $query->orderBy("auxiliar_justicia_carga_errores.id", "desc");
        }

        $auxiliar_justicia_carga_errores = $query->paginate($dto['limite'] ?? 100);
        return $auxiliar_justicia_carga_errores;
    }

}
