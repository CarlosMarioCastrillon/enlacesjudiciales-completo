<?php

namespace App\Model\Importacion;

use Illuminate\Database\Eloquent\Model;

class ProcesoJudicialCargaError extends Model
{

    protected $table = 'proceso_judicial_carga_errores';

    protected $fillable = [
        'numero_proceso',
        'nombres_demandantes',
        'nombres_demandados',
        'ciudad_nombre',
        'juzgado_nombre',
        'juzgado_numero',
        'clase_proceso_nombre',
        'responsable_documento',
        'area_nombre',
        'observacion',
        'empresa_id',
        'usuario_creacion_id',
        'usuario_creacion_nombre',
        'usuario_modificacion_id',
        'usuario_modificacion_nombre'
    ];

}
