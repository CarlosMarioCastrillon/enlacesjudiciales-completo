<?php

namespace App\Model\Importacion;

use Illuminate\Database\Eloquent\Model;

class ActuacionCargaError extends Model
{

    protected $table = 'actuacion_carga_errores';

    protected $fillable = [
        'numero_proceso',
        'descripcion_actuacion',
        'descripcion_anotacion',
        'fecha_registro',
        'fecha_actuacion',
        'fecha_inicio_termino',
        'fecha_vencimiento_termino',
        'descripcion_clase_proceso',
        'nombres_demandantes',
        'nombres_demandados',
        'ciudad_nombre',
        'juzgado_nombre',
        'despacho_nombre',
        'observacion',
        'usuario_creacion_id',
        'usuario_creacion_nombre',
        'usuario_modificacion_id',
        'usuario_modificacion_nombre'
    ];

}
