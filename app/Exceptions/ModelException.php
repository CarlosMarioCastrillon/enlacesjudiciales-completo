<?php


namespace App\Exceptions;


use Exception;

class ModelException extends Exception
{
    protected $model;

    public function __construct($message, $model = null, $code = 0, Exception $previous = null) {
        $this->model = $model;
        parent::__construct($message, $code, $previous);
    }

    public function getModel(){
        return $this->model;
    }

    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}
