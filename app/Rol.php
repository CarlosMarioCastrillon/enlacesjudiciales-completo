<?php

namespace App;

use App\Enum\TipoRolEnum;
use App\Model\Administracion\ParametroConstante;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Rol extends Model
{
    protected $table = 'roles';

    protected $fillable = [
        'id',
        'name',
        'type',
        'status',
        'guard_name',
        'creation_user_id',
        'creation_user_name',
        'modification_user_id',
        'modification_user_name'
    ];

    public static function obtenerColeccion($dto){
        $query = DB::table('roles')
            ->select(
                'roles.id AS id',
                'roles.name AS nombre',
                'roles.type AS tipo',
                'roles.status AS estado',
                'roles.creation_user_id AS usuario_creacion_id',
                'roles.creation_user_name AS usuario_creacion_nombre',
                'roles.modification_user_id AS usuario_modificacion_id',
                'roles.modification_user_name AS usuario_modificacion_nombre',
                'roles.created_at AS fecha_creacion',
                'roles.updated_at AS fecha_modificacion'
            );

        if(isset($dto['nombre'])){
            $query->where('roles.name', 'like', '%' . $dto['nombre'] . '%');
        }

        if (isset($dto['ordernar_por']) && count($dto['ordernar_por']) > 0){
            foreach ($dto['ordernar_por'] as $attribute => $value){
                if($attribute == 'nombre'){
                    $query->orderBy('roles.name', $value);
                }
                if($attribute == 'tipo'){
                    $query->orderBy('roles.type', $value);
                }
                if($attribute == 'estado'){
                    $query->orderBy('roles.status', $value);
                }
                if($attribute == 'usuario_creacion_nombre'){
                    $query->orderBy('roles.creation_user_name', $value);
                }
                if($attribute == 'usuario_modificacion_nombre'){
                    $query->orderBy('roles.modification_user_name', $value);
                }
                if($attribute == 'fecha_creacion'){
                    $query->orderBy('roles.created_at', $value);
                }
                if($attribute == 'fecha_modificacion'){
                    $query->orderBy('roles.updated_at', $value);
                }
            }
        }else{
            $query->orderBy("roles.name", "asc");
        }

        $roles = $query->paginate($dto['limite'] ?? 100);
        return $roles;
    }

    public static function obtenerColeccionLigera($dto){
        // Constantes
        $parametros = ParametroConstante::cargarParametros();

        $user = Auth::user();
        $rol = $user->rol();

        if(!(
            $rol->id == ($parametros['SUPERUSUARIO_ROL_ID'] ?? null) ||
            $rol->id == ($parametros['ADMINISTRADOR_ZONA_ROL_ID'] ?? null)
        )){
            $dto['tipo'] = TipoRolEnum::CLIENTE;
        }

        if($rol->id == ($parametros['ADMINISTRADOR_ZONA_ROL_ID'] ?? null)){
            $dto['rol_id_excluido'] = $parametros['SUPERUSUARIO_ROL_ID'] ?? null;
        }

        $query = DB::table('roles')
            ->select(
                'id',
                'name AS nombre'
            );

        if(isset($dto['rol_id_excluido'])){
            $query->whereNotIn('id', '<>', $dto['rol_id_excluido']);
        }

        if (isset($dto['cliente'])){
            $query->where('type', 3);
        }

 /*       if(isset($dto['tipo'])){
            $query->where('type', $dto['tipo']);
        }*/

        return $query
            ->orderBy('name', 'asc')
            ->get();
    }

    public static function obtenerPermisos($id, $dto){
        $query = DB::table('permissions')
            ->join('opciones_del_sistema AS modules', 'modules.id', '=', 'permissions.module')
            ->leftjoin('opciones_del_sistema AS submodules', 'submodules.id', '=', 'permissions.submodule')
            ->leftjoin('role_has_permissions', function ($join) use($id) {
                $join->on('role_has_permissions.permission_id', '=', 'permissions.id')
                    ->where('role_has_permissions.role_id', $id);
            })
            ->select(
                'permissions.id',
                'permissions.name AS nombre',
                'permissions.title AS titulo',
                'modules.nombre AS modulo',
                'submodules.nombre AS submodulo',
                DB::raw('IF(role_has_permissions.permission_id IS NOT NULL, 1, 0) AS permitido')
            )
            ->orderBy('modules.posicion', 'asc')
            ->orderBy('submodules.posicion', 'asc');

        if(isset($dto['modulo_id'])){
            $query->where('permissions.module', $dto['modulo_id']);
        }

        if(isset($dto['submodulo_id'])){
            $query->where('permissions.submodule', $dto['submodulo_id']);
        }

        return $query->get();
    }

}
