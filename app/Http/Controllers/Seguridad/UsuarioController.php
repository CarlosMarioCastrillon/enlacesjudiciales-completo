<?php

namespace App\Http\Controllers\Seguridad;

use App\Contracts\Seguridad\UsuarioService;
use App\Exceptions\ModelException;
use App\Model\Administracion\ParametroConstante;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class UsuarioController extends Controller
{

    protected  $usuarioService;

    public function __construct(UsuarioService $usuarioService){
        $this->usuarioService = $usuarioService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try{
            $datos = $request->all();
            if(!($request->ligera) && !($request->simple)) {
                $validator = Validator::make($datos, [
                    'limite' => 'integer|between:1,500'
                ]);

                if ($validator->fails()) {
                    return response(
                        get_response_body(format_messages_validator($validator))
                        , Response::HTTP_BAD_REQUEST
                    );
                }
            }
            if($request->ligera){
                $usuarios = $this->usuarioService->obtenerColeccionLigera($datos);
            }elseif ($request->simple) {
                $usuarios = $this->usuarioService->obtener($datos);
            }else{
                $user = Auth::user();
                if(!$user->can('ListarUsuario') && !$user->can('ListarUsuarioCliente')){
                    return response(null, Response::HTTP_FORBIDDEN);
                }

                if (isset($datos['ordenar_por'])) {
                    $datos['ordenar_por'] = format_order_by_attributes($datos);
                }
                $usuarios = $this->usuarioService->obtenerColeccion($datos);
            }
            return response($usuarios, Response::HTTP_OK);
        }catch(Exception $e){
            return response(get_response_body([$e->getMessage()]), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Constantes
        $parametros = ParametroConstante::cargarParametros();

        // Sesión
        $user = Auth::user();
        $rol = $user->rol();

        return view('usuario.form', [
            'rol' => $rol,
            'SUPERUSUARIO_ROL_ID' => $parametros['SUPERUSUARIO_ROL_ID'] ?? null,
            'ADMINISTRADOR_ZONA_ROL_ID' => $parametros['ADMINISTRADOR_ZONA_ROL_ID'] ?? null
        ]);
    }

    /**
     * Obtener el template para resumen de usuarios.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndexTemplate()
    {
        return view('usuario.index');
    }

    /**
     * Obtener el template para resumen de usuarios de clientes.
     *
     * @return \Illuminate\Http\Response
     */
    public function getClienteTemplate()
    {
        return view('usuario.cliente');
    }

    /**
     * Obtener el template para resumen de usuarios.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCambioClaveTemplate()
    {
        return view('usuario.cambio-clave');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction(); // Se abre la transacción
        try {
            $datos = $request->all();
            $validator = Validator::make($datos, [
                'nombre' => 'string|required|min:3|max:191',
                'email_uno' => 'unique:usuarios,email_uno',
                'estado' => 'boolean|required'
            ]);

            if ($validator->fails()) {
                return response(
                    get_response_body(format_messages_validator($validator))
                    , Response::HTTP_BAD_REQUEST
                );
            }

            $usuario = $this->usuarioService->modificarOCrear($datos);
            if ($usuario) {
                DB::commit(); // Se cierra la transacción correctamente
                return response(
                    get_response_body(["El usuario ha sido creado."], $usuario),
                    Response::HTTP_CREATED
                );
            } else {
                DB::rollback(); // Se devuelven los cambios, por que la transacción falla
                return response(get_response_body(["Ocurrió un error al intentar crear el usuario."]), Response::HTTP_CONFLICT);
            }
        }catch (ModelException $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(get_response_body([$e->getMessage()]), Response::HTTP_CONFLICT);
        }catch (Exception $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(get_response_body([$e->getMessage()]), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $datos['id'] = $id;
            $validator = Validator::make($datos, [
                'id' => 'integer|required|exists:usuarios,id'
            ]);

            if($validator->fails()) {
                return response(
                    get_response_body(format_messages_validator($validator))
                    , Response::HTTP_BAD_REQUEST
                );
            }

            return response($this->usuarioService->cargar($id), Response::HTTP_OK);
        }catch (Exception $e){
            return response($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction(); // Se abre la transacción
        try{
            $datos = $request->all();
            $datos['id'] = $id;
            if(!$request->cambio_clave){
                $validations = [
                    'nombre' => 'string|required|min:3|max:191',
                    'email_uno' => 'unique:usuarios,email_uno,'.$id,
                    'estado' => 'boolean|required'
                ];
            }
            $validations['id'] = 'integer|required|exists:usuarios,id';

            $validator = Validator::make($datos, $validations);
            if($validator->fails()) {
                return response(
                    get_response_body(format_messages_validator($validator))
                    , Response::HTTP_BAD_REQUEST
                );
            }

            if($request->cambio_clave){
                $this->usuarioService->cambiarClave($id, $datos);
                DB::commit(); // Se cierra la transacción correctamente
                return response(
                    get_response_body(["La contraseña ha sido modificada."]),
                    Response::HTTP_OK
                );
            }else{
                $usuario = $this->usuarioService->modificarOCrear($datos);
                if($usuario){
                    DB::commit(); // Se cierra la transacción correctamente
                    return response(
                        get_response_body(["El usuario ha sido modifcado."], $usuario),
                        Response::HTTP_OK
                    );
                } else {
                    DB::rollback(); // Se devuelven los cambios, por que la transacción falla
                    return response(get_response_body(["Ocurrió un error al intentar modificar el usuario."]), Response::HTTP_CONFLICT);;
                }
            }
        }catch (ModelException $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(get_response_body([$e->getMessage()]), Response::HTTP_CONFLICT);
        }catch (Exception $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(get_response_body([$e->getMessage()]), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction(); // Se abre la transacción
        try{
            $datos['id'] = $id;
            $validator = Validator::make($datos, [
                'id' => 'integer|required|exists:roles,id'
            ]);

            if($validator->fails()) {
                return response(
                    get_response_body(format_messages_validator($validator))
                    , Response::HTTP_BAD_REQUEST
                );
            }

            $eliminado = $this->usuarioService->eliminar($id);
            if($eliminado){
                DB::commit(); // Se cierra la transacción correctamente
                return response(
                    get_response_body(["El usuario ha sido eliminado."]),
                    Response::HTTP_OK
                );
            }else{
                DB::rollback(); // Se devuelven los cambios, por que la transacción falla
                return response(get_response_body(["Ocurrió un error al intentar eliminar el usuario."]), Response::HTTP_CONFLICT);
            }
        }catch (Exception $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Obtner la dependiente principal
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function obtenerDependientePrincipal(Request $request)
    {
        try{
            $datos = $request->all();
            $dependientePrincipal = $this->usuarioService->obtenerDependientePrincipal($datos);
            return response($dependientePrincipal, Response::HTTP_OK);
        }catch(Exception $e){
            return response(get_response_body([$e->getMessage()]), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

}
