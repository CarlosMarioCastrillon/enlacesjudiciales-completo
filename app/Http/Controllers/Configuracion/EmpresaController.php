<?php

namespace App\Http\Controllers\Configuracion;

use App\Contracts\Configuracion\EmpresaService;
use App\Exceptions\ModelException;
use App\Exports\EmpresaExport;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class EmpresaController extends Controller
{
    protected $empresaService;

    public function __construct(EmpresaService $empresaService){
        $this->empresaService = $empresaService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try{
            $datos = $request->all();
            if(!$request->ligera) {
                $validator = Validator::make($datos, [
                    'limite' => 'integer|between:1,500'
                ]);

                if ($validator->fails()) {
                    return response(
                        get_response_body(format_messages_validator($validator))
                        , Response::HTTP_BAD_REQUEST
                    );
                }
            }

            if($request->ligera){
                $empresas = $this->empresaService->obtenerColeccionLigera($datos);
            }else {
                if (isset($datos['ordenar_por'])) {
                    $datos['ordenar_por'] = format_order_by_attributes($datos);
                }
                $empresas = $this->empresaService->obtenerColeccion($datos);
            }
            return response($empresas, Response::HTTP_OK);
        }catch(Exception $e){
            return response(get_response_body([$e->getMessage()]), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Obtener archivo excel
     * @return Response|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function getExcel(Request $request){
        $dto = [
                'nombre' => $request->nombre,
                'ciudad_nombre' => $request->ciudad_nombre,
                'zona_nombre' => $request->zona_nombre,
                'numero_documento' => $request->numero_documento,
                'estado' => $request->estado,
        ];
        $excel = $this->empresaService->getExcel($dto);
        return $excel->download('empresas' . time() . '.xlsx');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('empresa.form');
    }

    /**
     * Obtener el template para la pantalla de resumen
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndexTemplate()
    {
        return view('empresa.index');
    }

    /**
     * Obtener el template para la pantalla de resumen
     *
     * @return \Illuminate\Http\Response
     */
    public function getInfoTemplate()
    {
        return view('empresa.info-empresa');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction(); // Se abre la transacción
        try{
            $datos = $request->all();
            $validator = Validator::make($datos, [
                'nombre' => 'string|required|min:4|max:191',
                'numero_documento' => 'string|required|min:2|max:20',
                'departamento_id' => 'integer|required',
                'tipo_documento_id' => 'integer|required',
                'tipo_empresa_id' => 'integer|required',
                'ciudad_id' => 'integer|required',
                'zona_id' => 'integer|required',
                'direccion' => 'string|required|min:4|max:191',
                'telefono_uno' => 'string|nullable|min:4|max:191',
                'telefono_dos' => 'string|nullable|min:10|max:191',
                'telefono_tres' => 'string|nullable|min:4|max:191',
                'email' => 'string|nullable|email',
                'actuacion_digital' => 'boolean|nullable',
                'vigilancia_procesos' => 'boolean|nullable',
                'dataprocesos' => 'boolean|nullable',
                'fecha_vencimiento' => 'required|date|after:today',
                'estado' => 'boolean|nullable'
            ]);

            if($validator->fails()) {
                return response(
                    get_response_body(format_messages_validator($validator))
                    , Response::HTTP_BAD_REQUEST
                );
            }

            $empresa = $this->empresaService->modificarOCrear($datos);
            if(isset($empresa)){
                DB::commit(); // Se cierra la transacción correctamente
                return response(
                    get_response_body(["La empresa ha sido creada."], $empresa),
                    Response::HTTP_CREATED
                );
            } else {
                DB::rollback(); // Se devuelven los cambios, por que la transacción falla
                return response(get_response_body(["Ocurrió un error al intentar crear la empresa."]), Response::HTTP_CONFLICT);
            }
        }catch (ModelException $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(null, Response::HTTP_CONFLICT);
        }catch (Exception $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(get_response_body([$e->getMessage()]), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $datos['id'] = $id;
            $validator = Validator::make($datos, [
                'id' => 'integer|required|exists:empresas,id'
            ]);

            if($validator->fails()) {
                return response(
                    get_response_body(format_messages_validator($validator))
                    , Response::HTTP_BAD_REQUEST
                );
            }

            return response($this->empresaService->cargar($id), Response::HTTP_OK);
        }catch (Exception $e){
            return response(get_response_body([$e->getMessage()]), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction(); // Se abre la transacción
        try{
            $datos = $request->all();
            $datos['id'] = $id;
            $validator = Validator::make($datos, [
                'id' => 'integer|required|exists:empresas,id',
                'nombre' => 'string|required|min:4|max:191',
                'numero_documento' => 'string|required|min:2|max:20',
                'departamento_id' => 'integer|required',
                'tipo_documento_id' => 'integer|required',
                'tipo_empresa_id' => 'integer|required',
                'ciudad_id' => 'integer|required',
                'zona_id' => 'integer|required',
                'direccion' => 'string|required|min:4|max:191',
                'telefono_uno' => 'string|nullable|min:4|max:191',
                'telefono_dos' => 'string|nullable|min:10|max:191',
                'telefono_tres' => 'string|nullable|min:4|max:191',
                'email' => 'string|nullable|email',
                'actuacion_digital' => 'boolean|nullable',
                'vigilancia_procesos' => 'boolean|nullable',
                'dataprocesos' => 'boolean|nullable',
                'fecha_vencimiento' => 'required|date|after:today',
                'estado' => 'boolean|nullable'
            ]);

            if($validator->fails()) {
                return response(
                    get_response_body(format_messages_validator($validator))
                    , Response::HTTP_BAD_REQUEST
                );
            }

            $empresa = $this->empresaService->modificarOCrear($datos);
            if(isset($empresa)){
                DB::commit(); // Se cierra la transacción correctamente
                return response(
                    get_response_body(["La empresa ha sido modificada."], $empresa),
                    Response::HTTP_OK
                );
            } else {
                DB::rollback(); // Se devuelven los cambios, por que la transacción falla
                return response(get_response_body(["Ocurrió un error al intentar modificar la empresa."]), Response::HTTP_CONFLICT);
            }
        }catch (ModelException $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(null, Response::HTTP_CONFLICT);
        }catch (Exception $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function modificarCobertura(Request $request, $id)
    {
        DB::beginTransaction(); // Se abre la transacción
        try{
            $datos = $request->all();
            $datos['id'] = $id;
            $validator = Validator::make($datos, [
                'id' => 'integer|required|exists:empresas,id',
                'departamento_id' => 'integer|required|exists:departamentos,id',
                'ciudad_ids' => 'array|nullable',
                'ciudad_ids.*' => 'integer|exists:ciudades,id'
            ]);

            if($validator->fails()) {
                return response(
                    get_response_body(format_messages_validator($validator))
                    , Response::HTTP_BAD_REQUEST
                );
            }

            $empresa = $this->empresaService->modificarCobertura($datos);
            if(isset($empresa)){
                DB::commit(); // Se cierra la transacción correctamente
                return response(
                    get_response_body(["La cobertura de la empresa ha sido modificada."], $empresa),
                    Response::HTTP_OK
                );
            } else {
                DB::rollback(); // Se devuelven los cambios, por que la transacción falla
                return response(get_response_body([
                    "Ocurrió un error al intentar modificar la cobertura de la empresa."
                ]), Response::HTTP_CONFLICT);
            }
        }catch (ModelException $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(get_response_body([$e->getMessage()]), Response::HTTP_CONFLICT);
        }catch (Exception $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction(); // Se abre la transacción
        try{
            $datos['id'] = $id;
            $validator = Validator::make($datos, [
                'id' => 'integer|required|exists:empresas,id'
            ]);

            if($validator->fails()) {
                return response(
                    get_response_body(format_messages_validator($validator))
                    , Response::HTTP_BAD_REQUEST
                );
            }

            $eliminado = $this->empresaService->eliminar($id);
            if($eliminado){
                DB::commit(); // Se cierra la transacción correctamente
                return response(
                    get_response_body(["La empresa ha sido eliminada."]),
                    Response::HTTP_OK
                );
            }else{
                DB::rollback(); // Se devuelven los cambios, por que la transacción falla
                return response(get_response_body(["Ocurrió un error al intentar eliminar la empresa."]), Response::HTTP_CONFLICT);
            }
        }catch (ModelException $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(get_response_body([$e->getMessage()]), Response::HTTP_CONFLICT);
        }catch (Exception $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}

