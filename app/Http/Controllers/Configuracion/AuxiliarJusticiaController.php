<?php

namespace App\Http\Controllers\Configuracion;

use App\Contracts\Configuracion\AuxiliarJusticiaService;
use App\Exceptions\ModelException;
use App\Imports\AuxiliarJusticiaImport;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class AuxiliarJusticiaController extends Controller
{

    protected $auxiliarJusticiaService;

    public function __construct(AuxiliarJusticiaService $auxiliarJusticiaService){
        $this->auxiliarJusticiaService = $auxiliarJusticiaService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try{
            $datos = $request->all();
            if(!($request->ligera)) {
                $validator = Validator::make($datos, [
                    'limite' => 'integer|between:1,500'
                ]);

                if ($validator->fails()) {
                    return response(
                        get_response_body(format_messages_validator($validator))
                        , Response::HTTP_BAD_REQUEST
                    );
                }
            }
            if($request->ligera){
                $auxiliaresJusticia = $this->auxiliarJusticiaService->obtenerColeccionLigera($datos);
            }else {
                if (isset($datos['ordenar_por'])) {
                    $datos['ordenar_por'] = format_order_by_attributes($datos);
                }
                $auxiliaresJusticia = $this->auxiliarJusticiaService->obtenerColeccion($datos);
            }
            return response($auxiliaresJusticia, Response::HTTP_OK);
        }catch(Exception $e){
            return response(get_response_body([$e->getMessage()]), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * subir archivo excel
     * @return Response|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function storeExcel(Request $request){
        Excel::import(new AuxiliarJusticiaImport, request()->file('AUXILIARES DE LA JUSTICIA - VERSION 2013.xls'));
        return redirect('/auxiliares-de-la-justicia')->with('success', 'All good!');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('auxiliar-justicia.form');
    }

    /**
     * Obtener el template para la pantalla de resumen
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndexTemplate()
    {
        return view('auxiliar-justicia.index');
    }

    /**
     * Obtener el template para la pantalla de resumen
     *
     * @return \Illuminate\Http\Response
     */
    public function getImportarTemplate()
    {
        return view('auxiliar-justicia.importar');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction(); // Se abre la transacción
        try{
            $datos = $request->all();
            $validator = Validator::make($datos, [
                'numero_documento' => 'string|required|min:4|max:15',
                'nombres_auxiliar' => 'string|required|min:4|max:191',
                'apellidos_auxiliar' => 'string|required|min:4|max:191',
                'nombre_ciudad' => 'string|required|min:4|max:191',
                'direccion_oficina' => 'string|required|min:4|max:191',
                'telefono_oficina' => 'string|required|min:4|max:191',
                'cargo' => 'string|required|min:4|max:191',
                'telefono_celular' => 'string|required|min:4|max:191',
                'direccion_residencia' => 'string|min:4|max:191',
                'telefono_residencia' => 'string|min:4|max:191',
                'correo_electronico' => 'string|required|email|max:191',
                'estado' => 'boolean|nullable'
            ]);

            if($validator->fails()) {
                return response(
                    get_response_body(format_messages_validator($validator))
                    , Response::HTTP_BAD_REQUEST
                );
            }

            $auxiliarJusticia = $this->auxiliarJusticiaService->modificarOCrear($datos);
            if(isset($auxiliarJusticia)){
                DB::commit(); // Se cierra la transacción correctamente
                return response(
                    get_response_body(["El auxiliar de la justicia ha sido creado."], $auxiliarJusticia),
                    Response::HTTP_CREATED
                );
            } else {
                DB::rollback(); // Se devuelven los cambios, por que la transacción falla
                return response(get_response_body(["Ocurrió un error al intentar crear el auxiliar de la justicia."]), Response::HTTP_CONFLICT);
            }
        }catch (ModelException $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(get_response_body([$e->getMessage()]), Response::HTTP_CONFLICT);
        }catch (Exception $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(get_response_body([$e->getMessage()]), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $datos['id'] = $id;
            $validator = Validator::make($datos, [
                'id' => 'integer|required|exists:auxiliares_justicia,id'
            ]);

            if($validator->fails()) {
                return response(
                    get_response_body(format_messages_validator($validator))
                    , Response::HTTP_BAD_REQUEST
                );
            }

            return response($this->auxiliarJusticiaService->cargar($id), Response::HTTP_OK);
        }catch (Exception $e){
            return response(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction(); // Se abre la transacción
        try{
            $datos = $request->all();
            $datos['id'] = $id;
            $validator = Validator::make($datos, [
                'id' => 'integer|required|exists:auxiliares_justicia,id',
                'numero_documento' => 'string|required|min:4|max:15',
                'nombres_auxiliar' => 'string|required|min:4|max:191',
                'apellidos_auxiliar' => 'string|required|min:4|max:191',
                'nombre_ciudad' => 'string|required|min:4|max:191',
                'direccion_oficina' => 'string|required|min:4|max:191',
                'telefono_oficina' => 'string|required|min:4|max:191',
                'cargo' => 'string|required|min:4|max:191',
                'telefono_celular' => 'string|required|min:4|max:191',
                'direccion_residencia' => 'string|min:4|max:191',
                'telefono_residencia' => 'string|min:4|max:191',
                'correo_electronico' => 'string|required|email|max:191',
                'estado' => 'boolean|nullable'
            ]);

            if($validator->fails()) {
                return response(
                    get_response_body(format_messages_validator($validator))
                    , Response::HTTP_BAD_REQUEST
                );
            }

            $auxiliarJusticia = $this->auxiliarJusticiaService->modificarOCrear($datos);
            if(isset($auxiliarJusticia)){
                DB::commit(); // Se cierra la transacción correctamente
                return response(
                    get_response_body(["El auxiliar de la justicia ha sido modificado."], $auxiliarJusticia),
                    Response::HTTP_OK
                );
            } else {
                DB::rollback(); // Se devuelven los cambios, por que la transacción falla
                return response(get_response_body(["Ocurrió un error al intentar modificar el auxiliar de la justicia."]), Response::HTTP_CONFLICT);
            }
        }catch (ModelException $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(get_response_body([$e->getMessage()]), Response::HTTP_CONFLICT);
        }catch (Exception $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(get_response_body([$e->getMessage()]), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction(); // Se abre la transacción
        try{
            $datos['id'] = $id;
            $validator = Validator::make($datos, [
                'id' => 'integer|required|exists:auxiliares_justicia,id'
            ]);

            if($validator->fails()) {
                return response(
                    get_response_body(format_messages_validator($validator))
                    , Response::HTTP_BAD_REQUEST
                );
            }

            $eliminado = $this->auxiliarJusticiaService->eliminar($id);
            if($eliminado){
                DB::commit(); // Se cierra la transacción correctamente
                return response(
                    get_response_body(["El auxiliar de la justicia ha sido eliminado."]),
                    Response::HTTP_OK
                );
            }else{
                DB::rollback(); // Se devuelven los cambios, por que la transacción falla
                return response(get_response_body(["Ocurrió un error al intentar eliminar el auxiliar de la justicia."]), Response::HTTP_CONFLICT);
            }
        }catch (Exception $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function importar(Request $request){
        DB::beginTransaction(); // Se abre la transacción
        try{
            $archivo = $request->file('file');
            $errores = $this->auxiliarJusticiaService->importar($archivo);
            DB::commit(); // Se cierra la transacción correctamente
            return response(
                get_response_body(["Los datos de los auxiliares de la justicias se han importado."], $errores),
                Response::HTTP_CREATED
            );
        }catch (ModelException $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(get_response_body([$e->getMessage()]), Response::HTTP_CONFLICT);
        }catch (Exception $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(get_response_body(["Revisar archivo de carga. Estructura de información no corresponde."]), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

}
