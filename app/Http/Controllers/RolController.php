<?php

namespace App\Http\Controllers;

use App\Contracts\Seguridad\RolService;
use App\Enum\TipoRolEnum;
use App\Exceptions\ModelException;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class RolController extends Controller
{

    protected  $rolService;

    public function __construct(RolService $rolService){
        $this->rolService = $rolService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        try{
            $datos = $request->all();
            if(!$request->ligera){
                $validator = Validator::make($datos, [
                    'limite' => 'integer|between:1,500'
                ]);

                if($validator->fails()) {
                    return response(
                        get_response_body(format_messages_validator($validator))
                        , Response::HTTP_BAD_REQUEST
                    );
                }
            }

            if($request->ligera){
                $roles = $this->rolService->obtenerColeccionLigera($datos);
            }else{
                $user = Auth::user();
                if(!$user->can('ListarRol')){
                    return response(null, Response::HTTP_FORBIDDEN);
                }

                if(isset($datos['ordenar_por'])){
                    $datos['ordenar_por'] = format_order_by_attributes($datos);
                }
                $roles = $this->rolService->obtenerColeccion($datos);
            }
            return response($roles, Response::HTTP_OK);
        }catch(Exception $e){
            return response($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('rol.form');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createPermiso()
    {
        return view('permiso.form');
    }

    /**
     * Obtener el template para resumen de roles.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndexTemplate()
    {
        return view('rol.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction(); // Se abre la transacción
        try {
            $datos = $request->all();
            $tipoRolOpciones = TipoRolEnum::obtenerOpciones();
            $validator = Validator::make($datos, [
                'nombre' => 'string|required|min:3|max:191',
                'tipo' => 'integer|required|in:' . join(",", $tipoRolOpciones),
                'estado' => 'boolean|required'
            ]);

            if ($validator->fails()) {
                return response(
                    get_response_body(format_messages_validator($validator))
                    , Response::HTTP_BAD_REQUEST
                );
            }

            $rol = $this->rolService->modificarOCrear($datos);
            if ($rol) {
                DB::commit(); // Se cierra la transacción correctamente
                return response(
                    get_response_body(["El rol ha sido creado."], $rol),
                    Response::HTTP_CREATED
                );
            } else {
                DB::rollback(); // Se devuelven los cambios, por que la transacción falla
                return response(get_response_body(["Ocurrió un error al intentar crear el rol."]), Response::HTTP_CONFLICT);
            }
        }catch (ModelException $e){
                DB::rollback(); // Se devuelven los cambios, por que la transacción falla
                return response(get_response_body([$e->getMessage()]), Response::HTTP_CONFLICT);
        }catch (Exception $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $datos['id'] = $id;
            $validator = Validator::make($datos, [
                'id' => 'integer|required|exists:roles,id'
            ]);

            if($validator->fails()) {
                return response(
                    get_response_body(format_messages_validator($validator))
                    , Response::HTTP_BAD_REQUEST
                );
            }

            return response($this->rolService->cargar($id), Response::HTTP_OK);
        }catch (Exception $e){
            return response(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction(); // Se abre la transacción
        try{
            $datos = $request->all();
            $datos['id'] = $id;
            $tipoRolOpciones = TipoRolEnum::obtenerOpciones();
            $validator = Validator::make($datos, [
                'id' => 'integer|required|exists:roles,id',
                'nombre' => 'string|required|min:3|max:191',
                'tipo' => 'integer|required|in:' . join(",", $tipoRolOpciones),
                'estado' => 'boolean|required'
            ]);

            if($validator->fails()) {
                return response(
                    get_response_body(format_messages_validator($validator))
                    , Response::HTTP_BAD_REQUEST
                );
            }

            $rol = $this->rolService->modificarOCrear($datos);
            if($rol){
                DB::commit(); // Se cierra la transacción correctamente
                return response(
                    get_response_body(["El rol ha sido modifcado."], $rol),
                    Response::HTTP_OK
                );
            } else {
                DB::rollback(); // Se devuelven los cambios, por que la transacción falla
                return response(get_response_body(["Ocurrió un error al intentar modificar el rol."]), Response::HTTP_CONFLICT);;
            }
        }catch (ModelException $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(get_response_body([$e->getMessage()]), Response::HTTP_CONFLICT);
        }catch (Exception $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(get_response_body([$e->getMessage()]), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction(); // Se abre la transacción
        try{
            $datos['id'] = $id;
            $validator = Validator::make($datos, [
                'id' => 'integer|required|exists:roles,id'
            ]);

            if($validator->fails()) {
                return response(
                    get_response_body(format_messages_validator($validator))
                    , Response::HTTP_BAD_REQUEST
                );
            }

            $eliminado = $this->rolService->eliminar($id);
            if($eliminado){
                DB::commit(); // Se cierra la transacción correctamente
                return response(
                    get_response_body(["El rol ha sido eliminado."]),
                    Response::HTTP_OK
                );
            }else{
                DB::rollback(); // Se devuelven los cambios, por que la transacción falla
                return response(get_response_body(["Ocurrió un error al intentar eliminar el rol."]), Response::HTTP_CONFLICT);
            }
        }catch (Exception $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Obtener permisos del rol, con y sin permisos
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function obtenerPermisos(Request $request, $id){
        try{
            $permisos = $this->rolService->obtenerPermisos($id, $request->all());
            return response($permisos, Response::HTTP_OK);
        }catch (Exception $e){
            return response(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Otorgar permisos al rol
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function otorgarPermisos(Request $request, $id){
        try{
            $permisos = $request->all();
            $this->rolService->otorgarPermisos($id, $permisos);
            return response(null, Response::HTTP_OK);
        }catch (Exception $e){
            return response(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Rovocar permisos al rol
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function revocarPermisos(Request $request, $id){
        try{
            $permisos = $request->all();
            $this->rolService->revocarPermisos($id, $permisos);
            return response(null, Response::HTTP_OK);
        }catch (Exception $e){
            return response(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

}
