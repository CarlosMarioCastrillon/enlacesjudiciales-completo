<?php

namespace App\Http\Controllers;

use App\Model\Administracion\ParametroConstante;
use App\Model\Seguridad\Usuario;
use Carbon\Carbon;
use DateTimeZone;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\User;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Load a user.
     *
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function load(Request $request, $id){
        //
    }

    /**
     * Obtener un token de sesión
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getToken(Request $request){
        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required'
        ]);

        if($validator->fails()){
            $errors = $validator->errors();
            return response([
                "messages" => $errors
            ], Response::HTTP_UNAUTHORIZED);
        }

        $http = new Client;
        $hostname = env("APP_URL");

        try{
            $usuario = Usuario::findBy($request->username);
            if(isset($usuario)){
                $response = $http->post($hostname . '/oauth/token', [
                    'form_params' => [
                        'client_id' => env("PASSWORD_CLIENT_ID"),
                        'client_secret' => env("PASSWORD_CLIENT_SECRET"),
                        'grant_type' => 'password',
                        'username' => $request->username,
                        'password' => $request->password
                    ]
                ]);

                if($response->getStatusCode() == Response::HTTP_OK){
                    $responseBody = json_decode((string) $response->getBody(), true);
                    return response($responseBody,Response::HTTP_OK);
                } else {
                    return response([
                        "messages" => ["La contraseña ingresada es inválida."]
                    ],Response::HTTP_UNAUTHORIZED);
                }
            }else{
                return response([
                    "messages" => ["El usuario con identificación " . $request->username . " no está registrado."]
                ],Response::HTTP_UNAUTHORIZED);
            }
        }catch (RequestException $e){
            return response([
                "messages" => ["La contraseña ingresada es inválida."]
            ],Response::HTTP_UNAUTHORIZED);
        }catch (Exception $e){
            return response([
                "messages" => $e->getMessage()
            ],Response::HTTP_UNAUTHORIZED);
        }
    }

    /**
     * Get info usuario en sesión
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getSession(){
        try{
            // Constantes
            $parametros = ParametroConstante::cargarParametros();

            $user = Auth::user();
            $rol = $user->rol();
            $usuario = $user->usuario();
            $empresa = $user->empresa();
            $tipoDocumento = $user->tipoDocumento();
            $permisos = $user->getAllPermissions();
            $listaPermisos = [];
            try{
                foreach ($permisos as $permiso){
                    array_push($listaPermisos, $permiso->name);
                }
            }catch (Exception $e){}

            return response([
                'rol' => [
                    'id' => $rol->id,
                    'nombre' => $rol->name
                ],
                'usuario' => [
                    'id' => $usuario->id,
                    'nombre' => $usuario->nombre,
                    'documento' => $usuario->documento,
                    'email' => $usuario->email_uno
                ],
                'empresa' =>  [
                    'id' => $empresa->id,
                    'nombre' => $empresa->nombre,
                    'numero_documento' => $empresa->numero_documento
                ],
                'tipo_documento' => [
                    'id' => $tipoDocumento->id,
                    'codigo' => $tipoDocumento->codigo,
                    'nombre' => $tipoDocumento->nombre
                ],
                'parametros' => $parametros,
                'permisos' => $listaPermisos
            ], Response::HTTP_OK);
        }catch (Exception $e){
            return response(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Revoke user tokens
     */
    public function logout(){
        try{
            $user = Auth::user();
            $userTokens = $user->tokens;
            foreach($userTokens as $token) {
                $token->revoke();
            }
            return response(null, Response::HTTP_OK);
        }catch (Exception $e){
            return response([
                "messages" => "Ocurrió un error al intentar cerrar la sesión."
            ], Response::HTTP_UNAUTHORIZED);
        }
    }

    public function getCambiarSesionTemplate()
    {
        return view('cambiar-sesion');
    }

}
