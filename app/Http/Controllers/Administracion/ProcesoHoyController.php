<?php

namespace App\Http\Controllers\Administracion;

use App\Contracts\Administracion\ActuacionPorEmpresaService;
use App\Exports\ActuacionPorEmpresaExport;
use Exception;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class ProcesoHoyController extends Controller
{
    protected $procesoHoyService;

    public function __construct(ActuacionPorEmpresaService $procesoHoyService){
        $this->procesoHoyService = $procesoHoyService;
    }
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try{
            $datos = $request->all();
            $validator = Validator::make($datos, [
                    'limite' => 'integer|between:1,500'
                ]);

                if ($validator->fails()) {
                    return response(
                        get_response_body(format_messages_validator($validator))
                        , Response::HTTP_BAD_REQUEST
                    );
                }

                if (isset($datos['ordenar_por'])) {
                    $datos['ordenar_por'] = format_order_by_attributes($datos);
                }
                $procesosHoy = $this->procesoHoyService->obtenerColeccion($datos);

            return response($procesosHoy, Response::HTTP_OK);
        }catch(Exception $e){
            return response(get_response_body([$e->getMessage()]), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * Obtener el template para la pantalla de resumen
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndexTemplate()
    {
        return view('proceso-hoy.index');
    }


    /**
     * Obtener archivo excel
     * @return Response|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function getExcel(Request $request){
        $dto = [
            'fecha_inicial' => $request->fecha_inicial,
            'fecha_final' => $request->fecha_final,
        ];
        return (new ActuacionPorEmpresaExport($dto))->download('procesos_hoy.xlsx');
    }
}

