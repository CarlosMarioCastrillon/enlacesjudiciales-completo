<?php

namespace App\Http\Controllers\Administracion;

use App\Contracts\Administracion\DiligenciaService;
use App\Exceptions\ModelException;
use App\Model\Administracion\Diligencia;
use App\Model\Administracion\DiligenciaHistoria;
use Exception;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class DiligenciaController extends Controller
{
    protected $diligenciaService;

    public function __construct(DiligenciaService $diligenciaService){
        $this->diligenciaService = $diligenciaService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try{
            $datos = $request->all();
            $validator = Validator::make($datos, [
                'limite' => 'integer|between:1,500'
            ]);

            if ($validator->fails()) {
                return response(
                    get_response_body(format_messages_validator($validator))
                    , Response::HTTP_BAD_REQUEST
                );
            }
            if (isset($datos['ordenar_por'])) {
                $datos['ordenar_por'] = format_order_by_attributes($datos);
            }
            $diligenciasDiligencias = $this->diligenciaService->obtenerColeccion($datos);
            return response($diligenciasDiligencias, Response::HTTP_OK);
        }catch(Exception $e){
            return response(get_response_body([$e->getMessage()]), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Obtener archivo excel
     * @return Response|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function getExcel(Request $request){
        $dto = [
            'numero_diligencia' => $request->numero_diligencia,
            'empresa_cliente' => $request->empresa_cliente,
            'numero_proceso' => $request->numero_proceso,
            'estado_diligencia' => $request->estado_diligencia,
            'fecha_inicial' => $request->fecha_inicial,
            'fecha_final' => $request->fecha_final,
            'dependiente' => $request->dependiente,
        ];
        $excel = $this->diligenciaService->getExcel($dto);
        return $excel->download('diligencias' . time() . '.xlsx');
    }

    /**
     * Obtener archivo excel
     * @return Response|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function obtenerConsultaExcel(Request $request){
        $dto = [
            'numero_diligencia' => $request->numero_diligencia,
            'numero_proceso' => $request->numero_proceso,
            'estado_diligencia' => $request->estado_diligencia,
            'fecha_inicial' => $request->fecha_inicial,
            'fecha_final' => $request->fecha_final,
        ];
        $excel = $this->diligenciaService->obtenerConsultaExcel($dto);
        return $excel->download('diligencias' . time() . '.xlsx');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('diligencia.form');
    }

    /**
     * Obtener el template para la pantalla de resumen
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndexTemplate()
    {
        return view('diligencia.index');
    }

    /**
     * Obtener el template para la pantalla de historia de diligencias
     *
     * @return \Illuminate\Http\Response
     */
    public function getHistoriaTemplate()
    {
        return view('diligencia.historia');
    }

    /**
     * Obtener el template para la pantalla de trámite
     *
     * @return \Illuminate\Http\Response
     */
    public function getTramiteTemplate()
    {
        return view('diligencia.tramite');
    }

    public function getHistoriaEditTemplate()
    {
        return view('diligencia.historia-form');
    }

    public function getConsultaClienteTemplate()
    {
        return view('diligencia.consulta-cliente');
    }

    public function getFormClienteTemplate()
    {
        return view('diligencia.form-cliente');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction(); // Se abre la transacción
        try{
            $datos = $request->all();
            $validator = Validator::make($datos, [
                'empresa_id' => 'integer|required',
                'proceso_id' => 'integer|required',
                'ciudad_id' => 'integer|required',
                'fecha_diligencia' => 'date|required',
                'departamento_id' => 'integer|required',
                'tipo_diligencia_id' => 'integer|required',
                'valor_diligencia' => 'integer|nullable',
                'gasto_envio' => 'integer|nullable',
                'otros_gastos' => 'integer|nullable',
                'costo_diligencia' => 'integer|nullable',
                'costo_envio' => 'integer|nullable',
                'otros_costos' => 'integer|nullable',
                'observaciones_cliente' => 'string|nullable',
            ]);

            if($validator->fails()) {
                return response(
                    get_response_body(format_messages_validator($validator))
                    , Response::HTTP_BAD_REQUEST
                );
            }

            $diligencia = $this->diligenciaService->modificarOCrear($datos);
            if(isset($diligencia)){
                DB::commit(); // Se cierra la transacción correctamente
                return response(get_response_body(["La diligencia ha sido creada."], $diligencia),
                    Response::HTTP_CREATED
                );
            } else {
                DB::rollback(); // Se devuelven los cambios, por que la transacción falla
                return response(get_response_body(["Ocurrió un error al intentar crear la diligencia."]), Response::HTTP_CONFLICT);
            }
        }catch (ModelException $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(get_response_body([$e->getMessage()]), Response::HTTP_CONFLICT);
        }catch (Exception $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(get_response_body([$e->getMessage()]), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $datos['id'] = $id;
            $validator = Validator::make($datos, [
                'id' => 'integer|required|exists:diligencias,id'
            ]);

            if($validator->fails()) {
                return response(
                    get_response_body(format_messages_validator($validator))
                    , Response::HTTP_BAD_REQUEST
                );
            }
            return response($this->diligenciaService->cargar($id), Response::HTTP_OK);
        }catch (Exception $e){
            return response(get_response_body([$e->getMessage()]), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction(); // Se abre la transacción
        try{
            $datos = $request->all();
            $datos['id'] = $id;
            $validator = Validator::make($datos, [
                'id' => 'integer|required|exists:diligencias,id',
                'empresa_id' => 'integer|required',
                'proceso_id' => 'integer|required',
                'fecha_diligencia' => 'date|required',
                'ciudad_id' => 'integer|required',
                'departamento_id' => 'integer|required',
                'tipo_diligencia_id' => 'integer|required',
                'valor_diligencia' => 'integer|nullable',
                'gasto_envio' => 'integer|nullable',
                'otros_gastos' => 'integer|nullable',
                'costo_diligencia' => 'integer|nullable',
                'costo_envio' => 'integer|nullable',
                'otros_costos' => 'integer|nullable',
                'observaciones_cliente' => 'string|nullable',
            ]);

            if($validator->fails()) {
                return response(
                    get_response_body(format_messages_validator($validator))
                    , Response::HTTP_BAD_REQUEST
                );
            }

            $diligencia = $this->diligenciaService->modificarOCrear($datos);
            if(isset($diligencia)){
                DB::commit(); // Se cierra la transacción correctamente
                return response(
                    get_response_body(["La diligencia ha sido modificada."], $diligencia),
                    Response::HTTP_OK
                );
            } else {
                DB::rollback(); // Se devuelven los cambios, por que la transacción falla
                return response(get_response_body(["Ocurrió un error al intentar modificar la diligencia."]), Response::HTTP_CONFLICT);
            }
        }catch (ModelException $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(get_response_body([$e->getMessage()]), Response::HTTP_CONFLICT);
        }catch (Exception $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(get_response_body([$e->getMessage()]), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction(); // Se abre la transacción
        try{
            $datos['id'] = $id;
            $validator = Validator::make($datos, [
                'id' => 'integer|required|exists:diligencias,id'
            ]);

            if($validator->fails()) {
                return response(
                    get_response_body(format_messages_validator($validator))
                    , Response::HTTP_BAD_REQUEST
                );
            }

            $eliminado = $this->diligenciaService->eliminar($id);
            if($eliminado){
                DB::commit(); // Se cierra la transacción correctamente
                return response(
                    get_response_body(["La diligencia ha sido eliminada."]),
                    Response::HTTP_OK
                );
            }else{
                DB::rollback(); // Se devuelven los cambios, por que la transacción falla
                return response(get_response_body(["Ocurrió un error al intentar eliminar la diligencia."]), Response::HTTP_CONFLICT);
            }
        }catch (Exception $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(get_response_body([$e->getMessage()]), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    // Achivos anexos

    /**
     * Obtener el archivo anexo de la actuación
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function obtenerAnexoDiligencia($id){
        try{
            $user = Auth::user();
            $empresa = $user->empresa();

            $diligencia = Diligencia::find($id);
            if(!isset($diligencia->url_anexo_diligencia)){
                return response(null, Response::HTTP_NOT_FOUND);
            }

            // Validar super user
            /*if($diligencia->empresa_id != $empresa->id){
                return response(null, Response::HTTP_FORBIDDEN);
            }*/

            $mime = Storage::disk('s3')->getDriver()->getMimetype($diligencia->url_anexo_diligencia);
            $size = Storage::disk('s3')->getDriver()->getSize($diligencia->url_anexo_diligencia);

            $response =  [
                'Content-Type' => $mime,
                'Content-Length' => $size,
                'Content-Disposition' => "attachment; filename=" .
                    str_replace(",", "", str_replace(";", "", $diligencia->archivo_anexo_diligencia))
            ];

            return \Response::make(Storage::disk('s3')->get($diligencia->url_anexo_diligencia), Response::HTTP_OK, $response);
        }catch(FileNotFoundException $e){
            return response($e->getMessage(), Response::HTTP_NOT_FOUND);
        }catch(Exception $e){
            return response($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Obtener el archivo anexo de la actuación
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function obtenerAnexoCostos($id){
        try{
            $user = Auth::user();
            $empresa = $user->empresa();

            $diligencia = Diligencia::find($id);
            if(!isset($diligencia->url_anexo_costos)){
                return response(null, Response::HTTP_NOT_FOUND);
            }

            // Validar super user
            /*if($diligencia->empresa_id != $empresa->id){
                return response(null, Response::HTTP_FORBIDDEN);
            }*/

            $mime = Storage::disk('s3')->getDriver()->getMimetype($diligencia->url_anexo_costos);
            $size = Storage::disk('s3')->getDriver()->getSize($diligencia->url_anexo_costos);

            $response =  [
                'Content-Type' => $mime,
                'Content-Length' => $size,
                'Content-Disposition' => "attachment; filename=" .
                    str_replace(",", "", str_replace(";", "", $diligencia->archivo_anexo_costos))
            ];

            return \Response::make(Storage::disk('s3')->get($diligencia->url_anexo_costos), Response::HTTP_OK, $response);
        }catch(FileNotFoundException $e){
            return response($e->getMessage(), Response::HTTP_NOT_FOUND);
        }catch(Exception $e){
            return response($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Obtener el archivo anexo de la actuación
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function obtenerAnexoRecibido($id){
        try{
            $user = Auth::user();
            $empresa = $user->empresa();

            $diligencia = Diligencia::find($id);
            if(!isset($diligencia->url_anexo_recibido)){
                return response(null, Response::HTTP_NOT_FOUND);
            }

            // Validar super user
            /*if($diligencia->empresa_id != $empresa->id){
                return response(null, Response::HTTP_FORBIDDEN);
            }*/

            $mime = Storage::disk('s3')->getDriver()->getMimetype($diligencia->url_anexo_recibido);
            $size = Storage::disk('s3')->getDriver()->getSize($diligencia->url_anexo_recibido);

            $response =  [
                'Content-Type' => $mime,
                'Content-Length' => $size,
                'Content-Disposition' => "attachment; filename=" .
                    str_replace(",", "", str_replace(";", "", $diligencia->archivo_anexo_recibido))
            ];

            return \Response::make(Storage::disk('s3')->get($diligencia->url_anexo_recibido), Response::HTTP_OK, $response);
        }catch(FileNotFoundException $e){
            return response($e->getMessage(), Response::HTTP_NOT_FOUND);
        }catch(Exception $e){
            return response($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Obtener el archivo anexo de la historia de la diligencia
     * @param $historiaId
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function obtenerAnexoDiligenciaHistoria($id, $historiaId){
        try{
            $user = Auth::user();
            $empresa = $user->empresa();

            $diligenciaHistoria = DiligenciaHistoria::find($historiaId);

            if(!isset($diligenciaHistoria->url_anexo_diligencia)){
                return response(null, Response::HTTP_NOT_FOUND);
            }

            // Validar super user
            /*if($diligencia->empresa_id != $empresa->id){
                return response(null, Response::HTTP_FORBIDDEN);
            }*/

            $mime = Storage::disk('s3')->getDriver()->getMimetype($diligenciaHistoria->url_anexo_diligencia);
            $size = Storage::disk('s3')->getDriver()->getSize($diligenciaHistoria->url_anexo_diligencia);

            $response =  [
                'Content-Type' => $mime,
                'Content-Length' => $size,
                'Content-Disposition' => "attachment; filename=" .
                    str_replace(",", "", str_replace(";", "", $diligenciaHistoria->archivo_anexo_diligencia))
            ];

            return \Response::make(Storage::disk('s3')->get($diligenciaHistoria->url_anexo_diligencia), Response::HTTP_OK, $response);
        }catch(FileNotFoundException $e){
            return response($e->getMessage(), Response::HTTP_NOT_FOUND);
        }catch(Exception $e){
            return response($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Obtener el archivo anexo de la historia de la diligencia
     * @param $historiaId
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function obtenerAnexoCostosHistoria($id, $historiaId){
        try{
            $user = Auth::user();
            $empresa = $user->empresa();

            $diligencia = DiligenciaHistoria::find($historiaId);
            if(!isset($diligencia->url_anexo_costos)){
                return response(null, Response::HTTP_NOT_FOUND);
            }

            // Validar super user
            /*if($diligencia->empresa_id != $empresa->id){
                return response(null, Response::HTTP_FORBIDDEN);
            }*/

            $mime = Storage::disk('s3')->getDriver()->getMimetype($diligencia->url_anexo_costos);
            $size = Storage::disk('s3')->getDriver()->getSize($diligencia->url_anexo_costos);

            $response =  [
                'Content-Type' => $mime,
                'Content-Length' => $size,
                'Content-Disposition' => "attachment; filename=" .
                    str_replace(",", "", str_replace(";", "", $diligencia->archivo_anexo_costos))
            ];

            return \Response::make(Storage::disk('s3')->get($diligencia->url_anexo_costos), Response::HTTP_OK, $response);
        }catch(FileNotFoundException $e){
            return response($e->getMessage(), Response::HTTP_NOT_FOUND);
        }catch(Exception $e){
            return response($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Obtener el archivo anexo de la historia de la diligencia
     * @param $historiaId
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function obtenerAnexoRecibidoHistoria($id, $historiaId){
        try{
            $user = Auth::user();
            $empresa = $user->empresa();

            $diligencia = DiligenciaHistoria::find($historiaId);
            if(!isset($diligencia->url_anexo_recibido)){
                return response(null, Response::HTTP_NOT_FOUND);
            }

            // Validar super user
            /*if($diligencia->empresa_id != $empresa->id){
                return response(null, Response::HTTP_FORBIDDEN);
            }*/

            $mime = Storage::disk('s3')->getDriver()->getMimetype($diligencia->url_anexo_recibido);
            $size = Storage::disk('s3')->getDriver()->getSize($diligencia->url_anexo_recibido);

            $response =  [
                'Content-Type' => $mime,
                'Content-Length' => $size,
                'Content-Disposition' => "attachment; filename=" .
                    str_replace(",", "", str_replace(";", "", $diligencia->archivo_anexo_recibido))
            ];

            return \Response::make(Storage::disk('s3')->get($diligencia->url_anexo_recibido), Response::HTTP_OK, $response);
        }catch(FileNotFoundException $e){
            return response($e->getMessage(), Response::HTTP_NOT_FOUND);
        }catch(Exception $e){
            return response($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Obtener el historico de las diligencias
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function cargarHistorico($id, $historiaId){
        try{
            $diligencias = $this->diligenciaService->cargarHistorico($historiaId);
            return response($diligencias, Response::HTTP_OK);
        }catch(Exception $e){
            return response(get_response_body([$e->getMessage()]), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Obtener coleccion diligencias por cliente
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function obtenerDiligenciasPorCliente(Request $request)
    {
        try{
            $datos = $request->all();
            $validator = Validator::make($datos, [
                'limite' => 'integer|between:1,500'
            ]);

            if ($validator->fails()) {
                return response(
                    get_response_body(format_messages_validator($validator))
                    , Response::HTTP_BAD_REQUEST
                );
            }

            if (isset($datos['ordenar_por'])) {
                $datos['ordenar_por'] = format_order_by_attributes($datos);
            }
            $diligencias = $this->diligenciaService->obtenerDiligenciaPorCliente($datos);
            return response($diligencias, Response::HTTP_OK);
        }catch(Exception $e){
            return response(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}

