<?php

namespace App\Http\Controllers\Administracion;

use App\Contracts\Administracion\ProcesoJudicialService;
use App\Exceptions\ModelException;
use App\Exports\ProcesoJudicialCargaErrorExport;
use App\Model\Administracion\ProcesoJudicial;
use App\Model\Configuracion\Ciudad;
use App\Model\Configuracion\Departamento;
use App\Model\Configuracion\Despacho;
use App\Model\Configuracion\Juzgado;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ProcesoJudicialController extends Controller
{
    protected $procesoJudicialService;

    public function __construct(ProcesoJudicialService $procesoJudicialService){
        $this->procesoJudicialService = $procesoJudicialService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try{
            $datos = $request->all();
            if(!($request->ligera || $request->de_referencia)) {
                $validator = Validator::make($datos, [
                    'limite' => 'integer|between:1,500'
                ]);

                if ($validator->fails()) {
                    return response(
                        get_response_body(format_messages_validator($validator))
                        , Response::HTTP_BAD_REQUEST
                    );
                }
            }

            if($request->ligera){
                $procesosJudiciales = $this->procesoJudicialService->obtenerColeccionLigera($datos);
            }elseif ($request->de_referencia){
                $procesosJudiciales = $this->procesoJudicialService->obtenerColeccionReferencia($datos);
            }else {
                if (isset($datos['ordenar_por'])) {
                    $datos['ordenar_por'] = format_order_by_attributes($datos);
                }
                $procesosJudiciales = $this->procesoJudicialService->obtenerColeccion($datos);
            }
            return response($procesosJudiciales, Response::HTTP_OK);
        }catch(Exception $e){
            return response(get_response_body([$e->getMessage()]), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('proceso-judicial.form');
    }

    /**
     * Obtener el template para la pantalla de resumen
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndexTemplate()
    {
        return view('proceso-judicial.index');
    }

    /**
     * Obtener el template para la pantalla de resumen
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getImportarTemplate()
    {
        return view('proceso-judicial.importar');
    }

    /**
     * Obtener el template para la pantalla de consulta de información de procesos generales
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getProcesoGeneralTemplate()
    {
        return view('proceso-judicial.proceso-general');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction(); // Se abre la transacción
        try{
            // Datos
            $datos = $request->all();

            // Información de sesión
            $user = Auth::user();
            $empresa = $user->empresa();
            $juzgado = null;
            if(isset($datos['juzgado_id'])){
                $juzgado = Juzgado::find($datos['juzgado_id']);
            }

            $validator = Validator::make($datos, [
                'departamento_id' => 'integer|required|exists:departamentos,id',
                'ciudad_id' => 'integer|required|exists:ciudades,id',
                'juzgado_id' => 'integer|required|exists:juzgados,id',
                'despacho_id' => function($attribute, $value, $onFailure) {
                    $conError = false;
                    if(!isset($value)){
                        $conError = true;
                        $mensaje = 'Número juzgado actual no válido.';
                    }
                    if($conError){
                        $onFailure($mensaje);
                    }
                },
                'anio_proceso' => function($attribute, $value, $onFailure) {
                    $conError = false;
                    if(!(isset($value) && trim($value) != "")){
                        $conError = true;
                        $mensaje = 'Número de año es requerido.';
                    }
                    if(!$conError){
                        $fechaActual = Carbon::now();
                        $fechaProceso = Carbon::now()->setYear($value);
                        if($fechaActual->diffInYears($fechaProceso) > 40 || $fechaActual->year < +$value){
                            $conError = true;
                            $mensaje = 'Número de año invalido.';
                        }
                    }
                    if($conError){
                        $onFailure($mensaje);
                    }
                },
                'numero_radicado' => function($attribute, $value, $onFailure) {
                    $conError = false;
                    if(!(isset($value) && trim($value) != "")){
                        $conError = true;
                        $mensaje = 'Número de radicado es requerido.';
                    }
                    if(!$conError && !(intval($value) >= 1 && intval($value) <= 99999)){
                        $conError = true;
                        $mensaje = 'Número de radicado incorrecto.';
                    }

                    if($conError){
                        $onFailure($mensaje);
                    }
                },
                'numero_radicado_proceso_actual' => function($attribute, $value, $onFailure) use($empresa, $juzgado) {
                    $mensajes = [];
                    $conError = false;
                    $contadorErrores = 0;
                    // Validar longitud de caracteres
                    if (!(isset($value) && strlen($value) == 23)) {
                        $conError = true;
                        $contadorErrores += 1;
                        array_push(
                            $mensajes,
                            $contadorErrores . '. Tamaño de número de proceso errado. Debe tener 23 dígitos.'
                        );
                    }
                    // Validar que sea numerico
                    if (!is_numeric($value)) {
                        $conError = true;
                        $contadorErrores += 1;
                        array_push(
                            $mensajes,
                            $contadorErrores . '. Número de proceso solo debe tener caracteres numéricos.'
                        );
                    }
                    // Validar que no exista en el sistema
                    $procesoJudicial = ProcesoJudicial::where('numero_radicado_proceso_actual', $value)
                        ->where('juzgado_actual', $juzgado->juzgado_rama ?? null)
                        ->where('empresa_id', $empresa->id)->first();
                    if(isset($procesoJudicial)){
                        $conError = true;
                        array_push($mensajes, 'Proceso ya existe para la empresa.');
                    }
                    // Validar datos número de proceso
                    $codigoDepartamento = substr($value,0,2);
                    $codigoCiudad = substr($value,0,5);
                    $codigoJuzgado = substr($value,0,9);
                    $numeroJuzgado = substr($value,5,2);
                    $numeroSala = substr($value,7,2);
                    $numeroDespacho = substr($value,9,3);
                    $anio = +substr($value,12,4);
                    $numeroRadicado = substr($value,16,5);
                    $consecutivo = substr($value,21,2);
                    $departamento = Departamento::findBy($codigoDepartamento);
                    if(!isset($departamento)){
                        $conError = true;
                        $contadorErrores += 1;
                        array_push($mensajes, $contadorErrores . '. Código departamento no válido.');
                    }
                    $ciudad = Ciudad::findBy($codigoCiudad);
                    if(!isset($ciudad)){
                        $conError = true;
                        $contadorErrores += 1;
                        array_push($mensajes, $contadorErrores . '. Código ciudad no válido.');
                    }
                    $juzgado = Juzgado::findBy($codigoJuzgado);
                    if(!isset($juzgado)){
                        $conError = true;
                        $contadorErrores += 1;
                        array_push($mensajes, $contadorErrores . '. Juzgado rama judicial no válido.');
                    }
                    $despacho = null;
                    if(isset($departamento) && isset($ciudad)){
                        $despacho = Despacho::where([
                            'departamento_id' => $departamento->id,
                            'ciudad_id' => $ciudad->id,
                            'juzgado' => $numeroJuzgado,
                            'sala' => $numeroSala,
                            'despacho' => $numeroDespacho
                        ])->first();
                    }
                    if(!isset($despacho)){
                        $conError = true;
                        $contadorErrores += 1;
                        array_push($mensajes, $contadorErrores . '. Número despacho no válido.');
                    }
                    $fechaActual = Carbon::now();
                    $fechaProceso = Carbon::now()->setYear($anio);
                    if($fechaActual->diffInYears($fechaProceso) > 40 || $fechaActual->year < $anio){
                        $conError = true;
                        $contadorErrores += 1;
                        array_push($mensajes, $contadorErrores . '. Año proceso incorrecto.');
                    }
                    if(!(intval($numeroRadicado) >= 1 && intval($numeroRadicado) <= 99999)){
                        $conError = true;
                        $contadorErrores += 1;
                        array_push($mensajes, $contadorErrores . '. Número de radicado incorrecto.');
                    }
                    if(!(intval($consecutivo) >= 0 && intval($consecutivo) <= 5)){
                        $conError = true;
                        $contadorErrores += 1;
                        array_push($mensajes, $contadorErrores . '. Número de consecutivo incorrecto.');
                    }

                    if($conError){
                        $onFailure(join(" ", $mensajes));
                    }
                },
                'nombres_demandantes' => 'string|required',
                'nombres_demandados' => 'string|required',
                'observaciones' => 'string|nullable',
                'abogado_responsable_id' => 'integer|nullable|exists:usuarios,id',
                'ultima_clase_proceso_id' => 'integer|nullable|exists:clases_de_procesos,id',
                'area_id' => 'integer|nullable|exists:areas,id',
            ]);

            if($validator->fails()) {
                return response(
                    get_response_body(format_messages_validator($validator))
                    , Response::HTTP_BAD_REQUEST
                );
            }

            $procesoJudicial = $this->procesoJudicialService->modificarOCrear($datos);
            DB::commit(); // Se cierra la transacción correctamente
            return response(
                get_response_body(["El proceso judicial ha sido creado."], $procesoJudicial),
                Response::HTTP_CREATED
            );
        }catch (ModelException $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(get_response_body([$e->getMessage()]), Response::HTTP_CONFLICT);
        }catch (Exception $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $datos['id'] = $id;
            $validator = Validator::make($datos, [
                'id' => 'integer|required|exists:procesos_judiciales,id'
            ]);

            if($validator->fails()) {
                return response(
                    get_response_body(format_messages_validator($validator))
                    , Response::HTTP_BAD_REQUEST
                );
            }

            $procesoJudicial = $this->procesoJudicialService->cargar($id);
            return response($procesoJudicial, Response::HTTP_OK);
        }catch (Exception $e){
            return response(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction(); // Se abre la transacción
        try{
            // Datos
            $datos = $request->all();

            // Información de sesión
            $user = Auth::user();
            $empresa = $user->empresa();
            $juzgado = null;
            if(isset($datos['juzgado_id'])){
                $juzgado = Juzgado::find($datos['juzgado_id']);
            }

            $validator = Validator::make($datos, [
                'id' => 'integer|required|exists:procesos_judiciales,id',
                'departamento_id' => 'integer|required|exists:departamentos,id',
                'ciudad_id' => 'integer|required|exists:ciudades,id',
                'juzgado_id' => 'integer|required|exists:juzgados,id',
                'despacho_id' => function($attribute, $value, $onFailure) {
                    $conError = false;
                    if(!isset($value)){
                        $conError = true;
                        $mensaje = 'Número juzgado actual no válido.';
                    }
                    if($conError){
                        $onFailure($mensaje);
                    }
                },
                'anio_proceso' => function($attribute, $value, $onFailure) {
                    $conError = false;
                    if(!(isset($value) && trim($value) != "")){
                        $conError = true;
                        $mensaje = 'Número de año es requerido.';
                    }
                    if(!$conError){
                        $fechaActual = Carbon::now();
                        $fechaProceso = Carbon::now()->setYear($value);
                        if($fechaActual->diffInYears($fechaProceso) > 40 || $fechaActual->year < +$value){
                            $conError = true;
                            $mensaje = 'Número de año invalido.';
                        }
                    }
                    if($conError){
                        $onFailure($mensaje);
                    }
                },
                'numero_radicado' => function($attribute, $value, $onFailure) {
                    $conError = false;
                    if(!(isset($value) && trim($value) != "")){
                        $conError = true;
                        $mensaje = 'Número de radicado es requerido.';
                    }
                    if(!$conError && !(intval($value) >= 1 && intval($value) <= 99999)){
                        $conError = true;
                        $mensaje = 'Número de radicado incorrecto.';
                    }

                    if($conError){
                        $onFailure($mensaje);
                    }
                },
                'numero_radicado_proceso_actual' => function($attribute, $value, $onFailure) use($datos, $empresa, $juzgado) {
                    $mensajes = [];
                    $conError = false;
                    $contadorErrores = 0;
                    // Validar longitud de caracteres
                    if (!(isset($value) && strlen($value) == 23)) {
                        $conError = true;
                        $contadorErrores += 1;
                        array_push(
                            $mensajes,
                            $contadorErrores . '. Tamaño de número de proceso errado. Debe tener 23 dígitos.'
                        );
                    }
                    // Validar que sea numerico
                    if (!is_numeric($value)) {
                        $conError = true;
                        $contadorErrores += 1;
                        array_push(
                            $mensajes,
                            $contadorErrores . '. Número de proceso solo debe tener caracteres numéricos.'
                        );
                    }
                    // Validar datos número de proceso
                    $codigoDepartamento = substr($value,0,2);
                    $codigoCiudad = substr($value,0,5);
                    $codigoJuzgado = substr($value,0,9);
                    $numeroJuzgado = substr($value,5,2);
                    $numeroSala = substr($value,7,2);
                    $numeroDespacho = substr($value,9,3);
                    $anio = +substr($value,12,4);
                    $numeroRadicado = substr($value,16,5);
                    $consecutivo = substr($value,21,2);
                    $departamento = Departamento::findBy($codigoDepartamento);
                    if(!isset($departamento)){
                        $conError = true;
                        $contadorErrores += 1;
                        array_push($mensajes, $contadorErrores . '. Código departamento no válido.');
                    }
                    $ciudad = Ciudad::findBy($codigoCiudad);
                    if(!isset($ciudad)){
                        $conError = true;
                        $contadorErrores += 1;
                        array_push($mensajes, $contadorErrores . '. Código ciudad no válido.');
                    }
                    $juzgado = Juzgado::findBy($codigoJuzgado);
                    if(!isset($juzgado)){
                        $conError = true;
                        $contadorErrores += 1;
                        array_push($mensajes, $contadorErrores . '. Juzgado rama judicial no válido.');
                    }
                    $despacho = null;
                    if(isset($departamento) && isset($ciudad)){
                        $despacho = Despacho::where([
                            'departamento_id' => $departamento->id,
                            'ciudad_id' => $ciudad->id,
                            'juzgado' => $numeroJuzgado,
                            'sala' => $numeroSala,
                            'despacho' => $numeroDespacho
                        ])->first();
                    }
                    if(!isset($despacho)){
                        $conError = true;
                        $contadorErrores += 1;
                        array_push($mensajes, $contadorErrores . '. Número despacho no válido.');
                    }
                    $fechaActual = Carbon::now();
                    $fechaProceso = Carbon::now()->setYear($anio);
                    if($fechaActual->diffInYears($fechaProceso) > 40 || $fechaActual->year < $anio){
                        $conError = true;
                        $contadorErrores += 1;
                        array_push($mensajes, $contadorErrores . '. Año proceso incorrecto.');
                    }
                    if(!(intval($numeroRadicado) >= 1 && intval($numeroRadicado) <= 99999)){
                        $conError = true;
                        $contadorErrores += 1;
                        array_push($mensajes, $contadorErrores . '. Número de radicado incorrecto.');
                    }
                    if(!(intval($consecutivo) >= 0 && intval($consecutivo) <= 5)){
                        $conError = true;
                        $contadorErrores += 1;
                        array_push($mensajes, $contadorErrores . '. Número de consecutivo incorrecto.');
                    }
                    if($conError){
                        $onFailure(join(" ", $mensajes));
                    }
                },
                'nombres_demandantes' => 'string|required',
                'nombres_demandados' => 'string|required',
                'observaciones' => 'string|nullable',
                'abogado_responsable_id' => 'integer|nullable|exists:usuarios,id',
                'ultima_clase_proceso_id' => 'integer|nullable|exists:clases_de_procesos,id',
                'area_id' => 'integer|nullable|exists:areas,id',
            ]);

            if($validator->fails()) {
                return response(
                    get_response_body(format_messages_validator($validator))
                    , Response::HTTP_BAD_REQUEST
                );
            }

            $procesoJudicial = $this->procesoJudicialService->modificarOCrear($datos);
            DB::commit(); // Se cierra la transacción correctamente
            return response(
                get_response_body(["El proceso ha sido modificado."], $procesoJudicial),
                Response::HTTP_OK
            );
        }catch (ModelException $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(get_response_body([$e->getMessage()]), Response::HTTP_CONFLICT);
        }catch (Exception $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction(); // Se abre la transacción
        try{
            $datos['id'] = $id;
            $validator = Validator::make($datos, [
                'id' => 'integer|required|exists:procesos_judiciales,id'
            ]);

            if($validator->fails()) {
                return response(
                    get_response_body(format_messages_validator($validator))
                    , Response::HTTP_BAD_REQUEST
                );
            }

            $eliminado = $this->procesoJudicialService->eliminar($id);
            if($eliminado){
                DB::commit(); // Se cierra la transacción correctamente
                return response(
                    get_response_body(["El proceso ha sido eliminado."]),
                    Response::HTTP_OK
                );
            }else{
                DB::rollback(); // Se devuelven los cambios, por que la transacción falla
                return response(get_response_body(["Ocurrió un error al intentar eliminar el procesoJudicial."]), Response::HTTP_CONFLICT);
            }
        }catch (Exception $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    // Consultas

    /**
     * Obtener reporte de procesos judiciales
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function obtenerProcesosPorRol(Request $request)
    {
        try{
            $datos = $request->all();
            $validator = Validator::make($datos, [
                'limite' => 'integer|between:1,500'
            ]);

            if ($validator->fails()) {
                return response(
                    get_response_body(format_messages_validator($validator))
                    , Response::HTTP_BAD_REQUEST
                );
            }

            if (isset($datos['ordenar_por'])) {
                $datos['ordenar_por'] = format_order_by_attributes($datos);
            }
            $procesosPorRol = $this->procesoJudicialService->obtenerProcesosPorRol($datos);
            return response($procesosPorRol, Response::HTTP_OK);
        }catch(Exception $e){
            return response(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function importar(Request $request){
        DB::beginTransaction(); // Se abre la transacción
        try{
            $archivo = $request->file('file');
            $errores = $this->procesoJudicialService->importar($archivo);
            DB::commit(); // Se cierra la transacción correctamente
            return response(
                get_response_body(["Los datos de los procesos judiciales se han importado."], $errores),
                Response::HTTP_CREATED
            );
        }catch (ModelException $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(get_response_body([$e->getMessage()]), Response::HTTP_CONFLICT);
        }catch (Exception $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(get_response_body(["Revisar archivo de carga. Estructura de información no corresponde."]), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function importarTexto(Request $request){
        DB::beginTransaction(); // Se abre la transacción
        try{
            $errores = $this->procesoJudicialService->importarTexto($request->texto);
            DB::commit(); // Se cierra la transacción correctamente
            return response(
                get_response_body(["Los datos de los procesos judiciales se han importado."], $errores),
                Response::HTTP_CREATED
            );
        }catch (ModelException $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(get_response_body([$e->getMessage()]), Response::HTTP_CONFLICT);
        }catch (Exception $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(get_response_body(["Revisar los datos. Estructura de información no corresponde."]), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function getExcelErrorImportacion(){
        return (new ProcesoJudicialCargaErrorExport())->download('errores_carga_procesos.xlsx');
    }

    public function getValidacionProceso(Request $request) {
        DB::beginTransaction(); // Se abre la transacción
        try{
            // Datos
            $datos = $request->all();

            // Información de sesión
            $user = Auth::user();
            $empresa = $user->empresa();
            $juzgado = null;
            if(isset($datos['juzgado_id'])){
                $juzgado = Juzgado::find($datos['juzgado_id']);
            }

            $validator = Validator::make($datos, [
                'departamento_id' => 'integer|required|exists:departamentos,id',
                'ciudad_id' => 'integer|required|exists:ciudades,id',
                'juzgado_id' => 'integer|required|exists:juzgados,id',
                'despacho_id' => function($attribute, $value, $onFailure) {
                    $conError = false;
                    if(!isset($value)){
                        $conError = true;
                        $mensaje = 'Número juzgado actual no válido.';
                    }
                    if($conError){
                        $onFailure($mensaje);
                    }
                },
                'anio_proceso' => function($attribute, $value, $onFailure) {
                    $conError = false;
                    if(!(isset($value) && trim($value) != "")){
                        $conError = true;
                        $mensaje = 'Número de año es requerido.';
                    }
                    if(!$conError){
                        $fechaActual = Carbon::now();
                        $fechaProceso = Carbon::now()->setYear($value);
                        if($fechaActual->diffInYears($fechaProceso) > 40 || $fechaActual->year < +$value){
                            $conError = true;
                            $mensaje = 'Número de año invalido.';
                        }
                    }
                    if($conError){
                        $onFailure($mensaje);
                    }
                },
                'numero_radicado' => function($attribute, $value, $onFailure) {
                    $conError = false;
                    if(!(isset($value) && trim($value) != "")){
                        $conError = true;
                        $mensaje = 'Número de radicado es requerido.';
                    }
                    if(!$conError && !(intval($value) >= 1 && intval($value) <= 99999)){
                        $conError = true;
                        $mensaje = 'Número de radicado incorrecto.';
                    }

                    if($conError){
                        $onFailure($mensaje);
                    }
                },
                'numero_radicado_proceso_actual' => function($attribute, $value, $onFailure) use($empresa, $juzgado, $datos) {
                    $mensajes = [];
                    $conError = false;
                    $contadorErrores = 0;
                    // Validar longitud de caracteres
                    if (!(isset($value) && strlen($value) == 23)) {
                        $conError = true;
                        $contadorErrores += 1;
                        array_push(
                            $mensajes,
                            $contadorErrores . '. Tamaño de número de proceso errado. Debe tener 23 dígitos.'
                        );
                    }
                    // Validar que sea numerico
                    if (!is_numeric($value)) {
                        $conError = true;
                        $contadorErrores += 1;
                        array_push(
                            $mensajes,
                            $contadorErrores . '. Número de proceso solo debe tener caracteres numéricos.'
                        );
                    }
                    // Validar que no exista en el sistema
                    if(!isset($datos['id'])){
                        $procesoJudicial = ProcesoJudicial::where('numero_radicado_proceso_actual', $value)
                            ->where('juzgado_actual', $juzgado->juzgado_rama ?? null)
                            ->where('empresa_id', $empresa->id)->first();
                        if(isset($procesoJudicial)){
                            $conError = true;
                            array_push($mensajes, 'Proceso ya existe para la empresa.');
                        }
                    }
                    // Validar datos número de proceso
                    $codigoDepartamento = substr($value,0,2);
                    $codigoCiudad = substr($value,0,5);
                    $codigoJuzgado = substr($value,0,9);
                    $numeroJuzgado = substr($value,5,2);
                    $numeroSala = substr($value,7,2);
                    $numeroDespacho = substr($value,9,3);
                    $anio = +substr($value,12,4);
                    $numeroRadicado = substr($value,16,5);
                    $consecutivo = substr($value,21,2);
                    $departamento = Departamento::findBy($codigoDepartamento);
                    if(!isset($departamento)){
                        $conError = true;
                        $contadorErrores += 1;
                        array_push($mensajes, $contadorErrores . '. Código departamento no válido.');
                    }
                    $ciudad = Ciudad::findBy($codigoCiudad);
                    if(!isset($ciudad)){
                        $conError = true;
                        $contadorErrores += 1;
                        array_push($mensajes, $contadorErrores . '. Código ciudad no válido.');
                    }
                    $juzgado = Juzgado::findBy($codigoJuzgado);
                    if(!isset($juzgado)){
                        $conError = true;
                        $contadorErrores += 1;
                        array_push($mensajes, $contadorErrores . '. Juzgado rama judicial no válido.');
                    }
                    $despacho = null;
                    if(isset($departamento) && isset($ciudad)){
                        $despacho = Despacho::where([
                            'departamento_id' => $departamento->id,
                            'ciudad_id' => $ciudad->id,
                            'juzgado' => $numeroJuzgado,
                            'sala' => $numeroSala,
                            'despacho' => $numeroDespacho
                        ])->first();
                    }
                    if(!isset($despacho)){
                        $conError = true;
                        $contadorErrores += 1;
                        array_push($mensajes, $contadorErrores . '. Número despacho no válido.');
                    }
                    $fechaActual = Carbon::now();
                    $fechaProceso = Carbon::now()->setYear($anio);
                    if($fechaActual->diffInYears($fechaProceso) > 40 || $fechaActual->year < $anio){
                        $conError = true;
                        $contadorErrores += 1;
                        array_push($mensajes, $contadorErrores . '. Año proceso incorrecto.');
                    }
                    if(!(intval($numeroRadicado) >= 1 && intval($numeroRadicado) <= 99999)){
                        $conError = true;
                        $contadorErrores += 1;
                        array_push($mensajes, $contadorErrores . '. Número de radicado incorrecto.');
                    }
                    if(!(intval($consecutivo) >= 0 && intval($consecutivo) <= 5)){
                        $conError = true;
                        $contadorErrores += 1;
                        array_push($mensajes, $contadorErrores . '. Número de consecutivo incorrecto.');
                    }

                    if($conError){
                        $onFailure(join(" ", $mensajes));
                    }
                }
            ]);

            if($validator->fails()) {
                return response(
                    get_response_body(format_messages_validator($validator))
                    , Response::HTTP_BAD_REQUEST
                );
            }

            $reporte = $this->procesoJudicialService->validarProcesoRama($request->all());
            DB::commit(); // Se cierra la transacción correctamente
            return response(
                get_response_body(["Validación realizada."], $reporte),
                Response::HTTP_OK
            );
        }catch (ModelException $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(get_response_body([$e->getMessage()]), Response::HTTP_CONFLICT);
        }catch (Exception $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

}

