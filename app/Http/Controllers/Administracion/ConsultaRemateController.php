<?php

namespace App\Http\Controllers\Administracion;

use App\Contracts\Administracion\ConsultaRemateService;
use App\Exports\ConsultaRemateExport;
use App\Model\Administracion\ConsultaRemate;
use Exception;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class ConsultaRemateController extends Controller
{
    protected $consultaRemateService;

    public function __construct(ConsultaRemateService $consultaRemateService){
        $this->consultaRemateService = $consultaRemateService;
    }
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try{
            $datos = $request->all();
            $validator = Validator::make($datos, [
                    'limite' => 'integer|between:1,500'
                ]);

                if ($validator->fails()) {
                    return response(
                        get_response_body(format_messages_validator($validator))
                        , Response::HTTP_BAD_REQUEST
                    );
                }

                if (isset($datos['ordenar_por'])) {
                    $datos['ordenar_por'] = format_order_by_attributes($datos);
                }
                $consultaRemate = $this->consultaRemateService->obtenerColeccion($datos);

            return response($consultaRemate, Response::HTTP_OK);
        }catch(Exception $e){
            return response(get_response_body([$e->getMessage()]), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * Obtener el template para la pantalla de resumen
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndexTemplate()
    {
        return view('consulta-remate.index');
    }

    /**
     * Obtener archivo excel
     * @return Response|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function getExcel(Request $request){
        $dto = [
            'ciudad' => $request->ciudad,
            'juzgado' => $request->juzgado,
            'despacho' => $request->despacho,
            'fecha_inicial' => $request->fecha_inicial,
            'fecha_final' => $request->fecha_final
        ];
        return (new ConsultaRemateExport($dto))->download('Consulta_remates.xlsx');
    }
}

