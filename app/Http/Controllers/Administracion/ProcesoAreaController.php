<?php

namespace App\Http\Controllers\Administracion;

use App\Exports\ProcesoAreaExport;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;


class ProcesoAreaController extends Controller
{
    /**
     * Obtener el template para la pantalla de resumen
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndexTemplate()
    {
        return view('proceso-area.index');
    }


    /**
     * Obtener archivo excel
     * @return Response|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function getExcel(Request $request){
        $dto = [
            'area' => $request->area,
        ];
        return (new ProcesoAreaExport($dto))->download('procesos_por_area' . time() . '.xlsx');
    }
}

