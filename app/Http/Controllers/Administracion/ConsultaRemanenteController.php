<?php

namespace App\Http\Controllers\Administracion;

use App\Contracts\Administracion\ConsultaRemanenteService;
use App\Exports\ConsultaRemanenteExport;
use App\Model\Administracion\ConsultaRemanente;
use Exception;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class ConsultaRemanenteController extends Controller
{
    protected $consultaRemanenteService;

    public function __construct(ConsultaRemanenteService $consultaRemanenteService){
        $this->consultaRemanenteService = $consultaRemanenteService;
    }
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try{
            $datos = $request->all();
            $validator = Validator::make($datos, [
                    'limite' => 'integer|between:1,500'
                ]);

                if ($validator->fails()) {
                    return response(
                        get_response_body(format_messages_validator($validator))
                        , Response::HTTP_BAD_REQUEST
                    );
                }

                if (isset($datos['ordenar_por'])) {
                    $datos['ordenar_por'] = format_order_by_attributes($datos);
                }
                $consultaRemanente = $this->consultaRemanenteService->obtenerColeccion($datos);

            return response($consultaRemanente, Response::HTTP_OK);
        }catch(Exception $e){
            return response(get_response_body([$e->getMessage()]), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * Obtener el template para la pantalla de resumen
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndexTemplate()
    {
        return view('consulta-remanente.index');
    }

    /**
     * Obtener archivo excel
     * @return Response|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function getExcel(Request $request){
        $dto = [
            'ciudad' => $request->ciudad,
            'juzgado' => $request->juzgado,
            'despacho' => $request->despacho,
            'tipo_notificacion' => $request->tipo_notificacion,
            'radicado' => $request->radicado,
            'descripcion_clase_proceso' => $request->descripcion_clase_proceso,
            'detalle' => $request->detalle,
            'demandante' => $request->demandante,
            'demandado' => $request->demandado,
            'fecha_inicial' => $request->fecha_inicial,
            'fecha_final' => $request->fecha_final
        ];
        return (new ConsultaRemanenteExport($dto))->download('Consulta_remanentes.xlsx');
    }
}

