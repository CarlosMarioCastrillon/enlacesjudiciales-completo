<?php

namespace App\Http\Controllers\Administracion;

use App\Contracts\Administracion\ActuacionService;
use App\Exceptions\ModelException;
use App\Exports\ActuacionCargaErrorExport;
use App\Exports\ActuacionPendienteExport;
use App\Exports\ActuacionReporteExport;
use App\Exports\ActuacionReportePorEmpresaExport;
use App\Exports\AudienciaCumplidaExport;
use App\Exports\AudienciaPendienteExport;
use App\Exports\ConsultaPersonalizadoDigitalExport;
use App\Exports\ConsultaRemanenteExport;
use App\Exports\ConsultaRemateExport;
use App\Exports\ProcesoHoyExport;
use App\Model\Administracion\Actuacion;
use Exception;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ActuacionController extends Controller
{
    protected $actuacionService;

    public function __construct(ActuacionService $actuacionService){
        $this->actuacionService = $actuacionService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try{
            $datos = $request->all();
            if(!$request->ligera) {
                $validator = Validator::make($datos, [
                    'limite' => 'integer|between:1,500'
                ]);

                if ($validator->fails()) {
                    return response(
                        get_response_body(format_messages_validator($validator))
                        , Response::HTTP_BAD_REQUEST
                    );
                }
            }

            if($request->ligera){
                $actuaciones = $this->actuacionService->obtenerColeccionLigera($datos);
            }else {
                if (isset($datos['ordenar_por'])) {
                    $datos['ordenar_por'] = format_order_by_attributes($datos);
                }
                $actuaciones = $this->actuacionService->obtenerColeccion($datos);
            }
            return response($actuaciones, Response::HTTP_OK);
        }catch(Exception $e){
            return response(get_response_body([$e->getMessage()]), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('actuacion.form');
    }

    /**
     * Obtener el template para la pantalla de resumen
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndexTemplate()
    {
        return view('actuacion.index');
    }

    /**
     * Obtener el template para la pantalla de importacion
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getImportarTemplate()
    {
        return view('actuacion.importar');
    }

    /**
     * Obtener el template para la pantalla de remates
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getRemateTemplate()
    {
        return view('actuacion.remate');
    }

    /**
     * Obtener el template para la pantalla de remanentes
     *
     * @return \Illuminate\Http\Response
     */
    public function getRemanenteTemplate()
    {
        return view('actuacion.remanente');
    }

    /**
     * Obtener el template para la pantalla de consulta de demandantes y demandados
     *
     * @return \Illuminate\Http\Response
     */
    public function getDemandanteDemandadoTemplate()
    {
        return view('actuacion.demandante-demandado');
    }

    /**
     * Obtener el template para la pantalla de consulta de procesos juzgado por fechas
     *
     * @return \Illuminate\Http\Response
     */
    public function getProcesoJuzgadoFechaTemplate()
    {
        return view('actuacion.proceso-juzgado-fecha');
    }

    /**
     * Obtener el template para la pantalla de procesos hoy
     *
     * @return \Illuminate\Http\Response
     */
    public function getProcesoHoyTemplate()
    {
        return view('actuacion.proceso-hoy');
    }

    /**
     * Obtener el template para la pantalla de autos
     *
     * @return \Illuminate\Http\Response
     */
    public function getAutoTemplate()
    {
        return view('actuacion.auto');
    }

    /**
     * Obtener el template para la pantalla de consulta personalizado digital
     *
     * @return \Illuminate\Http\Response
     */
    public function getPersonalizadoDigitalTemplate()
    {
        return view('actuacion.personalizado-digital');
    }

    /**
     * Obtener el template para la pantalla de reporte judicial
     *
     * @return \Illuminate\Http\Response
     */
    public function getReporteJudicialTemplate()
    {
        return view('actuacion.reporte-judicial');
    }

    /**
     * Obtener el template para la pantalla de audiencias pendientes
     *
     * @return \Illuminate\Http\Response
     */
    public function getAudienciaPendienteTemplate()
    {
        return view('actuacion.audiencia-pendiente');
    }

    /**
     * Obtener el template para la pantalla de audiencias cumplidas
     *
     * @return \Illuminate\Http\Response
     */
    public function getAudienciaCumplidaTemplate()
    {
        return view('actuacion.audiencia-cumplida');
    }

    /**
     * Obtener el template para la pantalla de términos de actuación
     *
     * @return \Illuminate\Http\Response
     */
    public function getTerminoActuacionTemplate()
    {
        return view('actuacion.termino-actuacion');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction(); // Se abre la transacción
        try{
            $datos = $request->all();
            $validator = Validator::make($datos, [
                'numero_proceso' => 'string|required|min:23|max:23',
                'actuacion' => 'string|required|min:4|max:191',
                'anotacion' => 'string|required|min:4|max:191',
                'fecha_registro' => 'date|required',
                'fecha_actuacion' => 'date|required',
                'fecha_inicio_termino' => 'date',
                'fecha_vencimiento_termino' => 'date',
                'ind_actuacion_audiencia' => 'string|max:1',
                'tipo_movimiento' => 'string|max:2',
                'nombres_demandantes' => 'string|required|min:2|max:191',
                'nombres_demandados' => 'string|required|min:2|max:191',
                'ciudad_id' => 'integer|required',
                'juzgado_id' => 'integer|required',
                'juzgado_origen_id' => 'integer|required',
                'despacho_id' => 'integer|required',
                'despacho_origen_id' => 'integer|required',
                'empresa_id' => 'integer|required',
                'etapa_proceso_id' => 'integer',
                'tipo_actuacion_id' => 'integer',
                'tipo_notificacion_id' => 'integer'
            ]);

            if($validator->fails()) {
                return response(
                    get_response_body(format_messages_validator($validator))
                    , Response::HTTP_BAD_REQUEST
                );
            }

            $actuacion = $this->actuacionService->modificarOCrear($datos);
            if(isset($actuacion)){
                DB::commit(); // Se cierra la transacción correctamente
                return response(
                    get_response_body(["La actuación ha sido creada."], $actuacion),
                    Response::HTTP_CREATED
                );
            } else {
                DB::rollback(); // Se devuelven los cambios, por que la transacción falla
                return response(get_response_body(["Ocurrió un error al intentar crear la actuacion."]), Response::HTTP_CONFLICT);
            }
        }catch (ModelException $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(null, Response::HTTP_CONFLICT);
        }catch (Exception $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $datos['id'] = $id;
            $validator = Validator::make($datos, [
                'id' => 'integer|required|exists:actuaciones,id'
            ]);

            if($validator->fails()) {
                return response(
                    get_response_body(format_messages_validator($validator))
                    , Response::HTTP_BAD_REQUEST
                );
            }

            return response($this->actuacionService->cargar($id), Response::HTTP_OK);
        }catch (Exception $e){
            return response(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction(); // Se abre la transacción
        try{
            $datos = $request->all();
            $datos['id'] = $id;
            $validator = Validator::make($datos, [
                'id' => 'integer|required|exists:actuaciones,id',
                'ciudad_id' => 'integer|required|exists:ciudades,id',
                'juzgado_id' => 'integer|required|exists:juzgados,id',
                'despacho_id' => 'integer|required|exists:despachos,id',
                'anotacion' => 'string|nullable|min:4|max:4000',
                'fecha_registro' => 'date|required',
                'fecha_actuacion' => 'date|required',
                'fecha_inicio_termino' => 'date|nullable',
                'fecha_vencimiento_termino' => 'date|nullable',
                'nombres_demandantes' => 'string|required|min:2|max:191',
                'nombres_demandados' => 'string|required|min:2|max:191',
                'descripcion_clase_proceso' => 'string|nullable|min:2|max:191',
                'tipo_notificacion_id' => 'integer|nullable|exists:tipos_notificaciones,id'
            ]);

            if($validator->fails()) {
                return response(
                    get_response_body(format_messages_validator($validator))
                    , Response::HTTP_BAD_REQUEST
                );
            }

            $actuacion = $this->actuacionService->modificarOCrear($datos);
            if(isset($actuacion)){
                DB::commit(); // Se cierra la transacción correctamente
                return response(
                    get_response_body(["La actuación ha sido modificada."], $actuacion),
                    Response::HTTP_OK
                );
            } else {
                DB::rollback(); // Se devuelven los cambios, por que la transacción falla
                return response(get_response_body(["Ocurrió un error al intentar modificar la actuación."]), Response::HTTP_CONFLICT);
            }
        }catch (ModelException $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(null, Response::HTTP_CONFLICT);
        }catch (Exception $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Modificar masivamente las actuaciones
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function updateMasivo(Request $request)
    {
        DB::beginTransaction(); // Se abre la transacción
        try{
            $datos = $request->all();
            $validator = Validator::make($datos, [
                'ciudad_id' => 'integer|required|exists:ciudades,id',
                'juzgado_id' => 'integer|required|exists:juzgados,id',
                'despacho_id' => 'integer|required|exists:despachos,id',
                'tipo_notificacion_id' => 'integer|nullable|exists:tipos_notificaciones,id'
            ]);

            if($validator->fails()) {
                return response(
                    get_response_body(format_messages_validator($validator))
                    , Response::HTTP_BAD_REQUEST
                );
            }

            $actuacion = $this->actuacionService->modificarMasivo($datos);
            if(isset($actuacion)){
                DB::commit(); // Se cierra la transacción correctamente
                return response(
                    get_response_body(["Las actuaciones han sido modificadas."], $actuacion),
                    Response::HTTP_OK
                );
            } else {
                DB::rollback(); // Se devuelven los cambios, por que la transacción falla
                return response(get_response_body(["Ocurrió un error al intentar modificar las actuaciones."]), Response::HTTP_CONFLICT);
            }
        }catch (ModelException $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(null, Response::HTTP_CONFLICT);
        }catch (Exception $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function modificarEstado (Request $request, $id)
    {
        DB::beginTransaction(); // Se abre la transacción
        try{
            $datos = $request->all();
            $validator = Validator::make($datos, [
                '*' => 'integer|required|exists:actuaciones,id',
            ]);

            if($validator->fails()) {
                return response(
                    get_response_body(format_messages_validator($validator))
                    , Response::HTTP_BAD_REQUEST
                );
            }

            foreach ($datos as $actuacionId) {
                $actuacion = $this->actuacionService->modificarOCrear([
                    'id' => $actuacionId,
                    'estado' => 1
                ]);
            }
            if(isset($actuacion)){
                DB::commit(); // Se cierra la transacción correctamente
                return response(
                    get_response_body(["El proceso ha sido modificado."], $actuacion),
                    Response::HTTP_OK
                );
            } else {
                DB::rollback(); // Se devuelven los cambios, por que la transacción falla
                return response(get_response_body(["Ocurrió un error al intentar modificar el proceso."]), Response::HTTP_CONFLICT);
            }
        }catch (ModelException $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(null, Response::HTTP_CONFLICT);
        }catch (Exception $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction(); // Se abre la transacción
        try{
            $datos['id'] = $id;
            $validator = Validator::make($datos, [
                'id' => 'integer|required|exists:actuaciones,id'
            ]);

            if($validator->fails()) {
                return response(
                    get_response_body(format_messages_validator($validator))
                    , Response::HTTP_BAD_REQUEST
                );
            }

            $eliminado = $this->actuacionService->eliminar($id);
            if($eliminado){
                DB::commit(); // Se cierra la transacción correctamente
                return response(
                    get_response_body(["La actuación ha sido eliminada."]),
                    Response::HTTP_OK
                );
            }else{
                DB::rollback(); // Se devuelven los cambios, por que la transacción falla
                return response(get_response_body(["Ocurrió un error al intentar eliminar la actuacion."]), Response::HTTP_CONFLICT);
            }
        }catch (Exception $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyMasivo(Request $request)
    {
        DB::beginTransaction(); // Se abre la transacción
        try{
            $datos = $request->all();
            $eliminado = $this->actuacionService->eliminarMasivo($datos);
            if($eliminado){
                DB::commit(); // Se cierra la transacción correctamente
                return response(
                    get_response_body(["Las actuaciones han sido eliminadas."]),
                    Response::HTTP_OK
                );
            }else{
                DB::rollback(); // Se devuelven los cambios, por que la transacción falla
                return response(get_response_body(["Ocurrió un error al intentar eliminar la actuaciones."]), Response::HTTP_CONFLICT);
            }
        }catch (Exception $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    // Consultas

    /**
     * Obtener reporte de actuaciones
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function obtenerReporte(Request $request)
    {
        try{
            $datos = $request->all();
            $validator = Validator::make($datos, [
                'limite' => 'integer|between:1,500'
            ]);

            if ($validator->fails()) {
                return response(
                    get_response_body(format_messages_validator($validator))
                    , Response::HTTP_BAD_REQUEST
                );
            }

            if (isset($datos['ordenar_por'])) {
                $datos['ordenar_por'] = format_order_by_attributes($datos);
            }
            $actuacionesExpedientes = $this->actuacionService->obtenerReporte($datos);
            return response($actuacionesExpedientes, Response::HTTP_OK);
        }catch(Exception $e){
            return response(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Obtener reporte de actuaciones
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function obtenerReportePorEmpresa(Request $request)
    {
        try{
            $datos = $request->all();
            $validator = Validator::make($datos, [
                'limite' => 'integer|between:1,500'
            ]);

            if ($validator->fails()) {
                return response(
                    get_response_body(format_messages_validator($validator))
                    , Response::HTTP_BAD_REQUEST
                );
            }

            if (isset($datos['ordenar_por'])) {
                $datos['ordenar_por'] = format_order_by_attributes($datos);
            }
            $actuaciones = $this->actuacionService->obtenerReportePorEmpresa($datos);
            return response($actuaciones, Response::HTTP_OK);
        }catch(Exception $e){
            return response(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Obtner la colecciones de ramates
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function obtenerRemates(Request $request)
    {
        try{
            $datos = $request->all();
            $validator = Validator::make($datos, [
                'limite' => 'integer|between:1,500'
            ]);

            if ($validator->fails()) {
                return response(
                    get_response_body(format_messages_validator($validator))
                    , Response::HTTP_BAD_REQUEST
                );
            }

            if (isset($datos['ordenar_por'])) {
                $datos['ordenar_por'] = format_order_by_attributes($datos);
            }
            $remates = $this->actuacionService->obtenerRemates($datos);
            return response($remates, Response::HTTP_OK);
        }catch(Exception $e){
            return response(get_response_body([$e->getMessage()]), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Obtner la colecciones de remanentes
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function obtenerRemanentes(Request $request)
    {
        try{
            $datos = $request->all();
            $validator = Validator::make($datos, [
                'limite' => 'integer|between:1,500'
            ]);

            if ($validator->fails()) {
                return response(
                    get_response_body(format_messages_validator($validator))
                    , Response::HTTP_BAD_REQUEST
                );
            }

            if (isset($datos['ordenar_por'])) {
                $datos['ordenar_por'] = format_order_by_attributes($datos);
            }
            $remates = $this->actuacionService->obtenerRemanentes($datos);
            return response($remates, Response::HTTP_OK);
        }catch(Exception $e){
            return response(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Obtner las audiencias pendientes
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function obtenerAudienciasPendientes(Request $request)
    {
        try{
            $datos = $request->all();
            $validator = Validator::make($datos, [
                'limite' => 'integer|between:1,500'
            ]);

            if ($validator->fails()) {
                return response(
                    get_response_body(format_messages_validator($validator))
                    , Response::HTTP_BAD_REQUEST
                );
            }

            if (isset($datos['ordenar_por'])) {
                $datos['ordenar_por'] = format_order_by_attributes($datos);
            }
            $audienciasPendientes = $this->actuacionService->obtenerAudienciasPendientes($datos);
            return response($audienciasPendientes, Response::HTTP_OK);
        }catch(Exception $e){
            return response(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Obtner las audiencias cumplidas
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function obtenerAudienciasCumplidas(Request $request)
    {
        try{
            $datos = $request->all();
            $validator = Validator::make($datos, [
                'limite' => 'integer|between:1,500'
            ]);

            if ($validator->fails()) {
                return response(
                    get_response_body(format_messages_validator($validator))
                    , Response::HTTP_BAD_REQUEST
                );
            }

            if (isset($datos['ordenar_por'])) {
                $datos['ordenar_por'] = format_order_by_attributes($datos);
            }
            $audienciasCumplidas = $this->actuacionService->obtenerAudienciasCumplidas($datos);
            return response($audienciasCumplidas, Response::HTTP_OK);
        }catch(Exception $e){
            return response(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Obtener las actuaciones pendientes y cumplidas
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function obtenerActuacionesPendientesYCumplidas(Request $request)
    {
        try{
            $datos = $request->all();
            $validator = Validator::make($datos, [
                'limite' => 'integer|between:1,500'
            ]);

            if ($validator->fails()) {
                return response(
                    get_response_body(format_messages_validator($validator))
                    , Response::HTTP_BAD_REQUEST
                );
            }

            if (isset($datos['ordenar_por'])) {
                $datos['ordenar_por'] = format_order_by_attributes($datos);
            }
            $movimientos = $this->actuacionService->obtenerActuacionesPendientesYCumplidas($datos);
            return response($movimientos, Response::HTTP_OK);
        }catch(Exception $e){
            return response(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    // Archivos excel

    /**
     * Obtener archivo excel de remate
     * @param Request $request
     * @return Response|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function obtenerRematesExcel(Request $request){
        $dto = [
            'ciudad' => $request->ciudad,
            'juzgado' => $request->juzgado,
            'despacho' => $request->despacho,
            'fecha_inicial' => $request->fecha_inicial,
            'fecha_final' => $request->fecha_final
        ];
        return (new ConsultaRemateExport($dto))->download('consulta-remates-' . time() . '.xlsx');
    }

    /**
     * Obtener archivo excel de remanentes
     * @param Request $request
     * @return Response|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function obtenerRemanentesExcel(Request $request){
        $dto = [
            'ciudad' => $request->ciudad,
            'juzgado' => $request->juzgado,
            'despacho' => $request->despacho,
            'tipo_notificacion' => $request->tipo_notificacion,
            'radicado' => $request->radicado,
            'descripcion_clase_proceso' => $request->descripcion_clase_proceso,
            'detalle' => $request->detalle,
            'demandante' => $request->demandante,
            'demandado' => $request->demandado,
            'fecha_inicial' => $request->fecha_inicial,
            'fecha_final' => $request->fecha_final
        ];
        return (new ConsultaRemanenteExport($dto))->download('consulta-remanentes-' . time() . '.xlsx');
    }

    /**
     * Obtener archivo excel de demandantes y demandados
     * @return Response|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function obtenerDemandanteDemandadoExcel(Request $request){
        $dto = [
            'ciudad' => $request->ciudad,
            'demandante' => $request->demandante,
            'demandado' => $request->demandado,
            'proceso' => $request->proceso,
        ];
        return (new ActuacionReporteExport($dto))->download('demandante-demandado' . time() . '.xlsx');
    }

    /**
     * Obtener archivo excel de procesos juzgado por fechas
     * @return Response|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function obtenerProcesoJuzgadoFechaExcel(Request $request){
        $dto = [
            'fecha' => $request->fecha,
            'ciudad' => $request->ciudad,
            'juzgado' => $request->juzgado,
            'despacho' => $request->despacho,
            'proceso' => $request->proceso,
        ];
        return (new ActuacionReporteExport($dto))->download('procesos-juzgado-fecha' . time() . '.xlsx');
    }

    /**
     * Obtener archivo excel procesos hoy
     * @return Response|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function obtenerProcesoHoyExcel(Request $request){
        $dto = [
            'fecha_inicial' => $request->fecha_inicial,
            'fecha_final' => $request->fecha_final,
        ];
        return (new ProcesoHoyExport($dto))->download('procesos-hoy-' . time() . '.xlsx');
    }

    /**
     * Obtener archivo excel autos
     * @return Response|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function obtenerAutoExcel(Request $request){
        $dto = [
            'fecha_inicial' => $request->fecha_inicial,
            'fecha_final' => $request->fecha_final,
            'url' => $request->url,
            'tipo_movimiento' => $request->tipo_movimiento
        ];
        return (new ActuacionReportePorEmpresaExport($dto))->download('consulta-autos-' . time() . '.xlsx');
    }

    public function obtenerPersonalizadoDigitalExcel(Request $request){
        $dto = [
            'ciudad' => $request->ciudad,
            'juzgado' => $request->juzgado,
            'despacho' => $request->despacho,
            'tipo_notificacion' => $request->tipo_notificacion,
            'radicado' => $request->radicado,
            'descripcion_clase_proceso' => $request->descripcion_clase_proceso,
            'detalle' => $request->detalle,
            'fecha_inicial' => $request->fecha_inicial,
            'fecha_final' => $request->fecha_final
        ];
        return (new ConsultaPersonalizadoDigitalExport($dto))->download('consulta-personalizado-digital-' . time() . '.xlsx');
    }

    public function obtenerReporteDigitalExcel(Request $request){
        $dto = [
            'ciudad' => $request->ciudad,
            'juzgado' => $request->juzgado,
            'despacho' => $request->despacho,
            'tipo_notificacion' => $request->tipo_notificacion,
            'fecha_inicial' => $request->fecha_inicial,
            'fecha_final' => $request->fecha_final
        ];
        return (new ReporteJudicialExport($dto))->download('reporte-judicial-' . time() . '.xlsx');
    }

    public function obtenerAudienciaPendienteExcel(Request $request){
        $dto = [
            'proceso' => $request->proceso,
            'actuacion' => $request->actuacion,
            'demandante' => $request->demandante,
            'demandado' => $request->demandado,
        ];
        return (new AudienciaPendienteExport($dto))->download('audiencia-pendiente-' . time() . '.xlsx');
    }

    public function obtenerAudienciaCumplidaExcel(Request $request){
        $dto = [
            'proceso' => $request->proceso,
            'actuacion' => $request->actuacion,
            'demandante' => $request->demandante,
            'demandado' => $request->demandado,
        ];
        return (new AudienciaCumplidaExport($dto))->download('audiencia-cumplida-' . time() . '.xlsx');
    }

    public function obtenerActuacionesPendientesExcel(Request $request){
        if($request->cumplimiento == "true"){
            $nombreArchivo = 'actuaciones-cumplidas-' . time() . '.xlsx';
        }else{
            $nombreArchivo = 'actuaciones-pendientes-' . time() . '.xlsx';
        }
        return (new ActuacionPendienteExport($request->all()))->download($nombreArchivo);
    }

    public function modificarActuacionesPendientes(Request $request){
        DB::beginTransaction(); // Se abre la transacción
        try{
            $datos = $request->all();
            foreach ($datos as $id){
                $this->actuacionService->modificarOCrear([
                    'id' => $id,
                    'estado' => 1
                ]);
            }
            DB::commit(); // Se cierra la transacción correctamente
            return response(
                get_response_body(["La actuaciones han sido pasadas al histórico."]),
                Response::HTTP_OK
            );
        }catch (ModelException $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(null, Response::HTTP_CONFLICT);
        }catch (Exception $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    // Achivos anexos

    /**
     * Obtener el archivo anexo de la actuación
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function obtenerArchivoAnexo($id){
        try{
            $user = Auth::user();
            $empresa = $user->empresa();

            $actuacion = Actuacion::find($id);
            if(!isset($actuacion->ruta_archivo_anexo)){
                return response(null, Response::HTTP_NOT_FOUND);
            }

            // TODO Validar con Carlos Mario, si a los superusuarios se les permite
            // TODO descargar actuaciones de otras empresas
            if($actuacion->empresa_id != $empresa->id){
                return response(null, Response::HTTP_FORBIDDEN);
            }

            $mime = Storage::disk('s3')->getDriver()->getMimetype($actuacion->ruta_archivo_anexo);
            $size = Storage::disk('s3')->getDriver()->getSize($actuacion->ruta_archivo_anexo);

            $response =  [
                'Content-Type' => $mime,
                'Content-Length' => $size,
                'Content-Disposition' => "attachment; filename=" .
                    str_replace(",", "", str_replace(";", "", $actuacion->nombre_archivo_anexo))
            ];

            return \Response::make(Storage::disk('s3')->get($actuacion->ruta_archivo_anexo), Response::HTTP_OK, $response);
        }catch(FileNotFoundException $e){
            return response($e->getMessage(), Response::HTTP_NOT_FOUND);
        }catch(Exception $e){
            return response($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Obtener el archivo anexo de la actuación
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function obtenerArchivoEstadoCartelera($id){
        try{
            $user = Auth::user();
            $empresa = $user->empresa();

            $actuacion = Actuacion::find($id);
            if(!isset($actuacion->ruta_archivo_estado_cartelera)){
                return response(null, Response::HTTP_NOT_FOUND);
            }

            // TODO Validar con Carlos Mario, si a los superusuarios se les permite
            // TODO descargar actuaciones de otras empresas
            if($actuacion->empresa_id != $empresa->id){
                return response(null, Response::HTTP_FORBIDDEN);
            }

            $mime = Storage::disk('s3')->getDriver()->getMimetype($actuacion->ruta_archivo_estado_cartelera);
            $size = Storage::disk('s3')->getDriver()->getSize($actuacion->ruta_archivo_estado_cartelera);

            $response =  [
                'Content-Type' => $mime,
                'Content-Length' => $size,
                'Content-Disposition' => "attachment; filename=" .
                    str_replace(",", "", str_replace(";", "", $actuacion->nombre_archivo_estado_cartelera))
            ];

            return \Response::make(Storage::disk('s3')->get($actuacion->ruta_archivo_anexo), Response::HTTP_OK, $response);
        }catch(FileNotFoundException $e){
            return response($e->getMessage(), Response::HTTP_NOT_FOUND);
        }catch(Exception $e){
            return response($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    // Importación de datos

    public function importar(Request $request){
        DB::beginTransaction(); // Se abre la transacción
        try{
            $archivo = $request->file('file');
            $errores = $this->actuacionService->importar($archivo);
            DB::commit(); // Se cierra la transacción correctamente
            return response(
                get_response_body(["Los datos de las actuaciones se han importado."], $errores),
                Response::HTTP_CREATED
            );
        }catch (ModelException $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(get_response_body([$e->getMessage()]), Response::HTTP_CONFLICT);
        }catch (Exception $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(get_response_body(["Revisar archivo de carga. Estructura de información no corresponde."]), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function importarTexto(Request $request){
        DB::beginTransaction(); // Se abre la transacción
        try{
            $errores = $this->actuacionService->importarTexto($request->texto);
            DB::commit(); // Se cierra la transacción correctamente
            return response(
                get_response_body(["Los datos de las actuaciones se han importado."], $errores),
                Response::HTTP_CREATED
            );
        }catch (ModelException $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(get_response_body([$e->getMessage()]), Response::HTTP_CONFLICT);
        }catch (Exception $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(get_response_body(["Revisar los datos. Estructura de información no corresponde."]), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function getExcelErrorImportacion(){
        return (new ActuacionCargaErrorExport())->download('errores_carga_actuaciones.xlsx');
    }

}

