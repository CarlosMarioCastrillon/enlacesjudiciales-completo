<?php

namespace App\Http\Controllers\Administracion;

use App\Contracts\Administracion\AuditoriaProcesoService;
use App\Exceptions\ModelException;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class AuditoriaProcesoController extends Controller
{
    protected $auditoriaProcesoService;

    public function __construct(AuditoriaProcesoService $auditoriaProcesoService){
        $this->auditoriaProcesoService = $auditoriaProcesoService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try{
            $datos = $request->all();
            $validator = Validator::make($datos, [
                'limite' => 'integer|between:1,500'
            ]);
            if ($validator->fails()) {
                return response(
                    get_response_body(format_messages_validator($validator))
                    , Response::HTTP_BAD_REQUEST
                );
            }
            if (isset($datos['ordenar_por'])) {
                $datos['ordenar_por'] = format_order_by_attributes($datos);
            }
            $programaciones = $this->auditoriaProcesoService->getColeccion($datos);

            return response($programaciones, Response::HTTP_OK);
        }catch(Exception $e){
            return response(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Obtener archivo excel de auditorias
     * @return Response|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function getExcelAuditorias(Request $request){
        $excel = $this->auditoriaProcesoService->getExcelAuditorias($request->programacion_id);
        return $excel->download('auditorias-procesos-' . time() . '.xlsx');
    }

    /**
     * Obtener archivo excel
     * @return Response|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function getExcelNovedades(Request $request){
        $excel = $this->auditoriaProcesoService->getExcelNovedades($request->programacion_id);
        return $excel->download('novedades-procesos-' . time() . '.xlsx');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Obtener el template para la pantalla de resumen
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndexTemplate()
    {
        return view('auditoria-proceso.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction(); // Se abre la transacción
        try{
            $mensaje = "";
            $datos = $request->all();

            // Cargar archivo
            $archivo = $request->file('file');
            if(isset($archivo)){
                $filasImportadas = $this->auditoriaProcesoService->cargarProcesos($archivo);
                $mensaje = "Se han cargado " . $filasImportadas . " procesos. ";
            }

            // Importar procesos
            if(isset($request->fecha_programacion)){
                $this->auditoriaProcesoService->programar($datos);
                $mensaje .= "Se ha programado la ejecución de una nueva auditoría.";
            }

            DB::commit(); // Se cierra la transacción correctamente
            return response(
                get_response_body([$mensaje]),
                Response::HTTP_CREATED
            );
        }catch (ModelException $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(null, Response::HTTP_CONFLICT);
        }catch (Exception $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

