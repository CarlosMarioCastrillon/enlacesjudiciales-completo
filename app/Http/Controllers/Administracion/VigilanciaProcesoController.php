<?php

namespace App\Http\Controllers\Administracion;

use App\Exports\VigilanciaProcesoExport;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class VigilanciaProcesoController extends Controller
{

    /**
     * Obtener el template para la pantalla de resumen
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndexTemplate()
    {
        return view('vigilancia-proceso.index');
    }


    /**
     * Obtener archivo excel
     * @return Response|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function getExcel(Request $request){
        $dto = [
            'estado' => $request->estado,
            'numero_proceso' => $request->numero_proceso,
            'demandante' => $request->demandante,
            'demandado' => $request->demandado
        ];
        return (new VigilanciaProcesoExport($dto))->download('procesos_en_vigilancia' . time() . '.xlsx');
    }
}

