<?php

namespace App\Http\Controllers\Administracion;

use App\Contracts\Administracion\ActuacionExpedienteService;
use App\Exports\ActuacionExpedienteExport;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;


class ProcesoJuzgadoFechaController extends Controller
{
    protected $procesoJuzgadoFechaService;

    public function __construct(ActuacionExpedienteService $procesoJuzgadoFechaService){
        $this->procesoJuzgadoFechaService = $procesoJuzgadoFechaService;
    }
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try{
            $datos = $request->all();
            $validator = Validator::make($datos, [
                'limite' => 'integer|between:1,500'
            ]);

            if ($validator->fails()) {
                return response(
                    get_response_body(format_messages_validator($validator))
                    , Response::HTTP_BAD_REQUEST
                );
            }

            if (isset($datos['ordenar_por'])) {
                $datos['ordenar_por'] = format_order_by_attributes($datos);
            }
            $procesosJuzgadoFecha = $this->procesoJuzgadoFechaService->obtenerColeccion($datos);

            return response($procesosJuzgadoFecha, Response::HTTP_OK);
        }catch(Exception $e){
            return response(get_response_body([$e->getMessage()]), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * Obtener el template para la pantalla de resumen
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndexTemplate()
    {
        return view('proceso-juzgado-fecha.index');
    }


    /**
     * Obtener archivo excel
     * @return Response|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function getExcel(Request $request){
        $dto = [
            'fecha' => $request->fecha,
            'ciudad' => $request->ciudad,
            'juzgado' => $request->juzgado,
            'despacho' => $request->despacho,
            'proceso' => $request->proceso,
        ];
        return (new ActuacionExpedienteExport($dto))->download('procesos_juzgado_fecha.xlsx');
    }
}

