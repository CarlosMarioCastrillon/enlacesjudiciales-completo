<?php

namespace App\Http\Controllers\Importacion;

use App\Contracts\Importacion\AuxiliarJusticiaCargaErrorService;
use App\Exceptions\ModelException;
use App\Exports\AuxiliarJusticiaCargaErrorExport;
use App\Imports\AuxiliarJusticiaImport;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class AuxiliarJusticiaCargaErrorController extends Controller
{

    protected $auxiliarJusticiaCargaErrorService;

    public function __construct(AuxiliarJusticiaCargaErrorService $auxiliarJusticiaCargaErrorService){
        $this->auxiliarJusticiaCargaErrorService = $auxiliarJusticiaCargaErrorService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try{
            $datos = $request->all();
            if(!($request->ligera)) {
                $validator = Validator::make($datos, [
                    'limite' => 'integer|between:1,500'
                ]);

                if ($validator->fails()) {
                    return response(
                        get_response_body(format_messages_validator($validator))
                        , Response::HTTP_BAD_REQUEST
                    );
                }
            }
            if($request->ligera){
                $auxiliaresJusticiaCargaError = $this->auxiliarJusticiaCargaErrorService->obtenerColeccionLigera($datos);
            }else {
                if (isset($datos['ordenar_por'])) {
                    $datos['ordenar_por'] = format_order_by_attributes($datos);
                }
                $auxiliaresJusticiaCargaError = $this->auxiliarJusticiaCargaErrorService->obtenerColeccion($datos);
            }
            return response($auxiliaresJusticiaCargaError, Response::HTTP_OK);
        }catch(Exception $e){
            return response(get_response_body([$e->getMessage()]), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * subir archivo excel
     * @return Response|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function storeExcel(Request $request){
        Excel::import(new AuxiliarJusticiaImport, request()->file('AUXILIARES DE LA JUSTICIA - VERSION 2013.xls'));
        return redirect('/auxiliares-de-la-justicia')->with('success', 'All good!');
    }


    /**
     * Obtener el template para la pantalla de resumen
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndexTemplate()
    {
        return view('auxiliar-justicia.index');
    }

    /**
     * Obtener el template para la pantalla de resumen
     *
     * @return \Illuminate\Http\Response
     */
    public function getImportarTemplate()
    {
        return view('auxiliar-justicia.importar');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function importar(Request $request){
        DB::beginTransaction(); // Se abre la transacción
        try{
            $datos = $request->all();
            $validator = Validator::make($datos, [
                'archivo_nombre' => 'string|required',
                'archivo_base_64' => 'string|required'
            ]);

            if($validator->fails()) {
                return response(
                    get_response_body(format_messages_validator($validator))
                    , Response::HTTP_BAD_REQUEST
                );
            }

            $errores = $this->auxiliarJusticiaCargaErrorService->importar($datos);
            DB::commit(); // Se cierra la transacción correctamente
            return response(
                get_response_body(["Los datos de los auxiliares de la justicias se han importado."], $errores),
                Response::HTTP_CREATED
            );
        }catch (ModelException $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response(get_response_body([$e->getMessage()]), Response::HTTP_CONFLICT);
        }catch (Exception $e){
            DB::rollback(); // Se devuelven los cambios, por que la transacción falla
            return response($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function getExcel(){

        return (new AuxiliarJusticiaCargaErrorExport())->download('busqueda_demandante_demandado.xlsx');
    }
}
