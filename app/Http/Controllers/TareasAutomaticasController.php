<?php

namespace App\Http\Controllers;

use App\Contracts\Administracion\ActuacionService;
use App\Contracts\Administracion\AuditoriaProcesoService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TareasAutomaticasController extends Controller
{

    protected $actuacionService;
    protected $auditoriaProcesoService;

    public function __construct(
        AuditoriaProcesoService $auditoriaProcesoService, ActuacionService $actuacionService
    ){
        $this->actuacionService = $actuacionService;
        $this->auditoriaProcesoService = $auditoriaProcesoService;
    }

    /**
     * Ejecutar proceso semanal de generación de preventas de fincas
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function lanzarAuditoriaProcesos(Request $request)
    {
        $this->auditoriaProcesoService->lanzarAuditoria();
        return response(null, Response::HTTP_OK);
    }

    /**
     * Ejecutar proceso semanal de generación de preventas de fincas
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function lanzarConsultaActuaciones(Request $request)
    {
        $this->actuacionService->lanzarLecturaActuaciones();
        return response(null, Response::HTTP_OK);
    }

}
