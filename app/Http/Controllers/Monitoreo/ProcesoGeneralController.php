<?php

namespace App\Http\Controllers\Monitoreo;

use App\Contracts\Monitoreo\ProcesoPorRolService;
use App\Exports\ProcesoPorRolExport;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class ProcesoGeneralController extends Controller
{
    protected $procesoGeneralService;

    public function __construct(ProcesoPorRolService $procesoGeneralService){
        $this->procesoGeneralService = $procesoGeneralService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try{
            $datos = $request->all();
            if(!($request->ligera)) {
                $validator = Validator::make($datos, [
                    'limite' => 'integer|between:1,500'
                ]);

                if ($validator->fails()) {
                    return response(
                        get_response_body(format_messages_validator($validator))
                        , Response::HTTP_BAD_REQUEST
                    );
                }
            }
            if($request->ligera){
                $procesosGenerales = $this->procesoGeneralService->obtenerColeccionLigera($datos);
            }else {
                if (isset($datos['ordenar_por'])) {
                    $datos['ordenar_por'] = format_order_by_attributes($datos);
                }
                $procesosGenerales = $this->procesoGeneralService->obtenerColeccion($datos);
            }
            return response($procesosGenerales, Response::HTTP_OK);
        }catch(Exception $e){
            return response(get_response_body([$e->getMessage()]), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * Obtener el template para la pantalla de resumen
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndexTemplate()
    {
        return view('proceso-general.index');
    }


    /**
     * Obtener archivo excel
     * @return Response|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function getExcel(Request $request){
        $dto = [
            'empresa' => $request->empresa,
            'numero_proceso' => $request->numero_proceso,
            'demandante' => $request->demandante,
            'demandado' => $request->demandado
        ];
        return (new ProcesoPorRolExport($dto))->download('Información general de procesos.xlsx');
    }
}

