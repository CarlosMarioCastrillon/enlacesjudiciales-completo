<?php namespace App\Enum;

use ReflectionClass;

class TipoMovimientoEnum
{

    const ACTUACION = "AC";
    const EXPEDIENTE = "EX";
    const REMATE = "RM";
    const VIGILANCIA = "VG";

}
