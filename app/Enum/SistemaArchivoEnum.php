<?php namespace App\Enum;

use ReflectionClass;

class SistemaArchivoEnum
{

    const ANEXO_ACTUACION = "anexo_actuacion";
    const ANEXO_DILIGENCIA = "anexo_diligencia";
    const ANEXO_COSTOS_DILIGENCIA = "anexo_costos_diligencia";
    const ANEXO_RECIBIDO_DILIGENCIA = "anexo_recibido_diligencia";
    const ANEXO_DILIGENCIA_HISTORIA = "anexo_diligencia_historia";
    const ANEXO_COSTOS_DILIGENCIA_HISTORIA = "anexo_costos_diligencia_historia";
    const ANEXO_RECIBIDO_DILIGENCIA_HISTORIA = "anexo_recibido_diligencia_historia";

}
