<?php namespace App\Enum;

use ReflectionClass;

class EstadoDiligenciaEnum
{

    const SOLICITADO = 1;
    const COTIZADO = 2;
    const APROBADO = 3;
    const TRAMITE = 4;
    const TERMINADO = 5;
    const CONTABILIZADO = 6;
    const FACTURADO = 7;

}
