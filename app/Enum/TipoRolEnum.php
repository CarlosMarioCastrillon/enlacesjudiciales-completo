<?php namespace App\Enum;

use ReflectionClass;

class TipoRolEnum
{

    const CLIENTE = 1;
    const SISTEMA = 2;
    const CLIENTE_DE_CLIENTE = 3;

    public static function obtenerOpciones() {
        $oClass = new ReflectionClass(TipoRolEnum::class);
        $constants = $oClass->getConstants();
        return array_values($constants);
    }

}