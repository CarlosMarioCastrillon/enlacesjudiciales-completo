<?php namespace App\Enum;

class TipoSujetoProcesalEnum
{

    const DEMANDANTE = "Demandante";
    const DEMANDADO = "Demandado";

}
