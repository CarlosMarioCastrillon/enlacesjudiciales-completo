<?php namespace App\Enum;

class TipoNovedadEnum
{

    const CON_ACTUACION = "A";
    const SIN_ACTUACION = "S";
    const SIN_CONEXION = "T";
    const ES_PRIVADO = "N";

    public static function obtenerDescripcion($novedad) {
        switch ($novedad){
            case TipoNovedadEnum::CON_ACTUACION:
                return __('common.novedad_a');
                break;
            case TipoNovedadEnum::SIN_ACTUACION:
                return __('common.novedad_s');
                break;
            case TipoNovedadEnum::SIN_CONEXION:
                return __('common.novedad_t');
                break;
            case TipoNovedadEnum::ES_PRIVADO:
                return __('common.novedad_n');
                break;
        }
    }

}
