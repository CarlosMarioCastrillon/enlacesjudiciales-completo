<?php namespace App\Enum;

class EstadoEjecucionEnum
{

    const SIN_EJECUTAR = 1;
    const EN_EJECUCION = 2;
    const EJECUTADO = 3;

}
