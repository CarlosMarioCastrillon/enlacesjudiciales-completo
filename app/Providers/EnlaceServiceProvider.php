<?php

namespace App\Providers;

use App\CloudStorageRepository;
use App\CloudStorageService;
use App\Contracts\Administracion\ActuacionExpedienteService;
use App\Contracts\Administracion\ActuacionPorEmpresaService;
use App\Contracts\Administracion\ActuacionService;
use App\Contracts\Administracion\AuditoriaProcesoService;
use App\Contracts\Administracion\ClienteService;
use App\Contracts\Administracion\ConsultaRemanenteService;
use App\Contracts\Administracion\ConsultaRemateService;
use App\Contracts\Administracion\DiligenciaService;
use App\Contracts\Administracion\ParametroConstanteService;
use App\Contracts\Administracion\ProcesoClienteService;
use App\Contracts\Administracion\ProcesoJudicialService;
use App\Contracts\Administracion\ProcesoValorService;
use App\Contracts\ConectorService;
use App\Contracts\Configuracion\AreaService;

use App\Contracts\Configuracion\AuxiliarJusticiaService;
use App\Contracts\Configuracion\CiudadService;
use App\Contracts\Configuracion\ClaseProcesoService;
use App\Contracts\Configuracion\CoberturaService;
use App\Contracts\Configuracion\ConceptoEconomicoService;
use App\Contracts\Configuracion\CopiaConceptoEconomicoService;
use App\Contracts\Configuracion\DepartamentoService;
use App\Contracts\Configuracion\DespachoService;
use App\Contracts\Configuracion\EmpresaService;
use App\Contracts\Configuracion\EtapaClaseProcesoService;
use App\Contracts\Configuracion\EtapaProcesoService;
use App\Contracts\Configuracion\IPCService;
use App\Contracts\Configuracion\JuzgadoService;
use App\Contracts\Configuracion\PlanService;
use App\Contracts\Configuracion\SalarioMinimoService;
use App\Contracts\Configuracion\ServicioService;
use App\Contracts\Configuracion\TarifaDiligenciaService;
use App\Contracts\Configuracion\TESService;
use App\Contracts\Configuracion\TipoActuacionService;
use App\Contracts\Configuracion\TipoDeEmpresaService;
use App\Contracts\Configuracion\TipoDiligenciaService;
use App\Contracts\Configuracion\TipoDocumentoService;
use App\Contracts\Configuracion\TipoNotificacionService;
use App\Contracts\Configuracion\ZonaService;
use App\Contracts\Importacion\AuxiliarJusticiaCargaErrorService;
use App\Contracts\Seguridad\AuditoriaMaestroService;
use App\Contracts\Seguridad\OpcionSistemaService;
use App\Contracts\Seguridad\RolService;
use App\Contracts\Seguridad\UsuarioService;
use App\Model\Administracion\ConsultaRemate;
use App\Model\Administracion\ParametroConstante;
use App\Repositories\Administracion\ActuacionExpedienteRepository;
use App\Repositories\Administracion\ActuacionPorEmpresaRepository;
use App\Repositories\Administracion\ActuacionRepository;
use App\Repositories\Administracion\AuditoriaProcesoRepository;
use App\Repositories\Administracion\ClienteRepository;
use App\Repositories\Administracion\ConsultaRemanenteRepository;
use App\Repositories\Administracion\ConsultaRemateRepository;
use App\Repositories\Administracion\DiligenciaRepository;
use App\Repositories\Administracion\ParametroConstanteRepository;
use App\Repositories\Administracion\ProcesoClienteRepository;
use App\Repositories\Administracion\ProcesoJudicialRepository;
use App\Repositories\ConectorRepository;
use App\Repositories\Importacion\AuxiliarJusticiaCargaErrorRepository;
use App\Repositories\Administracion\ProcesoValorRepository;
use App\Repositories\Configuracion\AreaRepository;
use App\Repositories\Configuracion\AuxiliarJusticiaRepository;
use App\Repositories\Configuracion\CiudadRepository;
use App\Repositories\Configuracion\ClaseProcesoRepository;
use App\Repositories\Configuracion\CoberturaRepository;
use App\Repositories\Configuracion\ConceptoEconomicoRepository;
use App\Repositories\Configuracion\CopiaConceptoEconomicoRepository;
use App\Repositories\Configuracion\DepartamentoRepository;
use App\Repositories\Configuracion\DespachoRepository;
use App\Repositories\Configuracion\EmpresaRepository;
use App\Repositories\Configuracion\EtapaClaseProcesoRepository;
use App\Repositories\Configuracion\EtapaProcesoRepository;
use App\Repositories\Configuracion\IPCRepository;
use App\Repositories\Configuracion\JuzgadoRepository;
use App\Repositories\Configuracion\PlanRepository;
use App\Repositories\Configuracion\SalarioMinimoRepository;
use App\Repositories\Configuracion\ServicioRepository;
use App\Repositories\Configuracion\TarifaDiligenciaRepository;
use App\Repositories\Configuracion\TESRepository;
use App\Repositories\Configuracion\TipoActuacionRepository;
use App\Repositories\Configuracion\TipoDeEmpresaRepository;
use App\Repositories\Configuracion\TipoDiligenciaRepository;
use App\Repositories\Configuracion\TipoDocumentoRepository;
use App\Repositories\Configuracion\TipoNotificacionRepository;
use App\Repositories\Configuracion\ZonaRepository;
use App\Repositories\Seguridad\AuditoriaMaestroRepository;
use App\Repositories\Seguridad\OpcionSistemaRepository;
use App\Repositories\Seguridad\RolRepository;
use App\Repositories\Seguridad\UsuarioRepository;
use Illuminate\Support\ServiceProvider;

class EnlaceServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // General
        $this->app->bind(ConectorService::class, ConectorRepository::class);

        // Seguridad
        $this->app->bind(RolService::class, RolRepository::class);
        $this->app->bind(UsuarioService::class, UsuarioRepository::class);
        $this->app->bind(OpcionSistemaService::class, OpcionSistemaRepository::class);
        $this->app->bind(AuditoriaMaestroService::class, AuditoriaMaestroRepository::class);

        // Configuracion
        $this->app->bind(ServicioService::class, ServicioRepository::class);
        $this->app->bind(TipoDeEmpresaService::class, TipoDeEmpresaRepository::class);
        $this->app->bind(ClaseProcesoService::class, ClaseProcesoRepository::class);
        $this->app->bind(EtapaProcesoService::class, EtapaProcesoRepository::class);
        $this->app->bind(DepartamentoService::class, DepartamentoRepository::class);
        $this->app->bind(EtapaClaseProcesoService::class, EtapaClaseProcesoRepository::class);
        $this->app->bind(ZonaService::class, ZonaRepository::class);
        $this->app->bind(CiudadService::class, CiudadRepository::class);
        $this->app->bind(JuzgadoService::class, JuzgadoRepository::class);
        $this->app->bind(TipoDocumentoService::class, TipoDocumentoRepository::class);
        $this->app->bind(DespachoService::class, DespachoRepository::class);
        $this->app->bind(EmpresaService::class, EmpresaRepository::class);
        $this->app->bind(AreaService::class, AreaRepository::class);
        $this->app->bind(PlanService::class, PlanRepository::class);
        $this->app->bind(ConceptoEconomicoService::class, ConceptoEconomicoRepository::class);
        $this->app->bind(CopiaConceptoEconomicoService::class, CopiaConceptoEconomicoRepository::class);
        $this->app->bind(TipoDiligenciaService::class, TipoDiligenciaRepository::class);
        $this->app->bind(CoberturaService::class, CoberturaRepository::class);
        $this->app->bind(TarifaDiligenciaService::class, TarifaDiligenciaRepository::class);
        $this->app->bind(ClienteService::class, ClienteRepository::class);
        $this->app->bind(TipoActuacionService::class, TipoActuacionRepository::class);
        $this->app->bind(ProcesoValorService::class, ProcesoValorRepository::class);
        $this->app->bind(ProcesoJudicialService::class, ProcesoJudicialRepository::class);
        $this->app->bind(ProcesoClienteService::class, ProcesoClienteRepository::class);
        $this->app->bind(DiligenciaService::class, DiligenciaRepository::class);
        $this->app->bind(TipoNotificacionService::class, TipoNotificacionRepository::class);
        $this->app->bind(SalarioMinimoService::class, SalarioMinimoRepository::class);
        $this->app->bind(TESService::class, TESRepository::class);
        $this->app->bind(IPCService::class, IPCRepository::class);
        $this->app->bind(AuxiliarJusticiaService::class, AuxiliarJusticiaRepository::class);
        $this->app->bind(ActuacionService::class, ActuacionRepository::class);
        $this->app->bind(ActuacionPorEmpresaService::class, ActuacionPorEmpresaRepository::class);
        $this->app->bind(ActuacionExpedienteService::class, ActuacionExpedienteRepository::class);
        $this->app->bind(AuxiliarJusticiaCargaErrorService::class, AuxiliarJusticiaCargaErrorRepository::class);
        $this->app->bind(ConsultaRemateService::class, ConsultaRemateRepository::class);
        $this->app->bind(ConsultaRemanenteService::class, ConsultaRemanenteRepository::class);
        $this->app->bind(ParametroConstanteService::class, ParametroConstanteRepository::class);

        // Administración
        $this->app->bind(AuditoriaProcesoService::class, AuditoriaProcesoRepository::class);

        // Sistema de archivos S3
        $this->app->bind(CloudStorageService::class, CloudStorageRepository::class);
    }
}
