<?php


namespace App\Contracts\Seguridad;


interface OpcionSistemaService
{

    /**
     * Cargar una opcion del sistema
     * @param $id
     * @return mixed
     */
    public function cargar($id);

    /**
     * Modificar o crear una nueva opción
     * @param $dto
     * @return mixed
     */
    public function modificarOCrear($dto);

    /**
     * Obtener la colección de opciones del sistema
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccion($dto);

    /**
     * Obtener la colección ligera de opciones del sistema
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccionLigera($dto);

    /**
     * Obtener arbol de opciones
     * @return mixed
     */
    public function obtenerArbolDeOpciones();

}
