<?php

namespace App\Contracts\Seguridad;

interface UsuarioService
{

    /**
     * Cargar un rol
     * @return mixed
     */
    public function cargar($id);

    /**
     * Modificar o crear un rol
     * @param $dto
     * @return mixed
     */
    public function modificarOCrear($dto);

    /**
     * Modificar contraseña
     * @param $id
     * @param $dto
     * @return mixed
     */
    public function cambiarClave($id, $dto);

    /**
     * Eliminar un rol
     * @param $id
     * @return mixed
     */
    public function eliminar($id);

    /**
     * Obtener la coleccion de usuarios
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccion($dto);

    /**
     * Obtener la coleccion simple de usuarios
     * @param $dto
     * @return mixed
     */
    public function obtener($dto);

    /**
     * Obtener la coleccion ligera de usuarios
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccionLigera($dto);

    /**
     * Obtener el dependiente principal
     * @param $dto
     * @return mixed
     */
    public function obtenerDependientePrincipal($dto);


}
