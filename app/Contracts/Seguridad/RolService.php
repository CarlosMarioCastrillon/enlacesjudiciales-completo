<?php

namespace App\Contracts\Seguridad;

interface RolService
{

    /**
     * Cargar un rol
     * @return mixed
     */
    public function cargar($id);

    /**
     * Modificar o crear un rol
     * @param $dto
     * @return mixed
     */
    public function modificarOCrear($dto);

    /**
     * Eliminar un rol
     * @param $id
     * @return mixed
     */
    public function eliminar($id);

    /**
     * Obtener la coleccion de roles
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccion($dto);

    /**
     * Obtener la coleccion ligera de roles
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccionLigera($dto);

    /**
     * Obtener permisos del rol
     * @param $id
     * @param $dto
     * @return mixed
     */
    public function obtenerPermisos($id, $dto);

    /**
     * Otorgar permisos al rol
     * @param $id
     * @param $permisos
     * @return mixed
     */
    public function otorgarPermisos($id, $permisos);

    /**
     * Revocar permisos al rol
     * @param $id
     * @param $permisos
     * @return mixed
     */
    public function revocarPermisos($id, $permisos);

}