<?php

namespace App\Contracts\Seguridad;

interface AuditoriaMaestroService
{

    /**
     * Cargar una auditoria
     * @return mixed
     */
    public function cargar($id);

    /**
     * Crear una auditoria
     * @param $dto
     * @return mixed
     */
    public function crear($dto);

    /**
     * Eliminar una auditoria
     * @param $id
     * @return mixed
     */
    public function eliminar($id);

    /**
     * Obtener la coleccion de auditorias
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccion($dto);

}