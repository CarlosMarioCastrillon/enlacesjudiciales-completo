<?php


namespace App\Contracts\Configuracion;


interface TipoDocumentoService
{

    /**
     * Cargar un tipo de documento
     * @param $id
     * @return mixed
     */
    public function cargar($id);

    /**
     * Modificar o crear un nuevo tipo de documento
     * @param $dto
     * @return mixed
     */
    public function modificarOCrear($dto);

    /**
     * Eliminar un tipo de documento
     * @param $id
     * @return mixed
     */
    public function eliminar($id);

    /**
     * Obtener la colección de tipos de documento
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccion($dto);

    /**
     * Obtener la colección ligera de tipos de documentos
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccionLigera($dto);
}
