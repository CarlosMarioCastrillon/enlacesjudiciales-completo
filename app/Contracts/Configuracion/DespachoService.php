<?php


namespace App\Contracts\Configuracion;


interface DespachoService
{

    /**
     * Cargar un despacho
     * @param $id
     * @return mixed
     */
    public function cargar($id);

    /**
     * Modificar o crear un nuevo despacho
     * @param $dto
     * @return mixed
     */
    public function modificarOCrear($dto);

    /**
     * Eliminar un despacho
     * @param $id
     * @return mixed
     */
    public function eliminar($id);

    /**
     * Obtener la colección de despachos
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccion($dto);

    /**
     * Obtener la colección ligera de los despachos
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccionLigera($dto);

    /**
     * Obtener la colección de referencia de los despachos
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccionReferencia($dto);

}
