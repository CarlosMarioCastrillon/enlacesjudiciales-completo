<?php


namespace App\Contracts\Configuracion;


interface EmpresaService
{

    /**
     * Cargar una empresa
     * @param $id
     * @return mixed
     */
    public function cargar($id);

    /**
     * Modificar o crear una nueva empresa
     * @param $dto
     * @return mixed
     */
    public function modificarOCrear($dto);

    /**
     * Modificar las coberturas de la empresa
     * @param $dto
     * @return mixed
     */
    public function modificarCobertura($dto);

    /**
     * Eliminar una empresa
     * @param $id
     * @return mixed
     */
    public function eliminar($id);

    /**
     * Obtener la colección de empresas
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccion($dto);

    /**
     * Obtener la colección ligera de empresas
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccionLigera($dto);

    /**
     * Obtener la colección de exportación a excel
     * @param $dto
     * @return mixed
     */
    public function getExcel($dto);

}
