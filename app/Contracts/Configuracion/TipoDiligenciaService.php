<?php


namespace App\Contracts\Configuracion;


interface TipoDiligenciaService
{

    /**
     * Cargar un tipo de diligencia
     * @param $id
     * @return mixed
     */
    public function cargar($id);

    /**
     * Modificar o crear un nuevo tipo de diligencia
     * @param $dto
     * @return mixed
     */
    public function modificarOCrear($dto);

    /**
     * Eliminar un tipo de diligencia
     * @param $id
     * @return mixed
     */
    public function eliminar($id);

    /**
     * Obtener la colección de tipos de diligencias
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccion($dto);

    /**
     * Obtener la colección ligera de tipos de diligencias
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccionLigera($dto);
}
