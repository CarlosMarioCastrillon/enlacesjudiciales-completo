<?php


namespace App\Contracts\Configuracion;


interface CopiaConceptoEconomicoService
{

    /**
     * Cargar un concepto económico
     * @param $id
     * @return mixed
     */
    public function cargar($id);

    /**
     * Modificar o crear un nuevo concepto económico
     * @param $dto
     * @return mixed
     */
    public function modificarOCrear($dto);

    /**
     * Eliminar un concepto económico
     * @param $id
     * @return mixed
     */
    public function eliminar($id);

    /**
     * Obtener la colección de conceptos económicos
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccion($dto);

    /**
     * Obtener la colección ligera de conceptos económicos
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccionLigera($dto);

}
