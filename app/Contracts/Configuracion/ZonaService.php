<?php


namespace App\Contracts\Configuracion;


interface ZonaService
{

    /**
     * Cargar un zona
     * @param $id
     * @return mixed
     */
    public function cargar($id);

    /**
     * Modificar o crear un nuevo zona
     * @param $dto
     * @return mixed
     */
    public function modificarOCrear($dto);

    /**
     * Eliminar un zona
     * @param $id
     * @return mixed
     */
    public function eliminar($id);

    /**
     * Obtener la colección de zonas
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccion($dto);

    /**
     * Obtener la colección ligera de zonas
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccionLigera($dto);
}
