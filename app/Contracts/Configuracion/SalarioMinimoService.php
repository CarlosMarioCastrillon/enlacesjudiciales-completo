<?php


namespace App\Contracts\Configuracion;


interface SalarioMinimoService
{

    /**
     * Cargar un salario mínimo
     * @param $id
     * @return mixed
     */
    public function cargar($id);

    /**
     * Modificar o crear un nuevo salario mínimo
     * @param $dto
     * @return mixed
     */
    public function modificarOCrear($dto);

    /**
     * Eliminar un salario mínimo
     * @param $id
     * @return mixed
     */
    public function eliminar($id);

    /**
     * Obtener la colección de salarios mínimos
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccion($dto);

    /**
     * Obtener la colección ligera de salarios mínimos
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccionLigera($dto);

}
