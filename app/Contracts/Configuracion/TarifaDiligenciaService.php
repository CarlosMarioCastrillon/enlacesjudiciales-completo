<?php


namespace App\Contracts\Configuracion;


interface TarifaDiligenciaService
{

    /**
     * Cargar una tarifa de diligencia
     * @param $id
     * @return mixed
     */
    public function cargar($id);

    /**
     * Modificar o crear un nueva tarifa de diligencia
     * @param $dto
     * @return mixed
     */
    public function modificarOCrear($dto);

    /**
     * Eliminar una tarifa de diligencia
     * @param $id
     * @return mixed
     */
    public function eliminar($id);

    /**
     * Obtener la colección de tarifas de diligencias
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccion($dto);

    /**
     * Obtener los valores de tarifas diligecias por tipo de diligencia
     * @param $dto
     * @return mixed
     */
    public function obtener($dto);

}
