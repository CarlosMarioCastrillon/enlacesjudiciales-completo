<?php


namespace App\Contracts\Configuracion;


interface AreaService
{

    /**
     * Cargar un área
     * @param $id
     * @return mixed
     */
    public function cargar($id);

    /**
     * Modificar o crear una nueva área
     * @param $dto
     * @return mixed
     */
    public function modificarOCrear($dto);

    /**
     * Eliminar un área
     * @param $id
     * @return mixed
     */
    public function eliminar($id);

    /**
     * Obtener la colección de áreas
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccion($dto);

    /**
     * Obtener la colección simple de áreas
     * @param $dto
     * @return mixed
     */
    public function obtener($dto);


}
