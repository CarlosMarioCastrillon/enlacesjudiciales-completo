<?php


namespace App\Contracts\Configuracion;


interface PlanService
{

    /**
     * Cargar un plan
     * @param $id
     * @return mixed
     */
    public function cargar($id);

    /**
     * Modificar o crear un nuevo plan
     * @param $dto
     * @return mixed
     */
    public function modificarOCrear($dto);

    /**
     * Eliminar un plan
     * @param $id
     * @return mixed
     */
    public function eliminar($id);

    /**
     * Obtener la colección de planes
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccion($dto);

    /**
     * Obtener la colección simple de planes
     * @param $dto
     * @return mixed
     */
    public function obtener($dto);


}
