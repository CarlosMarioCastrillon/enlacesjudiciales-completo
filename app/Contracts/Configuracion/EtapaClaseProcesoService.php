<?php


namespace App\Contracts\Configuracion;


interface EtapaClaseProcesoService
{

    /**
     * Cargar una etapa para una clase de proceso
     * @param $id
     * @return mixed
     */
    public function cargar($id);

    /**
     * Modificar o crear una etapa para una clase de proceso
     * @param $dto
     * @return mixed
     */
    public function modificarOCrear($dto);

    /**
     * Eliminar una etapa para una clase de proceso
     * @param $id
     * @return mixed
     */
    public function eliminar($id);

    /**
     * Obtener la colección de etapas por clase de proceso
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccion($dto);

}
