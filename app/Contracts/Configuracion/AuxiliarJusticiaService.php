<?php


namespace App\Contracts\Configuracion;


interface AuxiliarJusticiaService
{

    /**
     * Cargar un auxiliar de la justicia
     * @param $id
     * @return mixed
     */
    public function cargar($id);

    /**
     * Modificar o crear un nuevo auxiliar de la justicia
     * @param $dto
     * @return mixed
     */
    public function modificarOCrear($dto);

    /**
     * Importar datos desde archivo excel
     * @param $archivo
     * @return mixed
     */
    public function importar($archivo);

    /**
     * Eliminar un auxiliar de la justicia
     * @param $id
     * @return mixed
     */
    public function eliminar($id);

    /**
     * Obtener la colección de auxiliares de la justicias
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccion($dto);

    /**
     * Obtener la colección ligera de auxiliares de la justicias
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccionLigera($dto);

}
