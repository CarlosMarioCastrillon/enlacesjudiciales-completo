<?php


namespace App\Contracts\Configuracion;


interface EtapaProcesoService
{

    /**
     * Cargar una etapa de proceso
     * @param $id
     * @return mixed
     */
    public function cargar($id);

    /**
     * Modificar o crear un nueva etapa de proceso
     * @param $dto
     * @return mixed
     */
    public function modificarOCrear($dto);

    /**
     * Eliminar una etapa de proceso
     * @param $id
     * @return mixed
     */
    public function eliminar($id);

    /**
     * Obtener la colección de etapas de procesos
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccion($dto);

    /**
     * Obtener la colección ligera de las etapas del proceso
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccionLigera($dto);

}
