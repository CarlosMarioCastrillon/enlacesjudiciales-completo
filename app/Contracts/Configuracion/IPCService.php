<?php


namespace App\Contracts\Configuracion;


interface IPCService
{

    /**
     * Cargar un IPC
     * @param $id
     * @return mixed
     */
    public function cargar($id);

    /**
     * Modificar o crear un IPC
     * @param $dto
     * @return mixed
     */
    public function modificarOCrear($dto);

    /**
     * Eliminar un IPC
     * @param $id
     * @return mixed
     */
    public function eliminar($id);

    /**
     * Obtener la colección de IPC
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccion($dto);

    /**
     * Obtener la colección ligera de IPC
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccionLigera($dto);

}
