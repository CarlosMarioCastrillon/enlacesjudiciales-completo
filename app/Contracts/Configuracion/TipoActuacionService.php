<?php


namespace App\Contracts\Configuracion;


interface TipoActuacionService
{

    /**
     * Cargar un tipo de actuación
     * @param $id
     * @return mixed
     */
    public function cargar($id);

    /**
     * Modificar o crear un nuevo tipo de actuación
     * @param $dto
     * @return mixed
     */
    public function modificarOCrear($dto);

    /**
     * Eliminar un tipo de actuación
     * @param $id
     * @return mixed
     */
    public function eliminar($id);

    /**
     * Obtener la colección de tipos de actuación
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccion($dto);

    /**
     * Obtener la colección ligera de tipos de actuación
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccionLigera($dto);
}
