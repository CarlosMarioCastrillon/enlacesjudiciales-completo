<?php


namespace App\Contracts\Configuracion;


interface TipoNotificacionService
{

    /**
     * Cargar un tipo de notificacion
     * @param $id
     * @return mixed
     */
    public function cargar($id);

    /**
     * Modificar o crear un nuevo tipo de notificacion
     * @param $dto
     * @return mixed
     */
    public function modificarOCrear($dto);

    /**
     * Eliminar un tipo de notificacion
     * @param $id
     * @return mixed
     */
    public function eliminar($id);

    /**
     * Obtener la colección de tipos de notificaciones
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccion($dto);

    /**
     * Obtener la colección ligera de tipos de notificaciones
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccionLigera($dto);

}
