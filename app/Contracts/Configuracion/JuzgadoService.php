<?php


namespace App\Contracts\Configuracion;


interface JuzgadoService
{

    /**
     * Cargar un juzgado
     * @param $id
     * @return mixed
     */
    public function cargar($id);

    /**
     * Modificar o crear un nuevo juzgado
     * @param $dto
     * @return mixed
     */
    public function modificarOCrear($dto);

    /**
     * Eliminar un juzgado
     * @param $id
     * @return mixed
     */
    public function eliminar($id);

    /**
     * Obtener la colección de juzgados
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccion($dto);

    /**
     * Obtener la colección ligera de los juzgados
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccionLigera($dto);

    /**
     * Obtener la colección de referencia de los juzgados
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccionReferencia($dto);

}
