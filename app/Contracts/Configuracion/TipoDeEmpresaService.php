<?php


namespace App\Contracts\Configuracion;


interface TipoDeEmpresaService
{

    /**
     * Cargar un tipo de empresa
     * @param $id
     * @return mixed
     */
    public function cargar($id);

    /**
     * Modificar o crear un nuevo tipo de empresa
     * @param $dto
     * @return mixed
     */
    public function modificarOCrear($dto);

    /**
     * Eliminar un tipo de empresa
     * @param $id
     * @return mixed
     */
    public function eliminar($id);

    /**
     * Obtener la colección de tipos de empresa
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccion($dto);

    /**
     * Obtener la colección ligera de tipos de empresa
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccionLigera($dto);
}
