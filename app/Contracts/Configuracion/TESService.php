<?php


namespace App\Contracts\Configuracion;


interface TESService
{

    /**
     * Cargar una TES
     * @param $id
     * @return mixed
     */
    public function cargar($id);

    /**
     * Modificar o crear una TES
     * @param $dto
     * @return mixed
     */
    public function modificarOCrear($dto);

    /**
     * Eliminar una TES
     * @param $id
     * @return mixed
     */
    public function eliminar($id);

    /**
     * Obtener la colección de TES
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccion($dto);

    /**
     * Obtener la colección ligera de TES
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccionLigera($dto);

}
