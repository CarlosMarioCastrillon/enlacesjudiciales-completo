<?php


namespace App\Contracts\Configuracion;


interface CiudadService
{

    /**
     * Cargar una ciudad
     * @param $id
     * @return mixed
     */
    public function cargar($id);

    /**
     * Modificar o crear una nueva ciudad
     * @param $dto
     * @return mixed
     */
    public function modificarOCrear($dto);

    /**
     * Eliminar una ciudad
     * @param $id
     * @return mixed
     */
    public function eliminar($id);

    /**
     * Obtener la colección de ciudades
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccion($dto);

    /**
     * Obtener la colección ligera de las etapas del proceso
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccionLigera($dto);

}
