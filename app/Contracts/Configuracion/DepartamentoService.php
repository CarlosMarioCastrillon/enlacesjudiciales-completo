<?php


namespace App\Contracts\Configuracion;


interface DepartamentoService
{

    /**
     * Cargar un departamento
     * @param $id
     * @return mixed
     */
    public function cargar($id);

    /**
     * Modificar o crear un nuevo departamento
     * @param $dto
     * @return mixed
     */
    public function modificarOCrear($dto);

    /**
     * Eliminar un departamento
     * @param $id
     * @return mixed
     */
    public function eliminar($id);

    /**
     * Obtener la colección de departamentos
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccion($dto);

    /**
     * Obtener la colección ligera de departamentos
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccionLigera($dto);

}
