<?php


namespace App\Contracts\Configuracion;


interface ServicioService
{

    /**
     * Cargar un servicio
     * @param $id
     * @return mixed
     */
    public function cargar($id);

    /**
     * Modificar o crear un nuevo servicio
     * @param $dto
     * @return mixed
     */
    public function modificarOCrear($dto);

    /**
     * Eliminar un servicio
     * @param $id
     * @return mixed
     */
    public function eliminar($id);

    /**
     * Obtener la colección de servicios
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccion($dto);

    /**
     * Obtener la colección ligera de servicios
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccionLigera($dto);

}
