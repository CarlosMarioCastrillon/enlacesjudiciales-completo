<?php


namespace App\Contracts\Configuracion;


interface ClaseProcesoService
{

    /**
     * Cargar una clase de proceso
     * @param $id
     * @return mixed
     */
    public function cargar($id);

    /**
     * Modificar o crear un nueva clase de proceso
     * @param $dto
     * @return mixed
     */
    public function modificarOCrear($dto);

    /**
     * Eliminar una clase de proceso
     * @param $id
     * @return mixed
     */
    public function eliminar($id);

    /**
     * Obtener la colección de clases de procesos
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccion($dto);

    /**
     * Obtener la colección ligera de clases de procesos
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccionLigera($dto);

}
