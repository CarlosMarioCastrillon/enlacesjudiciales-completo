<?php


namespace App\Contracts\Configuracion;


interface CoberturaService
{

    /**
     * Cargar una cobertura
     * @param $id
     * @return mixed
     */
    public function cargar($id);

    /**
     * Modificar o crear un nueva cobertura
     * @param $dto
     * @return mixed
     */
    public function modificarOCrear($dto);

    /**
     * Eliminar una cobertura
     * @param $id
     * @return mixed
     */
    public function eliminar($id);

    /**
     * Obtener la colección de coberturas
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccion($dto);



}
