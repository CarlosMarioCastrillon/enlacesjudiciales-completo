<?php

namespace App\Contracts;

interface ConectorService
{

    /**
     * Obtener la información de un proceso
     * @param $dto
     * @return mixed
     */
    public function get($dto);

}
