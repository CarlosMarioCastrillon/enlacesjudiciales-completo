<?php

namespace App;

interface CloudStorageService
{

    /**
     * @param $pathName
     * @return mixed
     */
    public function get($pathName);

    /**
     * Put file in cloud storage
     * @param $file
     * @param $pathName
     * @param bool $makePublic
     * @return mixed
     */
    public function put($file, $pathName, $makePublic = false);

    /**
     * Copy file in cloud storage
     * @param $pathName
     * @param $newPathName
     * @param bool $makePublic
     * @return mixed
     */
    public function copy($pathName, $newPathName, $makePublic = false);

    /**
     * Delete file from cloud storage
     * @param $pathName
     * @return mixed
     */
    public function delete($pathName);

}
