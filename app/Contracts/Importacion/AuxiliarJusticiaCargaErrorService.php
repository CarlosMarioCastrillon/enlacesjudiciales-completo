<?php


namespace App\Contracts\Importacion;


interface AuxiliarJusticiaCargaErrorService
{

    /**
     * Importar datos desde archivo excel
     * @param $dto
     * @return mixed
     */
    public function importar($dto);

    /**
     * Obtener la colección de auxiliares de la justicias
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccion($dto);

}
