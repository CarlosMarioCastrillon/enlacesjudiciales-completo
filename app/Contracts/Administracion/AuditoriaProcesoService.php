<?php


namespace App\Contracts\Administracion;


interface AuditoriaProcesoService
{

    /**
     * Cargar procesos
     * @param $dto
     * @return mixed
     */
    public function cargarProcesos($dto);

    /**
     * Programar ejecución
     * @param $dto
     * @return mixed
     */
    public function programar($dto);

    /**
     * Obtener la colección de programaciones
     * @param $dto
     * @return mixed
     */
    public function getColeccion($dto);

    /**
     * Obtener excel de auditoría de procesos
     * @param $programacionId
     * @return mixed
     */
    public function getExcelAuditorias($programacionId);

    /**
     * Obtener excel de novedades de procesos
     * @param $programacionId
     * @return mixed
     */
    public function getExcelNovedades($programacionId);

    /**
     * Lanzar ejecución de la auditoria programada
     * @return mixed
     */
    public function lanzarAuditoria();

    /**
     * Ejecutar auditorias programadas
     * @param $dto
     * @return mixed
     */
    public function ejecutarAuditoria($dto);

}
