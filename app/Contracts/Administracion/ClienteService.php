<?php


namespace App\Contracts\Administracion;


interface ClienteService
{

    /**
     * Cargar un cliente
     * @param $id
     * @return mixed
     */
    public function cargar($id);

    /**
     * Modificar o crear un nuevo cliente
     * @param $dto
     * @return mixed
     */
    public function modificarOCrear($dto);

    /**
     * Eliminar un cliente
     * @param $id
     * @return mixed
     */
    public function eliminar($id);

    /**
     * Obtener la colección de clientes
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccion($dto);

    /**
     * Obtener la colección de exportación a excel
     * @param $dto
     * @return mixed
     */
    public function getExcel($dto);
}
