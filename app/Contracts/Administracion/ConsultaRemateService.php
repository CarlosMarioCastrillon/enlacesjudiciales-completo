<?php


namespace App\Contracts\Administracion;


interface ConsultaRemateService
{
    /**
     * Obtener la colección de actuaciones y expedientes para consultar Remate
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccion($dto);
}
