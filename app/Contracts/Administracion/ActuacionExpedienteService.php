<?php


namespace App\Contracts\Administracion;


interface ActuacionExpedienteService
{
    /**
     * Obtener la colección de actuaciones y expedientes
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccion($dto);
}
