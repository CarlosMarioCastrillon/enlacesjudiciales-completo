<?php


namespace App\Contracts\Administracion;


interface ProcesoJudicialService
{

    /**
     * Cargar un proceso judicial
     * @param $id
     * @return mixed
     */
    public function cargar($id);

    /**
     * Modificar o crear un nuevo proceso judicial
     * @param $dto
     * @return mixed
     */
    public function modificarOCrear($dto);

    /**
     * Obtener las validaciones un proceso antes de su registro
     * @param $dto
     * @return mixed
     */
    public function validarProcesoRama($dto);

    /**
     * Importar procesos desde archivo excel
     * @param $archivo
     * @return mixed
     */
    public function importar($archivo);

    /**
     * Importar procesos desde texto
     * @param $texto
     * @return mixed
     */
    public function importarTexto($texto);

    /**
     * Eliminar un proceso judicial
     * @param $id
     * @return mixed
     */
    public function eliminar($id);

    /**
     * Obtener la colección de procesos judiciales
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccion($dto);

    /**
     * Obtener la colección ligera de las procesos judiciales
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccionLigera($dto);

    /**
     * Obtener la colección de procesos judiciales por rol
     * @param $dto
     * @return mixed
     */
    public function obtenerProcesosPorRol($dto);
}
