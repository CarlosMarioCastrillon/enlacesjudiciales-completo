<?php


namespace App\Contracts\Administracion;


interface DiligenciaService
{

    /**
     * Cargar una diligencia
     * @param $id
     * @return mixed
     */
    public function cargar($id);

    /**
     * Cargar una diligencia
     * @param $id
     * @return mixed
     */
    public function cargarHistorico($id);

    /**
     * Modificar o crear una nueva diligencia
     * @param $dto
     * @return mixed
     */
    public function modificarOCrear($dto);

    /**
     * Eliminar una diligencia
     * @param $id
     * @return mixed
     */
    public function eliminar($id);

    /**
     * Obtener la colección de diligencias
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccion($dto);

    /**
     * Obtener la colección de diligencias por cliente
     * @param $dto
     * @return mixed
     */
    public function obtenerDiligenciaPorCliente($dto);
}
