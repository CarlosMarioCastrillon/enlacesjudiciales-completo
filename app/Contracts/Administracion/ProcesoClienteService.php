<?php


namespace App\Contracts\Administracion;


interface ProcesoClienteService
{

    /**
     * Cargar un valor
     * @param $id
     * @return mixed
     */
    public function cargar($id);

    /**
     * Modificar o crear un nuevo valor
     * @param $dto
     * @return mixed
     */
    public function modificarOCrear($dto);

    /**
     * Eliminar un valor
     * @param $id
     * @return mixed
     */
    public function eliminar($id);

    /**
     * Obtener la colección de valores
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccion($dto);
}
