<?php


namespace App\Contracts\Administracion;


interface ActuacionService
{

    /**
     * Cargar una actuación
     * @param $id
     * @return mixed
     */
    public function cargar($id);

    /**
     * Modificar o crear una nueva actuación
     * @param $dto
     * @return mixed
     */
    public function modificarOCrear($dto);

    /**
     * Modificar las actuaciones de forma masiva
     * @param $dto
     * @return mixed
     */
    public function modificarMasivo($dto);

    /**
     * Eliminar una actuación
     * @param $id
     * @return mixed
     */
    public function eliminar($id);

    /**
     * Eliminar las actuaciones de forma masiva
     * @param $dto
     * @return mixed
     */
    public function eliminarMasivo($dto);

    /**
     * Obtener la colección de actuaciones
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccion($dto);

    /**
     * Obtener la colección ligera de actuaciones
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccionLigera($dto);

    /**
     * Obtener el reporte de actuaciones
     * @param $dto
     * @return mixed
     */
    public function obtenerReporte($dto);

    /**
     * Obtener el reporte de actuaciones por empresa
     * @param $dto
     * @return mixed
     */
    public function obtenerReportePorEmpresa($dto);

    /**
     * Obtener la colección de remates
     * @param $dto
     * @return mixed
     */
    public function obtenerRemates($dto);

    /**
     * Obtener la colección de remanentes
     * @param $dto
     * @return mixed
     */
    public function obtenerRemanentes($dto);

    /**
     * Obtener audiencias pendientes
     * @param $dto
     * @return mixed
     */
    public function obtenerAudienciasPendientes($dto);

    /**
     * Obtener audiencias cumplidas
     * @param $dto
     * @return mixed
     */
    public function obtenerAudienciasCumplidas($dto);

    /**
     * Obtener actuaciones pendientes y cumplidas
     * @param $dto
     * @return mixed
     */
    public function obtenerActuacionesPendientesYCumplidas($dto);

    // Importación de datos

    /**
     * Importar procesos desde archivo excel
     * @param $archivo
     * @return mixed
     */
    public function importar($archivo);

    /**
     * Importar procesos desde texto
     * @param $texto
     * @return mixed
     */
    public function importarTexto($texto);

    /**
     * Lanzar lectura de actuaciones en la rama judicial
     * @return mixed
     */
    public function lanzarLecturaActuaciones();

    /**
     * Ejecutar lectura de actuaciones en la rama judicial
     * @param $dto
     * @return mixed
     */
    public function ejecutarLecturaDeActuaciones($dto);

}
