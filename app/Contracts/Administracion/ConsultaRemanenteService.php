<?php


namespace App\Contracts\Administracion;


interface ConsultaRemanenteService
{
    /**
     * Obtener la colección de actuaciones y expedientes para consultar Remanente
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccion($dto);
}
