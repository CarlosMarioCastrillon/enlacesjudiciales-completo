<?php


namespace App\Contracts\Administracion;


interface ParametroConstanteService
{

    /**
     * Cargar un parámetro
     * @param $id
     * @return mixed
     */
    public function cargar($id);

    /**
     * Modificar o crear un nuevo parámetro
     * @param $dto
     * @return mixed
     */
    public function modificarOCrear($dto);

    /**
     * Eliminar un parámetro
     * @param $id
     * @return mixed
     */
    public function eliminar($id);

    /**
     * Obtener la colección de parámetros
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccion($dto);
}
