<?php


namespace App\Contracts\Administracion;


interface ActuacionPorEmpresaService
{
    /**
     * Obtener la colección de actuaciones y expedientes
     * @param $dto
     * @return mixed
     */
    public function obtenerColeccion($dto);
}
