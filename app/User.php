<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasRoles, HasApiTokens, Notifiable;

    protected $guard_name ='api';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function rol(){
        return DB::table('roles')
            ->join('model_has_roles', 'model_has_roles.role_id', '=', 'roles.id')
            ->where('model_has_roles.model_type', User::class)
            ->where('model_has_roles.model_id', $this->id)
            ->select('roles.*')
            ->first();
    }

    public function usuario(){
        return DB::table('usuarios')
            ->where('usuarios.user_id', $this->id)
            ->select('usuarios.*')
            ->first();
    }

    public function empresa(){
        return DB::table('empresas')
            ->join('usuarios', 'usuarios.empresa_id', '=', 'empresas.id')
            ->where('usuarios.user_id', $this->id)
            ->select('empresas.*')
            ->groupBy('empresas.id')
            ->first();
    }

    public function tipoDocumento(){
        return DB::table('tipos_de_documentos')
            ->join('usuarios', 'usuarios.tipo_documento_id', '=', 'tipos_de_documentos.id')
            ->where('usuarios.user_id', $this->id)
            ->select('tipos_de_documentos.*')
            ->groupBy('tipos_de_documentos.id')
            ->first();
    }

}
