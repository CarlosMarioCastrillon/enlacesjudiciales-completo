<?php

namespace App\Imports;

use App\Contracts\Seguridad\AuditoriaMaestroService;
use App\Model\Configuracion\AuxiliarJusticia;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class AuxiliarJusticiaImport implements ToModel, WithBatchInserts, WithStartRow, WithValidation, SkipsOnFailure, SkipsOnError
{

    use Importable, SkipsFailures;

    private $empresaId;
    private $usuarioId;
    private $usuarioNombre;
    private $importedRows = 0;
    private $withErros = false;

    public function __construct($empresaId, $usuarioId, $usuarioNombre){
        $this->empresaId = $empresaId;
        $this->usuarioId = $usuarioId;
        $this->usuarioNombre = $usuarioNombre;
    }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        // Contador de filas importadas
        $this->importedRows++;

        return new AuxiliarJusticia([
            'nombre_ciudad'  =>  $row[0],
            'cargo'  =>  $row[1],
            'numero_documento'  =>  $row[2],
            'apellidos_auxiliar'  =>  $row[3],
            'nombres_auxiliar'  =>  $row[4],
            'direccion_oficina'  =>  $row[5],
            'telefono_oficina'  =>  $row[6],
            'direccion_residencia'  =>  $row[7],
            'telefono_residencia'  =>  $row[8],
            'telefono_celular'  =>  $row[9],
            'correo_electronico'  =>  $row[10],
            'empresa_id' => $this->empresaId,
            'usuario_creacion_id' => $this->usuarioId,
            'usuario_creacion_nombre' => $this->usuarioNombre,
            'usuario_modificacion_id' => $this->usuarioId,
            'usuario_modificacion_nombre' => $this->usuarioNombre
        ]);
    }

    /**
     * Fila de inicio
     * @return int
     */
    public function startRow(): int
    {
        return 2;
    }

    /**
     * Validación de columnas
     * @return array
     */
    public function rules(): array
    {
        return [
            '2' => 'numeric',
            '6' => 'numeric',
            '9' => 'numeric',
            '10' => 'email'
        ];
    }

    /**
     * Renombrar columnas
     * @return array
     */
    public function customValidationAttributes()
    {
        return [
            '0' => 'CIUDAD',
            '1' => 'CARGO',
            '2' => 'CEDULA',
            '3' => 'APELLIDOS',
            '4' => 'NOMBRES',
            '5' => 'DIR. OFICINA',
            '6' => 'TELEFONO OFICINA',
            '7' => 'DIRECCION RESIDENCIA',
            '8' => 'TELEFONO RESIDENCIA',
            '9' => 'CELULAR',
            '10' => 'E-MAIL'
        ];
    }

    /**
     * Inserción por lotes
     * @return int
     */
    public function batchSize(): int
    {
        return 100;
    }

    /**
     * @param \Throwable $e
     * @return int
     */
    public function onError(\Throwable $e)
    {
        return $this->withErros = true;
    }

    /**
     * Obtener cantidas filas insertadas
     * @return int
     */
    public function getImportedRows(): int
    {
        return $this->importedRows;
    }

    /**
     * Obtener cantidas filas insertadas
     * @return bool
     */
    public function getWithErrors(): bool
    {
        return $this->withErros;
    }

}
