<?php

namespace App\Imports;

use App\Model\Administracion\ParametroConstante;
use App\Model\Administracion\ProcesoJudicial;
use App\Model\Configuracion\Area;
use App\Model\Configuracion\Ciudad;
use App\Model\Configuracion\ClaseProceso;
use App\Model\Configuracion\Departamento;
use App\Model\Configuracion\Despacho;
use App\Model\Configuracion\Empresa;
use App\Model\Configuracion\Juzgado;
use App\Model\Seguridad\Usuario;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class ProcesoJudicialImport implements ToModel, WithBatchInserts, WithStartRow, WithValidation, SkipsOnFailure, SkipsOnError
{

    use Importable, SkipsFailures;

    private $empresaId;
    private $usuarioId;
    private $usuarioNombre;
    private $importedRows = 0;
    private $withErros = false;
    private $errors = [];
    private $imported = [];

    public function __construct($empresaId, $usuarioId, $usuarioNombre){
        $this->empresaId = $empresaId;
        $this->usuarioId = $usuarioId;
        $this->usuarioNombre = $usuarioNombre;
    }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        // Cargar parametros
        $parametros = ParametroConstante::cargarParametros();

        // Obtener los datos requeridos desde el número del proceso
        $codigoDepartamento = substr($row[0],0,2);
        $codigoCiudad = substr($row[0],0,5);
        $codigoJuzgado = substr($row[0],0,9);
        $numeroJuzgado = substr($row[0],5,2);
        $numeroSala = substr($row[0],7,2);
        $numeroDespacho = substr($row[0],9,3);
        $anio = substr($row[0],12,4);
        $numeroRadicado = substr($row[0],16,5);
        $consecutivo = substr($row[0],21,2);

        // Información de la empresa
        $empresa = Empresa::find($this->empresaId);

        // Obtener el responsable
        $usuario = Usuario::where([
            'empresa_id' => $this->empresaId,
            'documento' => $row[7] ?? null
        ])->first();

        // Departamento de origen
        $departamentoOrigen = Departamento::findBy($codigoDepartamento);

        // Obtener la ciudad actual
        $ciudadActual = Ciudad::where('nombre', $row[3])->first();

        // Obtener la ciudad de origen
        $ciudadOrigen = Ciudad::findBy($codigoCiudad);

        // Obtener la clase del proceso
        $claseProceso = ClaseProceso::where('nombre', $row[6])->first();

        // Obtener el juzgado actual
        $juzgadoActual = null;
        if(isset($ciudadActual)){
            $juzgadoActual = Juzgado::where('nombre', $row[4])->where('ciudad_id', $ciudadActual->id)->first();
        }

        // Obtener el juzgado de origen
        $juzgadoOrigen = Juzgado::findBy($codigoJuzgado);;

        // Obtener el despacho
        $despachoActual = null;
        $numeroDespachoActual = sprintf("%03d", $row[5]);
        if(isset($ciudadActual) && isset($juzgadoActual)){
            $despachoActual = Despacho::where([
                'departamento_id' => $ciudadActual->departamento_id,
                'ciudad_id' => $ciudadActual->id,
                'juzgado' => $juzgadoActual->juzgado,
                'sala' => $juzgadoActual->sala,
                'despacho' => $numeroDespachoActual
            ])->first();
        }

        $despachoOrigen = Despacho::where([
            'departamento_id' => $departamentoOrigen->id,
            'ciudad_id' => $ciudadOrigen->id,
            'juzgado' => $numeroJuzgado,
            'sala' => $numeroSala,
            'despacho' => $numeroDespacho
        ])->first();

        // Obtener la información de area de la empresa
        $area = Area::where('nombre', $row[8])->where('empresa_id', $this->empresaId)->first();
        if(!isset($area)){
            $area = Area::where('nombre', $parametros['AREA_EMPRESA_POR_DEFECTO'])
                ->where('empresa_id', $this->empresaId)->first();
        }

        // Realizar validaciones dependientes entre columnas
        $validator = Validator::make($row, [
            '3' => [
                function ($attribute, $value, $fail) use($ciudadActual) {
                    if(!isset($ciudadActual)){
                        $fail('Ciudad actual no válida.');
                    }
                }
            ],
            '4' => [
                function ($attribute, $value, $fail) use($juzgadoActual) {
                    if(!isset($juzgadoActual)){
                        $fail('Juzgado actual no válido.');
                    }
                }
            ],
            '5' => [
                function ($attribute, $value, $fail) use($despachoActual) {
                    if(!isset($despachoActual)){
                        $fail('Número juzgado actual no válido.');
                    }
                }
            ],
            '6' => [
                function ($attribute, $value, $fail) use($claseProceso) {
                    if(!isset($claseProceso)){
                        $fail('Clase de proceso no válida.');
                    }
                }
            ]
        ]);

        if($validator->fails()){
            foreach ($validator->errors()->messages() as $messages) {
                foreach ($messages as $error) {
                    $row['observacion'] = $error;
                    $this->errors[] = $row;
                }
            }

            return null;
        }

        // Departamento actual
        $departamentoActual = $ciudadActual->departamento;

        // Contador de filas importadas
        $this->importedRows++;

        // Registrar en los importados
        array_push($this->imported, [
            'numero_proceso' => $row[0],
            'nombres_demandantes' => $row[1],
            'nombres_demandados' => $row[2],
            'ciudad_nombre' => $row[3],
            'juzgado_nombre' => $row[4],
            'juzgado_numero' => $numeroDespachoActual,
            'clase_proceso_nombre' => $row[6],
            'responsable_documento' => $row[7],
            'area_nombre' => $row[8],
            'observacion' => "Registro correcto.",
            'empresa_id' => $this->empresaId,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'usuario_creacion_id' => $this->usuarioId,
            'usuario_creacion_nombre' => $this->usuarioNombre,
            'usuario_modificacion_id' => $this->usuarioId,
            'usuario_modificacion_nombre' => $this->usuarioNombre
        ]);

        // NUmero de proceso formado
        $numeroProceso = $ciudadActual->codigo_dane . $juzgadoActual->juzgado .
            $juzgadoActual->sala . $numeroDespachoActual . $anio . $numeroRadicado . '00';

        // Datos para control de duplicados
        $procesoJudicialReferencia =
            ProcesoJudicial::whereRaw('SUBSTRING(numero_radicado_proceso_actual, 1, 21) = ?', [
                substr($row[0],0,21)
            ])
            ->where('instancia_judicial_actual', 'S')
            ->where('empresa_id', $this->empresaId)->first();
        if(isset($procesoJudicialReferencia)){
            $instanciaJudicialActual = 'S';
            $procesoIdReferencia = $procesoJudicialReferencia->proceso_id_referencia;
            $procesoJudicialReferencia->instancia_judicial_actual = null;
            $procesoJudicialReferencia->save();
        }else{
            $instanciaJudicialActual = 'S';
            $procesoIdReferencia = null;
        }

        $dto = [
            'departamento_dane' => $departamentoActual->codigo_dane,
            'ciudad_dane' => $ciudadActual->codigo_dane,
            'juzgado_actual' => $juzgadoActual->juzgado_rama,
            'juzgado_origen_rama' => $codigoJuzgado,
            'despacho_numero' => $numeroDespachoActual,
            'despacho_origen_numero' => $numeroDespacho,
            'anio_proceso' => $anio,
            'numero_radicado' => $numeroRadicado,
            'numero_consecutivo' => $consecutivo,
            'numero_proceso' => $numeroProceso,
            'numero_radicado_proceso_actual' => $row[0],
            'proceso_id_referencia' => $procesoIdReferencia,
            'instancia_judicial_actual' => $instanciaJudicialActual,
            'nombres_demandantes' => $row[1] ?? null,
            'nombres_demandados' => $row[2] ?? null,
            'observaciones' => null,
            'indicativo_pertenece_rama' => $ciudadActual->rama_judicial . '' ?? '0',
            'indicativo_consulta_rama' => '0',
            'indicativo_tiene_vigilancia' => '0',
            'indicativo_estado' => '1',
            'empresa_id' => $this->empresaId,
            'zona_id' => $empresa->zona_id,
            'area_id' => $area->id ?? null,
            'cliente_id' => null,
            'abogado_responsable_id' => $usuario->id ?? null,
            'dependiente_asignado_id' => null,
            'ultima_clase_proceso_id' => $claseProceso->id ?? null,
            'ultima_etapa_proceso_id' => null,
            'departamento_id' => $departamentoActual->id,
            'ciudad_id' => $ciudadActual->id,
            'juzgado_id' => $juzgadoActual->id,
            'juzgado_origen_id' => $juzgadoOrigen->id,
            'despacho_id' => $despachoActual->id,
            'despacho_origen_id' => $despachoOrigen->id,
            'usuario_creacion_id' => $this->usuarioId,
            'usuario_creacion_nombre' => $this->usuarioNombre,
            'usuario_modificacion_id' => $this->usuarioId,
            'usuario_modificacion_nombre' => $this->usuarioNombre,
            'descripcion_clase_proceso' => $row[6]
        ];
        return new ProcesoJudicial($dto);
    }

    /**
     * Fila de inicio
     * @return int
     */
    public function startRow(): int
    {
        return 2;
    }

    /**
     * Validación de columnas
     * @return array
     */
    public function rules(): array
    {
        return [
            '0' => function($attribute, $value, $onFailure) {
                $mensajes = [];
                $conError = false;
                // Validar longitud de caracteres
                if (!(isset($value) && strlen($value) == 23)) {
                    $conError = true;
                    array_push($mensajes, 'Tamaño de número de proceso errado. Debe tener 23 dígitos.');
                }
                // Validar que sea numerico
                if (!is_numeric($value)) {
                    $conError = true;
                    array_push($mensajes, 'Número de proceso solo debe tener caracteres numéricos.');
                }
                // Validar que no exista en el sistema
                $procesoJudicial = ProcesoJudicial::where('numero_radicado_proceso_actual', $value)
                    ->where('empresa_id', $this->empresaId)->first();
                if(isset($procesoJudicial)){
                    $conError = true;
                    array_push($mensajes, 'Proceso ya existe para la empresa.');
                }
                // Validar datos número de proceso
                $codigoDepartamento = substr($value,0,2);
                $codigoCiudad = substr($value,0,5);
                $codigoJuzgado = substr($value,0,9);
                $numeroJuzgado = substr($value,5,2);
                $numeroSala = substr($value,7,2);
                $numeroDespacho = substr($value,9,3);
                $anio = +substr($value,12,4);
                $numeroRadicado = substr($value,16,5);
                $consecutivo = substr($value,21,2);
                $departamento = Departamento::findBy($codigoDepartamento);
                if(!isset($departamento)){
                    $conError = true;
                    array_push($mensajes, 'Código departamento no válido.');
                }
                $ciudad = Ciudad::findBy($codigoCiudad);
                if(!isset($ciudad)){
                    $conError = true;
                    array_push($mensajes, 'Código ciudad no válido.');
                }
                $juzgado = Juzgado::findBy($codigoJuzgado);
                if(!isset($juzgado)){
                    $conError = true;
                    array_push($mensajes, 'Juzgado rama judicial no válido.');
                }
                $despacho = null;
                if(isset($departamento) && isset($ciudad)){
                    $despacho = Despacho::where([
                        'departamento_id' => $departamento->id,
                        'ciudad_id' => $ciudad->id,
                        'juzgado' => $numeroJuzgado,
                        'sala' => $numeroSala,
                        'despacho' => $numeroDespacho
                    ])->first();
                }
                if(!isset($despacho)){
                    $conError = true;
                    array_push($mensajes, 'Número juzgado no válido.');
                }
                $fechaActual = Carbon::now();
                $fechaProceso = Carbon::now()->setYear($anio);
                if($fechaActual->diffInYears($fechaProceso) > 40 || $fechaActual->year < $anio){
                    $conError = true;
                    array_push($mensajes, 'Año proceso incorrecto.');
                }
                if(!(intval($numeroRadicado) >= 1 && intval($numeroRadicado) <= 99999)){
                    $conError = true;
                    array_push($mensajes, 'Número de radicado incorrecto.');
                }
                if(!(intval($consecutivo) >= 0 && intval($consecutivo) <= 5)){
                    $conError = true;
                    array_push($mensajes, 'Número de consecutivo incorrecto.');
                }

                if($conError){
                    $onFailure(join("||", $mensajes));
                }
            },
            '1' => 'string|nullable',
            '2' => 'string|nullable',
            '3' => 'string|required',
            '4' => 'string|required',
            '5' => function($attribute, $value, $onFailure) {
                $conError = false;
                if(!isset($value)){
                    $conError = true;
                    $mensaje = 'Número juzgado actual no válido.';
                }
                if($conError){
                    $onFailure($mensaje);
                }
            },
            '6' => 'string|exists:clases_de_procesos,nombre',
            '7' => function($attribute, $value, $onFailure) {
                $usuario = Usuario::where([
                    'empresa_id' => $this->empresaId,
                    'documento' => $value ?? null
                ])->first();
                if(!isset($usuario)){
                    $onFailure('Usuario no válido para la empresa.');
                }
            },
            '8' => function($attribute, $value, $onFailure) {
                $areaNombre = trim($value);
                if(isset($areaNombre)){
                    $area = Area::where([
                        'empresa_id' => $this->empresaId,
                        'nombre' => $areaNombre
                    ])->first();
                    if(!isset($area)){
                        $onFailure('Area no válida para la empresa.');
                    }
                }
            }
        ];
    }

    /**
     * Renombrar columnas
     * @return array
     */
    public function customValidationAttributes()
    {
        return [
            '0' => 'PROCESO ACTUAL',
            '1' => 'DEMANDANTE',
            '2' => 'DEMANDADO',
            '3' => 'CIUDAD ACTUAL',
            '4' => 'JUZGADO ACTUAL',
            '5' => 'NUMERO JUZGADO',
            '6' => 'CLASE PROCESO',
            '7' => 'RESPONSABLE',
            '8' => 'AREA'
        ];
    }

    /**
     * Mensajes personailizados
     * @return array
     */
    public function customValidationMessages()
    {
        return [
            '3.exists' => 'Ciudad actual no válida.',
            '4.exists' => 'Juzgado actual no válido.',
            '5.exists' => 'Número despacho no válido.',
            '6.exists' => 'Clase de proceso no válida.',
        ];
    }

    /**
     * Inserción por lotes
     * @return int
     */
    public function batchSize(): int
    {
        return 100;
    }

    /**
     * @param \Throwable $e
     * @return int
     */
    public function onError(\Throwable $e)
    {
        return $this->withErros = true;
    }

    /**
     * Obtener cantidas filas insertadas
     * @return int
     */
    public function getImportedRows(): int
    {
        return $this->importedRows;
    }

    /**
     * Obtener cantidas filas insertadas
     * @return bool
     */
    public function getWithErrors(): bool
    {
        return $this->withErros;
    }

    /**
     * Obtener los errores personalizados
     * @return array
     */
    public function getCustomErrors(): array
    {
        return $this->errors;
    }

    /**
     * Obtener las filas importadas
     * @return array
     */
    public function getImported(): array
    {
        return $this->imported;
    }

}
