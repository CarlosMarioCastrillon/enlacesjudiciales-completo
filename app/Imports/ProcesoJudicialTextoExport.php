<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\FromArray;

class ProcesoJudicialTextoExport implements FromArray
{

    private $texto;

    public function __construct($texto){
        $this->texto = $texto;
    }

    public function array(): array
    {
        $lineas = explode("\n", $this->texto);
        $datos = array_map(function ($linea){
            return explode("\t", $linea);
        }, $lineas);

        return $datos;
    }
}
