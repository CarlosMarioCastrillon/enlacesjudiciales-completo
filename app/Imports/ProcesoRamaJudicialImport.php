<?php

namespace App\Imports;

use App\Model\Administracion\ProcesoRamaJudicial;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class ProcesoRamaJudicialImport implements ToModel, WithBatchInserts, WithStartRow, WithValidation, SkipsOnFailure, SkipsOnError
{

    use Importable, SkipsFailures;

    private $importedRows = 0;
    private $withErrors = false;

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        // Contador de filas importadas
        $this->importedRows++;

        return new ProcesoRamaJudicial([
            'numero_proceso'  =>  $row[0]
        ]);
    }

    /**
     * Fila de inicio
     * @return int
     */
    public function startRow(): int
    {
        return 2;
    }

    /**
     * Validación de columnas
     * @return array
     */
    public function rules(): array
    {
        return [
            '0' => 'string'
        ];
    }

    /**
     * Renombrar columnas
     * @return array
     */
    public function customValidationAttributes()
    {
        return [
            '0' => 'Número proceso'
        ];
    }

    /**
     * Inserción por lotes
     * @return int
     */
    public function batchSize(): int
    {
        return 10;
    }

    /**
     * @param \Throwable $e
     * @return int
     */
    public function onError(\Throwable $e)
    {
        return $this->withErrors = true;
    }

    /**
     * Obtener cantidas filas insertadas
     * @return int
     */
    public function getImportedRows(): int
    {
        return $this->importedRows;
    }

    /**
     * Obtener cantidas filas insertadas
     * @return bool
     */
    public function getWithErrors(): bool
    {
        return $this->withErrors;
    }

}
