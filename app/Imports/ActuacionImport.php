<?php

namespace App\Imports;

use App\Model\Administracion\Actuacion;
use App\Model\Administracion\ParametroConstante;
use App\Model\Administracion\ProcesoJudicial;
use App\Model\Configuracion\Area;
use App\Model\Configuracion\Ciudad;
use App\Model\Configuracion\Departamento;
use App\Model\Configuracion\Despacho;
use App\Model\Configuracion\Empresa;
use App\Model\Configuracion\Juzgado;
use App\Model\Seguridad\Usuario;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;

class ActuacionImport implements ToCollection, WithStartRow, SkipsOnFailure, SkipsOnError
{

    use Importable, SkipsFailures;

    private $usuarioId;
    private $usuarioNombre;
    private $importedRows = 0;
    private $withErrors = false;
    private $errors = [];
    private $imported = [];

    public function __construct($usuarioId, $usuarioNombre){
        $this->usuarioId = $usuarioId;
        $this->usuarioNombre = $usuarioNombre;
    }

    /**
     * Cargar colección de actuaciones
     * @param Collection $rows
     */
    public function collection(Collection $rows)
    {
        // Parametros
        $parametros = ParametroConstante::cargarParametros();

        // Procesar datos
        $imported = [];
        $actuaciones = [];
        foreach ($rows as $rowKey => $row) {
            $row = $row->toArray();
            // Validaciones iniciales
            $mensajes = [
                '0.string' => 'El campo ciudad debe ser una cadena de texto.',
                '0.exists' => 'Ciudad actual no válida.',
                '1.string' => 'El campo juzgado debe ser una cadena de texto.',
                '1.exists' => 'Juzgado actual no válido.',
                '2.string' => 'El campo despacho debe ser una cadena de texto.',
                '2.exists' => 'Despacho actual no válido.',
                '4.string' => 'El campo demandantes debe ser una cadena de texto.',
                '4.required' => 'Los demandantes son requeridos.',
                '5.string' => 'El campo demandados debe ser una cadena de texto.',
                '5.required' => 'Los demandados es requeridos.',
                '6.string' => 'El campo actuación debe ser una cadena de texto.',
                '7.string' => 'El campo anotación debe ser una cadena de texto.',
                '8.required' => 'La fecha de registro es requerida.',
                '8.date_format' => 'El formato de fecha de registro es incorrecto.',
                '9.required' => 'La fecha de actuación es requerida.',
                '9.date_format' => 'El formato de fecha de actuación es incorrecto.',
                '10.date_format' => 'El formato de fecha de inicio termino es incorrecto.',
                '11.date_format' => 'El formato de fecha de vencimiento termino es incorrecto.',
                '12.string' => 'El campo tipo/clase de proceso debe ser una cadena de texto.'
            ];
            $validator = Validator::make($row, [
                '0' => 'string|exists:ciudades,nombre',
                '1' => 'string|exists:juzgados,nombre',
                '2' => function($attribute, $value, $onFailure) {
                    $conError = false;
                    if(!isset($value)){
                        $conError = true;
                        $mensaje = 'Número de despacho no válido.';
                    }
                    if($conError){
                        $onFailure($mensaje);
                    }
                },
                '3' => [
                    function($attribute, $value, $onFailure) {
                        $mensajes = [];
                        $conError = false;
                        // Validar longitud de caracteres
                        if (!(isset($value) && (strlen($value) == 23 || strlen($value) == 9 || ($value == 0)))) {
                            $conError = true;
                            array_push($mensajes, 'Tamaño de número de proceso errado. Debe tener 9 ó 23 dígitos.');
                        }
                        // Validar que sea numerico
                        if (!is_numeric($value)) {
                            $conError = true;
                            array_push($mensajes, 'Número de proceso solo debe tener caracteres numéricos.');
                        }

                        if(isset($value) && strlen($value) == 23){
                            // Validar datos número de proceso
                            $codigoDepartamento = substr($value,0,2);
                            $codigoCiudad = substr($value,0,5);
                            $codigoJuzgado = substr($value,0,9);
                            $numeroJuzgado = substr($value,5,2);
                            $numeroSala = substr($value,7,2);
                            $numeroDespacho = substr($value,9,3);
                            $anio = +substr($value,12,4);
                            $numeroRadicado = substr($value,16,5);
                            $consecutivo = substr($value,21,2);

                            $departamento = Departamento::findBy($codigoDepartamento);
                            if(!isset($departamento)){
                                $conError = true;
                                array_push($mensajes, 'Código departamento no válido.');
                            }
                            $ciudad = Ciudad::findBy($codigoCiudad);
                            if(!isset($ciudad)){
                                $conError = true;
                                array_push($mensajes, 'Código ciudad no válido.');
                            }
                            $juzgado = Juzgado::findBy($codigoJuzgado);
                            if(!isset($juzgado)){
                                $conError = true;
                                array_push($mensajes, 'Juzgado rama judicial no válido.');
                            }
                            $despacho = null;
                            if(isset($departamento) && isset($ciudad)){
                                $despacho = Despacho::where([
                                    'departamento_id' => $departamento->id,
                                    'ciudad_id' => $ciudad->id,
                                    'juzgado' => $numeroJuzgado,
                                    'sala' => $numeroSala,
                                    'despacho' => $numeroDespacho
                                ])->first();
                            }
                            if(!isset($despacho)){
                                $conError = true;
                                array_push($mensajes, 'Número despacho no válido.');
                            }
                            $fechaActual = Carbon::now();
                            $fechaProceso = Carbon::now()->setYear($anio);
                            if($fechaActual->diffInYears($fechaProceso) > 40 || $fechaActual->year < $anio){
                                $conError = true;
                                array_push($mensajes, 'Año proceso incorrecto.');
                            }
                            if(!(intval($numeroRadicado) >= 1 && intval($numeroRadicado) <= 99999)){
                                $conError = true;
                                array_push($mensajes, 'Número de radicado incorrecto.');
                            }
                            if(!(intval($consecutivo) >= 0 && intval($consecutivo) <= 99)){
                                $conError = true;
                                array_push($mensajes, 'Número de consecutivo incorrecto.');
                            }
                        }

                        if(isset($value) && strlen($value) == 9){
                            $anio = +substr($value,0,4);
                            $numeroRadicado = substr($value,5,5);
                            $fechaActual = Carbon::now();
                            $fechaProceso = Carbon::now()->setYear($anio);
                            if($fechaActual->diffInYears($fechaProceso) > 40 || $fechaActual->year < $anio){
                                $conError = true;
                                array_push($mensajes, 'Año proceso incorrecto.');
                            }
                            if(!(intval($numeroRadicado) >= 1 && intval($numeroRadicado) <= 99999)){
                                $conError = true;
                                array_push($mensajes, 'Número de radicado incorrecto.');
                            }
                        }

                        if($conError){
                            $onFailure(join("||", $mensajes));
                        }
                    }
                ],
                '4' => 'string|required',
                '5' => 'string|required',
                '6' => 'string|nullable',
                '7' => 'string|nullable',
                '8' => 'required|date_format:Y-m-d',
                '9' => 'required|date_format:Y-m-d',
                '10' => 'nullable|date_format:Y-m-d',
                '11' => 'nullable|date_format:Y-m-d',
                '12' => 'string|nullable'
            ], $mensajes);
            if($validator->fails()){
                foreach ($validator->errors()->messages() as $messages) {
                    foreach ($messages as $error) {
                        $row['key'] = $rowKey;
                        $row['observacion'] = $error;
                        $this->errors[] = $row;
                    }
                }

                continue;
            }

            // Obtener los datos requeridos desde el número del proceso
            if(strlen($row[3]) == 23){
                $codigoDepartamento = substr($row[3],0,2);
                $codigoCiudad = substr($row[3],0,5);
                $codigoJuzgado = substr($row[3],0,9);
                $numeroJuzgado = substr($row[3],5,2);
                $numeroSala = substr($row[3],7,2);
                $numeroDespacho = substr($row[3],9,3);
                $anio = +substr($row[3],12,4);
                $numeroRadicado = substr($row[3],16,5);
                $consecutivo = substr($row[3],21,2);
                // Departamento de origen
                $departamentoOrigen = Departamento::findBy($codigoDepartamento);
                // Obtener la ciudad de origen
                $ciudadOrigen = Ciudad::findBy($codigoCiudad);
                // Obtener el juzgado de origen
                $juzgadoOrigen = Juzgado::findBy($codigoJuzgado);
                // Obtener el despacho de origen
                $despachoOrigen = Despacho::where([
                    'departamento_id' => $departamentoOrigen->id,
                    'ciudad_id' => $ciudadOrigen->id,
                    'juzgado' => $numeroJuzgado,
                    'sala' => $numeroSala,
                    'despacho' => $numeroDespacho
                ])->first();
            }else if(strlen($row[3]) == 9){
                $anio = +substr($row[3],0,4);
                $numeroRadicado = substr($row[3],4,5);
                $consecutivo = '00';
            }

            // Obtener la ciudad actual
            $ciudadActual = Ciudad::where('nombre', $row[0])->first();
            // Obtener el juzgado actual
            $juzgadoActual = null;
            if(isset($ciudadActual)){
                $juzgadoActual = Juzgado::where('nombre', $row[1])->where('ciudad_id', $ciudadActual->id)->first();
            }
            // Obtener el despacho
            $despachoActual = null;
            $numeroDespachoActual = sprintf("%03d", $row[2]);
            if(isset($ciudadActual) && isset($juzgadoActual)){
                $despachoActual = Despacho::where([
                    'ciudad_id' => $ciudadActual->id,
                    'juzgado' => $juzgadoActual->juzgado,
                    'sala' => $juzgadoActual->sala,
                    'despacho' => $numeroDespachoActual
                ])->first();
            }

            // Realizar validaciones dependientes entre columnas
            $validator = Validator::make($row, [
                '0' => [
                    function ($attribute, $value, $fail) use($ciudadActual) {
                        if(!isset($ciudadActual)){
                            $fail('Ciudad actual no válida.');
                        }
                    }
                ],
                '1' => [
                    function ($attribute, $value, $fail) use($juzgadoActual) {
                        if(!isset($juzgadoActual)){
                            $fail('Juzgado actual no válido.');
                        }
                    }
                ],
                '2' => [
                    function ($attribute, $value, $fail) use($despachoActual) {
                        if(!isset($despachoActual)){
                            $fail('Número despacho no válido.');
                        }
                    }
                ]
            ]);
            if($validator->fails()){
                foreach ($validator->errors()->messages() as $messages) {
                    foreach ($messages as $error) {
                        $row['key'] = $rowKey;
                        $row['observacion'] = $error;
                        $this->errors[] = $row;
                    }
                }

                continue;
            }

            // Obtener el departamento actual
            $departamentoActual = Departamento::find($ciudadActual->departamento_id);

            // Obtener el numero proceso de trabajo
            // Si es ProcesoRamaJudicial (23 digitos) se toma el de excel,
            // Si es ProcesoEstadoCartelera se forma con los datos del excel
            $numeroProcesoTrabajo = $row[3];
            if(strlen($row[3]) == 9){
                $numeroProcesoTrabajo = $ciudadActual->codigo_dane . $juzgadoActual->juzgado . $juzgadoActual->sala .
                    $despachoActual->despacho . $row[3] . '00';
            }

            // Obtener los procesos judiciales de las diferentes compañías
            $procesosJudiciales = null;
            if(strlen($row[3]) == 9){
                $procesosJudiciales =
                    ProcesoJudicial::where('numero_proceso', $numeroProcesoTrabajo)
                        ->where('juzgado_id', $juzgadoActual->id)
                        ->where('nombres_demandantes', 'like', "%" . $row[4] . "%")
                        ->get();
                if(!(isset($procesosJudiciales) && count($procesosJudiciales) > 0)){
                    $procesosJudiciales =
                        ProcesoJudicial::where('numero_proceso', $numeroProcesoTrabajo)
                            ->where('instancia_judicial_actual', 'S')
                            ->get();
                }
            }else if(strlen($row[3]) == 23){
                $procesosJudiciales =
                    ProcesoJudicial::where('numero_radicado_proceso_actual', $numeroProcesoTrabajo)
                        ->where('juzgado_id', $juzgadoActual->id)
                        ->where('nombres_demandantes', 'like', "%" . $row[4] . "%")
                        ->get();
                if(!(isset($procesosJudiciales) && count($procesosJudiciales) > 0)){
                    $procesosJudiciales =
                        ProcesoJudicial::where('numero_radicado_proceso_actual', $numeroProcesoTrabajo)
                            ->where('instancia_judicial_actual', 'S')
                            ->get();
                }
            }

            // Crear proceso para la empresa prestadora del servicio si no se encontraron anteriormente
            if(!(isset($procesosJudiciales) && count($procesosJudiciales) > 0) && strlen($row[3]) >= 9){
                // Formar numero proceso
                if(strlen($row[3]) == 9){
                    $numeroProceso = $numeroProcesoTrabajo;
                }else{
                    $numeroProceso = $ciudadActual->codigo_dane . $juzgadoActual->juzgado . $juzgadoActual->sala .
                        $despachoActual->despacho . substr($row[3], 12, 9) . '00';
                }

                // Empresa prestadora del servicio
                $empresaServicio = Empresa::find($parametros['EMPRESA_PRESTADORA_SERVICIO_ID']);

                // Area por defecto de la empresa de servicio
                $area = Area::where('nombre', $parametros['AREA_EMPRESA_POR_DEFECTO'] ?? null)
                    ->where('empresa_id', $parametros['EMPRESA_PRESTADORA_SERVICIO_ID'])->first();

                // Administrador de la empresa de servicio
                $administrador = Usuario::query()
                    ->join('model_has_roles', function ($join) use($parametros) {
                        $join->on('model_has_roles.model_id', '=', 'usuarios.user_id')
                            ->where('model_has_roles.role_id', $parametros['ADMINISTRADOR_EMPRESA_ROL_ID'] ?? null);
                    })
                    ->select('usuarios.*')
                    ->where('usuarios.empresa_id','=', $parametros['EMPRESA_PRESTADORA_SERVICIO_ID'])
                    ->first();

                $procesoJudicial = ProcesoJudicial::create([
                    'departamento_dane' => $departamentoActual->codigo_dane,
                    'ciudad_dane' => $ciudadActual->codigo_dane,
                    'juzgado_actual' => $juzgadoActual->juzgado_rama,
                    'juzgado_origen_rama' => strlen($row[3]) == 9 ? $juzgadoActual->juzgado_rama : $codigoJuzgado,
                    'despacho_numero' => $despachoActual->despacho,
                    'despacho_origen_numero' => strlen($row[3]) == 9 ? $despachoActual->despacho : $numeroDespacho,
                    'anio_proceso' => $anio,
                    'numero_radicado' => $numeroRadicado,
                    'numero_consecutivo' => $consecutivo,
                    'numero_proceso' => $numeroProceso,
                    'numero_radicado_proceso_actual' => strlen($row[3]) == 9 ? $numeroProcesoTrabajo : $row[3],
                    'proceso_id_referencia' => null,
                    'instancia_judicial_actual' => 'S',
                    'nombres_demandantes' => $row[4],
                    'nombres_demandados' => $row[5],
                    'observaciones' => null,
                    'indicativo_pertenece_rama' => $ciudadActual->rama_judicial == 1 ? 'S' : 'N',
                    'indicativo_consulta_rama' => '0',
                    'indicativo_tiene_vigilancia' => '0',
                    'indicativo_estado' => '1',
                    'descripcion_clase_proceso' => $row[12],
                    'empresa_id' => $parametros['EMPRESA_PRESTADORA_SERVICIO_ID'],
                    'zona_id' => $empresaServicio->zona_id ?? null,
                    'area_id' => $area->id ?? null,
                    'cliente_id' => null,
                    'abogado_responsable_id' => $administrador->id ?? null,
                    'dependiente_asignado_id' => null,
                    'ultima_clase_proceso_id' => null,
                    'ultima_etapa_proceso_id' => null,
                    'ultimo_tipo_actuacion_id' => null,
                    'departamento_id' => $departamentoActual->id,
                    'ciudad_id' => $ciudadActual->id,
                    'juzgado_id' => $juzgadoActual->id,
                    'juzgado_origen_id' => strlen($row[3]) == 9 ? $juzgadoActual->id : $juzgadoOrigen->id,
                    'despacho_id' => $despachoActual->id,
                    'despacho_origen_id' => strlen($row[3]) == 9 ? $despachoActual->id : $despachoOrigen->id,
                    'usuario_creacion_id' => $this->usuarioId,
                    'usuario_creacion_nombre' => $this->usuarioNombre,
                    'usuario_modificacion_id' => $this->usuarioId,
                    'usuario_modificacion_nombre' => $this->usuarioNombre
                ]);
                if(isset($procesoJudicial)){
                    $procesoJudicial->proceso_id_referencia = $procesoJudicial->id;
                    $procesoJudicial->save();
                }

                $procesosJudiciales = [$procesoJudicial];
            }

            // Generar las actuaciones para los procesos encontrados
            foreach ($procesosJudiciales ?? [] as $procesoJudicial){
                // Registrar en los importados
                array_push($imported, [
                    'numero_proceso' => $row[3],
                    'descripcion_actuacion' => $row[6],
                    'descripcion_anotacion' => $row[7],
                    'fecha_registro' => $row[8],
                    'fecha_actuacion' => $row[9],
                    'fecha_inicio_termino' => $row[10],
                    'fecha_vencimiento_termino' => $row[11],
                    'descripcion_clase_proceso' => $row[12],
                    'nombres_demandantes' => $row[4],
                    'nombres_demandados' => $row[5],
                    'ciudad_nombre' => $row[0],
                    'juzgado_nombre' => $row[1],
                    'despacho_nombre' => $row[2],
                    'observacion' => "Registro correcto.",
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'usuario_creacion_id' => $this->usuarioId,
                    'usuario_creacion_nombre' => $this->usuarioNombre,
                    'usuario_modificacion_id' => $this->usuarioId,
                    'usuario_modificacion_nombre' => $this->usuarioNombre
                ]);

                // Registro de actuaciones
                array_push($actuaciones, [
                    'numero_proceso' => $procesoJudicial->numero_proceso,
                    'numero_radicado_proceso_actual' => $procesoJudicial->numero_radicado_proceso_actual,
                    'actuacion' => $row[6],
                    'anotacion' => $row[7],
                    'fecha_registro' => $row[8],
                    'fecha_actuacion' => $row[9],
                    'fecha_inicio_termino' => $row[10],
                    'fecha_vencimiento_termino' => $row[11],
                    'descripcion_clase_proceso' => $row[12],
                    'ind_actuacion_audiencia' => null,
                    'tipo_movimiento' => strlen($row[3]) == 9 ? 'AC' : 'AR',
                    'nombres_demandantes' => $row[4],
                    'nombres_demandados' => $row[5],
                    'observaciones' => null,
                    'proceso_id' => $procesoJudicial->proceso_id_referencia,
                    'ciudad_id' => $procesoJudicial->ciudad_id,
                    'juzgado_id' => $procesoJudicial->juzgado_id,
                    'juzgado_origen_id' => $procesoJudicial->juzgado_origen_id,
                    'despacho_id' => $procesoJudicial->despacho_id,
                    'despacho_origen_id' => $procesoJudicial->despacho_origen_id,
                    'empresa_id' => $procesoJudicial->empresa_id,
                    'etapa_proceso_id' => null,
                    'tipo_actuacion_id' => null,
                    'tipo_notificacion_id' => null,
                    'nombre_archivo_anexo' => null,
                    'ruta_archivo_anexo' => null,
                    'ruta_archivo_estado_cartelera' => null,
                    'nombre_archivo_estado_cartelera' => null,
                    'estado' => '1',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'usuario_creacion_id' => $this->usuarioId,
                    'usuario_creacion_nombre' => $this->usuarioNombre,
                    'usuario_modificacion_id' => $this->usuarioId,
                    'usuario_modificacion_nombre' => $this->usuarioNombre
                ]);
            }

            if((strlen($row[3]) == 1 && $row[3] == 0) && !isset($procesosJudiciales)){
                // Registrar en los importados
                array_push($imported, [
                    'numero_proceso' => $row[3],
                    'descripcion_actuacion' => $row[6],
                    'descripcion_anotacion' => $row[7],
                    'fecha_registro' => $row[8],
                    'fecha_actuacion' => $row[9],
                    'fecha_inicio_termino' => $row[10],
                    'fecha_vencimiento_termino' => $row[11],
                    'descripcion_clase_proceso' => $row[12],
                    'nombres_demandantes' => $row[4],
                    'nombres_demandados' => $row[5],
                    'ciudad_nombre' => $row[0],
                    'juzgado_nombre' => $row[1],
                    'despacho_nombre' => $row[2],
                    'observacion' => "Registro correcto.",
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'usuario_creacion_id' => $this->usuarioId,
                    'usuario_creacion_nombre' => $this->usuarioNombre,
                    'usuario_modificacion_id' => $this->usuarioId,
                    'usuario_modificacion_nombre' => $this->usuarioNombre
                ]);

                // Registro de actuaciones
                array_push($actuaciones, [
                    'numero_proceso' => $numeroProcesoTrabajo,
                    'numero_radicado_proceso_actual' => $numeroProcesoTrabajo,
                    'actuacion' => $row[6],
                    'anotacion' => $row[7],
                    'fecha_registro' => $row[8],
                    'fecha_actuacion' => $row[9],
                    'fecha_inicio_termino' => $row[10],
                    'fecha_vencimiento_termino' => $row[11],
                    'descripcion_clase_proceso' => $row[12],
                    'ind_actuacion_audiencia' => null,
                    'tipo_movimiento' => 'AC',
                    'nombres_demandantes' => $row[4],
                    'nombres_demandados' => $row[5],
                    'observaciones' => null,
                    'proceso_id' => null,
                    'ciudad_id' => $ciudadActual->id,
                    'juzgado_id' => $juzgadoActual->id,
                    'juzgado_origen_id' => $juzgadoActual->id,
                    'despacho_id' => $despachoActual->id,
                    'despacho_origen_id' => $despachoActual->id,
                    'empresa_id' => $parametros['EMPRESA_PRESTADORA_SERVICIO_ID'],
                    'etapa_proceso_id' => null,
                    'tipo_actuacion_id' => null,
                    'tipo_notificacion_id' => null,
                    'nombre_archivo_anexo' => null,
                    'ruta_archivo_anexo' => null,
                    'ruta_archivo_estado_cartelera' => null,
                    'nombre_archivo_estado_cartelera' => null,
                    'estado' => '1',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'usuario_creacion_id' => $this->usuarioId,
                    'usuario_creacion_nombre' => $this->usuarioNombre,
                    'usuario_modificacion_id' => $this->usuarioId,
                    'usuario_modificacion_nombre' => $this->usuarioNombre
                ]);
            }

            // Contador de filas importadas
            $this->importedRows++;
        }

        // Registros importados del excel
        $this->imported = $imported;

        // Cargar datos
        $actuaciones = collect($actuaciones);
        $chunks = $actuaciones->chunk(100);
        foreach ($chunks as $chunk){
            Actuacion::insert($chunk->toArray());
        }
    }

    /**
     * Fila de inicio
     * @return int
     */
    public function startRow(): int
    {
        return 2;
    }

    /**
     * Renombrar columnas
     * @return array
     */
    public function customValidationAttributes()
    {
        return [
            '0' => 'CIUDAD',
            '1' => 'JUZGADO',
            '2' => 'DESPACHO',
            '3' => 'NUMERO PROCESO',
            '4' => 'DEMANDANTES',
            '5' => 'DEMANDADOS',
            '6' => 'ACTUACION',
            '7' => 'ANOTACION',
            '8' => 'FECHA REGISTRO',
            '9' => 'FECHA ACTUACION',
            '10' => 'FECHA INICIO TERMINO',
            '11' => 'FECHA VENCIMIENTO TERMINO',
            '12' => 'TIPO/CLASE DE PROCESO'
        ];
    }

    /**
     * Mensajes personailizados
     * @return array
     */
    public function customValidationMessages()
    {
        return [
            '0.exists' => 'Ciudad actual no válida.',
            '1.exists' => 'Juzgado actual no válido.',
            '2.exists' => 'Despacho actual no válido.'
        ];
    }

    /**
     * @param \Throwable $e
     * @return int
     */
    public function onError(\Throwable $e)
    {
        return $this->withErrors = true;
    }

    /**
     * Obtener cantidas filas insertadas
     * @return int
     */
    public function getImportedRows(): int
    {
        return $this->importedRows;
    }

    /**
     * Obtener cantidas filas insertadas
     * @return bool
     */
    public function getWithErrors(): bool
    {
        return $this->withErrors;
    }

    /**
     * Obtener los errores personalizados
     * @return array
     */
    public function getCustomErrors(): array
    {
        return $this->errors;
    }

    /**
     * Obtener las filas importadas
     * @return array
     */
    public function getImported(): array
    {
        return $this->imported;
    }

}
