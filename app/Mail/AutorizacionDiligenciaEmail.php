<?php

namespace App\Mail;

use App\Model\Administracion\ParametroConstante;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AutorizacionDiligenciaEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $empresa;
    protected $diligencia;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($empresa, $diligencia)
    {
        $this->empresa = $empresa;
        $this->diligencia = $diligencia;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // Constantes
        $parametros = ParametroConstante::cargarParametros();

        $mail = $this->view('mail.autorizacion-diligencia', [
            "diligencia" => $this->diligencia
        ])
        ->subject("Solicitud aprobación diligencia proceso " . $this->diligencia['proceso_judicial']['numero_proceso'] ?? '');

        if(isset($parametros['CORREO_RESPUESTA_DILIGENCIA'])){
            $mail->replyTo($parametros['CORREO_RESPUESTA_DILIGENCIA']);
        }

        return $mail;
    }
}
