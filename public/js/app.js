(function() {
    'use strict';

    angular
        .module('enlaces.core', [
            'environment',
            'ng', //From the selectize XD
            'ngRoute',
            'angular-jwt',
            'ngAnimate',
            'perfect_scrollbar',
            'angular-loading-bar',
            'cgBusy',
            'ui.bootstrap',
            'smart-table',
            'ngFileUpload',
            'pascalprecht.translate',
            'ngSanitize'
        ]);

    angular
        .module('enlaces.app', [
            /* Shared modules */
            'enlaces.core'
        ]);

})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .factory('httpResponseInterceptor', ['$q', '$rootScope', '$location', 'Constantes', function($q, $rootScope, $location, Constantes) {
            return {
                responseError: function(rejection) {
                    if (rejection.status === Constantes.Response.HTTP_UNAUTHORIZED) {
                        localStorage.clear();
                        window.location = '/login';
                    }
                    return $q.reject(rejection);
                }
            };
        }]);

    angular
        .module('enlaces.app')
        .config(function(envServiceProvider) {
            // set the domains and variables for each environment
            envServiceProvider.config({
                domains: {
                    development: ['localhost', 'enlaces.test'],
                    production: ['pcl.legal', 'mle.legal'],
                    test: ['enlaces.assis.com.co'],
                    enlaces: ['litisdata.assis.com.co'],
                    litisdata: ['server2021.litisdata.com', 'litisdata.com']
                },
                vars: {
                    development: {
                        apiUrl: 'http://enlaces.test/api/'
                    },
                    production: {
                        apiUrl: 'https://enlaces.legal/api/'
                    },
                    enlaces: {
                        apiUrl: 'https://litisdata.assis.com.co/api/'
                    },
                    litisdata: {
                        apiUrl: 'https://server2021.litisdata.com/api/'
                    },
                    test: {
                        apiUrl: 'https://enlaces.assis.com.co/api/'
                    },
                    defaults: {
                        apiUrl: 'https://app.enlaces.com/api/'
                    }
                }
            });

            envServiceProvider.check();
        })
        .config(function($sceDelegateProvider) {
            $sceDelegateProvider.resourceUrlWhitelist([
                // Allow same origin resource loads.
                'self',
                // Allow loading from our assets domain. **.
                'https://enlaces.test/**',
                'https://pcl.legal/**',
                'https://test.pcl.legal/**'
            ])
        })
        .config(function($httpProvider, jwtOptionsProvider) {
            jwtOptionsProvider.config({
                authPrefix: localStorage.getItem('tokenType') + " ",
                tokenGetter: function() {
                    return localStorage.getItem('token');
                },
                whiteListedDomains: [
                    'localhost',
                    'enlaces.test',
                    'local.enlaces',
                    'pcl.legal',
                    'mle.legal',
                    'test.pcl.legal',
                    'test.mle.legal'
                ]
            });

            $httpProvider.interceptors.push('jwtInterceptor');
            $httpProvider.interceptors.push('httpResponseInterceptor');
        })
        .config(function(datetimepickerProvider) {
            datetimepickerProvider.setOptions({
                locale: 'en'
            });
        })
        .config(function(cfpLoadingBarProvider) {
            cfpLoadingBarProvider.includeSpinner  = false;
        })
        .config( function($translateProvider) {
            // TODO @Javier tomar de la configuración del usuario
            var locale = "es_a";

            $translateProvider
                .useStaticFilesLoader({
                    prefix: '/i18n/',
                    suffix: '.json'
                })
                .preferredLanguage(locale ? locale : "es_a")
                .useMissingTranslationHandlerLog();
        });

    angular
        .module('enlaces.app')
        .run(runBlock);

    runBlock.$inject = [
        '$http',
        '$rootScope',
        '$location',
        '$anchorScroll',
        'envService',
        'jwtHelper'
    ];

    function runBlock($http, $rootScope, $location, $anchorScroll, envService, jwtHelper) {
        /*$http.get(envService.read('apiUrl') + "users/current/session")
        .then(function (response){
            localStorage.setItem('session', JSON.stringify(response.data));
        })
        .catch(function (error){
            // TODO Darle manejo de errores de sessión
        });*/

        $rootScope.$on('$locationChangeStart', function() {
            var token = localStorage.getItem('token');
            var isTokenExpired = token ? jwtHelper.isTokenExpired(token) : true;
            if(isTokenExpired){
                localStorage.clear();
                window.location = '/login';
            }
        });

        $rootScope.$on('$routeChangeSuccess', function(newRoute, oldRoute) {
            if($location.hash()) $anchorScroll();
        });
    }

})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .config(function($routeProvider, envServiceProvider) {
            $routeProvider
                .when('/', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'roles/index-template',
                    controller: 'rolController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                // Seguridad
                .when('/roles', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'roles/index-template',
                    controller: 'rolController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })
                .when('/usuarios', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'usuarios/index-template',
                    controller: 'usuarioController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })
                // Opciones del sistema
                .when('/opciones-del-sistema', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'opciones-del-sistema/index-template',
                    controller: 'opcionSistemaController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })
                // Permisos
                .when('/roles/:id/permisos', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'roles/null/permisos/create',
                    controller: 'permisoEditController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })
                .when('/auditoria-maestros', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'auditoria-maestros/index-template',
                    controller: 'auditoriaMaestroController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                // Configuración
                .when('/servicios', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'servicios/index-template',
                    controller: 'servicioController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/tipos-de-empresa', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'tipos-de-empresa/index-template',
                    controller: 'tipoDeEmpresaController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/clases-de-procesos', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'clases-de-procesos/index-template',
                    controller: 'claseProcesoController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/etapas-de-procesos', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'etapas-de-procesos/index-template',
                    controller: 'etapaProcesoController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/departamentos', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'departamentos/index-template',
                    controller: 'departamentoController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/etapas-de-la-clase-de-proceso', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'etapa-clase-procesos/index-template',
                    controller: 'etapaClaseProcesoController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/zonas', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'zonas/index-template',
                    controller: 'zonaController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/ciudades', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'ciudades/index-template',
                    controller: 'ciudadController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/juzgados', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'juzgados/index-template',
                    controller: 'juzgadoController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/tipos-de-documento', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'tipos-documentos/index-template',
                    controller: 'tipoDocumentoController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/despachos', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'despachos/index-template',
                    controller: 'despachoController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/empresas', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'empresas/index-template',
                    controller: 'empresaController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/empresas/:id/areas', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'areas/index-template',
                    controller: 'areaController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/empresas/areas', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'areas/index-template',
                    controller: 'areaController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/empresas/:id/planes', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'planes/index-template',
                    controller: 'planController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/conceptos-economicos', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'conceptos-economicos/index-template',
                    controller: 'conceptoEconomicoController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/copia-conceptos-economicos', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'copia-conceptos-economicos/index-template',
                    controller: 'copiaConceptoEconomicoController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/empresas/:empresaId/usuarios', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'usuarios/index-template',
                    controller: 'usuarioController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/tipos-de-diligencias', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'tipos-diligencias/index-template',
                    controller: 'tipoDiligenciaController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/empresas/:id/coberturas', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'coberturas/index-template',
                    controller: 'coberturaController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/tarifas-diligencias', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'tarifas-diligencias/index-template',
                    controller: 'tarifaDiligenciaController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/clientes', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'clientes/index-template',
                    controller: 'clienteController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/tipos-de-actuaciones', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'tipos-actuaciones/index-template',
                    controller: 'tipoActuacionController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })
                .when('/mis-procesos-judiciales', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'procesos-judiciales/index-template',
                    controller: 'procesoJudicialController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })
                .when('/mis-procesos-judiciales/:id/valores', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'procesos-valores/index-template',
                    controller: 'procesoValorController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })
                .when('/mis-procesos-judiciales/importacion', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'procesos-judiciales/importar-template',
                    controller: 'procesoJudicialImportarController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })
                .when('/actuaciones', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'actuaciones/index-template',
                    controller: 'actuacionController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })
                .when('/actuaciones/importacion', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'actuaciones/importar-template',
                    controller: 'actuacionImportarController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/usuarios-clientes/:id/procesos-cliente', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'procesos-clientes/index-template',
                    controller: 'procesoClienteController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/diligencias', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'diligencias/index-template',
                    controller: 'diligenciaController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/diligencias-tramite', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'diligencias/tramite/template',
                    controller: 'diligenciaController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/diligencias/:id/formulario', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'diligencias/create',
                    controller: 'diligenciaEditController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/diligencias-tramite/:id/formulario', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'diligencias/create',
                    controller: 'diligenciaEditController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/diligencias/:id/historia', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'diligencias/historia/template',
                    controller: 'diligenciaHistoriaController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })
                .when('/diligencias/:diligencia_id/historia/:id/formulario', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'diligencias/historia/create',
                    controller: 'diligenciaHistoriaEditController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/diligencias/cliente', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'diligencias/consulta-cliente/template',
                    controller: 'diligenciaClienteController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/diligencias/formulario-cliente', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'diligencias/form-cliente/template',
                    controller: 'diligenciaClienteEditController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/tipos-de-notificacion', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'tipos-notificaciones/index-template',
                    controller: 'tipoNotificacionController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/mis-procesos-en-vigilancia', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'vigilancias-procesos/index-template',
                    controller: 'vigilanciaProcesoController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/salario-minimo', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'salarios-minimos/index-template',
                    controller: 'salarioMinimoController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/TES', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'TES/index-template',
                    controller: 'tesController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/IPC', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'IPC/index-template',
                    controller: 'ipcController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/auxiliares-de-la-justicia', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'auxiliares-justicia/index-template',
                    controller: 'auxiliarJusticiaController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/auxiliares-de-la-justicia/importacion', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'auxiliares-justicia/importar-template',
                    controller: 'auxiliarJusticiaImportarController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/mis-procesos-hoy', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'actuaciones/procesos-hoy/template',
                    controller: 'procesoHoyController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/procesos-por-juzgado-fecha', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'actuaciones/procesos-juzgado-fecha/template',
                    controller: 'procesoJuzgadoFechaController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/buscar-demandante-demandado', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'actuaciones/demandantes-demandados/template',
                    controller: 'busquedaDemandanteDemandadoController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/procesos-por-area', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'procesos-areas/index-template',
                    controller: 'procesoAreaController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/informacion-general-procesos', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'procesos-judiciales/procesos-generales/template',
                    controller: 'procesoGeneralController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/consulta-autos', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'actuaciones/autos/template',
                    controller: 'consultaAutoController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/consulta-remates', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'remates/template',
                    controller: 'consultaRemateController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/consulta-remanentes', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'remanentes/template',
                    controller: 'consultaRemanenteController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/consulta-reporte-judicial', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'remanentes/template',
                    controller: 'consultaRemanenteController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/consulta-personalizado-digital', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'actuaciones/personalizados-digitales/template',
                    controller: 'consultaPersonalizadoDigitalController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/audiencia-pendiente', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'actuaciones/audiencias-pendientes/template',
                    controller: 'audienciaPendienteController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/audiencia-cumplida', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'actuaciones/audiencias-cumplidas/template',
                    controller: 'audienciaCumplidaController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/termino-actuacion', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'actuaciones/termino-actuacion/template',
                    controller: 'terminoActuacionController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/actuacion-cumplida', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'actuaciones/termino-actuacion/template',
                    controller: 'terminoActuacionController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/parametros-constantes', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'parametros-constantes/index-template',
                    controller: 'parametroConstanteController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/usuarios-clientes', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'usuarios/cliente/template',
                    controller: 'usuarioController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })

                .when('/auditoria-procesos', {
                    templateUrl: envServiceProvider.read("apiUrl") + 'auditoria-procesos/index-template',
                    controller: 'auditoriaProcesoController',
                    controllerAs: 'vm',
                    reloadOnSearch: false
                })


                .otherwise({redirectTo: '/'});
        });
})();

(function(angular) {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('navController', navController);

    navController.$inject = [
        '$http',
        'envService',
        '$uibModal',
        'sessionUtil',
        'messageUtil',
        'Constantes'
    ];

    function navController($http, envService, $uibModal,  sessionUtil, messageUtil, Constants){
        var nav = this;

        nav.session = sessionUtil.getAll();

        // Acciones
        nav.logout = logout;
        nav.infoEmpresa = infoEmpresa;

        function logout() {
            localStorage.removeItem('token');
            localStorage.removeItem('tokenType');
            window.location = '/login';
        }

        function infoEmpresa() {
            $uibModal.open({
                animation: true,

                templateUrl: envService.read('apiUrl') + 'infoEmpresa/template',
                controller: 'infoEmpresaController as vm',
                size: 'lg',
                resolve: {
                    empresa: nav.session.empresa.id
                }
            });
        }

    }
})(window.angular);

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('menuController', menuController);

    menuController.$inject = [
        'envService',
        'genericService',
        'sessionUtil',
        'Constantes'
    ];

    function menuController(envService, genericService, sessionUtil, Constantes) {
        var menu = this;
        menu.companyLogo = "https://www.logogenie.net/download/preview/medium/4168236";
        menu.companyName = "Enlaces judiciales";

        cargarMenu();

        function cargarMenu() {
            genericService.obtenerColeccionLigera("opciones-del-sistema",{
                arbol_de_opciones: true
            })
            .then(function (response){
                if(response.status === Constantes.Response.HTTP_OK){
                    var modulos = [],
                        permisosHijos,
                        submodulos,
                        opcionesDelSistemaAplicadas = [],
                        opcionesDelSistema = [].concat(response.data);

                    angular.forEach(opcionesDelSistema, function (opcion) {
                        submodulos = [];
                        if(opcion.hijos && opcion.hijos.length > 0){
                            // Validar si los submodulos están permitidos para el usuario
                            permisosHijos = [];
                            angular.forEach(opcion.hijos, function (hijo) {
                                if(hijo.clave_permiso && sessionUtil.can([hijo.clave_permiso])){
                                    // Marcar el titulo de la página en la URL
                                    if(hijo.url && _.includes(hijo.url, "?")){
                                        var partesUrl = hijo.url.split("?");
                                        hijo.url = partesUrl[0] + "?o=" + hijo.id + "&" + partesUrl[1];
                                    }else{
                                        hijo.url += "?o=" + hijo.id;
                                    }
                                    submodulos.push(hijo);
                                    permisosHijos.push(hijo.clave_permiso);
                                    opcionesDelSistemaAplicadas.push(hijo);
                                }
                            });

                            // Validar que se tenga al menos un permiso a los submodulos
                            if(permisosHijos && permisosHijos.length > 0){
                                opcion.submodulos = submodulos;
                                modulos.push(opcion);
                            }
                        }else if(opcion.clave_permiso && sessionUtil.can([opcion.clave_permiso])){
                            // Marcar el titulo de la página en la URL
                            if(opcion.url && _.includes(opcion.url, "?")){
                                var partesUrl = opcion.url.split("?");
                                opcion.url = partesUrl[0] + "?o=" + opcion.id + "&" + partesUrl[1];
                            }else{
                                opcion.url += "?o=" + opcion.id;
                            }
                            // Si el modulo no tiene hijos se ingresa como un modulo independiente
                            opcion.submodulos = submodulos;
                            modulos.push(opcion);
                            opcionesDelSistemaAplicadas.push(opcion);
                        }
                    });

                    // Guardar opciones aplicadas
                    localStorage.setItem('opcionesDelSistema', JSON.stringify(opcionesDelSistemaAplicadas));

                    // Establecer modulos
                    menu.modulos = modulos;
                }
            });
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('confirmarController', confirmarController);

    confirmarController.$inject = [
        '$uibModalInstance',
        'parametros'
    ];

    function confirmarController($uibModalInstance, parametros){
        var vm = this;
        vm.mensaje = parametros.mensaje;

        // Acciones
        vm.ok = ok;
        vm.cancelar = cancelar;

        function ok() {
            $uibModalInstance.close({
                ok: true
            });
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('infoEmpresaController', infoEmpresaController);

    infoEmpresaController.$inject = [
        '$uibModalInstance',
        'genericService',
        'messageUtil',
        'Constantes',
        'empresa'
    ];

    function infoEmpresaController(
        $uibModalInstance, genericService, messageUtil, Constantes, empresa
    ){
        var vm = this;

        //Variables para mostrar y ocultar
        vm.mostrarPlanes = false;
        vm.mostrarUsuarios = false;
        vm.mostrarAreas = false;

        //control grid
       // vm.cargarEmpresa = cargarEmpresa;
       // vm.obtenerPlanes = obtenerPlanes;
       // vm.obtenerUsuarios = obtenerUsuarios;
        //vm.obtenerAreas = obtenerAreas;

       // function cargarEmpresa() {
            genericService.cargar("empresas", empresa)
                .then(function (response) {
                    if (response.status === Constantes.Response.HTTP_OK) {
                        vm.empresa = response.data;
                    }
                });
        //}

      //  function obtenerPlanes() {
      //      if(!vm.cargandoPlanes) {
            //    vm.cargandoPlanes = true;
                genericService.obtener("planes", {
                    empresa_id: empresa,
                    simple: true
                }).then(function (response) {
                        if (response.status === Constantes.Response.HTTP_OK) {
                            var planes = response.data;
                            if (planes && planes.length > 0) {
                                vm.planes = [].concat(planes);
                            } else {
                                vm.planes = [];
                            }
                        }
                    }).finally(function () {
                    vm.cargandoPlanes = false;
                });
        //    }
       // }

/*        function obtenerUsuarios() {
            if(!vm.cargandoUsuarios) {
                vm.cargandoUsuarios = true;*/
                genericService.obtener("usuarios", {
                    empresa_id: empresa,
                    simple: true
                }).then(function (response) {
                        if (response.status === Constantes.Response.HTTP_OK) {
                            var usuarios = response.data;
                            if (usuarios && usuarios.length > 0) {
                                vm.usuarios = [].concat(usuarios);
                            } else {
                                vm.usuarios = [];
                            }
                        }
                    }).finally(function () {
                    vm.cargandoUsuarios = false;
                });
  /*          }
        }*/

/*        function obtenerAreas() {
            if(!vm.cargandoAreas) {
                vm.cargandoAreas = true;*/
                genericService.obtener("areas", {
                    empresa_id: empresa,
                    simple: true
                }).then(function (response) {
                        if (response.status === Constantes.Response.HTTP_OK) {
                            var areas = response.data;
                            if (areas && areas.length > 0) {
                                vm.areas = [].concat(areas);
                            } else {
                                vm.areas = [];
                            }
                        }
                    }).finally(function () {
                    vm.cargandoAreas = false;
                });
/*            }
        }*/

    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('rolController', rolController);

    rolController.$inject = [
        '$scope',
        '$timeout',
        '$routeParams',
        '$uibModal',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function rolController(
        $scope, $timeout, $routeParams, $uibModal, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "roles";

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.TipoRolEnum = Constantes.TipoRol;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.crear = crear;

        // Modulos externos
        vm.confirmarEliminar = confirmarEliminar;

        // Limpiar filtros
        limpiarFiltros();

        // Grid
        function obtenerResumen(tableState, ctrl){
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }
            if(pipePromise) $timeout.cancel(pipePromise);
            pipePromise = $timeout(function() {
                if(!vm.cargando){
                    vm.cargando = true;
                    genericService.obtenerColeccion(recurso, tableState)
                        .then(function (response){
                            if(response.status === Constantes.Response.HTTP_OK){
                                var coleccion = response.data.datos;
                                if(coleccion && coleccion.length > 0 ){
                                    vm.coleccion = [].concat(coleccion);
                                    vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                                    vm.paginacion.paginaActual = response.data.pagina_actual;
                                    vm.paginacion.desde = response.data.desde;
                                    vm.paginacion.hasta = response.data.hasta;
                                    vm.paginacion.total = response.data.total;
                                    tableState.pagination.numberOfPages = response.data.ultima_pagina;
                                }else{
                                    vm.coleccion = [];
                                }
                            }
                        }).finally(function () {
                            vm.cargando = false;
                        });
                }
            }, 500);
        }

        function limpiarFiltros(){
            vm.tipo = null;
            vm.nombre = null;
            $scope.isReset = true;
        }

        /*function cargarFiltros() {
            var filtros = gridUtil.getFormattedSearchPredicate("servicio");
            vm.nombre = filtros && filtros.nombre ? filtros.nombre : null;
        }*/

        // Acciones

        function eliminar(row){
            genericService.eliminar(recurso, row.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        $scope.$broadcast('refreshGrid');
                        messageUtil.success(response.data.mensajes[0]);
                    }else if(
                        response.status === Constantes.Response.HTTP_CONFLICT ||
                        response.status === Constantes.Response.HTTP_BAD_REQUEST
                    ){
                        messageUtil.error(response.data.mensajes[0]);
                    }
                });
        }

        // Modulos externos

        function crear(rol) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + recurso + '/create',
                controller: 'rolEditController as vm',
                size: 'md',
                resolve: {
                    rol: function () {
                        return rol;
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(rol && rol.id && response && response.id){
                    rol.nombre = response.nombre;
                    rol.estado = response.estado;
                    rol.tipo = response.tipo;
                    rol.usuario_creacion_id = response.usuario_creacion_id;
                    rol.usuario_creacion_nombre = response.usuario_creacion_nombre;
                    rol.usuario_modificacion_id = response.usuario_modificacion_id;
                    rol.usuario_modificacion_nombre = response.usuario_modificacion_nombre;
                }else if(response && response.id){
                    $scope.$broadcast('refreshGrid');
                }
            });
        }

        function confirmarEliminar(row) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + 'confirmar-template',
                controller: 'confirmarController as vm',
                size: 'md',
                resolve: {
                    parametros: function () {
                        return {
                            mensaje: "¿Estás seguro de que quieres eliminar el rol?"
                        };
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(response.ok){
                    eliminar(row);
                }
            });
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('rolEditController', rolEditController);

    rolEditController.$inject = [
        '$uibModalInstance',
        'genericService',
        'messageUtil',
        'Constantes',
        'rol'
    ];

    function rolEditController(
        $uibModalInstance, genericService, messageUtil, Constantes, rol
    ){
        var vm = this,
            recurso = "roles";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        // Init autocompletes
        initAutoCompleteTipoRol();

        if(rol && rol.id){
            cargar();
        }else{
            vm.estado = true;
        }

        function cargar() {
            genericService.cargar(recurso, rol.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var rol = response.data;
                        vm.nombre = rol.nombre;
                        vm.tipo = rol.tipo;
                        vm.estado = rol.estado;
                        vm.fechaCreacion = rol.fecha_creacion;
                        vm.fechaModificacion = rol.fecha_modificacion;
                        vm.usuarioCreacionNombre = rol.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = rol.usuario_modificacion_nombre;
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardando){
                vm.deshabilitarGuardando = true;
                var datos = {
                    id: rol ? +rol.id: null,
                    nombre: vm.nombre,
                    tipo: vm.tipo,
                    estado: vm.estado
                };

                var promesa = null;
                if(!(rol && rol.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        $uibModalInstance.close(response.data.datos);
                        messageUtil.success(response.data.mensajes[0]);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }
                }).finally(function () {
                    vm.deshabilitarGuardando = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }

        // Autocompletes

        function initAutoCompleteTipoRol() {
            vm.configTipoRol = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'id',
                allowEmptyOption: false,
                create: false,
                persist: true
            };
            vm.opcionesTipoRol = [
                {id: Constantes.TipoRol.CLIENTE, nombre: "Del cliente"},
                {id: Constantes.TipoRol.SISTEMA, nombre: "Del sistema"},
                {id: Constantes.TipoRol.CLIENTE_DE_CLIENTE, nombre: "Del cliente de mi cliente"}
            ];
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('usuarioController', usuarioController);

    usuarioController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        '$uibModal',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function usuarioController(
        $scope, $routeParams, $timeout, $uibModal, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "usuarios";

        vm.empresaId = $routeParams.empresaId ? $routeParams.empresaId : null;
        vm.usuarioCliente = $routeParams.cliente ? $routeParams.cliente : null;
        vm.titulo = $routeParams.o ? sessionUtil.getTituloVista($routeParams.o) : "Usuarios";

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.TipoRolEnum = Constantes.TipoRol;

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.crear = crear;
        vm.cambiarClave = cambiarClave;

        // Modulos externos
        vm.confirmarEliminar = confirmarEliminar;

        // Limpiar filtros
        limpiarFiltros();

        // Grid
        function obtenerResumen(tableState, ctrl){
            tableState.search = tableState.search || {};
            tableState.search.predicateObject = tableState.search.predicateObject || {};
            if(!!vm.empresaId){
                tableState.search.predicateObject.empresa_id = vm.empresaId;
            }
            if(!!vm.usuarioCliente){
                tableState.search.predicateObject.usuario_cliente = vm.usuarioCliente;
            }
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }
            if(pipePromise) $timeout.cancel(pipePromise);
            pipePromise = $timeout(function() {
                if(!vm.cargando){
                    vm.cargando = true;
                    genericService.obtenerColeccion(recurso, tableState)
                        .then(function (response){
                            if(response.status === Constantes.Response.HTTP_OK){
                                var coleccion = response.data.datos;
                                if(coleccion && coleccion.length > 0 ){
                                    vm.coleccion = [].concat(coleccion);
                                    vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                                    vm.paginacion.paginaActual = response.data.pagina_actual;
                                    vm.paginacion.desde = response.data.desde;
                                    vm.paginacion.hasta = response.data.hasta;
                                    vm.paginacion.total = response.data.total;
                                    tableState.pagination.numberOfPages = response.data.ultima_pagina;
                                }else{
                                    vm.coleccion = [];
                                }
                            }
                        }).finally(function () {
                        vm.cargando = false;
                    });
                }
            }, 500);
        }

        function limpiarFiltros(){
            vm.tipo = null;
            vm.nombre = null;
            $scope.isReset = true;
        }

        /*function cargarFiltros() {
            var filtros = gridUtil.getFormattedSearchPredicate("servicio");
            vm.nombre = filtros && filtros.nombre ? filtros.nombre : null;
        }*/

        // Acciones

        function eliminar(row){
            genericService.eliminar(recurso, row.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        $scope.$broadcast('refreshGrid');
                        messageUtil.success(response.data.mensajes[0]);
                    }else if(
                        response.status === Constantes.Response.HTTP_CONFLICT ||
                        response.status === Constantes.Response.HTTP_BAD_REQUEST
                    ){
                        messageUtil.error(response.data.mensajes[0]);
                    }
                });
        }

        // Modulos externos

        function crear(usuario) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + recurso + '/create',
                controller: 'usuarioEditController as vm',
                size: 'lg',
                resolve: {
                    parametros: function () {
                        return {
                            usuario: usuario,
                            cliente: vm.usuarioCliente
                        };
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(usuario && usuario.id && response && response.id){
                    rol.nombre = response.nombre;
                    rol.estado = response.estado;
                    rol.tipo = response.tipo;
                    rol.usuario_creacion_id = response.usuario_creacion_id;
                    rol.usuario_creacion_nombre = response.usuario_creacion_nombre;
                    rol.usuario_modificacion_id = response.usuario_modificacion_id;
                    rol.usuario_modificacion_nombre = response.usuario_modificacion_nombre;
                }else if(response && response.id){
                    $scope.$broadcast('refreshGrid');
                }
            });
        }

        function confirmarEliminar(row) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + 'confirmar-template',
                controller: 'confirmarController as vm',
                size: 'md',
                resolve: {
                    parametros: function () {
                        return {
                            mensaje: "¿Estás seguro de que quieres eliminar el usuario?"
                        };
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(response.ok){
                    eliminar(row);
                }
            });
        }

        function cambiarClave(row) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + 'usuarios/cambio-clave-template',
                controller: 'usuarioCambioClaveController as vm',
                size: 'md',
                resolve: {
                    parametros: function () {
                        return {
                            id: row.id,
                            nombre: row.nombre,
                            documento: row.documento,
                            empresaNombre: row.empresa_nombre,
                            rolNombre: row.rol_nombre
                        };
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(response.ok){
                    eliminar(row);
                }
            });
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('usuarioEditController', usuarioEditController);

    usuarioEditController.$inject = [
        '$timeout',
        '$uibModalInstance',
        'genericService',
        'messageUtil',
        'Constantes',
        'sessionUtil',
        'parametros'
    ];

    function usuarioEditController(
        $timeout, $uibModalInstance, genericService, messageUtil, Constantes, sessionUtil, parametros
    ){
        var vm = this,
            usuario = parametros.usuario,
            empresa = sessionUtil.getEmpresa(),
            recurso = "usuarios";

        vm.usuarioCliente = parametros.cliente ? parametros.cliente : null;

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        // Init autocompletes
        initAutocompleteEmpresa();
        initAutocompleteRol();
        initAutocompleteTipoDocumento();
        initAutocompleteDepartamento();
        initAutocompleteCiudad();
        initAutocompleteArea();

        if(usuario && usuario.id){
            vm.formatoFechaVencimiento = {
                useCurrent: false,
                format: 'YYYY-MM-DD'
            };
            cargar();
        }else{
            vm.estado = true;
            vm.dependientePrincipal = false;
            vm.conMicroportal = false;
            vm.formatoFechaVencimiento = {
                useCurrent: false,
                format: 'YYYY-MM-DD',
                minDate: 'now'
            };
        }

        if(vm.usuarioCliente){
            vm.empresaId = empresa.id;
            vm.deshabilitarEmpresaId = true;
            vm.deshabilitarRolId = true;
        }else{
            vm.deshabilitarEmpresaId = false;
            vm.deshabilitarRolId = false;
        }

        function cargar() {
            genericService.cargar(recurso, usuario.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var usuario = response.data,
                            rol = usuario.rol,
                            tipoDocumento = usuario.tipo_documento,
                            empresa = usuario.empresa,
                            departamento = usuario.departamento,
                            ciudad = usuario.ciudad,
                            area = usuario.area;

                        // Guardar el usuario origial
                        vm.usuarioOriginal = usuario;

                        // Cargar datos basicos de usuario
                        vm.id = usuario.id;
                        vm.documento = usuario.documento;
                        vm.nombre = usuario.nombre;
                        vm.emailUno = usuario.email_uno;
                        vm.emailDos = usuario.email_dos;
                        vm.fechaVencimiento = usuario.fecha_vencimiento;
                        vm.dependientePrincipal = !!usuario.dependiente_principal;
                        vm.conMicroportal = !!usuario.con_microportal;
                        vm.conMovimientoProceso = !!usuario.con_movimiento_proceso;
                        vm.conMontajeActuacion = !!usuario.con_montaje_actuacion;
                        vm.conMovimientoProcesoDos = !!usuario.con_movimiento_proceso_dos;
                        vm.conMontajeActuacionDos = !!usuario.con_montaje_actuacion_dos;
                        vm.conTarea = !!usuario.con_tarea;
                        vm.conRespuestaTarea = !!usuario.con_respuesta_tarea;
                        vm.conVencimientoTerminoTarea = !!usuario.con_ven_termino_tarea;
                        vm.conVencimientoTerminoActuacionRama = !!usuario.con_ven_termino_actuacion_rama;
                        vm.conVencimientoTerminoActuacionArchivo = !!usuario.con_ven_termino_actuacion_archivo;
                        vm.estado = usuario.estado;
                        vm.fechaCreacion = usuario.fecha_creacion;
                        vm.fechaModificacion = usuario.fecha_modificacion;
                        vm.usuarioCreacionNombre = usuario.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = usuario.usuario_modificacion_nombre;

                        // Cargar rol
                        vm.rolId = rol.id;
                        vm.opcionesRol.push({id: rol.id, nombre: rol.nombre});

                        // Cargar tipo de documento
                        vm.tipoDocumentoId = tipoDocumento.id;
                        vm.opcionesTipoDocumento.push({id: tipoDocumento.id, codigo: tipoDocumento.codigo});

                        // Cargar empresa
                        vm.empresaId = empresa.id;
                        vm.opcionesEmpresa.push({id: empresa.id, nombre: empresa.nombre});

                        // Cargar departamento
                        if(departamento){
                            vm.departamentoId = departamento.id;
                            vm.opcionesDepartamento.push({id: departamento.id, nombre: departamento.nombre});
                        }

                        // Cargar ciudad
                        if(ciudad){
                            vm.ciudadId = ciudad.id;
                            vm.opcionesCiudad.push({id: ciudad.id, nombre: ciudad.nombre});
                        }

                        // Cargar area
                        if(area){
                            vm.areaId = area.id;
                            vm.opcionesArea.push({id: area.id, nombre: area.nombre});
                        }
                    }
                });
        }

        function guardar() {
            var conError = false, message;
            if(vm.emailDos && vm.emailUno == vm.emailDos){
                conError = true;
                message = "El E-mail dos debe ser diferente al E-mail uno."
            }
            vm.guardando = true;
            if(!conError && vm.form.$valid && !vm.deshabilitarGuardando){
                vm.deshabilitarGuardando = true;
                var datos = {
                    id: usuario ? +usuario.id: null,
                    documento: vm.documento,
                    nombre: vm.nombre,
                    clave: vm.clave,
                    email_uno: vm.emailUno,
                    email_dos: vm.emailDos,
                    fecha_vencimiento: vm.fechaVencimiento,
                    dependiente_principal: vm.dependientePrincipal,
                    con_microportal: vm.conMicroportal,
                    con_movimiento_proceso: vm.conMovimientoProceso,
                    con_montaje_actuacion: vm.conMontajeActuacion,
                    con_movimiento_proceso_dos: vm.conMovimientoProcesoDos,
                    con_montaje_actuacion_dos: vm.conMontajeActuacionDos,
                    con_tarea: vm.conTarea,
                    con_respuesta_tarea: vm.conRespuestaTarea,
                    con_ven_termino_tarea: vm.conVencimientoTerminoTarea,
                    con_ven_termino_actuacion_rama: vm.conVencimientoTerminoActuacionRama,
                    con_ven_termino_actuacion_archivo: vm.conVencimientoTerminoActuacionArchivo,
                    estado: vm.estado,
                    tipo_documento_id: vm.tipoDocumentoId,
                    empresa_id: vm.empresaId,
                    rol_id: vm.rolId,
                    departamento_id: vm.departamentoId,
                    ciudad_id: vm.ciudadId,
                    area_id: vm.areaId
                };

                var promesa = null;
                if(!(usuario && usuario.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        $uibModalInstance.close(response.data.datos);
                        messageUtil.success(response.data.mensajes[0]);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }
                }).finally(function () {
                    vm.deshabilitarGuardando = false;
                });
            }

            if(conError){
                messageUtil.error(message);
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }

        // Autocompletes

        function initAutocompleteEmpresa() {
            vm.configEmpresa = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                onItemAdd: function (value, $item) {
                    if(vm.empresaId !== value){
                        vm.opcionesArea = [];
                        $timeout(function () {
                            vm.limpiarOpcionesArea = true;
                            genericService.obtenerColeccionLigera("areas", {
                                ligera: true,
                                empresa_id: value
                            })
                                .then(function (response){
                                    var areas = response.data;
                                    if(vm.usuarioOriginal && vm.usuarioOriginal.area){
                                        vm.areaId = vm.usuarioOriginal.area.id;
                                        vm.opcionesArea.push({
                                            id: vm.usuarioOriginal.area.id,
                                            nombre: vm.usuarioOriginal.area.nombre
                                        });
                                    }
                                    vm.opcionesArea = [].concat(areas);
                                });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.opcionesArea = [];
                    vm.areaId = null;
                    $timeout(function () {
                        vm.limpiarOpcionesArea = true;
                    }, 100);
                }
            };

            vm.opcionesEmpresa = [];
            genericService.obtenerColeccionLigera("empresas", { ligera: true })
                .then(function (response){
                    vm.opcionesEmpresa = [].concat(response.data);
                });
        }

        function initAutocompleteRol() {
            vm.configRol = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true
            };

            vm.opcionesRol = [];
            genericService.obtenerColeccionLigera("roles",
                {
                    ligera: true,
                    cliente: vm.usuarioCliente
                })
                .then(function (response){
                    vm.opcionesRol = [].concat(response.data);
                    if (!!vm.usuarioCliente) {
                        vm.rolId = vm.opcionesRol[0].id;
                    }
                });
        }

        function initAutocompleteArea() {
            vm.configArea = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true
            };
            vm.opcionesArea = [];
        }

        function initAutocompleteDepartamento() {
            vm.configDepartamento = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                onItemAdd: function (value, $item) {
                    if(vm.departamentoId !== value){
                        vm.opcionesCiudad = [];
                        $timeout(function () {
                            vm.limpiarOpcionesCiudad = true;
                            genericService.obtenerColeccionLigera("ciudades", {
                                ligera: true,
                                departamento_id: value
                            })
                            .then(function (response){
                                var ciudades = response.data;
                                if(vm.usuarioOriginal && vm.usuarioOriginal.ciudad){
                                    vm.ciudadId = vm.usuarioOriginal.ciudad.id;
                                    vm.opcionesCiudad.push({
                                        id: vm.usuarioOriginal.ciudad.id,
                                        nombre: vm.usuarioOriginal.ciudad.nombre
                                    });
                                }
                                vm.opcionesCiudad = [].concat(ciudades);
                            });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.opcionesCiudad = [];
                    vm.ciudadId = null;
                    $timeout(function () {
                        vm.limpiarOpcionesCiudad = true;
                    }, 100);
                }
            };

            vm.opcionesDepartamento = [];
            genericService.obtenerColeccionLigera("departamentos",{
                ligera: true
            })
            .then(function (response){
                vm.opcionesDepartamento = [].concat(response.data);
            });
        }

        function initAutocompleteCiudad() {
            vm.configCiudad = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                lock: false
            };
            vm.opcionesCiudad = [];
        }

        function initAutocompleteTipoDocumento() {
            vm.configTipoDocumento = {
                valueField: 'id',
                labelField: 'codigo',
                searchField: ['codigo'],
                sortField: 'codigo',
                allowEmptyOption: false,
                create: false,
                persist: true
            };

            vm.opcionesTipoDocumento = [];
            genericService.obtenerColeccionLigera("tipos-documentos",{
                ligera: true
            })
            .then(function (response){
                vm.opcionesTipoDocumento = [].concat(response.data);
            });
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('usuarioCambioClaveController', usuarioCambioClaveController);

    usuarioCambioClaveController.$inject = [
        '$uibModalInstance',
        'genericService',
        'messageUtil',
        'Constantes',
        'parametros'
    ];

    function usuarioCambioClaveController($uibModalInstance, genericService, messageUtil, Constantes, parametros){
        var vm = this;
        vm.usuario = parametros;

        // Acciones
        vm.cambiarClave = cambiarClave;
        vm.cancelar = cancelar;

        function cambiarClave() {
            var conErrores = false;
            if(vm.usuario.nuevaClave !== vm.usuario.confirmacionClave){
                conErrores = true;
                messageUtil.error("La contraseña de confirmación debe ser igual a la nueva contraseña.");
            }
            vm.guardando = true;
            if(vm.form.$valid && !conErrores){
                var datos = {
                    id: parametros.id,
                    nueva_clave: vm.usuario.nuevaClave,
                    cambio_clave: true
                };
                genericService.modificar("usuarios", datos)
                    .then(function (response) {
                        if(response.status === Constantes.Response.HTTP_OK){
                            messageUtil.success(response.data.mensajes[0]);
                            $uibModalInstance.close();
                        }else{
                            messageUtil.error(response.data.mensajes[0]);
                        }
                    });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }
    }
})();

(function () {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('permisoEditController', permisoEditController);

    permisoEditController.$inject = [
        '$scope',
        '$http',
        '$routeParams',
        '$timeout',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function permisoEditController(
        $scope, $http, $routeParams, $timeout, envService, genericService, messageUtil, Constantes
    ) {
        var vm = this,
            promise,
            pipeSincronizarPermisos,
            rolId = $routeParams.id;

        vm.coleccion = [];
        vm.TipoRolEnum = Constantes.TipoRol;

        // Acciones
        vm.filtrar = filtrar;
        vm.cambiarPermiso = cambiarPermiso;

        // inicial autocomples
        initAutocompleteModulo();
        initAutocompleteSubmodulo();

        // Carga inicial
        cargarRol();
        obtenerPermisos();

        function cargarRol() {
            genericService.cargar("roles", rolId)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        vm.rol = response.data;
                    }
                });
        }

        function filtrar() {
            if (promise !== null) {
                $timeout.cancel(promise);
            }
            promise = $timeout(function () {
                obtenerPermisos();
                promise = null;
            }, 400);
        }

        function obtenerPermisos() {
            genericService.obtener("roles/" + rolId + "/permisos", {
                modulo_id: vm.moduloId,
                submodulo_id: vm.submoduloId
            })
            .then(function (response){
                if(response.status === Constantes.Response.HTTP_OK){
                    var permisos = response.data;
                    vm.coleccion = permisos;
                }
            });
        }

        function cambiarPermiso(permiso) {
            vm.permisosAOtorgar = vm.permisosAOtorgar || [];
            vm.permisosARevocar = vm.permisosARevocar || [];
            if(permiso.permitido){
                vm.permisosAOtorgar.push(permiso.clave);
            }else{
                vm.permisosARevocar.push(permiso.clave);
            }

            clearTimeout(pipeSincronizarPermisos);
            pipeSincronizarPermisos = setTimeout(sincronizarPermisos, 1000);
        }

        function sincronizarPermisos() {
            if(vm.permisosAOtorgar && vm.permisosAOtorgar.length > 0){
                $http.post(envService.read('apiUrl') + "roles/" + rolId + "/permisos", vm.permisosAOtorgar);
                vm.permisosAOtorgar = [];
            }

            if(vm.permisosARevocar && vm.permisosARevocar.length > 0){
                $http.put(envService.read('apiUrl') + "roles/" + rolId + "/permisos", vm.permisosARevocar);
                vm.permisosARevocar = [];
            }
        }

        // Autocompletes

        function initAutocompleteModulo() {
            vm.configModulo = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                onItemAdd: function (value, $item) {
                    if(!vm.cargandoSubmodulos){
                        vm.cargandoSubmodulos = true;
                        vm.opcionesSubmodulo = [];
                        vm.submoduloId = null;
                        $timeout(function () {
                            vm.limpiarOpcionesSubmodulo = true;
                            genericService.obtenerColeccionLigera("opciones-del-sistema", {
                                ligera: true,
                                padre_id: value
                            })
                            .then(function (response){
                                vm.opcionesSubmodulo = [].concat(response.data);
                            }).finally(function () {
                                vm.cargandoSubmodulos = false;
                            });
                        }, 100);
                    }
                },
                onItemRemove: function(value){
                    vm.opcionesSubmodulo = [];
                    vm.submoduloId = null;
                    $timeout(function () {
                        vm.limpiarOpcionesSubmodulo = true;
                    }, 100);
                }
            };

            vm.opcionesModulo = [{id: null, nombre: null}];
            genericService.obtenerColeccionLigera("opciones-del-sistema",{
                ligera: true,
                sin_padre: true
            })
            .then(function (response){
                vm.opcionesModulo = [].concat(response.data);
            });
        }

        function initAutocompleteSubmodulo() {
            vm.configSubmodulo = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                onItemAdd: function (value, $item) {
                    //
                },
                onItemRemove: function(value){
                    //
                }
            };
            vm.opcionesSubmodulo = [{id: null, nombre: null}];
        }

    }
})();


(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('auditoriaMaestroController', auditoriaMaestroController);

    auditoriaMaestroController.$inject = [
        '$scope',
        '$timeout',
        '$routeParams',
        '$uibModal',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function auditoriaMaestroController(
        $scope, $timeout, $routeParams, $uibModal, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "auditoria-maestros";

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Limpiar filtros
        limpiarFiltros();

        // Grid
        function obtenerResumen(tableState, ctrl){
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }
            if(pipePromise) $timeout.cancel(pipePromise);
            pipePromise = $timeout(function() {
                if(!vm.cargando){
                    vm.cargando = true;
                    genericService.obtenerColeccion(recurso, tableState)
                        .then(function (response){
                            if(response.status === Constantes.Response.HTTP_OK){
                                var coleccion = response.data.datos;
                                if(coleccion && coleccion.length > 0 ){
                                    var nombreRecursoSplit;
                                    coleccion.forEach(function (row) {
                                        nombreRecursoSplit = row.nombre_recurso.split("\\");
                                        row.nombre_recurso = nombreRecursoSplit[nombreRecursoSplit.length - 1];
                                        row.recurso = JSON.parse(row.recurso_original);
                                        if(row.recurso_resultante){
                                            row.recurso_modificado = JSON.parse(row.recurso_resultante);
                                        }
                                    });
                                    vm.coleccion = [].concat(coleccion);
                                    vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                                    vm.paginacion.paginaActual = response.data.pagina_actual;
                                    vm.paginacion.desde = response.data.desde;
                                    vm.paginacion.hasta = response.data.hasta;
                                    vm.paginacion.total = response.data.total;
                                    tableState.pagination.numberOfPages = response.data.ultima_pagina;
                                }else{
                                    vm.coleccion = [];
                                }
                            }
                        }).finally(function () {
                            vm.cargando = false;
                        });
                }
            }, 500);
        }

        function limpiarFiltros(){
            vm.filtroNombre = null;
            vm.fitroDescripcion = null;
            vm.filtroFechaDesde = null;
            vm.filtroFechaHasta = null;
            $scope.isReset = true;
        }

        /*function cargarFiltros() {
            var filtros = gridUtil.getFormattedSearchPredicate("servicio");
            vm.nombre = filtros && filtros.nombre ? filtros.nombre : null;
        }*/

    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('opcionSistemaController', opcionSistemaController);

    opcionSistemaController.$inject = [
        '$scope',
        '$timeout',
        '$routeParams',
        '$uibModal',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function opcionSistemaController(
        $scope, $timeout, $routeParams, $uibModal, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "opciones-del-sistema";

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.crear = crear;

        // Limpiar filtros
        limpiarFiltros();

        // Grid
        function obtenerResumen(tableState, ctrl){
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }
            if(pipePromise) $timeout.cancel(pipePromise);
            pipePromise = $timeout(function() {
                if(!vm.cargando){
                    vm.cargando = true;
                    genericService.obtenerColeccion(recurso, tableState)
                        .then(function (response){
                            if(response.status === Constantes.Response.HTTP_OK){
                                var coleccion = response.data.datos;
                                if(coleccion && coleccion.length > 0 ){
                                    vm.coleccion = [].concat(coleccion);
                                    vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                                    vm.paginacion.paginaActual = response.data.pagina_actual;
                                    vm.paginacion.desde = response.data.desde;
                                    vm.paginacion.hasta = response.data.hasta;
                                    vm.paginacion.total = response.data.total;
                                    tableState.pagination.numberOfPages = response.data.ultima_pagina;
                                }else{
                                    vm.coleccion = [];
                                }
                            }
                        }).finally(function () {
                            vm.cargando = false;
                        });
                }
            }, 500);
        }

        function limpiarFiltros(){
            vm.nombre = null;
            vm.marca = null;
            $scope.isReset = true;
        }

        /*function cargarFiltros() {
            var filtros = gridUtil.getFormattedSearchPredicate("servicio");
            vm.nombre = filtros && filtros.nombre ? filtros.nombre : null;
        }*/

        // Modulos externos

        function crear(opcionSistema) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + recurso + '/create',
                controller: 'opcionSistemaEditController as vm',
                size: 'md',
                resolve: {
                    opcionSistema: function () {
                        return opcionSistema;
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(opcionSistema && opcionSistema.id && response && response.id){
                    opcionSistema.nombre = response.nombre;
                    opcionSistema.icono_menu = response.icono_menu;
                    opcionSistema.clave_permiso = response.clave_permiso;
                    opcionSistema.posicion = response.posicion;
                    opcionSistema.estado = response.estado;
                    opcionSistema.opcion_del_sistema_id = response.opcion_del_sistema_id;
                    opcionSistema.usuario_modificacion_id = response.usuario_modificacion_id;
                    opcionSistema.usuario_modificacion_nombre = response.usuario_modificacion_nombre;
                    opcionSistema.fecha_creacion = response.fecha_creacion;
                    opcionSistema.fecha_modificacion = response.fecha_modificacion;
                }else if(response && response.id){
                    $scope.$broadcast('refreshGrid');
                }
            });
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('opcionSistemaEditController', opcionSistemaEditController);

    opcionSistemaEditController.$inject = [
        '$uibModalInstance',
        'genericService',
        'messageUtil',
        'Constantes',
        'opcionSistema'
    ];

    function opcionSistemaEditController(
        $uibModalInstance, genericService, messageUtil, Constantes, opcionSistema
    ){
        var vm = this,
            recurso = "opciones-del-sistema";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        // inicial autocomples
        initAutocompleteModulo();

        if(opcionSistema && opcionSistema.id){
            cargar();
        }else{
            vm.estado = true;
        }

        function cargar() {
            genericService.cargar(recurso, opcionSistema.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var opcionSistema = response.data;
                        vm.nombre = opcionSistema.nombre;
                        vm.iconoMenu = opcionSistema.icono_menu;
                        vm.clavePermiso = opcionSistema.clave_permiso;
                        vm.posicion = opcionSistema.posicion;
                        vm.url = opcionSistema.url;
                        vm.urlAyuda = opcionSistema.url_ayuda;
                        vm.estado = opcionSistema.estado;
                        vm.moduloId = opcionSistema.opcion_del_sistema_id;
                        vm.fechaCreacion = opcionSistema.fecha_creacion;
                        vm.fechaModificacion = opcionSistema.fecha_modificacion;
                        vm.usuarioCreacionNombre = opcionSistema.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = opcionSistema.usuario_modificacion_nombre;
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;
                var datos = {
                    id: opcionSistema ? +opcionSistema.id : null,
                    nombre: vm.nombre,
                    icono_menu: vm.iconoMenu,
                    clave_permiso: vm.clavePermiso,
                    posicion: vm.posicion,
                    url: vm.url,
                    url_ayuda: vm.urlAyuda,
                    estado: vm.estado,
                    opcion_del_sistema_id: vm.moduloId
                };

                var promesa = null;
                if(!(opcionSistema && opcionSistema.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        $uibModalInstance.close(response.data.datos);
                        messageUtil.success(response.data.mensajes[0]);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }
                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }

        function initAutocompleteModulo() {
            vm.configModulo = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
            };

            vm.opcionesModulo = [];
            genericService.obtenerColeccionLigera("opciones-del-sistema",{
                ligera: true,
                sin_padre: true
            })
            .then(function (response){
                vm.opcionesModulo = [].concat(response.data);
            });
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('servicioController', servicioController);

    servicioController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        '$uibModal',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function servicioController(
        $scope, $routeParams, $timeout, $uibModal, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "servicios";

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.crear = crear;

        // Modulos externos
        vm.confirmarEliminar = confirmarEliminar;

        // Limpiar filtros
        limpiarFiltros();

        // Grid
        function obtenerResumen(tableState, ctrl){
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }
            if(pipePromise) $timeout.cancel(pipePromise);
            pipePromise = $timeout(function() {
                if(!vm.cargando){
                    vm.cargando = true;
                    genericService.obtenerColeccion(recurso, tableState)
                        .then(function (response){
                            if(response.status === Constantes.Response.HTTP_OK){
                                var coleccion = response.data.datos;
                                if(coleccion && coleccion.length > 0 ){
                                    vm.coleccion = [].concat(coleccion);
                                    vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                                    vm.paginacion.paginaActual = response.data.pagina_actual;
                                    vm.paginacion.desde = response.data.desde;
                                    vm.paginacion.hasta = response.data.hasta;
                                    vm.paginacion.total = response.data.total;
                                    tableState.pagination.numberOfPages = response.data.ultima_pagina;
                                }else{
                                    vm.coleccion = [];
                                }
                            }
                        }).finally(function () {
                            vm.cargando = false;
                        });
                }
            }, 500);
        }

        function limpiarFiltros(){
            vm.nombre = null;
            vm.marca = null;
            $scope.isReset = true;
        }

        /*function cargarFiltros() {
            var filtros = gridUtil.getFormattedSearchPredicate("servicio");
            vm.nombre = filtros && filtros.nombre ? filtros.nombre : null;
        }*/

        // Acciones

        function eliminar(row){
            genericService.eliminar(recurso, row.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        $scope.$broadcast('refreshGrid');
                        messageUtil.success(response.data.mensajes[0]);
                    }else if(
                        response.status === Constantes.Response.HTTP_CONFLICT ||
                        response.status === Constantes.Response.HTTP_BAD_REQUEST
                    ){
                        messageUtil.error(response.data.mensajes[0]);
                    }
                });
        }

        // Modulos externos

        function crear(servicio) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + recurso + '/create',
                controller: 'servicioEditController as vm',
                size: 'md',
                resolve: {
                    servicio: function () {
                        return servicio;
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(servicio && servicio.id && response && response.id){
                    servicio.nombre = response.nombre;
                    servicio.estado = response.estado;
                    servicio.usuario_modificacion_id = response.usuario_modificacion_id;
                    servicio.usuario_modificacion_nombre = response.usuario_modificacion_nombre;
                    servicio.fecha_creacion = response.fecha_creacion;
                    servicio.fecha_modificacion = response.fecha_modificacion;
                }else if(response && response.id){
                    $scope.$broadcast('refreshGrid');
                }
            });
        }

        function confirmarEliminar(row) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + 'confirmar-template',
                controller: 'confirmarController as vm',
                size: 'md',
                resolve: {
                    parametros: function () {
                        return {
                            mensaje: "¿Estás seguro de que quieres eliminar el servicio?"
                        };
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(response.ok){
                    eliminar(row);
                }
            });
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('servicioEditController', servicioEditController);

    servicioEditController.$inject = [
        '$uibModalInstance',
        'genericService',
        'messageUtil',
        'Constantes',
        'servicio'
    ];

    function servicioEditController(
        $uibModalInstance, genericService, messageUtil, Constantes, servicio
    ){
        var vm = this,
            recurso = "servicios";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        if(servicio && servicio.id){
            cargar();
        }else{
            vm.estado = true;
        }

        function cargar() {
            genericService.cargar(recurso, servicio.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var servicio = response.data;
                        vm.nombre = servicio.nombre;
                        vm.estado = servicio.estado;
                        vm.fechaCreacion = servicio.fecha_creacion;
                        vm.fechaModificacion = servicio.fecha_modificacion;
                        vm.usuarioCreacionNombre = servicio.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = servicio.usuario_modificacion_nombre;
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;
                var datos = {
                    id: servicio ? +servicio.id : null,
                    nombre: vm.nombre.toUpperCase(),
                    estado: vm.estado
                };

                var promesa = null;
                if(!(servicio && servicio.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        $uibModalInstance.close(response.data.datos);
                        messageUtil.success(response.data.mensajes[0]);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }
                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('tipoDeEmpresaController', tipoDeEmpresaController);

    tipoDeEmpresaController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        '$uibModal',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function tipoDeEmpresaController(
        $scope, $routeParams, $timeout, $uibModal, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "tipos-de-empresa";

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.crear = crear;

        // Modulos externos
        vm.confirmarEliminar = confirmarEliminar;

        // Limpiar filtros
        limpiarFiltros();

        // Grid
        function obtenerResumen(tableState, ctrl){
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }
            if(pipePromise) $timeout.cancel(pipePromise);
            pipePromise = $timeout(function() {
                if(!vm.cargando){
                    vm.cargando = true;
                    genericService.obtenerColeccion(recurso, tableState)
                        .then(function (response){
                            if(response.status === Constantes.Response.HTTP_OK){
                                var coleccion = response.data.datos;
                                if(coleccion && coleccion.length > 0 ){
                                    vm.coleccion = [].concat(coleccion);
                                    vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                                    vm.paginacion.paginaActual = response.data.pagina_actual;
                                    vm.paginacion.desde = response.data.desde;
                                    vm.paginacion.hasta = response.data.hasta;
                                    vm.paginacion.total = response.data.total;
                                    tableState.pagination.numberOfPages = response.data.ultima_pagina;
                                }else{
                                    vm.coleccion = [];
                                }
                            }
                        }).finally(function () {
                            vm.cargando = false;
                        });
                }
            }, 500);
        }

        function limpiarFiltros(){
            vm.nombre = null;
            vm.marca = null;
            $scope.isReset = true;
        }

        /*function cargarFiltros() {
            var filtros = gridUtil.getFormattedSearchPredicate("tipo-de-empresa");
            vm.nombre = filtros && filtros.nombre ? filtros.nombre : null;
        }*/

        // Acciones

        function eliminar(row){
            genericService.eliminar(recurso, row.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        $scope.$broadcast('refreshGrid');
                        messageUtil.success(response.data.mensajes[0]);
                    }else if(
                        response.status === Constantes.Response.HTTP_CONFLICT ||
                        response.status === Constantes.Response.HTTP_BAD_REQUEST
                    ){
                        messageUtil.error(response.data.mensajes[0]);
                    }
                });
        }

        // Modulos externos

        function crear(tipoDeEmpresa) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + recurso + '/create',
                controller: 'tipoDeEmpresaEditController as vm',
                size: 'md',
                resolve: {
                    tipoDeEmpresa: function () {
                        return tipoDeEmpresa;
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(tipoDeEmpresa && tipoDeEmpresa.id && response && response.id){
                    tipoDeEmpresa.nombre = response.nombre;
                    tipoDeEmpresa.estado = response.estado;
                    tipoDeEmpresa.usuario_modificacion_id = response.usuario_modificacion_id;
                    tipoDeEmpresa.usuario_modificacion_nombre = response.usuario_modificacion_nombre;
                    tipoDeEmpresa.fecha_creacion = response.fecha_creacion;
                    tipoDeEmpresa.fecha_modificacion = response.fecha_modificacion;
                }else if(response && response.id){
                    $scope.$broadcast('refreshGrid');
                }
            });
        }

        function confirmarEliminar(row) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + 'confirmar-template',
                controller: 'confirmarController as vm',
                size: 'md',
                resolve: {
                    parametros: function () {
                        return {
                            mensaje: "¿Estás seguro de que quieres eliminar el tipo de empresa?"
                        };
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(response.ok){
                    eliminar(row);
                }
            });
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('tipoDeEmpresaEditController', tipoDeEmpresaEditController);

    tipoDeEmpresaEditController.$inject = [
        '$uibModalInstance',
        'genericService',
        'messageUtil',
        'Constantes',
        'tipoDeEmpresa'
    ];

    function tipoDeEmpresaEditController(
        $uibModalInstance, genericService, messageUtil, Constantes, tipoDeEmpresa
    ){
        var vm = this,
            recurso = "tipos-de-empresa";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        if(tipoDeEmpresa && tipoDeEmpresa.id){
            cargar();
        }else{
            vm.estado = true;
        }

        function cargar() {
            genericService.cargar(recurso, tipoDeEmpresa.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var tipoDeEmpresa = response.data;
                        vm.nombre = tipoDeEmpresa.nombre;
                        vm.estado = tipoDeEmpresa.estado;
                        vm.fechaCreacion = tipoDeEmpresa.fecha_creacion;
                        vm.fechaModificacion = tipoDeEmpresa.fecha_modificacion;
                        vm.usuarioCreacionNombre = tipoDeEmpresa.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = tipoDeEmpresa.usuario_modificacion_nombre;
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;
                var datos = {
                    id: tipoDeEmpresa ? +tipoDeEmpresa.id : null,
                    nombre: vm.nombre.toUpperCase(),
                    estado: vm.estado
                };

                var promesa = null;
                if(!(tipoDeEmpresa && tipoDeEmpresa.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        $uibModalInstance.close(response.data.datos);
                        messageUtil.success(response.data.mensajes[0]);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }
                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('claseProcesoController', claseProcesoController);

    claseProcesoController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        '$uibModal',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function claseProcesoController(
        $scope, $routeParams, $timeout, $uibModal, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "clases-de-procesos";

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.crear = crear;

        // Modulos externos
        vm.confirmarEliminar = confirmarEliminar;

        // Limpiar filtros
        limpiarFiltros();

        // Grid
        function obtenerResumen(tableState, ctrl){
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }
            if(pipePromise) $timeout.cancel(pipePromise);
            pipePromise = $timeout(function() {
                if(!vm.cargando){
                    vm.cargando = true;
                    genericService.obtenerColeccion(recurso, tableState)
                        .then(function (response){
                            if(response.status === Constantes.Response.HTTP_OK){
                                var coleccion = response.data.datos;
                                if(coleccion && coleccion.length > 0 ){
                                    vm.coleccion = [].concat(coleccion);
                                    vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                                    vm.paginacion.paginaActual = response.data.pagina_actual;
                                    vm.paginacion.desde = response.data.desde;
                                    vm.paginacion.hasta = response.data.hasta;
                                    vm.paginacion.total = response.data.total;
                                    tableState.pagination.numberOfPages = response.data.ultima_pagina;
                                }else{
                                    vm.coleccion = [];
                                }
                            }
                        }).finally(function () {
                            vm.cargando = false;
                        });
                }
            }, 500);
        }

        function limpiarFiltros(){
            vm.nombre = null;
            vm.marca = null;
            $scope.isReset = true;
        }

        /*function cargarFiltros() {
            var filtros = gridUtil.getFormattedSearchPredicate("clase-de-proceso");
            vm.nombre = filtros && filtros.nombre ? filtros.nombre : null;
        }*/

        // Acciones

        function eliminar(row){
            genericService.eliminar(recurso, row.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        $scope.$broadcast('refreshGrid');
                        messageUtil.success(response.data.mensajes[0]);
                    }else if(
                        response.status === Constantes.Response.HTTP_CONFLICT ||
                        response.status === Constantes.Response.HTTP_BAD_REQUEST
                    ){
                        messageUtil.error(response.data.mensajes[0]);
                    }
                });
        }

        // Modulos externos

        function crear(claseProceso) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + recurso + '/create',
                controller: 'claseProcesoEditController as vm',
                size: 'md',
                resolve: {
                    claseProceso: function () {
                        return claseProceso;
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(claseProceso && claseProceso.id && response && response.id){
                    claseProceso.nombre = response.nombre;
                    claseProceso.estado = response.estado;
                    claseProceso.usuario_modificacion_id = response.usuario_modificacion_id;
                    claseProceso.usuario_modificacion_nombre = response.usuario_modificacion_nombre;
                    claseProceso.fecha_creacion = response.fecha_creacion;
                    claseProceso.fecha_modificacion = response.fecha_modificacion;
                }else if(response && response.id){
                    $scope.$broadcast('refreshGrid');
                }
            });
        }

        function confirmarEliminar(row) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + 'confirmar-template',
                controller: 'confirmarController as vm',
                size: 'md',
                resolve: {
                    parametros: function () {
                        return {
                            mensaje: "¿Estás seguro de que quieres eliminar la clase de proceso?"
                        };
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(response.ok){
                    eliminar(row);
                }
            });
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('claseProcesoEditController', claseProcesoEditController);

    claseProcesoEditController.$inject = [
        '$uibModalInstance',
        'genericService',
        'messageUtil',
        'Constantes',
        'claseProceso'
    ];

    function claseProcesoEditController(
        $uibModalInstance, genericService, messageUtil, Constantes, claseProceso
    ){
        var vm = this,
            recurso = "clases-de-procesos";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        if(claseProceso && claseProceso.id){
            cargar();
        }else{
            vm.estado = true;
        }

        function cargar() {
            genericService.cargar(recurso, claseProceso.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var claseProceso = response.data;
                        vm.nombre = claseProceso.nombre;
                        vm.estado = claseProceso.estado;
                        vm.fechaCreacion = claseProceso.fecha_creacion;
                        vm.fechaModificacion = claseProceso.fecha_modificacion;
                        vm.usuarioCreacionNombre = claseProceso.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = claseProceso.usuario_modificacion_nombre;
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;
                var datos = {
                    id: claseProceso ? +claseProceso.id : null,
                    nombre: vm.nombre.toUpperCase(),
                    estado: vm.estado
                };

                var promesa = null;
                if(!(claseProceso && claseProceso.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        $uibModalInstance.close(response.data.datos);
                    }

                    messageUtil.success(response.data.mensajes[0]);
                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('etapaProcesoController', etapaProcesoController);

    etapaProcesoController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        '$uibModal',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function etapaProcesoController(
        $scope, $routeParams, $timeout, $uibModal, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "etapas-de-procesos";

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.crear = crear;

        // Modulos externos
        vm.confirmarEliminar = confirmarEliminar;

        // Limpiar filtros
        limpiarFiltros();

        // Grid
        function obtenerResumen(tableState, ctrl){
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }
            if(pipePromise) $timeout.cancel(pipePromise);
            pipePromise = $timeout(function() {
                if(!vm.cargando){
                    vm.cargando = true;
                    genericService.obtenerColeccion(recurso, tableState)
                        .then(function (response){
                            if(response.status === Constantes.Response.HTTP_OK){
                                var coleccion = response.data.datos;
                                if(coleccion && coleccion.length > 0 ){
                                    vm.coleccion = [].concat(coleccion);
                                    vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                                    vm.paginacion.paginaActual = response.data.pagina_actual;
                                    vm.paginacion.desde = response.data.desde;
                                    vm.paginacion.hasta = response.data.hasta;
                                    vm.paginacion.total = response.data.total;
                                    tableState.pagination.numberOfPages = response.data.ultima_pagina;
                                }else{
                                    vm.coleccion = [];
                                }
                            }
                        }).finally(function () {
                            vm.cargando = false;
                        });
                }
            }, 500);
        }

        function limpiarFiltros(){
            vm.nombre = null;
            vm.marca = null;
            $scope.isReset = true;
        }

        /*function cargarFiltros() {
            var filtros = gridUtil.getFormattedSearchPredicate("etapa-de-proceso");
            vm.nombre = filtros && filtros.nombre ? filtros.nombre : null;
        }*/

        // Acciones

        function eliminar(row){
            genericService.eliminar(recurso, row.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        $scope.$broadcast('refreshGrid');
                        messageUtil.success(response.data.mensajes[0]);
                    }else if(
                        response.status === Constantes.Response.HTTP_CONFLICT ||
                        response.status === Constantes.Response.HTTP_BAD_REQUEST
                    ){
                        messageUtil.error(response.data.mensajes[0]);
                    }
                });
        }

        // Modulos externos

        function crear(etapaProceso) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + recurso + '/create',
                controller: 'etapaProcesoEditController as vm',
                size: 'md',
                resolve: {
                    etapaProceso: function () {
                        return etapaProceso;
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(etapaProceso && etapaProceso.id && response && response.id){
                    etapaProceso.nombre = response.nombre;
                    etapaProceso.estado = response.estado;
                    etapaProceso.usuario_modificacion_id = response.usuario_modificacion_id;
                    etapaProceso.usuario_modificacion_nombre = response.usuario_modificacion_nombre;
                    etapaProceso.fecha_creacion = response.fecha_creacion;
                    etapaProceso.fecha_modificacion = response.fecha_modificacion;
                }else if(response && response.id){
                    $scope.$broadcast('refreshGrid');
                }
            });
        }

        function confirmarEliminar(row) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + 'confirmar-template',
                controller: 'confirmarController as vm',
                size: 'md',
                resolve: {
                    parametros: function () {
                        return {
                            mensaje: "¿Estás seguro de que quieres eliminar la etapa de proceso?"
                        };
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(response.ok){
                    eliminar(row);
                }
            });
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('etapaProcesoEditController', etapaProcesoEditController);

    etapaProcesoEditController.$inject = [
        '$uibModalInstance',
        'genericService',
        'messageUtil',
        'Constantes',
        'etapaProceso'
    ];

    function etapaProcesoEditController(
        $uibModalInstance, genericService, messageUtil, Constantes, etapaProceso
    ){
        var vm = this,
            recurso = "etapas-de-procesos";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        if(etapaProceso && etapaProceso.id){
            cargar();
        }else{
            vm.estado = true;
        }

        function cargar() {
            genericService.cargar(recurso, etapaProceso.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var etapaProceso = response.data;
                        vm.nombre = etapaProceso.nombre;
                        vm.estado = etapaProceso.estado;
                        vm.fechaCreacion = etapaProceso.fecha_creacion;
                        vm.fechaModificacion = etapaProceso.fecha_modificacion;
                        vm.usuarioCreacionNombre = etapaProceso.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = etapaProceso.usuario_modificacion_nombre;
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;
                var datos = {
                    id: etapaProceso ? +etapaProceso.id : null,
                    nombre: vm.nombre.toUpperCase(),
                    estado: vm.estado
                };

                var promesa = null;
                if(!(etapaProceso && etapaProceso.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        $uibModalInstance.close(response.data.datos);
                        messageUtil.success(response.data.mensajes[0]);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }
                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('departamentoController', departamentoController);

    departamentoController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        '$uibModal',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function departamentoController(
        $scope, $routeParams, $timeout, $uibModal, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "departamentos";

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.crear = crear;

        // Modulos externos
        vm.confirmarEliminar = confirmarEliminar;

        // Limpiar filtros
        limpiarFiltros();

        // Grid
        function obtenerResumen(tableState, ctrl){
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }
            if(pipePromise) $timeout.cancel(pipePromise);
            pipePromise = $timeout(function() {
                if(!vm.cargando){
                    vm.cargando = true;
                    genericService.obtenerColeccion(recurso, tableState)
                        .then(function (response){
                            if(response.status === Constantes.Response.HTTP_OK){
                                var coleccion = response.data.datos;
                                if(coleccion && coleccion.length > 0 ){
                                    vm.coleccion = [].concat(coleccion);
                                    vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                                    vm.paginacion.paginaActual = response.data.pagina_actual;
                                    vm.paginacion.desde = response.data.desde;
                                    vm.paginacion.hasta = response.data.hasta;
                                    vm.paginacion.total = response.data.total;
                                    tableState.pagination.numberOfPages = response.data.ultima_pagina;
                                }else{
                                    vm.coleccion = [];
                                }
                            }
                        }).finally(function () {
                            vm.cargando = false;
                        });
                }
            }, 500);
        }

        function limpiarFiltros(){
            vm.nombre = null;
            vm.marca = null;
            $scope.isReset = true;
        }

        /*function cargarFiltros() {
            var filtros = gridUtil.getFormattedSearchPredicate("departamento");
            vm.nombre = filtros && filtros.nombre ? filtros.nombre : null;
        }*/

        // Acciones

        function eliminar(row){
            genericService.eliminar(recurso, row.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        $scope.$broadcast('refreshGrid');
                        messageUtil.success(response.data.mensajes[0]);
                    }else if(
                        response.status === Constantes.Response.HTTP_CONFLICT ||
                        response.status === Constantes.Response.HTTP_BAD_REQUEST
                    ){
                        messageUtil.error(response.data.mensajes[0]);
                    }
                });
        }

        // Modulos externos

        function crear(departamento) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + recurso + '/create',
                controller: 'departamentoEditController as vm',
                size: 'md',
                resolve: {
                    departamento: function () {
                        return departamento;
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(departamento && departamento.id && response && response.id){
                    departamento.nombre = response.nombre;
                    departamento.codigo_dane = response.codigo_dane;
                    departamento.estado = response.estado;
                    departamento.usuario_modificacion_id = response.usuario_modificacion_id;
                    departamento.usuario_modificacion_nombre = response.usuario_modificacion_nombre;
                    departamento.fecha_creacion = response.fecha_creacion;
                    departamento.fecha_modificacion = response.fecha_modificacion;
                }else if(response && response.id){
                    $scope.$broadcast('refreshGrid');
                }
            });
        }

        function confirmarEliminar(row) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + 'confirmar-template',
                controller: 'confirmarController as vm',
                size: 'md',
                resolve: {
                    parametros: function () {
                        return {
                            mensaje: "¿Estás seguro de que quieres eliminar el departamento?"
                        };
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(response.ok){
                    eliminar(row);
                }
            });
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('departamentoEditController', departamentoEditController);

    departamentoEditController.$inject = [
        '$uibModalInstance',
        'genericService',
        'messageUtil',
        'Constantes',
        'departamento'
    ];

    function departamentoEditController(
        $uibModalInstance, genericService, messageUtil, Constantes, departamento
    ){
        var vm = this,
            recurso = "departamentos";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        if(departamento && departamento.id){
            cargar();
        }else{
            vm.estado = true;
        }

        function cargar() {
            genericService.cargar(recurso, departamento.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var departamento = response.data;
                        vm.nombre = departamento.nombre;
                        vm.codigoDane = departamento.codigo_dane;
                        vm.estado = departamento.estado;
                        vm.fechaCreacion = departamento.fecha_creacion;
                        vm.fechaModificacion = departamento.fecha_modificacion;
                        vm.usuarioCreacionNombre = departamento.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = departamento.usuario_modificacion_nombre;
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;
                var datos = {
                    id: departamento ? +departamento.id : null,
                    nombre: vm.nombre.toUpperCase(),
                    codigo_dane: vm.codigoDane,
                    estado: vm.estado
                };

                var promesa = null;
                if(!(departamento && departamento.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        $uibModalInstance.close(response.data.datos);
                        messageUtil.success(response.data.mensajes[0]);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }


                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('etapaClaseProcesoController', etapaClaseProcesoController);

    etapaClaseProcesoController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        '$uibModal',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function etapaClaseProcesoController(
        $scope, $routeParams, $timeout, $uibModal, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "etapa-clase-procesos";

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.crear = crear;

        // Modulos externos
        vm.confirmarEliminar = confirmarEliminar;

        // Limpiar filtros
        limpiarFiltros();

        // Grid
        function obtenerResumen(tableState, ctrl){
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }
            if(pipePromise) $timeout.cancel(pipePromise);
            pipePromise = $timeout(function() {
                if(!vm.cargando){
                    vm.cargando = true;
                    genericService.obtenerColeccion(recurso, tableState)
                        .then(function (response){
                            if(response.status === Constantes.Response.HTTP_OK){
                                var coleccion = response.data.datos;
                                if(coleccion && coleccion.length > 0 ){
                                    vm.coleccion = [].concat(coleccion);
                                    vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                                    vm.paginacion.paginaActual = response.data.pagina_actual;
                                    vm.paginacion.desde = response.data.desde;
                                    vm.paginacion.hasta = response.data.hasta;
                                    vm.paginacion.total = response.data.total;
                                    tableState.pagination.numberOfPages = response.data.ultima_pagina;
                                }else{
                                    vm.coleccion = [];
                                }
                            }
                        }).finally(function () {
                            vm.cargando = false;
                        });
                }
            }, 500);
        }

        function limpiarFiltros(){
            vm.claseProcesoNombre = null;
            vm.etapaProcesoNombre = null;
            vm.marca = null;
            $scope.isReset = true;
        }

        /*function cargarFiltros() {
            var filtros = gridUtil.getFormattedSearchPredicate("etapa-clase-proceso");
            vm.claseProcesoNombre = filtros && filtros.claseProceso ? filtros.claseProcesoNombre : null;
            vm.etapaProcesoNombre = filtros && filtros.etapaProceso ? filtros.etapaProcesoNombre : null;
        }*/

        // Acciones

        function eliminar(row){
            genericService.eliminar(recurso, row.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        $scope.$broadcast('refreshGrid');
                        messageUtil.success(response.data.mensajes[0]);
                    }else if(
                        response.status === Constantes.Response.HTTP_CONFLICT ||
                        response.status === Constantes.Response.HTTP_BAD_REQUEST
                    ){
                        messageUtil.error(response.data.mensajes[0]);
                    }
                });
        }

        // Modulos externos

        function crear(etapaClaseProceso) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + recurso + '/create',
                controller: 'etapaClaseProcesoEditController as vm',
                size: 'md',
                resolve: {
                    etapaClaseProceso: function () {
                        return etapaClaseProceso;
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(etapaClaseProceso && etapaClaseProceso.id && response && response.id){
                    etapaClaseProceso.claseProcesoNombre = response.clase_proceso_nombre;
                    etapaClaseProceso.etapaProcesoNombre = response.etapa_proceso_nombre;
                    etapaClaseProceso.estado = response.estado;
                    etapaClaseProceso.usuario_modificacion_id = response.usuario_modificacion_id;
                    etapaClaseProceso.usuario_modificacion_nombre = response.usuario_modificacion_nombre;
                    etapaClaseProceso.fecha_creacion = response.fecha_creacion;
                    etapaClaseProceso.fecha_modificacion = response.fecha_modificacion;
                    etapaClaseProceso.etapa_proceso_nombre = response.etapa_proceso.nombre;
                    etapaClaseProceso.clase_proceso_nombre = response.clase_proceso.nombre;
                }else if(response && response.id){
                    $scope.$broadcast('refreshGrid');
                }
            });
        }

        function confirmarEliminar(row) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + 'confirmar-template',
                controller: 'confirmarController as vm',
                size: 'md',
                resolve: {
                    parametros: function () {
                        return {
                            mensaje: "¿Estás seguro de que quieres eliminar la etapa de la clase de proceso?"
                        };
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(response.ok){
                    eliminar(row);
                }
            });
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('etapaClaseProcesoEditController', etapaClaseProcesoEditController);

    etapaClaseProcesoEditController.$inject = [
        '$uibModalInstance',
        'genericService',
        'messageUtil',
        'Constantes',
        'etapaClaseProceso'
    ];

    function etapaClaseProcesoEditController(
        $uibModalInstance, genericService, messageUtil, Constantes, etapaClaseProceso
    ){
        var vm = this,
            recurso = "etapa-clase-procesos";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        // inicial autocomples
        initAutocompleteClaseProceso();
        initAutocompleteEtapaProceso();

        if(etapaClaseProceso && etapaClaseProceso.id){
            cargar();
        }else{
            vm.estado = true;
        }

        function cargar() {
            genericService.cargar(recurso, etapaClaseProceso.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var etapaClaseProceso = response.data,
                            claseProceso = etapaClaseProceso.clase_proceso,
                            etapaProceso = etapaClaseProceso.etapa_proceso;

                        vm.estado = etapaClaseProceso.estado;
                        vm.fechaCreacion = etapaClaseProceso.fecha_creacion;
                        vm.fechaModificacion = etapaClaseProceso.fecha_modificacion;
                        vm.usuarioCreacionNombre = etapaClaseProceso.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = etapaClaseProceso.usuario_modificacion_nombre;

                        if(claseProceso){
                            vm.claseProcesoId = claseProceso.id;
                            vm.opcionesClaseProceso.push({id: claseProceso.id, nombre: claseProceso.nombre});
                        }

                        if(etapaProceso){
                            vm.etapaProcesoId = etapaProceso.id;
                            vm.opcionesEtapaProceso.push({id: etapaProceso.id, nombre: etapaProceso.nombre});
                        }
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;
                var datos = {
                    id: etapaClaseProceso ? +etapaClaseProceso.id : null,
                    etapa_proceso_id: vm.etapaProcesoId,
                    clase_proceso_id: vm.claseProcesoId,
                    estado: vm.estado
                };

                var promesa = null;
                if(!(etapaClaseProceso && etapaClaseProceso.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        $uibModalInstance.close(response.data.datos);
                    }

                    messageUtil.success(response.data.mensajes[0]);
                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }

        function initAutocompleteClaseProceso() {
            vm.configClaseProceso = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
            };

            vm.opcionesClaseProceso = [];
            genericService.obtenerColeccionLigera("clases-de-procesos",{
                ligera: true,
            })
            .then(function (response){
                vm.opcionesClaseProceso = [].concat(response.data);
            });
        }

        function initAutocompleteEtapaProceso() {
            vm.configEtapaProceso = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
            };

            vm.opcionesEtapaProceso = [];
            genericService.obtenerColeccionLigera("etapas-de-procesos",{
                ligera: true,
            })
            .then(function (response){
                vm.opcionesEtapaProceso = [].concat(response.data);
            });
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('zonaController', zonaController);

    zonaController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        '$uibModal',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function zonaController(
        $scope, $routeParams, $timeout, $uibModal, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "zonas";

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.crear = crear;

        // Modulos externos
        vm.confirmarEliminar = confirmarEliminar;

        // Limpiar filtros
        limpiarFiltros();

        // Grid
        function obtenerResumen(tableState, ctrl){
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }
            if(pipePromise) $timeout.cancel(pipePromise);
            pipePromise = $timeout(function() {
                if(!vm.cargando){
                    vm.cargando = true;
                    genericService.obtenerColeccion(recurso, tableState)
                        .then(function (response){
                            if(response.status === Constantes.Response.HTTP_OK){
                                var coleccion = response.data.datos;
                                if(coleccion && coleccion.length > 0 ){
                                    vm.coleccion = [].concat(coleccion);
                                    vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                                    vm.paginacion.paginaActual = response.data.pagina_actual;
                                    vm.paginacion.desde = response.data.desde;
                                    vm.paginacion.hasta = response.data.hasta;
                                    vm.paginacion.total = response.data.total;
                                    tableState.pagination.numberOfPages = response.data.ultima_pagina;
                                }else{
                                    vm.coleccion = [];
                                }
                            }
                        }).finally(function () {
                            vm.cargando = false;
                        });
                }
            }, 500);
        }

        function limpiarFiltros(){
            vm.nombre = null;
            vm.marca = null;
            $scope.isReset = true;
        }

        /*function cargarFiltros() {
            var filtros = gridUtil.getFormattedSearchPredicate("zona");
            vm.nombre = filtros && filtros.nombre ? filtros.nombre : null;
        }*/

        // Acciones

        function eliminar(row){
            genericService.eliminar(recurso, row.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        $scope.$broadcast('refreshGrid');
                        messageUtil.success(response.data.mensajes[0]);
                    }else if(
                        response.status === Constantes.Response.HTTP_CONFLICT ||
                        response.status === Constantes.Response.HTTP_BAD_REQUEST
                    ){
                        messageUtil.error(response.data.mensajes[0]);
                    }
                });
        }

        // Modulos externos

        function crear(zona) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + recurso + '/create',
                controller: 'zonaEditController as vm',
                size: 'md',
                resolve: {
                    zona: function () {
                        return zona;
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(zona && zona.id && response && response.id){
                    zona.nombre = response.nombre;
                    zona.coordinador_vigilancia = response.coordinador_vigilancia;
                    zona.estado = response.estado;
                    zona.usuario_modificacion_id = response.usuario_modificacion_id;
                    zona.usuario_modificacion_nombre = response.usuario_modificacion_nombre;
                    zona.fecha_creacion = response.fecha_creacion;
                    zona.fecha_modificacion = response.fecha_modificacion;
                }else if(response && response.id){
                    $scope.$broadcast('refreshGrid');
                }
            });
        }

        function confirmarEliminar(row) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + 'confirmar-template',
                controller: 'confirmarController as vm',
                size: 'md',
                resolve: {
                    parametros: function () {
                        return {
                            mensaje: "¿Estás seguro de que quieres eliminar la zona?"
                        };
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(response.ok){
                    eliminar(row);
                }
            });
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('zonaEditController', zonaEditController);

    zonaEditController.$inject = [
        '$uibModalInstance',
        'genericService',
        'messageUtil',
        'Constantes',
        'zona'
    ];

    function zonaEditController(
        $uibModalInstance, genericService, messageUtil, Constantes, zona
    ){
        var vm = this,
            recurso = "zonas";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        if(zona && zona.id){
            cargar();
        }else{
            vm.estado = true;
        }

        function cargar() {
            genericService.cargar(recurso, zona.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var zona = response.data;
                        vm.nombre = zona.nombre;
                        vm.coordinadorVigilancia = zona.coordinador_vigilancia;
                        vm.estado = zona.estado;
                        vm.fechaCreacion = zona.fecha_creacion;
                        vm.fechaModificacion = zona.fecha_modificacion;
                        vm.usuarioCreacionNombre = zona.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = zona.usuario_modificacion_nombre;
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;
                var datos = {
                    id: zona ? +zona.id : null,
                    nombre: vm.nombre.toUpperCase(),
                    coordinador_vigilancia: vm.coordinadorVigilancia,
                    estado: vm.estado
                };

                var promesa = null;
                if(!(zona && zona.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        $uibModalInstance.close(response.data.datos);
                        messageUtil.success(response.data.mensajes[0]);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }
                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('ciudadController', ciudadController);

    ciudadController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        '$uibModal',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function ciudadController(
        $scope, $routeParams, $timeout, $uibModal, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "ciudades";

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.crear = crear;

        // Modulos externos
        vm.confirmarEliminar = confirmarEliminar;

        // Limpiar filtros
        limpiarFiltros();

        // Grid
        function obtenerResumen(tableState, ctrl){
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }
            if(pipePromise) $timeout.cancel(pipePromise);
            pipePromise = $timeout(function() {
                if(!vm.cargando){
                    vm.cargando = true;
                    genericService.obtenerColeccion(recurso, tableState)
                        .then(function (response){
                            if(response.status === Constantes.Response.HTTP_OK){
                                var coleccion = response.data.datos;
                                if(coleccion && coleccion.length > 0 ){
                                    vm.coleccion = [].concat(coleccion);
                                    vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                                    vm.paginacion.paginaActual = response.data.pagina_actual;
                                    vm.paginacion.desde = response.data.desde;
                                    vm.paginacion.hasta = response.data.hasta;
                                    vm.paginacion.total = response.data.total;
                                    tableState.pagination.numberOfPages = response.data.ultima_pagina;
                                }else{
                                    vm.coleccion = [];
                                }
                            }
                        }).finally(function () {
                            vm.cargando = false;
                        });
                }
            }, 500);
        }

        function limpiarFiltros(){
            vm.nombre = null;
            vm.marca = null;
            vm.departamento = null;
            $scope.isReset = true;
        }

        /*function cargarFiltros() {
            var filtros = gridUtil.getFormattedSearchPredicate("ciudad");
            vm.nombre = filtros && filtros.nombre ? filtros.nombre : null;
        }*/

        // Acciones

        function eliminar(row){
            genericService.eliminar(recurso, row.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        $scope.$broadcast('refreshGrid');
                        messageUtil.success(response.data.mensajes[0]);
                    }else if(
                        response.status === Constantes.Response.HTTP_CONFLICT ||
                        response.status === Constantes.Response.HTTP_BAD_REQUEST
                    ){
                        messageUtil.error(response.data.mensajes[0]);
                    }
                });
        }

        // Modulos externos

        function crear(ciudad) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + recurso + '/create',
                controller: 'ciudadEditController as vm',
                size: 'md',
                resolve: {
                    ciudad: function () {
                        return ciudad;
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(ciudad && ciudad.id && response && response.id){
                    ciudad.nombre = response.nombre;
                    ciudad.codigo_dane = response.codigo_dane;
                    ciudad.departamento_nombre = response.departamento.nombre;
                    ciudad.rama_judicial = response.rama_jucicial;
                    ciudad.estado = response.estado;
                    ciudad.usuario_modificacion_id = response.usuario_modificacion_id;
                    ciudad.usuario_modificacion_nombre = response.usuario_modificacion_nombre;
                    ciudad.fecha_creacion = response.fecha_creacion;
                    ciudad.fecha_modificacion = response.fecha_modificacion;
                }else if(response && response.id){
                    $scope.$broadcast('refreshGrid');
                }
            });
        }

        function confirmarEliminar(row) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + 'confirmar-template',
                controller: 'confirmarController as vm',
                size: 'md',
                resolve: {
                    parametros: function () {
                        return {
                            mensaje: "¿Estás seguro de que quieres eliminar la ciudad?"
                        };
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(response.ok){
                    eliminar(row);
                }
            });
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('ciudadEditController', ciudadEditController);

    ciudadEditController.$inject = [
        '$uibModalInstance',
        'genericService',
        'messageUtil',
        'Constantes',
        'ciudad'
    ];

    function ciudadEditController(
        $uibModalInstance, genericService, messageUtil, Constantes, ciudad
    ){
        var vm = this,
            recurso = "ciudades";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        // inicial autocomples
        initAutocompleteDepartamento();

        if(ciudad && ciudad.id){
            cargar();
        }else{
            vm.estado = true;
            vm.ramaJudicial = false;
        }

        function cargar() {
            genericService.cargar(recurso, ciudad.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var ciudad = response.data;
                        vm.nombre = ciudad.nombre;
                        vm.codigoDane = ciudad.codigo_dane;
                        vm.departamentoId = ciudad.departamento_id;
                        vm.estado = ciudad.estado;
                        vm.ramaJudicial = ciudad.rama_judicial;
                        vm.fechaCreacion = ciudad.fecha_creacion;
                        vm.fechaModificacion = ciudad.fecha_modificacion;
                        vm.usuarioCreacionNombre = ciudad.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = ciudad.usuario_modificacion_nombre;
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;
                var datos = {
                    id: ciudad ? +ciudad.id : null,
                    nombre: vm.nombre.toUpperCase(),
                    codigo_dane: vm.codigoDane,
                    departamento_id: vm.departamentoId,
                    estado: vm.estado,
                    rama_judicial: vm.ramaJudicial
                };

                var promesa = null;
                if(!(ciudad && ciudad.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        messageUtil.success(response.data.mensajes[0]);
                        $uibModalInstance.close(response.data.datos);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }


                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }

        function initAutocompleteDepartamento() {
            vm.configDepartamento = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
            };

            vm.opcionesDepartamento = [];
            genericService.obtenerColeccionLigera("departamentos",{
                ligera: true,
            })
            .then(function (response){
                vm.opcionesDepartamentos = [].concat(response.data);
            });
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('juzgadoController', juzgadoController);

    juzgadoController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        '$uibModal',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function juzgadoController(
        $scope, $routeParams, $timeout, $uibModal, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "juzgados";

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.crear = crear;

        // Modulos externos
        vm.confirmarEliminar = confirmarEliminar;

        // Limpiar filtros
        limpiarFiltros();

        // Grid
        function obtenerResumen(tableState, ctrl){
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }
            if(pipePromise) $timeout.cancel(pipePromise);
            pipePromise = $timeout(function() {
                if(!vm.cargando){
                    vm.cargando = true;
                    genericService.obtenerColeccion(recurso, tableState)
                        .then(function (response){
                            if(response.status === Constantes.Response.HTTP_OK){
                                var coleccion = response.data.datos;
                                if(coleccion && coleccion.length > 0 ){
                                    vm.coleccion = [].concat(coleccion);
                                    vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                                    vm.paginacion.paginaActual = response.data.pagina_actual;
                                    vm.paginacion.desde = response.data.desde;
                                    vm.paginacion.hasta = response.data.hasta;
                                    vm.paginacion.total = response.data.total;
                                    tableState.pagination.numberOfPages = response.data.ultima_pagina;
                                }else{
                                    vm.coleccion = [];
                                }
                            }
                        }).finally(function () {
                            vm.cargando = false;
                        });
                }
            }, 500);
        }

        function limpiarFiltros(){
            vm.nombre = null;
            vm.ciudad = null;
            $scope.isReset = true;
        }

        /*function cargarFiltros() {
            var filtros = gridUtil.getFormattedSearchPredicate("juzgado");
            vm.nombre = filtros && filtros.nombre ? filtros.nombre : null;
        }*/

        // Acciones

        function eliminar(row){
            genericService.eliminar(recurso, row.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        $scope.$broadcast('refreshGrid');
                        messageUtil.success(response.data.mensajes[0]);
                    }else if(
                        response.status === Constantes.Response.HTTP_CONFLICT ||
                        response.status === Constantes.Response.HTTP_BAD_REQUEST
                    ){
                        messageUtil.error(response.data.mensajes[0]);
                    }
                });
        }

        // Modulos externos

        function crear(juzgado) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + recurso + '/create',
                controller: 'juzgadoEditController as vm',
                size: 'md',
                resolve: {
                    juzgado: function () {
                        return juzgado;
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(juzgado && juzgado.id && response && response.id){
                    juzgado.nombre = response.nombre;
                    juzgado.departamento_nombre = response.departamento.nombre;
                    juzgado.ciudad_nombre = response.ciudad.nombre;
                    juzgado.juzgado = response.juzgado;
                    juzgado.sala = response.sala;
                    juzgado.juzgado_rama = response.juzgado_rama;
                    juzgado.juzgado_actual = response.juzgado_actual;
                    juzgado.estado = response.estado;
                    juzgado.usuario_modificacion_id = response.usuario_modificacion_id;
                    juzgado.usuario_modificacion_nombre = response.usuario_modificacion_nombre;
                    juzgado.fecha_creacion = response.fecha_creacion;
                    juzgado.fecha_modificacion = response.fecha_modificacion;
                }else if(response && response.id){
                    $scope.$broadcast('refreshGrid');
                }
            });
        }

        function confirmarEliminar(row) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + 'confirmar-template',
                controller: 'confirmarController as vm',
                size: 'md',
                resolve: {
                    parametros: function () {
                        return {
                            mensaje: "¿Estás seguro de que quieres eliminar el juzgado?"
                        };
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(response.ok){
                    eliminar(row);
                }
            });
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('juzgadoEditController', juzgadoEditController);

    juzgadoEditController.$inject = [
        '$uibModalInstance',
        '$timeout',
        'genericService',
        'messageUtil',
        'Constantes',
        'juzgado'
    ];

    function juzgadoEditController(
        $uibModalInstance, $timeout, genericService, messageUtil, Constantes, juzgado
    ){
        var vm = this,
            recurso = "juzgados";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        // inicial autocomples
        initAutocompleteDepartamento();
        initAutocompleteCiudad();
        initAutocompleteJuzgado();

        if(juzgado && juzgado.id){
            cargar();
        }else{
            vm.estado = true;
            vm.juzgadoActual = true;
        }

        function cargar() {
            genericService.cargar(recurso, juzgado.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var juzgado = response.data,
                            ciudad = juzgado.ciudad,
                            departamento = juzgado.departamento;

                        vm.nombre = juzgado.nombre;
                        vm.juzgado = juzgado.juzgado;
                        vm.sala = juzgado.sala;
                        vm.juzgadoRama = juzgado.juzgado_rama;
                        vm.juzgadoActual = juzgado.juzgado_actual;
                        vm.estado = juzgado.estado;
                        vm.fechaCreacion = juzgado.fecha_creacion;
                        vm.fechaModificacion = juzgado.fecha_modificacion;
                        vm.usuarioCreacionNombre = juzgado.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = juzgado.usuario_modificacion_nombre;

                        if(ciudad){
                            vm.departamentoId = departamento.id;
                            vm.opcionesCiudad.push({id: ciudad.id, nombre: ciudad.nombre});
                            vm.ciudadId = ciudad.id;
                            vm.opcionesDepartamento.push({id: departamento.id, nombre: departamento.nombre});
                            vm.limpiarOpcionesCiudad = false;
                        }
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;

                var datos = {
                    id: juzgado ? +juzgado.id : null,
                    nombre: vm.nombre.toUpperCase(),
                    departamento_id: vm.departamentoId,
                    ciudad_id: vm.ciudadId,
                    juzgado: vm.juzgado,
                    sala: vm.sala,
                    juzgado_actual: vm.juzgadoActual,
                    estado: vm.estado

            };

                var promesa = null;
                if(!(juzgado && juzgado.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        messageUtil.success(response.data.mensajes[0]);
                        $uibModalInstance.close(response.data.datos);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }


                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }

        function initAutocompleteJuzgado() {
            vm.configJuzgado = {
                valueField: 'juzgado_rama',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                render: {
                    item: function(item, escape) {
                        return '<div data-juzgado-rama="' + escape(item.juzgado_rama) +
                            '" data-nombre="' + escape(item.nombre) +
                            '">' + escape(item.nombre) + '</div>';
                    }
                },
                onItemAdd: function (value, $item) {
                    if(vm.juzgadoReferencia !== value){
                        var juzgadoRama = $item[0].dataset.juzgadoRama,
                            juzgadoNombre = $item[0].dataset.nombre;
                        vm.nombre = juzgadoNombre;
                        vm.sala = juzgadoRama.substr(7,2);
                        vm.juzgado = juzgadoRama.substr(5,2);
                    }
                },
                onItemRemove: function () {
                    vm.nombre = null;
                    vm.sala = null;
                    vm.juzgado = null;
                }
            };

            vm.opcionesJuzgado = [];
            genericService.obtenerColeccionLigera("juzgados",{
                de_referencia: true
            })
            .then(function (response){
                vm.opcionesJuzgado = [].concat(response.data);
            });
        }

        function initAutocompleteDepartamento() {
            vm.configDepartamento = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                onItemAdd: function (value, $item) {
                    if(vm.departamentoId !== value){
                        vm.opcionesCiudad = [];
                        $timeout(function () {
                            vm.limpiarOpcionesCiudad = true;
                            genericService.obtenerColeccionLigera("ciudades", {
                                ligera: true,
                                departamento_id: value
                            })
                            .then(function (response){
                                vm.opcionesCiudad = [].concat(response.data);
                            });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.opcionesCiudad = [];
                    vm.ciudadId = null;
                    $timeout(function () {
                        vm.limpiarOpcionesCiudad = true;
                    }, 100);
                }
            };

            vm.opcionesDepartamentos = [];
            genericService.obtenerColeccionLigera("departamentos",{
                ligera: true,
            })
            .then(function (response){
                vm.opcionesDepartamentos = [].concat(response.data);
            });
        }

        function initAutocompleteCiudad() {
            vm.configCiudad = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                lock: false,
            };
            vm.opcionesCiudad = [];
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('tipoDocumentoController', tipoDocumentoController);

    tipoDocumentoController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        '$uibModal',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function tipoDocumentoController(
        $scope, $routeParams, $timeout, $uibModal, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "tipos-documentos";

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.crear = crear;

        // Modulos externos
        vm.confirmarEliminar = confirmarEliminar;

        // Limpiar filtros
        limpiarFiltros();

        // Grid
        function obtenerResumen(tableState, ctrl){
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }
            if(pipePromise) $timeout.cancel(pipePromise);
            pipePromise = $timeout(function() {
                if(!vm.cargando){
                    vm.cargando = true;
                    genericService.obtenerColeccion(recurso, tableState)
                        .then(function (response){
                            if(response.status === Constantes.Response.HTTP_OK){
                                var coleccion = response.data.datos;
                                if(coleccion && coleccion.length > 0 ){
                                    vm.coleccion = [].concat(coleccion);
                                    vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                                    vm.paginacion.paginaActual = response.data.pagina_actual;
                                    vm.paginacion.desde = response.data.desde;
                                    vm.paginacion.hasta = response.data.hasta;
                                    vm.paginacion.total = response.data.total;
                                    tableState.pagination.numberOfPages = response.data.ultima_pagina;
                                }else{
                                    vm.coleccion = [];
                                }
                            }
                        }).finally(function () {
                            vm.cargando = false;
                        });
                }
            }, 500);
        }

        function limpiarFiltros(){
            vm.nombre = null;
            vm.marca = null;
            $scope.isReset = true;
        }

        /*function cargarFiltros() {
            var filtros = gridUtil.getFormattedSearchPredicate("tipo-de-empresa");
            vm.nombre = filtros && filtros.nombre ? filtros.nombre : null;
        }*/

        // Acciones

        function eliminar(row){
            genericService.eliminar(recurso, row.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        $scope.$broadcast('refreshGrid');
                        messageUtil.success(response.data.mensajes[0]);
                    }else if(
                        response.status === Constantes.Response.HTTP_CONFLICT ||
                        response.status === Constantes.Response.HTTP_BAD_REQUEST
                    ){
                        messageUtil.error(response.data.mensajes[0]);
                    }
                });
        }

        // Modulos externos

        function crear(tipoDocumento) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + recurso + '/create',
                controller: 'tipoDocumentoEditController as vm',
                size: 'md',
                resolve: {
                    tipoDocumento: function () {
                        return tipoDocumento;
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(tipoDocumento && tipoDocumento.id && response && response.id){
                    tipoDocumento.codigo = response.codigo;
                    tipoDocumento.nombre = response.nombre;
                    tipoDocumento.estado = response.estado;
                    tipoDocumento.usuario_modificacion_id = response.usuario_modificacion_id;
                    tipoDocumento.usuario_modificacion_nombre = response.usuario_modificacion_nombre;
                    tipoDocumento.fecha_creacion = response.fecha_creacion;
                    tipoDocumento.fecha_modificacion = response.fecha_modificacion;
                }else if(response && response.id){
                    $scope.$broadcast('refreshGrid');
                }
            });
        }

        function confirmarEliminar(row) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + 'confirmar-template',
                controller: 'confirmarController as vm',
                size: 'md',
                resolve: {
                    parametros: function () {
                        return {
                            mensaje: "¿Estás seguro de que quieres eliminar el tipo de empresa?"
                        };
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(response.ok){
                    eliminar(row);
                }
            });
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('tipoDocumentoEditController', tipoDocumentoEditController);

    tipoDocumentoEditController.$inject = [
        '$uibModalInstance',
        'genericService',
        'messageUtil',
        'Constantes',
        'tipoDocumento'
    ];

    function tipoDocumentoEditController(
        $uibModalInstance, genericService, messageUtil, Constantes, tipoDocumento
    ){
        var vm = this,
            recurso = "tipos-documentos";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        if(tipoDocumento && tipoDocumento.id){
            cargar();
        }else{
            vm.estado = true;
        }

        function cargar() {
            genericService.cargar(recurso, tipoDocumento.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var tipoDocumento = response.data;
                        vm.codigo = tipoDocumento.codigo;
                        vm.nombre = tipoDocumento.nombre;
                        vm.estado = tipoDocumento.estado;
                        vm.fechaCreacion = tipoDocumento.fecha_creacion;
                        vm.fechaModificacion = tipoDocumento.fecha_modificacion;
                        vm.usuarioCreacionNombre = tipoDocumento.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = tipoDocumento.usuario_modificacion_nombre;
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;
                var datos = {
                    id: tipoDocumento ? +tipoDocumento.id : null,
                    codigo: vm.codigo.toUpperCase(),
                    nombre: vm.nombre.toUpperCase(),
                    estado: vm.estado
                };

                var promesa = null;
                if(!(tipoDocumento && tipoDocumento.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        $uibModalInstance.close(response.data.datos);
                        messageUtil.success(response.data.mensajes[0]);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }
                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('despachoController', despachoController);

    despachoController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        '$uibModal',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function despachoController(
        $scope, $routeParams, $timeout, $uibModal, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "despachos";

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.crear = crear;

        // Modulos externos
        vm.confirmarEliminar = confirmarEliminar;

        // Limpiar filtros
        limpiarFiltros();

        // Grid
        function obtenerResumen(tableState, ctrl){
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }
            if(pipePromise) $timeout.cancel(pipePromise);
            pipePromise = $timeout(function() {
                if(!vm.cargando){
                    vm.cargando = true;
                    genericService.obtenerColeccion(recurso, tableState)
                        .then(function (response){
                            if(response.status === Constantes.Response.HTTP_OK){
                                var coleccion = response.data.datos;
                                if(coleccion && coleccion.length > 0 ){
                                    vm.coleccion = [].concat(coleccion);
                                    vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                                    vm.paginacion.paginaActual = response.data.pagina_actual;
                                    vm.paginacion.desde = response.data.desde;
                                    vm.paginacion.hasta = response.data.hasta;
                                    vm.paginacion.total = response.data.total;
                                    tableState.pagination.numberOfPages = response.data.ultima_pagina;
                                }else{
                                    vm.coleccion = [];
                                }
                            }
                        }).finally(function () {
                            vm.cargando = false;
                        });
                }
            }, 500);
        }

        function limpiarFiltros(){
            vm.nombre = null;
            vm.ciudad = null;
            $scope.isReset = true;
        }

        /*function cargarFiltros() {
            var filtros = gridUtil.getFormattedSearchPredicate("despacho");
            vm.nombre = filtros && filtros.nombre ? filtros.nombre : null;
        }*/

        // Acciones

        function eliminar(row){
            genericService.eliminar(recurso, row.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        $scope.$broadcast('refreshGrid');
                        messageUtil.success(response.data.mensajes[0]);
                    }else if(
                        response.status === Constantes.Response.HTTP_CONFLICT ||
                        response.status === Constantes.Response.HTTP_BAD_REQUEST
                    ){
                        messageUtil.error(response.data.mensajes[0]);
                    }
                });
        }

        // Modulos externos

        function crear(despacho) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + recurso + '/create',
                controller: 'despachoEditController as vm',
                size: 'md',
                resolve: {
                    despacho: function () {
                        return despacho;
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(despacho && despacho.id && response && response.id){
                    despacho.nombre = response.nombre;
                    despacho.departamento_nombre = response.departamento.nombre;
                    despacho.ciudad_nombre = response.ciudad.nombre;
                    despacho.juzgado = response.juzgado;
                    despacho.sala = response.sala;
                    despacho.despacho = response.despacho;
                    despacho.estado = response.estado;
                    despacho.usuario_modificacion_id = response.usuario_modificacion_id;
                    despacho.usuario_modificacion_nombre = response.usuario_modificacion_nombre;
                    despacho.fecha_creacion = response.fecha_creacion;
                    despacho.fecha_modificacion = response.fecha_modificacion;
                }else if(response && response.id){
                    $scope.$broadcast('refreshGrid');
                }
            });
        }

        function confirmarEliminar(row) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + 'confirmar-template',
                controller: 'confirmarController as vm',
                size: 'md',
                resolve: {
                    parametros: function () {
                        return {
                            mensaje: "¿Estás seguro de que quieres eliminar el despacho?"
                        };
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(response.ok){
                    eliminar(row);
                }
            });
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('despachoEditController', despachoEditController);

    despachoEditController.$inject = [
        '$uibModalInstance',
        '$timeout',
        'genericService',
        'messageUtil',
        'Constantes',
        'despacho'
    ];

    function despachoEditController(
        $uibModalInstance, $timeout, genericService, messageUtil, Constantes, despacho
    ){
        var vm = this,
            recurso = "despachos";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        // inicial autocomples
        initAutocompleteDepartamento();
        initAutocompleteCiudad();
        initAutocompleteDespacho();

        if(despacho && despacho.id){
            cargar();
        }else{
            vm.estado = true;
            vm.despachoActual = true;
        }

        function cargar() {
            genericService.cargar(recurso, despacho.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var despacho = response.data,
                            ciudad = despacho.ciudad,
                            departamento = despacho.departamento;

                        vm.nombre = despacho.nombre;
                        vm.juzgado = despacho.juzgado;
                        vm.sala = despacho.sala;
                        vm.despacho = despacho.despacho;
                        vm.estado = despacho.estado;
                        vm.fechaCreacion = despacho.fecha_creacion;
                        vm.fechaModificacion = despacho.fecha_modificacion;
                        vm.usuarioCreacionNombre = despacho.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = despacho.usuario_modificacion_nombre;

                        if(ciudad){
                            vm.departamentoId = departamento.id;
                            vm.opcionesDepartamento.push({id: departamento.id, nombre: departamento.nombre});
                            vm.ciudadId = ciudad.id;
                            vm.opcionesCiudad.push({id: ciudad.id, nombre: ciudad.nombre});
                            vm.limpiarOpcionesCiudad = false;
                        }
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;

                var datos = {
                    id: despacho ? +despacho.id : null,
                    nombre: vm.nombre.toUpperCase(),
                    departamento_id: vm.departamentoId,
                    ciudad_id: vm.ciudadId,
                    juzgado: vm.juzgado,
                    sala: vm.sala,
                    despacho: vm.despacho,
                    estado: vm.estado

            };

                var promesa = null;
                if(!(despacho && despacho.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        messageUtil.success(response.data.mensajes[0]);
                        $uibModalInstance.close(response.data.datos);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }


                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }

        function initAutocompleteDespacho() {
            vm.configDespacho = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                render: {
                    item: function(item, escape) {
                        return '<div data-nombre="' + escape(item.nombre) +
                            '" data-sala="' + escape(item.sala) +
                            '" data-juzgado="' + escape(item.juzgado) +
                            '" data-despacho="' + escape(item.despacho) +
                            '">' + escape(item.nombre) + '</div>';
                    }
                },
                onItemAdd: function (value, $item) {
                    if(vm.despachoReferencia !== value){
                        vm.nombre = $item[0].dataset.nombre;
                        vm.sala = $item[0].dataset.sala;
                        vm.juzgado = $item[0].dataset.juzgado;
                        vm.despacho =  $item[0].dataset.despacho;
                    }
                },
                onItemRemove: function () {
                    vm.nombre = null;
                    vm.sala = null;
                    vm.juzgado = null;
                    vm.despacho = null;
                }
            };

            vm.opcionesDespachos = [];
            genericService.obtenerColeccionLigera("despachos",{
                de_referencia: true,
            })
                .then(function (response){
                    vm.opcionesDespachos = [].concat(response.data);
                });
        }

        function initAutocompleteDepartamento() {
            vm.configDepartamento = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                onItemAdd: function (value, $item) {
                    if(vm.departamentoId !== value){
                        vm.opcionesCiudad = [];
                        $timeout(function () {
                            vm.limpiarOpcionesCiudad = true;
                            genericService.obtenerColeccionLigera("ciudades", {
                                ligera: true,
                                departamento_id: value
                            })
                            .then(function (response){
                                vm.opcionesCiudad = [].concat(response.data);
                            });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.opcionesCiudad = [];
                    vm.ciudadId = null;
                    $timeout(function () {
                        vm.limpiarOpcionesCiudad = true;
                    }, 100);
                }
            };

            vm.opcionesDepartamento = [];
            genericService.obtenerColeccionLigera("departamentos",{
                ligera: true,
            })
            .then(function (response){
                vm.opcionesDepartamentos = [].concat(response.data);
            });
        }

        function initAutocompleteCiudad() {
            vm.configCiudad = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                lock: false,
            };
            vm.opcionesCiudad = [];
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('empresaController', empresaController);

    empresaController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        '$uibModal',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function empresaController(
        $scope, $routeParams, $timeout, $uibModal, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "empresas";

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.filtroEstado = null;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.crear = crear;
        vm.obtenerExcel = obtenerExcel;

        // Modulos externos
        vm.confirmarEliminar = confirmarEliminar;

        // Limpiar filtros
        limpiarFiltros();

        // Grid
        function obtenerResumen(tableState, ctrl){
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }

            if(pipePromise) $timeout.cancel(pipePromise);
            pipePromise = $timeout(function() {
                if(!vm.cargando){
                    vm.cargando = true;
                    genericService.obtenerColeccion(recurso, tableState)
                        .then(function (response){
                            if(response.status === Constantes.Response.HTTP_OK){
                                var coleccion = response.data.datos;
                                if(coleccion && coleccion.length > 0 ){
                                    vm.coleccion = [].concat(coleccion);
                                    vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                                    vm.paginacion.paginaActual = response.data.pagina_actual;
                                    vm.paginacion.desde = response.data.desde;
                                    vm.paginacion.hasta = response.data.hasta;
                                    vm.paginacion.total = response.data.total;
                                    tableState.pagination.numberOfPages = response.data.ultima_pagina;
                                }else{
                                    vm.coleccion = [];
                                }
                            }
                        }).finally(function () {
                            vm.cargando = false;
                        });
                }
            }, 500);
        }

        function limpiarFiltros(){
            vm.filtroNombre = null;
            vm.filtroCiudadNombre = null;
            vm.filtroZonaNombre = null;
            vm.filtroNumeroDocumento = null;
            vm.filtroEstado = null;
            $scope.isReset = true;
        }

        /*function cargarFiltros() {
            var filtros = gridUtil.getFormattedSearchPredicate("empresa");
            vm.nombre = filtros && filtros.nombre ? filtros.nombre : null;
        }*/

        // Acciones

        function eliminar(row){
            genericService.eliminar(recurso, row.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        $scope.$broadcast('refreshGrid');
                        messageUtil.success(response.data.mensajes[0]);
                    }else if(
                        response.status === Constantes.Response.HTTP_CONFLICT ||
                        response.status === Constantes.Response.HTTP_BAD_REQUEST
                    ){
                        messageUtil.error(response.data.mensajes[0]);
                    }
                });
        }

        // Modulos externos

        function crear(empresa) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + recurso + '/create',
                controller: 'empresaEditController as vm',
                size: 'lg',
                resolve: {
                    empresa: function () {
                        return empresa;
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(empresa && empresa.id && response && response.id){
                    empresa.nombre = response.nombre;
                    empresa.tipo_documento_codigo = response.tipo_documento.codigo;
                    empresa.tipo_empresa_nombre = response.tipo_empresa.nombre;
                    empresa.numero_documento = response.numero_documento;
                    empresa.departamento_nombre = response.departamento.nombre;
                    empresa.ciudad_nombre = response.ciudad.nombre;
                    empresa.zona_nombre = response.zona.nombre;
                    empresa.telefono_uno = response.telefono_uno;
                    empresa.telefono_dos = response.telefono_dos;
                    empresa.telefono_tres = response.telefono_tres;
                    empresa.email = response.email;
                    empresa.fecha_vencimiento = response.fecha_vencimiento;
                    empresa.actuaciones_digitales = response.actuaciones_digitales;
                    empresa.vigilancia = response.vigilancia;
                    empresa.dataprocesos = response.dataprocesos;
                    empresa.observacion = response.observacion;
                    empresa.estado = response.estado;
                    empresa.usuario_modificacion_id = response.usuario_modificacion_id;
                    empresa.usuario_modificacion_nombre = response.usuario_modificacion_nombre;
                    empresa.fecha_creacion = response.fecha_creacion;
                    empresa.fecha_modificacion = response.fecha_modificacion;

                }else if(response && response.id){
                    $scope.$broadcast('refreshGrid');
                }
            });
        }

        function obtenerExcel() {
            genericService.obtenerArchivo(recurso + "/excel", {
                nombre: vm.filtroNombre,
                numero_documento: vm.filtroNumeroDocumento,
                ciudad_nombre: vm.filtroCiudadNombre,
                zona_nombre: vm.filtroZonaNombre,
                estado: vm.filtroEstado,
            });
        }

        function confirmarEliminar(row) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + 'confirmar-template',
                controller: 'confirmarController as vm',
                size: 'lg',
                resolve: {
                    parametros: function () {
                        return {
                            mensaje: "¿Estás seguro de que quieres eliminar el empresa?"
                        };
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(response.ok){
                    eliminar(row);
                }
            });
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('empresaEditController', empresaEditController);

    empresaEditController.$inject = [
        '$uibModalInstance',
        '$timeout',
        'genericService',
        'messageUtil',
        'Constantes',
        'empresa'
    ];

    function empresaEditController(
        $uibModalInstance, $timeout, genericService, messageUtil, Constantes, empresa
    ){
        var vm = this,
            recurso = "empresas";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        // inicial autocomples
        initAutocompleteDepartamento();
        initAutocompleteCiudad();
        initAutocompleteTipoDocumento();
        initAutocompleteTipoEmpresa();
        initAutocompleteZona();

        if(empresa && empresa.id){
            vm.id = empresa.id;
            cargar();
            var today = moment().millisecond(0).second(0).minute(0).hour(0),
                fechaVencimiento = moment(empresa.fecha_vencimiento).millisecond(0).second(0).minute(0).hour(0);

            vm.datetimepickerOptions = {
                useCurrent: false,
                minDate: fechaVencimiento.diff(today, 'days') >= 0 ? today : fechaVencimiento,
                format: 'YYYY-MM-DD'
            };
        }else{
            vm.estado = true;
            vm.actuacionDigital = false;
            vm.vigilanciaProceso = false;
            vm.dataproceso = false;
            vm.datetimepickerOptions = {
                useCurrent: false,
                minDate: 'now',
                format: 'YYYY-MM-DD'
            };
        }

        function cargar() {
            genericService.cargar(recurso, empresa.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var empresa = response.data,
                            ciudad = empresa.ciudad,
                            departamento = empresa.departamento,
                            tipo_documento = empresa.tipo_documento,
                            tipo_empresa = empresa.tipo_empresa,
                            zona = empresa.zona;

                        vm.nombre = empresa.nombre;
                        vm.numeroDocumento = empresa.numero_documento;
                        vm.direccion = empresa.direccion;
                        vm.telefonoFijo = empresa.telefono_uno;
                        vm.telefonoCelular = empresa.telefono_dos;
                        vm.telefonoOtro = empresa.telefono_tres;

                        vm.correo = empresa.email;
                        vm.vigilanciaProceso = empresa.vigilancia;
                        vm.actuacionDigital = empresa.actuaciones_digitales;
                        vm.dataproceso = empresa.dataprocesos;
                        vm.fechaVencimiento = empresa.fecha_vencimiento;
                        vm.observacion = empresa.observacion;
                        vm.estado = empresa.estado;
                        vm.fechaCreacion = empresa.fecha_creacion;
                        vm.fechaModificacion = empresa.fecha_modificacion;
                        vm.usuarioCreacionNombre = empresa.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = empresa.usuario_modificacion_nombre;

                        if(ciudad){
                            vm.departamentoId = departamento.id;
                            vm.opcionesDepartamentos.push({id: departamento.id, nombre: departamento.nombre});
                            vm.ciudadId = ciudad.id;
                            vm.opcionesCiudad.push({id: ciudad.id, nombre: ciudad.nombre});
                            vm.limpiarOpcionesCiudad = false;
                        }
                        if(tipo_documento){
                            vm.tipoDocumentoId = tipo_documento.id;
                            vm.opcionesTipoDocumento.push({id: tipo_documento.id, codigo: tipo_documento.codigo});
                        }
                        if(tipo_empresa){
                            vm.tipoEmpresaId = tipo_empresa.id;
                            vm.opcionesTipoEmpresa.push({id: tipo_empresa.id, nombre: tipo_empresa.nombre});
                        }
                        if(zona){
                            vm.zonaId = zona.id;
                            vm.opcionesZona.push({id: zona.id, nombre: zona.nombre});
                        }
                    }
                });
        }

        function guardar() {
            // Formatear datos
            vm.numeroDocumento = !!vm.numeroDocumento ? vm.numeroDocumento.trim().replace(/\s/g, '') : vm.numeroDocumento;

            // Validaciones
            var conError = false;
            if(!(!!vm.telefonoFijo && vm.telefonoFijo !== "" || !!vm.telefonoCelular && vm.telefonoCelular !== "")){
                conError = true;
                messageUtil.error("Debe ingresar el Teléfono fijo o el Teléfono celular.");
            }

            vm.guardando = true;
            if(!conError && vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;

                var datos = {
                    id: empresa ? +empresa.id : null,
                    nombre: vm.nombre.toUpperCase(),
                    tipo_documento_id: vm.tipoDocumentoId,
                    numero_documento: vm.numeroDocumento,
                    departamento_id: vm.departamentoId,
                    ciudad_id: vm.ciudadId,
                    direccion: vm.direccion,
                    tipo_empresa_id: vm.tipoEmpresaId,
                    zona_id: vm.zonaId,
                    telefono_uno: vm.telefonoFijo,
                    telefono_dos: vm.telefonoCelular,
                    telefono_tres: vm.telefonoOtro,
                    email: vm.correo,
                    fecha_vencimiento: vm.fechaVencimiento,
                    con_actuaciones_digitales: vm.actuacionDigital,
                    con_vigilancia: vm.vigilanciaProceso,
                    con_data_procesos: vm.dataproceso,
                    observacion: vm.observacion,
                    clave: vm.clave,
                    estado: vm.estado
                };

                var promesa = null;
                if(!(empresa && empresa.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        messageUtil.success(response.data.mensajes[0]);
                        $uibModalInstance.close(response.data.datos);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }


                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }

            if(!vm.form.$valid){
                messageUtil.error("Se presentaron errores en el formulario.");
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }

        function initAutocompleteDepartamento() {
            vm.configDepartamento = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                onItemAdd: function (value, $item) {
                    if(vm.departamentoId !== value){
                        vm.opcionesCiudad = [];
                        $timeout(function () {
                            vm.limpiarOpcionesCiudad = true;
                            genericService.obtenerColeccionLigera("ciudades", {
                                ligera: true,
                                departamento_id: value
                            })
                            .then(function (response){
                                vm.opcionesCiudad = [].concat(response.data);
                            });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.opcionesCiudad = [];
                    vm.ciudadId = null;
                    $timeout(function () {
                        vm.limpiarOpcionesCiudad = true;
                    }, 100);
                }
            };

            vm.opcionesDepartamentos = [];
            genericService.obtenerColeccionLigera("departamentos",{
                ligera: true,
            })
            .then(function (response){
                vm.opcionesDepartamentos = [].concat(response.data);
            });
        }

        function initAutocompleteCiudad() {
            vm.configCiudad = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                lock: false,
            };
            vm.opcionesCiudad = [];
        }

        function initAutocompleteTipoDocumento() {
            vm.configTipoDocumento = {
                valueField: 'id',
                labelField: 'codigo',
                searchField: ['codigo'],
                sortField: 'codigo',
                allowEmptyOption: false,
                create: false,
                persist: true,
            };

            vm.opcionesTipoDocumento = [];
            genericService.obtenerColeccionLigera("tipos-documentos",{
                ligera: true,
            })
            .then(function (response){
                vm.opcionesTipoDocumento = [].concat(response.data);
            });
        }

        function initAutocompleteTipoEmpresa() {
            vm.configTipoEmpresa = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
            };

            vm.opcionesTipoEmpresa = [];
            genericService.obtenerColeccionLigera("tipos-de-empresa",{
                ligera: true,
            })
            .then(function (response){
                vm.opcionesTipoEmpresa = [].concat(response.data);
            });
        }

        function initAutocompleteZona() {
            vm.configZona = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
            };

            vm.opcionesZona = [];
            genericService.obtenerColeccionLigera("zonas",{
                ligera: true,
            })
            .then(function (response){
                vm.opcionesZona = [].concat(response.data);
            });
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('areaController', areaController);

    areaController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        '$uibModal',
        'envService',
        'genericService',
        'sessionUtil',
        'messageUtil',
        'Constantes'
    ];

    function areaController(
        $scope, $routeParams, $timeout, $uibModal, envService, genericService, sessionUtil, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "areas",
            empresaId = $routeParams.id && $routeParams.id !== "null" ? $routeParams.id : sessionUtil.getEmpresa().id;

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.titulo = $routeParams.o ? sessionUtil.getTituloVista($routeParams.o) : "Areas de la empresa";

        // Cargar los datos de la empresa
        genericService.cargar("empresas", empresaId)
            .then(function (response) {
                if (response.status === Constantes.Response.HTTP_OK) {
                    vm.empresa = response.data;
                }
            });

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.crear = crear;

        // Modulos externos
        vm.confirmarEliminar = confirmarEliminar;

        // Limpiar filtros
        limpiarFiltros();

        // Grid
        function obtenerResumen(tableState, ctrl){
            tableState.search = tableState.search || {};
            tableState.search.predicateObject = tableState.search.predicateObject || {};
            tableState.search.predicateObject.empresa_id = empresaId;
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }
            if(pipePromise) $timeout.cancel(pipePromise);
            pipePromise = $timeout(function() {
                if(!vm.cargando){
                    vm.cargando = true;
                    genericService.obtenerColeccion(recurso, tableState)
                        .then(function (response){
                            if(response.status === Constantes.Response.HTTP_OK){
                                var coleccion = response.data.datos;
                                if(coleccion && coleccion.length > 0 ){
                                    vm.coleccion = [].concat(coleccion);
                                    vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                                    vm.paginacion.paginaActual = response.data.pagina_actual;
                                    vm.paginacion.desde = response.data.desde;
                                    vm.paginacion.hasta = response.data.hasta;
                                    vm.paginacion.total = response.data.total;
                                    tableState.pagination.numberOfPages = response.data.ultima_pagina;
                                }else{
                                    vm.coleccion = [];
                                }
                            }
                        }).finally(function () {
                            vm.cargando = false;
                        });
                }
            }, 500);
        }

        function limpiarFiltros(){
            vm.nombre = null;
            vm.marca = null;
            $scope.isReset = true;
        }

        /*function cargarFiltros() {
            var filtros = gridUtil.getFormattedSearchPredicate("area");
            vm.nombre = filtros && filtros.nombre ? filtros.nombre : null;
        }*/

        // Acciones

        function eliminar(row){
            genericService.eliminar(recurso, row.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        $scope.$broadcast('refreshGrid');
                        messageUtil.success(response.data.mensajes[0]);
                    }else if(
                        response.status === Constantes.Response.HTTP_CONFLICT ||
                        response.status === Constantes.Response.HTTP_BAD_REQUEST
                    ){
                        messageUtil.error(response.data.mensajes[0]);
                    }
                });
        }

        // Modulos externos

        function crear(area) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + recurso + '/create',
                controller: 'areaEditController as vm',
                size: 'md',
                resolve: {
                    parametros: function () {
                        return {
                            area: area,
                            empresa: vm.empresa
                        };
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(area && area.id && response && response.id){
                    area.nombre = response.nombre;
                    area.usuario_id = response.usuario_id;
                    area.estado = response.estado;
                    area.usuario_modificacion_id = response.usuario_modificacion_id;
                    area.usuario_modificacion_nombre = response.usuario_modificacion_nombre;
                    area.fecha_creacion = response.fecha_creacion;
                    area.fecha_modificacion = response.fecha_modificacion;
                }else if(response && response.id){
                    $scope.$broadcast('refreshGrid');
                }
            });
        }

        function confirmarEliminar(row) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + 'confirmar-template',
                controller: 'confirmarController as vm',
                size: 'md',
                resolve: {
                    parametros: function () {
                        return {
                            mensaje: "¿Estás seguro de que quieres eliminar la area?"
                        };
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(response.ok){
                    eliminar(row);
                }
            });
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('areaEditController', areaEditController);

    areaEditController.$inject = [
        '$uibModalInstance',
        'genericService',
        'messageUtil',
        'Constantes',
        'parametros'
    ];

    function areaEditController(
        $uibModalInstance, genericService, messageUtil, Constantes, parametros
    ){
        var vm = this,
            recurso = "areas";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        // inicial autocomples
        initAutocompleteUsuario();

        if(parametros.area && parametros.area.id){
            cargar();
        }else{
            vm.estado = true;
        }

        function cargar() {
            genericService.cargar(recurso, parametros.area.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var area = response.data,
                            usuario = area.usuario;
                        vm.nombre = area.nombre;
                        vm.estado = area.estado;
                        vm.fechaCreacion = area.fecha_creacion;
                        vm.fechaModificacion = area.fecha_modificacion;
                        vm.usuarioCreacionNombre = area.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = area.usuario_modificacion_nombre;
                        if(usuario) {
                            vm.usuarioId = area.usuario_id;
                            vm.opcionesUsuario.push({id: usuario.id, nombre: usuario.nombre});
                        }
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;
                var datos = {
                    id: parametros.area ? +parametros.area.id : null,
                    nombre: vm.nombre.toUpperCase(),
                    empresa_id: parametros.empresa.id,
                    usuario_id: vm.usuarioId,
                    estado: vm.estado
                };

                var promesa = null;
                if(!(parametros.area && parametros.area.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        $uibModalInstance.close(response.data.datos);
                    }

                    messageUtil.success(response.data.mensajes[0]);
                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }

        function initAutocompleteUsuario() {
            vm.configUsuario = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
            };

            vm.opcionesUsuario = [];
            genericService.obtenerColeccionLigera("usuarios",{
                ligera: true,
                empresa_id: parametros.empresa.id
            })
                .then(function (response){
                    vm.opcionesUsuario = [].concat(response.data);
                });
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('planController', planController);

    planController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        '$uibModal',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function planController(
        $scope, $routeParams, $timeout, $uibModal, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "planes",
            empresaId = $routeParams.id ? $routeParams.id : null;

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.titulo = $routeParams.o ? sessionUtil.getTituloVista($routeParams.o) : "Planes";

        // Cargar los datos de la empresa
        genericService.cargar("empresas", empresaId)
            .then(function (response) {
                if (response.status === Constantes.Response.HTTP_OK) {
                    vm.empresa = response.data;
                }
            });

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.crear = crear;

        // Modulos externos
        vm.confirmarEliminar = confirmarEliminar;

        // Limpiar filtros
        limpiarFiltros();

        // Grid
        function obtenerResumen(tableState, ctrl){
            tableState.search = tableState.search || {};
            tableState.search.predicateObject = tableState.search.predicateObject || {};
            tableState.search.predicateObject.empresa_id = empresaId;
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }
            if(pipePromise) $timeout.cancel(pipePromise);
            pipePromise = $timeout(function() {
                if(!vm.cargando){
                    vm.cargando = true;
                    genericService.obtenerColeccion(recurso, tableState)
                        .then(function (response){
                            if(response.status === Constantes.Response.HTTP_OK){
                                var coleccion = response.data.datos;
                                if(coleccion && coleccion.length > 0 ){
                                    vm.coleccion = [].concat(coleccion);
                                    vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                                    vm.paginacion.paginaActual = response.data.pagina_actual;
                                    vm.paginacion.desde = response.data.desde;
                                    vm.paginacion.hasta = response.data.hasta;
                                    vm.paginacion.total = response.data.total;
                                    tableState.pagination.numberOfPages = response.data.ultima_pagina;
                                }else{
                                    vm.coleccion = [];
                                }
                            }
                        }).finally(function () {
                            vm.cargando = false;
                        });
                }
            }, 500);
        }

        function limpiarFiltros(){
            vm.nombre = null;
            vm.marca = null;
            $scope.isReset = true;
        }

        /*function cargarFiltros() {
            var filtros = gridUtil.getFormattedSearchPredicate("plan");
            vm.nombre = filtros && filtros.nombre ? filtros.nombre : null;
        }*/

        // Acciones

        function eliminar(row){
            genericService.eliminar(recurso, row.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        $scope.$broadcast('refreshGrid');
                        messageUtil.success(response.data.mensajes[0]);
                    }else if(
                        response.status === Constantes.Response.HTTP_CONFLICT ||
                        response.status === Constantes.Response.HTTP_BAD_REQUEST
                    ){
                        messageUtil.error(response.data.mensajes[0]);
                    }
                });
        }

        // Modulos externos

        function crear(plan) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + recurso + '/create',
                controller: 'planEditController as vm',
                size: 'md',
                resolve: {
                    parametros: function () {
                        return {
                            plan: plan,
                            empresa: vm.empresa
                        };
                    }
                }
            });

            modalInstance.result.then(function (response) {
                if(plan && plan.id && response && response.id){
                    plan.servicio_id = response.servicio_id;
                    plan.departamento_id = response.departamento_id;
                    plan.ciudad_id = response.ciudad_id;
                    plan.empresa_id = empresaId;
                    plan.cantidad = response.cantidad;
                    plan.valor = response.valor;
                    plan.observacion = response.observacion;
                    plan.estado = response.estado;
                    plan.usuario_modificacion_id = response.usuario_modificacion_id;
                    plan.usuario_modificacion_nombre = response.usuario_modificacion_nombre;
                    plan.fecha_creacion = response.fecha_creacion;
                    plan.fecha_modificacion = response.fecha_modificacion;
                }else if(response && response.id){
                    $scope.$broadcast('refreshGrid');
                }
            });
        }

        function confirmarEliminar(row) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + 'confirmar-template',
                controller: 'confirmarController as vm',
                size: 'md',
                resolve: {
                    parametros: function () {
                        return {
                            mensaje: "¿Estás seguro de que quieres eliminar la plan?"
                        };
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(response.ok){
                    eliminar(row);
                }
            });
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('planEditController', planEditController);

    planEditController.$inject = [
        '$uibModalInstance',
        '$timeout',
        'genericService',
        'messageUtil',
        'Constantes',
        'parametros'
    ];

    function planEditController(
        $uibModalInstance, $timeout, genericService, messageUtil, Constantes, parametros
    ){
        var vm = this,
            recurso = "planes";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        // inicial autocomples
        initAutocompleteDepartamento();
        initAutocompleteCiudad();
        initAutocompleteServicio();

        if(parametros.plan && parametros.plan.id){
            cargar();
        }else{
            vm.estado = true;
        }

        function cargar() {
            genericService.cargar(recurso, parametros.plan.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK) {
                        var plan = response.data,
                            servicio = plan.servicio,
                            ciudad = plan.ciudad,
                            departamento = plan.departamento;
                        vm.cantidad = plan.cantidad;
                        vm.valor = plan.valor;
                        vm.observacion = plan.observacion;
                        vm.estado = plan.estado;
                        vm.fechaCreacion = plan.fecha_creacion;
                        vm.fechaModificacion = plan.fecha_modificacion;
                        vm.usuarioCreacionNombre = plan.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = plan.usuario_modificacion_nombre;
                        vm.departamentoId = plan.departamento_id;
                        vm.ciudadId = plan.ciudad_id;
                        vm.servicioId = plan.servicio_id;

                        if (ciudad) {
                            vm.departamentoId = departamento.id;
                            vm.opcionesDepartamentos.push({id: departamento.id, nombre: departamento.nombre});
                            vm.ciudadId = ciudad.id;
                            vm.opcionesCiudad.push({id: ciudad.id, nombre: ciudad.nombre});
                            vm.limpiarOpcionesCiudad = false;
                        }

                        if (servicio) {
                            vm.servicioId = servicio.id;
                            vm.opcionesServicios.push({id: servicio.id, nombre: servicio.nombre});
                        }

                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;
                var datos = {
                    id: parametros.plan ? +parametros.plan.id : null,
                    servicio_id: vm.servicioId,
                    departamento_id: vm.departamentoId,
                    ciudad_id: vm.ciudadId,
                    empresa_id: parametros.empresa.id,
                    cantidad: vm.cantidad,
                    valor: vm.valor,
                    observacion: vm.observacion,
                    estado: vm.estado
                };

                var promesa = null;
                if(!(parametros.plan && parametros.plan.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        $uibModalInstance.close(response.data.datos);
                        messageUtil.success(response.data.mensajes[0]);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }
                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }

        function initAutocompleteDepartamento() {
            vm.configDepartamento = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                onItemAdd: function (value, $item) {
                    if(vm.departamentoId !== value){
                        vm.opcionesCiudad = [];
                        $timeout(function () {
                            vm.limpiarOpcionesCiudad = true;
                            genericService.obtenerColeccionLigera("ciudades", {
                                ligera: true,
                                departamento_id: value
                            })
                            .then(function (response){
                                vm.opcionesCiudad = [].concat(response.data);
                            });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.opcionesCiudad = [];
                    vm.ciudadId = null;
                    $timeout(function () {
                        vm.limpiarOpcionesCiudad = true;
                    }, 100);
                }
            };

            vm.opcionesDepartamentos = [];
            genericService.obtenerColeccionLigera("departamentos",{
                ligera: true,
            })
            .then(function (response){
                vm.opcionesDepartamentos = [].concat(response.data);
            });
        }

        function initAutocompleteCiudad() {
            vm.configCiudad = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                lock: false,
            };
            vm.opcionesCiudad = [];
        }

        function initAutocompleteServicio() {
            vm.configServicio = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
            };

            vm.opcionesServicios = [];
            genericService.obtenerColeccionLigera("servicios",{
                ligera: true,
            })
                .then(function (response){
                    vm.opcionesServicios = [].concat(response.data);
                });
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('conceptoEconomicoController', conceptoEconomicoController);

    conceptoEconomicoController.$inject = [
        '$translate',
        '$scope',
        '$routeParams',
        '$timeout',
        '$uibModal',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function conceptoEconomicoController(
     $translate, $scope, $routeParams, $timeout, $uibModal, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            i18n = {},
            recurso = "conceptos-economicos";

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.TipoConceptoEnum = Constantes.TipoConcepto;
        vm.ClaseConceptoEnum = Constantes.ClaseConcepto;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.crear = crear;

        // Modulos externos
        vm.confirmarEliminar = confirmarEliminar;

        // Limpiar filtros
        limpiarFiltros();

        // Cargar traducciones
        cargarTraducciones();

        // Cargar autocompletes
        initAutocompleteTipoConcepto();

        // Grid
        function obtenerResumen(tableState, ctrl){
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }
            if(pipePromise) $timeout.cancel(pipePromise);
            pipePromise = $timeout(function() {
                if(!vm.cargando){
                    vm.cargando = true;
                    genericService.obtenerColeccion(recurso, tableState)
                        .then(function (response){
                            if(response.status === Constantes.Response.HTTP_OK){
                                var coleccion = response.data.datos;
                                if(coleccion && coleccion.length > 0 ){
                                    vm.coleccion = [].concat(coleccion);
                                    vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                                    vm.paginacion.paginaActual = response.data.pagina_actual;
                                    vm.paginacion.desde = response.data.desde;
                                    vm.paginacion.hasta = response.data.hasta;
                                    vm.paginacion.total = response.data.total;
                                    tableState.pagination.numberOfPages = response.data.ultima_pagina;
                                }else{
                                    vm.coleccion = [];
                                }
                            }
                        }).finally(function () {
                            vm.cargando = false;
                        });
                }
            }, 500);
        }

        function limpiarFiltros(){
            vm.nombre = null;
            vm.filterTipoConcepto = null;
            $scope.isReset = true;
        }

        /*function cargarFiltros() {
            var filtros = gridUtil.getFormattedSearchPredicate("conceptoEconomico");
            vm.nombre = filtros && filtros.nombre ? filtros.nombre : null;
        }*/

        // Acciones

        function eliminar(row){
            genericService.eliminar(recurso, row.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        $scope.$broadcast('refreshGrid');
                        messageUtil.success(response.data.mensajes[0]);
                    }else if(
                        response.status === Constantes.Response.HTTP_CONFLICT ||
                        response.status === Constantes.Response.HTTP_BAD_REQUEST
                    ){
                        messageUtil.error(response.data.mensajes[0]);
                    }
                });
        }

        // Modulos externos

        function crear(conceptoEconomico) {
            // genericService.obtenerArchivo(recurso + "/excel", {
            //     nombre: vm.nombre
            // });

            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + recurso + '/create',
                controller: 'conceptoEconomicoEditController as vm',
                size: 'md',
                resolve: {
                    conceptoEconomico: function () {
                        return conceptoEconomico;
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(conceptoEconomico && conceptoEconomico.id && response && response.id){
                    conceptoEconomico.nombre = response.nombre;
                    conceptoEconomico.tipo_concepto = response.tipo_concepto;
                    conceptoEconomico.clase_concepto = response.clase_concepto;
                    conceptoEconomico.estado = response.estado;
                    conceptoEconomico.usuario_modificacion_id = response.usuario_modificacion_id;
                    conceptoEconomico.usuario_modificacion_nombre = response.usuario_modificacion_nombre;
                    conceptoEconomico.fecha_creacion = response.fecha_creacion;
                    conceptoEconomico.fecha_modificacion = response.fecha_modificacion;
                }else if(response && response.id){
                    $scope.$broadcast('refreshGrid');
                }
            });
        }

        function confirmarEliminar(row) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + 'confirmar-template',
                controller: 'confirmarController as vm',
                size: 'md',
                resolve: {
                    parametros: function () {
                        return {
                            mensaje: "¿Estás seguro de que quieres eliminar el concepto económico?"
                        };
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(response.ok){
                    eliminar(row);
                }
            });
        }

        function initAutocompleteTipoConcepto() {
            vm.configTipoConcepto = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
            };
            vm.opcionesTipoConcepto = [];
        }

        // Traducciones

        function cargarTraducciones() {
            $translate([
                "COSTOS_INTERESES_DEMANDA", "HONORARIOS_EXTRAS", "COSTOS_DE_PROCESOS"
            ]).then(function (translations) {
                i18n.COSTOS_INTERESES_DEMANDA = translations.COSTOS_INTERESES_DEMANDA;
                i18n.HONORARIOS_EXTRAS = translations.HONORARIOS_EXTRAS;
                i18n.COSTOS_DE_PROCESOS = translations.COSTOS_DE_PROCESOS;

                vm.opcionesTipoConcepto = [
                    {id: Constantes.TipoConcepto.COSTOS_INTERESES_DEMANDA, nombre: i18n.COSTOS_INTERESES_DEMANDA},
                    {id: Constantes.TipoConcepto.HONORARIOS_EXTRAS, nombre: i18n.HONORARIOS_EXTRAS},
                    {id: Constantes.TipoConcepto.COSTOS_DE_PROCESOS, nombre: i18n.COSTOS_DE_PROCESOS}
                ];
            }, function (translationIds) {});
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('conceptoEconomicoEditController', conceptoEconomicoEditController);

    conceptoEconomicoEditController.$inject = [
        '$translate',
        '$uibModalInstance',
        'genericService',
        'messageUtil',
        'Constantes',
        'conceptoEconomico'
    ];

    function conceptoEconomicoEditController(
        $translate, $uibModalInstance, genericService, messageUtil, Constantes, conceptoEconomico
    ){
        var vm = this,
            i18n = {},
            recurso = "conceptos-economicos";

        vm.ClaseConceptoEnum = Constantes.ClaseConcepto;

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        // Cargar traducciones
        cargarTraducciones();

        // Cargar autocompletes
        initAutocompleteTipoConcepto();

        if(conceptoEconomico && conceptoEconomico.id){
            cargar();
        }else{
            vm.estado = true;
        }

        function cargar() {
            genericService.cargar(recurso, conceptoEconomico.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var conceptoEconomico = response.data;
                        vm.nombre = conceptoEconomico.nombre;
                        vm.tipoConcepto = conceptoEconomico.tipo_concepto;
                        vm.claseConcepto = conceptoEconomico.clase_concepto;
                        vm.estado = conceptoEconomico.estado;
                        vm.fechaCreacion = conceptoEconomico.fecha_creacion;
                        vm.fechaModificacion = conceptoEconomico.fecha_modificacion;
                        vm.usuarioCreacionNombre = conceptoEconomico.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = conceptoEconomico.usuario_modificacion_nombre;
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;
                var datos = {
                    id: conceptoEconomico ? +conceptoEconomico.id : null,
                    nombre: vm.nombre.toUpperCase(),
                    tipo_concepto: vm.tipoConcepto,
                    clase_concepto: vm.claseConcepto,
                    estado: vm.estado
                };

                var promesa = null;
                if(!(conceptoEconomico && conceptoEconomico.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        $uibModalInstance.close(response.data.datos);
                    }

                    messageUtil.success(response.data.mensajes[0]);
                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }

        // Autocompletes

        function initAutocompleteTipoConcepto() {
            vm.configTipoConcepto = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true
            };
        }

        // Traducciones

        function cargarTraducciones() {
            $translate([
                "COSTOS_INTERESES_DEMANDA", "HONORARIOS_EXTRAS", "COSTOS_DE_PROCESOS"
            ]).then(function (translations) {
                i18n.COSTOS_INTERESES_DEMANDA = translations.COSTOS_INTERESES_DEMANDA;
                i18n.HONORARIOS_EXTRAS = translations.HONORARIOS_EXTRAS;
                i18n.COSTOS_DE_PROCESOS = translations.COSTOS_DE_PROCESOS;

                vm.opcionesTipoConcepto = [
                    {id: Constantes.TipoConcepto.COSTOS_INTERESES_DEMANDA, nombre: i18n.COSTOS_INTERESES_DEMANDA},
                    {id: Constantes.TipoConcepto.HONORARIOS_EXTRAS, nombre: i18n.HONORARIOS_EXTRAS},
                    {id: Constantes.TipoConcepto.COSTOS_DE_PROCESOS, nombre: i18n.COSTOS_DE_PROCESOS}
                ];
            }, function (translationIds) {});
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('copiaConceptoEconomicoController', copiaConceptoEconomicoController);

    copiaConceptoEconomicoController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        '$uibModal',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function copiaConceptoEconomicoController(
        $scope, $routeParams, $timeout, $uibModal, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            i18n = {},
            recurso = "copia-conceptos-economicos";

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.TipoConceptoEnum = Constantes.TipoConcepto;
        vm.ClaseConceptoEnum = Constantes.ClaseConcepto;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.crear = crear;

        // Modulos externos
        vm.confirmarEliminar = confirmarEliminar;

        // Limpiar filtros
        limpiarFiltros();

        // Grid
        function obtenerResumen(tableState, ctrl){
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }
            if(pipePromise) $timeout.cancel(pipePromise);
            pipePromise = $timeout(function() {
                if(!vm.cargando){
                    vm.cargando = true;
                    genericService.obtenerColeccion(recurso, tableState)
                        .then(function (response){
                            if(response.status === Constantes.Response.HTTP_OK){
                                var coleccion = response.data.datos;
                                if(coleccion && coleccion.length > 0 ){
                                    vm.coleccion = [].concat(coleccion);
                                    vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                                    vm.paginacion.paginaActual = response.data.pagina_actual;
                                    vm.paginacion.desde = response.data.desde;
                                    vm.paginacion.hasta = response.data.hasta;
                                    vm.paginacion.total = response.data.total;
                                    tableState.pagination.numberOfPages = response.data.ultima_pagina;
                                }else{
                                    vm.coleccion = [];
                                }
                            }
                        }).finally(function () {
                            vm.cargando = false;
                        });
                }
            }, 500);
        }

        function limpiarFiltros(){
            vm.nombre = null;
            vm.marca = null;
            $scope.isReset = true;
        }

        /*function cargarFiltros() {
            var filtros = gridUtil.getFormattedSearchPredicate("conceptoEconomico");
            vm.nombre = filtros && filtros.nombre ? filtros.nombre : null;
        }*/

        // Acciones

        function eliminar(row){
            genericService.eliminar(recurso, row.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        $scope.$broadcast('refreshGrid');
                        messageUtil.success(response.data.mensajes[0]);
                    }else if(
                        response.status === Constantes.Response.HTTP_CONFLICT ||
                        response.status === Constantes.Response.HTTP_BAD_REQUEST
                    ){
                        messageUtil.error(response.data.mensajes[0]);
                    }
                });
        }

        // Modulos externos

        function crear(conceptoEconomico) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + recurso + '/create',
                controller: 'copiaConceptoEconomicoEditController as vm',
                size: 'md',
                resolve: {
                    conceptoEconomico: function () {
                        return conceptoEconomico;
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(conceptoEconomico && conceptoEconomico.id && response && response.id){
                    conceptoEconomico.nombre = response.nombre;
                    conceptoEconomico.tipo_concepto = response.tipo_concepto;
                    conceptoEconomico.clase_concepto = response.clase_concepto;
                    conceptoEconomico.estado = response.estado;
                    conceptoEconomico.usuario_modificacion_id = response.usuario_modificacion_id;
                    conceptoEconomico.usuario_modificacion_nombre = response.usuario_modificacion_nombre;
                    conceptoEconomico.fecha_creacion = response.fecha_creacion;
                    conceptoEconomico.fecha_modificacion = response.fecha_modificacion;
                }else if(response && response.id){
                    $scope.$broadcast('refreshGrid');
                }
            });
        }

        function confirmarEliminar(row) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + 'confirmar-template',
                controller: 'confirmarController as vm',
                size: 'md',
                resolve: {
                    parametros: function () {
                        return {
                            mensaje: "¿Estás seguro de que quieres eliminar el conceptoEconomico?"
                        };
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(response.ok){
                    eliminar(row);
                }
            });
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('copiaConceptoEconomicoEditController', copiaConceptoEconomicoEditController);

    copiaConceptoEconomicoEditController.$inject = [
        '$translate',
        '$uibModalInstance',
        'genericService',
        'messageUtil',
        'Constantes',
        'conceptoEconomico'
    ];

    function copiaConceptoEconomicoEditController(
        $translate, $uibModalInstance, genericService, messageUtil, Constantes, conceptoEconomico
    ){
        var vm = this,
            i18n = {},
            recurso = "copia-conceptos-economicos";

        vm.ClaseConceptoEnum = Constantes.ClaseConcepto;

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        // Cargar traducciones
        cargarTraducciones();

        // Cargar autocompletes
        initAutocompleteTipoConcepto();

        if(conceptoEconomico && conceptoEconomico.id){
            cargar();
        }else{
            vm.estado = true;
        }

        function cargar() {
            genericService.cargar(recurso, conceptoEconomico.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var conceptoEconomico = response.data;
                        vm.nombre = conceptoEconomico.nombre;
                        vm.tipoConcepto = conceptoEconomico.tipo_concepto;
                        vm.claseConcepto = conceptoEconomico.clase_concepto;
                        vm.estado = conceptoEconomico.estado;
                        vm.fechaCreacion = conceptoEconomico.fecha_creacion;
                        vm.fechaModificacion = conceptoEconomico.fecha_modificacion;
                        vm.usuarioCreacionNombre = conceptoEconomico.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = conceptoEconomico.usuario_modificacion_nombre;
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;
                var datos = {
                    id: conceptoEconomico ? +conceptoEconomico.id : null,
                    nombre: vm.nombre.toUpperCase(),
                    tipo_concepto: vm.tipoConcepto,
                    clase_concepto: vm.claseConcepto,
                    estado: vm.estado
                };

                var promesa = null;
                if(!(conceptoEconomico && conceptoEconomico.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        $uibModalInstance.close(response.data.datos);
                    }

                    messageUtil.success(response.data.mensajes[0]);
                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }

        // Autocompletes

        function initAutocompleteTipoConcepto() {
            vm.configTipoConcepto = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true
            };
        }

        // Traducciones

        function cargarTraducciones() {
            $translate([
                "COSTOS_INTERESES_DEMANDA", "HONORARIOS_EXTRAS", "COSTOS_DE_PROCESOS"
            ]).then(function (translations) {
                i18n.COSTOS_INTERESES_DEMANDA = translations.COSTOS_INTERESES_DEMANDA;
                i18n.HONORARIOS_EXTRAS = translations.HONORARIOS_EXTRAS;
                i18n.COSTOS_DE_PROCESOS = translations.COSTOS_DE_PROCESOS;

                vm.opcionesTipoConcepto = [
                    {id: Constantes.TipoConcepto.COSTOS_INTERESES_DEMANDA, nombre: i18n.COSTOS_INTERESES_DEMANDA},
                    {id: Constantes.TipoConcepto.HONORARIOS_EXTRAS, nombre: i18n.HONORARIOS_EXTRAS},
                    {id: Constantes.TipoConcepto.COSTOS_DE_PROCESOS, nombre: i18n.COSTOS_DE_PROCESOS}
                ];
            }, function (translationIds) {});
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('coberturaController', coberturaController);

    coberturaController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        '$uibModal',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function coberturaController(
        $scope, $routeParams, $timeout, $uibModal, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "coberturas",
            empresaId = $routeParams.id ? $routeParams.id : null;

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.titulo = $routeParams.o ? sessionUtil.getTituloVista($routeParams.o) : "Coberturas";

        // Cargar los datos de la empresa
        genericService.cargar("empresas", empresaId)
            .then(function (response) {
                if (response.status === Constantes.Response.HTTP_OK) {
                    vm.empresa = response.data;
                }
            });

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.crear = crear;

        // Limpiar filtros
        limpiarFiltros();

        // Grid
        function obtenerResumen(tableState, ctrl){
            tableState.search = tableState.search || {};
            tableState.search.predicateObject = tableState.search.predicateObject || {};
            tableState.search.predicateObject.empresa_id = empresaId;
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }
            if(pipePromise) $timeout.cancel(pipePromise);
            pipePromise = $timeout(function() {
                if(!vm.cargando){
                    vm.cargando = true;
                    genericService.obtenerColeccion(recurso, tableState)
                        .then(function (response){
                            if(response.status === Constantes.Response.HTTP_OK){
                                var coleccion = response.data.datos;
                                if(coleccion && coleccion.length > 0 ){
                                    vm.coleccion = [].concat(coleccion);
                                    vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                                    vm.paginacion.paginaActual = response.data.pagina_actual;
                                    vm.paginacion.desde = response.data.desde;
                                    vm.paginacion.hasta = response.data.hasta;
                                    vm.paginacion.total = response.data.total;
                                    tableState.pagination.numberOfPages = response.data.ultima_pagina;
                                }else{
                                    vm.coleccion = [];
                                }
                            }
                        }).finally(function () {
                            vm.cargando = false;
                        });
                }
            }, 500);
        }

        function limpiarFiltros(){
            vm.nombre = null;
            vm.marca = null;
            $scope.isReset = true;
        }

        // Modulos externos

        function crear(cobertura) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + recurso + '/create',
                controller: 'coberturaEditController as vm',
                size: 'md',
                resolve: {
                    parametros: function () {
                        return {
                            cobertura: cobertura,
                            empresa: vm.empresa
                        };
                    }
                }
            });

            modalInstance.result.then(function (response) {
                $scope.$broadcast('refreshGrid');
            });
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('coberturaEditController', coberturaEditController);

    coberturaEditController.$inject = [
        '$uibModalInstance',
        '$timeout',
        'genericService',
        'messageUtil',
        'Constantes',
        'parametros'
    ];

    function coberturaEditController(
        $uibModalInstance, $timeout, genericService, messageUtil, Constantes, parametros
    ){
        var vm = this;

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;
        vm.seleccionar = seleccionar;
        vm.seleccionarTodo = seleccionarTodo;
        vm.deseleccionar = deseleccionar;
        vm.deseleccionarTodo = deseleccionarTodo;

        // inicial autocomples
        initAutocompleteDepartamento();

        // Cargar coberturas
        cargar();
        if(parametros.cobertura && parametros.cobertura.departamento_id){
            vm.departamentoId = parametros.cobertura.departamento_id;
        }

        function cargar() {
            genericService.cargar("empresas", parametros.empresa.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK) {
                        // Coberturas actuales
                        vm.ciudadesEnCobertura = response.data.ciudades_en_cobertura;
                        var departamento = response.data.departamentos_en_cobertura;
                        if(vm.ciudadesEnCobertura) {
                            vm.departamentoId = departamento.id;
                            vm.opcionesDepartamento.push({id: departamento.id, nombre: departamento.nombre});
                        }
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;

                var ciudadesIds = [];
                if(vm.ciudadesCobertura && vm.ciudadesCobertura.length > 0){
                    vm.ciudadesCobertura.forEach(function (ciudadCobertura) {
                        ciudadesIds.push(ciudadCobertura.id);
                    });
                }

                var datos = {
                    departamento_id: vm.departamentoId,
                    ciudad_ids: ciudadesIds
                };

                var promesa = genericService.modificar("empresas/" + parametros.empresa.id + "/coberturas", datos);
                promesa.then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        messageUtil.success(response.data.mensajes[0]);
                        $uibModalInstance.close();
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }

                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }

        function seleccionar(){
            vm.ciudades = vm.ciudades || [];
            vm.ciudadesCobertura = vm.ciudadesCobertura || [];
            vm.ciudades.forEach(function (ciudad) {
                if(_.includes(vm.ciudadesSinSeleccionar, ciudad.id + "")){
                    vm.ciudadesCobertura.push(ciudad);
                }
            });
            _.remove(vm.ciudades, function (ciudad) {
                return _.includes(vm.ciudadesSinSeleccionar, ciudad.id + "");
            });

            vm.ciudadesSinSeleccionar = [];
        }

        function deseleccionar(){
            vm.ciudades = vm.ciudades || [];
            vm.ciudadesCobertura = vm.ciudadesCobertura || [];
            vm.ciudadesCobertura.forEach(function (ciudadCobertura) {
                if(_.includes(vm.ciudadesSeleccionadas, ciudadCobertura.id + "")){
                    vm.ciudades.push(ciudadCobertura);
                }
            });
            _.remove(vm.ciudadesCobertura, function (ciudadCobertura) {
                return _.includes(vm.ciudadesSeleccionadas, ciudadCobertura.id + "");
            });
            vm.ciudadesSeleccionadas = [];
        }

        function seleccionarTodo(){
            vm.ciudadesCobertura =  _.union(vm.ciudades, vm.ciudadesCobertura);
            vm.ciudades = [];
            vm.ciudadesSinSeleccionar = [];
            vm.ciudadesSeleccionadas = [];
        }

        function deseleccionarTodo(){
            vm.ciudades =  _.union(vm.ciudadesCobertura, vm.ciudades);
            vm.ciudadesCobertura = [];
            vm.ciudadesSinSeleccionar = [];
            vm.ciudadesSeleccionadas = [];
        }

        // Autocompletes

        function initAutocompleteDepartamento() {
            vm.configDepartamento = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                onItemAdd: function (value, $item) {
                    if(vm.departamentoId !== value && !vm.limpiarOpcionesCiudad){
                        vm.ciudades = [];
                        vm.ciudadesCobertura = [];
                        $timeout(function () {
                            vm.cargandoCiudades = true;
                            genericService.obtenerColeccionLigera("ciudades", {
                                ligera: true,
                                departamento_id: value
                            })
                            .then(function (response){
                                var ciudades = response.data;
                                if(ciudades && ciudades.length > 0){
                                    ciudades.forEach(function (ciudad) {
                                        vm.ciudades = vm.ciudades || [];
                                        vm.ciudadesCobertura = vm.ciudadesCobertura || [];
                                        if(_.includes(vm.ciudadesEnCobertura, ciudad.id)){
                                            vm.ciudadesCobertura.push(ciudad);
                                        }else{
                                            vm.ciudades.push(ciudad);
                                        }
                                    });
                                }
                            }).finally(function () {
                                vm.cargandoCiudades = false;
                            });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.ciudades = [];
                }
            };

            vm.opcionesDepartamentos = [];
            genericService.obtenerColeccionLigera("departamentos",{
                ligera: true,
            })
            .then(function (response){
                vm.opcionesDepartamentos = [].concat(response.data);
            });
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('tipoActuacionController', tipoActuacionController);

    tipoActuacionController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        '$uibModal',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function tipoActuacionController(
        $scope, $routeParams, $timeout, $uibModal, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "tipos-actuaciones";

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.crear = crear;

        // Modulos externos
        vm.confirmarEliminar = confirmarEliminar;

        // Limpiar filtros
        limpiarFiltros();

        // Grid
        function obtenerResumen(tableState, ctrl){
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }
            if(pipePromise) $timeout.cancel(pipePromise);
            pipePromise = $timeout(function() {
                if(!vm.cargando){
                    vm.cargando = true;
                    genericService.obtenerColeccion(recurso, tableState)
                        .then(function (response){
                            if(response.status === Constantes.Response.HTTP_OK){
                                var coleccion = response.data.datos;
                                if(coleccion && coleccion.length > 0 ){
                                    vm.coleccion = [].concat(coleccion);
                                    vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                                    vm.paginacion.paginaActual = response.data.pagina_actual;
                                    vm.paginacion.desde = response.data.desde;
                                    vm.paginacion.hasta = response.data.hasta;
                                    vm.paginacion.total = response.data.total;
                                    tableState.pagination.numberOfPages = response.data.ultima_pagina;
                                }else{
                                    vm.coleccion = [];
                                }
                            }
                        }).finally(function () {
                            vm.cargando = false;
                        });
                }
            }, 500);
        }

        function limpiarFiltros(){
            vm.nombre = null;
            vm.marca = null;
            $scope.isReset = true;
        }

        /*function cargarFiltros() {
            var filtros = gridUtil.getFormattedSearchPredicate("tipo-de-actuación");
            vm.nombre = filtros && filtros.nombre ? filtros.nombre : null;
        }*/

        // Acciones

        function eliminar(row){
            genericService.eliminar(recurso, row.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        $scope.$broadcast('refreshGrid');
                        messageUtil.success(response.data.mensajes[0]);
                    }else if(
                        response.status === Constantes.Response.HTTP_CONFLICT ||
                        response.status === Constantes.Response.HTTP_BAD_REQUEST
                    ){
                        messageUtil.error(response.data.mensajes[0]);
                    }
                });
        }

        // Modulos externos

        function crear(tipoActuacion) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + recurso + '/create',
                controller: 'tipoActuacionEditController as vm',
                size: 'md',
                resolve: {
                    tipoActuacion: function () {
                        return tipoActuacion;
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(tipoActuacion && tipoActuacion.id && response && response.id){
                    tipoActuacion.nombre = response.nombre;
                    tipoActuacion.estado = response.estado;
                    tipoActuacion.usuario_modificacion_id = response.usuario_modificacion_id;
                    tipoActuacion.usuario_modificacion_nombre = response.usuario_modificacion_nombre;
                    tipoActuacion.fecha_creacion = response.fecha_creacion;
                    tipoActuacion.fecha_modificacion = response.fecha_modificacion;
                }else if(response && response.id){
                    $scope.$broadcast('refreshGrid');
                }
            });
        }

        function confirmarEliminar(row) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + 'confirmar-template',
                controller: 'confirmarController as vm',
                size: 'md',
                resolve: {
                    parametros: function () {
                        return {
                            mensaje: "¿Estás seguro de que quieres eliminar el tipo de actuación?"
                        };
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(response.ok){
                    eliminar(row);
                }
            });
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('tipoActuacionEditController', tipoActuacionEditController);

    tipoActuacionEditController.$inject = [
        '$uibModalInstance',
        'genericService',
        'messageUtil',
        'Constantes',
        'tipoActuacion'
    ];

    function tipoActuacionEditController(
        $uibModalInstance, genericService, messageUtil, Constantes, tipoActuacion
    ){
        var vm = this,
            recurso = "tipos-actuaciones";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        if(tipoActuacion && tipoActuacion.id){
            cargar();
        }else{
            vm.estado = true;
        }

        function cargar() {
            genericService.cargar(recurso, tipoActuacion.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var tipoActuacion = response.data;
                        vm.nombre = tipoActuacion.nombre;
                        vm.estado = tipoActuacion.estado;
                        vm.fechaCreacion = tipoActuacion.fecha_creacion;
                        vm.fechaModificacion = tipoActuacion.fecha_modificacion;
                        vm.usuarioCreacionNombre = tipoActuacion.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = tipoActuacion.usuario_modificacion_nombre;
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;
                var datos = {
                    id: tipoActuacion ? +tipoActuacion.id : null,
                    nombre: vm.nombre.toUpperCase(),
                    estado: vm.estado
                };

                var promesa = null;
                if(!(tipoActuacion && tipoActuacion.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        $uibModalInstance.close(response.data.datos);
                        messageUtil.success(response.data.mensajes[0]);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }
                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('tipoNotificacionController', tipoNotificacionController);

    tipoNotificacionController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        '$uibModal',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function tipoNotificacionController(
        $scope, $routeParams, $timeout, $uibModal, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "tipos-notificaciones";

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.crear = crear;

        // Modulos externos
        vm.confirmarEliminar = confirmarEliminar;

        // Limpiar filtros
        limpiarFiltros();

        // Grid
        function obtenerResumen(tableState, ctrl){
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }
            if(pipePromise) $timeout.cancel(pipePromise);
            pipePromise = $timeout(function() {
                if(!vm.cargando){
                    vm.cargando = true;
                    genericService.obtenerColeccion(recurso, tableState)
                        .then(function (response){
                            if(response.status === Constantes.Response.HTTP_OK){
                                var coleccion = response.data.datos;
                                if(coleccion && coleccion.length > 0 ){
                                    vm.coleccion = [].concat(coleccion);
                                    vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                                    vm.paginacion.paginaActual = response.data.pagina_actual;
                                    vm.paginacion.desde = response.data.desde;
                                    vm.paginacion.hasta = response.data.hasta;
                                    vm.paginacion.total = response.data.total;
                                    tableState.pagination.numberOfPages = response.data.ultima_pagina;
                                }else{
                                    vm.coleccion = [];
                                }
                            }
                        }).finally(function () {
                            vm.cargando = false;
                        });
                }
            }, 500);
        }

        function limpiarFiltros(){
            vm.nombre = null;
            vm.marca = null;
            $scope.isReset = true;
        }

        /*function cargarFiltros() {
            var filtros = gridUtil.getFormattedSearchPredicate("tipoNotificacion");
            vm.nombre = filtros && filtros.nombre ? filtros.nombre : null;
        }*/

        // Acciones

        function eliminar(row){
            genericService.eliminar(recurso, row.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        $scope.$broadcast('refreshGrid');
                        messageUtil.success(response.data.mensajes[0]);
                    }else if(
                        response.status === Constantes.Response.HTTP_CONFLICT ||
                        response.status === Constantes.Response.HTTP_BAD_REQUEST
                    ){
                        messageUtil.error(response.data.mensajes[0]);
                    }
                });
        }

        // Modulos externos

        function crear(tipoNotificacion) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + recurso + '/create',
                controller: 'tipoNotificacionEditController as vm',
                size: 'md',
                resolve: {
                    tipoNotificacion: function () {
                        return tipoNotificacion;
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(tipoNotificacion && tipoNotificacion.id && response && response.id){
                    tipoNotificacion.nombre = response.nombre;
                    tipoNotificacion.estado = response.estado;
                    tipoNotificacion.usuario_modificacion_id = response.usuario_modificacion_id;
                    tipoNotificacion.usuario_modificacion_nombre = response.usuario_modificacion_nombre;
                    tipoNotificacion.fecha_creacion = response.fecha_creacion;
                    tipoNotificacion.fecha_modificacion = response.fecha_modificacion;
                }else if(response && response.id){
                    $scope.$broadcast('refreshGrid');
                }
            });
        }

        function confirmarEliminar(row) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + 'confirmar-template',
                controller: 'confirmarController as vm',
                size: 'md',
                resolve: {
                    parametros: function () {
                        return {
                            mensaje: "¿Estás seguro de que quieres eliminar el tipo de notificación?"
                        };
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(response.ok){
                    eliminar(row);
                }
            });
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('tipoNotificacionEditController', tipoNotificacionEditController);

    tipoNotificacionEditController.$inject = [
        '$uibModalInstance',
        'genericService',
        'messageUtil',
        'Constantes',
        'tipoNotificacion'
    ];

    function tipoNotificacionEditController(
        $uibModalInstance, genericService, messageUtil, Constantes, tipoNotificacion
    ){
        var vm = this,
            recurso = "tipos-notificaciones";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        if(tipoNotificacion && tipoNotificacion.id){
            cargar();
        }else{
            vm.estado = true;
        }

        function cargar() {
            genericService.cargar(recurso, tipoNotificacion.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var tipoNotificacion = response.data;
                        vm.nombre = tipoNotificacion.nombre;
                        vm.estado = tipoNotificacion.estado;
                        vm.fechaCreacion = tipoNotificacion.fecha_creacion;
                        vm.fechaModificacion = tipoNotificacion.fecha_modificacion;
                        vm.usuarioCreacionNombre = tipoNotificacion.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = tipoNotificacion.usuario_modificacion_nombre;
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;
                var datos = {
                    id: tipoNotificacion ? +tipoNotificacion.id : null,
                    nombre: vm.nombre.toUpperCase(),
                    estado: vm.estado
                };

                var promesa = null;
                if(!(tipoNotificacion && tipoNotificacion.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        $uibModalInstance.close(response.data.datos);
                        messageUtil.success(response.data.mensajes[0]);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }
                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('salarioMinimoController', salarioMinimoController);

    salarioMinimoController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        '$uibModal',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function salarioMinimoController(
        $scope, $routeParams, $timeout, $uibModal, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "salarios-minimos";

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.crear = crear;

        // Modulos externos
        vm.confirmarEliminar = confirmarEliminar;

        // Limpiar filtros
        limpiarFiltros();

        // Grid
        function obtenerResumen(tableState, ctrl){
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }
            if(pipePromise) $timeout.cancel(pipePromise);
            pipePromise = $timeout(function() {
                if(!vm.cargando){
                    vm.cargando = true;
                    genericService.obtenerColeccion(recurso, tableState)
                        .then(function (response){
                            if(response.status === Constantes.Response.HTTP_OK){
                                var coleccion = response.data.datos;
                                if(coleccion && coleccion.length > 0 ){
                                    vm.coleccion = [].concat(coleccion);
                                    vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                                    vm.paginacion.paginaActual = response.data.pagina_actual;
                                    vm.paginacion.desde = response.data.desde;
                                    vm.paginacion.hasta = response.data.hasta;
                                    vm.paginacion.total = response.data.total;
                                    tableState.pagination.numberOfPages = response.data.ultima_pagina;
                                }else{
                                    vm.coleccion = [];
                                }
                            }
                        }).finally(function () {
                            vm.cargando = false;
                        });
                }
            }, 500);
        }

        function limpiarFiltros(){
            vm.filtroFecha = null;
            $scope.isReset = true;
        }

        /*function cargarFiltros() {
            var filtros = gridUtil.getFormattedSearchPredicate("salarioMinimo");
            vm.nombre = filtros && filtros.nombre ? filtros.nombre : null;
        }*/

        // Acciones

        function eliminar(row){
            genericService.eliminar(recurso, row.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        $scope.$broadcast('refreshGrid');
                        messageUtil.success(response.data.mensajes[0]);
                    }else if(
                        response.status === Constantes.Response.HTTP_CONFLICT ||
                        response.status === Constantes.Response.HTTP_BAD_REQUEST
                    ){
                        messageUtil.error(response.data.mensajes[0]);
                    }
                });
        }

        // Modulos externos

        function crear(salarioMinimo) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + recurso + '/create',
                controller: 'salarioMinimoEditController as vm',
                size: 'md',
                resolve: {
                    salarioMinimo: function () {
                        return salarioMinimo;
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(salarioMinimo && salarioMinimo.id && response && response.id){
                    salarioMinimo.fecha = response.fecha;
                    salarioMinimo.valor = response.valor;
                    salarioMinimo.estado = response.estado;
                    salarioMinimo.usuario_modificacion_id = response.usuario_modificacion_id;
                    salarioMinimo.usuario_modificacion_nombre = response.usuario_modificacion_nombre;
                    salarioMinimo.fecha_creacion = response.fecha_creacion;
                    salarioMinimo.fecha_modificacion = response.fecha_modificacion;
                }else if(response && response.id){
                    $scope.$broadcast('refreshGrid');
                }
            });
        }

        function confirmarEliminar(row) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + 'confirmar-template',
                controller: 'confirmarController as vm',
                size: 'md',
                resolve: {
                    parametros: function () {
                        return {
                            mensaje: "¿Estás seguro de que quieres eliminar el salarioMinimo?"
                        };
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(response.ok){
                    eliminar(row);
                }
            });
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('salarioMinimoEditController', salarioMinimoEditController);

    salarioMinimoEditController.$inject = [
        '$uibModalInstance',
        'genericService',
        'messageUtil',
        'Constantes',
        'salarioMinimo'
    ];

    function salarioMinimoEditController(
        $uibModalInstance, genericService, messageUtil, Constantes, salarioMinimo
    ){
        var vm = this,
            recurso = "salarios-minimos";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        if(salarioMinimo && salarioMinimo.id){
            cargar();
        }else{
            vm.estado = true;
        }

        function cargar() {
            genericService.cargar(recurso, salarioMinimo.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var salarioMinimo = response.data;
                        vm.fecha = salarioMinimo.fecha;
                        vm.valor = salarioMinimo.valor_salario_min;
                        vm.estado = salarioMinimo.estado;
                        vm.fechaCreacion = salarioMinimo.fecha_creacion;
                        vm.fechaModificacion = salarioMinimo.fecha_modificacion;
                        vm.usuarioCreacionNombre = salarioMinimo.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = salarioMinimo.usuario_modificacion_nombre;
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;
                var datos = {
                    id: salarioMinimo ? +salarioMinimo.id : null,
                    fecha: vm.fecha,
                    valor_salario_min: vm.valor,
                    estado: vm.estado
                };

                var promesa = null;
                if(!(salarioMinimo && salarioMinimo.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        $uibModalInstance.close(response.data.datos);
                        messageUtil.success(response.data.mensajes[0]);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }
                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                    //location.reload();
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('tesController', tesController);

    tesController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        '$uibModal',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function tesController(
        $scope, $routeParams, $timeout, $uibModal, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "TES";

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.crear = crear;

        // Modulos externos
        vm.confirmarEliminar = confirmarEliminar;

        // Limpiar filtros
        limpiarFiltros();

        // Grid
        function obtenerResumen(tableState, ctrl){
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }
            if(pipePromise) $timeout.cancel(pipePromise);
            pipePromise = $timeout(function() {
                if(!vm.cargando){
                    vm.cargando = true;
                    genericService.obtenerColeccion(recurso, tableState)
                        .then(function (response){
                            if(response.status === Constantes.Response.HTTP_OK){
                                var coleccion = response.data.datos;
                                if(coleccion && coleccion.length > 0 ){
                                    vm.coleccion = [].concat(coleccion);
                                    vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                                    vm.paginacion.paginaActual = response.data.pagina_actual;
                                    vm.paginacion.desde = response.data.desde;
                                    vm.paginacion.hasta = response.data.hasta;
                                    vm.paginacion.total = response.data.total;
                                    tableState.pagination.numberOfPages = response.data.ultima_pagina;
                                }else{
                                    vm.coleccion = [];
                                }
                            }
                        }).finally(function () {
                            vm.cargando = false;
                        });
                }
            }, 500);
        }

        function limpiarFiltros(){
            vm.filtroFecha = null;
            $scope.isReset = true;
        }

        /*function cargarFiltros() {
            var filtros = gridUtil.getFormattedSearchPredicate("tes");
            vm.nombre = filtros && filtros.nombre ? filtros.nombre : null;
        }*/

        // Acciones

        function eliminar(row){
            genericService.eliminar(recurso, row.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        $scope.$broadcast('refreshGrid');
                        messageUtil.success(response.data.mensajes[0]);
                    }else if(
                        response.status === Constantes.Response.HTTP_CONFLICT ||
                        response.status === Constantes.Response.HTTP_BAD_REQUEST
                    ){
                        messageUtil.error(response.data.mensajes[0]);
                    }
                });
        }

        // Modulos externos

        function crear(tes) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + recurso + '/create',
                controller: 'tesEditController as vm',
                size: 'md',
                resolve: {
                    tes: function () {
                        return tes;
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(tes && tes.id && response && response.id){
                    tes.fecha = response.fecha;
                    tes.porcentaje_1anio = response.porcentaje_1anio;
                    tes.porcentaje_5anio = response.porcentaje_5anio;
                    tes.porcentaje_10anio = response.porcentaje_10anio;
                    tes.estado = response.estado;
                    tes.usuario_modificacion_id = response.usuario_modificacion_id;
                    tes.usuario_modificacion_nombre = response.usuario_modificacion_nombre;
                    tes.fecha_creacion = response.fecha_creacion;
                    tes.fecha_modificacion = response.fecha_modificacion;
                }else if(response && response.id){
                    $scope.$broadcast('refreshGrid');
                }
            });
        }

        function confirmarEliminar(row) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + 'confirmar-template',
                controller: 'confirmarController as vm',
                size: 'md',
                resolve: {
                    parametros: function () {
                        return {
                            mensaje: "¿Estás seguro de que quieres eliminar el tes?"
                        };
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(response.ok){
                    eliminar(row);
                }
            });
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('tesEditController', tesEditController);

    tesEditController.$inject = [
        '$uibModalInstance',
        'genericService',
        'messageUtil',
        'Constantes',
        'tes'
    ];

    function tesEditController(
        $uibModalInstance, genericService, messageUtil, Constantes, tes
    ){
        var vm = this,
            recurso = "TES";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        if(tes && tes.id){
            cargar();
            // var today = moment().millisecond(0).second(0).minute(0).hour(0),
            //     fechaMaxima = moment(tes.fecha).millisecond(0).second(0).minute(0).hour(0);
            //
            // vm.datetimepickerOptions = {
            //     useCurrent: false,
            //     maxDate: fechaMaxima.diff(today, 'days') >= 0 ? today : fechaMaxima,
            //     format: 'YYYY-MM-DD'
            // };
        }else{
            vm.estado = true;
        }

        function cargar() {
            genericService.cargar(recurso, tes.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var tes = response.data;
                        vm.fecha = tes.fecha;
                        vm.porcentaje1Anio = tes.porcentaje_1anio;
                        vm.porcentaje5Anio = tes.porcentaje_5anio;
                        vm.porcentaje10Anio = tes.porcentaje_10anio;
                        vm.estado = tes.estado;
                        vm.fechaCreacion = tes.fecha_creacion;
                        vm.fechaModificacion = tes.fecha_modificacion;
                        vm.usuarioCreacionNombre = tes.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = tes.usuario_modificacion_nombre;
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;
                var datos = {
                    id: tes ? +tes.id : null,
                    fecha: vm.fecha,
                    porcentaje_1anio: vm.porcentaje1Anio,
                    porcentaje_5anio: vm.porcentaje5Anio,
                    porcentaje_10anio: vm.porcentaje10Anio,
                    estado: vm.estado
                };

                var promesa = null;
                if(!(tes && tes.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        $uibModalInstance.close(response.data.datos);
                        messageUtil.success(response.data.mensajes[0]);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }
                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('ipcController', ipcController);

    ipcController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        '$uibModal',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function ipcController(
        $scope, $routeParams, $timeout, $uibModal, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "IPC";

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.crear = crear;

        // Modulos externos
        vm.confirmarEliminar = confirmarEliminar;

        // Limpiar filtros
        limpiarFiltros();

        // Grid
        function obtenerResumen(tableState, ctrl){
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }
            if(pipePromise) $timeout.cancel(pipePromise);
            pipePromise = $timeout(function() {
                if(!vm.cargando){
                    vm.cargando = true;
                    genericService.obtenerColeccion(recurso, tableState)
                        .then(function (response){
                            if(response.status === Constantes.Response.HTTP_OK){
                                var coleccion = response.data.datos;
                                if(coleccion && coleccion.length > 0 ){
                                    vm.coleccion = [].concat(coleccion);
                                    vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                                    vm.paginacion.paginaActual = response.data.pagina_actual;
                                    vm.paginacion.desde = response.data.desde;
                                    vm.paginacion.hasta = response.data.hasta;
                                    vm.paginacion.total = response.data.total;
                                    tableState.pagination.numberOfPages = response.data.ultima_pagina;
                                }else{
                                    vm.coleccion = [];
                                }
                            }
                        }).finally(function () {
                            vm.cargando = false;
                        });
                }
            }, 500);
        }

        function limpiarFiltros(){
            vm.filtroFecha = null;
            $scope.isReset = true;
        }

        /*function cargarFiltros() {
            var filtros = gridUtil.getFormattedSearchPredicate("ipc");
            vm.nombre = filtros && filtros.nombre ? filtros.nombre : null;
        }*/

        // Acciones

        function eliminar(row){
            genericService.eliminar(recurso, row.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        $scope.$broadcast('refreshGrid');
                        messageUtil.success(response.data.mensajes[0]);
                    }else if(
                        response.status === Constantes.Response.HTTP_CONFLICT ||
                        response.status === Constantes.Response.HTTP_BAD_REQUEST
                    ){
                        messageUtil.error(response.data.mensajes[0]);
                    }
                });
        }

        // Modulos externos

        function crear(ipc) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + recurso + '/create',
                controller: 'ipcEditController as vm',
                size: 'md',
                resolve: {
                    ipc: function () {
                        return ipc;
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(ipc && ipc.id && response && response.id){
                    ipc.fecha = response.fecha;
                    ipc.porcentaje_1anio = response.porcentaje_1anio;
                    ipc.porcentaje_5anio = response.porcentaje_5anio;
                    ipc.porcentaje_10anio = response.porcentaje_10anio;
                    ipc.estado = response.estado;
                    ipc.usuario_modificacion_id = response.usuario_modificacion_id;
                    ipc.usuario_modificacion_nombre = response.usuario_modificacion_nombre;
                    ipc.fecha_creacion = response.fecha_creacion;
                    ipc.fecha_modificacion = response.fecha_modificacion;
                }else if(response && response.id){
                    $scope.$broadcast('refreshGrid');
                }
            });
        }

        function confirmarEliminar(row) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + 'confirmar-template',
                controller: 'confirmarController as vm',
                size: 'md',
                resolve: {
                    parametros: function () {
                        return {
                            mensaje: "¿Estás seguro de que quieres eliminar el IPC?"
                        };
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(response.ok){
                    eliminar(row);
                }
            });
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('ipcEditController', ipcEditController);

    ipcEditController.$inject = [
        '$uibModalInstance',
        'genericService',
        'messageUtil',
        'Constantes',
        'ipc'
    ];

    function ipcEditController(
        $uibModalInstance, genericService, messageUtil, Constantes, ipc
    ){
        var vm = this,
            recurso = "IPC";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        if(ipc && ipc.id){
            cargar();
            // var today = moment().millisecond(0).second(0).minute(0).hour(0),
            //     fechaMaxima = moment(ipc.fecha).millisecond(0).second(0).minute(0).hour(0);
            //
            // vm.datetimepickerOptions = {
            //     useCurrent: false,
            //     maxDate: fechaMaxima.diff(today, 'days') >= 0 ? today : fechaMaxima,
            //     format: 'YYYY-MM-DD'
            // };
        }else{
            vm.estado = true;
        }

        function cargar() {
            genericService.cargar(recurso, ipc.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var ipc = response.data;
                        vm.fecha = ipc.fecha;
                        vm.indiceIPC = ipc.indice_IPC;
                        vm.inflacionAnual = ipc.inflacion_anual;
                        vm.inflacionMensual = ipc.inflacion_mensual;
                        vm.inflacionAnio = ipc.inflacion_anio;
                        vm.estado = ipc.estado;
                        vm.fechaCreacion = ipc.fecha_creacion;
                        vm.fechaModificacion = ipc.fecha_modificacion;
                        vm.usuarioCreacionNombre = ipc.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = ipc.usuario_modificacion_nombre;
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;
                var datos = {
                    id: ipc ? +ipc.id : null,
                    fecha: vm.fecha,
                    indice_IPC: vm.indiceIPC,
                    inflacion_anual: vm.inflacionAnual,
                    inflacion_mensual: vm.inflacionMensual,
                    inflacion_anio: vm.inflacionAnio,
                    estado: vm.estado
                };

                var promesa = null;
                if(!(ipc && ipc.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        $uibModalInstance.close(response.data.datos);
                        messageUtil.success(response.data.mensajes[0]);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }
                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('auxiliarJusticiaController', auxiliarJusticiaController);

    auxiliarJusticiaController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        '$uibModal',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function auxiliarJusticiaController(
        $scope, $routeParams, $timeout, $uibModal, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "auxiliares-justicia";

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.crear = crear;

        // Modulos externos
        vm.confirmarEliminar = confirmarEliminar;

        // Limpiar filtros
        limpiarFiltros();

        // Grid
        function obtenerResumen(tableState, ctrl){
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }
            if(pipePromise) $timeout.cancel(pipePromise);
            pipePromise = $timeout(function() {
                if(!vm.cargando){
                    vm.cargando = true;
                    genericService.obtenerColeccion(recurso, tableState)
                        .then(function (response){
                            if(response.status === Constantes.Response.HTTP_OK){
                                var coleccion = response.data.datos;
                                if(coleccion && coleccion.length > 0 ){
                                    vm.coleccion = [].concat(coleccion);
                                    vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                                    vm.paginacion.paginaActual = response.data.pagina_actual;
                                    vm.paginacion.desde = response.data.desde;
                                    vm.paginacion.hasta = response.data.hasta;
                                    vm.paginacion.total = response.data.total;
                                    tableState.pagination.numberOfPages = response.data.ultima_pagina;
                                }else{
                                    vm.coleccion = [];
                                }
                            }
                        }).finally(function () {
                            vm.cargando = false;
                        });
                }
            }, 500);
        }

        function limpiarFiltros(){
            vm.filtroNumeroDocumento = null;
            vm.filtroNombresAuxiliares = null;
            vm.filtroApellidosAuxiliares = null;
            vm.filtroCiudadNombre = null;
            vm.filtroEstado = null;
            $scope.isReset = true;
        }

        /*function cargarFiltros() {
            var filtros = gridUtil.getFormattedSearchPredicate("auxiliarJusticia");
            vm.nombre = filtros && filtros.nombre ? filtros.nombre : null;
        }*/

        // Acciones

        function eliminar(row){
            genericService.eliminar(recurso, row.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        $scope.$broadcast('refreshGrid');
                        messageUtil.success(response.data.mensajes[0]);
                    }else if(
                        response.status === Constantes.Response.HTTP_CONFLICT ||
                        response.status === Constantes.Response.HTTP_BAD_REQUEST
                    ){
                        messageUtil.error(response.data.mensajes[0]);
                    }
                });
        }

        // Modulos externos

        function crear(auxiliarJusticia) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + recurso + '/create',
                controller: 'auxiliarJusticiaEditController as vm',
                size: 'lg',
                resolve: {
                    auxiliarJusticia: function () {
                        return auxiliarJusticia;
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(auxiliarJusticia && auxiliarJusticia.id && response && response.id){
                    auxiliarJusticia.numero_documento = response.numero_documento,
                    auxiliarJusticia.nombres_auxiliar = response.nombres_auxiliar,
                    auxiliarJusticia.apellidos_auxiliar = response.apellidos_auxiliar,
                    auxiliarJusticia.ciudad = response.nombre_ciudad,
                    auxiliarJusticia.direccion_oficina = response.direccion_oficina,
                    auxiliarJusticia.telefono_oficina = response.telefono_oficina,
                    auxiliarJusticia.cargo = response.cargo,
                    auxiliarJusticia.telefono_celular = response.telefono_celular,
                    auxiliarJusticia.direccion_residencia = response.direccion_residencia,
                    auxiliarJusticia.telefono_residencia = response.telefono_residencia,
                    auxiliarJusticia.correo = response.correo_electronico,
                    auxiliarJusticia.estado = response.estado;
                    auxiliarJusticia.usuario_modificacion_id = response.usuario_modificacion_id;
                    auxiliarJusticia.usuario_modificacion_nombre = response.usuario_modificacion_nombre;
                    auxiliarJusticia.fecha_creacion = response.fecha_creacion;
                    auxiliarJusticia.fecha_modificacion = response.fecha_modificacion;
                }else if(response && response.id){
                    $scope.$broadcast('refreshGrid');
                }
            });
        }
        function importar() {

        }

        function confirmarEliminar(row) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + 'confirmar-template',
                controller: 'confirmarController as vm',
                size: 'md',
                resolve: {
                    parametros: function () {
                        return {
                            mensaje: "¿Estás seguro de que quieres eliminar el auxiliar de la justicia?"
                        };
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(response.ok){
                    eliminar(row);
                }
            });
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('auxiliarJusticiaEditController', auxiliarJusticiaEditController);

    auxiliarJusticiaEditController.$inject = [
        '$uibModalInstance',
        'genericService',
        'messageUtil',
        'Constantes',
        'auxiliarJusticia'
    ];

    function auxiliarJusticiaEditController(
        $uibModalInstance, genericService, messageUtil, Constantes, auxiliarJusticia
    ){
        var vm = this,
            recurso = "auxiliares-justicia";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        if(auxiliarJusticia && auxiliarJusticia.id){
            cargar();
        }else{
            vm.estado = true;
        }

        function cargar() {
            genericService.cargar(recurso, auxiliarJusticia.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var auxiliarJusticia = response.data;
                        vm.numeroDocumento = auxiliarJusticia.numero_documento,
                        vm.nombresAuxiliares = auxiliarJusticia.nombres_auxiliar,
                        vm.apellidosAuxiliares = auxiliarJusticia.apellidos_auxiliar,
                        vm.ciudad = auxiliarJusticia.nombre_ciudad,
                        vm.direccionOficina = auxiliarJusticia.direccion_oficina,
                        vm.telefonoOficina = auxiliarJusticia.telefono_oficina,
                        vm.cargo = auxiliarJusticia.cargo,
                        vm.telefonoCelular = auxiliarJusticia.telefono_celular,
                        vm.direccionResidencia = auxiliarJusticia.direccion_residencia,
                        vm.telefonoResidencia = auxiliarJusticia.telefono_residencia,
                        vm.correo = auxiliarJusticia.correo_electronico,
                        vm.estado = auxiliarJusticia.estado;
                        vm.fechaCreacion = auxiliarJusticia.fecha_creacion;
                        vm.fechaModificacion = auxiliarJusticia.fecha_modificacion;
                        vm.usuarioCreacionNombre = auxiliarJusticia.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = auxiliarJusticia.usuario_modificacion_nombre;
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;
                var datos = {
                    id: auxiliarJusticia ? +auxiliarJusticia.id : null,
                    numero_documento: vm.numeroDocumento,
                    nombres_auxiliar: vm.nombresAuxiliares.toUpperCase(),
                    apellidos_auxiliar: vm.apellidosAuxiliares.toUpperCase(),
                    nombre_ciudad: vm.ciudad.toUpperCase(),
                    direccion_oficina: vm.direccionOficina.toUpperCase(),
                    telefono_oficina: vm.telefonoOficina,
                    cargo: vm.cargo,
                    telefono_celular: vm.telefonoCelular,
                    direccion_residencia: vm.direccionResidencia,
                    telefono_residencia: vm.telefonoResidencia,
                    correo_electronico: vm.correo,
                    estado: vm.estado
                };

                var promesa = null;
                if(!(auxiliarJusticia && auxiliarJusticia.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        $uibModalInstance.close(response.data.datos);
                        messageUtil.success(response.data.mensajes[0]);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }
                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('auxiliarJusticiaImportarController', auxiliarJusticiaImportarController);

    auxiliarJusticiaImportarController.$inject = [
        '$scope',
        '$http',
        '$routeParams',
        '$timeout',
        '$uibModal',
        'sessionUtil',
        'envService',
        'genericService',
        'Upload',
        'messageUtil',
        'Constantes'
    ];

    function auxiliarJusticiaImportarController(
        $scope, $http, $routeParams, $timeout, $uibModal, sessionUtil, envService, genericService, Upload, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "auxiliares-justicia";

        // Variables de inicio
        vm.pasoImportarDatos = true;
        vm.pasoVerificarDatos = false;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Instancias
        initWizard();

        // Acciones
        vm.finalizarImportarExcel = finalizarImportarExcel;
        vm.importarExcel = importarExcel;
        vm.obtenerExcel = obtenerExcel;

        // Acciones archivos
        vm.cargarArchivo = cargarArchivo;
        vm.descargarArchivo = descargarArchivo;
        vm.eliminarArchivo = eliminarArchivo;

        // Paso 1, para ir al Paso 2

        function importarExcel(){
            if(!vm.cargandoArchivo){
                vm.cargandoArchivo = true;
                genericService.cargarArchivo(recurso + "/excel", vm.archivo)
                    .then(function (response) {
                        if(response.status === Constantes.Response.HTTP_CREATED){
                            // Datos de respuesta
                            var datos = response.data.datos;
                            vm.errores = datos.errores;
                            vm.registrosFallidos = datos.registros_fallidos;
                            vm.registrosCargados = datos.registros_cargados;
                            vm.registrosProcesados = datos.registros_procesados;

                            // Mostrar mensaje de respuesta
                            messageUtil.success(response.data.mensajes[0]);
                        }
                    }).catch(function (error) {
                        vm.errores = [];
                        vm.registrosFallidos = 0;
                        vm.registrosCargados = 0;
                        vm.registrosProcesados = 0;

                        // Mostrar mensaje de respuesta
                        messageUtil.error(error.data.mensajes[0]);
                    }).finally(function () {
                        vm.pasoImportarDatos = false;
                        vm.pasoVerificarDatos = true;
                        vm.cargandoArchivo = false;
                    });
            }
        }

        function finalizarImportarExcel(){
            vm.pasoVerificarDatos = false;
            vm.archivo = null;
            vm.archivoNombre = null;
            vm.archivoBase64 = null;
            vm.pasoImportarDatos = true;
        }

        // Acciones archivos

        function cargarArchivo($archivo) {
            Upload.dataUrl($archivo, true).then(function(base64) {
                if(base64){
                    vm.archivo = $archivo;
                    vm.archivoNombre = $archivo.name;
                    vm.archivoBase64 = base64;
                }
            });
        }

        function descargarArchivo() {
            var a = document.createElement("a");
            document.body.appendChild(a);
            a.style = "display: none";

            var blob = Upload.dataUrltoBlob(vm.archivoBase64, vm.archivoNombre),
                url = window.URL.createObjectURL(blob);

            a.href = url;
            a.download = vm.archivoNombre;
            a.click();
            window.URL.revokeObjectURL(url);
        }

        // Acciones

        function obtenerExcel() {
            genericService.obtenerArchivo('auxiliares-justicia-errores' + "/excel",{});
        }

        function eliminarArchivo() {
            vm.archivo = null;
            vm.archivoNombre = null;
            vm.archivoBase64 = null;
        }

        // Instanciar modulos

        function initWizard(){
            //Initialize tooltips
            $('.nav-tabs > li a[title]').tooltip();
            //Wizard
            $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
                var $target = $(e.target);
                if ($target.parent().hasClass('disabled')) {
                    return false;
                }
            });
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('procesoJudicialImportarController', procesoJudicialImportarController);

    procesoJudicialImportarController.$inject = [
        '$scope',
        '$http',
        '$routeParams',
        '$timeout',
        '$uibModal',
        'sessionUtil',
        'envService',
        'genericService',
        'Upload',
        'messageUtil',
        'Constantes'
    ];

    function procesoJudicialImportarController(
        $scope, $http, $routeParams, $timeout, $uibModal, sessionUtil, envService, genericService, Upload, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "procesos-judiciales";

        // Variables de inicio
        vm.excelActivo = true;
        vm.textoActivo = false;
        vm.pasoImportarDatos = true;
        vm.pasoVerificarDatos = false;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Instancias
        initWizard();

        // Acciones
        vm.finalizarImportarExcel = finalizarImportarExcel;
        vm.importar = importar;
        vm.obtenerExcel = obtenerExcel;

        // Acciones archivos
        vm.cargarArchivo = cargarArchivo;
        vm.descargarArchivo = descargarArchivo;
        vm.eliminarArchivo = eliminarArchivo;

        // Paso 1, para ir al Paso 2

        function importar() {
            if(vm.excelActivo){
                importarExcel();
            }else{
                importarTexto();
            }
        }

        function importarExcel(){
            if(!vm.cargandoArchivo){
                vm.cargandoArchivo = true;
                genericService.cargarArchivo(recurso + "/importar-excel", vm.archivo)
                    .then(function (response) {
                        if(response.status === Constantes.Response.HTTP_CREATED){
                            // Datos de respuesta
                            var datos = response.data.datos;
                            vm.errores = datos.errores;
                            vm.registrosFallidos = datos.registros_fallidos;
                            vm.registrosCargados = datos.registros_cargados;
                            vm.registrosProcesados = datos.registros_procesados;

                            // Mostrar mensaje de respuesta
                            messageUtil.success(response.data.mensajes[0]);
                        }
                    }).catch(function (error) {
                        vm.errores = [];
                        vm.registrosFallidos = 0;
                        vm.registrosCargados = 0;
                        vm.registrosProcesados = 0;

                        // Mostrar mensaje de respuesta
                        messageUtil.error(error.data.mensajes[0]);
                    }).finally(function () {
                        vm.pasoImportarDatos = false;
                        vm.pasoVerificarDatos = true;
                        vm.cargandoArchivo = false;
                    });
            }
        }

        function importarTexto() {
            if(!vm.cargandoArchivo){
                vm.cargandoArchivo = true;
                genericService.crear(recurso + "/importar-texto", {
                    texto: vm.procesosEnTexto
                })
                .then(function (response) {
                    if(response.status === Constantes.Response.HTTP_CREATED){
                        // Datos de respuesta
                        var datos = response.data.datos;
                        vm.errores = datos.errores;
                        vm.registrosFallidos = datos.registros_fallidos;
                        vm.registrosCargados = datos.registros_cargados;
                        vm.registrosProcesados = datos.registros_procesados;

                        // Mostrar mensaje de respuesta
                        messageUtil.success(response.data.mensajes[0]);
                    }
                }).catch(function (error) {
                    vm.errores = [];
                    vm.registrosFallidos = 0;
                    vm.registrosCargados = 0;
                    vm.registrosProcesados = 0;

                    // Mostrar mensaje de respuesta
                    messageUtil.error(error.data.mensajes[0]);
                }).finally(function () {
                    vm.pasoImportarDatos = false;
                    vm.pasoVerificarDatos = true;
                    vm.cargandoArchivo = false;
                });
            }
        }

        function finalizarImportarExcel(){
            vm.pasoVerificarDatos = false;
            vm.procesosEnTexto = null;
            vm.archivo = null;
            vm.archivoNombre = null;
            vm.archivoBase64 = null;
            vm.pasoImportarDatos = true;
        }

        // Acciones archivos

        function cargarArchivo($archivo) {
            Upload.dataUrl($archivo, true).then(function(base64) {
                if(base64){
                    vm.archivo = $archivo;
                    vm.archivoNombre = $archivo.name;
                    vm.archivoBase64 = base64;
                }
            });
        }

        function descargarArchivo() {
            var a = document.createElement("a");
            document.body.appendChild(a);
            a.style = "display: none";

            var blob = Upload.dataUrltoBlob(vm.archivoBase64, vm.archivoNombre),
                url = window.URL.createObjectURL(blob);

            a.href = url;
            a.download = vm.archivoNombre;
            a.click();
            window.URL.revokeObjectURL(url);
        }

        // Acciones

        function obtenerExcel() {
            genericService.obtenerArchivo(recurso + "/importar-excel",{});
        }

        function eliminarArchivo() {
            vm.archivo = null;
            vm.archivoNombre = null;
            vm.archivoBase64 = null;
        }

        // Instanciar modulos

        function initWizard(){
            //Initialize tooltips
            $('.nav-tabs > li a[title]').tooltip();
            //Wizard
            $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
                var $target = $(e.target);
                if ($target.parent().hasClass('disabled')) {
                    return false;
                }
            });
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('actuacionImportarController', actuacionImportarController);

    actuacionImportarController.$inject = [
        '$scope',
        '$http',
        '$routeParams',
        '$timeout',
        '$uibModal',
        'sessionUtil',
        'envService',
        'genericService',
        'Upload',
        'messageUtil',
        'Constantes'
    ];

    function actuacionImportarController(
        $scope, $http, $routeParams, $timeout, $uibModal, sessionUtil, envService, genericService, Upload, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "actuaciones";

        // Variables de inicio
        vm.excelActivo = true;
        vm.textoActivo = false;
        vm.pasoImportarDatos = true;
        vm.pasoVerificarDatos = false;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Instancias
        initWizard();

        // Acciones
        vm.finalizarImportarExcel = finalizarImportarExcel;
        vm.importar = importar;
        vm.obtenerExcel = obtenerExcel;

        // Acciones archivos
        vm.cargarArchivo = cargarArchivo;
        vm.descargarArchivo = descargarArchivo;
        vm.eliminarArchivo = eliminarArchivo;

        // Paso 1, para ir al Paso 2

        function importar() {
            if(vm.excelActivo){
                importarExcel();
            }else{
                importarTexto();
            }
        }

        function importarExcel(){
            if(!vm.cargandoArchivo){
                vm.cargandoArchivo = true;
                genericService.cargarArchivo(recurso + "/importar-excel", vm.archivo)
                    .then(function (response) {
                        if(response.status === Constantes.Response.HTTP_CREATED){
                            // Datos de respuesta
                            var datos = response.data.datos;
                            vm.errores = datos.errores;
                            vm.registrosFallidos = datos.registros_fallidos;
                            vm.registrosCargados = datos.registros_cargados;
                            vm.registrosProcesados = datos.registros_procesados;

                            // Mostrar mensaje de respuesta
                            messageUtil.success(response.data.mensajes[0]);
                        }
                    }).catch(function (error) {
                        vm.errores = [];
                        vm.registrosFallidos = 0;
                        vm.registrosCargados = 0;
                        vm.registrosProcesados = 0;

                        // Mostrar mensaje de respuesta
                        messageUtil.error(error.data.mensajes[0]);
                    }).finally(function () {
                        vm.pasoImportarDatos = false;
                        vm.pasoVerificarDatos = true;
                        vm.cargandoArchivo = false;
                    });
            }
        }

        function importarTexto() {
            if(!vm.cargandoArchivo){
                vm.cargandoArchivo = true;
                genericService.crear(recurso + "/importar-texto", {
                    texto: vm.procesosEnTexto
                })
                .then(function (response) {
                    if(response.status === Constantes.Response.HTTP_CREATED){
                        // Datos de respuesta
                        var datos = response.data.datos;
                        vm.errores = datos.errores;
                        vm.registrosFallidos = datos.registros_fallidos;
                        vm.registrosCargados = datos.registros_cargados;
                        vm.registrosProcesados = datos.registros_procesados;

                        // Mostrar mensaje de respuesta
                        messageUtil.success(response.data.mensajes[0]);
                    }
                }).catch(function (error) {
                    vm.errores = [];
                    vm.registrosFallidos = 0;
                    vm.registrosCargados = 0;
                    vm.registrosProcesados = 0;

                    // Mostrar mensaje de respuesta
                    messageUtil.error(error.data.mensajes[0]);
                }).finally(function () {
                    vm.pasoImportarDatos = false;
                    vm.pasoVerificarDatos = true;
                    vm.cargandoArchivo = false;
                });
            }
        }

        function finalizarImportarExcel(){
            vm.pasoVerificarDatos = false;
            vm.procesosEnTexto = null;
            vm.archivo = null;
            vm.archivoNombre = null;
            vm.archivoBase64 = null;
            vm.pasoImportarDatos = true;
        }

        // Acciones archivos

        function cargarArchivo($archivo) {
            Upload.dataUrl($archivo, true).then(function(base64) {
                if(base64){
                    vm.archivo = $archivo;
                    vm.archivoNombre = $archivo.name;
                    vm.archivoBase64 = base64;
                }
            });
        }

        function descargarArchivo() {
            var a = document.createElement("a");
            document.body.appendChild(a);
            a.style = "display: none";

            var blob = Upload.dataUrltoBlob(vm.archivoBase64, vm.archivoNombre),
                url = window.URL.createObjectURL(blob);

            a.href = url;
            a.download = vm.archivoNombre;
            a.click();
            window.URL.revokeObjectURL(url);
        }

        // Acciones

        function obtenerExcel() {
            genericService.obtenerArchivo(recurso + "/importar-excel",{});
        }

        function eliminarArchivo() {
            vm.archivo = null;
            vm.archivoNombre = null;
            vm.archivoBase64 = null;
        }

        // Instanciar modulos

        function initWizard(){
            //Initialize tooltips
            $('.nav-tabs > li a[title]').tooltip();
            //Wizard
            $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
                var $target = $(e.target);
                if ($target.parent().hasClass('disabled')) {
                    return false;
                }
            });
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('clienteController', clienteController);

    clienteController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        '$uibModal',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function clienteController(
        $scope, $routeParams, $timeout, $uibModal, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "clientes";

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.crear = crear;
        vm.obtenerExcel = obtenerExcel;

        // Modulos externos
        vm.confirmarEliminar = confirmarEliminar;

        // Limpiar filtros
        limpiarFiltros();

        // Grid
        function obtenerResumen(tableState, ctrl){
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }
            if(pipePromise) $timeout.cancel(pipePromise);
            pipePromise = $timeout(function() {
                if(!vm.cargando){
                    vm.cargando = true;
                    genericService.obtenerColeccion(recurso, tableState)
                        .then(function (response){
                            if(response.status === Constantes.Response.HTTP_OK){
                                var coleccion = response.data.datos;
                                if(coleccion && coleccion.length > 0 ){
                                    vm.coleccion = [].concat(coleccion);
                                    vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                                    vm.paginacion.paginaActual = response.data.pagina_actual;
                                    vm.paginacion.desde = response.data.desde;
                                    vm.paginacion.hasta = response.data.hasta;
                                    vm.paginacion.total = response.data.total;
                                    tableState.pagination.numberOfPages = response.data.ultima_pagina;
                                }else{
                                    vm.coleccion = [];
                                }
                            }
                        }).finally(function () {
                            vm.cargando = false;
                        });
                }
            }, 500);
        }

        function limpiarFiltros(){
                vm.filtroNombre = null;
                vm.filtroNumeroDocumento = null;
                vm.filtroCiudadNombre = null;
                $scope.isReset = true;
        }

        /*function cargarFiltros() {
            var filtros = gridUtil.getFormattedSearchPredicate("cliente");
            vm.nombre = filtros && filtros.nombre ? filtros.nombre : null;
        }*/

        function obtenerExcel() {
            genericService.obtenerArchivo(recurso + "/excel", {
                nombre: vm.filtroNombre,
                numero_documento: vm.filtroNumeroDocumento,
                ciudad_nombre: vm.filtroCiudadNombre
            });
        }

        // Acciones

        function eliminar(row){
            genericService.eliminar(recurso, row.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        $scope.$broadcast('refreshGrid');
                        messageUtil.success(response.data.mensajes[0]);
                    }else if(
                        response.status === Constantes.Response.HTTP_CONFLICT ||
                        response.status === Constantes.Response.HTTP_BAD_REQUEST
                    ){
                        messageUtil.error(response.data.mensajes[0]);
                    }
                });
        }

        // Modulos externos

        function crear(cliente) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + recurso + '/create',
                controller: 'clienteEditController as vm',
                size: 'md',
                resolve: {
                    cliente: function () {
                        return cliente;
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(cliente && cliente.id && response && response.id){
                    cliente.nombre = response.nombre;
                    cliente.tipo_documento_codigo = response.tipo_documento.codigo;
                    cliente.numero_documento = response.numero_documento;
                    cliente.departamento_nombre = response.departamento.nombre;
                    cliente.ciudad_nombre = response.ciudad.nombre;
                    cliente.telefono_uno = response.telefono_uno;
                    cliente.telefono_dos = response.telefono_dos;
                    cliente.email = response.email;
                    cliente.estado = response.estado;
                    cliente.usuario_modificacion_id = response.usuario_modificacion_id;
                    cliente.usuario_modificacion_nombre = response.usuario_modificacion_nombre;
                    cliente.fecha_creacion = response.fecha_creacion;
                    cliente.fecha_modificacion = response.fecha_modificacion;

                }else if(response && response.id){
                    $scope.$broadcast('refreshGrid');
                }
            });
        }

        function confirmarEliminar(row) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + 'confirmar-template',
                controller: 'confirmarController as vm',
                size: 'lg',
                resolve: {
                    parametros: function () {
                        return {
                            mensaje: "¿Estás seguro de que quieres eliminar el cliente?"
                        };
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(response.ok){
                    eliminar(row);
                }
            });
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('clienteEditController', clienteEditController);

    clienteEditController.$inject = [
        '$uibModalInstance',
        '$timeout',
        'genericService',
        'messageUtil',
        'Constantes',
        'cliente'
    ];

    function clienteEditController(
        $uibModalInstance, $timeout, genericService, messageUtil, Constantes, cliente
    ){
        var vm = this,
            recurso = "clientes";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        // inicial autocomples
        initAutocompleteDepartamento();
        initAutocompleteCiudad();
        initAutocompleteTipoDocumento();

        if(cliente && cliente.id){
            cargar();
        }else{
            vm.estado = true;
        }

        function cargar() {
            genericService.cargar(recurso, cliente.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var cliente = response.data,
                            ciudad = cliente.ciudad,
                            departamento = cliente.departamento,
                            tipo_documento = cliente.tipo_documento;

                        vm.nombre = cliente.nombre;
                        vm.numeroDocumento = cliente.numero_documento;
                        vm.direccion = cliente.direccion;
                        vm.telefonoFijo = cliente.telefono_uno;
                        vm.telefonoCelular = cliente.telefono_dos;
                        vm.correo = cliente.email;
                        vm.estado = cliente.estado;
                        vm.fechaCreacion = cliente.fecha_creacion;
                        vm.fechaModificacion = cliente.fecha_modificacion;
                        vm.usuarioCreacionNombre = cliente.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = cliente.usuario_modificacion_nombre;

                        if(ciudad){
                            vm.departamentoId = departamento.id;
                            vm.opcionesDepartamento.push({id: departamento.id, nombre: departamento.nombre});
                            vm.ciudadId = ciudad.id;
                            vm.opcionesCiudad.push({id: ciudad.id, nombre: ciudad.nombre});
                            vm.limpiarOpcionesCiudad = false;
                        }
                        if(tipo_documento){
                            vm.tipoDocumentoId = tipo_documento.id;
                            vm.opcionesTipoDocumento.push({id: tipo_documento.id, codigo: tipo_documento.codigo});
                        }
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;

                var datos = {
                    id: cliente ? +cliente.id : null,
                    nombre: vm.nombre.toUpperCase(),
                    tipo_documento_id: vm.tipoDocumentoId,
                    numero_documento: vm.numeroDocumento,
                    departamento_id: vm.departamentoId,
                    ciudad_id: vm.ciudadId,
                    direccion: vm.direccion,
                    telefono_uno: vm.telefonoFijo,
                    telefono_dos: vm.telefonoCelular,
                    email: vm.correo,
                    estado: vm.estado

            };

                var promesa = null;
                if(!(cliente && cliente.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        messageUtil.success(response.data.mensajes[0]);
                        $uibModalInstance.close(response.data.datos);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }


                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }

        function initAutocompleteDepartamento() {
            vm.configDepartamento = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                onItemAdd: function (value, $item) {
                    if(vm.departamentoId !== value){
                        vm.opcionesCiudad = [];
                        $timeout(function () {
                            vm.limpiarOpcionesCiudad = true;
                            genericService.obtenerColeccionLigera("ciudades", {
                                ligera: true,
                                departamento_id: value
                            })
                            .then(function (response){
                                vm.opcionesCiudad = [].concat(response.data);
                            });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.opcionesCiudad = [];
                    vm.ciudadId = null;
                    $timeout(function () {
                        vm.limpiarOpcionesCiudad = true;
                    }, 100);
                }
            };

            vm.opcionesDepartamento = [];
            genericService.obtenerColeccionLigera("departamentos",{
                ligera: true,
            })
            .then(function (response){
                vm.opcionesDepartamentos = [].concat(response.data);
            });
        }

        function initAutocompleteCiudad() {
            vm.configCiudad = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                lock: false,
            };
            vm.opcionesCiudad = [];
        }

        function initAutocompleteTipoDocumento() {
            vm.configTipoDocumento = {
                valueField: 'id',
                labelField: 'codigo',
                searchField: ['codigo'],
                sortField: 'codigo',
                allowEmptyOption: false,
                create: false,
                persist: true,
            };

            vm.opcionesTipoDocumento = [];
            genericService.obtenerColeccionLigera("tipos-documentos",{
                ligera: true,
            })
            .then(function (response){
                vm.opcionesTipoDocumento = [].concat(response.data);
            });
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('procesoJudicialController', procesoJudicialController);

    procesoJudicialController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        '$uibModal',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function procesoJudicialController(
        $scope, $routeParams, $timeout, $uibModal, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "procesos-judiciales";

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.crear = crear;

        // Modulos externos
        vm.confirmarEliminar = confirmarEliminar;

        // Limpiar filtros
        limpiarFiltros();

        // Grid
        function obtenerResumen(tableState, ctrl){
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }
            if(pipePromise) $timeout.cancel(pipePromise);
            pipePromise = $timeout(function() {
                if(!vm.cargando){
                    vm.cargando = true;
                    genericService.obtenerColeccion(recurso, tableState)
                        .then(function (response){
                            if(response.status === Constantes.Response.HTTP_OK){
                                var coleccion = response.data.datos;
                                if(coleccion && coleccion.length > 0 ){
                                    vm.coleccion = [].concat(coleccion);
                                    vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                                    vm.paginacion.paginaActual = response.data.pagina_actual;
                                    vm.paginacion.desde = response.data.desde;
                                    vm.paginacion.hasta = response.data.hasta;
                                    vm.paginacion.total = response.data.total;
                                    tableState.pagination.numberOfPages = response.data.ultima_pagina;
                                }else{
                                    vm.coleccion = [];
                                }
                            }
                        }).finally(function () {
                            vm.cargando = false;
                        });
                }
            }, 500);
        }

        function limpiarFiltros(){
            vm.nombre = null;
            vm.ciudad = null;
            $scope.isReset = true;
        }

        // Acciones

        function eliminar(row){
            genericService.eliminar(recurso, row.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        $scope.$broadcast('refreshGrid');
                        messageUtil.success(response.data.mensajes[0]);
                    }else if(
                        response.status === Constantes.Response.HTTP_CONFLICT ||
                        response.status === Constantes.Response.HTTP_BAD_REQUEST
                    ){
                        messageUtil.error(response.data.mensajes[0]);
                    }
                });
        }

        // Modulos externos

        function crear(procesoJudicial) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + recurso + '/create',
                controller: 'procesoJudicialEditController as vm',
                size: 'lg',
                resolve: {
                    procesoJudicial: function () {
                        return procesoJudicial;
                    }
                }
            });
            modalInstance.result.then(function (response) {
                $scope.$broadcast('refreshGrid');
            });
        }

        function confirmarEliminar(row) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + 'confirmar-template',
                controller: 'confirmarController as vm',
                size: 'lg',
                resolve: {
                    parametros: function () {
                        return {
                            mensaje: "¿Estás seguro de que quieres eliminar el proceso?"
                        };
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(response.ok){
                    eliminar(row);
                }
            });
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('procesoJudicialEditController', procesoJudicialEditController)
        .value('cgBusyDefaults',{
            message: 'Consultando proceso en la rama...'
        });

    procesoJudicialEditController.$inject = [
        '$uibModalInstance',
        '$timeout',
        'genericService',
        'sessionUtil',
        'langUtil',
        'messageUtil',
        'Constantes',
        'procesoJudicial'
    ];

    function procesoJudicialEditController(
        $uibModalInstance, $timeout, genericService, sessionUtil, langUtil, messageUtil, Constantes, procesoJudicial
    ){
        var vm = this,
            recurso = "procesos-judiciales";

        // Acciones
        vm.validarYGuardar = validarYGuardar;
        vm.cancelar = cancelar;
        vm.confirmarValidar = confirmarValidar;
        vm.validar = validar;
        vm.validarAnio = validarAnio;
        vm.validarRadicado = validarRadicado;
        vm.validarConsecutivo = validarConsecutivo;
        vm.modificarConsecutivo = modificarConsecutivo;
        vm.validarDemandante = validarDemandante;
        vm.validarDemandado = validarDemandado;
        vm.validarObservaciones = validarObservaciones;
        vm.establecerCambioInstancia = establecerCambioInstancia;

        // inicial autocomples
        initAutocompleteDepartamento();
        initAutocompleteCiudad();
        initAutocompleteJuzgado();
        initAutocompleteDespacho();
        initAutocompleteResponsable();
        initAutocompleteClaseProceso();
        initAutocompleteEtapaProceso();
        initAutocompleteArea();
        initAutocompleteCliente();
        initAutocompleteTipoActuacion();

        // Cargar pantalla
        init();

        function init() {
            // Parametros constantes
            vm.entorno = sessionUtil.getParametros();

            // Enumeradores
            vm.CambioInstanciaEnum = Constantes.CambioInstancia;

            vm.promesasActivas = [];
            var minDate = moment().subtract(40, 'years');
            vm.anioOptions = {
                useCurrent: false, minDate: minDate, maxDate: 'now', format: 'YYYY'
            };
            if(procesoJudicial && procesoJudicial.id){
                vm.modoEdicion = true;
                cargar();
            }else{
                vm.procesoValidado = false;
                vm.camposSinValidarOcultos = true;
            }

            // Configuración busy
            vm.cbusy = {
                promise: vm.promesasActivas,
                message:'Validando proceso...'
            }

            // Mostrar campos
            if(vm.entorno && +vm.entorno.VER_RESPONSABLE_EN_PROCESO){
                vm.verResponsable = true;
            }
            if(vm.entorno && +vm.entorno.VER_CLASE_PROCESO_EN_PROCESO){
                vm.verClaseProceso = true;
            }
            if(vm.entorno && +vm.entorno.VER_AREA_EN_PROCESO){
                vm.verArea = true;
            }

            // Evitar letras radicado
            $timeout(function () {
                $('.only-numbers').on("keypress", function (event) {
                    var charCode = (event.keyCode ? event.keyCode : event.which);
                    if (charCode !== 46 && charCode > 31 && (charCode < 48 || charCode > 57)){
                        event.preventDefault();
                    }
                });
            }, 100);
        }

        function cargar() {
            genericService.cargar(recurso, procesoJudicial.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var procesoJudicial = response.data;

                        vm.anio = procesoJudicial.anio_proceso;
                        vm.radicado = procesoJudicial.numero_radicado;
                        vm.radicadoProcesoActual = procesoJudicial.numero_radicado_proceso_actual;
                        vm.anioNombre = procesoJudicial.anio_proceso;
                        vm.radicadoNombre = procesoJudicial.numero_radicado;
                        vm.consecutivoNombre = procesoJudicial.numero_radicado_proceso_actual.substring(21, 23);
                        vm.radicadoProcesoActualNombre = procesoJudicial.numero_radicado_proceso_actual;
                        vm.observaciones = procesoJudicial.observaciones;
                        vm.demandante = procesoJudicial.nombres_demandantes;
                        vm.demandado = procesoJudicial.nombres_demandados;
                        vm.fechaCreacion = procesoJudicial.fecha_creacion;
                        vm.fechaModificacion = procesoJudicial.fecha_modificacion;
                        vm.usuarioCreacionNombre = procesoJudicial.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = procesoJudicial.usuario_modificacion_nombre;
                        vm.estado = procesoJudicial.indicativo_estado;
                        if(procesoJudicial.departamento){
                            vm.departamentoId = procesoJudicial.departamento.id;
                            vm.departamentoNombre = procesoJudicial.departamento.nombre;
                            vm.opcionesDepartamento.push({
                                id: procesoJudicial.departamento.id,
                                nombre: procesoJudicial.departamento.nombre,
                                codigo_dane: procesoJudicial.departamento.codigo_dane
                            });
                            vm.departamentoIdOriginal = procesoJudicial.departamento.id;
                        }
                        if(procesoJudicial.ciudad){
                            vm.ciudadNombre = procesoJudicial.ciudad.nombre;
                            vm.opcionesCiudad.push({
                                id: procesoJudicial.ciudad.id,
                                nombre: procesoJudicial. ciudad.nombre,
                                codigo_dane: procesoJudicial.ciudad.codigo_dane
                            });
                            vm.ciudadIdOriginal = procesoJudicial.ciudad.id;
                        }
                        if(procesoJudicial.juzgado){
                            vm.juzgadoNombre = procesoJudicial.juzgado.nombre;
                            vm.opcionesJuzgado.push({
                                id: procesoJudicial.juzgado.id,
                                nombre: procesoJudicial.juzgado.nombre,
                                juzgado: procesoJudicial.juzgado.juzgado,
                                sala: procesoJudicial.juzgado.sala
                            });
                            vm.juzgadoIdOriginal = procesoJudicial.juzgado.id;
                        }
                        if(procesoJudicial.despacho){
                            vm.despachoNombre = procesoJudicial.despacho.nombre;
                            vm.opcionesDespacho.push({
                                id: procesoJudicial.despacho.id,
                                nombre: procesoJudicial.despacho.nombre,
                                despacho: procesoJudicial.despacho.despacho
                            });
                            vm.despachoIdOriginal = procesoJudicial.despacho.id;
                        }
                        if(procesoJudicial.abogado){
                            vm.responsableId = procesoJudicial.abogado.id;
                            vm.opcionesResponsable.push({
                                id: procesoJudicial.abogado.id, nombre: procesoJudicial.abogado.nombre
                            });
                        }
                        if(procesoJudicial.clase_proceso){
                            vm.claseProcesoId = procesoJudicial.clase_proceso.id;
                            vm.opcionesClaseProceso.push({
                                id: procesoJudicial.clase_proceso.id, nombre: procesoJudicial.clase_proceso.nombre
                            });
                        }
                        if(procesoJudicial.etapa_proceso){
                            vm.etapaProcesoId = procesoJudicial.etapa_proceso.id;
                            vm.opcionesEtapaProceso.push({
                                id: procesoJudicial.etapa_proceso.id, nombre: procesoJudicial.etapa_proceso.nombre
                            });
                        }
                        if(procesoJudicial.area){
                            vm.areaId = procesoJudicial.area.id;
                            vm.opcionesArea.push({
                                id: procesoJudicial.area.id, nombre: procesoJudicial.area.nombre
                            });
                        }
                        if(procesoJudicial.cliente){
                            vm.clienteId = procesoJudicial.cliente.id;
                            vm.opcionesCliente.push({
                                id: procesoJudicial.cliente.id, nombre: procesoJudicial.cliente.nombre
                            });
                        }
                        if(procesoJudicial.tipo_actuacion){
                            vm.tipoActuacionId = procesoJudicial.tipo_actuacion.id;
                            vm.opcionesTipoActuacion.push({
                                id: procesoJudicial.tipo_actuacion.id, nombre: procesoJudicial.tipo_actuacion.nombre
                            });
                        }
                    }
                });
        }

        function validarYGuardar() {
            if(!vm.procesoValidado){
                validar();
            }else{
                guardar();
            }
        }

        function confirmarValidar() {
            vm.procesoValidado = false;
            if(
                !vm.procesoValidado &&
                vm.radicadoProcesoActual && vm.radicadoProcesoActual.trim().length === 23
            ){
                validar();
            }
        }

        function validar() {
            if(
                !vm.procesoValidado &&
                vm.radicadoProcesoActual && vm.radicadoProcesoActual.trim().length === 23 && !vm.guardadoDeshabilitado
            ){
                vm.guardadoDeshabilitado = true;
                var datos = {
                    id: procesoJudicial ? +procesoJudicial.id : null,
                    departamento_id: vm.departamentoId ? vm.departamentoId : null,
                    ciudad_id: vm.ciudadId ? vm.ciudadId : null,
                    juzgado_id: vm.juzgadoId ? vm.juzgadoId : null,
                    despacho_id: vm.despachoId ? vm.despachoId : null,
                    anio_proceso: vm.anio ? vm.anio : null,
                    numero_radicado: vm.radicado ? vm.radicado.trim() : null,
                    numero_radicado_proceso_actual: vm.radicadoProcesoActual ? vm.radicadoProcesoActual.trim() : null
                };
                if(vm.modoEdicion){
                    datos.con_cambio_instancia = vm.conCambioInstancia;
                }
                var promesa = genericService.crear(recurso + "/validacion", datos);
                vm.promesasActivas.push(promesa);
                promesa.then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var reporte = response.data.datos;
                        if(reporte.demandante || reporte.demandado){
                            vm.demandante = reporte.demandante ? reporte.demandante.toUpperCase() : vm.demandante;
                            vm.demandado = reporte.demandado ? reporte.demandado.toUpperCase() : vm.demandado;
                        }else{
                            messageUtil.warning(reporte.mensaje);
                        }
                        vm.procesoValidado = true;
                        vm.camposSinValidarOcultos = false;
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }
                }).finally(function () {
                    vm.guardadoDeshabilitado = false;
                });
            }else if(!(procesoJudicial && procesoJudicial.id) && !vm.procesoValidado && !vm.guardadoDeshabilitado){
                messageUtil.error("Tamaño del radicado proceso actual errado. Debe tener 23 dígitos.");
            }
        }

        function validarAnio() {
            if(vm.anio && vm.anio.length < 4){
                messageUtil.error("Tamaño de número de año errado. Debe tener 4 digitos.");
            }
        }

        function validarRadicado() {
            if(vm.radicado && vm.radicado.length < 5){
                vm.radicado = langUtil.zeroFill(vm.radicado, 5);
            }
        }

        function validarConsecutivo() {
            if(vm.consecutivo && vm.consecutivo.length < 2){
                vm.consecutivo = langUtil.zeroFill(vm.consecutivo, 2);
            }
        }

        function modificarConsecutivo() {
            var consecutivo = vm.consecutivo;
            if(consecutivo && consecutivo.length < 2){
                consecutivo = langUtil.zeroFill(consecutivo, 2);
            }
            if(vm.radicadoProcesoActual && vm.radicadoProcesoActual.length >= 21){
                vm.radicadoProcesoActual = vm.radicadoProcesoActual.substring(0, 21) +
                    (consecutivo ? consecutivo : '');
            }
            if(+vm.consecutivo > 5){
                messageUtil.error("El dato consecutivo debe ser menor o igual a 5.");
            }
        }

        function validarDemandante() {
            vm.demandante = vm.demandante ? vm.demandante.toUpperCase() : vm.demandante;
        }

        function validarDemandado() {
            vm.demandado = vm.demandado ? vm.demandado.toUpperCase() : vm.demandado;
        }

        function validarObservaciones() {
            vm.observaciones = vm.observaciones ? vm.observaciones.toUpperCase() : vm.observaciones;
        }

        function establecerCambioInstancia() {
            if(vm.conCambioInstancia && vm.conCambioInstancia === vm.conCambioInstanciaActual){
                vm.conCambioInstancia = null;
            }else{
                vm.conCambioInstanciaActual = vm.conCambioInstancia;
            }

            var numeroConsecutivoActual = vm.radicadoProcesoActualNombre.substr(21, 2);
            if(vm.conCambioInstancia === Constantes.CambioInstancia.INFERIOR && parseInt(numeroConsecutivoActual) === 0){
                vm.conCambioInstancia = vm.conCambioInstancia === Constantes.CambioInstancia.SUPERIOR;
                messageUtil.error("No puede existir instancia anterior para el proceso.");
            }
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.guardadoDeshabilitado = true;
                var datos = {
                    id: procesoJudicial ? +procesoJudicial.id : null,
                    departamento_id: vm.departamentoId,
                    ciudad_id: vm.ciudadId,
                    juzgado_id: vm.juzgadoId,
                    despacho_id: vm.despachoId,
                    anio_proceso: vm.anio ? vm.anio : null,
                    numero_radicado: vm.radicado.trim(),
                    numero_radicado_proceso_actual: vm.radicadoProcesoActual.trim(),
                    nombres_demandantes: vm.demandante.trim(),
                    nombres_demandados: vm.demandado.trim(),
                    observaciones: vm.observaciones ? vm.observaciones.trim() : null,
                    abogado_responsable_id: langUtil.isNotBlank(vm.responsableId) ? vm.responsableId : null,
                    ultima_clase_proceso_id: langUtil.isNotBlank(vm.claseProcesoId) ? vm.claseProcesoId : null,
                    area_id: langUtil.isNotBlank(vm.areaId) ? vm.areaId : null
                };
                if(vm.modoEdicion){
                    datos.con_cambio_instancia = vm.conCambioInstancia;
                    datos.ultima_etapa_proceso_id = langUtil.isNotBlank(vm.etapaProcesoId) ? vm.etapaProcesoId : null;
                    datos.cliente_id = langUtil.isNotBlank(vm.clienteId) ? vm.clienteId : null;
                    datos.ultimo_tipo_actuacion_id = langUtil.isNotBlank(vm.tipoActuacionId) ? vm.tipoActuacionId : null;
                    datos.indicativo_estado = vm.estado;
                }

                var promesa = null;
                if(!(procesoJudicial && procesoJudicial.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        messageUtil.success(response.data.mensajes[0]);
                        $uibModalInstance.close(response.data.datos);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }
                }).finally(function () {
                    vm.guardadoDeshabilitado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }

        function initAutocompleteDepartamento() {
            vm.configDepartamento = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                render: {
                    item: function(item, escape) {
                        return '<div>' +
                            '<span>' + escape(item.nombre) + '</span>' +
                            '<span> - ' + escape(item.codigo_dane) + '</span>' +
                            '</div>';
                    },
                    option: function(item, escape) {
                        return '<div>' +
                            '<span>' + escape(item.nombre) + '</span>' +
                            '<span> - ' + escape(item.codigo_dane) + '</span>' +
                            '</div>';
                    }
                },
                onItemAdd: function (value, $item) {
                    if(!vm.cargandoCiudades){
                        vm.cargandoCiudades = true;
                        vm.despachoId = null;
                        vm.juzgadoId = null;
                        vm.ciudadId = null;
                        vm.opcionesDespacho = [];
                        vm.opcionesJuzgado = [];
                        vm.opcionesCiudad = [];
                        $timeout(function () {
                            vm.limpiarOpcionesDespacho = true;
                            vm.limpiarOpcionesJuzgado = true;
                            vm.limpiarOpcionesCiudad = true;
                            genericService.obtenerColeccionLigera("ciudades", {
                                ligera: true,
                                departamento_id: value
                            }).then(function (response){
                                vm.opcionesCiudad = [].concat(response.data);
                                if(vm.ciudadIdOriginal){
                                    vm.ciudadId = vm.ciudadIdOriginal;
                                    vm.ciudadIdOriginal = null;
                                }
                            }).finally(function () {
                                vm.cargandoCiudades = false;
                            });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.despachoId = null;
                    vm.juzgadoId = null;
                    vm.ciudadId = null;
                    vm.opcionesDespacho = [];
                    vm.opcionesJuzgado = [];
                    vm.opcionesCiudad = [];
                    $timeout(function () {
                        vm.limpiarOpcionesDespacho = true;
                        vm.limpiarOpcionesJuzgado = true;
                        vm.limpiarOpcionesCiudad = true;
                    }, 300);
                }
            };
            vm.opcionesDepartamento = [];
            genericService.obtenerColeccionLigera("departamentos",{
                ligera: true,
            })
            .then(function (response){
                vm.opcionesDepartamento = [].concat(response.data);
            });
        }

        function initAutocompleteCiudad() {
            vm.configCiudad = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                render: {
                    item: function(item, escape) {
                        return '<div>' +
                            '<span>' + escape(item.nombre) + '</span>' +
                            '<span> - ' + escape(item.codigo_dane) + '</span>' +
                            '</div>';
                    },
                    option: function(item, escape) {
                        return '<div>' +
                            '<span>' + escape(item.nombre) + '</span>' +
                            '<span> - ' + escape(item.codigo_dane) + '</span>' +
                            '</div>';
                    }
                },
                onItemAdd: function (value, $item) {
                    if(!vm.cargandoJuzgados){
                        vm.cargandoJuzgados = true;
                        vm.despachoId = null;
                        vm.juzgadoId = null;
                        vm.opcionesDespacho = [];
                        vm.opcionesJuzgado = [];
                        $timeout(function () {
                            vm.limpiarOpcionesDespacho = true;
                            vm.limpiarOpcionesJuzgado = true;
                            genericService.obtenerColeccionLigera("juzgados", {
                                ligera: true,
                                ciudad_id: value,
                                juzgado_actual: 1
                            }).then(function (response){
                                vm.opcionesJuzgado = [].concat(response.data);
                                if(vm.juzgadoIdOriginal){
                                    vm.juzgadoId = vm.juzgadoIdOriginal;
                                    vm.juzgadoIdOriginal = null;
                                }
                            }).finally(function () {
                                vm.cargandoJuzgados = false;
                            });
                        }, 300);
                    }
                },
                onItemRemove: function () {
                    vm.despachoId = null;
                    vm.juzgadoId = null;
                    vm.opcionesDespacho = [];
                    vm.opcionesJuzgado = [];
                    $timeout(function () {
                        vm.limpiarOpcionesDespacho = true;
                        vm.limpiarOpcionesJuzgado = true;
                    }, 300);
                }
            };
            vm.opcionesCiudad = [];
        }

        function initAutocompleteJuzgado() {
            vm.configJuzgado = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                render: {
                    item: function(item, escape) {
                        return '<div>' +
                            '<span>' + escape(item.nombre) + '</span>' +
                            '<span> - ' + escape(item.juzgado) + escape(item.sala) + '</span>' +
                            '</div>';
                    },
                    option: function(item, escape) {
                        return '<div>' +
                            '<span>' + escape(item.nombre) + '</span>' +
                            '<span> - ' + escape(item.juzgado) + escape(item.sala) + '</span>' +
                            '</div>';
                    }
                },
                onItemAdd: function (value, $item) {
                    if(!vm.cargandoDespachos){
                        vm.cargandoDespachos = true;
                        vm.despachoId = null;
                        vm.opcionesDespacho = [];
                        $timeout(function () {
                            vm.limpiarOpcionesDespacho = true;
                            genericService.obtenerColeccionLigera("despachos", {
                                ligera: true,
                                ciudad_id: vm.ciudadId ? vm.ciudadId : null,
                                juzgado_id: value
                            })
                            .then(function (response){
                                vm.opcionesDespacho = [].concat(response.data);
                                if(vm.despachoIdOriginal){
                                    vm.despachoId = vm.despachoIdOriginal;
                                    vm.despachoIdOriginal = null;
                                }else{
                                    var juzgadoNombre = $item[0].innerText;
                                    var despacho000 = _.find(response.data, function (despacho) {
                                        return despacho.despacho == '000';
                                    });
                                    if(despacho000 && juzgadoNombre && !juzgadoNombre.includes('JUZGADO')){
                                        vm.despachoId = despacho000.id;
                                    }
                                }
                            }).finally(function () {
                                vm.cargandoDespachos = false;
                            });
                        }, 300);
                    }
                },
                onItemRemove: function () {
                    vm.despachoId = null;
                    vm.opcionesDespacho = [];
                    $timeout(function () {
                        vm.limpiarOpcionesDespacho = true;
                    }, 300);
                }
            };
            vm.opcionesJuzgado = [];
        }

        function initAutocompleteDespacho() {
            vm.configDespacho = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                render: {
                    item: function(item, escape) {
                        return '<div>' +
                            '<span>' + escape(item.despacho) + '</span>' +
                            '</div>';
                    },
                    option: function(item, escape) {
                        return '<div>' +
                            '<span>' + escape(item.despacho) + '</span>' +
                            '</div>';
                    }
                }
            };
            vm.opcionesDespacho = [];
        }

        function initAutocompleteResponsable() {
            vm.configResponsable = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
            };
            vm.opcionesResponsable = [];
            genericService.obtenerColeccionLigera("usuarios",{
                ligera: true,
            })
            .then(function (response){
                vm.opcionesResponsable = [].concat(response.data);
            });
        }

        function initAutocompleteClaseProceso() {
            vm.configClaseProceso = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
            };
            vm.opcionesClaseProceso = [];
            genericService.obtenerColeccionLigera("clases-de-procesos",{
                ligera: true,
            })
            .then(function (response){
                vm.opcionesClaseProceso = [].concat(response.data);
            });
        }

        function initAutocompleteEtapaProceso() {
            vm.configEtapaProceso = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
            };
            vm.opcionesEtapaProceso = [];
            genericService.obtenerColeccionLigera("etapas-de-procesos",{
                ligera: true,
            })
            .then(function (response){
                vm.opcionesEtapaProceso = [].concat(response.data);
            });
        }

        function initAutocompleteArea() {
            vm.configArea = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true
            };
            vm.opcionesArea = [];
            genericService.obtenerColeccionLigera("areas",{
                ligera: true
            })
            .then(function (response){
                vm.opcionesArea = [].concat(response.data);
            });
        }

        function initAutocompleteCliente() {
            vm.configCliente = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true
            };
            vm.opcionesCliente = [];
            genericService.obtenerColeccionLigera("clientes",{
                ligera: true
            })
            .then(function (response){
                vm.opcionesCliente = [].concat(response.data);
            });
        }

        function initAutocompleteTipoActuacion() {
            vm.configTipoActuacion = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true
            };
            vm.opcionesTipoActuacion = [];
            genericService.obtenerColeccionLigera("tipos-actuaciones",{
                ligera: true
            })
            .then(function (response){
                vm.opcionesTipoActuacion = [].concat(response.data);
            });
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('procesoValorController', procesoValorController);

    procesoValorController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        '$uibModal',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function procesoValorController(
        $scope, $routeParams, $timeout, $uibModal, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "procesos-valores",
            procesoId = $routeParams.id ? $routeParams.id : null;

        vm.coleccion = [];
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Cargar todos los valores del proceso
        cargar();

        // Cargar los datos de la proceso
        genericService.cargar("procesos-judiciales", procesoId)
            .then(function (response) {
                if (response.status === Constantes.Response.HTTP_OK) {
                    vm.proceso = response.data;
                }
            });

        function cargar(){
            vm.cargando = true;
            genericService.obtener(recurso, {proceso_id: procesoId})
            .then(function (response){
                if(response.status === Constantes.Response.HTTP_OK){
                    vm.coleccion = response.data;
                }
            }).finally(function () {
                vm.cargando = false;
            });
        }

        // Acciones
        vm.crear = crear;

        // Modulos externos
        vm.confirmarEliminar = confirmarEliminar;

        /*function cargarFiltros() {
            var filtros = gridUtil.getFormattedSearchPredicate("procesoValor");
            vm.nombre = filtros && filtros.nombre ? filtros.nombre : null;
        }*/

        // Acciones

        function eliminar(procesoValor){
            genericService.eliminar(recurso, procesoValor.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        $scope.$broadcast('refreshGrid');
                        messageUtil.success(response.data.mensajes[0]);
                        location.reload();
                    }else if(
                        response.status === Constantes.Response.HTTP_CONFLICT ||
                        response.status === Constantes.Response.HTTP_BAD_REQUEST
                    ){
                        messageUtil.error(response.data.mensajes[0]);
                    }
                });
        }

        // Modulos externos

        function crear(procesoValor) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + recurso + '/create',
                controller: 'procesoValorEditController as vm',
                size: 'md',
                resolve: {
                    parametros: function () {
                        if (procesoValor){
                            vm.procesoValor = procesoValor;
                        }else{
                            vm.procesoValor = [];
                        }
                        return {
                            procesoValor: vm.procesoValor,
                            proceso: vm.proceso
                        };
                    }
                }
            });

            modalInstance.result.then(function (response) {
                if(procesoValor && procesoValor.id && response && response.id){
                    procesoValor.concepto_id = response.concepto_id;
                    procesoValor.fecha = response.fecha;
                    procesoValor.valor = response.valor;
                    procesoValor.proceso_id = procesoId;
                    procesoValor.estado = response.estado;
                    procesoValor.observacion = response.observacion;
                    procesoValor.usuario_modificacion_id = response.usuario_modificacion_id;
                    procesoValor.usuario_modificacion_nombre = response.usuario_modificacion_nombre;
                    procesoValor.fecha_creacion = response.fecha_creacion;
                    procesoValor.fecha_modificacion = response.fecha_modificacion;
                }else if(response && response.id){
                    $scope.$broadcast('refreshGrid');
                }
            });
        }

        function confirmarEliminar(procesoValor) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + 'confirmar-template',
                controller: 'confirmarController as vm',
                size: 'md',
                resolve: {
                    parametros: function () {
                        return {
                            mensaje: "¿Estás seguro de que quieres eliminar el valor?"
                        };
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(response.ok){
                    eliminar(procesoValor);
                }
            });
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('procesoValorEditController', procesoValorEditController);

    procesoValorEditController.$inject = [
        '$uibModalInstance',
        '$timeout',
        'genericService',
        'messageUtil',
        'Constantes',
        'parametros'
    ];

    function procesoValorEditController(
        $uibModalInstance, $timeout, genericService, messageUtil, Constantes, parametros
    ){
        var vm = this,
            recurso = "procesos-valores";


        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;
        vm.cargarConceptos = cargarConceptos;

        // inicial autocomples
        initAutocompleteTipoConcepto();

        if(parametros.proceso && parametros.procesoValor.id){
            cargar();
        }else{
            vm.estado = true;
        }

        vm.proceso = parametros.proceso;

        function cargar() {
            genericService.cargar(recurso, parametros.procesoValor.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK) {
                        var procesoValor = response.data,
                            concepto = procesoValor.concepto;
                        vm.valor = procesoValor.valor;
                        vm.fecha = procesoValor.fecha;
                        vm.observacion = procesoValor.observacion;
                        vm.estado = procesoValor.estado;
                        vm.fechaCreacion = procesoValor.fecha_creacion;
                        vm.fechaModificacion = procesoValor.fecha_modificacion;
                        vm.usuarioCreacionNombre = procesoValor.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = procesoValor.usuario_modificacion_nombre;
                        vm.conceptoId = procesoValor.concepto_id;

                        if (concepto) {
                            vm.tipoConcepto = concepto.tipo_concepto;
                            vm.conceptoId = concepto.id;
                            vm.opcionesConceptos.push({id: concepto.id, nombre: concepto.nombre});
                        }

                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;
                var datos = {
                    id: parametros.procesoValor ? +parametros.procesoValor.id : null,
                    concepto_id: vm.conceptoId,
                    valor: vm.valor,
                    fecha: vm.fecha,
                    observacion: vm.observacion,
                    proceso_id: vm.proceso.id,
                    estado: vm.estado
                };

                var promesa = null;
                if(!(parametros.procesoValor && parametros.procesoValor.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        $uibModalInstance.close(response.data.datos);
                        messageUtil.success(response.data.mensajes[0]);
                        location.reload();
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }
                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }

        function cargarConceptos() {
            vm.opcionesConceptos = [];
            $timeout(function () {
                vm.limpiarOpcionesConceptos = true;
                genericService.obtenerColeccionLigera("conceptos-economicos",{
                    ligera: true,
                    tipo_concepto_id: vm.tipoConcepto
                })
                .then(function (response){
                    vm.opcionesConceptos = [].concat(response.data);
                });
            }, 100);
        }

        function initAutocompleteTipoConcepto() {
            vm.configConcepto = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
            };
            vm.opcionesConceptos = [];
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('procesoClienteController', procesoClienteController);

    procesoClienteController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        '$uibModal',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function procesoClienteController(
        $scope, $routeParams, $timeout, $uibModal, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "procesos-clientes",
            clienteId = $routeParams.id ? $routeParams.id : null;

        vm.usuarioCliente = ($routeParams.cliente && +$routeParams.cliente === 1);
        vm.empresa = sessionUtil.getEmpresa();

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Cargar los datos del cliente

        genericService.cargar("usuarios", clienteId)
            .then(function (response) {
                if (response.status === Constantes.Response.HTTP_OK) {
                    if (response.data.empresa.id === vm.empresa.id) {
                        vm.cliente = response.data;
                    }else{
                        vm.cliente = null;
                    }
                }
            });
        function obtenerResumen(tableState, ctrl){
            tableState.search = tableState.search || {};
            tableState.search.predicateObject = tableState.search.predicateObject || {};
            tableState.search.predicateObject.cliente_id = clienteId;
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }
            if(pipePromise) $timeout.cancel(pipePromise);
            pipePromise = $timeout(function() {
                if(!vm.cargando){
                    vm.cargando = true;
                    genericService.obtenerColeccion(recurso, tableState)
                        .then(function (response){
                            if(response.status === Constantes.Response.HTTP_OK){
                                var coleccion = response.data.datos;
                                if(coleccion && coleccion.length > 0 ){
                                    vm.coleccion = [].concat(coleccion);
                                    vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                                    vm.paginacion.paginaActual = response.data.pagina_actual;
                                    vm.paginacion.desde = response.data.desde;
                                    vm.paginacion.hasta = response.data.hasta;
                                    vm.paginacion.total = response.data.total;
                                    tableState.pagination.numberOfPages = response.data.ultima_pagina;
                                }else{
                                    vm.coleccion = [];
                                }
                            }
                        }).finally(function () {
                        vm.cargando = false;
                    });
                }
            }, 500);
        }

        // Acciones
        vm.crear = crear;

        // Modulos externos
        vm.confirmarEliminar = confirmarEliminar;

        // Limpiar filtros
        limpiarFiltros();

        function limpiarFiltros(){
            $scope.isReset = true;
        }

        /*function cargarFiltros() {
            var filtros = gridUtil.getFormattedSearchPredicate("procesoCliente");
            vm.nombre = filtros && filtros.nombre ? filtros.nombre : null;
        }*/

        // Acciones

        function eliminar(row){
            genericService.eliminar(recurso, row.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        $scope.$broadcast('refreshGrid');
                        messageUtil.success(response.data.mensajes[0]);
                    }else if(
                        response.status === Constantes.Response.HTTP_CONFLICT ||
                        response.status === Constantes.Response.HTTP_BAD_REQUEST
                    ){
                        messageUtil.error(response.data.mensajes[0]);
                    }
                });
        }

        // Modulos externos

        function crear(row) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + recurso + '/create',
                controller: 'procesoClienteEditController as vm',
                size: 'lg',
                resolve: {
                    parametros: function () {
                        if (row){
                            vm.procesoCliente = row;
                        }else{
                            vm.procesoCliente = [];
                        }
                        return {
                            procesoCliente: vm.procesoCliente,
                            cliente: vm.cliente,
                            empresa: vm.empresa
                        };
                    }
                }
            });

            modalInstance.result.then(function (response) {
                if(row && row.id && response && response.id){
                    procesoCliente.proceso_id = response.proceso_id;
                    procesoCliente.numero_proceso = response.numero_proceso;
                    procesoCliente.cliente_id = clienteId;
                    procesoCliente.estado = response.estado;
                    procesoCliente.usuario_modificacion_id = response.usuario_modificacion_id;
                    procesoCliente.usuario_modificacion_nombre = response.usuario_modificacion_nombre;
                    procesoCliente.fecha_creacion = response.fecha_creacion;
                    procesoCliente.fecha_modificacion = response.fecha_modificacion;
                }else if(response && response.id){
                    $scope.$broadcast('refreshGrid');
                }
            });
        }

        function confirmarEliminar(row) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + 'confirmar-template',
                controller: 'confirmarController as vm',
                size: 'md',
                resolve: {
                    parametros: function () {
                        return {
                            mensaje: "¿Estás seguro de que quieres eliminar el proceso por cliente?"
                        };
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(response.ok){
                    eliminar(row);
                }
            });
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('procesoClienteEditController', procesoClienteEditController);

    procesoClienteEditController.$inject = [
        '$uibModalInstance',
        '$timeout',
        'genericService',
        'messageUtil',
        'Constantes',
        'parametros'
    ];

    function procesoClienteEditController(
        $uibModalInstance, $timeout, genericService, messageUtil, Constantes, parametros
    ){
        var vm = this,
            recurso = "procesos-clientes";

        vm.procesoId = null;
        vm.empresa = parametros.empresa;
        vm.cliente = parametros.cliente;
        // Acciones
        vm.guardar = guardar;

        vm.cancelar = cancelar;
        // inicial autocomples
        initAutocompleteProceso();

        if(parametros.procesoCliente && parametros.procesoCliente.id){
            cargar();
        }else{
            vm.numero_proceso = null;
            vm.estado = true;
        }

        vm.cliente = parametros.cliente;

        function cargar() {
            genericService.cargar(recurso, parametros.procesoCliente.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK) {
                        var procesoCliente = response.data,
                            procesoJudicial = procesoCliente.procesoJudicial;
                        vm.estado = procesoCliente.estado;
                        vm.fechaCreacion = procesoCliente.fecha_creacion;
                        vm.fechaModificacion = procesoCliente.fecha_modificacion;
                        vm.usuarioCreacionNombre = procesoCliente.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = procesoCliente.usuario_modificacion_nombre;
                        vm.conceptoId = procesoCliente.concepto_id;
                        vm.numero_proceso = procesoCliente.numero_proceso;

                        if (procesoJudicial) {
                            vm.procesoId = procesoJudicial.id;
                            vm.opcionesProcesos.push({id: procesoJudicial.id, nombre: procesoJudicial.nombre});
                        }

                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;
                var datos = {
                    id: parametros.procesoCliente ? +parametros.procesoCliente.id : null,
                    usuario_id: vm.cliente.id,
                    proceso_id: vm.procesoId,
                    estado: vm.estado
                };

                var promesa = null;
                if(!(parametros.procesoCliente && parametros.procesoCliente.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        $uibModalInstance.close(response.data.datos);
                        messageUtil.success(response.data.mensajes[0]);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }
                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }

        function initAutocompleteProceso() {
            vm.configProceso = {
                valueField: 'id',
                labelField: 'numero_proceso',
                searchField: ['numero_proceso'],
                sortField: 'numero_proceso',
                allowEmptyOption: false,
                create: false,
                persist: true,
                load: function(query, callback) {
                    if (!query.length) return callback();
                    genericService.obtenerColeccionLigera("procesos-judiciales", {
                        q: query,
                        ligera: true,
                        limite: 100
                    }).then(function(response){
                        callback(response.data);
                        // Validar so hay datos
                        if(!response.data || response.data.length === 0){
                            vm.message = "No se encuentra en la lista";
                        }
                    }).catch(function(){
                        callback();
                    });
                },
                onItemAdd: function (value, $item) {
                    if(vm.procesoId !== value){
                        $timeout(function (){
                            genericService.cargar("procesos-judiciales", vm.procesoId)
                                .then(function (response) {
                                    vm.proceso = response.data;
                                    vm.numero_proceso = vm.proceso.numero_proceso;
                                    vm.message = null;
                                });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.opcionesProcesos = [];
                    vm.procesoId = null;
                    $timeout(function () {
                        vm.limpiarOpcionesProcesos = true;
                    }, 100);
                }
            };
            genericService.obtenerColeccionLigera("procesos-judiciales",{
                ligera: true,
                limite: 100
            })
            .then(function (response){
                $timeout(function () {
                    vm.opcionesProcesos = [].concat(response.data);
                }, 100);
            });
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('vigilanciaProcesoController', vigilanciaProcesoController);

    vigilanciaProcesoController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function vigilanciaProcesoController(
        $scope, $routeParams, $timeout, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "vigilancias-procesos";

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.filtroEstado = null;
        vm.activarBuscar = true;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.buscar = buscar;
        vm.obtenerExcel = obtenerExcel;

        // Limpiar filtros
        limpiarFiltros();

        // Grid
        function buscar() {
            vm.buscando = true;
            if(vm.form.$valid) {
                vm.activarBuscar = true;
                $scope.$broadcast('refreshGrid');
            }
        }

        function obtenerResumen(tableState, ctrl){
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }

            if(vm.paginacion && vm.paginacion.paginaActual && tableState.pagination){
                var start = tableState.pagination.start || 0,
                    number = tableState.pagination.number || 10,
                    nextPage = (start + number) / number;
                if(vm.paginacion.paginaActual != nextPage){
                    vm.activarBuscar = true;
                }else if(vm.activarBuscar){
                    tableState.pagination.start = 0;
                    tableState.pagination.number = vm.itemsPorPagina;
                }
            }

            if(vm.activarBuscar && !vm.cargando){
                vm.cargando = true;
                genericService.obtenerColeccion('procesos-judiciales', tableState).then(function (response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var coleccion = response.data.datos;
                        if(coleccion && coleccion.length > 0 ){
                            vm.coleccion = [].concat(coleccion);
                            vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                            vm.paginacion.paginaActual = response.data.pagina_actual;
                            vm.paginacion.desde = response.data.desde;
                            vm.paginacion.hasta = response.data.hasta;
                            vm.paginacion.total = response.data.total;
                            tableState.pagination.numberOfPages = response.data.ultima_pagina;
                        }else{
                            vm.coleccion = [];
                        }
                    }
                }).finally(function () {
                    vm.buscando = false;
                    vm.cargando = false;
                    vm.activarBuscar = false;
                });
            }
        }

        function limpiarFiltros(){
            vm.filtroProceso= null;
            vm.filtroEstado = null;
            vm.filtroDemandante = null;
            vm.filtroDemandado = null;
            vm.activarBuscar = true;
            $scope.isReset = true;
        }

        /*function cargarFiltros() {
            var filtros = gridUtil.getFormattedSearchPredicate("vigilanciaProceso");
            vm.nombre = filtros && filtros.nombre ? filtros.nombre : null;
        }*/

        // Acciones

        function obtenerExcel() {
            if(vm.coleccion.length > 0) {
                genericService.obtenerArchivo(recurso + "/excel", {
                    estado: vm.filtroEstado,
                    numero_proceso: vm.filtroProceso,
                    demandante: vm.filtroDemandante,
                    demandado: vm.filtroDemandado
                });
            }
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('procesoHoyController', procesoHoyController);

    procesoHoyController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function procesoHoyController(
        $scope, $routeParams, $timeout, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            today = new Date(),
            date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();

        vm.filtroFechaInicial = date;
        vm.filtroFechaFinal = date;

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.filtroEstado = null;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.obtenerExcel = obtenerExcel;
        vm.obtenerArchivoAnexo = obtenerArchivoAnexo;
        vm.obtenerArchivoEstadoCartelera = obtenerArchivoEstadoCartelera;

        // Limpiar filtros
        limpiarFiltros();

        // Grid
        function obtenerResumen(tableState, ctrl){
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }
            if(pipePromise) $timeout.cancel(pipePromise);
            pipePromise = $timeout(function() {
                if(!vm.cargando){
                    vm.cargando = true;
                    genericService.obtenerColeccion("actuaciones/reporte-por-empresa", tableState)
                        .then(function (response){
                            if(response.status === Constantes.Response.HTTP_OK){
                                var coleccion = response.data.datos;
                                if(coleccion && coleccion.length > 0 ){
                                    vm.coleccion = [].concat(coleccion);
                                    vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                                    vm.paginacion.paginaActual = response.data.pagina_actual;
                                    vm.paginacion.desde = response.data.desde;
                                    vm.paginacion.hasta = response.data.hasta;
                                    vm.paginacion.total = response.data.total;
                                    tableState.pagination.numberOfPages = response.data.ultima_pagina;
                                }else{
                                    vm.coleccion = [];
                                }
                            }
                        }).finally(function () {
                            vm.cargando = false;
                        });
                }
            }, 500);
        }

        function limpiarFiltros(){
            // vm.filtroFechaInicial = null;
            // vm.filtroFechaFinal = null;
            $scope.isReset = true;
        }

        /*function cargarFiltros() {
            var filtros = gridUtil.getFormattedSearchPredicate("procesoHoy");
            vm.nombre = filtros && filtros.nombre ? filtros.nombre : null;
        }*/

        // Acciones

        function obtenerExcel() {
            if(vm.coleccion.length > 0) {
                genericService.obtenerArchivo("actuaciones/procesos-hoy/excel", {
                    fecha_inicial: vm.filtroFechaInicial,
                    fecha_final: vm.filtroFechaFinal
                });
            }
        }

        function obtenerArchivoAnexo(row) {
            genericService.obtenerArchivo("actuaciones/" + row.id + "/archivo-anexo")
                .then(function (response) {
                    if(response.status === Constantes.Response.HTTP_NOT_FOUND){
                        messageUtil.error("No se encontro un archivo anexo.");
                    }else if(response.status === Constantes.Response.HTTP_FORBIDDEN){
                        messageUtil.error("No tiene permitido descargar este archivo.");
                    }
                });
        }

        function obtenerArchivoEstadoCartelera(row) {
            genericService.obtenerArchivo("actuaciones/" + row.id + "/archivo-estado-cartelera")
                .then(function (response) {
                    if(response.status === Constantes.Response.HTTP_NOT_FOUND){
                        messageUtil.error("No se encontro un archivo de estado cartelera.");
                    }else if(response.status === Constantes.Response.HTTP_FORBIDDEN){
                        messageUtil.error("No tiene permitido descargar este archivo.");
                    }
                });
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('procesoJuzgadoFechaController', procesoJuzgadoFechaController);

    procesoJuzgadoFechaController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function procesoJuzgadoFechaController(
        $scope, $routeParams, $timeout, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise;

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 5;
        $scope.isReset = true;
        vm.activarBuscar = false;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.buscar = buscar;
        vm.obtenerExcel = obtenerExcel;
        vm.obtenerArchivoAnexo = obtenerArchivoAnexo;

        // inicial autocomples
        initAutocompleteCiudad();
        initAutocompleteJuzgado();
        initAutocompleteDespacho();
        initAutocompleteProceso();

        // Limpiar filtros
        limpiarFiltros();

        // Grid

        function buscar() {
            vm.buscando = true;
            if(vm.form.$valid) {
                vm.activarBuscar = true;
                $scope.$broadcast('refreshGrid');
            }
        }

        function obtenerResumen(tableState, ctrl){
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }

            if(vm.paginacion && vm.paginacion.paginaActual && tableState.pagination){
                var start = tableState.pagination.start || 0,
                    number = tableState.pagination.number || 10,
                    nextPage = (start + number) / number;
                if(vm.paginacion.paginaActual != nextPage){
                    vm.activarBuscar = true;
                }else if(vm.activarBuscar){
                    tableState.pagination.start = 0;
                    tableState.pagination.number = vm.itemsPorPagina;
                }
            }

            if(vm.activarBuscar && !vm.cargando){
                vm.cargando = true;
                genericService.obtenerColeccion("actuaciones/reporte", tableState).then(function (response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var coleccion = response.data.datos;
                        if(coleccion && coleccion.length > 0 ){
                            vm.coleccion = [].concat(coleccion);
                            vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                            vm.paginacion.paginaActual = response.data.pagina_actual;
                            vm.paginacion.desde = response.data.desde;
                            vm.paginacion.hasta = response.data.hasta;
                            vm.paginacion.total = response.data.total;
                            tableState.pagination.numberOfPages = response.data.ultima_pagina;
                        }else{
                            vm.coleccion = [];
                        }
                    }
                }).finally(function () {
                    vm.buscando = false;
                    vm.cargando = false;
                    vm.activarBuscar = false;
                });
            }
        }


        function limpiarFiltros(){
            vm.filtroFecha = null;
            vm.filtroCiudad = null;
            vm.filtroJuzgado = null;
            vm.filtroDespacho = null;
            vm.filtroProceso = null;
            vm.coleccion = [];
        }

        // Acciones

        function obtenerExcel() {
            if(vm.coleccion.length > 0) {
                genericService.obtenerArchivo("actuaciones/procesos-juzgado-fecha/excel", {
                    fecha: vm.filtroFecha,
                    ciudad: vm.filtroCiudad,
                    juzgado: vm.filtroJuzgado,
                    despacho: vm.filtroDespacho,
                    proceso: vm.filtroProceso
                });
            }
        }

        function obtenerArchivoAnexo(row) {
            genericService.obtenerArchivo("actuaciones/" + row.id + "/archivo-anexo")
                .then(function (response) {
                    if(response.status === Constantes.Response.HTTP_NOT_FOUND){
                        messageUtil.error("No se encontro un archivo anexo.");
                    }else if(response.status === Constantes.Response.HTTP_FORBIDDEN){
                        messageUtil.error("No tiene permitido descargar este archivo.");
                    }
                });
        }

        function initAutocompleteCiudad() {
            vm.configCiudad = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                lock: false,
                onItemAdd: function (value, $item) {
                    if(vm.filtroCiudad !== value){
                        vm.opcionesJuzgado = [];
                        vm.opcionesDespacho = [];
                        vm.opcionesProceso = [];
                        vm.filtroJuzgado = null;
                        vm.filtroDespacho = null;
                        vm.filtroProceso = null;
                        $timeout(function () {
                            vm.limpiarOpcionesJuzgado = true;
                            vm.limpiarOpcionesDespacho = true;
                            vm.limpiarOpcionesProceso = true;
                            vm.filtroJuzgado = null;
                            vm.filtroDespacho = null;
                            vm.filtroProceso = null;
                            genericService.obtenerColeccionLigera("juzgados", {
                                ligera: true,
                                ciudad_id: value
                            })
                                .then(function (response){
                                    vm.opcionesJuzgado = [].concat(response.data);
                                });
                            genericService.obtenerColeccionLigera("procesos-judiciales", {
                                ligera: true,
                                ciudad_id: value,
                                limite: 100
                            })
                            .then(function (response){
                                vm.opcionesProceso = [].concat(response.data);
                            });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.opcionesJuzgado = [];
                    vm.opcionesDespacho = [];
                    vm.opcionesProceso = [];
                    vm.filtroJuzgado = null;
                    vm.filtroDespacho = null;
                    vm.filtroProceso = null;
                    $timeout(function () {
                        vm.limpiarOpcionesJuzgado = true;
                        vm.limpiarOpcionesDespacho = true;
                        vm.limpiarOpcionesProceso = true;
                    }, 100);
                }
            };
            vm.opcionesCiudad = [];
            genericService.obtenerColeccionLigera("ciudades", {
                ligera: true,
                con_cobertura: true
            })
            .then(function (response){
                vm.opcionesCiudad = [].concat(response.data);
            });
        }

        function initAutocompleteJuzgado() {
            vm.configJuzgado = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                onItemAdd: function (value, $item) {
                    if(vm.filtroJuzgado !== value){
                        vm.opcionesDespacho = [];
                        vm.filtroDespacho = null;
                        $timeout(function () {
                            vm.limpiarOpcionesDespacho = true;
                            genericService.obtenerColeccionLigera("despachos", {
                                ligera: true,
                                juzgado_id: value
                            })
                                .then(function (response){
                                    vm.opcionesDespacho = [].concat(response.data);
                                });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.opcionesDespacho = [];
                    vm.filtroDespacho = null;
                    $timeout(function () {
                        vm.limpiarOpcionesDespacho = true;
                    }, 100);
                }
            };

            vm.opcionesJuzgado = [];
        }

        function initAutocompleteDespacho() {
            vm.configDespacho = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
            };

            vm.opcionesDespacho = [];
        }

        function initAutocompleteProceso() {
            vm.configProceso = {
                valueField: 'id',
                labelField: 'numero_proceso',
                searchField: ['numero_proceso'],
                sortField: 'numero_proceso',
                allowEmptyOption: false,
                create: false,
                persist: true,
            };

            vm.opcionesProceso = [];
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('busquedaDemandanteDemandadoController', busquedaDemandanteDemandadoController);

    busquedaDemandanteDemandadoController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function busquedaDemandanteDemandadoController(
        $scope, $routeParams, $timeout, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise;

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 5;
        $scope.isReset = true;
        vm.activarBuscar = false;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.buscar = buscar;
        vm.obtenerExcel = obtenerExcel;
        vm.obtenerArchivoAnexo = obtenerArchivoAnexo;

        // inicial autocomples
        initAutocompleteDepartamento();
        initAutocompleteCiudad();

        // Limpiar filtros
            limpiarFiltros();

        // Grid

        function buscar() {
            vm.buscando = true;
            if(vm.form.$valid) {
                vm.activarBuscar = true;
                $scope.$broadcast('refreshGrid');
            }
        }

        function obtenerResumen(tableState, ctrl){
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }

            if(vm.paginacion && vm.paginacion.paginaActual && tableState.pagination){
                var start = tableState.pagination.start || 0,
                    number = tableState.pagination.number || 10,
                    nextPage = (start + number) / number;
                if(vm.paginacion.paginaActual != nextPage){
                    vm.activarBuscar = true;
                }else if(vm.activarBuscar){
                    tableState.pagination.start = 0;
                    tableState.pagination.number = vm.itemsPorPagina;
                }
            }

            if(vm.activarBuscar && !vm.cargando){
                vm.cargando = true;
                genericService.obtenerColeccion("actuaciones/reporte", tableState).then(function (response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var coleccion = response.data.datos;
                        if(coleccion && coleccion.length > 0 ){
                            vm.coleccion = [].concat(coleccion);
                            vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                            vm.paginacion.paginaActual = response.data.pagina_actual;
                            vm.paginacion.desde = response.data.desde;
                            vm.paginacion.hasta = response.data.hasta;
                            vm.paginacion.total = response.data.total;
                            tableState.pagination.numberOfPages = response.data.ultima_pagina;
                        }else{
                            vm.coleccion = [];
                        }
                    }
                }).finally(function () {
                    vm.buscando = false;
                    vm.cargando = false;
                    vm.activarBuscar = false;
                });
            }
        }

        function limpiarFiltros(){
            vm.filtroDepartamento = null;
            vm.filtroCiudad = null;
            vm.filtroDemandante = null;
            vm.filtroDemandado = null;
            vm.coleccion = [];
        }

        // Acciones

        function obtenerExcel() {
            if(vm.coleccion.length > 0) {
                genericService.obtenerArchivo("actuaciones/demandantes-demandados/excel", {
                    ciudad: vm.filtroCiudad,
                    proceso: vm.filtroProceso,
                    demandante: vm.filtroDemandante,
                    demandado: vm.filtroDemandado
                });
            }
        }

        function obtenerArchivoAnexo(row) {
            genericService.obtenerArchivo("actuaciones/" + row.id + "/archivo-anexo")
                .then(function (response) {
                    if(response.status === Constantes.Response.HTTP_NOT_FOUND){
                        messageUtil.error("No se encontro un archivo anexo.");
                    }else if(response.status === Constantes.Response.HTTP_FORBIDDEN){
                        messageUtil.error("No tiene permitido descargar este archivo.");
                    }
                });
        }

        function initAutocompleteDepartamento() {
            vm.configDepartamento = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                onItemAdd: function (value, $item) {
                    if(vm.filtroDepartamento !== value){
                        vm.filtroCiudad = null;
                        vm.opcionesCiudad = [];
                        $timeout(function () {
                            vm.limpiarOpcionesCiudad = true;
                            genericService.obtenerColeccionLigera("ciudades", {
                                ligera: true,
                                departamento_id: value
                            })
                                .then(function (response){
                                    vm.opcionesCiudad = [].concat(response.data);
                                });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.opcionesCiudad = [];
                    vm.filtroCiudad = null;
                    $timeout(function () {
                        vm.limpiarOpcionesCiudad = true;
                    }, 100);
                }
            };

            vm.opcionesDepartamento = [];
            genericService.obtenerColeccionLigera("departamentos",{
                ligera: true,
            })
                .then(function (response){
                    vm.opcionesDepartamentos = [].concat(response.data);
                });
        }

        function initAutocompleteCiudad() {
            vm.configCiudad = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                lock: false,
            };
                vm.opcionesCiudad = [];
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('procesoAreaController', procesoAreaController);

    procesoAreaController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        '$uibModal',
        'envService',
        'genericService',
        'sessionUtil',
        'messageUtil',
        'Constantes'
    ];

    function procesoAreaController(
        $scope, $routeParams, $timeout, $uibModal, envService, genericService, sessionUtil, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "procesos-areas",
            empresaId = sessionUtil.getEmpresa().id;

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        vm.activarBuscar = false;
        $scope.isReset = true;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Cargar los datos de la empresa
        genericService.cargar("empresas", empresaId)
            .then(function (response) {
                if (response.status === Constantes.Response.HTTP_OK) {
                    vm.empresa = response.data;
                }
            });

        // Control de grid
        vm.obtenerResumen = obtenerResumen;

        // Acciones
        vm.buscar = buscar;
        vm.obtenerExcel = obtenerExcel;
        vm.limpiarFiltros = limpiarFiltros;

        // inicial autocomples
        initAutocompleteArea();

        // Limpiar filtros
        limpiarFiltros();


        function buscar() {
            vm.buscando = true;
            if(vm.form.$valid && !vm.deshabilitarBuscado) {
                vm.activarBuscar = true;
                $scope.$broadcast('refreshGrid');
            }
        }

        // Grid
        function obtenerResumen(tableState, ctrl){
            tableState.search = tableState.search || {};
            tableState.search.predicateObject = tableState.search.predicateObject || {};
            tableState.search.predicateObject.empresa_id = empresaId;
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }

            if(vm.paginacion && vm.paginacion.paginaActual && tableState.pagination){
                var start = tableState.pagination.start || 0,
                    number = tableState.pagination.number || 10,
                    nextPage = (start + number) / number;
                if(vm.paginacion.paginaActual != nextPage){
                    vm.activarBuscar = true;
                }
            }

            if(vm.activarBuscar && !vm.cargando){
                vm.cargando = true;
                genericService.obtenerColeccion('procesos-judiciales', tableState).then(function (response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var coleccion = response.data.datos;
                        if(coleccion && coleccion.length > 0 ){
                            vm.coleccion = [].concat(coleccion);
                            vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                            vm.paginacion.paginaActual = response.data.pagina_actual;
                            vm.paginacion.desde = response.data.desde;
                            vm.paginacion.hasta = response.data.hasta;
                            vm.paginacion.total = response.data.total;
                            tableState.pagination.numberOfPages = response.data.ultima_pagina;
                        }else{
                            vm.coleccion = [];
                        }
                    }
                }).finally(function () {
                    vm.buscando = false;
                    vm.cargando = false;
                    vm.activarBuscar = false;
                });
            }
        }

        function limpiarFiltros(){
            vm.filtroArea = null;
            vm.coleccion = [];
        }

        function obtenerExcel() {
            if(vm.coleccion.length > 0) {
                genericService.obtenerArchivo(recurso + "/excel", {
                    area: vm.filtroArea,
                });
            }
        }

        function initAutocompleteArea() {
            vm.configArea = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
            };

            vm.opcionesArea = [];
            genericService.obtenerColeccionLigera("areas",{
                ligera: true,
                empresa_id: empresaId
            })
                .then(function (response){
                    vm.opcionesArea = [].concat(response.data);
                });
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('consultaAutoController', consultaAutoController);

    consultaAutoController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function consultaAutoController(
        $scope, $routeParams, $timeout, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            today = new Date(),
            fechaInicial = (today.getFullYear()-1) + '-' + (today.getMonth() + 1) + '-' + today.getDate(),
            fechaFinal = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();

        vm.filtroFechaInicial = fechaInicial;
        vm.filtroFechaFinal = fechaFinal;

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.activarBuscar = false;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.buscar = buscar;
        vm.obtenerExcel = obtenerExcel;
        vm.obtenerArchivoAnexo = obtenerArchivoAnexo;

        // Limpiar filtros
        limpiarFiltros();

        // Grid

        function buscar() {
            vm.buscando = true;
            if(vm.form.$valid) {
                vm.activarBuscar = true;
                $scope.$broadcast('refreshGrid');
            }
        }

        function obtenerResumen(tableState, ctrl){
            tableState.search = tableState.search || {};
            tableState.search.predicateObject = tableState.search.predicateObject || {};
            tableState.search.predicateObject.url = true;
            tableState.search.predicateObject.tipo_movimiento = true;
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }

            if(vm.paginacion && vm.paginacion.paginaActual && tableState.pagination){
                var start = tableState.pagination.start || 0,
                    number = tableState.pagination.number || 10,
                    nextPage = (start + number) / number;
                if(vm.paginacion.paginaActual != nextPage){
                    vm.activarBuscar = true;
                }else if(vm.activarBuscar){
                    tableState.pagination.start = 0;
                    tableState.pagination.number = vm.itemsPorPagina;
                }
            }

            if(vm.activarBuscar && !vm.cargando){
                vm.cargando = true;
                genericService.obtenerColeccion("actuaciones/reporte-por-empresa", tableState)
                    .then(function (response){
                        if(response.status === Constantes.Response.HTTP_OK){
                            var coleccion = response.data.datos;
                            if(coleccion && coleccion.length > 0 ){
                                vm.coleccion = [].concat(coleccion);
                                vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                                vm.paginacion.paginaActual = response.data.pagina_actual;
                                vm.paginacion.desde = response.data.desde;
                                vm.paginacion.hasta = response.data.hasta;
                                vm.paginacion.total = response.data.total;
                                tableState.pagination.numberOfPages = response.data.ultima_pagina;
                            }else{
                                vm.coleccion = [];
                            }
                        }
                    }).finally(function () {
                        vm.buscando = false;
                        vm.cargando = false;
                        vm.activarBuscar = false;
                    });
            }
        }

        function limpiarFiltros(){
            vm.filtroFechaInicial = fechaInicial;
            vm.filtroFechaFinal = fechaFinal;
            vm.coleccion = [];
            $scope.isReset = true;
        }

        /*function cargarFiltros() {
            var filtros = gridUtil.getFormattedSearchPredicate("procesoHoy");
            vm.nombre = filtros && filtros.nombre ? filtros.nombre : null;
        }*/

        // Acciones

        function obtenerExcel() {
            if(vm.coleccion.length > 0) {
                genericService.obtenerArchivo("actuaciones/autos/excel", {
                    fecha_inicial: vm.filtroFechaInicial,
                    fecha_final: vm.filtroFechaFinal,
                    con_archivo_anexo: true,
                    tipo_movimiento: true
                });
            }
        }

        function obtenerArchivoAnexo(row) {
            genericService.obtenerArchivo("actuaciones/" + row.id + "/archivo-anexo")
                .then(function (response) {
                    if(response.status === Constantes.Response.HTTP_NOT_FOUND){
                        messageUtil.error("No se encontro un archivo anexo.");
                    }else if(response.status === Constantes.Response.HTTP_FORBIDDEN){
                        messageUtil.error("No tiene permitido descargar este archivo.");
                    }
                });
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('consultaRemateController', consultaRemateController);

    consultaRemateController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function consultaRemateController(
        $scope, $routeParams, $timeout, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            today = new Date(),
            fechaInicial = (today.getFullYear()-1) + '-' + (today.getMonth() + 1) + '-' + today.getDate(),
            fechaFinal = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
        vm.filtroFechaInicial = fechaInicial;
        vm.filtroFechaFinal = fechaFinal;

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 5;
        $scope.isReset = true;
        vm.activarBuscar = false;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.buscar = buscar;
        vm.obtenerExcel = obtenerExcel;
        vm.obtenerArchivoAnexo = obtenerArchivoAnexo;

        // inicial autocomples
        initAutocompleteDepartamento();
        initAutocompleteCiudad();
        initAutocompleteJuzgado();
        initAutocompleteDespacho();

        // Limpiar filtros
        limpiarFiltros();

        // Grid

        function buscar() {
            vm.buscando = true;
            if(vm.form.$valid) {
                vm.activarBuscar = true;
                $scope.$broadcast('refreshGrid');
            }
        }

        function obtenerResumen(tableState, ctrl){
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }

            if(vm.paginacion && vm.paginacion.paginaActual && tableState.pagination){
                var start = tableState.pagination.start || 0,
                    number = tableState.pagination.number || 10,
                    nextPage = (start + number) / number;
                if(vm.paginacion.paginaActual != nextPage){
                    vm.activarBuscar = true;
                }else if(vm.activarBuscar){
                    tableState.pagination.start = 0;
                    tableState.pagination.number = vm.itemsPorPagina;
                }
            }

            if(vm.activarBuscar && !vm.cargando){
                vm.cargando = true;
                genericService.obtenerColeccion("remates", tableState).then(function (response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var coleccion = response.data.datos;
                        if(coleccion && coleccion.length > 0 ){
                            vm.coleccion = [].concat(coleccion);
                            vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                            vm.paginacion.paginaActual = response.data.pagina_actual;
                            vm.paginacion.desde = response.data.desde;
                            vm.paginacion.hasta = response.data.hasta;
                            vm.paginacion.total = response.data.total;
                            tableState.pagination.numberOfPages = response.data.ultima_pagina;
                        }else{
                            vm.coleccion = [];
                        }
                    }
                }).finally(function () {
                    vm.buscando = false;
                    vm.cargando = false;
                    vm.activarBuscar = false;
                });
            }
        }

        function limpiarFiltros(){
            vm.filtroFechaInicial = fechaInicial;
            vm.filtroFechaFinal = fechaFinal;
            vm.filtroDepartamento = null;
            vm.filtroCiudad = null;
            vm.filtroJuzgado = null;
            vm.filtroDespacho = null;
            vm.coleccion = [];
        }

        // Acciones

        function obtenerExcel() {
            if(vm.coleccion.length > 0) {
                genericService.obtenerArchivo("remates/excel", {
                    fecha_inicial: vm.filtroFechaInicial,
                    fecha_final: vm.filtroFechaFinal,
                    ciudad: vm.filtroCiudad,
                    juzgado: vm.filtroJuzgado,
                    despacho: vm.filtroDespacho
                });
            }
        }

        function obtenerArchivoAnexo(row) {
            genericService.obtenerArchivo("actuaciones/" + row.id + "/archivo-anexo")
                .then(function (response) {
                    if(response.status === Constantes.Response.HTTP_NOT_FOUND){
                        messageUtil.error("No se encontro un archivo anexo.");
                    }else if(response.status === Constantes.Response.HTTP_FORBIDDEN){
                        messageUtil.error("No tiene permitido descargar este archivo.");
                    }
                });
        }

        // Instaciar autocompletes

        function initAutocompleteDepartamento() {
            vm.configDepartamento = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                onItemAdd: function (value, $item) {
                    if(vm.filtroDepartamento !== value){
                        vm.filtroCiudad = null;
                        vm.opcionesCiudad = [];
                        $timeout(function () {
                            vm.limpiarOpcionesCiudad = true;
                            genericService.obtenerColeccionLigera("ciudades", {
                                ligera: true,
                                departamento_id: value
                            })
                                .then(function (response){
                                    vm.opcionesCiudad = [].concat(response.data);
                                });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.opcionesCiudad = [];
                    vm.filtroCiudad = null;
                    $timeout(function () {
                        vm.limpiarOpcionesCiudad = true;
                    }, 100);
                }
            };

            vm.opcionesDepartamento = [];
            genericService.obtenerColeccionLigera("departamentos",{
                ligera: true,
            })
                .then(function (response){
                    vm.opcionesDepartamentos = [].concat(response.data);
                });
        }

        function initAutocompleteCiudad() {
            vm.configCiudad = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                lock: false,
                onItemAdd: function (value, $item) {
                    if(vm.filtroCiudad !== value){
                        vm.opcionesJuzgado = [];
                        vm.opcionesDespacho = [];
                        vm.opcionesProceso = [];
                        vm.filtroJuzgado = null;
                        vm.filtroDespacho = null;
                        vm.filtroProceso = null;
                        $timeout(function () {
                            vm.limpiarOpcionesJuzgado = true;
                            vm.limpiarOpcionesDespacho = true;
                            vm.limpiarOpcionesProceso = true;
                            vm.filtroJuzgado = null;
                            vm.filtroDespacho = null;
                            vm.filtroProceso = null;
                            genericService.obtenerColeccionLigera("juzgados", {
                                ligera: true,
                                ciudad_id: value
                            })
                                .then(function (response){
                                    vm.opcionesJuzgado = [].concat(response.data);
                                });
                            genericService.obtenerColeccionLigera("procesos-judiciales", {
                                ligera: true,
                                ciudad_id: value,
                                limite: 100
                            })
                                .then(function (response){
                                    vm.opcionesProceso = [].concat(response.data);
                                });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.opcionesJuzgado = [];
                    vm.opcionesDespacho = [];
                    vm.opcionesProceso = [];
                    vm.filtroJuzgado = null;
                    vm.filtroDespacho = null;
                    vm.filtroProceso = null;
                    $timeout(function () {
                        vm.limpiarOpcionesJuzgado = true;
                        vm.limpiarOpcionesDespacho = true;
                        vm.limpiarOpcionesProceso = true;
                    }, 100);
                }
            };
            vm.opcionesCiudad = [];
        }

        function initAutocompleteJuzgado() {
            vm.configJuzgado = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                onItemAdd: function (value, $item) {
                    if(vm.filtroJuzgado !== value){
                        vm.opcionesDespacho = [];
                        vm.filtroDespacho = null;
                        $timeout(function () {
                            vm.limpiarOpcionesDespacho = true;
                            genericService.obtenerColeccionLigera("despachos", {
                                ligera: true,
                                juzgado_id: value
                            })
                                .then(function (response){
                                    vm.opcionesDespacho = [].concat(response.data);
                                });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.opcionesDespacho = [];
                    vm.filtroDespacho = null;
                    $timeout(function () {
                        vm.limpiarOpcionesDespacho = true;
                    }, 100);
                }
            };

            vm.opcionesJuzgado = [];
        }

        function initAutocompleteDespacho() {
            vm.configDespacho = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
            };

            vm.opcionesDespacho = [];
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('consultaRemanenteController', consultaRemanenteController);

    consultaRemanenteController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function consultaRemanenteController(
        $scope, $routeParams, $timeout, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            today = new Date(),
            fechaInicial = (today.getFullYear()-1) + '-' + (today.getMonth() + 1) + '-' + today.getDate(),
            fechaFinal = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();

        vm.consultaRemanentes = ($routeParams.consulta && +$routeParams.consulta === 1);
        vm.filtroFechaInicial = fechaInicial;
        vm.filtroFechaFinal = fechaFinal;

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 5;
        $scope.isReset = true;
        vm.activarBuscar = false;
        vm.mostrar = false;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid

        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones

        vm.buscar = buscar;
        vm.obtenerExcel = obtenerExcel;
        vm.obtenerArchivoAnexo = obtenerArchivoAnexo;
        vm.click = function () {
            vm.mostrar = true;
        };

        // Instanciar autocomples

        initAutocompleteDepartamento();
        initAutocompleteCiudad();
        initAutocompleteJuzgado();
        initAutocompleteDespacho();
        initAutocompleteTipoNotificacion();

        // Limpiar filtros
        limpiarFiltros();

        function buscar() {
            vm.buscando = true;
            if(vm.form.$valid) {
                vm.activarBuscar = true;
                $scope.$broadcast('refreshGrid');
            }
        }

        function obtenerResumen(tableState, ctrl){
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }

            if(vm.paginacion && vm.paginacion.paginaActual && tableState.pagination){
                var start = tableState.pagination.start || 0,
                    number = tableState.pagination.number || 10,
                    nextPage = (start + number) / number;
                if(vm.paginacion.paginaActual != nextPage){
                    vm.activarBuscar = true;
                }else if(vm.activarBuscar){
                    tableState.pagination.start = 0;
                    tableState.pagination.number = vm.itemsPorPagina;
                }
            }

            if(vm.activarBuscar && !vm.cargando){
                vm.cargando = true;
                genericService.obtenerColeccion("remanentes", tableState).then(function (response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var coleccion = response.data.datos;
                        if(coleccion && coleccion.length > 0 ){
                            vm.coleccion = [].concat(coleccion);
                            vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                            vm.paginacion.paginaActual = response.data.pagina_actual;
                            vm.paginacion.desde = response.data.desde;
                            vm.paginacion.hasta = response.data.hasta;
                            vm.paginacion.total = response.data.total;
                            tableState.pagination.numberOfPages = response.data.ultima_pagina;
                        }else{
                            vm.coleccion = [];
                        }
                    }
                }).finally(function () {
                    vm.buscando = false;
                    vm.cargando = false;
                    vm.activarBuscar = false;
                });
            }
        }

        function limpiarFiltros(){
            vm.filtroDepartamento = null;
            vm.filtroCiudad = null;
            vm.filtroJuzgado = null;
            vm.filtroDespacho = null;
            vm.filtroTipoNotificacion = null;
            vm.filtroRadicado = null;
            vm.filtroTipoProceso = null;
            vm.filtroDetalle = null;
            vm.filtroDemandante = null;
            vm.filtroDemandado = null;
            vm.filtroFechaInicial = fechaInicial;
            vm.filtroFechaFinal = fechaFinal;
            vm.coleccion = [];
        }

        function obtenerExcel() {
            if(vm.coleccion.length > 0) {
                genericService.obtenerArchivo("remanentes/excel", {
                    ciudad: vm.filtroCiudad,
                    juzgado: vm.filtroJuzgado,
                    despacho: vm.filtroDespacho,
                    tipo_notificacion: vm.filtroTipoNotificacion,
                    radicado: vm.filtroRadicado,
                    descripcion_clase_proceso: vm.filtroTipoProceso,
                    detalle: vm.filtroDetalle,
                    demandante: vm.filtroDemandante,
                    demandado: vm.filtroDemandado,
                    fecha_inicial: vm.filtroFechaInicial,
                    fecha_final: vm.filtroFechaFinal
                });
            }
        }

        function obtenerArchivoAnexo(row) {
            genericService.obtenerArchivo("actuaciones/" + row.id + "/archivo-anexo")
                .then(function (response) {
                    if(response.status === Constantes.Response.HTTP_NOT_FOUND){
                        messageUtil.error("No se encontro un archivo anexo.");
                    }else if(response.status === Constantes.Response.HTTP_FORBIDDEN){
                        messageUtil.error("No tiene permitido descargar este archivo.");
                    }
                });
        }

        // Instanciar autocompletes

        function initAutocompleteDepartamento() {
            vm.configDepartamento = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                onItemAdd: function (value, $item) {
                    if(vm.filtroDepartamento !== value){
                        vm.filtroCiudad = null;
                        vm.opcionesCiudad = [];
                        $timeout(function () {
                            vm.limpiarOpcionesCiudad = true;
                            genericService.obtenerColeccionLigera("ciudades", {
                                ligera: true,
                                departamento_id: value
                            })
                                .then(function (response){
                                    vm.opcionesCiudad = [].concat(response.data);
                                });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.opcionesCiudad = [];
                    vm.filtroCiudad = null;
                    $timeout(function () {
                        vm.limpiarOpcionesCiudad = true;
                    }, 100);
                }
            };

            vm.opcionesDepartamento = [];
            genericService.obtenerColeccionLigera("departamentos",{
                ligera: true,
            })
                .then(function (response){
                    vm.opcionesDepartamentos = [].concat(response.data);
                });
        }

        function initAutocompleteCiudad() {
            vm.configCiudad = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                lock: false,
                onItemAdd: function (value, $item) {
                    if(vm.filtroCiudad !== value){
                        vm.opcionesJuzgado = [];
                        vm.opcionesDespacho = [];
                        vm.opcionesProceso = [];
                        vm.filtroJuzgado = null;
                        vm.filtroDespacho = null;
                        vm.filtroProceso = null;
                        $timeout(function () {
                            vm.limpiarOpcionesJuzgado = true;
                            vm.limpiarOpcionesDespacho = true;
                            vm.limpiarOpcionesProceso = true;
                            vm.filtroJuzgado = null;
                            vm.filtroDespacho = null;
                            vm.filtroProceso = null;
                            genericService.obtenerColeccionLigera("juzgados", {
                                ligera: true,
                                ciudad_id: value
                            })
                                .then(function (response){
                                    vm.opcionesJuzgado = [].concat(response.data);
                                });
                            genericService.obtenerColeccionLigera("procesos-judiciales", {
                                ligera: true,
                                ciudad_id: value,
                                limite: 100
                            })
                                .then(function (response){
                                    vm.opcionesProceso = [].concat(response.data);
                                });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.opcionesJuzgado = [];
                    vm.opcionesDespacho = [];
                    vm.opcionesProceso = [];
                    vm.filtroJuzgado = null;
                    vm.filtroDespacho = null;
                    vm.filtroProceso = null;
                    $timeout(function () {
                        vm.limpiarOpcionesJuzgado = true;
                        vm.limpiarOpcionesDespacho = true;
                        vm.limpiarOpcionesProceso = true;
                    }, 100);
                }
            };
            vm.opcionesCiudad = [];
        }

        function initAutocompleteJuzgado() {
            vm.configJuzgado = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                onItemAdd: function (value, $item) {
                    if(vm.filtroJuzgado !== value){
                        vm.opcionesDespacho = [];
                        vm.filtroDespacho = null;
                        $timeout(function () {
                            vm.limpiarOpcionesDespacho = true;
                            genericService.obtenerColeccionLigera("despachos", {
                                ligera: true,
                                juzgado_id: value
                            })
                                .then(function (response){
                                    vm.opcionesDespacho = [].concat(response.data);
                                });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.opcionesDespacho = [];
                    vm.filtroDespacho = null;
                    $timeout(function () {
                        vm.limpiarOpcionesDespacho = true;
                    }, 100);
                }
            };

            vm.opcionesJuzgado = [];
        }

        function initAutocompleteDespacho() {
            vm.configDespacho = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,

            };

            vm.opcionesDespacho = [];
        }

        function initAutocompleteTipoNotificacion() {
            vm.configTipoNotificacion = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true
            };
            vm.opcionesTipoNotificacion = [];
            genericService.obtenerColeccionLigera("tipos-notificaciones",{
                ligera: true,
                // empresa_id: empresaId
            })
                .then(function (response){
                    vm.opcionesTipoNotificacion = [].concat(response.data);
                });
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('consultaPersonalizadoDigitalController', consultaPersonalizadoDigitalController);

    consultaPersonalizadoDigitalController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function consultaPersonalizadoDigitalController(
        $scope, $routeParams, $timeout, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            today = new Date(),
            fechaInicial = (today.getFullYear()-1) + '-' + (today.getMonth() + 1) + '-' + today.getDate(),
            fechaFinal = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();

        vm.filtroFechaInicial = fechaInicial;
        vm.filtroFechaFinal = fechaFinal;

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 5;
        $scope.isReset = true;
        vm.activarBuscar = false;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid

        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones

        vm.buscar = buscar;
        vm.obtenerExcel = obtenerExcel;
        vm.obtenerArchivoAnexo = obtenerArchivoAnexo;

        // Instanciar autocomples

        initAutocompleteDepartamento();
        initAutocompleteCiudad();
        initAutocompleteJuzgado();
        initAutocompleteDespacho();
        initAutocompleteTipoNotificacion();

        // Limpiar filtros
        limpiarFiltros();

        function buscar() {
            vm.buscando = true;
            if(vm.form.$valid) {
                vm.activarBuscar = true;
                $scope.$broadcast('refreshGrid');
            }
        }

        function obtenerResumen(tableState, ctrl){
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }

            if(vm.paginacion && vm.paginacion.paginaActual && tableState.pagination){
                var start = tableState.pagination.start || 0,
                    number = tableState.pagination.number || 10,
                    nextPage = (start + number) / number;
                if(vm.paginacion.paginaActual != nextPage){
                    vm.activarBuscar = true;
                }else if(vm.activarBuscar){
                    tableState.pagination.start = 0;
                    tableState.pagination.number = vm.itemsPorPagina;
                }
            }

            if(vm.activarBuscar && !vm.cargando){
                vm.cargando = true;
                genericService.obtenerColeccion("remanentes", tableState).then(function (response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var coleccion = response.data.datos;
                        if(coleccion && coleccion.length > 0 ){
                            vm.coleccion = [].concat(coleccion);
                            vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                            vm.paginacion.paginaActual = response.data.pagina_actual;
                            vm.paginacion.desde = response.data.desde;
                            vm.paginacion.hasta = response.data.hasta;
                            vm.paginacion.total = response.data.total;
                            tableState.pagination.numberOfPages = response.data.ultima_pagina;
                        }else{
                            vm.coleccion = [];
                        }
                    }
                }).finally(function () {
                    vm.buscando = false;
                    vm.cargando = false;
                    vm.activarBuscar = false;
                });
            }
        }

        function limpiarFiltros(){
            vm.filtroDepartamento = null;
            vm.filtroCiudad = null;
            vm.filtroJuzgado = null;
            vm.filtroDespacho = null;
            vm.filtroTipoNotificacion = null;
            vm.filtroRadicado = null;
            vm.filtroTipoProceso = null;
            vm.filtroDetalle = null;
            vm.filtroFechaInicial = fechaInicial;
            vm.filtroFechaFinal = fechaFinal;
            vm.coleccion = [];
        }

        function obtenerExcel() {
            if(vm.coleccion.length > 0) {
                genericService.obtenerArchivo("actuaciones/personalizados-digitales/excel", {
                    ciudad: vm.filtroCiudad,
                    juzgado: vm.filtroJuzgado,
                    despacho: vm.filtroDespacho,
                    tipo_notificacion: vm.filtroTipoNotificacion,
                    radicado: vm.filtroRadicado,
                    descripcion_clase_proceso: vm.filtroTipoProceso,
                    detalle: vm.filtroDetalle,
                    fecha_inicial: vm.filtroFechaInicial,
                    fecha_final: vm.filtroFechaFinal
                });
            }
        }

        function obtenerArchivoAnexo(row) {
            genericService.obtenerArchivo("actuaciones/" + row.id + "/archivo-anexo")
                .then(function (response) {
                    if(response.status === Constantes.Response.HTTP_NOT_FOUND){
                        messageUtil.error("No se encontro un archivo anexo.");
                    }else if(response.status === Constantes.Response.HTTP_FORBIDDEN){
                        messageUtil.error("No tiene permitido descargar este archivo.");
                    }
                });
        }

        // Instanciar autocompletes

        function initAutocompleteDepartamento() {
            vm.configDepartamento = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                onItemAdd: function (value, $item) {
                    if(vm.filtroDepartamento !== value){
                        vm.filtroCiudad = null;
                        vm.opcionesCiudad = [];
                        $timeout(function () {
                            vm.limpiarOpcionesCiudad = true;
                            genericService.obtenerColeccionLigera("ciudades", {
                                ligera: true,
                                departamento_id: value
                            })
                                .then(function (response){
                                    vm.opcionesCiudad = [].concat(response.data);
                                });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.opcionesCiudad = [];
                    vm.filtroCiudad = null;
                    $timeout(function () {
                        vm.limpiarOpcionesCiudad = true;
                    }, 100);
                }
            };

            vm.opcionesDepartamento = [];
            genericService.obtenerColeccionLigera("departamentos",{
                ligera: true,
            })
                .then(function (response){
                    vm.opcionesDepartamentos = [].concat(response.data);
                });
        }

        function initAutocompleteCiudad() {
            vm.configCiudad = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                lock: false,
                onItemAdd: function (value, $item) {
                    if(vm.filtroCiudad !== value){
                        vm.opcionesJuzgado = [];
                        vm.opcionesDespacho = [];
                        vm.opcionesProceso = [];
                        vm.filtroJuzgado = null;
                        vm.filtroDespacho = null;
                        vm.filtroProceso = null;
                        $timeout(function () {
                            vm.limpiarOpcionesJuzgado = true;
                            vm.limpiarOpcionesDespacho = true;
                            vm.limpiarOpcionesProceso = true;
                            vm.filtroJuzgado = null;
                            vm.filtroDespacho = null;
                            vm.filtroProceso = null;
                            genericService.obtenerColeccionLigera("juzgados", {
                                ligera: true,
                                ciudad_id: value
                            })
                                .then(function (response){
                                    vm.opcionesJuzgado = [].concat(response.data);
                                });
                            genericService.obtenerColeccionLigera("procesos-judiciales", {
                                ligera: true,
                                ciudad_id: value,
                                limite: 100
                            })
                                .then(function (response){
                                    vm.opcionesProceso = [].concat(response.data);
                                });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.opcionesJuzgado = [];
                    vm.opcionesDespacho = [];
                    vm.opcionesProceso = [];
                    vm.filtroJuzgado = null;
                    vm.filtroDespacho = null;
                    vm.filtroProceso = null;
                    $timeout(function () {
                        vm.limpiarOpcionesJuzgado = true;
                        vm.limpiarOpcionesDespacho = true;
                        vm.limpiarOpcionesProceso = true;
                    }, 100);
                }
            };
            vm.opcionesCiudad = [];
        }

        function initAutocompleteJuzgado() {
            vm.configJuzgado = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                onItemAdd: function (value, $item) {
                    if(vm.filtroJuzgado !== value){
                        vm.opcionesDespacho = [];
                        vm.filtroDespacho = null;
                        $timeout(function () {
                            vm.limpiarOpcionesDespacho = true;
                            genericService.obtenerColeccionLigera("despachos", {
                                ligera: true,
                                juzgado_id: value
                            })
                                .then(function (response){
                                    vm.opcionesDespacho = [].concat(response.data);
                                });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.opcionesDespacho = [];
                    vm.filtroDespacho = null;
                    $timeout(function () {
                        vm.limpiarOpcionesDespacho = true;
                    }, 100);
                }
            };

            vm.opcionesJuzgado = [];
        }

        function initAutocompleteDespacho() {
            vm.configDespacho = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,

            };

            vm.opcionesDespacho = [];
        }

        function initAutocompleteTipoNotificacion() {
            vm.configTipoNotificacion = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true
            };
            vm.opcionesTipoNotificacion = [];
            genericService.obtenerColeccionLigera("tipos-notificaciones",{
                ligera: true,
                // empresa_id: empresaId
            })
                .then(function (response){
                    vm.opcionesTipoNotificacion = [].concat(response.data);
                });
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('parametroConstanteController', parametroConstanteController);

    parametroConstanteController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        '$uibModal',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function parametroConstanteController(
        $scope, $routeParams, $timeout, $uibModal, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "parametros-constantes";

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;

        // Acciones
        vm.crear = crear;

        // Modulos externos
        vm.confirmarEliminar = confirmarEliminar;

        // Grid
        function obtenerResumen(tableState, ctrl){
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }
            if(pipePromise) $timeout.cancel(pipePromise);
            pipePromise = $timeout(function() {
                if(!vm.cargando){
                    vm.cargando = true;
                    genericService.obtenerColeccion(recurso, tableState)
                        .then(function (response){
                            if(response.status === Constantes.Response.HTTP_OK){
                                var coleccion = response.data.datos;
                                if(coleccion && coleccion.length > 0 ){
                                    vm.coleccion = [].concat(coleccion);
                                    vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                                    vm.paginacion.paginaActual = response.data.pagina_actual;
                                    vm.paginacion.desde = response.data.desde;
                                    vm.paginacion.hasta = response.data.hasta;
                                    vm.paginacion.total = response.data.total;
                                    tableState.pagination.numberOfPages = response.data.ultima_pagina;
                                }else{
                                    vm.coleccion = [];
                                }
                            }
                        }).finally(function () {
                            vm.cargando = false;
                        });
                }
            }, 500);
        }

        // Acciones

        function eliminar(row){
            genericService.eliminar(recurso, row.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        $scope.$broadcast('refreshGrid');
                        messageUtil.success(response.data.mensajes[0]);
                    }else if(
                        response.status === Constantes.Response.HTTP_CONFLICT ||
                        response.status === Constantes.Response.HTTP_BAD_REQUEST
                    ){
                        messageUtil.error(response.data.mensajes[0]);
                    }
                });
        }

        // Modulos externos

        function crear(parametro_constante) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + recurso + '/create',
                controller: 'parametroConstanteEditController as vm',
                size: 'md',
                resolve: {
                    parametro_constante: function () {
                        return parametro_constante;
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(parametro_constante && parametro_constante.id && response && response.id){
                    parametro_constante.codigo_parametro = response.codigo_parametro;
                    parametro_constante.descripcion_parametro = response.descripcion_parametro;
                    parametro_constante.valor_parametro = response.valor_parametro;
                    parametro_constante.estado_registro = response.estado_registro;
                    parametro_constante.usuario_modificacion_id = response.usuario_modificacion_id;
                    parametro_constante.usuario_modificacion_nombre = response.usuario_modificacion_nombre;
                    parametro_constante.fecha_creacion = response.fecha_creacion;
                    parametro_constante.fecha_modificacion = response.fecha_modificacion;

                }else if(response && response.id){
                    $scope.$broadcast('refreshGrid');
                }
            });
        }

        function confirmarEliminar(row) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + 'confirmar-template',
                controller: 'confirmarController as vm',
                size: 'lg',
                resolve: {
                    parametros: function () {
                        return {
                            mensaje: "¿Estás seguro de que quieres eliminar el parámetro?"
                        };
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(response.ok){
                    eliminar(row);
                }
            });
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('parametroConstanteEditController', parametroConstanteEditController);

    parametroConstanteEditController.$inject = [
        '$uibModalInstance',
        '$timeout',
        'genericService',
        'messageUtil',
        'Constantes',
        'parametro_constante'
    ];

    function parametroConstanteEditController(
        $uibModalInstance, $timeout, genericService, messageUtil, Constantes, parametro_constante
    ){
        var vm = this,
            recurso = "parametros-constantes";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        if(parametro_constante && parametro_constante.id){
            cargar();
        }else{
            vm.estado = true;
        }

        function cargar() {
            genericService.cargar(recurso, parametro_constante.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var parametro_constante = response.data;

                        vm.codigo = parametro_constante.codigo_parametro;
                        vm.descripcion = parametro_constante.descripcion_parametro;
                        vm.valor = parametro_constante.valor_parametro;
                        vm.estado = parametro_constante.estado_registro;
                        vm.fechaCreacion = parametro_constante.fecha_creacion;
                        vm.fechaModificacion = parametro_constante.fecha_modificacion;
                        vm.usuarioCreacionNombre = parametro_constante.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = parametro_constante.usuario_modificacion_nombre;
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;

                var datos = {
                    id: parametro_constante ? +parametro_constante.id : null,
                    codigo_parametro: vm.codigo,
                    descripcion_parametro: vm.descripcion,
                    valor_parametro: vm.valor,
                    estado_registro: vm.estado

            };

                var promesa = null;
                if(!(parametro_constante && parametro_constante.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        messageUtil.success(response.data.mensajes[0]);
                        $uibModalInstance.close(response.data.datos);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }


                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('audienciaPendienteController', audienciaPendienteController);

    audienciaPendienteController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function audienciaPendienteController(
        $scope, $routeParams, $timeout, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise;

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 5;
        $scope.isReset = true;
        vm.activarBuscar = true;
        vm.filtroVenceHoy = false;
        vm.filtroPorVencer = false;
        vm.filtroPendientes = false;
        vm.filtroMasDeDiezDias = false;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.buscar = buscar;
        vm.obtenerExcel = obtenerExcel;
        vm.obtenerArchivoAnexo = obtenerArchivoAnexo;

        // Limpiar filtros
        limpiarFiltros();

        // Grid

        function buscar() {
            vm.buscando = true;
            if(vm.form.$valid) {
                vm.activarBuscar = true;
                $scope.$broadcast('refreshGrid');
            }
        }

        function obtenerResumen(tableState, ctrl){
            tableState.search = tableState.search || {};
            tableState.search.predicateObject = tableState.search.predicateObject || {};
            tableState.search.predicateObject.vence_hoy = vm.filtroVenceHoy;
            tableState.search.predicateObject.por_vencer = vm.filtroPorVencer;
            tableState.search.predicateObject.pendientes = vm.filtroPendientes;
            tableState.search.predicateObject.mas_de_diz_dias = vm.filtroMasDeDiezDias;

            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }

            if(vm.paginacion && vm.paginacion.paginaActual && tableState.pagination){
                var start = tableState.pagination.start || 0,
                    number = tableState.pagination.number || 10,
                    nextPage = (start + number) / number;
                if(vm.paginacion.paginaActual != nextPage){
                    vm.activarBuscar = true;
                }else if(vm.activarBuscar){
                    tableState.pagination.start = 0;
                    tableState.pagination.number = vm.itemsPorPagina;
                }
            }

            if(vm.activarBuscar && !vm.cargando){
                vm.cargando = true;
                genericService.obtenerColeccion("actuaciones/audiencia-pendiente", tableState).then(function (response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var coleccion = response.data.datos;
                        if(coleccion && coleccion.length > 0){
                            var fechaActual = moment(),
                                diferencia = null,
                                fechaVencimientoTermino = null;
                            angular.forEach(coleccion, function (row) {
                                fechaVencimientoTermino = moment(row.fecha_vencimiento_termino);
                                diferencia = fechaVencimientoTermino.diff(fechaActual, 'days');
                                if(diferencia === 0){
                                    row.style = {'background-color': '#EEF2E6'};
                                }else if(diferencia >= 1 && diferencia <= 3){
                                    row.style = {'background-color': '#FFFF9D'};
                                }else if(diferencia >= 4 && diferencia <= 10){
                                    row.style = {'background-color': '#9BFF93'};
                                }else if(diferencia > 10){
                                    row.style = {'background-color': '#92D0FE'};
                                }
                            });
                            vm.coleccion = [].concat(coleccion);
                            vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                            vm.paginacion.paginaActual = response.data.pagina_actual;
                            vm.paginacion.desde = response.data.desde;
                            vm.paginacion.hasta = response.data.hasta;
                            vm.paginacion.total = response.data.total;
                            tableState.pagination.numberOfPages = response.data.ultima_pagina;
                        }else{
                            vm.coleccion = [];
                        }
                    }
                }).finally(function () {
                    vm.buscando = false;
                    vm.cargando = false;
                    vm.activarBuscar = false;
                });
            }
        }

        function limpiarFiltros(){
            vm.filtroProceso = null;
            vm.filtroActuacion = null;
            vm.filtroDemandante = null;
            vm.filtroDemandado = null;
            vm.activarBuscar = true;
            $scope.isReset = true;
        }

        // Acciones

        function obtenerExcel() {
            if(vm.coleccion.length > 0) {
                genericService.obtenerArchivo("actuaciones/audiencias-pendientes/excel", {
                    proceso: vm.filtroProceso,
                    actuacion: vm.filtroActuacion,
                    demandante: vm.filtroDemandante,
                    demandado: vm.filtroDemandado,
                    vence_hoy: vm.filtroVenceHoy,
                    por_vencer: vm.filtroPorVencer,
                    pendientes: vm.filtroPendientes,
                    mas_de_diz_dias: vm.filtroMasDeDiezDias
                });
            }
        }

        function obtenerArchivoAnexo(row) {
            genericService.obtenerArchivo("actuaciones/" + row.id + "/archivo-anexo")
                .then(function (response) {
                    if(response.status === Constantes.Response.HTTP_NOT_FOUND){
                        messageUtil.error("No se encontro un archivo anexo.");
                    }else if(response.status === Constantes.Response.HTTP_FORBIDDEN){
                        messageUtil.error("No tiene permitido descargar este archivo.");
                    }
                });
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('audienciaCumplidaController', audienciaCumplidaController);

    audienciaCumplidaController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function audienciaCumplidaController(
        $scope, $routeParams, $timeout, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise;

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 5;
        $scope.isReset = true;
        vm.activarBuscar = true;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.buscar = buscar;
        vm.obtenerExcel = obtenerExcel;
        vm.obtenerArchivoAnexo = obtenerArchivoAnexo;

        // Limpiar filtros
        limpiarFiltros();

        // Grid

        function buscar() {
            vm.buscando = true;
            if(vm.form.$valid) {
                vm.activarBuscar = true;
                $scope.$broadcast('refreshGrid');
            }
        }

        function obtenerResumen(tableState, ctrl){
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }

            if(vm.paginacion && vm.paginacion.paginaActual && tableState.pagination){
                var start = tableState.pagination.start || 0,
                    number = tableState.pagination.number || 10,
                    nextPage = (start + number) / number;
                if(vm.paginacion.paginaActual != nextPage){
                    vm.activarBuscar = true;
                }else if(vm.activarBuscar){
                    tableState.pagination.start = 0;
                    tableState.pagination.number = vm.itemsPorPagina;
                }
            }

            if(vm.activarBuscar && !vm.cargando){
                vm.cargando = true;
                genericService.obtenerColeccion("actuaciones/audiencia-cumplida", tableState).then(function (response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var coleccion = response.data.datos;
                        if(coleccion && coleccion.length > 0 ){
                            vm.coleccion = [].concat(coleccion);
                            vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                            vm.paginacion.paginaActual = response.data.pagina_actual;
                            vm.paginacion.desde = response.data.desde;
                            vm.paginacion.hasta = response.data.hasta;
                            vm.paginacion.total = response.data.total;
                            tableState.pagination.numberOfPages = response.data.ultima_pagina;
                        }else{
                            vm.coleccion = [];
                        }
                    }
                }).finally(function () {
                    vm.buscando = false;
                    vm.cargando = false;
                    vm.activarBuscar = false;
                });
            }
        }


        function limpiarFiltros(){
            vm.filtroProceso = null;
            vm.filtroActuacion = null;
            vm.filtroDemandante = null;
            vm.filtroDemandado = null;
            vm.activarBuscar = true;
            $scope.isReset = true;
        }

        // Acciones

        function obtenerExcel() {
            if(vm.coleccion.length > 0) {
                genericService.obtenerArchivo("actuaciones/audiencias-cumplidas/excel", {
                    proceso: vm.filtroProceso,
                    actuacion: vm.filtroActuacion,
                    demandante: vm.filtroDemandante,
                    demandado: vm.filtroDemandado
                });
            }
        }

        function obtenerArchivoAnexo(row) {
            genericService.obtenerArchivo("actuaciones/" + row.id + "/archivo-anexo")
                .then(function (response) {
                    if(response.status === Constantes.Response.HTTP_NOT_FOUND){
                        messageUtil.error("No se encontro un archivo anexo.");
                    }else if(response.status === Constantes.Response.HTTP_FORBIDDEN){
                        messageUtil.error("No tiene permitido descargar este archivo.");
                    }
                });
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('terminoActuacionController', terminoActuacionController);

    terminoActuacionController.$inject = [
        '$scope',
        '$timeout',
        '$uibModal',
        'sessionUtil',
        'envService',
        '$routeParams',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function terminoActuacionController(
        $scope, $timeout, $uibModal, sessionUtil, envService, $routeParams, genericService, messageUtil, Constantes
    ){
        var vm = this,
            recurso = "actuaciones";
        vm.pasa =  !!($routeParams.pasa && +$routeParams.pasa === 1);

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 5;
        $scope.isReset = true;
        vm.activarBuscar = true;
        vm.filtroVenceHoy = false;
        vm.filtroPorVencer = false;
        vm.filtroPendientes = false;
        vm.filtroMasDeDiezDias = false;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.buscar = buscar;
        vm.obtenerExcel = obtenerExcel;
        vm.obtenerArchivoAnexo = obtenerArchivoAnexo;
        vm.pasarActuaciones = pasarActuaciones;

        // Limpiar filtros
        limpiarFiltros();

        // Grid

        function buscar() {
            vm.buscando = true;
            if(vm.form.$valid) {
                vm.activarBuscar = true;
                $scope.$broadcast('refreshGrid');
            }
        }

        function obtenerResumen(tableState, ctrl){
            tableState.search = tableState.search || {};
            tableState.search.predicateObject = tableState.search.predicateObject || {};
            tableState.search.predicateObject.cumplimiento = vm.pasa;
            tableState.search.predicateObject.vence_hoy = vm.filtroVenceHoy;
            tableState.search.predicateObject.por_vencer = vm.filtroPorVencer;
            tableState.search.predicateObject.pendientes = vm.filtroPendientes;
            tableState.search.predicateObject.mas_de_diz_dias = vm.filtroMasDeDiezDias;

            if(vm.paginacion && vm.paginacion.paginaActual && tableState.pagination){
                var start = tableState.pagination.start || 0,
                    number = tableState.pagination.number || 10,
                    nextPage = (start + number) / number;
                if(vm.paginacion.paginaActual != nextPage){
                    vm.activarBuscar = true;
                }else if(vm.activarBuscar){
                    tableState.pagination.start = 0;
                    tableState.pagination.number = vm.itemsPorPagina;
                }
            }

            if(vm.activarBuscar && !vm.cargando){
                vm.cargando = true;
                genericService.obtenerColeccion(recurso + "/actuaciones-pendientes", tableState)
                .then(function (response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var coleccion = response.data.datos;
                        if(coleccion && coleccion.length > 0 ){
                            var fechaActual = moment(),
                                diferencia = null,
                                fechaVencimientoTermino = null;
                            angular.forEach(coleccion, function (row) {
                                fechaVencimientoTermino = moment(row.fecha_vencimiento_termino);
                                diferencia = fechaVencimientoTermino.diff(fechaActual, 'days');
                                if(diferencia === 0){
                                    row.style = {'background-color': '#EEF2E6'};
                                }else if(diferencia >= 1 && diferencia <= 3){
                                    row.style = {'background-color': '#FFFF9D'};
                                }else if(diferencia >= 4 && diferencia <= 10){
                                    row.style = {'background-color': '#9BFF93'};
                                }else if(diferencia > 10){
                                    row.style = {'background-color': '#92D0FE'};
                                }
                            });
                            vm.coleccion = [].concat(coleccion);
                            vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                            vm.paginacion.paginaActual = response.data.pagina_actual;
                            vm.paginacion.desde = response.data.desde;
                            vm.paginacion.hasta = response.data.hasta;
                            vm.paginacion.total = response.data.total;
                            tableState.pagination.numberOfPages = response.data.ultima_pagina;
                        }else{
                            vm.coleccion = [];
                        }
                    }
                }).finally(function () {
                    vm.buscando = false;
                    vm.cargando = false;
                    vm.activarBuscar = false;
                });
            }
        }

        function pasarActuaciones() {
            var actuacionIds = [];
            angular.forEach(vm.coleccion, function (row) {
                if(row.pasa){
                    actuacionIds.push(row.id);
                }
            });

            if(actuacionIds.length > 0){
                genericService.modificar(recurso + "/actuaciones-pendientes", actuacionIds)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        buscar();
                        messageUtil.success(response.data.mensajes[0]);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }
                });
            }else{
                messageUtil.error("Debe seleccionar al menos una actuación");
            }
        }

        function limpiarFiltros(){
            vm.nombre = null;
            vm.ciudad = null;
            $scope.isReset = true;
        }

        // Acciones

        function obtenerExcel() {
            if(vm.coleccion.length > 0) {
                genericService.obtenerArchivo("actuaciones/actuaciones-pendientes/excel", {
                    proceso: vm.filtroProceso,
                    actuacion: vm.filtroActuacion,
                    demandante: vm.filtroDemandante,
                    demandado: vm.filtroDemandado,
                    vence_hoy: vm.filtroVenceHoy,
                    por_vencer: vm.filtroPorVencer,
                    pendientes: vm.filtroPendientes,
                    mas_de_diz_dias: vm.filtroMasDeDiezDias,
                    cumplimiento: vm.pasa
                });
            }
        }

        function obtenerArchivoAnexo(row) {
            genericService.obtenerArchivo("actuaciones/" + row.id + "/archivo-anexo")
                .then(function (response) {
                    if(response.status === Constantes.Response.HTTP_NOT_FOUND){
                        messageUtil.error("No se encontro un archivo anexo.");
                    }else if(response.status === Constantes.Response.HTTP_FORBIDDEN){
                        messageUtil.error("No tiene permitido descargar este archivo.");
                    }
                });
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('auditoriaProcesoController', auditoriaProcesoController);

    auditoriaProcesoController.$inject = [
        '$scope',
        '$http',
        '$routeParams',
        '$timeout',
        'sessionUtil',
        'envService',
        'genericService',
        'Upload',
        'messageUtil',
        'Constantes'
    ];

    function auditoriaProcesoController(
        $scope, $http, $routeParams, $timeout, sessionUtil, envService, genericService, Upload, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "auditoria-procesos";

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.abrirFormulario = abrirFormulario;
        vm.programar = programar;
        vm.cancelar = cancelar;
        vm.descargarAuditoria = descargarAuditoria;
        vm.descargarNovedades = descargarNovedades;

        // Acciones archivos
        vm.cargarArchivo = cargarArchivo;
        vm.descargarArchivo = descargarArchivo;
        vm.eliminarArchivo = eliminarArchivo;

        // Limpiar filtros
        limpiarFiltros();

        // Grid

        function obtenerResumen(tableState, ctrl){
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }
            if(pipePromise) $timeout.cancel(pipePromise);
            pipePromise = $timeout(function() {
                if(!vm.cargando){
                    vm.cargando = true;
                    genericService.obtenerColeccion(recurso, tableState)
                        .then(function (response){
                            if(response.status === Constantes.Response.HTTP_OK){
                                var coleccion = response.data.datos;
                                if(coleccion && coleccion.length > 0 ){
                                    vm.coleccion = [].concat(coleccion);
                                    vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                                    vm.paginacion.paginaActual = response.data.pagina_actual;
                                    vm.paginacion.desde = response.data.desde;
                                    vm.paginacion.hasta = response.data.hasta;
                                    vm.paginacion.total = response.data.total;
                                    tableState.pagination.numberOfPages = response.data.ultima_pagina;
                                }else{
                                    vm.coleccion = [];
                                }
                            }
                        }).finally(function () {
                            vm.cargando = false;
                        });
                }
            }, 500);
        }

        function limpiarFiltros(){
            $scope.isReset = true;
        }

        // Acciones

        function abrirFormulario() {
            limpiarFormulario();
            vm.formularioVisible = !vm.formularioVisible;
        }

        function programar(){
            var conErrores = false, mensaje;
            if(!(vm.fechaProgramacion || vm.archivo)){
                conErrores = true;
                mensaje = "Debe indicar la fecha de programación o cargar un archivo.";
            }

            if(!conErrores && !vm.cargandoArchivo){
                vm.cargandoArchivo = true;
                var fd = new FormData();
                if(vm.fechaProgramacion){
                    fd.append('recientes', vm.recientes);
                    fd.append('fecha_programacion', vm.fechaProgramacion);
                }
                if(vm.fechaProgramacionPasada){
                    fd.append('fecha_programacion_pasada', vm.fechaProgramacionPasada);
                }
                if(vm.archivo){
                    fd.append('file', vm.archivo);
                }
                return $http.post(envService.read('apiUrl') + recurso, fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined}
                }).then(function (response) {
                    if(response.status === Constantes.Response.HTTP_CREATED){
                        // Recargar grid
                        limpiarFiltros();

                        // Limpiar formulario
                        limpiarFormulario();

                        // Carrar formulario
                        vm.formularioVisible = false;

                        // Mostrar mensaje de respuesta
                        messageUtil.success(response.data.mensajes[0]);
                    }
                }).finally(function () {
                    vm.cargandoArchivo = false;
                });
            }

            if(conErrores){
                messageUtil.error(mensaje);
            }
        }

        function cancelar() {
            limpiarFormulario();
            vm.formularioVisible = false;
        }

        function descargarAuditoria(row){
            genericService.obtenerArchivo('auditoria-procesos/excel', {
                programacion_id: row.id
            });
        }

        function descargarNovedades(row){
            genericService.obtenerArchivo('auditoria-procesos/novedades/excel', {
                programacion_id: row.id
            });
        }

        function limpiarFormulario() {
            eliminarArchivo();
            vm.recientes = true;
            vm.fechaProgramacion = null;
            vm.fechaProgramacionPasada = null;
        }

        // Acciones archivos

        function cargarArchivo($archivo) {
            Upload.dataUrl($archivo, true).then(function(base64) {
                if(base64){
                    vm.archivo = $archivo;
                    vm.archivoNombre = $archivo.name;
                    vm.archivoBase64 = base64;
                }
            });
        }

        function descargarArchivo() {
            var a = document.createElement("a");
            document.body.appendChild(a);
            a.style = "display: none";

            var blob = Upload.dataUrltoBlob(vm.archivoBase64, vm.archivoNombre),
                url = window.URL.createObjectURL(blob);

            a.href = url;
            a.download = vm.archivoNombre;
            a.click();
            window.URL.revokeObjectURL(url);
        }

        function eliminarArchivo() {
            vm.archivo = null;
            vm.archivoNombre = null;
            vm.archivoBase64 = null;
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('actuacionController', actuacionController);

    actuacionController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        '$uibModal',
        'sessionUtil',
        'envService',
        'genericService',
        'langUtil',
        'messageUtil',
        'Constantes'
    ];

    function actuacionController(
        $scope, $routeParams, $timeout, $uibModal, sessionUtil, envService, genericService,
        langUtil, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "actuaciones";

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.crear = crear;
        vm.seleccionar = seleccionar;
        vm.seleccionarTodo = seleccionarTodo;

        // Modulos externos
        vm.confirmarEliminar = confirmarEliminar;
        vm.confirmarEliminarMasivo = confirmarEliminarMasivo;

        init();

        function init() {
            vm.coleccion = [];
            vm.paginacion = {};
            vm.itemsPorPagina = 25;
            $scope.isReset = true;
            vm.titulo = sessionUtil.getTituloVista($routeParams.o);

            // inicial autocomples
            initAutocompleteDepartamento();
            initAutocompleteCiudad();
            initAutocompleteJuzgado();
            initAutocompleteDespacho();
            initAutocompleteTipoNotificacion();
        }

        // Grid

        function obtenerResumen(tableState, ctrl){
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }
            if(pipePromise) $timeout.cancel(pipePromise);
            pipePromise = $timeout(function() {
                if(!vm.cargando){
                    vm.cargando = true;
                    genericService.obtenerColeccion(recurso, tableState)
                        .then(function (response){
                            if(response.status === Constantes.Response.HTTP_OK){
                                var coleccion = response.data.datos;
                                if(coleccion && coleccion.length > 0 ){
                                    angular.forEach(coleccion, function (row) {
                                        row.seleccionado = vm.todoSeleccionado;
                                    });
                                    vm.coleccion = [].concat(coleccion);
                                    vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                                    vm.paginacion.paginaActual = response.data.pagina_actual;
                                    vm.paginacion.desde = response.data.desde;
                                    vm.paginacion.hasta = response.data.hasta;
                                    vm.paginacion.total = response.data.total;
                                    tableState.pagination.numberOfPages = response.data.ultima_pagina;
                                }else{
                                    vm.coleccion = [];
                                }
                            }
                        }).finally(function () {
                            vm.cargando = false;
                        });
                }
            }, 500);
        }

        function limpiarFiltros(){
            vm.filtroDepartamentoId = null;
            vm.filtroCiudadId = null;
            vm.filtroJuzgadoId = null;
            vm.filtroDespachoId = null;
            vm.filtroNumeroRadicadoActual = null;
            vm.filtroFechaDesde = null;
            vm.filtroFechaHasta = null;
            vm.filtroTipoNotificacionId = null;
            $scope.isReset = true;
        }

        // Acciones

        function eliminar(row){
            genericService.eliminar(recurso, row.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        $scope.$broadcast('refreshGrid');
                        messageUtil.success(response.data.mensajes[0]);
                    }else if(
                        response.status === Constantes.Response.HTTP_CONFLICT ||
                        response.status === Constantes.Response.HTTP_BAD_REQUEST
                    ){
                        messageUtil.error(response.data.mensajes[0]);
                    }
                });
        }

        function eliminarMasivo(){
            var actuacionIds = [];
            angular.forEach(vm.coleccion, function (row) {
                if(row.seleccionado){
                    actuacionIds.push(row.id);
                }
            });
            genericService.modificar(recurso + "/todo/eliminar", {
                filtro_todo: !!vm.todoSeleccionado,
                filtro_actuacion_ids: !vm.todoSeleccionado ? actuacionIds : null,
                filtro_departamento_id: langUtil.isNotBlank(vm.filtroDepartamentoId) ? vm.filtroDepartamentoId : null,
                filtro_ciudad_id: langUtil.isNotBlank(vm.filtroCiudadId) ? vm.filtroCiudadId : null,
                filtro_juzgado_id: langUtil.isNotBlank(vm.filtroJuzgadoId) ? vm.filtroJuzgadoId : null,
                filtro_despacho_id: langUtil.isNotBlank(vm.filtroDespachoId) ? vm.filtroDespachoId : null,
                filtro_numero_radicado_actual:
                    langUtil.isNotBlank(vm.filtroNumeroRadicadoActual) ? vm.filtroNumeroRadicadoActual : null,
                filtro_fecha_desde: langUtil.isNotBlank(vm.filtroFechaDesde) ? vm.filtroFechaDesde : null,
                filtro_fecha_hasta: langUtil.isNotBlank(vm.filtroFechaHasta) ? vm.filtroFechaHasta : null,
                filtro_tipo_notificacion_id:
                    langUtil.isNotBlank(vm.filtroTipoNotificacionId) ? vm.filtroTipoNotificacionId : null
            })
            .then(function(response){
                if(response.status === Constantes.Response.HTTP_OK){
                    vm.todoSeleccionado = false;
                    $scope.$broadcast('refreshGrid');
                    messageUtil.success(response.data.mensajes[0]);
                }else if(
                    response.status === Constantes.Response.HTTP_CONFLICT ||
                    response.status === Constantes.Response.HTTP_BAD_REQUEST
                ){
                    messageUtil.error(response.data.mensajes[0]);
                }
            });
        }

        function seleccionar(row) {
            if(!row.seleccionado && vm.todoSeleccionado){
                vm.todoSeleccionado = false;
            }
        }

        function seleccionarTodo() {
            angular.forEach(vm.coleccion, function (actuacion) {
                actuacion.seleccionado = vm.todoSeleccionado;
            });
        }

        // Modulos externos

        function crear(actuacion) {
            var conError = false;
            if(!actuacion){
                var key, mensaje,
                    departamentoId = null, departamentoNombre = null, departamentoCodigoDane = null,
                    ciudadId = null, ciudadNombre = null, ciudadCodigoDane = null,
                    juzgadoId = null, juzgadoNombre = null, juzgadoNumero = null,
                    despachoId = null, despachoNumero = null, tipoNotificacionNombre = null,
                    coleccion = _.filter(vm.coleccion, function (row) {
                        return row.seleccionado;
                    });
                if(!(coleccion && coleccion.length > 0)){
                    conError = true;
                    mensaje = "Debe seleccionar al menos una actuación.";
                }
                var departamentos = _.groupBy(coleccion, 'departamento_id');
                if(departamentos && langUtil.getSize(departamentos) === 1){
                    for (key in departamentos) {
                        departamentoId = departamentos[key][0].departamento_id;
                        departamentoNombre = departamentos[key][0].departamento_nombre;
                        departamentoCodigoDane = departamentos[key][0].departamento_codigo_dane;
                    }
                    var ciudades = _.groupBy(coleccion, 'ciudad_id');
                    if(ciudades && langUtil.getSize(ciudades) === 1){
                        for (key in ciudades) {
                            ciudadId = ciudades[key][0].ciudad_id;
                            ciudadNombre = ciudades[key][0].ciudad_nombre;
                            ciudadCodigoDane = ciudades[key][0].ciudad_codigo_dane;
                        }
                        var juzgados = _.groupBy(coleccion, 'juzgado_id');
                        if(juzgados && langUtil.getSize(juzgados) === 1){
                            for (key in juzgados) {
                                juzgadoId = juzgados[key][0].juzgado_id;
                                juzgadoNombre = juzgados[key][0].juzgado_nombre;
                                juzgadoNumero = juzgados[key][0].juzgado_numero;
                            }
                            var despachos = _.groupBy(coleccion, 'despacho_id');
                            if(despachos && langUtil.getSize(despachos) === 1){
                                for (key in despachos) {
                                    despachoId = despachos[key][0].despacho_id;
                                    despachoNumero = despachos[key][0].despacho_numero;
                                }
                            }else{
                                despachoNumero = "VARIOS";
                                conError = true;
                                mensaje = "Para modificación masiva de información se debe seleccionar solo un despacho.";
                            }
                        }else{
                            juzgadoNombre = "VARIOS";
                            despachoNumero = "VARIOS";
                            conError = true;
                            mensaje = "Para modificación masiva de información se debe seleccionar solo un juzgado.";
                        }
                    }else{
                        ciudadNombre = "VARIOS";
                        juzgadoNombre = "VARIOS";
                        despachoNumero = "VARIOS";
                    }
                }else{
                    departamentoNombre = "VARIOS";
                    ciudadNombre = "VARIOS";
                    juzgadoNombre = "VARIOS";
                    despachoNumero = "VARIOS";
                }
                var tiposNotificaciones = _.groupBy(coleccion, 'departamento_id');
                if(tiposNotificaciones && langUtil.getSize(tiposNotificaciones) === 1){
                    for (key in tiposNotificaciones) {
                        tipoNotificacionNombre = tiposNotificaciones[key][0].tipo_notificacion_nombre;
                    }
                }else{
                    tipoNotificacionNombre = "VARIOS";
                }
            }

            if(!conError){
                var actuacionIds = [];
                angular.forEach(vm.coleccion, function (row) {
                    if(row.seleccionado){
                        actuacionIds.push(row.id);
                    }
                });

                var modalInstance = $uibModal.open({
                    animation: true,
                    backdrop: 'static',
                    templateUrl: envService.read('apiUrl') + recurso + '/create',
                    controller: 'actuacionEditController as vm',
                    size: 'lg',
                    resolve: {
                        actuacion: function () {
                            return actuacion;
                        },
                        parametros: function () {
                            return !actuacion ? {
                                masivo: true,
                                todo: !!vm.todoSeleccionado,
                                actuacion_ids: !vm.todoSeleccionado ? actuacionIds : null,
                                filtro_departamento_id: vm.filtroDepartamentoId,
                                filtro_ciudad_id: vm.filtroCiudadId,
                                filtro_juzgado_id: vm.filtroJuzgadoId,
                                filtro_despacho_id: vm.filtroDespachoId,
                                filtro_numero_radicado_actual: vm.filtroNumeroRadicadoActual,
                                filtro_fecha_desde: vm.filtroFechaDesde,
                                filtro_fecha_hasta: vm.filtroFechaHasta,
                                filtro_tipo_notificacion_id: vm.filtroTipoNotificacionId,
                                departamento_id: departamentoId,
                                departamento_nombre: departamentoNombre,
                                departamento_codigo_dane: departamentoCodigoDane,
                                ciudad_id: ciudadId,
                                ciudad_nombre: ciudadNombre,
                                ciudad_codigo_dane: ciudadCodigoDane,
                                juzgado_id: juzgadoId,
                                juzgado_nombre: juzgadoNombre,
                                juzgado_numero: juzgadoNumero,
                                despacho_id: despachoId,
                                despacho_numero: despachoNumero,
                                tipo_notificacion_nombre: tipoNotificacionNombre
                            } : null;
                        }
                    }
                });
                modalInstance.result.then(function (response) {
                    vm.todoSeleccionado = false;
                    $scope.$broadcast('refreshGrid');
                });
            }

            if(conError){
                messageUtil.error(mensaje);
            }
        }

        function confirmarEliminar(row) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + 'confirmar-template',
                controller: 'confirmarController as vm',
                size: 'md',
                resolve: {
                    parametros: function () {
                        return {
                            mensaje: "¿Estás seguro de que quieres eliminar la actuación?"
                        };
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(response.ok){
                    eliminar(row);
                }
            });
        }

        function confirmarEliminarMasivo() {
            var conError = false,
                coleccion = _.filter(vm.coleccion, function (row) {
                    return row.seleccionado;
                });
            if(!(coleccion && coleccion.length > 0)){
                conError = true;
            }

            if(!conError){
                var modalInstance = $uibModal.open({
                    animation: true,
                    backdrop: 'static',
                    templateUrl: envService.read('apiUrl') + 'confirmar-template',
                    controller: 'confirmarController as vm',
                    size: 'md',
                    resolve: {
                        parametros: function () {
                            return {
                                mensaje: "¿Está seguro que desea eliminar la actuaciones seleccionadas?"
                            };
                        }
                    }
                });
                modalInstance.result.then(function (response) {
                    if(response.ok){
                        eliminarMasivo();
                    }
                });
            }else{
                messageUtil.error("Debe seleccionar al menos una actuación.");
            }
        }

        // Autocompletes

        function initAutocompleteDepartamento() {
            vm.configDepartamento = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                render: {
                    item: function(item, escape) {
                        return '<div>' +
                            '<span>' + escape(item.nombre) + '</span>' +
                            '<span> - ' + escape(item.codigo_dane) + '</span>' +
                            '</div>';
                    },
                    option: function(item, escape) {
                        return '<div>' +
                            '<span>' + escape(item.nombre) + '</span>' +
                            '<span> - ' + escape(item.codigo_dane) + '</span>' +
                            '</div>';
                    }
                },
                onItemAdd: function (value, $item) {
                    if(!vm.cargandoCiudades){
                        vm.cargandoCiudades = true;
                        vm.filtroDespachoId = null;
                        vm.filtroJuzgadoId = null;
                        vm.filtroCiudadId = null;
                        vm.opcionesDespacho = [];
                        vm.opcionesJuzgado = [];
                        vm.opcionesCiudad = [];
                        $timeout(function () {
                            vm.limpiarOpcionesDespacho = true;
                            vm.limpiarOpcionesJuzgado = true;
                            vm.limpiarOpcionesCiudad = true;
                            genericService.obtenerColeccionLigera("ciudades", {
                                ligera: true,
                                departamento_id: value
                            }).then(function (response){
                                vm.opcionesCiudad = [].concat(response.data);
                            }).finally(function () {
                                vm.cargandoCiudades = false;
                            });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.filtroDespachoId = null;
                    vm.filtroJuzgadoId = null;
                    vm.filtroCiudadId = null;
                    vm.opcionesDespacho = [];
                    vm.opcionesJuzgado = [];
                    vm.opcionesCiudad = [];
                    $timeout(function () {
                        vm.limpiarOpcionesDespacho = true;
                        vm.limpiarOpcionesJuzgado = true;
                        vm.limpiarOpcionesCiudad = true;
                    }, 300);
                }
            };
            vm.opcionesDepartamento = [];
            genericService.obtenerColeccionLigera("departamentos",{
                ligera: true,
            })
            .then(function (response){
                vm.opcionesDepartamento = [].concat(response.data);
            });
        }

        function initAutocompleteCiudad() {
            vm.configCiudad = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                render: {
                    item: function(item, escape) {
                        return '<div>' +
                            '<span>' + escape(item.nombre) + '</span>' +
                            '<span> - ' + escape(item.codigo_dane) + '</span>' +
                            '</div>';
                    },
                    option: function(item, escape) {
                        return '<div>' +
                            '<span>' + escape(item.nombre) + '</span>' +
                            '<span> - ' + escape(item.codigo_dane) + '</span>' +
                            '</div>';
                    }
                },
                onItemAdd: function (value, $item) {
                    if(!vm.cargandoJuzgados){
                        vm.cargandoJuzgados = true;
                        vm.filtroDespachoId = null;
                        vm.filtroJuzgadoId = null;
                        vm.opcionesDespacho = [];
                        vm.opcionesJuzgado = [];
                        $timeout(function () {
                            vm.limpiarOpcionesDespacho = true;
                            vm.limpiarOpcionesJuzgado = true;
                            genericService.obtenerColeccionLigera("juzgados", {
                                ligera: true,
                                ciudad_id: value
                            }).then(function (response){
                                vm.opcionesJuzgado = [].concat(response.data);
                            }).finally(function () {
                                vm.cargandoJuzgados = false;
                            });
                        }, 300);
                    }
                },
                onItemRemove: function () {
                    vm.filtroDespachoId = null;
                    vm.filtroJuzgadoId = null;
                    vm.opcionesDespacho = [];
                    vm.opcionesJuzgado = [];
                    $timeout(function () {
                        vm.limpiarOpcionesDespacho = true;
                        vm.limpiarOpcionesJuzgado = true;
                    }, 300);
                }
            };
            vm.opcionesCiudad = [];
        }

        function initAutocompleteJuzgado() {
            vm.configJuzgado = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                render: {
                    item: function(item, escape) {
                        return '<div>' +
                            '<span>' + escape(item.nombre) + '</span>' +
                            '<span> - ' + escape(item.juzgado) + escape(item.sala) + '</span>' +
                            '</div>';
                    },
                    option: function(item, escape) {
                        return '<div>' +
                            '<span>' + escape(item.nombre) + '</span>' +
                            '<span> - ' + escape(item.juzgado) + escape(item.sala) + '</span>' +
                            '</div>';
                    }
                },
                onItemAdd: function (value, $item) {
                    if(!vm.cargandoDespachos){
                        vm.cargandoDespachos = true;
                        vm.filtroDespachoId = null;
                        vm.opcionesDespacho = [];
                        $timeout(function () {
                            vm.limpiarOpcionesDespacho = true;
                            genericService.obtenerColeccionLigera("despachos", {
                                ligera: true,
                                ciudad_id: vm.filtroCiudadId ? vm.filtroCiudadId : null,
                                juzgado_id: value
                            }).then(function (response){
                                vm.opcionesDespacho = [].concat(response.data);
                            }).finally(function () {
                                vm.cargandoDespachos = false;
                            });
                        }, 300);
                    }
                },
                onItemRemove: function () {
                    vm.filtroDespachoId = null;
                    vm.opcionesDespacho = [];
                    $timeout(function () {
                        vm.limpiarOpcionesDespacho = true;
                    }, 300);
                }
            };
            vm.opcionesJuzgado = [];
        }

        function initAutocompleteDespacho() {
            vm.configDespacho = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                render: {
                    item: function(item, escape) {
                        return '<div>' +
                            '<span>' + escape(item.despacho) + '</span>' +
                            '</div>';
                    },
                    option: function(item, escape) {
                        return '<div>' +
                            '<span>' + escape(item.despacho) + '</span>' +
                            '</div>';
                    }
                }
            };
            vm.opcionesDespacho = [];
        }

        function initAutocompleteTipoNotificacion() {
            vm.configTipoNotificacion = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true
            };
            vm.opcionesTipoNotificacion = [];
            genericService.obtenerColeccionLigera("tipos-notificaciones",{
                ligera: true,
            }).then(function (response){
                vm.opcionesTipoNotificacion = [].concat(response.data);
            });
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('actuacionEditController', actuacionEditController);

    actuacionEditController.$inject = [
        '$uibModalInstance',
        '$timeout',
        'genericService',
        'langUtil',
        'messageUtil',
        'Constantes',
        'actuacion',
        'parametros'
    ];

    function actuacionEditController(
        $uibModalInstance, $timeout, genericService, langUtil, messageUtil, Constantes, actuacion, parametros
    ){
        var vm = this,
            recurso = "actuaciones";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;
        vm.guardarMasivo = guardarMasivo;
        vm.validarDemandante = validarDemandante;
        vm.validarDemandado = validarDemandado;

        init();

        function init() {
            // Inicial autocomples
            initAutocompleteDepartamento();
            initAutocompleteCiudad();
            initAutocompleteJuzgado();
            initAutocompleteDespacho();
            initAutocompleteTipoNotificacion();

            // Cargar actuación
            if(parametros && parametros.masivo){
                vm.modoEdicion = true;
                vm.modoEdicionMasiva = true;
                vm.departamentoNombre = parametros.departamento_nombre;
                vm.ciudadNombre = parametros.ciudad_nombre;
                vm.juzgadoNombre = parametros.juzgado_nombre;
                vm.despachoNumero = parametros.despacho_numero;
                vm.tipoNotificacionNombre = parametros.tipo_notificacion_nombre;
                if(parametros.ciudad_id && parametros.departamento_id){
                    vm.departamentoId = parametros.departamento_id;
                    vm.opcionesDepartamento.push({
                        id: parametros.departamento_id,
                        nombre: parametros.departamento_nombre,
                        codigo_dane: parametros.departamento_codigo_dane
                    });
                    vm.departamentoIdOriginal = parametros.departamento_id;
                    vm.opcionesCiudad.push({
                        id: parametros.ciudad_id,
                        nombre: parametros.ciudad_nombre,
                        codigo_dane: parametros.ciudad_codigo_dane
                    });
                    vm.ciudadIdOriginal = parametros.ciudad_id;
                }
            }
            if(actuacion && actuacion.id){
                vm.modoEdicion = true;
                cargar();
            }
        }

        // Acciones

        function cargar() {
            genericService.cargar(recurso, actuacion.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var actuacion = response.data;
                        vm.numeroRadicadoActual = actuacion.numero_radicado_proceso_actual;
                        vm.fechaRegistro = actuacion.fecha_registro;
                        vm.fechaActuacion = actuacion.fecha_actuacion;
                        vm.fechaInicioTermino = actuacion.fecha_inicio_termino;
                        vm.fechaVencimientoTermino = actuacion.fecha_vencimiento_termino;
                        vm.demandante = actuacion.nombres_demandantes;
                        vm.demandado = actuacion.nombres_demandados;
                        vm.anotacion = actuacion.anotacion;
                        vm.descripcionClaseProceso = actuacion.descripcion_clase_proceso;
                        vm.fechaCreacion = actuacion.created_at;
                        vm.fechaModificacion = actuacion.updated_at;
                        vm.usuarioCreacionNombre = actuacion.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = actuacion.usuario_modificacion_nombre;
                        vm.estado = actuacion.estado;
                        if(actuacion.ciudad && actuacion.ciudad.departamento){
                            vm.departamentoId = actuacion.ciudad.departamento.id;
                            vm.opcionesDepartamento.push({
                                id: actuacion.ciudad.departamento.id,
                                nombre: actuacion.ciudad.departamento.nombre,
                                codigo_dane: actuacion.ciudad.departamento.codigo_dane
                            });
                            vm.departamentoIdOriginal = actuacion.ciudad.departamento.id;

                            vm.opcionesCiudad.push({
                                id: actuacion.ciudad.id,
                                nombre: actuacion.ciudad.nombre,
                                codigo_dane: actuacion.ciudad.codigo_dane
                            });
                            vm.ciudadIdOriginal = actuacion.ciudad.id;
                        }
                        if(actuacion.juzgado){
                            vm.opcionesJuzgado.push({
                                id: actuacion.juzgado.id,
                                nombre: actuacion.juzgado.nombre,
                                juzgado: actuacion.juzgado.juzgado,
                                sala: actuacion.juzgado.sala
                            });
                            vm.juzgadoIdOriginal = actuacion.juzgado.id;
                        }
                        if(actuacion.despacho){
                            vm.opcionesDespacho.push({
                                id: actuacion.despacho.id,
                                nombre: actuacion.despacho.nombre,
                                despacho: actuacion.despacho.despacho
                            });
                            vm.despachoIdOriginal = actuacion.despacho.id;
                        }
                        if(actuacion.tipo_notificacion){
                            vm.tipoNotificacionId = actuacion.tipo_notificacion.id;
                            vm.opcionesTipoActuacion.push({
                                id: actuacion.tipo_notificacion.id, nombre: actuacion.tipo_notificacion.nombre
                            });
                        }
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.guardadoDeshabilitado = true;
                var datos = {
                    id: actuacion ? +actuacion.id : null,
                    ciudad_id: vm.ciudadId,
                    juzgado_id: vm.juzgadoId,
                    despacho_id: vm.despachoId,
                    anotacion: vm.anotacion,
                    fecha_registro: vm.fechaRegistro,
                    fecha_actuacion: vm.fechaActuacion,
                    fecha_inicio_termino: vm.fechaInicioTermino,
                    fecha_vencimiento_termino: vm.fechaVencimientoTermino,
                    nombres_demandantes: vm.demandante.trim(),
                    nombres_demandados: vm.demandado.trim(),
                    descripcion_clase_proceso: vm.descripcionClaseProceso ? vm.descripcionClaseProceso.trim() : null,
                    tipo_notificacion_id: langUtil.isNotBlank(vm.tipoNotificacionId) ? vm.tipoNotificacionId : null
                };

                var promesa = null;
                if(!(actuacion && actuacion.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        messageUtil.success(response.data.mensajes[0]);
                        $uibModalInstance.close(response.data.datos);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }
                }).finally(function () {
                    vm.guardadoDeshabilitado = false;
                });
            }
        }

        function guardarMasivo() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.guardadoDeshabilitado = true;
                var datos = {
                    // Campos a modificar
                    ciudad_id: vm.ciudadId,
                    juzgado_id: vm.juzgadoId,
                    despacho_id: vm.despachoId,
                    tipo_notificacion_id: langUtil.isNotBlank(vm.tipoNotificacionId) ? vm.tipoNotificacionId : null,
                    // Filtros
                    filtro_todo: !!parametros.todo,
                    filtro_actuacion_ids:
                        parametros.actuacion_ids && parametros.actuacion_ids.length > 0 ? parametros.actuacion_ids : null,
                    filtro_departamento_id:
                        langUtil.isNotBlank(parametros.filtro_departamento_id) ? parametros.filtro_departamento_id : null,
                    filtro_ciudad_id: langUtil.isNotBlank(parametros.filtro_ciudad_id) ? parametros.filtro_ciudad_id : null,
                    filtro_juzgado_id: langUtil.isNotBlank(parametros.filtro_juzgado_id) ? parametros.filtro_juzgado_id : null,
                    filtro_despacho_id: langUtil.isNotBlank(parametros.filtro_despacho_id) ? parametros.filtro_despacho_id : null,
                    filtro_numero_radicado_actual:
                        langUtil.isNotBlank(parametros.filtro_numero_radicado_actual) ? parametros.filtro_numero_radicado_actual : null,
                    filtro_fecha_desde: langUtil.isNotBlank(parametros.filtro_fecha_desde) ? parametros.filtro_fecha_desde : null,
                    filtro_fecha_hasta: langUtil.isNotBlank(parametros.filtro_fecha_hasta) ? parametros.filtro_fecha_hasta : null,
                    filtro_tipo_notificacion_id:
                        langUtil.isNotBlank(parametros.filtro_tipo_notificacion_id) ? parametros.filtro_tipo_notificacion_id : null
                };

                genericService.modificar(recurso, datos)
                    .then(function(response){
                        if(
                            response.status === Constantes.Response.HTTP_CREATED ||
                            response.status === Constantes.Response.HTTP_OK
                        ){
                            messageUtil.success(response.data.mensajes[0]);
                            $uibModalInstance.close(response.data.datos);
                        }else{
                            messageUtil.error(response.data.mensajes[0]);
                        }
                    }).finally(function () {
                        vm.guardadoDeshabilitado = false;
                    });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }

        function validarDemandante() {
            vm.demandante = vm.demandante ? vm.demandante.toUpperCase() : vm.demandante;
        }

        function validarDemandado() {
            vm.demandado = vm.demandado ? vm.demandado.toUpperCase() : vm.demandado;
        }

        // Autocompletes

        function initAutocompleteDepartamento() {
            vm.configDepartamento = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                render: {
                    item: function(item, escape) {
                        return '<div>' +
                            '<span>' + escape(item.nombre) + '</span>' +
                            '<span> - ' + escape(item.codigo_dane) + '</span>' +
                            '</div>';
                    },
                    option: function(item, escape) {
                        return '<div>' +
                            '<span>' + escape(item.nombre) + '</span>' +
                            '<span> - ' + escape(item.codigo_dane) + '</span>' +
                            '</div>';
                    }
                },
                onItemAdd: function (value, $item) {
                    if(!vm.cargandoCiudades){
                        vm.cargandoCiudades = true;
                        vm.despachoId = null;
                        vm.juzgadoId = null;
                        vm.ciudadId = null;
                        vm.opcionesDespacho = [];
                        vm.opcionesJuzgado = [];
                        vm.opcionesCiudad = [];
                        $timeout(function () {
                            vm.limpiarOpcionesDespacho = true;
                            vm.limpiarOpcionesJuzgado = true;
                            vm.limpiarOpcionesCiudad = true;
                            genericService.obtenerColeccionLigera("ciudades", {
                                ligera: true,
                                departamento_id: value
                            }).then(function (response){
                                vm.opcionesCiudad = [].concat(response.data);
                                if(vm.ciudadIdOriginal){
                                    vm.ciudadId = vm.ciudadIdOriginal;
                                    vm.ciudadIdOriginal = null;
                                }
                            }).finally(function () {
                                vm.cargandoCiudades = false;
                            });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.despachoId = null;
                    vm.juzgadoId = null;
                    vm.ciudadId = null;
                    vm.opcionesDespacho = [];
                    vm.opcionesJuzgado = [];
                    vm.opcionesCiudad = [];
                    $timeout(function () {
                        vm.limpiarOpcionesDespacho = true;
                        vm.limpiarOpcionesJuzgado = true;
                        vm.limpiarOpcionesCiudad = true;
                    }, 300);
                }
            };
            vm.opcionesDepartamento = [];
            genericService.obtenerColeccionLigera("departamentos",{
                ligera: true,
            }).then(function (response){
                vm.opcionesDepartamento = [].concat(response.data);
            });
        }

        function initAutocompleteCiudad() {
            vm.configCiudad = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                render: {
                    item: function(item, escape) {
                        return '<div>' +
                            '<span>' + escape(item.nombre) + '</span>' +
                            '<span> - ' + escape(item.codigo_dane) + '</span>' +
                            '</div>';
                    },
                    option: function(item, escape) {
                        return '<div>' +
                            '<span>' + escape(item.nombre) + '</span>' +
                            '<span> - ' + escape(item.codigo_dane) + '</span>' +
                            '</div>';
                    }
                },
                onItemAdd: function (value, $item) {
                    if(!vm.cargandoJuzgados){
                        vm.cargandoJuzgados = true;
                        vm.despachoId = null;
                        vm.juzgadoId = null;
                        vm.opcionesDespacho = [];
                        vm.opcionesJuzgado = [];
                        $timeout(function () {
                            vm.limpiarOpcionesDespacho = true;
                            vm.limpiarOpcionesJuzgado = true;
                            genericService.obtenerColeccionLigera("juzgados", {
                                ligera: true,
                                ciudad_id: value
                            }).then(function (response){
                                vm.opcionesJuzgado = [].concat(response.data);
                                if(vm.juzgadoIdOriginal){
                                    vm.juzgadoId = vm.juzgadoIdOriginal;
                                    vm.juzgadoIdOriginal = null;
                                }
                            }).finally(function () {
                                vm.cargandoJuzgados = false;
                            });
                        }, 300);
                    }
                },
                onItemRemove: function () {
                    vm.despachoId = null;
                    vm.juzgadoId = null;
                    vm.opcionesDespacho = [];
                    vm.opcionesJuzgado = [];
                    $timeout(function () {
                        vm.limpiarOpcionesDespacho = true;
                        vm.limpiarOpcionesJuzgado = true;
                    }, 300);
                }
            };
            vm.opcionesCiudad = [];
        }

        function initAutocompleteJuzgado() {
            vm.configJuzgado = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                render: {
                    item: function(item, escape) {
                        return '<div>' +
                            '<span>' + escape(item.nombre) + '</span>' +
                            '<span> - ' + escape(item.juzgado) + escape(item.sala) + '</span>' +
                            '</div>';
                    },
                    option: function(item, escape) {
                        return '<div>' +
                            '<span>' + escape(item.nombre) + '</span>' +
                            '<span> - ' + escape(item.juzgado) + escape(item.sala) + '</span>' +
                            '</div>';
                    }
                },
                onItemAdd: function (value, $item) {
                    if(!vm.cargandoDespachos){
                        vm.cargandoDespachos = true;
                        vm.despachoId = null;
                        vm.opcionesDespacho = [];
                        $timeout(function () {
                            vm.limpiarOpcionesDespacho = true;
                            genericService.obtenerColeccionLigera("despachos", {
                                ligera: true,
                                ciudad_id: vm.ciudadId ? vm.ciudadId : null,
                                juzgado_id: value
                            }).then(function (response){
                                vm.opcionesDespacho = [].concat(response.data);
                                if(vm.despachoIdOriginal){
                                    vm.despachoId = vm.despachoIdOriginal;
                                    vm.despachoIdOriginal = null;
                                }else{
                                    var juzgadoNombre = $item[0].innerText;
                                    var despacho000 = _.find(response.data, function (despacho) {
                                        return despacho.despacho == '000';
                                    });
                                    if(despacho000 && juzgadoNombre && !juzgadoNombre.includes('JUZGADO')){
                                        vm.despachoId = despacho000.id;
                                    }
                                }
                            }).finally(function () {
                                vm.cargandoDespachos = false;
                            });
                        }, 300);
                    }
                },
                onItemRemove: function () {
                    vm.despachoId = null;
                    vm.opcionesDespacho = [];
                    $timeout(function () {
                        vm.limpiarOpcionesDespacho = true;
                    }, 300);
                }
            };
            vm.opcionesJuzgado = [];
        }

        function initAutocompleteDespacho() {
            vm.configDespacho = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                render: {
                    item: function(item, escape) {
                        return '<div>' +
                            '<span>' + escape(item.despacho) + '</span>' +
                            '</div>';
                    },
                    option: function(item, escape) {
                        return '<div>' +
                            '<span>' + escape(item.despacho) + '</span>' +
                            '</div>';
                    }
                }
            };
            vm.opcionesDespacho = [];
        }

        function initAutocompleteTipoNotificacion() {
            vm.configTipoNotificacion = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true
            };
            vm.opcionesTipoNotificacion = [];
            genericService.obtenerColeccionLigera("tipos-notificaciones",{
                ligera: true,
            }).then(function (response){
                vm.opcionesTipoNotificacion = [].concat(response.data);
            });
        }

    }
})();

 (function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('tipoDiligenciaController', tipoDiligenciaController);

    tipoDiligenciaController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        '$uibModal',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function tipoDiligenciaController(
        $scope, $routeParams, $timeout, $uibModal, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "tipos-diligencias";

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.crear = crear;

        // Modulos externos
        vm.confirmarEliminar = confirmarEliminar;

        // Limpiar filtros
        limpiarFiltros();

        // Grid
        function obtenerResumen(tableState, ctrl){
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }
            if(pipePromise) $timeout.cancel(pipePromise);
            pipePromise = $timeout(function() {
                if(!vm.cargando){
                    vm.cargando = true;
                    genericService.obtenerColeccion(recurso, tableState)
                        .then(function (response){
                            if(response.status === Constantes.Response.HTTP_OK){
                                var coleccion = response.data.datos;
                                if(coleccion && coleccion.length > 0 ){
                                    vm.coleccion = [].concat(coleccion);
                                    vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                                    vm.paginacion.paginaActual = response.data.pagina_actual;
                                    vm.paginacion.desde = response.data.desde;
                                    vm.paginacion.hasta = response.data.hasta;
                                    vm.paginacion.total = response.data.total;
                                    tableState.pagination.numberOfPages = response.data.ultima_pagina;
                                }else{
                                    vm.coleccion = [];
                                }
                            }
                        }).finally(function () {
                            vm.cargando = false;
                        });
                }
            }, 500);
        }

        function limpiarFiltros(){
            vm.nombre = null;
            vm.marca = null;
            $scope.isReset = true;
        }

        /*function cargarFiltros() {
            var filtros = gridUtil.getFormattedSearchPredicate("tipo-de-diligencia");
            vm.nombre = filtros && filtros.nombre ? filtros.nombre : null;
        }*/

        // Acciones

        function eliminar(row){
            genericService.eliminar(recurso, row.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        $scope.$broadcast('refreshGrid');
                        messageUtil.success(response.data.mensajes[0]);
                    }else if(
                        response.status === Constantes.Response.HTTP_CONFLICT ||
                        response.status === Constantes.Response.HTTP_BAD_REQUEST
                    ){
                        messageUtil.error(response.data.mensajes[0]);
                    }
                });
        }

        // Modulos externos

        function crear(tipoDiligencia) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + recurso + '/create',
                controller: 'tipoDiligenciaEditController as vm',
                size: 'md',
                resolve: {
                    tipoDiligencia: function () {
                        return tipoDiligencia;
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(tipoDiligencia && tipoDiligencia.id && response && response.id){
                    tipoDiligencia.nombre = response.nombre;
                    tipoDiligencia.solicitud_autorizacion = response.solicitud_autorizacion;
                    tipoDiligencia.estado = response.estado;
                    tipoDiligencia.usuario_modificacion_id = response.usuario_modificacion_id;
                    tipoDiligencia.usuario_modificacion_nombre = response.usuario_modificacion_nombre;
                    tipoDiligencia.fecha_creacion = response.fecha_creacion;
                    tipoDiligencia.fecha_modificacion = response.fecha_modificacion;
                }else if(response && response.id){
                    $scope.$broadcast('refreshGrid');
                }
            });
        }

        function confirmarEliminar(row) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + 'confirmar-template',
                controller: 'confirmarController as vm',
                size: 'md',
                resolve: {
                    parametros: function () {
                        return {
                            mensaje: "¿Estás seguro de que quieres eliminar el tipo de diligencia?"
                        };
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(response.ok){
                    eliminar(row);
                }
            });
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('tipoDiligenciaEditController', tipoDiligenciaEditController);

    tipoDiligenciaEditController.$inject = [
        '$uibModalInstance',
        'genericService',
        'messageUtil',
        'Constantes',
        'tipoDiligencia'
    ];

    function tipoDiligenciaEditController(
        $uibModalInstance, genericService, messageUtil, Constantes, tipoDiligencia
    ){
        var vm = this,
            recurso = "tipos-diligencias";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        if(tipoDiligencia && tipoDiligencia.id){
            cargar();
        }else{
            vm.estado = true;
        }

        function cargar() {
            genericService.cargar(recurso, tipoDiligencia.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var tipoDiligencia = response.data;
                        vm.nombre = tipoDiligencia.nombre;
                        vm.solicitudAutorizacion = tipoDiligencia.solicitud_autorizacion;
                        vm.estado = tipoDiligencia.estado;
                        vm.fechaCreacion = tipoDiligencia.fecha_creacion;
                        vm.fechaModificacion = tipoDiligencia.fecha_modificacion;
                        vm.usuarioCreacionNombre = tipoDiligencia.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = tipoDiligencia.usuario_modificacion_nombre;
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;
                var datos = {
                    id: tipoDiligencia ? +tipoDiligencia.id : null,
                    nombre: vm.nombre.toUpperCase(),
                    solicitud_autorizacion: vm.solicitudAutorizacion,
                    estado: vm.estado
                };

                var promesa = null;
                if(!(tipoDiligencia && tipoDiligencia.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        $uibModalInstance.close(response.data.datos);
                        messageUtil.success(response.data.mensajes[0]);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }
                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('tarifaDiligenciaController', tarifaDiligenciaController);

    tarifaDiligenciaController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        '$uibModal',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function tarifaDiligenciaController(
        $scope, $routeParams, $timeout, $uibModal, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "tarifas-diligencias";

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.crear = crear;

        // Modulos externos
        vm.confirmarEliminar = confirmarEliminar;

        // Limpiar filtros
        limpiarFiltros();

        // Grid
        function obtenerResumen(tableState, ctrl){
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }
            if(pipePromise) $timeout.cancel(pipePromise);
            pipePromise = $timeout(function() {
                if(!vm.cargando){
                    vm.cargando = true;
                    genericService.obtenerColeccion(recurso, tableState)
                        .then(function (response){
                            if(response.status === Constantes.Response.HTTP_OK){
                                var coleccion = response.data.datos;
                                if(coleccion && coleccion.length > 0 ){
                                    vm.coleccion = [].concat(coleccion);
                                    vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                                    vm.paginacion.paginaActual = response.data.pagina_actual;
                                    vm.paginacion.desde = response.data.desde;
                                    vm.paginacion.hasta = response.data.hasta;
                                    vm.paginacion.total = response.data.total;
                                    tableState.pagination.numberOfPages = response.data.ultima_pagina;
                                }else{
                                    vm.coleccion = [];
                                }
                            }
                        }).finally(function () {
                            vm.cargando = false;
                        });
                }
            }, 500);
        }

        function limpiarFiltros(){
            vm.nombre = null;
            vm.ciudad = null;
            $scope.isReset = true;
        }

        /*function cargarFiltros() {
            var filtros = gridUtil.getFormattedSearchPredicate("tarifaDiligencia");
            vm.nombre = filtros && filtros.nombre ? filtros.nombre : null;
        }*/

        // Acciones

        function eliminar(row){
            genericService.eliminar(recurso, row.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        $scope.$broadcast('refreshGrid');
                        messageUtil.success(response.data.mensajes[0]);
                    }else if(
                        response.status === Constantes.Response.HTTP_CONFLICT ||
                        response.status === Constantes.Response.HTTP_BAD_REQUEST
                    ){
                        messageUtil.error(response.data.mensajes[0]);
                    }
                });
        }

        // Modulos externos

        function crear(tarifaDiligencia) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + recurso + '/create',
                controller: 'tarifaDiligenciaEditController as vm',
                size: 'md',
                resolve: {
                    tarifaDiligencia: function () {
                        return tarifaDiligencia;
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(tarifaDiligencia && tarifaDiligencia.id && response && response.id){
                    tarifaDiligencia.tipo_diligencia_id = response.tipo_diligencia_id;
                    tarifaDiligencia.departamento_nombre = response.departamento.nombre;
                    tarifaDiligencia.ciudad_nombre = response.ciudad.nombre;
                    tarifaDiligencia.valor_diligencia = response.valor_diligencia;
                    tarifaDiligencia.gasto_envio = response.gasto_envio;
                    tarifaDiligencia.otros_gastos = response.otros_gastos;
                    tarifaDiligencia.estado = response.estado;
                    tarifaDiligencia.usuario_modificacion_id = response.usuario_modificacion_id;
                    tarifaDiligencia.usuario_modificacion_nombre = response.usuario_modificacion_nombre;
                    tarifaDiligencia.fecha_creacion = response.fecha_creacion;
                    tarifaDiligencia.fecha_modificacion = response.fecha_modificacion;
                }else if(response && response.id){
                    $scope.$broadcast('refreshGrid');
                }
            });
        }

        function confirmarEliminar(row) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + 'confirmar-template',
                controller: 'confirmarController as vm',
                size: 'md',
                resolve: {
                    parametros: function () {
                        return {
                            mensaje: "¿Estás seguro de que quieres eliminar la tarifa?"
                        };
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(response.ok){
                    eliminar(row);
                }
            });
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('tarifaDiligenciaEditController', tarifaDiligenciaEditController);

    tarifaDiligenciaEditController.$inject = [
        '$uibModalInstance',
        '$timeout',
        'genericService',
        'messageUtil',
        'Constantes',
        'tarifaDiligencia'
    ];

    function tarifaDiligenciaEditController(
        $uibModalInstance, $timeout, genericService, messageUtil, Constantes, tarifaDiligencia
    ){
        var vm = this,
            recurso = "tarifas-diligencias";

        // Acciones
        vm.guardar = guardar;
        vm.cancelar = cancelar;

        // inicial autocomples
        initAutocompleteDepartamento();
        initAutocompleteCiudad();
        initAutocompleteTipoDiligencia();

        if(tarifaDiligencia && tarifaDiligencia.id){
            cargar();
        }else{
            vm.estado = true;
            vm.tarifaDiligenciaActual = true;
        }

        function cargar() {
            genericService.cargar(recurso, tarifaDiligencia.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var tarifaDiligencia = response.data,
                            ciudad = tarifaDiligencia.ciudad,
                            departamento = tarifaDiligencia.departamento,
                            tipoDiligencia = tarifaDiligencia.tipo_diligencia;

                        vm.valorDiligencia = tarifaDiligencia.valor_diligencia;
                        vm.gastoEnvio = tarifaDiligencia.gasto_envio;
                        vm.otrosGastos = tarifaDiligencia.otros_gastos;
                        vm.estado = tarifaDiligencia.estado;
                        vm.fechaCreacion = tarifaDiligencia.fecha_creacion;
                        vm.fechaModificacion = tarifaDiligencia.fecha_modificacion;
                        vm.usuarioCreacionNombre = tarifaDiligencia.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = tarifaDiligencia.usuario_modificacion_nombre;

                        if(tipoDiligencia) {
                            vm.tipoDiligenciaId = tipoDiligencia.id;
                            vm.opcionesTipoDiligencia.push({id: tipoDiligencia.id, nombre: tipoDiligencia.nombre});
                        }
                        if(ciudad){
                            vm.departamentoId = departamento.id;
                            vm.opcionesDepartamento.push({id: departamento.id, nombre: departamento.nombre});
                            vm.ciudadId = ciudad.id;
                            vm.opcionesCiudad.push({id: ciudad.id, nombre: ciudad.nombre});
                            vm.limpiarOpcionesCiudad = false;
                        }
                    }
                });
        }

        function guardar() {
            vm.guardando = true;
            if(vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;

                var datos = {
                    id: tarifaDiligencia ? +tarifaDiligencia.id : null,
                    tipo_diligencia_id: vm.tipoDiligenciaId,
                    departamento_id: vm.departamentoId,
                    ciudad_id: vm.ciudadId,
                    valor_diligencia: vm.valorDiligencia,
                    gasto_envio: vm.gastoEnvio,
                    otros_gastos: vm.otrosGastos,
                    estado: vm.estado

            };

                var promesa = null;
                if(!(tarifaDiligencia && tarifaDiligencia.id)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        messageUtil.success(response.data.mensajes[0]);
                        $uibModalInstance.close(response.data.datos);
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }


                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function cancelar() {
            $uibModalInstance.dismiss('cancel');
        }

        function initAutocompleteTipoDiligencia() {
            vm.configTipoDiligencia = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
            };

            vm.opcionesTipoDiligencia = [];
            genericService.obtenerColeccionLigera("tipos-diligencias",{
                ligera: true,
            })
                .then(function (response){
                    vm.opcionesTipoDiligencia = [].concat(response.data);
                });
        }

        function initAutocompleteDepartamento() {
            vm.configDepartamento = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                onItemAdd: function (value, $item) {
                    if(vm.departamentoId !== value){
                        vm.opcionesCiudad = [];
                        $timeout(function () {
                            vm.limpiarOpcionesCiudad = true;
                            genericService.obtenerColeccionLigera("ciudades", {
                                ligera: true,
                                departamento_id: value
                            })
                            .then(function (response){
                                vm.opcionesCiudad = [].concat(response.data);
                            });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.opcionesCiudad = [];
                    vm.ciudadId = null;
                    $timeout(function () {
                        vm.limpiarOpcionesCiudad = true;
                    }, 100);
                }
            };

            vm.opcionesDepartamento = [];
            genericService.obtenerColeccionLigera("departamentos",{
                ligera: true,
            })
            .then(function (response){
                vm.opcionesDepartamentos = [].concat(response.data);
            });
        }

        function initAutocompleteCiudad() {
            vm.configCiudad = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                lock: false,
            };
            vm.opcionesCiudad = [];
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('diligenciaController', diligenciaController);

    diligenciaController.$inject = [
        '$translate',
        '$scope',
        '$timeout',
        '$routeParams',
        '$uibModal',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function diligenciaController(
        $translate, $scope, $timeout, $routeParams, $uibModal, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            i18n = {},
            pipePromise,
            recurso = "diligencias";

        vm.diligenciaTramite = !!($routeParams.dependiente && +$routeParams.dependiente === 1);

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.EstadoDiligenciaEnum = Constantes.EstadoDiligencia;
        vm.activarBuscar = true;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Modulos externos
        vm.confirmarEliminar = confirmarEliminar;

        //Acciones
        vm.buscar = buscar;
        vm.obtenerExcel = obtenerExcel;

        // Limpiar filtros
        limpiarFiltros();

        // Cargar traducciones
        cargarTraducciones();

        // Cargar autocompletes
        initAutocompleteEstadoDiligencia();

        // Grid

        function buscar() {
            vm.buscando = true;
            if(vm.form.$valid) {
                vm.activarBuscar = true;
                $scope.$broadcast('refreshGrid');
            }
        }

        function obtenerResumen(tableState, ctrl) {
            if (vm.diligenciaTramite) {
                tableState.search = tableState.search || {};
                tableState.search.predicateObject = tableState.search.predicateObject || {};
                tableState.search.predicateObject.dependiente = vm.diligenciaTramite;
            }
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }

            if(vm.paginacion && vm.paginacion.paginaActual && tableState.pagination){
                var start = tableState.pagination.start || 0,
                    number = tableState.pagination.number || 10,
                    nextPage = (start + number) / number;
                if(vm.paginacion.paginaActual != nextPage){
                    vm.activarBuscar = true;
                }else if(vm.activarBuscar){
                    tableState.pagination.start = 0;
                    tableState.pagination.number = vm.itemsPorPagina;
                }
            }

            if(vm.activarBuscar && !vm.cargando){
                vm.cargando = true;
                genericService.obtenerColeccion(recurso, tableState).then(function (response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var coleccion = response.data.datos;
                        if(coleccion && coleccion.length > 0 ){
                            vm.coleccion = [].concat(coleccion);
                            vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                            vm.paginacion.paginaActual = response.data.pagina_actual;
                            vm.paginacion.desde = response.data.desde;
                            vm.paginacion.hasta = response.data.hasta;
                            vm.paginacion.total = response.data.total;
                            tableState.pagination.numberOfPages = response.data.ultima_pagina;
                        }else{
                            vm.coleccion = [];
                        }
                    }
                }).finally(function () {
                    vm.buscando = false;
                    vm.cargando = false;
                    vm.activarBuscar = false;
                });
            }
        }

        function obtenerExcel() {
            genericService.obtenerArchivo(recurso + "/excel", {
                numero_diligencia: vm.numeroDiligencia,
                empresa_cliente: vm.empresaCliente,
                numero_proceso: vm.proceso,
                estado_diligencia: vm.filtroEstado,
                fecha_inicial: vm.filtroFechaInicial,
                fecha_final: vm.filtroFechaFinal,
                dependiente: vm.diligenciaTramite,
            });
        }

        function limpiarFiltros(){
            vm.nombre = null;
            vm.ciudad = null;
            vm.numeroDiligencia = null;
            vm.empresaCliente = null;
            vm.proceso = null;
            vm.filtroEstado = null;
            vm.filtroFechaInicial = null;
            vm.filtroFechaFinal = null;
            vm.activarBuscar = true;
            $scope.isReset = true;
        }

        /*function cargarFiltros() {
            var filtros = gridUtil.getFormattedSearchPredicate("diligencia");
            vm.nombre = filtros && filtros.nombre ? filtros.nombre : null;
        }*/

        // Acciones

        function eliminar(row){
            genericService.eliminar(recurso, row.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        $scope.$broadcast('refreshGrid');
                        location.reload();
                        messageUtil.success(response.data.mensajes[0]);
                    }else if(
                        response.status === Constantes.Response.HTTP_CONFLICT ||
                        response.status === Constantes.Response.HTTP_BAD_REQUEST
                    ){
                        messageUtil.error(response.data.mensajes[0]);
                    }
                });
        }

        function confirmarEliminar(row) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + 'confirmar-template',
                controller: 'confirmarController as vm',
                size: 'md',
                resolve: {
                    parametros: function () {
                        return {
                            mensaje: "¿Estás seguro de que quieres eliminar la tarifa?"
                        };
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(response.ok){
                    eliminar(row);
                }
            });
        }

        function initAutocompleteEstadoDiligencia() {
            vm.configEstadoDiligencia = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'id',
                allowEmptyOption: false,
                create: false,
                persist: true
            };
            vm.opcionesEstadoDiligencia = [];
        }

        // Traducciones

        function cargarTraducciones() {
            $translate([
                "SOLICITADO", "COTIZADO", "APROBADO", "TRAMITE", "TERMINADO", "CONTABILIZADO", "FACTURADO"
            ]).then(function (translations) {
                i18n.SOLICITADO = translations.SOLICITADO;
                i18n.COTIZADO = translations.COTIZADO;
                i18n.APROBADO = translations.APROBADO;
                i18n.TRAMITE = translations.TRAMITE;
                i18n.TERMINADO = translations.TERMINADO;
                i18n.CONTABILIZADO = translations.CONTABILIZADO;
                i18n.FACTURADO = translations.FACTURADO;

                vm.opcionesEstadoDiligencia = [
                    {id: Constantes.EstadoDiligencia.SOLICITADO, nombre: i18n.SOLICITADO},
                    {id: Constantes.EstadoDiligencia.COTIZADO, nombre: i18n.COTIZADO},
                    {id: Constantes.EstadoDiligencia.APROBADO, nombre: i18n.APROBADO},
                    {id: Constantes.EstadoDiligencia.TRAMITE, nombre: i18n.TRAMITE},
                    {id: Constantes.EstadoDiligencia.TERMINADO, nombre: i18n.TERMINADO},
                    {id: Constantes.EstadoDiligencia.CONTABILIZADO, nombre: i18n.CONTABILIZADO},
                    {id: Constantes.EstadoDiligencia.FACTURADO, nombre: i18n.FACTURADO}
                ];
            }, function (translationIds) {});
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('diligenciaEditController', diligenciaEditController);

    diligenciaEditController.$inject = [
        '$translate',
        '$timeout',
        '$routeParams',
        'genericService',
        'sessionUtil',
        'messageUtil',
        'Upload',
        'langUtil',
        'Constantes'
    ];

    function diligenciaEditController(
        $translate, $timeout, $routeParams, genericService, sessionUtil, messageUtil, Upload, langUtil, Constantes
    ){
        var vm = this,
            i18n = {},
            recurso = "diligencias",
            diligenciaId = $routeParams.id && $routeParams.id !== "null" ? $routeParams.id : null;

        vm.entorno = sessionUtil.getParametros();
        vm.id = diligenciaId;
        vm.EstadoDiligenciaEnum = Constantes.EstadoDiligencia;
        vm.modoAcceso = $routeParams.modo;
        vm.diligenciaTramite = ($routeParams.dependiente && +$routeParams.dependiente === 1);
        vm.ModoAccesoEnum = {
            CREACION: 1,
            MODIFICACION: 2,
            CAMBIO_ESTADO: 3
        };
        if(!(vm.entorno && vm.entorno.INDICATIVO_COBRO_HABILITADO)){
            vm.deshabilitarIndicativoCobro = true;
        }

        // Acciones
        vm.guardar = guardar;
        vm.volver = volver;

        // Acciones archivos
        vm.cargarArchivo = cargarArchivo;
        vm.cargarAnexoCostos = cargarAnexoCostos;
        vm.cargarAnexoRecibido = cargarAnexoRecibido;
        vm.eliminarArchivo = eliminarArchivo;
        vm.eliminarAnexoCostos = eliminarAnexoCostos;
        vm.eliminarAnexoRecibido = eliminarAnexoRecibido;
        vm.restaurarArchivo = restaurarArchivo;
        vm.restaurarAnexoCostos = restaurarAnexoCostos;
        vm.restaurarAnexoRecibido = restaurarAnexoRecibido;
        vm.descargarArchivo = descargarArchivo;
        vm.descargarAnexoCostos = descargarAnexoCostos;
        vm.descargarAnexoRecibido = descargarAnexoRecibido;

        // Cargar traducciones
        cargarTraducciones();

        // Instanciar componentes jquery
        instanciarEventosFormatoMoneda();

        // Instanciar autocomples
        initAutocompleteDepartamento();
        initAutocompleteCiudad();
        initAutocompleteTipoDiligencia();
        initAutocompleteEmpresa();
        initAutocompleteProceso();
        initAutocompleteEstadoDiligencia();
        if(!vm.diligenciaTramite) {
            initAutocompleteUsuario();
        }

        if(diligenciaId){
            cargar();
        }else{
            vm.indicativoCobro = true;
            vm.indicativoAutorizacion = false;
            vm.deshabilitarFecha = false;
            vm.fecha = moment().format('YYYY-MM-DD');
        }

        // Acciones

        function cargar() {
            genericService.cargar(recurso, diligenciaId)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var diligencia = response.data,
                            ciudad = diligencia.ciudad,
                            departamento = diligencia.departamento,
                            tipoDiligencia = diligencia.tipo_diligencia,
                            empresa = diligencia.empresa,
                            proceso = diligencia.proceso_judicial,
                            dependiente = diligencia.dependiente;

                        vm.diligenciaOriginal = diligencia;
                        vm.numeroDiligencia = diligenciaId;
                        vm.fecha = diligencia.fecha_diligencia;
                        vm.observacionesCliente = diligencia.observaciones_cliente;
                        vm.observacionesInternas = diligencia.observaciones_internas;
                        vm.estadoDiligencia = diligencia.estado_diligencia;
                        vm.fechaCreacion = diligencia.fecha_creacion;
                        vm.archivoNombre = diligencia.archivo_anexo_diligencia;
                        vm.anexoCostosNombre = diligencia.archivo_anexo_costos;
                        vm.anexoRecibidoNombre = diligencia.archivo_anexo_recibido;
                        vm.fechaModificacion = diligencia.fecha_modificacion;
                        vm.usuarioCreacionNombre = diligencia.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = diligencia.usuario_modificacion_nombre;
                        vm.indicativoCobro = diligencia.indicativo_cobro;
                        vm.indicativoAutorizacion = diligencia.solicitud_autorizacion;
                        vm.detalleValores = diligencia.detalle_valores;

                        // Formatear valores
                        if(diligencia.valor_diligencia){
                            vm.valorDiligencia = langUtil.formatCurrency(diligencia.valor_diligencia);
                        }
                        if(diligencia.gastos_envio){
                            vm.gastoEnvio = langUtil.formatCurrency(diligencia.gastos_envio);
                        }
                        if(diligencia.otros_gastos){
                            vm.otrosGastos = langUtil.formatCurrency(diligencia.otros_gastos);
                        }
                        if(diligencia.costo_diligencia){
                            vm.costoDiligencia = langUtil.formatCurrency(diligencia.costo_diligencia);
                        }
                        if(diligencia.costo_envio){
                            vm.costoEnvio = langUtil.formatCurrency(diligencia.costo_envio);
                        }
                        if(diligencia.otros_costos){
                            vm.otrosCostos = langUtil.formatCurrency(diligencia.otros_costos);
                        }

                        // Cargar achivo
                        vm.archivoNombre = diligencia.archivo_anexo_diligencia;

                        // Autocompletes
                        if(empresa){
                            vm.empresaId = empresa.id;
                            vm.opcionesEmpresas.push({id: empresa.id, nombre: empresa.nombre});
                        }
                        if(proceso) {
                            vm.procesoId = proceso.id;
                            vm.opcionesProcesos.push({id: proceso.id, numero_proceso: proceso.numero_proceso});
                        }
                        if(departamento){
                            vm.departamentoId = departamento.id;
                            vm.opcionesDepartamentos.push({id: departamento.id, nombre: departamento.nombre});
                        }
                        if(ciudad){
                            vm.ciudadId = ciudad.id;
                            vm.opcionesCiudad.push({id: ciudad.id, nombre: ciudad.nombre});
                        }
                        if(tipoDiligencia) {
                            vm.tipoDiligenciaId = tipoDiligencia.id;
                            vm.opcionesTipoDiligencia.push({id: tipoDiligencia.id, nombre: tipoDiligencia.nombre});
                        }
                        if(dependiente){
                            vm.usuarioId = dependiente.id;
                            vm.dependienteId = dependiente.id;
                            if (!vm.diligenciaTramite) {
                                vm.opcionesUsuario.push({id: dependiente.id, nombre: dependiente.nombre});
                            }
                        }
                        if (vm.diligenciaTramite){
                            vm.deshabilitarEmpresaId = true;
                            vm.deshabilitarProcesoId = true;
                            vm.deshabilitarDepartamentoId = true;
                            vm.deshabilitarCiudadId = true;
                            vm.deshabilitarTipoDiligenciaId = true;
                            vm.deshabilitarNumeroDiligencia = true;
                            vm.deshabilitarAnexoDiligencia = false;
                            vm.deshabilitarDetalleValores = false;
                            vm.deshabilitarObservacionesCliente = true;
                        }else if(vm.modoAcceso == vm.ModoAccesoEnum.CAMBIO_ESTADO) {
                            switch (diligencia.estado_diligencia){
                                case Constantes.EstadoDiligencia.SOLICITADO:
                                    vm.deshabilitarEmpresaId = true;
                                    vm.deshabilitarProcesoId = true;
                                    vm.deshabilitarDepartamentoId = true;
                                    vm.deshabilitarCiudadId = true;
                                    vm.deshabilitarEstadoDiligencia = true;
                                    vm.deshabilitarIndicativoCobro = true;
                                    vm.deshabilitarIndicativoAutorizacion = true;
                                    vm.deshabilitarFecha = true;
                                    vm.deshabilitarTipoDiligenciaId = true;
                                    vm.deshabilitarDetalleValores = true;
                                    vm.deshabilitarUsuarioId = true;

                                    vm.estadoDiligencia = Constantes.EstadoDiligencia.COTIZADO;
                                    break;
                                case Constantes.EstadoDiligencia.COTIZADO:
                                    vm.deshabilitarEmpresaId = true;
                                    vm.deshabilitarProcesoId = true;
                                    vm.deshabilitarDepartamentoId = true;
                                    vm.deshabilitarCiudadId = true;
                                    vm.deshabilitarEstadoDiligencia = true;
                                    vm.deshabilitarIndicativoCobro = true;
                                    vm.deshabilitarIndicativoAutorizacion = true;
                                    vm.deshabilitarFecha = true;
                                    vm.deshabilitarTipoDiligenciaId = true;
                                    vm.deshabilitarValorDiligencia = true;
                                    vm.deshabilitarGastoEnvio = true;
                                    vm.deshabilitarOtrosGastos = true;
                                    vm.deshabilitarDetalleValores = true;
                                    vm.deshabilitarUsuarioId = true;

                                    vm.estadoDiligencia = Constantes.EstadoDiligencia.APROBADO;
                                    break;
                                case Constantes.EstadoDiligencia.APROBADO:
                                    vm.deshabilitarEmpresaId = true;
                                    vm.deshabilitarProcesoId = true;
                                    vm.deshabilitarDepartamentoId = true;
                                    vm.deshabilitarCiudadId = true;
                                    vm.deshabilitarEstadoDiligencia = true;
                                    vm.deshabilitarIndicativoCobro = true;
                                    vm.deshabilitarIndicativoAutorizacion = true;
                                    vm.deshabilitarFecha = true;
                                    vm.deshabilitarTipoDiligenciaId = true;
                                    vm.deshabilitarDetalleValores = true;
                                    vm.deshabilitarAnexoDiligencia = true;

                                    vm.estadoDiligencia = Constantes.EstadoDiligencia.TRAMITE;
                                    break;
                                case Constantes.EstadoDiligencia.TRAMITE:
                                    vm.deshabilitarEmpresaId = true;
                                    vm.deshabilitarProcesoId = true;
                                    vm.deshabilitarDepartamentoId = true;
                                    vm.deshabilitarCiudadId = true;
                                    vm.deshabilitarEstadoDiligencia = true;
                                    vm.deshabilitarIndicativoCobro = true;
                                    vm.deshabilitarIndicativoAutorizacion = true;
                                    vm.deshabilitarFecha = true;
                                    vm.deshabilitarTipoDiligenciaId = true;
                                    vm.deshabilitarAnexoDiligencia = true;
                                    vm.deshabilitarUsuarioId = true;
                                    vm.mostrarAnexos = true;

                                    vm.estadoDiligencia = Constantes.EstadoDiligencia.TERMINADO;
                                    break;
                                case Constantes.EstadoDiligencia.TERMINADO:
                                    vm.deshabilitarEmpresaId = true;
                                    vm.deshabilitarProcesoId = true;
                                    vm.deshabilitarDepartamentoId = true;
                                    vm.deshabilitarCiudadId = true;
                                    vm.deshabilitarEstadoDiligencia = true;
                                    vm.deshabilitarIndicativoCobro = true;
                                    vm.deshabilitarIndicativoAutorizacion = true;
                                    vm.deshabilitarFecha = true;
                                    vm.deshabilitarTipoDiligenciaId = true;
                                    vm.deshabilitarAnexoDiligencia = true;
                                    vm.deshabilitarUsuarioId = true;
                                    vm.mostrarAnexos = true;

                                    vm.estadoDiligencia = Constantes.EstadoDiligencia.CONTABILIZADO;
                                    break;
                                case Constantes.EstadoDiligencia.CONTABILIZADO:
                                    vm.deshabilitarEmpresaId = true;
                                    vm.deshabilitarProcesoId = true;
                                    vm.deshabilitarDepartamentoId = true;
                                    vm.deshabilitarCiudadId = true;
                                    vm.deshabilitarEstadoDiligencia = true;
                                    vm.deshabilitarIndicativoCobro = true;
                                    vm.deshabilitarIndicativoAutorizacion = true;
                                    vm.deshabilitarFecha = true;
                                    vm.deshabilitarTipoDiligenciaId = true;
                                    vm.deshabilitarAnexoDiligencia = true;
                                    vm.deshabilitarUsuarioId = true;
                                    vm.mostrarAnexos = true;

                                    vm.estadoDiligencia = Constantes.EstadoDiligencia.FACTURADO;
                                    break;
                                default:
                                    vm.deshabilitarEmpresaId = true;
                                    vm.deshabilitarProcesoId = true;
                                    vm.deshabilitarDepartamentoId = true;
                                    vm.deshabilitarCiudadId = true;
                                    vm.deshabilitarEstadoDiligencia = true;
                                    vm.deshabilitarIndicativoCobro = true;
                                    vm.deshabilitarIndicativoAutorizacion = true;
                                    vm.deshabilitarFecha = true;
                                    vm.deshabilitarTipoDiligenciaId = true;
                                    vm.deshabilitarValorDiligencia = true;
                                    vm.deshabilitarGastoEnvio = true;
                                    vm.deshabilitarOtrosGastos = true;
                                    vm.deshabilitarDetalleValores = true;
                                    vm.deshabilitarObservacionesCliente = true;
                                    vm.deshabilitarObservacionesInternas = true;
                                    vm.deshabilitarAnexoDiligencia = true;
                                    break;
                            }
                        }
                    }
                });
        }

        function guardar() {
            // Validaciones
            var conErrores = false,
                valorDiligencia = vm.valorDiligencia && vm.valorDiligencia.trim() !== "" ?
                    Number(vm.valorDiligencia.replace(/[^0-9.-]+/g,"")) : null,
                gastoEnvio = vm.gastoEnvio && vm.gastoEnvio.trim() !== "" ?
                    Number(vm.gastoEnvio.replace(/[^0-9.-]+/g,"")) : null,
                otrosGastos = vm.otrosGastos && vm.otrosGastos.trim() !== "" ?
                    Number(vm.otrosGastos.replace(/[^0-9.-]+/g,"")) : null,
                costoDiligencia = vm.costoDiligencia && vm.costoDiligencia.trim() !== "" ?
                    Number(vm.costoDiligencia.replace(/[^0-9.-]+/g,"")) : null,
                costoEnvio = vm.costoEnvio && vm.costoEnvio.trim() !== "" ?
                    Number(vm.costoEnvio.replace(/[^0-9.-]+/g,"")) : null,
                otrosCostos = vm.otrosCostos && vm.otrosCostos.trim() !== "" ?
                    Number(vm.otrosCostos.replace(/[^0-9.-]+/g,"")) : null;
            if(
                (!valorDiligencia || valorDiligencia === 0) && vm.estadoDiligencia &&
                vm.estadoDiligencia != Constantes.EstadoDiligencia.SOLICITADO
            ){
                conErrores = true;
                vm.valorDiligenicaInvalid = true;
            }

            vm.guardando = true;
            if(!conErrores && vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;
                console.log(vm.dependienteId);
                var datos = {
                    id: diligenciaId,
                    empresa_id: vm.empresaId,
                    proceso_id: vm.procesoId,
                    fecha_diligencia: vm.fecha,
                    tipo_diligencia_id: vm.tipoDiligenciaId,
                    departamento_id: vm.departamentoId,
                    ciudad_id: vm.ciudadId,
                    dependiente_id: vm.usuarioId ? vm.usuarioId : vm.dependienteId ? vm.dependienteId : null,
                    valor_diligencia: valorDiligencia,
                    gastos_envio: gastoEnvio,
                    otros_gastos: otrosGastos,
                    costo_diligencia: costoDiligencia,
                    costo_envio: costoEnvio,
                    otros_costos: otrosCostos,
                    estado_diligencia: vm.estadoDiligencia,
                    indicativo_cobro: vm.indicativoCobro,
                    solicitud_autorizacion: vm.indicativoAutorizacion,
                    observaciones_cliente: vm.observacionesCliente,
                    observaciones_internas: vm.observacionesInternas,
                    detalle_valores: vm.detalleValores,
                    base_64_anexo_diligencia: vm.archivoBase64,
                    base_64_anexo_costos: vm.anexoCostosBase64,
                    base_64_anexo_recibido: vm.anexoRecibidoBase64,
                    nombre_anexo_diligencia: vm.archivoNombre,
                    nombre_anexo_costos: vm.anexoCostosNombre,
                    nombre_anexo_recibido: vm.anexoRecibidoNombre,
                    eliminar_anexo_diligenica: !!vm.archivoEliminado,
                    eliminar_anexo_costos: !!vm.anexoCostosEliminado,
                    eliminar_anexo_recibido: !!vm.anexoRecibidoEliminado,
                    diligencia_tramite: vm.diligenciaTramite
                };

                var promesa;
                if(!(diligenciaId)){
                    promesa = genericService.crear(recurso, datos);
                }else{
                    promesa = genericService.modificar(recurso, datos);
                }

                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        messageUtil.success(response.data.mensajes[0]);
                        volver();
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }
                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function volver() {
            if(vm.diligenciaTramite){
                window.location = "#/diligencias-tramite?dependiente=1";
            }else {
                window.location = "#/diligencias";
            }
        }

        // Acciones archivos

        function cargarArchivo($archivo) {
            Upload.dataUrl($archivo, true).then(function(base64) {
                if(base64){
                    vm.archivoEliminado = false;
                    vm.archivo = $archivo;
                    vm.archivoNombre = $archivo.name;
                    vm.archivoBase64 = base64;
                }
            });
        }

        function cargarAnexoCostos($anexoCostos) {
            Upload.dataUrl($anexoCostos, true).then(function(base64) {
                if(base64){
                    vm.anexoCostosEliminado = false;
                    vm.anexoCostos = $anexoCostos;
                    vm.anexoCostosNombre = $anexoCostos.name;
                    vm.anexoCostosBase64 = base64;
                }
            });
        }

        function cargarAnexoRecibido($anexoRecibido) {
            Upload.dataUrl($anexoRecibido, true).then(function(base64) {
                if(base64){
                    vm.anexoRecibidoEliminado = false;
                    vm.anexoRecibido = $anexoRecibido;
                    vm.anexoRecibidoNombre = $anexoRecibido.name;
                    vm.anexoRecibidoBase64 = base64;
                }
            });
        }

        function descargarArchivo() {
            if(diligenciaId && !vm.archivoBase64){
                genericService.obtenerArchivo(recurso + "/" + diligenciaId + "/anexo-diligencia")
                    .then(function (response) {
                        if(response.status === Constantes.Response.HTTP_NOT_FOUND){
                            messageUtil.error("No se encontro un archivo anexo.");
                        }else if(response.status === Constantes.Response.HTTP_FORBIDDEN){
                            messageUtil.error("No tiene permitido descargar este archivo.");
                        }
                    });
            }else{
                var a = document.createElement("a");
                document.body.appendChild(a);
                a.style = "display: none";
                var blob = Upload.dataUrltoBlob(vm.archivoBase64, vm.archivoNombre),
                    url = window.URL.createObjectURL(blob);
                a.href = url;
                a.download = vm.archivoNombre;
                a.click();
                window.URL.revokeObjectURL(url);
            }
        }

        function descargarAnexoCostos() {
            genericService.obtenerArchivo(recurso + "/" + diligenciaId + "/anexo-costos")
                .then(function (response) {
                    if(response.status === Constantes.Response.HTTP_NOT_FOUND){
                        messageUtil.error("No se encontro un archivo anexo.");
                    }else if(response.status === Constantes.Response.HTTP_FORBIDDEN){
                        messageUtil.error("No tiene permitido descargar este archivo.");
                    }
                });
        }

        function descargarAnexoRecibido() {
            genericService.obtenerArchivo(recurso + "/" + diligenciaId + "/anexo-recibido")
                .then(function (response) {
                    if(response.status === Constantes.Response.HTTP_NOT_FOUND){
                        messageUtil.error("No se encontro un archivo anexo.");
                    }else if(response.status === Constantes.Response.HTTP_FORBIDDEN){
                        messageUtil.error("No tiene permitido descargar este archivo.");
                    }
                });
        }

        function eliminarArchivo() {
            if(diligenciaId && !vm.archivoBase64){
                vm.archivoEliminado = true;
            }else{
                vm.archivo = null;
                vm.archivoNombre = null;
                vm.archivoBase64 = null;
            }
        }

        function eliminarAnexoCostos() {
            if(diligenciaId && !vm.anexoCostosBase64){
                vm.anexoCostosEliminado = true;
            }else{
                vm.anexoCostos = null;
                vm.anexoCostosNombre = null;
                vm.anexoCostosBase64 = null;
            }
        }

        function eliminarAnexoRecibido() {
            if(diligenciaId && !vm.anexoRecibidoBase64){
                vm.anexoRecibidoEliminado = true;
            }else{
                vm.anexoRecibido = null;
                vm.anexoRecibidoNombre = null;
                vm.anexoRecibidoBase64 = null;
            }
        }

        function restaurarArchivo(attachment) {
            vm.archivoEliminado = false;
        }

        function restaurarAnexoCostos(attachment) {
            vm.anexoCostosEliminado = false;
        }

        function restaurarAnexoRecibido(attachment) {
            vm.anexoRecibidoEliminado = false;
        }

        // Instanciar autocompletes

        function initAutocompleteEmpresa() {
            vm.configEmpresa = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                onItemAdd: function (value, $item) {
                    if(!vm.cargandoProcesos){
                        vm.cargandoProcesos = true;
                        vm.procesoId = null;
                        vm.opcionesProcesos = [];
                        $timeout(function () {
                            vm.limpiarOpcionesProcesos = true;
                            genericService.obtenerColeccionLigera("procesos-judiciales", {
                                ligera: true,
                                empresa_cliente: value,
                                limite: 100
                            })
                            .then(function (response){
                                var procesos = response.data;
                                if(vm.diligenciaOriginal && vm.diligenciaOriginal.proceso_judicial) {
                                    vm.procesoId = vm.diligenciaOriginal.proceso_judicial.id;
                                    procesos.push({
                                        id: vm.diligenciaOriginal.proceso_judicial.id,
                                        numero_proceso: vm.diligenciaOriginal.proceso_judicial.numero_proceso
                                    });
                                }
                                vm.opcionesProcesos = [].concat(procesos);
                            }).finally(function () {
                                vm.cargandoProcesos = false;
                            });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.opcionesProcesos = [];
                    vm.procesoId = null;
                    $timeout(function () {
                        vm.limpiarOpcionesProcesos = true;
                    }, 100);
                }
            };

            vm.opcionesEmpresas = [];
            genericService.obtenerColeccionLigera("empresas",{
                ligera: true
            })
            .then(function (response){
                vm.opcionesEmpresas = [].concat(response.data);
            });
        }

        function initAutocompleteProceso() {
            vm.configProceso = {
                valueField: 'id',
                labelField: 'numero_proceso',
                searchField: ['numero_proceso'],
                sortField: 'numero_proceso',
                allowEmptyOption: false,
                create: false,
                persist: true,
                lock: false,
                load: function(query, callback) {
                    if (!query.length) return callback();
                    genericService.obtenerColeccionLigera("procesos-judiciales", {
                        q: query,
                        ligera: true,
                        limite: 100
                    }).then(function(response){
                        callback(response.data);
                        // Validar so hay datos
                        if(!response.data || response.data.length === 0){
                            vm.message = "No se encuentra en la lista";
                        }
                    }).catch(function(){
                        callback();
                    });
                },
                onItemAdd: function (value, $item) {
                    if(!vm.cargandoInfo) {
                        vm.cargandoInfo = true;
                        if (!diligenciaId) {
                            var opcionesDepartamento = vm.opcionesDepartamentos;
                            vm.departamentoId = null;
                            vm.opcionesDepartamentos = [];
                        }
                        $timeout(function (){
                            vm.limpiarOpcionesDepartamento = true;
                            genericService.cargar("procesos-judiciales", value)
                                .then(function (response) {
                                    vm.proceso = response.data;
                                    if (!diligenciaId) {
                                        var departamento = response.data.departamento,
                                            ciudad = response.data.ciudad;
                                        vm.opcionesDepartamentos = opcionesDepartamento || [];
                                        vm.opcionesDepartamentos.push({id: departamento.id, nombre: departamento.nombre});

                                        vm.ciudadIdActual = ciudad.id;
                                        vm.departamentoId = departamento.id;
                                    }
                                }).finally(function () {
                                    vm.cargandoInfo = false;
                                });
                        }, 100);
                    }
                },
                onItemRemove: function () {}
            };
            vm.opcionesProcesos = [];
        }

        function initAutocompleteTipoDiligencia() {
            vm.configTipoDiligencia = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                onItemAdd: function (value, $item) {
                    if(!diligenciaId && vm.departamentoId && vm.ciudadId && !vm.cargandoTarifas){
                        vm.cargandoTarifas = true;
                        genericService.obtener('tarifas-diligencias',{
                            tipo_diligencia_id: value,
                            departamento_id: vm.departamentoId,
                            ciudad_id: vm.ciudadId,
                            obtener: true
                        })
                        .then(function(response){
                            if(response.status === Constantes.Response.HTTP_OK) {
                                var datos = response.data;
                                if(datos && datos.length > 0 && datos[0].valor_diligencia){
                                    vm.valorDiligencia = langUtil.formatCurrency(datos[0].valor_diligencia);
                                }
                                if(datos && datos.length > 0 && datos[0].gasto_envio){
                                    vm.gastoEnvio = langUtil.formatCurrency(datos[0].gasto_envio);
                                }
                                if(datos && datos.length > 0 && datos[0].otros_gastos){
                                    vm.otrosGastos = langUtil.formatCurrency(datos[0].otros_gastos);
                                }
                            }
                        }).finally(function () {
                            vm.cargandoTarifas = false;
                        });
                    }

                    if(!diligenciaId && !vm.cargandoTipoDiligencia){
                        vm.cargandoTipoDiligencia = true;
                        genericService.cargar('tipos-diligencias', value)
                            .then(function(response){
                                if(response.status === Constantes.Response.HTTP_OK) {
                                    vm.indicativoAutorizacion = response.data.solicitud_autorizacion;
                                }
                            }).finally(function () {
                                vm.cargandoTipoDiligencia = false;
                            });
                    }
                }
            };

            vm.opcionesTipoDiligencia = [];
            genericService.obtenerColeccionLigera("tipos-diligencias",{
                ligera: true,
            })
            .then(function (response){
                vm.opcionesTipoDiligencia = [].concat(response.data);
            });
        }

        function initAutocompleteEstadoDiligencia() {
            vm.configEstadoDiligencia = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'id',
                allowEmptyOption: false,
                create: false,
                persist: true,
                lock: false
            };

            if(diligenciaId){
                vm.opcionesEstadoDiligencia = [
                    {id: Constantes.EstadoDiligencia.SOLICITADO, nombre: "Solicitado"},
                    {id: Constantes.EstadoDiligencia.COTIZADO, nombre: "Cotizado"},
                    {id: Constantes.EstadoDiligencia.APROBADO, nombre: "Aprobado"},
                    {id: Constantes.EstadoDiligencia.TRAMITE, nombre: "Trámite"},
                    {id: Constantes.EstadoDiligencia.TERMINADO, nombre: "Terminado"},
                    {id: Constantes.EstadoDiligencia.CONTABILIZADO, nombre: "Contabilizado"},
                    {id: Constantes.EstadoDiligencia.FACTURADO, nombre: "Facturado"}
                ];
            }else{
                vm.opcionesEstadoDiligencia = [
                    {id: Constantes.EstadoDiligencia.SOLICITADO, nombre: "Solicitado"},
                    {id: Constantes.EstadoDiligencia.COTIZADO, nombre: "Cotizado"},
                    {id: Constantes.EstadoDiligencia.APROBADO, nombre: "Aprobado"}
                ];
            }
        }

        function initAutocompleteDepartamento() {
            vm.configDepartamento = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                onItemAdd: function (value, $item) {
                    if(!vm.cargandoCiudades){
                        vm.cargandoCiudades = true;
                        vm.ciudadId = null;
                        vm.opcionesCiudad = [];
                        $timeout(function () {
                            vm.limpiarOpcionesCiudad = true;
                            genericService.obtenerColeccionLigera("ciudades", {
                                ligera: true,
                                departamento_id: value
                            })
                            .then(function (response){
                                vm.opcionesCiudad = [].concat(response.data);
                                if(vm.ciudadIdActual){
                                    vm.ciudadId = vm.ciudadIdActual;
                                    vm.ciudadIdActual = null;
                                }
                            }).finally(function () {
                                vm.cargandoCiudades = false;
                            });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.opcionesCiudad = [];
                    vm.ciudadId = null;
                    $timeout(function () {
                        vm.limpiarOpcionesCiudad = true;
                    }, 100);
                }
            };
            vm.opcionesDepartamentos = [];
            genericService.obtenerColeccionLigera("departamentos",{
                ligera: true,
            })
            .then(function (response){
                vm.opcionesDepartamentos = [].concat(response.data);
            });
        }

        function initAutocompleteCiudad() {
            vm.configCiudad = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                lock: false,
                onItemAdd: function (value, $item) {
                    if(
                        !(vm.id && vm.usuarioId) &&
                        (vm.estadoDiligencia == Constantes.EstadoDiligencia.TRAMITE && !vm.diligenciaTramite) &&
                        !vm.cargandoUsuarios
                    ){
                        vm.cargandoUsuarios = true;
                        vm.usuarioId = null;
                        vm.opcionesUsuario = [];
                        $timeout(function () {
                            vm.limpiarOpcionesUsuario = true;
                            genericService.obtenerColeccionLigera("usuarios",{
                                ligera: true,
                                es_dependiente: true,
                                ciudad_id: value
                            })
                            .then(function (response){
                                vm.opcionesUsuario = [].concat(response.data);
                            });
                            genericService.obtener("usuarios/dependiente-principal")
                            .then(function (response) {
                                let dependientePrincipal = response.data[0];
                                vm.usuarioId = dependientePrincipal.id;
                                vm.opcionesUsuario.push({
                                    id: dependientePrincipal.id,
                                    nombre: dependientePrincipal.nombre
                                });
                            }).finally(function () {
                                vm.cargandoUsuarios = false;
                            });
                        }, 100);
                    }
               },
                onItemRemove: function () {
                    vm.opcionesUsuario = [];
                    vm.usuarioId = null;
                    $timeout(function () {
                        vm.limpiarOpcionesUsuario = true;
                    }, 100);
                }
            };
            vm.opcionesCiudad = [];
        }

        function initAutocompleteUsuario() {
            vm.configUsuario = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
            };
            vm.opcionesUsuario = [];
        }

        // Instanciar componentes jquery

        function instanciarEventosFormatoMoneda() {
            $("input[data-type='currency']").on({
                keyup: function() {
                    langUtil.jqueryFormatCurrency($(this));
                },
                blur: function() {
                    langUtil.jqueryFormatCurrency($(this), "blur");
                }
            });
        }

        // Traducciones

        function cargarTraducciones() {
            $translate([
                "SOLICITADO", "COTIZADO", "APROBADO", "TRAMITE", "TERMINADO", "CONTABILIZADO", "FACTURADO"
            ]).then(function (translations) {
                i18n.SOLICITADO = translations.SOLICITADO;
                i18n.COTIZADO = translations.COTIZADO;
                i18n.APROBADO = translations.APROBADO;
                i18n.TRAMITE = translations.TRAMITE;
                i18n.TERMINADO = translations.TERMINADO;
                i18n.CONTABILIZADO = translations.CONTABILIZADO;
                i18n.FACTURADO = translations.FACTURADO;
            }, function (translationIds) {});
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('diligenciaHistoriaController', diligenciaHistoriaController);

    diligenciaHistoriaController.$inject = [
        '$translate',
        '$routeParams',
        '$scope',
        '$timeout',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function diligenciaHistoriaController(
        $translate, $routeParams, $scope, $timeout, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            i18n = {},
            pipePromise,
            recurso = "diligencias-historia",
            diligenciaId = $routeParams.id;

        vm.consulta = $routeParams.consulta && $routeParams.consulta !== "null" ? $routeParams.consulta : null;

        vm.diligencia = [];
        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.EstadoDiligenciaEnum = Constantes.EstadoDiligencia;
        vm.activarBuscar = true;

        // Control de grid
        vm.obtenerResumen = obtenerResumen;

        // Modulos externos
        vm.confirmarEliminar = confirmarEliminar;

        // Cargar traducciones
        cargarTraducciones();

        // Cargar autocompletes
        initAutocompleteEstadoDiligencia();

        // Grid

        function obtenerResumen(){
            genericService.cargar('diligencias', diligenciaId)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        vm.diligencia = response.data;
                        vm.coleccion = vm.diligencia.diligencia_historia;
                    }
                }).finally(function () {
                    vm.cargando = false;
                });

        }

        // Acciones

        function eliminar(row){
            genericService.eliminar(recurso, row.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        $scope.$broadcast('refreshGrid');
                        location.reload();
                        messageUtil.success(response.data.mensajes[0]);
                    }else if(
                        response.status === Constantes.Response.HTTP_CONFLICT ||
                        response.status === Constantes.Response.HTTP_BAD_REQUEST
                    ){
                        messageUtil.error(response.data.mensajes[0]);
                    }
                });
        }

        function confirmarEliminar(row) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + 'confirmar-template',
                controller: 'confirmarController as vm',
                size: 'md',
                resolve: {
                    parametros: function () {
                        return {
                            mensaje: "¿Estás seguro de que quieres eliminar la tarifa?"
                        };
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(response.ok){
                    eliminar(row);
                }
            });
        }

        function initAutocompleteEstadoDiligencia() {
            vm.configEstadoDiligencia = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true
            };
            vm.opcionesEstadoDiligencia = [];
        }

        // Traducciones

        function cargarTraducciones() {
            $translate([
                "SOLICITADO", "COTIZADO", "APROBADO"
            ]).then(function (translations) {
                i18n.SOLICITADO = translations.SOLICITADO;
                i18n.COTIZADO = translations.COTIZADO;
                i18n.APROBADO = translations.APROBADO;

                vm.opcionesEstadoDiligencia = [
                    {id: Constantes.EstadoDiligencia.SOLICITADO, nombre: i18n.SOLICITADO},
                    {id: Constantes.EstadoDiligencia.COTIZADO, nombre: i18n.COTIZADO},
                    {id: Constantes.EstadoDiligencia.APROBADO, nombre: i18n.APROBADO}
                ];
            }, function (translationIds) {});
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('diligenciaHistoriaEditController', diligenciaHistoriaEditController);

    diligenciaHistoriaEditController.$inject = [
        '$translate',
        '$timeout',
        '$routeParams',
        'genericService',
        'messageUtil',
        'Upload',
        'langUtil',
        'Constantes'
    ];

    function diligenciaHistoriaEditController(
        $translate, $timeout, $routeParams, genericService, messageUtil, Upload, langUtil, Constantes
    ){
        var vm = this,
            diligenciaId = $routeParams.diligencia_id,
            diligenciaHistoriaId = $routeParams.id;

        vm.EstadoDiligenciaEnum = Constantes.EstadoDiligencia;

        // Acciones
        vm.volver = volver;

        // Acciones archivos
        vm.descargarArchivoDiligencia = descargarArchivoDiligencia;
        vm.descargarArchivoCostos = descargarArchivoCostos;
        vm.descargarArchivoRecibido = descargarArchivoRecibido;

        // Carga inicial
        cargar();

        // Acciones

        function cargar() {
            genericService.cargar('diligencias/' + diligenciaId + '/historia', diligenciaHistoriaId)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var diligenciaHistoria = response.data,
                            ciudad = diligenciaHistoria.ciudad,
                            departamento = diligenciaHistoria.departamento,
                            tipoDiligencia = diligenciaHistoria.tipo_diligencia,
                            empresa = diligenciaHistoria.empresa,
                            proceso = diligenciaHistoria.proceso_judicial;

                        vm.numeroDiligencia = diligenciaId;
                        vm.secuencia = diligenciaHistoria.secuencia;
                        vm.empresa = empresa.nombre;
                        vm.numeroProceso = proceso.numero_proceso;
                        vm.claseProceso = proceso.clase_proceso;
                        vm.demandante = proceso.demandante;
                        vm.demandado = proceso.demandado;
                        vm.juzgado = proceso.juzgado;
                        vm.departamento = departamento.nombre;
                        vm.ciudad = ciudad.nombre;
                        vm.fechaDiligencia = diligenciaHistoria.fecha_diligencia;
                        vm.tipoDiligencia = tipoDiligencia.nombre;
                        vm.estadoDiligencia = diligenciaHistoria.estado_diligencia;
                        vm.fechaModificacion = diligenciaHistoria.fecha_modificacion;
                        vm.solicitudAutorizacion = diligenciaHistoria.solicitud_autorizacion;
                        vm.indicativoCobro = diligenciaHistoria.indicativo_cobro;
                        vm.observacionesCliente = diligenciaHistoria.observaciones_cliente;
                        vm.observacionesInternas = diligenciaHistoria.observaciones_internas;
                        vm.fechaCreacion = diligenciaHistoria.fecha_creacion;
                        vm.usuarioCreacionNombre = diligenciaHistoria.usuario_creacion_nombre;
                        vm.usuarioModificacionNombre = diligenciaHistoria.usuario_modificacion_nombre;

                        vm.valorDiligencia = diligenciaHistoria.valor_diligencia ? diligenciaHistoria.valor_diligencia : 0;
                        vm.gastoEnvio = diligenciaHistoria.gastos_envio ? diligenciaHistoria.gastos_envio : 0;
                        vm.otrosGastos = diligenciaHistoria.otros_gastos ? diligenciaHistoria.otros_gastos : 0;
                        vm.costoDiligencia = diligenciaHistoria.costo_diligencia ? diligenciaHistoria.costo_diligencia : 0;
                        vm.costoEnvio = diligenciaHistoria.costo_envio ? diligenciaHistoria.costo_envio : 0;
                        vm.otrosCostos = diligenciaHistoria.otros_costos ? diligenciaHistoria.otros_costos : 0;

                        // Cargar achivo
                        vm.archivoDiligencia = diligenciaHistoria.archivo_anexo_diligencia;
                        vm.archivoCostos = diligenciaHistoria.archivo_anexo_costos;
                        vm.archivoRecibido = diligenciaHistoria.archivo_anexo_recibido;
                    }
                });
        }

        function volver() {
            window.history.back();
        }

        // Acciones archivos

        function descargarArchivoDiligencia() {
            genericService.obtenerArchivo('diligencias/' + diligenciaId + '/historia/' + diligenciaHistoriaId + "/anexo-diligencia")
            .then(function (response) {
                if(response.status === Constantes.Response.HTTP_NOT_FOUND){
                    messageUtil.error("No se encontro un archivo anexo.");
                }else if(response.status === Constantes.Response.HTTP_FORBIDDEN){
                    messageUtil.error("No tiene permitido descargar este archivo.");
                }
            });
        }

        function descargarArchivoCostos() {
            genericService.obtenerArchivo('diligencias/' + diligenciaId + '/historia/' + diligenciaHistoriaId + "/anexo-costos")
            .then(function (response) {
                if(response.status === Constantes.Response.HTTP_NOT_FOUND){
                    messageUtil.error("No se encontro un archivo anexo.");
                }else if(response.status === Constantes.Response.HTTP_FORBIDDEN){
                    messageUtil.error("No tiene permitido descargar este archivo.");
                }
            });
        }

        function descargarArchivoRecibido() {
            genericService.obtenerArchivo('diligencias/' + diligenciaId + '/historia/' + diligenciaHistoriaId + "/anexo-recibido")
            .then(function (response) {
                if(response.status === Constantes.Response.HTTP_NOT_FOUND){
                    messageUtil.error("No se encontro un archivo anexo.");
                }else if(response.status === Constantes.Response.HTTP_FORBIDDEN){
                    messageUtil.error("No tiene permitido descargar este archivo.");
                }
            });
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('diligenciaClienteController', diligenciaClienteController);

    diligenciaClienteController.$inject = [
        '$translate',
        '$scope',
        '$timeout',
        '$routeParams',
        '$uibModal',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function diligenciaClienteController(
        $translate, $scope, $timeout, $routeParams, $uibModal, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            i18n = {},
            pipePromise,
            recurso = "diligencias";

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 15;
        $scope.isReset = true;
        vm.EstadoDiligenciaEnum = Constantes.EstadoDiligencia;
        vm.activarBuscar = true;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Modulos externos
        vm.confirmarEliminar = confirmarEliminar;

        //Acciones
        vm.buscar = buscar;
        vm.obtenerExcel = obtenerConsultaExcel;


        // Acciones archivos
        vm.descargarArchivo = descargarArchivo;
        vm.descargarAnexoCostos = descargarAnexoCostos;
        vm.descargarAnexoRecibido = descargarAnexoRecibido;

        // Limpiar filtros
        limpiarFiltros();

        // Cargar traducciones
        cargarTraducciones();

        // Cargar autocompletes
        initAutocompleteEstadoDiligencia();

        // Grid

        function buscar() {
            vm.buscando = true;
            if(vm.form.$valid) {
                vm.activarBuscar = true;
                $scope.$broadcast('refreshGrid');
            }
        }

        function obtenerResumen(tableState, ctrl) {
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }

            if(vm.paginacion && vm.paginacion.paginaActual && tableState.pagination){
                var start = tableState.pagination.start || 0,
                    number = tableState.pagination.number || 10,
                    nextPage = (start + number) / number;
                if(vm.paginacion.paginaActual != nextPage){
                    vm.activarBuscar = true;
                }else if(vm.activarBuscar){
                    tableState.pagination.start = 0;
                    tableState.pagination.number = vm.itemsPorPagina;
                }
            }

            if(vm.activarBuscar && !vm.cargando){
                vm.cargando = true;
                genericService.obtenerColeccion(recurso + '/consulta-cliente', tableState).then(function (response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var coleccion = response.data.datos;
                        if(coleccion && coleccion.length > 0 ){
                            vm.coleccion = [].concat(coleccion);
                            vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                            vm.paginacion.paginaActual = response.data.pagina_actual;
                            vm.paginacion.desde = response.data.desde;
                            vm.paginacion.hasta = response.data.hasta;
                            vm.paginacion.total = response.data.total;
                            tableState.pagination.numberOfPages = response.data.ultima_pagina;
                        }else{
                            vm.coleccion = [];
                        }
                    }
                }).finally(function () {
                    vm.buscando = false;
                    vm.cargando = false;
                    vm.activarBuscar = false;
                });
            }
        }

        function obtenerConsultaExcel() {
            genericService.obtenerArchivo(recurso + "/consulta-cliente/excel", {
                numero_diligencia: vm.numeroDiligencia,
                numero_proceso: vm.proceso,
                estado_diligencia: vm.filterEstado,
                fecha_inicial: vm.filterFechaInicial,
                fecha_final: vm.filterFechaFinal,
            });
        }

        function limpiarFiltros(){
            vm.numeroDiligencia = null;
            vm.proceso = null;
            vm.filtroEstado = null;
            vm.filtroFechaInicial = null;
            vm.filtroFechaFinal = null;
            vm.activarBuscar = true;
            $scope.isReset = true;

        }

        /*function cargarFiltros() {
            var filtros = gridUtil.getFormattedSearchPredicate("diligencia");
            vm.nombre = filtros && filtros.nombre ? filtros.nombre : null;
        }*/

        // Acciones

        function eliminar(row){
            genericService.eliminar(recurso, row.id)
                .then(function(response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        $scope.$broadcast('refreshGrid');
                        location.reload();
                        messageUtil.success(response.data.mensajes[0]);
                    }else if(
                        response.status === Constantes.Response.HTTP_CONFLICT ||
                        response.status === Constantes.Response.HTTP_BAD_REQUEST
                    ){
                        messageUtil.error(response.data.mensajes[0]);
                    }
                });
        }

        function confirmarEliminar(row) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: envService.read('apiUrl') + 'confirmar-template',
                controller: 'confirmarController as vm',
                size: 'md',
                resolve: {
                    parametros: function () {
                        return {
                            mensaje: "¿Estás seguro de que quieres eliminar la tarifa?"
                        };
                    }
                }
            });
            modalInstance.result.then(function (response) {
                if(response.ok){
                    eliminar(row);
                }
            });
        }

        function descargarArchivo(row) {
            genericService.obtenerArchivo(recurso + "/" + row.id + "/anexo-diligencia")
                .then(function (response) {
                    if(response.status === Constantes.Response.HTTP_NOT_FOUND){
                        messageUtil.error("No se encontro un archivo anexo.");
                    }else if(response.status === Constantes.Response.HTTP_FORBIDDEN){
                        messageUtil.error("No tiene permitido descargar este archivo.");
                    }
                });
        }

        function descargarAnexoCostos(row) {
            genericService.obtenerArchivo(recurso + "/" + row.id + "/anexo-costos")
                .then(function (response) {
                    if(response.status === Constantes.Response.HTTP_NOT_FOUND){
                        messageUtil.error("No se encontro un archivo anexo.");
                    }else if(response.status === Constantes.Response.HTTP_FORBIDDEN){
                        messageUtil.error("No tiene permitido descargar este archivo.");
                    }
                });
        }

        function descargarAnexoRecibido(row) {
            genericService.obtenerArchivo(recurso + "/" + row.id + "/anexo-recibido")
                .then(function (response) {
                    if(response.status === Constantes.Response.HTTP_NOT_FOUND){
                        messageUtil.error("No se encontro un archivo anexo.");
                    }else if(response.status === Constantes.Response.HTTP_FORBIDDEN){
                        messageUtil.error("No tiene permitido descargar este archivo.");
                    }
                });
        }

        function initAutocompleteEstadoDiligencia() {
            vm.configEstadoDiligencia = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'id',
                allowEmptyOption: false,
                create: false,
                persist: true
            };
            vm.opcionesEstadoDiligencia = [];
        }

        // Traducciones

        function cargarTraducciones() {
            $translate([
                "SOLICITADO", "COTIZADO", "APROBADO", "TRAMITE", "TERMINADO", "CONTABILIZADO", "FACTURADO"
            ]).then(function (translations) {
                i18n.SOLICITADO = translations.SOLICITADO;
                i18n.COTIZADO = translations.COTIZADO;
                i18n.APROBADO = translations.APROBADO;
                i18n.TRAMITE = translations.TRAMITE;
                i18n.TERMINADO = translations.TERMINADO;
                i18n.CONTABILIZADO = translations.CONTABILIZADO;
                i18n.FACTURADO = translations.FACTURADO;

                vm.opcionesEstadoDiligencia = [
                    {id: Constantes.EstadoDiligencia.SOLICITADO, nombre: i18n.SOLICITADO},
                    {id: Constantes.EstadoDiligencia.COTIZADO, nombre: i18n.COTIZADO},
                    {id: Constantes.EstadoDiligencia.APROBADO, nombre: i18n.APROBADO},
                    {id: Constantes.EstadoDiligencia.TRAMITE, nombre: i18n.TRAMITE},
                    {id: Constantes.EstadoDiligencia.TERMINADO, nombre: i18n.TERMINADO},
                    {id: Constantes.EstadoDiligencia.CONTABILIZADO, nombre: i18n.CONTABILIZADO},
                    {id: Constantes.EstadoDiligencia.FACTURADO, nombre: i18n.FACTURADO}
                ];
            }, function (translationIds) {});
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('diligenciaClienteEditController', diligenciaClienteEditController);

    diligenciaClienteEditController.$inject = [
        '$translate',
        '$timeout',
        '$routeParams',
        'genericService',
        'sessionUtil',
        'messageUtil',
        'Upload',
        'langUtil',
        'Constantes'
    ];

    function diligenciaClienteEditController(
        $translate, $timeout, $routeParams, genericService, sessionUtil, messageUtil, Upload, langUtil, Constantes
    ){
        var vm = this,
            i18n = {},
            recurso = "diligencias";

        vm.entorno = sessionUtil.getParametros();
        vm.empresa = sessionUtil.getEmpresa();
        vm.EstadoDiligenciaEnum = Constantes.EstadoDiligencia;

        // Acciones
        vm.guardar = guardar;
        vm.volver = volver;

        // Acciones archivos
        vm.cargarArchivo = cargarArchivo;
        vm.eliminarArchivo = eliminarArchivo;
        vm.descargarArchivo = descargarArchivo;

        // Cargar traducciones
        cargarTraducciones();

        // Instanciar componentes jquery
        instanciarEventosFormatoMoneda();

        // Instanciar autocomples
        initAutocompleteDepartamento();
        initAutocompleteCiudad();
        initAutocompleteEmpresa();
        initAutocompleteTipoDiligencia();
        initAutocompleteProceso();
        initAutocompleteEstadoDiligencia();
        initAutocompleteUsuario();

        vm.deshabilitarFecha = true;
        vm.fecha = moment().format('YYYY-MM-DD');
        vm.indicativoCobro = true;
        vm.indicativoAutorizacion = false;
        vm.deshabilitarEstadoDiligencia = true;


        // Acciones

        function guardar() {
            // Validaciones
            var conErrores = false,
                valorDiligencia = vm.valorDiligencia && vm.valorDiligencia.trim() !== "" ?
                    Number(vm.valorDiligencia.replace(/[^0-9.-]+/g,"")) : null,
                gastoEnvio = vm.gastoEnvio && vm.gastoEnvio.trim() !== "" ?
                    Number(vm.gastoEnvio.replace(/[^0-9.-]+/g,"")) : null,
                otrosGastos = vm.otrosGastos && vm.otrosGastos.trim() !== "" ?
                    Number(vm.otrosGastos.replace(/[^0-9.-]+/g,"")) : null,
                costoDiligencia = vm.costoDiligencia && vm.costoDiligencia.trim() !== "" ?
                    Number(vm.costoDiligencia.replace(/[^0-9.-]+/g,"")) : null,
                costoEnvio = vm.costoEnvio && vm.costoEnvio.trim() !== "" ?
                    Number(vm.costoEnvio.replace(/[^0-9.-]+/g,"")) : null,
                otrosCostos = vm.otrosCostos && vm.otrosCostos.trim() !== "" ?
                    Number(vm.otrosCostos.replace(/[^0-9.-]+/g,"")) : null;

            vm.guardando = true;
            if(!conErrores && vm.form.$valid && !vm.deshabilitarGuardado){
                vm.deshabilitarGuardado = true;
                var datos = {
                    empresa_id: vm.empresaId,
                    proceso_id: vm.procesoId,
                    fecha_diligencia: vm.fecha,
                    tipo_diligencia_id: vm.tipoDiligenciaId,
                    departamento_id: vm.departamentoId,
                    ciudad_id: vm.ciudadId,
                    dependiente_id: vm.usuarioId,
                    valor_diligencia: valorDiligencia,
                    gastos_envio: gastoEnvio,
                    otros_gastos: otrosGastos,
                    costo_diligencia: costoDiligencia,
                    costo_envio: costoEnvio,
                    otros_costos: otrosCostos,
                    estado_diligencia: vm.estadoDiligencia,
                    indicativo_cobro: vm.indicativoCobro,
                    solicitud_autorizacion: vm.indicativoAutorizacion,
                    observaciones_cliente: vm.observacionesCliente,
                    base_64_anexo_diligencia: vm.archivoBase64,
                    nombre_anexo_diligencia: vm.archivoNombre,
                };

                var promesa;
                    promesa = genericService.crear(recurso, datos);


                promesa.then(function(response){
                    if(
                        response.status === Constantes.Response.HTTP_CREATED ||
                        response.status === Constantes.Response.HTTP_OK
                    ){
                        messageUtil.success(response.data.mensajes[0]);
                        volver();
                    }else{
                        messageUtil.error(response.data.mensajes[0]);
                    }
                }).finally(function () {
                    vm.deshabilitarGuardado = false;
                });
            }
        }

        function volver() {
            window.location = "#/diligencias/cliente";
        }

        // Acciones archivos

        function cargarArchivo($archivo) {
            Upload.dataUrl($archivo, true).then(function(base64) {
                if(base64){
                    vm.archivoEliminado = false;
                    vm.archivo = $archivo;
                    vm.archivoNombre = $archivo.name;
                    vm.archivoBase64 = base64;
                }
            });
        }

        function descargarArchivo() {
                var a = document.createElement("a");
                document.body.appendChild(a);
                a.style = "display: none";
                var blob = Upload.dataUrltoBlob(vm.archivoBase64, vm.archivoNombre),
                    url = window.URL.createObjectURL(blob);
                a.href = url;
                a.download = vm.archivoNombre;
                a.click();
                window.URL.revokeObjectURL(url);
        }

        function eliminarArchivo() {
            vm.archivo = null;
            vm.archivoNombre = null;
            vm.archivoBase64 = null;
        }

        // Instanciar autocompletes
        function initAutocompleteEmpresa() {
            vm.configEmpresa = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                onItemAdd: function (value, $item) {
                    if(!vm.cargandoProcesos){
                        vm.cargandoProcesos = true;
                        vm.procesoId = null;
                        vm.opcionesProcesos = [];
                        $timeout(function () {
                            vm.limpiarOpcionesProcesos = true;
                            genericService.obtenerColeccionLigera("procesos-judiciales", {
                                ligera: true,
                                empresa_cliente: value,
                                limite: 100
                            })
                                .then(function (response){
                                    var procesos = response.data;
                                    if(vm.diligenciaOriginal && vm.diligenciaOriginal.proceso_judicial) {
                                        vm.procesoId = vm.diligenciaOriginal.proceso_judicial.id;
                                        procesos.push({
                                            id: vm.diligenciaOriginal.proceso_judicial.id,
                                            numero_proceso: vm.diligenciaOriginal.proceso_judicial.numero_proceso
                                        });
                                    }
                                    vm.opcionesProcesos = [].concat(procesos);
                                }).finally(function () {
                                vm.cargandoProcesos = false;
                            });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.opcionesProcesos = [];
                    vm.procesoId = null;
                    $timeout(function () {
                        vm.limpiarOpcionesProcesos = true;
                    }, 100);
                }
            };

            vm.opcionesEmpresas = [];
            genericService.obtenerColeccionLigera("empresas",{
                ligera: true
            })
                .then(function (response){
                    vm.opcionesEmpresas = [].concat(response.data);
                });
            vm.empresaId = vm.empresa.id;
            vm.deshabilitarEmpresaId = true;
        }

        function initAutocompleteProceso() {
            vm.configProceso = {
                valueField: 'id',
                labelField: 'numero_proceso',
                searchField: ['numero_proceso'],
                sortField: 'numero_proceso',
                allowEmptyOption: false,
                create: false,
                persist: true,
                lock: false,
                load: function(query, callback) {
                    if (!query.length) return callback();
                    genericService.obtenerColeccionLigera("procesos-judiciales", {
                        q: query,
                        ligera: true,
                        limite: 100
                    }).then(function(response){
                        callback(response.data);
                        // Validar so hay datos
                        if(!response.data || response.data.length === 0){
                            vm.message = "No se encuentra en la lista";
                        }
                    }).catch(function(){
                        callback();
                    });
                },
                onItemAdd: function (value, $item) {
                    if(vm.procesoId !== value && !vm.cargandoInfo) {
                        vm.cargandoInfo = true;
                        var opcionesDepartamento = vm.opcionesDepartamentos;
                        vm.departamentoId = null;
                        vm.opcionesDepartamentos = [];

                        $timeout(function (){
                            vm.limpiarOpcionesDepartamento = true;
                            genericService.cargar("procesos-judiciales", vm.procesoId)
                                .then(function (response) {
                                    vm.proceso = response.data;
                                    var departamento = response.data.departamento,
                                        ciudad = response.data.ciudad;
                                    vm.opcionesDepartamentos = opcionesDepartamento || [];
                                    vm.opcionesDepartamentos.push({id: departamento.id, nombre: departamento.nombre});

                                    vm.ciudadIdActual = ciudad.id;
                                    vm.departamentoId = departamento.id;

                                }).finally(function () {
                                    vm.cargandoInfo = false;
                                });
                        }, 100);
                    }
                },
                onItemRemove: function () {}
            };
            vm.opcionesProcesos = [];
        }

        function initAutocompleteTipoDiligencia() {
            vm.configTipoDiligencia = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                onItemAdd: function (value, $item) {
                    if(vm.departamentoId && vm.ciudadId && !vm.cargandoTarifas){
                        vm.cargandoTarifas = true;
                        genericService.obtener('tarifas-diligencias',{
                            tipo_diligencia_id: value,
                            departamento_id: vm.departamentoId,
                            ciudad_id: vm.ciudadId,
                            obtener: true
                        })
                        .then(function(response){
                            if(response.status === Constantes.Response.HTTP_OK) {
                                var datos = response.data;
                                if(datos && datos.length > 0 && datos[0].valor_diligencia){
                                    vm.valorDiligencia = langUtil.formatCurrency(datos[0].valor_diligencia);
                                }
                                if(datos && datos.length > 0 && datos[0].gasto_envio){
                                    vm.gastoEnvio = langUtil.formatCurrency(datos[0].gasto_envio);
                                }
                                if(datos && datos.length > 0 && datos[0].otros_gastos){
                                    vm.otrosGastos = langUtil.formatCurrency(datos[0].otros_gastos);
                                }
                            }
                        }).finally(function () {
                            vm.cargandoTarifas = false;
                        });
                    }

                    if(!vm.cargandoTipoDiligencia){
                        vm.cargandoTipoDiligencia = true;
                        genericService.cargar('tipos-diligencias', value)
                            .then(function(response){
                                if(response.status === Constantes.Response.HTTP_OK) {
                                    vm.indicativoAutorizacion = response.data.solicitud_autorizacion;
                                }
                            }).finally(function () {
                                vm.cargandoTipoDiligencia = false;
                            });
                    }
                }
            };

            vm.opcionesTipoDiligencia = [];
            genericService.obtenerColeccionLigera("tipos-diligencias",{
                ligera: true,
            })
            .then(function (response){
                vm.opcionesTipoDiligencia = [].concat(response.data);
            });
        }

        function initAutocompleteDepartamento() {
            vm.configDepartamento = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                onItemAdd: function (value, $item) {
                    if(!vm.cargandoCiudades){
                        vm.cargandoCiudades = true;
                        vm.ciudadId = null;
                        vm.opcionesCiudad = [];
                        $timeout(function () {
                            vm.limpiarOpcionesCiudad = true;
                            genericService.obtenerColeccionLigera("ciudades", {
                                ligera: true,
                                departamento_id: value
                            })
                            .then(function (response){
                                vm.opcionesCiudad = [].concat(response.data);
                                if(vm.ciudadIdActual){
                                    vm.ciudadId = vm.ciudadIdActual;
                                    vm.ciudadIdActual = null;
                                }
                            }).finally(function () {
                                vm.cargandoCiudades = false;
                            });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.opcionesCiudad = [];
                    vm.ciudadId = null;
                    $timeout(function () {
                        vm.limpiarOpcionesCiudad = true;
                    }, 100);
                }
            };
            vm.opcionesDepartamentos = [];
            genericService.obtenerColeccionLigera("departamentos",{
                ligera: true,
            })
            .then(function (response){
                vm.opcionesDepartamentos = [].concat(response.data);
            });
        }

        function initAutocompleteCiudad() {
            vm.configCiudad = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
                lock: false,
                onItemAdd: function (value, $item) {
                    if(!vm.cargandoUsuarios){
                        vm.cargandoUsuarios = true;
                        vm.usuarioId = null;
                        vm.opcionesUsuario = [];
                        $timeout(function () {
                            vm.limpiarOpcionesUsuario = true;
                            genericService.obtenerColeccionLigera("usuarios",{
                                ligera: true,
                                es_dependiente: true,
                                ciudad_id: value
                            })
                            .then(function (response){
                                var dependientes = response.data;
                                if(vm.diligenciaOriginal && vm.diligenciaOriginal.dependiente){
                                    vm.usuarioId = vm.diligenciaOriginal.dependiente.id;
                                    dependientes.push({
                                        id: vm.diligenciaOriginal.dependiente.id,
                                        nombre: vm.diligenciaOriginal.dependiente.nombre
                                    });
                                }
                                vm.opcionesUsuario = [].concat(dependientes);
                            }).finally(function () {
                                vm.cargandoUsuarios = false;
                            });
                        }, 100);
                    }
                },
                onItemRemove: function () {
                    vm.opcionesUsuario = [];
                    vm.usuarioId = null;
                    $timeout(function () {
                        vm.limpiarOpcionesUsuario = true;
                    }, 100);
                }
            };
            vm.opcionesCiudad = [];
        }

        function initAutocompleteEstadoDiligencia() {
            vm.configEstadoDiligencia = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'id',
                allowEmptyOption: false,
                create: false,
                persist: true,
                lock: false
            };
                vm.opcionesEstadoDiligencia = [
                    {id: Constantes.EstadoDiligencia.SOLICITADO, nombre: "Solicitado"},
                ];
            vm.estadoDiligencia = Constantes.EstadoDiligencia.SOLICITADO;
        }

        function initAutocompleteUsuario() {
            vm.configUsuario = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
            };
            vm.opcionesUsuario = [];
            genericService.obtenerColeccionLigera("usuarios",{
                ligera: true,
                es_dependiente: true,
                ciudad_id: vm.ciudadId ? vm.ciudadId : null
            })
            .then(function (response){
                vm.opcionesUsuario = [].concat(response.data);
            }).finally(function () {
                vm.cargandoUsuarios = false;
            });
        }

        // Instanciar componentes jquery

        function instanciarEventosFormatoMoneda() {
            $("input[data-type='currency']").on({
                keyup: function() {
                    langUtil.jqueryFormatCurrency($(this));
                },
                blur: function() {
                    langUtil.jqueryFormatCurrency($(this), "blur");
                }
            });
        }

        // Traducciones

        function cargarTraducciones() {
            $translate([
                "SOLICITADO", "COTIZADO", "APROBADO", "TRAMITE", "TERMINADO", "CONTABILIZADO", "FACTURADO"
            ]).then(function (translations) {
                i18n.SOLICITADO = translations.SOLICITADO;
                i18n.COTIZADO = translations.COTIZADO;
                i18n.APROBADO = translations.APROBADO;
                i18n.TRAMITE = translations.TRAMITE;
                i18n.TERMINADO = translations.TERMINADO;
                i18n.CONTABILIZADO = translations.CONTABILIZADO;
                i18n.FACTURADO = translations.FACTURADO;
            }, function (translationIds) {});
        }

    }
})();

(function() {
    'use strict';

    angular
        .module('enlaces.app')
        .controller('procesoGeneralController', procesoGeneralController);

    procesoGeneralController.$inject = [
        '$scope',
        '$routeParams',
        '$timeout',
        'sessionUtil',
        'envService',
        'genericService',
        'messageUtil',
        'Constantes'
    ];

    function procesoGeneralController(
        $scope, $routeParams, $timeout, sessionUtil, envService, genericService, messageUtil, Constantes
    ){
        var vm = this,
            pipePromise,
            recurso = "procesos-generales";

        vm.coleccion = [];
        vm.paginacion = {};
        vm.itemsPorPagina = 5;
        $scope.isReset = true;
        vm.titulo = sessionUtil.getTituloVista($routeParams.o);

        // Control de grid
        vm.obtenerResumen = obtenerResumen;
        vm.limpiarFiltros = limpiarFiltros;

        // Acciones
        vm.buscar = buscar;
        vm.obtenerExcel = obtenerExcel;

        // inicial autocomples
        initAutocompleteEmpresa();

        // Limpiar filtros
        limpiarFiltros();

        // Grid

        function buscar() {
            vm.buscando = true;
            if(vm.form.$valid && !vm.deshabilitarBuscado) {
                vm.activarBuscar = true;
                $scope.$broadcast('refreshGrid');
            }
        }

        function obtenerResumen(tableState, ctrl){
            if(tableState.pagination && !tableState.pagination.number){
                tableState.pagination.number = vm.itemsPorPagina;
            }else{
                vm.itemsPorPagina = tableState.pagination.number;
            }

            if(vm.paginacion && vm.paginacion.paginaActual && tableState.pagination){
                var start = tableState.pagination.start || 0,
                    number = tableState.pagination.number || 10,
                    nextPage = (start + number) / number;
                if(vm.paginacion.paginaActual != nextPage){
                    vm.activarBuscar = true;
                }else if(vm.activarBuscar){
                    tableState.pagination.start = 0;
                    tableState.pagination.number = vm.itemsPorPagina;
                }
            }

            if(vm.activarBuscar && !vm.cargando){
                vm.cargando = true;
                genericService.obtenerColeccion(recurso, tableState).then(function (response){
                    if(response.status === Constantes.Response.HTTP_OK){
                        var coleccion = response.data.datos;
                        if(coleccion && coleccion.length > 0 ){
                            vm.coleccion = [].concat(coleccion);
                            vm.paginacion.ultimaPagina = response.data.ultima_pagina;
                            vm.paginacion.paginaActual = response.data.pagina_actual;
                            vm.paginacion.desde = response.data.desde;
                            vm.paginacion.hasta = response.data.hasta;
                            vm.paginacion.total = response.data.total;
                            tableState.pagination.numberOfPages = response.data.ultima_pagina;
                        }else{
                            vm.coleccion = [];
                        }
                    }
                }).finally(function () {
                    vm.buscando = false;
                    vm.cargando = false;
                    vm.activarBuscar = false;
                });
            }
        }

        function limpiarFiltros(){
            vm.filtroEmpresa = null;
            vm.filtroProceso = null;
            vm.filtroDemandante = null;
            vm.filtroDemandado = null;
            vm.activarBuscar = true;
            $scope.isReset = true;
        }

        // Acciones

        function obtenerExcel() {
            genericService.obtenerArchivo(recurso + "/excel", {
                empresa: vm.filtroEmpresa,
                numero_proceso: vm.filtroProceso,
                demandante: vm.filtroDemandante,
                demandado: vm.filtroDemandado
            });
        }

        function initAutocompleteEmpresa() {
            vm.configEmpresa = {
                valueField: 'id',
                labelField: 'nombre',
                searchField: ['nombre'],
                sortField: 'nombre',
                allowEmptyOption: false,
                create: false,
                persist: true,
            };
            vm.opcionesEmpresa = [];
            genericService.obtenerColeccionLigera("empresas",{
                ligera: true,
            })
                .then(function (response){
                    vm.opcionesEmpresa = [].concat(response.data);
                });
        }
    }
})();

(function(angular) {
    'use strict';

    angular.module('enlaces.app')
        .factory('genericService', genericService);

    genericService.$inject = ['$http', 'fileUtil', 'envService'];

    function genericService($http, fileUtil, envService){

        return {
            obtener: obtener,
            cargar: cargar,
            crear: crear,
            modificar: modificar,
            eliminar: eliminar,
            cargarArchivo: cargarArchivo,
            obtenerArchivo: obtenerArchivo,
            obtenerColeccion: obtenerColeccion,
            obtenerColeccionLigera: obtenerColeccionLigera

        };

        function obtener(recurso, params){
            return $http.get(envService.read('apiUrl') + recurso, params ? {
                params: params
            } : null)
            .then(function (response){
                return response;
            })
            .catch(function (error){
                return error;
            });
        }

        function cargar(recurso, id){
            return $http.get(envService.read('apiUrl') + recurso + "/" + id)
                .then(function (response){
                    return response;
                })
                .catch(function (error){
                    return error;
                });
        }

        function crear(recurso, object) {
            return $http.post(envService.read('apiUrl') + recurso, object)
                .then(function (response){
                    return response;
                })
                .catch(function (error){
                    return error;
                });
        }

        function modificar(recurso, object) {
            return $http.put(envService.read('apiUrl') + recurso + (object.id ? "/" + object.id : ""), object)
                .then(function (response){
                    return response;
                })
                .catch(function (error){
                    return error;
                });
        }

        function eliminar(recurso, id, params) {
            return $http.delete(envService.read('apiUrl') + recurso + (id ? "/" + id : ""), params ? {
                params: params
            } : null)
                .then(function (response){
                    return response;
                })
                .catch(function (error){
                    return error;
                });
        }

        function obtenerArchivo(recurso, params){
            return fileUtil.get(envService.read('apiUrl') + recurso, params, true);
        }

        function cargarArchivo(recurso, file){
            var fd = new FormData();
            fd.append('file', file);
            return $http.post(envService.read('apiUrl') + recurso, fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            });
        }

        function obtenerColeccion(recurso, tableState) {
            var pagination = tableState.pagination,
                search = tableState.search && tableState.search.predicateObject ? tableState.search.predicateObject : {},
                sort = tableState.sort && tableState.sort.predicate ? tableState.sort.predicate : [],
                start = pagination.start || 0,
                number = pagination.number || 10,
                actualPage = (start + number) / number,
                searchStr = "?page=" + actualPage + "&limite=" + number,
                sortStr = "&ordenar_por=";

            angular.forEach(search, function (value, index) {
                searchStr += "&" + index + "=" + value;
            });

            angular.forEach(sort, function (value, index) {
                sortStr += value.predicate + ":" + (value.reverse ? "desc" : "asc");
                if((index + 1) < sort.length){
                    sortStr += ",";
                }
            });

            return $http.get(envService.read('apiUrl') + recurso + searchStr + (sort.length > 0 ? sortStr : ""))
            .then(function (response){
                return response;
            })
            .catch(function (error){
                return error;
            });
        }

        function obtenerColeccionLigera(recurso, params) {
            return $http.get(envService.read('apiUrl') + recurso, params ? {
                params: params
            } : null)
            .then(function (response){
                return response;
            })
            .catch(function (error){
                return error;
            });
        }

    }
})(window.angular);

(function() {
    'use strict';

    angular.module('enlaces.app')
        .factory('fileUtil', fileUtil);

    fileUtil.$inject = [
        '$http',
        'Constantes'
    ];

    function fileUtil($http, Constantes){

        return {
            get: get
        };

        function get(httpPath, params, toDownload, method) {
            var promise = null;
            if(method && method === "POST"){
                promise = $http.post(httpPath, params, {
                    responseType: 'arraybuffer'
                });
            }else{
                promise = $http.get(httpPath, {
                    params: params,
                    responseType: 'arraybuffer'
                });
            }

            return promise
            .then(openComplete)
            .catch(openFailed);

            function openComplete(response) {
                if(response.status === Constantes.Response.HTTP_OK){
                    var octetStreamMime = 'application/octet-stream';
                    var success = false;

                    // Get the headers
                    var headers = response.headers();

                    // Get the filename from the [x-filename|content-disposition] header or default to "file"
                    var filename = "file";
                    if(headers['x-filename']){
                        filename = headers['x-filename'];
                    }else if(headers['content-disposition']) {
                        //Content-Disposition: inline; filename="file"
                        filename = headers['content-disposition'].split(";")[1].split("=")[1].replace(/"/gi, "");
                    }

                    // Determine the content type from the header or default to "application/octet-stream"
                    var contentType = headers['content-type'] || octetStreamMime;

                    // Prepare a blob URL
                    var blob = new Blob([response.data], { type: contentType });

                    try {
                        // Try using saveBlob if supported -- IE11 & Edge --
                        if(toDownload){
                            if(navigator.msSaveBlob)
                                navigator.msSaveBlob(blob, filename);
                            else {
                                // Try using other saveBlob implementations, if available
                                var saveBlob = navigator.webkitSaveBlob || navigator.mozSaveBlob || navigator.saveBlob;
                                if(saveBlob === undefined) throw "Not supported";
                                saveBlob(blob, filename);
                            }
                        }else {
                            navigator.msSaveOrOpenBlob(blob, filename);
                        }
                        success = true;
                    } catch(ex) {
                        console.log(ex);
                    }

                    // Get the blob url creator
                    if(!success){
                        var urlCreator = window.URL || window.webkitURL || window.mozURL || window.msURL;
                        var url = urlCreator.createObjectURL(blob);
                        if(urlCreator) {
                            // Try to use a download link
                            var link = document.createElement('a');
                            try {
                                if(toDownload){
                                    link.href = url;
                                    link.download = filename;
                                    link.click();
                                } else {
                                    var isChrome = false,
                                        isChromium = window.chrome,
                                        winNav = window.navigator,
                                        vendorName = winNav.vendor,
                                        isOpera = winNav.userAgent.indexOf("OPR") > -1,
                                        isIEedge = winNav.userAgent.indexOf("Edge") > -1,
                                        isIOSChrome = winNav.userAgent.match("CriOS");

                                    if (isIOSChrome) {
                                        isChrome = true;
                                    } else {
                                        isChrome = !!(isChromium !== null && typeof isChromium !== "undefined" &&
                                        vendorName === "Google Inc." && isOpera === false && isIEedge === false);
                                    }

                                    if(isChrome){
                                        link.href = url;
                                        link.download = filename;
                                        link.click();
                                    } else {
                                        var reader = new FileReader();
                                        reader.onload = function(event){
                                            window.open(event.target.result, '_blank');
                                        };
                                        reader.readAsDataURL(blob);
                                    }
                                }

                                success = true;
                            } catch(ex) {
                                console.log(ex);
                            }

                            if(!success) {
                                // Fallback to window.location method
                                try {
                                    // Prepare a blob URL
                                    // Use application/octet-stream when using window.location to force download
                                    window.location = url;
                                    success = true;
                                } catch(ex) {
                                    console.log(ex);
                                }
                            }
                        }
                    }

                    if(!success) {
                        // Fallback to window.open method
                        window.open(httpPath, '_blank', '');
                    }
                }

                return response;
            }

            function openFailed(error){
                console.log(error);
                return error;
            }
        }

    }

})();
(function($) {
    'use strict';

    angular.module('enlaces.app')
        .factory('messageUtil', messageUtil);

    function messageUtil(){

        return {
            error: error,
            warning: warning,
            success: success,
            showFormError: showFormError,
            showSnackbar: showSnackbar
        };
        
        function error(message) {
            MDSnackbars.hide();
            MDSnackbars.show({
                type: 'danger',
                text: '<div style="position: relative;"><span>' + message + '</span><a type="button" id="snackbar-action" style="cursor: pointer; position: absolute; right: 0; color: black;"><i class="material-icons">close</i></a></div>',
                html: true,
                fullWidth: true,
                clickToClose: true,
                timeout: 10000,
                animation: 'slideup'
            });
            setTimeout(function () {
                $("#snackbar-action").off("click").on("click", function () {
                    MDSnackbars.hide();
                })
            }, 1000);
        }

        function warning(message) {
            MDSnackbars.hide();
            MDSnackbars.show({
                type: 'warning',
                text: '<div style="position: relative;"><span>' + message + '</span><a type="button" id="snackbar-action" style="cursor: pointer; position: absolute; right: 0; color: black;"><i class="material-icons">close</i></a></div>',
                html: true,
                fullWidth: true,
                clickToClose: true,
                timeout: 10000,
                animation: 'slideup'
            });
            setTimeout(function () {
                $("#snackbar-action").off("click").on("click", function () {
                    MDSnackbars.hide();
                })
            }, 1000);
        }
        
        function success(message) {
            MDSnackbars.hide();
            MDSnackbars.show({
                type: 'success',
                text: '<div style="position: relative;"><span>' + message + '</span><a type="button" id="snackbar-action" style="cursor: pointer; position: absolute; right: 0; color: black;"><i class="material-icons">close</i></a></div>',
                html: true,
                fullWidth: true,
                clickToClose: true,
                timeout: 10000,
                animation: 'slideup'
            });
            setTimeout(function () {
                $("#snackbar-action").off("click").on("click", function () {
                    MDSnackbars.hide();
                })
            }, 1000);
        }

        function showSnackbar(options, actionOptions) {
            MDSnackbars.hide();
            MDSnackbars.show(options);
            if(actionOptions){
                //Se pone el timeout por que se demora unos segundos en pintarse el snackbar luego del show
                setTimeout(function () {
                    $("#snackbar-action").off("click").on("click", function () {
                        angular.element(document.getElementById(actionOptions.scopeElement)).scope()[actionOptions.controllerName][actionOptions.callFunction].apply(this, actionOptions.parameters);
                        angular.element(document.getElementById(actionOptions.scopeElement)).scope().$apply();
                    })
                }, 1000);
            }
        }

        function showFormError(message) {
            $("#error-message").html(message);
        }
    }
})(jQuery);
(function() {
    'use strict';

    angular.module('enlaces.app')
        .factory('sessionUtil', sessionUtil);

    function sessionUtil(){

        return {
            getAll: getAll,
            getRol: getRol,
            getUsuario: getUsuario,
            getEmpresa: getEmpresa,
            getTipoDocumento: getTipoDocumento,
            getPermisos: getPermisos,
            getParametros: getParametros,
            getTituloVista: getTituloVista,
            can: can
        };

        function getAll() {
            return JSON.parse(localStorage.getItem('session'));
        }

        function getRol() {
            var session = JSON.parse(localStorage.getItem('session'));
            return session.rol;
        }

        function getUsuario() {
            var session = JSON.parse(localStorage.getItem('session'));
            return session.usuario;
        }

        function getEmpresa() {
            var session = JSON.parse(localStorage.getItem('session'));
            return session.empresa;
        }

        function getTipoDocumento() {
            var session = JSON.parse(localStorage.getItem('session'));
            return session.tipo_documento;
        }

        function getPermisos() {
            var session = JSON.parse(localStorage.getItem('session'));
            return session.permisos;
        }

        function getParametros() {
            var session = JSON.parse(localStorage.getItem('session'));
            return session.parametros;
        }

        function getTituloVista(opcionId) {
            var opcionesDelSistema = JSON.parse(localStorage.getItem('opcionesDelSistema')),
                opcion = _.find(opcionesDelSistema, function (opcionAux) {
                    return opcionAux.id == opcionId;
                });

            return opcion ? opcion.nombre : '';
        }

        function can(permisos) {
            var can = false,
                permisoAux = null,
                session = JSON.parse(localStorage.getItem('session'));

            angular.forEach(permisos, function (permisoAValidar) {
                permisoAux = _.find(session.permisos, function (permiso) {
                    return permiso === permisoAValidar;
                });

                if(permisoAux){
                    can = true;
                }
            });

            return can;
        }

    }

})();

(function() {
    'use strict';

    angular.module('enlaces.app')
        .factory('langUtil', langUtil);

    function langUtil(){

        return {
            isBlank: isBlank,
            isNotBlank: isNotBlank,
            formatCurrency: formatCurrency,
            jqueryFormatCurrency: jqueryFormatCurrency,
            zeroFill: zeroFill,
            getSize: getSize
        };

        function isBlank(value) {
            value = _.trim(value);
            return _.isEmpty(value) && !_.isNumber(value) || _.isNaN(value);
        }

        function isNotBlank(value) {
            return !isBlank(value);
        }

        function formatNumber(n) {
            // format number 1000000 to 1,234,567
            return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        }

        function formatCurrency(number, decimals) {
            number = number ? (number + "").trim() : null;
            if(!number || isNaN(number)){
                return number;
            }

            var formateador = new Intl.NumberFormat('en-US', {
                style: 'currency',
                currency: 'USD',
                minimumFractionDigits: decimals ? decimals : 2
            });

            return formateador.format(number);
        }

        function jqueryFormatCurrency(input, blur) {
            // appends $ to value, validates decimal side
            // and puts cursor back in right position.

            // get input value
            var input_val = input.val();

            // don't validate empty input
            if (input_val === "") { return; }

            // original length
            var original_len = input_val.length;

            // initial caret position
            var caret_pos = input.prop("selectionStart");

            // check for decimal
            if (input_val.indexOf(".") >= 0) {

                // get position of first decimal
                // this prevents multiple decimals from
                // being entered
                var decimal_pos = input_val.indexOf(".");

                // split number by decimal point
                var left_side = input_val.substring(0, decimal_pos);
                var right_side = input_val.substring(decimal_pos);

                // add commas to left side of number
                left_side = formatNumber(left_side);

                // validate right side
                right_side = formatNumber(right_side);

                // On blur make sure 2 numbers after decimal
                if (blur === "blur") {
                    right_side += "00";
                }

                // Limit decimal to only 2 digits
                right_side = right_side.substring(0, 2);

                // join number by .
                input_val = "$" + left_side + "." + right_side;

            } else {
                // no decimal entered
                // add commas to number
                // remove all non-digits
                input_val = formatNumber(input_val);
                input_val = "$" + input_val;

                // final formatting
                if (blur === "blur") {
                    input_val += ".00";
                }
            }

            // send updated string to input
            input.val(input_val);

            // put caret back in the right position
            var updated_len = input_val.length;
            caret_pos = updated_len - original_len + caret_pos;
            input[0].setSelectionRange(caret_pos, caret_pos);
        }

        function zeroFill(number, width)
        {
            width -= number.toString().length;
            if ( width > 0 ) {
                return new Array( width + (/\./.test( number ) ? 2 : 1) ).join('0') + number;
            }
            return number + "";
        }

        function getSize(obj) {
            var size = 0,
                key;
            for (key in obj) {
                if (obj.hasOwnProperty(key)) size++;
            }
            return size;
        }

    }

})();

(function() {
    'use strict';

    angular.module('enlaces.app')
        .constant('Constantes', {
            Response: {
                HTTP_OK: 200,
                HTTP_CREATED: 201,
                HTTP_ACCEPTED: 202,
                HTTP_NO_CONTENT: 204,
                HTTP_BAD_REQUEST: 400,
                HTTP_UNAUTHORIZED: 401,
                HTTP_FORBIDDEN: 403,
                HTTP_NOT_FOUND: 404,
                HTTP_CONFLICT: 409
            },
            SiNo: {
                SI: 'S',
                NO: 'N'
            },
            TipoRol: {
                CLIENTE: 1,
                SISTEMA: 2,
                CLIENTE_DE_CLIENTE: 3
            },
            ClaseConcepto: {
                DEBITO: 'DB',
                CREDITO: 'CR'
            },
            TipoConcepto: {
                COSTOS_INTERESES_DEMANDA: 'D',
                HONORARIOS_EXTRAS: 'G',
                COSTOS_DE_PROCESOS: 'C'
            },
            EstadoDiligencia: {
                SOLICITADO: 1,
                COTIZADO: 2,
                APROBADO: 3,
                TRAMITE: 4,
                TERMINADO: 5,
                CONTABILIZADO: 6,
                FACTURADO: 7
            },
            CambioInstancia: {
                SUPERIOR: 1,
                INFERIOR: 2
            }
        });
})();

(function() {
    'use strict';

    angular.module('enlaces.app')
    .directive('pageSelect', pageSelect);

    function pageSelect(){
        return {
            restrict: 'E',
            template: '<div><div style="float: left; margin-right: 4px; margin-top: 7px;">Página: </div><div style="float: left; width: 35px; margin-top: 6px;"><input class="input-grid" style="text-align: center; width: 40px;" type="number" ng-model="inputPage" ng-change="selectPage(inputPage)"></div></div>',
            link: function(scope, element, attrs) {
                scope.$watch('currentPage', function(c) {
                    scope.inputPage = c;
                });
            }
        }
    }
})();
(function() {
    'use strict';

    angular.module('enlaces.app')
    .directive('stPersist', persist);

    function persist(){
        return {
            require: '^stTable',
            link: function (scope, element, attr, ctrl) {
                var nameSpace = attr.stPersist;
                //save the table state every time it changes
                scope.$watch(function () {
                    return ctrl.tableState();
                }, function (newValue, oldValue) {
                    if (newValue !== oldValue) {
                        localStorage.setItem(nameSpace, JSON.stringify(newValue));
                    }
                }, true);
                //fetch the table state when the directive is loaded
                if (localStorage.getItem(nameSpace)) {
                    ctrl.setKeepPaging(true);
                    var savedState = JSON.parse(localStorage.getItem(nameSpace));
                    var tableState = ctrl.tableState();
                    angular.extend(tableState, savedState);
                    ctrl.pipe();
                }

            }
        };
    }
})();
(function() {
    'use strict';

    angular.module('enlaces.app')
    .directive('stReset', reset);

    function reset(){
        return {
            require: '^stTable',
            link: function (scope, element, attr, ctrl) {
                scope.$watch(function () {
                    if(scope.isReset) {
                        // remove local storage
                        if (attr.stPersist) {
                            localStorage.removeItem(attr.stPersist);
                        }
                        // reset table state
                        var tableState = ctrl.tableState();
                        tableState.search = {};
                        tableState.sort = {};
                        tableState.pagination.start = 0;
                        ctrl.pipe();
                        // reset scope value
                        scope.isReset = false;
                    }
                });
            }
        };
    }
})();
(function() {
    'use strict';

    angular.module('enlaces.app')
    .directive('stResetSecondary', reset);

    function reset(){
        return {
            require: '^stTable',
            link: function (scope, element, attr, ctrl) {
                scope.$watch(function () {
                    if(scope.isResetSecondary) {
                        // remove local storage
                        if (attr.stPersist) {
                            localStorage.removeItem(attr.stPersist);
                        }
                        // reset table state
                        var tableState = ctrl.tableState();
                        tableState.search = {};
                        tableState.sort = {};
                        tableState.pagination.start = 0;
                        ctrl.pipe();
                        // reset scope value
                        scope.isResetSecondary = false;
                    }
                });
            }
        };
    }
})();
(function() {
    'use strict';

    angular.module('enlaces.app')
        .directive('stSearchToggle', stSearchToggle);

    function stSearchToggle(){
        return {
            restrict: 'AE',
            require: '^stTable',
            scope: {
                searchModel: '=',
                searchPredicate: '@'
            },
            link: function(scope, element, attr, table) {
                scope.$watch('searchModel', function(){
                    table.search(scope.searchModel, scope.searchPredicate);
                }, true);
            }
        }
    }
})();
(function() {
    'use strict';

    angular.module('enlaces.app')
    .directive('refreshTable', refreshTable);

    function refreshTable(){
        return {
            require:'stTable',
            restrict: "A",
            link:function(scope, elem, attr, table){
                scope.$on("refreshGrid", function() {
                    table.pipe(table.tableState());
                });
            }
        };
    }
})();
(function() {
    'use strict';

    angular.module('enlaces.app')
        .directive('focusMe', focusMe);

    focusMe.$inject = [
        '$timeout',
        '$parse'
    ];

    function focusMe($timeout, $parse){
        return {
            link: function (scope, element, attrs) {
                var model = $parse(attrs.focusMe);
                scope.$watch(model, function (value) {
                    console.log('value=', value);
                    if (value === true) {
                        $timeout(function () {
                            element[0].focus();
                        });
                    }
                });
                // to address @blesh's comment, set attribute value to 'false'
                // on blur event:
                element.bind('blur', function () {
                    console.log('blur');
                    scope.$apply(model.assign(scope, false));
                });
            }
        };
    }
})();
(function () {
    'use strict';

    angular
        .module('enlaces.app')
        .provider('datetimepicker', function () {
            var default_options = {};

            this.setOptions = function (options) {
                default_options = options;
            };

            this.$get = function () {
                return {
                    getOptions: function () {
                        return default_options;
                    }
                };
            };
        })
        .directive('datetimepicker', [
            '$timeout',
            'datetimepicker',
            function ($timeout,
                      datetimepicker) {

                var default_options = datetimepicker.getOptions();

                return {
                    require: ['?ngModel'],
                    restrict: 'AE',
                    scope: {
                        datetimepickerOptions: '@',
                        searchModel: '=',
                        searchPredicate: '@'
                    },
                    link: function ($scope, $element, $attrs, directives) {
                        var passed_in_options = $scope.$eval($attrs.datetimepickerOptions);
                        var options = jQuery.extend({}, default_options, passed_in_options);

                        var ngModelCtrl = directives[0];

                        $element
                            .on('dp.change', function (e) {
                                if (ngModelCtrl) {
                                    $timeout(function () {
                                        ngModelCtrl.$setViewValue(e.target.value);
                                    });
                                }
                            })
                            .datetimepicker(options);

                        function setPickerValue() {
                            var date = null;

                            if (ngModelCtrl && ngModelCtrl.$viewValue) {
                                date = ngModelCtrl.$viewValue;
                            }

                            $element
                                .data('DateTimePicker')
                                .date(date);
                        }

                        if (ngModelCtrl) {
                            ngModelCtrl.$render = function () {
                                setPickerValue();
                            };
                        }

                        setPickerValue();
                    }
                };
            }
        ]);
})();
(function () {
    'use strict';

    angular
        .module('enlaces.app')
        .provider('datetimepickerStTable', function () {
            var default_options = {};

            this.setOptions = function (options) {
                default_options = options;
            };

            this.$get = function () {
                return {
                    getOptions: function () {
                        return default_options;
                    }
                };
            };
        })
        .directive('datetimepickerStTable', [
            '$timeout',
            'datetimepickerStTable',
            function ($timeout,
                      datetimepickerStTable) {

                var default_options = datetimepickerStTable.getOptions();

                return {
                    require: ['?ngModel','^?stTable'],
                    restrict: 'AE',
                    scope: {
                        datetimepickerOptions: '@',
                        searchModel: '=',
                        searchPredicate: '@'
                    },
                    link: function ($scope, $element, $attrs, directives) {
                        var passed_in_options = $scope.$eval($attrs.datetimepickerOptions);
                        var options = jQuery.extend({}, default_options, passed_in_options);

                        var ngModelCtrl = directives[0],
                            table = directives[1];

                        $element
                            .on('dp.change', function (e) {
                                if (ngModelCtrl) {
                                    $timeout(function () {
                                        ngModelCtrl.$setViewValue(e.target.value);
                                    });
                                }
                            })
                            .datetimepicker(options);

                        function setPickerValue() {
                            var date = null;

                            if (ngModelCtrl && ngModelCtrl.$viewValue) {
                                date = ngModelCtrl.$viewValue;
                            }

                            $element
                                .data('DateTimePicker')
                                .date(date);
                        }

                        if (ngModelCtrl) {
                            ngModelCtrl.$render = function () {
                                setPickerValue();
                            };
                        }

                        $scope.$watch('searchModel', function(){
                            if(table){
                                table.search($scope.searchModel, $scope.searchPredicate);
                            }
                        }, true);

                        setPickerValue();
                    }
                };
            }
        ]);
})();
(function (angular) {
    'use strict';

    angular.module('enlaces.app')
        .directive('selectize', selectize);

    function selectize($timeout){
        return {
            restrict: 'A',
            require: ['ngModel', '^?stTable'],
            scope: {
                selectize: '&',
                options: '&',
                onFocus: '&focus',
                disabledInput: '=',
                disabledTyping: '=',
                clearOptions: '=',
                searchModel: '=',
                searchPredicate: '@'
            },
            link: function (scope, element, attrs, directives) {
                var changing, options, selectize, invalidValues = [];
                var data = scope.options();
                var settings = scope.selectize();

                var ngModel = directives[0],
                    table = directives[1];

                // Default options
                options = angular.extend({
                    delimiter: ',',
                    persist: true,
                    mode: (element[0].tagName === 'SELECT') ? ((element[0].hasAttribute('multiple')) ? 'multi' : 'single') : 'multi' // if element isn't select or has attr 'multiple' set mode 'multi'
                }, settings || {});

                // Activate the widget
                selectize = element.selectize(options)[0].selectize;

                /*if(scope.disabledTyping){
                    selectize.$control_input.on('keydown', function(e) {
                        var key = e.charCode || e.keyCode;
                        if(key !== 8){
                            e.preventDefault();
                        }
                    });
                }*/

                selectize.on('change', function () {
                    setModelValue(selectize.getValue());
                });

                selectize.on('focus', function () {
                    scope.onFocus();
                    scope.$apply();
                });

                function setModelValue(value) {
                    if (changing) {
                        return;
                    }
                    scope.$parent.$apply(function () {
                        ngModel.$setViewValue(value);
                    });

                    if (options.mode === 'single') {
                        selectize.blur();
                    }
                }

                // Normalize the model value to an array
                function parseValues(value) {
                    if (angular.isArray(value)) {
                        return value;
                    }
                    if (!value) {
                        return [];
                    }
                    return String(value).split(options.delimiter);
                }

                // Non-strict indexOf
                function indexOfLike(arr, val) {
                    for (var i = 0; i < arr.length; i++) {
                        if (arr[i] === val) {
                            return i;
                        }
                    }
                    return -1;
                }

                // Boolean wrapper to indexOfLike
                function contains(arr, val) {
                    return indexOfLike(arr, val) !== -1;
                }

                // Store invalid items for late-loading options
                function storeInvalidValues(values, resultValues) {
                    values.map(function (val) {
                        if (!(contains(resultValues, val) || contains(invalidValues, val))) {
                            invalidValues.push(val);
                        }
                    });
                }

                function restoreInvalidValues(newOptions, values) {
                    var i, index;
                    for (i = 0; i < newOptions.length; i++) {
                        index = indexOfLike(invalidValues, newOptions[i][selectize.settings.valueField]);
                        if (index !== -1) {
                            values.push(newOptions[i][selectize.settings.valueField]);
                            invalidValues.splice(index, 1);
                        }
                    }
                }

                function setSelectizeValue(value) {
                    $timeout(function () {
                        var values = parseValues(value);

                        if (changing || values === parseValues(selectize.getValue())) {
                            return;
                        }

                        changing = true;

                        selectize.setValue(values);
                        storeInvalidValues(values, parseValues(selectize.getValue()));

                        changing = false;
                    });
                }

                function setSelectizeOptions(newOptions) {
                    var values = parseValues(ngModel.$viewValue);

                    if (options.mode === 'multi' && newOptions) {
                        restoreInvalidValues(newOptions, values);
                    }

                    selectize.addOption(newOptions);
                    selectize.refreshOptions(false);
                    setSelectizeValue(values);
                }

                scope.$parent.$watch(attrs.ngModel, setSelectizeValue);

                if (attrs.options) {
                    scope.$parent.$watch(attrs.options, setSelectizeOptions, true);
                }

                scope.$parent.$watch(data, setSelectizeOptions(data), true);

                scope.$on('$destroy', function () {
                    selectize.destroy();
                });

                //Clear options
                scope.$watch('clearOptions', function(){
                    if(scope.clearOptions){
                        selectize.clearOptions();
                        scope.clearOptions = false;
                    }
                });

                //Disabled input
                scope.$watch('disabledInput', function(){
                    if(scope.disabledInput){
                        selectize.disable();
                    } else {
                        selectize.enable();
                    }
                });

                //Disabled typing
                /*scope.$watch('disabledTyping', function(){
                    if(scope.disabledTyping){
                        selectize.$control_input.on('keydown', function(e) {
                            var key = e.charCode || e.keyCode;
                            if(key !== 8){
                                e.preventDefault();
                            }
                        });
                    } else {
                        selectize.$control_input.off('keydown');
                    }
                });*/

                //Search for smart table
                scope.$watch('searchModel', function(){
                    if(table){
                        table.search(scope.searchModel, scope.searchPredicate);
                    }
                }, true);
            }
        };
    }
})(this.angular);
